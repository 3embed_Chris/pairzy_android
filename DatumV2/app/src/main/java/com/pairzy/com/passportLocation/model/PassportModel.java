package com.pairzy.com.passportLocation.model;

import android.location.Address;
import android.location.Geocoder;
import android.text.TextUtils;
import com.pairzy.com.data.source.PassportLocationDataSource;
import com.pairzy.com.locationScreen.model.LocationHolder;
import com.pairzy.com.util.Utility;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
/**
 * <H>PassportModel class</H>
 * @author 3Embed.
 * @since 21/5/18.
 */
public class PassportModel
{
    @Inject
    ArrayList<PassportLocation> arrayList;
    @Inject
    PassportAdapter passportAdapter;
    @Inject
    Geocoder geocoder;
    @Inject
    Utility utility;
    @Inject
    PassportLocationDataSource locationDataSource;

    private final int MAX_SIZE = 5;

    @Inject
    PassportModel(){
    }

    /*
     * Add PassportLocation item to arrayList at first position
     * and maintain the MAX_SIZE size of arrayList.
     */
    public void addItemToList(PassportLocation passportLocation)
    {
        try {
            if (getListSize() == MAX_SIZE)
            {
                arrayList.remove(MAX_SIZE-1);
            }
            arrayList.add(0,passportLocation);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /*
     *Return Size of arrayList
     */
    public int getListSize(){
        return arrayList.size();
    }

    public PassportLocation getItem(int position) {
        if(position < getListSize())
            return arrayList.get(position);
        return null;
    }

    /**
     * Remove item at specified position.
     * @param position
     * @return remove_status.
     */
    public boolean removeItem(int position)
    {
        if(position < getListSize())
        {
            arrayList.remove(position);
            return true;
        }
        return false;
    }


    public void notifyAdapter()
    {
        passportAdapter.notifyDataSetChanged();
    }

    /**
     * Select an item at position in arrayList.
     * @param position
     * @return Selected Item.
     */
    public void handleItemSelection(int position)
    {
        for(int i = 0; i < getListSize(); i++)
        {
            PassportLocation location = arrayList.get(i);
            try
            {
                if(position == i)
                {
                    location.setSelected(true);
                }else
                {
                    location.setSelected(false);
                }
            }catch (Exception e){}
        }
    }

    /**
     * Checking is that item at position selection changed or not.
     * @param position  item position
     * @return isItemSelectionChanged.
     */
    public boolean isSelectionChanged(int position)
    {
        try
        {
            if(position < getListSize())
            {
                PassportLocation passportLocation = arrayList.get(position);
                return !passportLocation.isSelected();
            }
        }catch (Exception e){}
        return false;
    }

    /**
     * Fetch the full address via lat and long.
     * @param locationHolder contain lat , long.
     * @return PassportLocation.
     */
    public PassportLocation getLocationName(LocationHolder locationHolder) {
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(locationHolder.getLatitude(),locationHolder.getLongitude(),1);
            if(addresses != null){
                String  subLocation = addresses.get(0).getSubLocality();
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String addressName = addresses.get(0).getFeatureName();
                boolean cityEmpty = TextUtils.isEmpty(city);
                boolean stateEmpty = TextUtils.isEmpty(state);
                boolean countryEmpty = TextUtils.isEmpty(country);
                String subAddress = ((cityEmpty)?"":city+((stateEmpty)?"":","))
                        +((stateEmpty)?"":state+((countryEmpty)?"":","))
                        +((countryEmpty)?"":country+".");
                PassportLocation currentLocation = new PassportLocation();
                currentLocation.setLocationName(addressName);
                currentLocation.setSubLocationName(subAddress);
                currentLocation.setLatitude(locationHolder.getLatitude());
                currentLocation.setLongitude(locationHolder.getLongitude());
                return currentLocation;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /*
     * Fetch the saved featureLocation object.
     */
    public PassportLocation parseFeatureLocation()
    {
        try {
            String featureLocationJson = locationDataSource.getFeatureLocation();
            PassportLocation featureLocation = utility.getGson().fromJson(featureLocationJson, PassportLocation.class);
            if (featureLocation != null)
                return featureLocation;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /*
     * Fetch the saved passportLocationList.
     */
    public void parsePassportLocationList(ArrayList<String> locationArray)
    {
        arrayList.clear();
        PassportLocation featureLocation=getFeatureLocation();
        try
        {
            arrayList.clear();
            boolean isFound=false;
            for(String locationJson: locationArray)
            {
                PassportLocation passportLocation = utility.getGson().fromJson(locationJson,PassportLocation.class);
                if(passportLocation != null)
                {
                    passportLocation.setSelected(false);
                    try
                    {
                        if(featureLocation!=null)
                        {
                            if(passportLocation.getLongitude().equals(featureLocation.getLongitude())
                                    &&passportLocation.getLatitude().equals(featureLocation.getLatitude()))
                            {
                                passportLocation.setSelected(true);
                                isFound=true;
                            }
                        }
                    }catch (Exception e){}
                    arrayList.add(passportLocation);
                }
            }
            if(!isFound&&featureLocation!=null)
            {
                arrayList.add(0,featureLocation);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }





    /*
     * it will check for feature location first then saved current location.
     */
    public PassportLocation getFeatureLocation()
    {
        try {
            String locationJson = locationDataSource.getFeatureLocation();
            if(!TextUtils.isEmpty(locationJson))
            {
                PassportLocation featureLocation = utility.getGson().fromJson(locationJson, PassportLocation.class);
                if(!TextUtils.isEmpty(featureLocation.getLocationName()))
                {
                    return featureLocation;
                }
            }
        }catch (Exception e)
        {}
        return null;
    }
    /*
     * Save passportLocationList to sharedPreference.
     */
    public void savePassportList()
    {
        ArrayList<String> locationArray = getJsonLocationList();
        if(locationArray != null)
            locationDataSource.setPassportLocations(locationArray);
    }


    /*
     * Helper method help to convert the Object List to jsonString List.
     */
    private ArrayList<String> getJsonLocationList()
    {
        ArrayList<String> locationArray = new ArrayList<>();
        for (PassportLocation passportLocation : arrayList)
        {
            try
            {
                String locationJson = utility.getGson().toJson(passportLocation);
                locationArray.add(locationJson);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return locationArray;
    }


    /**
     * Save Feature Location to the SharedPreference.
     * @param featureLocation
     */
    public void saveFeatureLocation(PassportLocation featureLocation)
    {
        try
        {
            String featureLocationJson = utility.getGson().toJson(featureLocation);
            locationDataSource.setFeatureLocation(featureLocationJson);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /*
     * Clear the FeatureLocation Selection from arrayList.
     */
    public boolean removeFeatureLocation()
    {
        try{
            for(PassportLocation passportLocation : arrayList){
                passportLocation.setSelected(false);
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }


    /*
     * Fetch the saved currentLocation object.
     */
    public PassportLocation parseCurrentLocation()
    {
        try
        {
            String currentLocationJson = locationDataSource.getCurrentLocation();
            PassportLocation currentLocation = utility.getGson().fromJson(currentLocationJson, PassportLocation.class);
            if(!currentLocation.getLocationName().isEmpty())
                return currentLocation;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }




    /**
     * Save Current Location to the SharedPreference.
     * @param currentLocation
     */
    public void saveCurrentLocation(PassportLocation currentLocation)
    {
        try {
            String currentLocationJson = utility.getGson().toJson(currentLocation);
            locationDataSource.setCurrentLocation(currentLocationJson);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public PassportLocation getFeatureFromList()
    {
        for(int i = 0; i < getListSize(); i++)
        {
            PassportLocation location = arrayList.get(i);
            if(location.isSelected())
            {
                return location;
            }
        }
        return null;
    }


    public void unSelectAll()
    {
        for(int i = 0; i < getListSize(); i++)
        {
            PassportLocation location = arrayList.get(i);
            location.setSelected(false);
            passportAdapter.notifyDataSetChanged();
        }
    }
}
