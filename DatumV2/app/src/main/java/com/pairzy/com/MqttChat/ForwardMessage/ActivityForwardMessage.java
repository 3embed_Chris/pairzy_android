package com.pairzy.com.MqttChat.ForwardMessage;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.AppController;
import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.pairzy.com.MqttChat.Database.CouchDbController;
import com.pairzy.com.MqttChat.DownloadFile.FileUploadService;
import com.pairzy.com.MqttChat.DownloadFile.FileUtils;
import com.pairzy.com.MqttChat.DownloadFile.ServiceGenerator;
import com.pairzy.com.MqttChat.Utilities.ApiOnServer;
import com.pairzy.com.MqttChat.Utilities.CustomLinearLayoutManager;
import com.pairzy.com.MqttChat.Utilities.MqttEvents;
import com.pairzy.com.MqttChat.Utilities.RecyclerItemClickListener;
import com.pairzy.com.MqttChat.Utilities.TimestampSorter;
import com.pairzy.com.MqttChat.Utilities.Utilities;
import com.pairzy.com.R;
import com.pairzy.com.home.Matches.Chats.Model.ChatListItem;
import com.pairzy.com.util.FileUtil.Config;
import com.pairzy.com.util.TypeFaceManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by moda on 30/08/17.
 */

/**
 * @since 06/05/17.
 */
public class ActivityForwardMessage extends BaseDaggerActivity {


    @Inject
    TypeFaceManager typeFaceManager;

    private int count = 0;
    private RelativeLayout send_rl, root;

    private Bus bus = AppController.getBus();

    private ArrayList<Forward_ContactItem> mContactData = new ArrayList<>();


    private ArrayList<Forward_ContactItem> mCollapsedContactData = new ArrayList<>();


    private ArrayList<ChatListItem> mChatData = new ArrayList<>();


    private SearchView searchView;
    private TextView messageCall;//, noMatch;


    private TextView noContact, noChat, tvRecentChat;
    private Forward_ContactsAdapter mAdapter;

    private Forward_ChatsAdapter mAdapter2;

    private TextView selectedContacts, viewAllHide_tv;
    private int height = 0;

    private static final double MAX_VIDEO_SIZE = 26 * 1024 * 1024;
    private Intent intent;

    private static final int IMAGE_QUALITY = 50;//change it to higher level if want,but then slower image sending


    private ProgressDialog pDialog;


    private RelativeLayout chatsHeader, contactsHeader, viewAll_rl;


    private boolean hasAtleastOneChat = false, showingAllContacts = false;//, hasAtleastOneContact = false;
    private RecyclerView recyclerViewContacts;

    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forward_message);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.intent = getIntent();
        mContactData = new ArrayList<>();

        height = (int) (51 * getResources().getDisplayMetrics().density);

        /*
         * Initialization of the xml content.*/
        initializeXmlContent();

        bus.register(this);
    }

    /*
     * Initilaization of all the xml content.*/
    private void initializeXmlContent() {

        root = (RelativeLayout) findViewById(R.id.root);

        tvRecentChat = findViewById(R.id.tvChat);
        tvRecentChat.setTypeface(typeFaceManager.getCircularAirBook());

        chatsHeader = (RelativeLayout) findViewById(R.id.rl3);

        contactsHeader = (RelativeLayout) findViewById(R.id.rl2);

        viewAll_rl = (RelativeLayout) findViewById(R.id.rl6);
        viewAllHide_tv = (TextView) findViewById(R.id.viewAll_tv);


        recyclerViewContacts = (RecyclerView) findViewById(R.id.contacts_rv);
        recyclerViewContacts.setHasFixedSize(true);
        mAdapter = new Forward_ContactsAdapter(ActivityForwardMessage.this, mContactData,typeFaceManager);
        recyclerViewContacts.setItemAnimator(new DefaultItemAnimator());

        recyclerViewContacts.setLayoutManager(new CustomLinearLayoutManager(ActivityForwardMessage.this, LinearLayoutManager.VERTICAL, false));
        recyclerViewContacts.setAdapter(mAdapter);


        final RecyclerView recyclerViewChats = (RecyclerView) findViewById(R.id.chats_rv);
        recyclerViewChats.setHasFixedSize(true);
        mAdapter2 = new Forward_ChatsAdapter(ActivityForwardMessage.this, mChatData,typeFaceManager);
        recyclerViewChats.setItemAnimator(new DefaultItemAnimator());

        recyclerViewChats.setLayoutManager(new CustomLinearLayoutManager(ActivityForwardMessage.this, LinearLayoutManager.VERTICAL, false));
        recyclerViewChats.setAdapter(mAdapter2);


        ImageView sendButton = (ImageView) findViewById(R.id.send_iv);

        messageCall = (TextView) findViewById(R.id.userMessagechat);
        messageCall.setTypeface(typeFaceManager.getCircularAirBook());
        // noMatch = (TextView) findViewById(R.id.noMatch);

        noChat = (TextView) findViewById(R.id.noChat);
        noContact = (TextView) findViewById(R.id.noContact);
        selectedContacts = (TextView) findViewById(R.id.selectedContacts);

        final ImageView close = (ImageView) findViewById(R.id.close);

        send_rl = (RelativeLayout) findViewById(R.id.rl);
        addChats();
        addContacts();

        recyclerViewContacts.addOnItemTouchListener(new RecyclerItemClickListener(ActivityForwardMessage.this, recyclerViewContacts, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (position >= 0) {


                    Forward_ContactItem item = mAdapter.getList().get(position);
                    if (item.isSelected()) {


                        item.setSelected(false);


                        count--;
                    } else {


                        item.setSelected(true);


                        count++;


                    }


                    int i = findPosition(item.getContactUid());
                    if (i != -1) {
                        mContactData.set(i, item);
                        mAdapter.notifyItemChanged(i);
                    }

                    if (count < 1) {
                        send_rl.setVisibility(View.GONE);

                        if (hasAtleastOneChat) {
                            recyclerViewChats.setPadding(0, 0, 0, 0);
                        } else {
                            recyclerViewContacts.setPadding(0, 0, 0, 0);
                        }


                    } else {
                        if (hasAtleastOneChat) {
                            recyclerViewChats.setPadding(0, 0, 0, height);
                        } else {
                            recyclerViewContacts.setPadding(0, 0, 0, height);
                        }

                        send_rl.setVisibility(View.VISIBLE);


                    }


                    updateSelectedContactsText();
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {
            }
        }));


        recyclerViewChats.addOnItemTouchListener(new RecyclerItemClickListener(ActivityForwardMessage.this, recyclerViewChats,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (position >= 0) {


                            ChatListItem item = mAdapter2.getList().get(position);
                            if (item.isSelected()) {


                                item.setSelected(false);


                                count--;
                            } else {


                                item.setSelected(true);


                                count++;


                            }


                            int i = findPositionChat(item.getReceiverUid(), item.getSecretId());
                            if (i != -1) {
                                mChatData.set(i, item);
                                mAdapter2.notifyItemChanged(i);
                            }

                            if (count < 1) {
                                send_rl.setVisibility(View.GONE);
                                if (hasAtleastOneChat) {
                                    recyclerViewChats.setPadding(0, 0, 0, 0);
                                } else {
                                    recyclerViewContacts.setPadding(0, 0, 0, 0);
                                }


                            } else {

                                if (hasAtleastOneChat) {
                                    recyclerViewChats.setPadding(0, 0, 0, height);
                                } else {
                                    recyclerViewContacts.setPadding(0, 0, 0, height);
                                }

                                send_rl.setVisibility(View.VISIBLE);


                            }


                            updateSelectedContactsText();
                        }
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {
                    }
                }));


        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (intent != null) {


                    if (pDialog == null) {
                        pDialog = new ProgressDialog(ActivityForwardMessage.this);
                        pDialog.setMessage(getString(R.string.Forwarding));
                        pDialog.setCancelable(false);
                    }
                    pDialog.show();

                    Bundle extras = intent.getExtras();


                    int messageType = extras.getInt("messageType");


                    if (messageType == 1 || messageType == 2 || messageType == 5 || messageType == 7 || messageType == 9) {


                        prepareForUpload(messageType, -1, extras.getString("payload"));
                    } else if (messageType == 10) {

                        int replyType = Integer.parseInt(extras.getString("replyType"));

                        if (replyType == 1 || replyType == 2 || replyType == 5 || replyType == 7 || replyType == 9) {


                            prepareForUpload(10, replyType, extras.getString("payload"));
                        } else {

                            forwardMessageToMultipleContacts(10, null, null, null);

                        }

                    } else {


                        forwardMessageToMultipleContacts(messageType, null, null, null);
                    }


                }


            }
        });


        searchView = (SearchView) findViewById(R.id.search);

        searchView.setVisibility(View.VISIBLE);
        searchView.setIconified(true);
        searchView.setBackgroundColor(ContextCompat.getColor(ActivityForwardMessage.this, R.color.color_white));
        searchView.setIconifiedByDefault(true);
        searchView.clearFocus();
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    close.setVisibility(View.INVISIBLE);
                }
            }
        });


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                searchView.setIconifiedByDefault(true);
                searchView.setIconified(true);
                searchView.setQuery("", false);
                searchView.clearFocus();


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mAdapter.getFilter().filter(newText);


                mAdapter2.getFilter().filter(newText);

                return false;
            }
        });


        /*
         * Dont need it anymore in the present logic
         */

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //  noMatch.setVisibility(View.GONE);


                        noChat.setVisibility(View.GONE);
                        noContact.setVisibility(View.GONE);


                        if (mContactData.size() == 0 && mChatData.size() == 0) {


                            messageCall.setVisibility(View.VISIBLE);
                        }

                        close.setVisibility(View.VISIBLE);
                    }
                });


                return false;
            }
        });

        AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(com.google.android.material.R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        TextView title = (TextView) findViewById(R.id.title);


        title.setText(getString(R.string.Forward));
        title.setTypeface(typeFaceManager.getCircularAirBold());

        viewAll_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manipulateAdapterContents();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void minimizeCallScreen(JSONObject obj) {
        try {
            Intent intent = new Intent(ActivityForwardMessage.this, ChatMessageActivity.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra("receiverUid", obj.getString("receiverUid"));
            intent.putExtra("receiverName", obj.getString("receiverName"));
            intent.putExtra("documentId", obj.getString("documentId"));

            intent.putExtra("receiverImage", obj.getString("receiverImage"));
            intent.putExtra("colorCode", obj.getString("colorCode"));

            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void getMessage(JSONObject object) {
        try {
            if (object.getString("eventName").equals("callMinimized")) {

                minimizeCallScreen(object);
            } else if (object.getString("eventName").equals(MqttEvents.UserUpdates.value + "/" + AppController.getInstance().getUserId())) {


                switch (object.getInt("type")) {


                    case 1:


                        /*
                         * Status update by any of the contact
                         */



                        /*
                         * To update in the contacts list
                         */

                        final int pos = findContactPositionInList(object.getString("userId"));
                        if (pos != -1) {


                            Forward_ContactItem item = mContactData.get(pos);


                            item.setContactStatus(object.getString("socialStatus"));

                            mContactData.set(pos, item);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyItemChanged(pos);
                                }
                            });
                        }


                        break;

                    case 2:
                        /*
                         * Profile pic update
                         */


                        /*
                         * To update in the contacts list
                         */


                        final int pos1 = findContactPositionInList(object.getString("userId"));
                        if (pos1 != -1) {

                            /*
                             * Have to clear the glide cache
                             */
//                            Glide.get(getActivity()).clearDiskCache();
//
//                            Glide.get(getActivity()).clearMemory();
//                            Glide.get(getActivity()).getBitmapPool().clearMemory();
                            Forward_ContactItem item = mContactData.get(pos1);


                            item.setContactImage(object.getString("profilePic"));
                            mContactData.set(pos1, item);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyItemChanged(pos1);
                                }
                            });
                        }


                        break;
                    case 3:
                        /*
                         * Any of mine previous phone contact join
                         */

                        Forward_ContactItem item2 = new Forward_ContactItem();
                        item2.setContactImage("");
                        item2.setContactUid(object.getString("userId"));


                        item2.setContactStatus(getString(R.string.default_status));

                        item2.setContactIdentifier(object.getString("number"));


                        item2.setContactName(object.getString("name"));
                        final int position = findContactPositionInList(object.getString("userId"));
                        if (position == -1) {


                            mContactData.add(item2);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyItemInserted(mContactData.size() - 1);
                                }
                            });


                        } else {
                            mContactData.set(position, item2);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter.notifyItemChanged(position);
                                }
                            });

                        }
                        break;

                    case 4:
                        /*
                         * New contact added request send,for the response of the PUT contact api
                         */


                        switch (object.getInt("subtype")) {

                            case 0:

                                /*
                                 * Contact name or number changed but number still valid
                                 */


                                final int pos3 = findContactPositionInList(object.getString("contactUid"));


                                if (pos3 != -1) {


                                    Forward_ContactItem item = mContactData.get(pos3);


                                    item.setContactName(object.getString("contactName"));
                                    mContactData.set(pos3, item);

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mAdapter.notifyItemChanged(pos3);
                                        }
                                    });


                                }

                                break;
                            case 1:
                                /*
                                 * Number of active contact changed and new number not in contact
                                 */

                                final int pos4 = findContactPositionInList(object.getString("contactUid"));


                                if (pos4 != -1) {


                                    mContactData.remove(pos4);


                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mAdapter.notifyItemChanged(pos4);
                                        }
                                    });
                                }

                                break;

                            case 2:

                                /*
                                 * New contact added
                                 */


                                try {
                                    Forward_ContactItem item = new Forward_ContactItem();

                                    item.setContactImage(object.getString("contactPicUrl"));
                                    item.setContactName(object.getString("contactName"));
                                    item.setContactStatus(object.getString("contactStatus"));
                                    item.setContactIdentifier(object.getString("contactIdentifier"));
                                    item.setContactUid(object.getString("contactUid"));
                                    mContactData.add(item);


                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mAdapter.notifyItemInserted(mContactData.size() - 1);
                                        }
                                    });

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                break;
                        }


                        break;

                    case 5:
                        /*
                         * Contact deleted request send,for the response of the DELETE contact api
                         */


                        /*
                         * Number was in active contact
                         */
                        if (object.has("status") && object.getInt("status") == 0) {

                            final int pos2 = findContactPositionInList(object.getString("userId"));

                            if (pos2 != -1) {


                                mContactData.remove(pos2);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        mAdapter.notifyItemChanged(pos2);
                                        //  mAdapter.notifyDataSetChanged();
                                    }
                                });
                            }
                        }
                        break;


                    case 6: {

                        if (object.getBoolean("blocked")) {
                            removeFromContactsAndChats(object.getString("initiatorId"));

                        }
                        break;
                    }


                }


            } else if (object.getString("eventName").equals("ContactNameUpdated")) {

                final int pos = findContactPositionInList(object.getString("contactUid"));


                Forward_ContactItem item = mContactData.get(pos);


                item.setContactName(object.getString("contactName"));
                mContactData.set(pos, item);


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.notifyDataSetChanged();
                    }
                });

            } else if (object.getString("eventName").equals(MqttEvents.GroupChats.value + "/" + AppController.getInstance().getUserId())) {

                /*
                 * To remove the current groupchat from list of possible chats into which message can be forwarded
                 */

                if (!object.has("payload")) {
                    /*
                     * To only check if the current member has been removed from some group
                     */


                    if (object.getInt("type") == 2) {
                        /*
                         * Member removed
                         */


                        String chatId = object.getString("groupId");


                        for (int i = 0; i < mChatData.size(); i++) {


                            if (mChatData.get(i).getReceiverUid().equals(chatId)) {


                                if (mChatData.get(i).isSelected()) {

                                    mChatData.remove(i);
                                    final int k = i;


                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            if (k == 0) {


                                                mAdapter2.notifyDataSetChanged();
                                            } else {


                                                mAdapter2.notifyItemRemoved(k);

                                            }

                                        }
                                    });

                                    updateSelectedContactsText();


                                } else {


                                    mChatData.remove(i);
                                    final int k = i;


                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            if (k == 0) {


                                                mAdapter2.notifyDataSetChanged();
                                            } else {


                                                mAdapter2.notifyItemRemoved(k);

                                            }

                                        }
                                    });

                                }
                            }


                        }


                    }


                }
            }

        } catch (
                JSONException e)

        {
            e.printStackTrace();
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        try {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


        bus.unregister(this);
    }

    public void showNoSearchResults(final CharSequence constraint, boolean flag, final int type) {

        try {
            if (flag) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        noChat.setVisibility(View.GONE);
                        noContact.setVisibility(View.GONE);

                        if (mContactData.size() == 0 && mChatData.size() == 0) {
                            messageCall.setText(getString(R.string.forward_chats_contacts));

                            messageCall.setVisibility(View.VISIBLE);


                        } else {

                            messageCall.setVisibility(View.GONE);
                        }


//                        else if (type == 1) {
//                            if (mContactData.size() == 0) {
//
//                                messageCall.setText(getString(R.string.forward_contacts));
//                                messageCall.setVisibility(View.VISIBLE);
//
//
//                            } else {
//
//                                messageCall.setVisibility(View.GONE);
//                            }
//                        } else if (type == 2) {
//                            if (mChatData.size() == 0) {
//
//                                messageCall.setText(getString(R.string.forward_chats));
//                                messageCall.setVisibility(View.VISIBLE);
//                            } else {
//
//                                messageCall.setVisibility(View.GONE);
//                            }
//                        }
                    }
                });
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (type == 1) {
                            if (mAdapter.getList().size() == 0) {
                                if (noContact != null) {

                                    noContact.setText(getString(R.string.noMatch) + " " + constraint);
                                    noContact.setVisibility(View.VISIBLE);


                                    messageCall.setVisibility(View.GONE);

                                }
                            } else {
                                noContact.setVisibility(View.GONE);


                            }
                        } else {


                            if (mAdapter2.getList().size() == 0) {
                                if (noChat != null) {

                                    noChat.setText(getString(R.string.noMatch) + " " + constraint);
                                    noChat.setVisibility(View.VISIBLE);


                                    messageCall.setVisibility(View.GONE);

                                }
                            } else {
                                noChat.setVisibility(View.GONE);


                            }


                        }
                    }
                });
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private void addContacts() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mContactData.clear();
                mAdapter.notifyDataSetChanged();
            }
        });
        //ArrayList<Map<String, Object>> contacts = AppController.getInstance().getDbController().loadContacts(AppController.getInstance().getContactsDocId());
//        Forward_ContactItem contact;
//        Map<String, Object> contactIthPosition;
//        if (mContactData != null) {
//            String contactUid;
//            boolean hasChatWithContact;
//            for (int i = 0; i < contacts.size(); i++) {
//                contactIthPosition = contacts.get(i);
///*
// * Not showing current user in list of contacts to whom message can be forwarded
// */
//                hasChatWithContact = false;
//                contactUid = (String) contactIthPosition.get("contactUid");
//                for (int j = 0; j < mChatData.size(); j++) {
//                    if (mChatData.get(j).getReceiverUid().equals(contactUid)) {
//                        hasChatWithContact = true;
//                        break;
//
//                    }
//                }
//                if (!hasChatWithContact) {
//                    if (!contactUid.equals(AppController.getInstance().getActiveReceiverId())) {
//                        if (!contactIthPosition.containsKey("blocked")) {
//                            contact = new Forward_ContactItem();
//                            contact.setSelected(false);
//                            contact.setContactIdentifier((String) contactIthPosition.get("contactIdentifier"));
//
//                            contact.setContactImage((String) contactIthPosition.get("contactPicUrl"));
//                            contact.setContactName((String) contactIthPosition.get("contactName"));
//                            contact.setContactStatus((String) contactIthPosition.get("contactStatus"));
//                            contact.setContactUid(contactUid);
//                            mContactData.add(contact);
//                        }
//                    }
//                }
//
//            }
//
//
////            runOnUiThread(new Runnable() {
////                @Override
////                public void run() {
////                    mAdapter.notifyDataSetChanged();
////                }
////            });
//
//
//            if (mContactData.size() == 0) {
//
//                if (!hasAtleastOneChat) {
//                    messageCall.setVisibility(View.VISIBLE);
//                }
//                contactsHeader.setVisibility(View.GONE);
//
//            } else if (mContactData.size() > 0) {
//                messageCall.setVisibility(View.GONE);
//                Collections.sort(mContactData, new SortContactsToForward());
//
//                int size;
//                if (mContactData.size() > 2) {
//                    viewAllHide_tv.setText(getString(R.string.ViewAll));
//                    size = 2;
//                } else {
//                    size = mContactData.size();
//                    viewAll_rl.setVisibility(View.GONE);
//                }
//
//                for (int i = 0; i < size; i++) {
//                    try {
//                        mCollapsedContactData.add(mContactData.get(i));
//                    } catch (IndexOutOfBoundsException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//
//                mAdapter = new Forward_ContactsAdapter(ActivityForwardMessage.this, mCollapsedContactData);
//                recyclerViewContacts.setAdapter(mAdapter);
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//
//
//                        mAdapter.notifyDataSetChanged();
//
//                    }
//                });
//
//            }
//        }
    }

    private int findContactPositionInList(String contactUid) {
        int pos = -1;

        for (int i = 0; i < mContactData.size(); i++) {


            if (mContactData.get(i).getContactUid().equals(contactUid)) {

                return i;
            }
        }

        return pos;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
//        mContactData.clear();

        this.intent = intent;
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//
//                mAdapter.notifyDataSetChanged();
//
//            }
//        });

        addChats();
        addContacts();
    }

    private int findPosition(String contactUid) {


        for (int i = 0; i < mContactData.size(); i++) {


            if (mContactData.get(i).getContactUid().equals(contactUid)) {

                return i;
            }


        }

        return -1;
    }


    private int findPositionChat(String contactUid, String secretId) {


        for (int i = 0; i < mChatData.size(); i++) {


            if (mChatData.get(i).getReceiverUid().equals(contactUid) && mChatData.get(i).getSecretId().equals(secretId)) {

                return i;
            }


        }

        return -1;
    }

    private void updateSelectedContactsText() {
        if (count > 0) {
            String currentlySelectedContacts = "";
            for (int i = 0; i < mContactData.size(); i++) {

                if (mContactData.get(i).isSelected()) {

                    currentlySelectedContacts = currentlySelectedContacts + "," + mContactData.get(i).getContactName();
                }
            }


            for (int i = 0; i < mChatData.size(); i++) {

                if (mChatData.get(i).isSelected()) {

                    currentlySelectedContacts = currentlySelectedContacts + "," + mChatData.get(i).getReceiverName();
                }
            }


            selectedContacts.setText(currentlySelectedContacts.substring(1));
        } else {

            selectedContacts.setText("");
        }
    }


    private void prepareForUpload(int messageType, int replyType, String filePath) {


        switch (messageType) {


            case 1: {
                Uri uriImage = null;
                String idImage = null;
                Bitmap bm = null;


                try {


                    final BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(filePath, options);


                    int height = options.outHeight;
                    int width = options.outWidth;

                    float density = getResources().getDisplayMetrics().density;
                    int reqHeight;


                    if (width != 0) {


                        reqHeight = (int) ((150 * density) * (height / width));


                        bm = decodeSampledBitmapFromResource(filePath, (int) (150 * density), reqHeight);


                        ByteArrayOutputStream baos = new ByteArrayOutputStream();


                        if (bm != null) {

                            bm.compress(Bitmap.CompressFormat.JPEG, IMAGE_QUALITY, baos);
                            //bm = null;
                            byte[] b = baos.toByteArray();
                            try {
                                baos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            baos = null;
//                b = compress(b);


                            idImage = new Utilities().gmtToEpoch(Utilities.tsInGmt());

                            File f = convertByteArrayToFile(b, idImage, ".jpg");
                            b = null;

                            uriImage = Uri.fromFile(f);
                            f = null;


                        } else {


                            if (root != null) {

                                Snackbar snackbar = Snackbar.make(root, R.string.string_482, Snackbar.LENGTH_SHORT);


                                snackbar.show();
                                View view = snackbar.getView();
                                TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                            }


                        }


                    } else {


                        if (root != null) {

                            Snackbar snackbar = Snackbar.make(root, R.string.string_482, Snackbar.LENGTH_SHORT);


                            snackbar.show();
                            View view = snackbar.getView();
                            TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                        }
                    }


                } catch (OutOfMemoryError e) {
                    e.printStackTrace();


                    if (root != null) {

                        Snackbar snackbar = Snackbar.make(root, R.string.string_491, Snackbar.LENGTH_SHORT);


                        snackbar.show();
                        View view = snackbar.getView();
                        TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                    }


                }


                if (uriImage != null) {

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    bm.compress(Bitmap.CompressFormat.JPEG, 1, baos);


                    bm = null;
                    byte[] b = baos.toByteArray();

                    try {
                        baos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    baos = null;


//                    forwardMessageToMultipleContacts(1, -1, idImage, Base64.encodeToString(b, Base64.DEFAULT).trim(), uriImage);


                    forwardMessageToMultipleContacts(1, idImage, Base64.encodeToString(b, Base64.DEFAULT).trim(), uriImage);


                    uriImage = null;
                    b = null;
                    bm = null;

                }

                break;
            }
            case 2: {
                Uri uri = null;
                String id = null;
                try {


                    File video = new File(filePath);

                    if (video.length() <= (MAX_VIDEO_SIZE)) {

                        try {


                            byte[] b = convertFileToByteArray(video);
                            video = null;
                            //        b = compress(b);


                            id = new Utilities().gmtToEpoch(Utilities.tsInGmt());

                            File f = convertByteArrayToFile(b, id, ".mp4");
                            b = null;

                            uri = Uri.fromFile(f);
                            f = null;

                            b = null;


                        } catch (OutOfMemoryError e) {

                            if (root != null) {

                                Snackbar snackbar = Snackbar.make(root, R.string.string_511, Snackbar.LENGTH_SHORT);


                                snackbar.show();
                                View view = snackbar.getView();
                                TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                            }


                        }

                        if (uri != null) {


                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            Bitmap bm2 = ThumbnailUtils.createVideoThumbnail(filePath,
                                    MediaStore.Images.Thumbnails.MINI_KIND);

                            if (bm2 != null) {

                                bm2.compress(Bitmap.CompressFormat.JPEG, 1, baos);
                                bm2 = null;
                                byte[] b = baos.toByteArray();
                                try {
                                    baos.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                baos = null;


//                                forwardMessageToMultipleContacts(2, -1, id, Base64.encodeToString(b, Base64.DEFAULT).trim(), uri);


                                forwardMessageToMultipleContacts(2, id, Base64.encodeToString(b, Base64.DEFAULT).trim(), uri);


                                uri = null;
                                b = null;

                            }
                        } else {


                            if (root != null) {

                                Snackbar snackbar = Snackbar.make(root, getString(R.string.string_674)
                                        , Snackbar.LENGTH_SHORT);


                                snackbar.show();
                                View view = snackbar.getView();
                                TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                            }


                        }

                    } else {


                        if (root != null) {

                            Snackbar snackbar = Snackbar.make(root, getString(R.string.string_52) + " " + MAX_VIDEO_SIZE / (1024 * 1024) + " " + getString(R.string.string_56), Snackbar.LENGTH_SHORT);


                            snackbar.show();
                            View view = snackbar.getView();
                            TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                        }


                    }


                } catch (NullPointerException e) {


                    if (root != null) {

                        Snackbar snackbar = Snackbar.make(root, R.string.string_674, Snackbar.LENGTH_SHORT);


                        snackbar.show();
                        View view = snackbar.getView();
                        TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                    }

                }


                break;

            }
            case 5: {

                String idAudio = null;
                Uri uriAudio = null;
                try {


                    File audio = new File(filePath);


                    if (audio.length() <= (MAX_VIDEO_SIZE)) {
                        try {


                            byte[] b = convertFileToByteArray(audio);
                            audio = null;
                            // b = compress(b);


                            idAudio = new Utilities().gmtToEpoch(Utilities.tsInGmt());

                            File f = convertByteArrayToFile(b, idAudio, ".mp3");
                            b = null;

                            uriAudio = Uri.fromFile(f);
                            f = null;

                            b = null;


                        } catch (OutOfMemoryError e) {
                            e.printStackTrace();
                            if (root != null) {

                                Snackbar snackbar = Snackbar.make(root, R.string.string_531, Snackbar.LENGTH_SHORT);


                                snackbar.show();
                                View view = snackbar.getView();
                                TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                            }


                        }

                        if (uriAudio != null) {

//                            forwardMessageToMultipleContacts(2, -1, idAudio, null, uriAudio);

                            forwardMessageToMultipleContacts(5, idAudio, null, uriAudio);

                            uriAudio = null;

                        }
                    } else {


                        if (root != null) {

                            Snackbar snackbar = Snackbar.make(root, getString(R.string.string_54) + " " + MAX_VIDEO_SIZE / (1024 * 1024) + " " + getString(R.string.string_56), Snackbar.LENGTH_SHORT);


                            snackbar.show();
                            View view = snackbar.getView();
                            TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                        }

                    }


                } catch (NullPointerException e) {


                    if (root != null) {

                        Snackbar snackbar = Snackbar.make(root, R.string.string_667, Snackbar.LENGTH_SHORT);


                        snackbar.show();
                        View view = snackbar.getView();
                        TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                    }

                }
                break;
            }

            case 7: {

                Uri uriDoodle = null;
                String idDoodle = null;
                Bitmap bmDoodle = null;


                try {


                    final BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(filePath, options);


                    int height = options.outHeight;
                    int width = options.outWidth;

                    float density = getResources().getDisplayMetrics().density;
                    int reqHeight;


                    if (width != 0) {


                        reqHeight = (int) ((150 * density) * (height / width));


                        bmDoodle = decodeSampledBitmapFromResource(filePath, (int) (150 * density), reqHeight);


                        ByteArrayOutputStream baos = new ByteArrayOutputStream();


                        if (bmDoodle != null) {

                            bmDoodle.compress(Bitmap.CompressFormat.JPEG, IMAGE_QUALITY, baos);
                            //bm = null;
                            byte[] b = baos.toByteArray();
                            try {
                                baos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            baos = null;
//                b = compress(b);


                            idDoodle = new Utilities().gmtToEpoch(Utilities.tsInGmt());

                            File f = convertByteArrayToFile(b, idDoodle, ".jpg");
                            b = null;

                            uriDoodle = Uri.fromFile(f);
                            f = null;


                        } else {


                            if (root != null) {

                                Snackbar snackbar = Snackbar.make(root, R.string.string_481, Snackbar.LENGTH_SHORT);


                                snackbar.show();
                                View view = snackbar.getView();
                                TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                            }


                        }


                    } else {


                        if (root != null) {

                            Snackbar snackbar = Snackbar.make(root, R.string.string_481, Snackbar.LENGTH_SHORT);


                            snackbar.show();
                            View view = snackbar.getView();
                            TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                        }
                    }


                } catch (OutOfMemoryError e) {
                    e.printStackTrace();


                    if (root != null) {

                        Snackbar snackbar = Snackbar.make(root, R.string.string_492, Snackbar.LENGTH_SHORT);


                        snackbar.show();
                        View view = snackbar.getView();
                        TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                    }


                }


                if (uriDoodle != null) {

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    bmDoodle.compress(Bitmap.CompressFormat.JPEG, 1, baos);


                    bmDoodle = null;
                    byte[] b = baos.toByteArray();

                    try {
                        baos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    baos = null;


//                    forwardMessageToMultipleContacts(7, -1, idDoodle, Base64.encodeToString(b, Base64.DEFAULT).trim(), uriDoodle);


                    forwardMessageToMultipleContacts(7, idDoodle, Base64.encodeToString(b, Base64.DEFAULT).trim(), uriDoodle);

                    uriDoodle = null;
                    b = null;
                    bmDoodle = null;

                }


                break;
            }
            case 9:

            {
//                forwardMessageToMultipleContacts(9, -1, null, null, Uri.fromFile(new File(filePath)));

                forwardMessageToMultipleContacts(9, null, null, Uri.fromFile(new File(filePath)));
                break;
            }


            case 10: {


                switch (replyType) {

                    case 1: {
                        Uri uriImage = null;
                        String idImage = null;
                        Bitmap bm = null;


                        try {


                            final BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            BitmapFactory.decodeFile(filePath, options);


                            int height = options.outHeight;
                            int width = options.outWidth;

                            float density = getResources().getDisplayMetrics().density;
                            int reqHeight;


                            if (width != 0) {


                                reqHeight = (int) ((150 * density) * (height / width));


                                bm = decodeSampledBitmapFromResource(filePath, (int) (150 * density), reqHeight);


                                ByteArrayOutputStream baos = new ByteArrayOutputStream();


                                if (bm != null) {

                                    bm.compress(Bitmap.CompressFormat.JPEG, IMAGE_QUALITY, baos);
                                    //bm = null;
                                    byte[] b = baos.toByteArray();
                                    try {
                                        baos.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    baos = null;
//                b = compress(b);


                                    idImage = new Utilities().gmtToEpoch(Utilities.tsInGmt());

                                    File f = convertByteArrayToFile(b, idImage, ".jpg");
                                    b = null;

                                    uriImage = Uri.fromFile(f);
                                    f = null;


                                } else {


                                    if (root != null) {

                                        Snackbar snackbar = Snackbar.make(root, R.string.string_481, Snackbar.LENGTH_SHORT);


                                        snackbar.show();
                                        View view = snackbar.getView();
                                        TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                                    }


                                }


                            } else {


                                if (root != null) {

                                    Snackbar snackbar = Snackbar.make(root, R.string.string_481, Snackbar.LENGTH_SHORT);


                                    snackbar.show();
                                    View view = snackbar.getView();
                                    TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                                }
                            }


                        } catch (OutOfMemoryError e) {
                            e.printStackTrace();


                            if (root != null) {

                                Snackbar snackbar = Snackbar.make(root, R.string.string_491, Snackbar.LENGTH_SHORT);


                                snackbar.show();
                                View view = snackbar.getView();
                                TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                            }


                        }


                        if (uriImage != null) {

                            ByteArrayOutputStream baos = new ByteArrayOutputStream();

                            bm.compress(Bitmap.CompressFormat.JPEG, 1, baos);


                            bm = null;
                            byte[] b = baos.toByteArray();

                            try {
                                baos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            baos = null;


//                            forwardMessageToMultipleContacts(10, 1, idImage, Base64.encodeToString(b, Base64.DEFAULT).trim(), uriImage);


                            forwardMessageToMultipleContacts(10, idImage, Base64.encodeToString(b, Base64.DEFAULT).trim(), uriImage);

                            uriImage = null;
                            b = null;
                            bm = null;

                        }

                        break;
                    }
                    case 2: {
                        Uri uri = null;
                        String id = null;
                        try {


                            File video = new File(filePath);

                            if (video.length() <= (MAX_VIDEO_SIZE)) {

                                try {


                                    byte[] b = convertFileToByteArray(video);
                                    video = null;
                                    //        b = compress(b);


                                    id = new Utilities().gmtToEpoch(Utilities.tsInGmt());

                                    File f = convertByteArrayToFile(b, id, ".mp4");
                                    b = null;

                                    uri = Uri.fromFile(f);
                                    f = null;

                                    b = null;


                                } catch (OutOfMemoryError e) {

                                    if (root != null) {

                                        Snackbar snackbar = Snackbar.make(root, R.string.string_511, Snackbar.LENGTH_SHORT);


                                        snackbar.show();
                                        View view = snackbar.getView();
                                        TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                                    }


                                }

                                if (uri != null) {


                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                    Bitmap bm2 = ThumbnailUtils.createVideoThumbnail(filePath,
                                            MediaStore.Images.Thumbnails.MINI_KIND);

                                    if (bm2 != null) {

                                        bm2.compress(Bitmap.CompressFormat.JPEG, 1, baos);
                                        bm2 = null;
                                        byte[] b = baos.toByteArray();
                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


//                                        forwardMessageToMultipleContacts(10, 2, id, Base64.encodeToString(b, Base64.DEFAULT).trim(), uri);
                                        forwardMessageToMultipleContacts(10, id, Base64.encodeToString(b, Base64.DEFAULT).trim(), uri);

                                        uri = null;
                                        b = null;

                                    } else {


                                        if (root != null) {

                                            Snackbar snackbar = Snackbar.make(root, getString(R.string.string_674)

                                                    , Snackbar.LENGTH_SHORT);


                                            snackbar.show();
                                            View view = snackbar.getView();
                                            TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                                        }


                                    }
                                } else {


                                    if (root != null) {

                                        Snackbar snackbar = Snackbar.make(root, getString(R.string.string_674)

                                                , Snackbar.LENGTH_SHORT);


                                        snackbar.show();
                                        View view = snackbar.getView();
                                        TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                                    }


                                }

                            } else {


                                if (root != null) {

                                    Snackbar snackbar = Snackbar.make(root, getString(R.string.string_52) + " " + MAX_VIDEO_SIZE / (1024 * 1024) + " " + getString(R.string.string_56), Snackbar.LENGTH_SHORT);


                                    snackbar.show();
                                    View view = snackbar.getView();
                                    TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                                }


                            }


                        } catch (NullPointerException e) {


                            if (root != null) {

                                Snackbar snackbar = Snackbar.make(root, R.string.string_674, Snackbar.LENGTH_SHORT);


                                snackbar.show();
                                View view = snackbar.getView();
                                TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                            }

                        }


                        break;

                    }
                    case 5: {

                        String idAudio = null;
                        Uri uriAudio = null;
                        try {


                            File audio = new File(filePath);


                            if (audio.length() <= (MAX_VIDEO_SIZE)) {
                                try {


                                    byte[] b = convertFileToByteArray(audio);
                                    audio = null;
                                    // b = compress(b);


                                    idAudio = new Utilities().gmtToEpoch(Utilities.tsInGmt());

                                    File f = convertByteArrayToFile(b, idAudio, ".mp3");
                                    b = null;

                                    uriAudio = Uri.fromFile(f);
                                    f = null;

                                    b = null;


                                } catch (OutOfMemoryError e) {
                                    e.printStackTrace();
                                    if (root != null) {

                                        Snackbar snackbar = Snackbar.make(root, R.string.string_531, Snackbar.LENGTH_SHORT);


                                        snackbar.show();
                                        View view = snackbar.getView();
                                        TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                                    }


                                }

                                if (uriAudio != null) {

//                                    forwardMessageToMultipleContacts(10, 5, idAudio, null, uriAudio);

                                    forwardMessageToMultipleContacts(10, idAudio, null, uriAudio);
                                    uriAudio = null;

                                }
                            } else {


                                if (root != null) {

                                    Snackbar snackbar = Snackbar.make(root, getString(R.string.string_54) + " " + MAX_VIDEO_SIZE / (1024 * 1024) + " " + getString(R.string.string_56), Snackbar.LENGTH_SHORT);


                                    snackbar.show();
                                    View view = snackbar.getView();
                                    TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                                }

                            }


                        } catch (NullPointerException e) {


                            if (root != null) {

                                Snackbar snackbar = Snackbar.make(root, R.string.string_667, Snackbar.LENGTH_SHORT);


                                snackbar.show();
                                View view = snackbar.getView();
                                TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                            }

                        }
                        break;
                    }

                    case 7: {

                        Uri uriDoodle = null;
                        String idDoodle = null;
                        Bitmap bmDoodle = null;


                        try {


                            final BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            BitmapFactory.decodeFile(filePath, options);


                            int height = options.outHeight;
                            int width = options.outWidth;

                            float density = getResources().getDisplayMetrics().density;
                            int reqHeight;


                            if (width != 0) {


                                reqHeight = (int) ((150 * density) * (height / width));


                                bmDoodle = decodeSampledBitmapFromResource(filePath, (int) (150 * density), reqHeight);


                                ByteArrayOutputStream baos = new ByteArrayOutputStream();


                                if (bmDoodle != null) {

                                    bmDoodle.compress(Bitmap.CompressFormat.JPEG, IMAGE_QUALITY, baos);
                                    //bm = null;
                                    byte[] b = baos.toByteArray();
                                    try {
                                        baos.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    baos = null;
//                b = compress(b);


                                    idDoodle = new Utilities().gmtToEpoch(Utilities.tsInGmt());

                                    File f = convertByteArrayToFile(b, idDoodle, ".jpg");
                                    b = null;

                                    uriDoodle = Uri.fromFile(f);
                                    f = null;


                                } else {


                                    if (root != null) {

                                        Snackbar snackbar = Snackbar.make(root, R.string.string_481, Snackbar.LENGTH_SHORT);


                                        snackbar.show();
                                        View view = snackbar.getView();
                                        TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                                    }


                                }


                            } else {


                                if (root != null) {

                                    Snackbar snackbar = Snackbar.make(root, R.string.string_481, Snackbar.LENGTH_SHORT);


                                    snackbar.show();
                                    View view = snackbar.getView();
                                    TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                                }
                            }


                        } catch (OutOfMemoryError e) {
                            e.printStackTrace();


                            if (root != null) {

                                Snackbar snackbar = Snackbar.make(root, R.string.string_492, Snackbar.LENGTH_SHORT);


                                snackbar.show();
                                View view = snackbar.getView();
                                TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                            }


                        }


                        if (uriDoodle != null) {

                            ByteArrayOutputStream baos = new ByteArrayOutputStream();

                            bmDoodle.compress(Bitmap.CompressFormat.JPEG, 1, baos);


                            bmDoodle = null;
                            byte[] b = baos.toByteArray();

                            try {
                                baos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            baos = null;


//                            forwardMessageToMultipleContacts(10, 7, idDoodle, Base64.encodeToString(b, Base64.DEFAULT).trim(), uriDoodle);


                            forwardMessageToMultipleContacts(10, idDoodle, Base64.encodeToString(b, Base64.DEFAULT).trim(), uriDoodle);

                            uriDoodle = null;
                            b = null;
                            bmDoodle = null;

                        }


                        break;
                    }
                    case 9:

                    {
//                        forwardMessageToMultipleContacts(10, 9, null, null, Uri.fromFile(new File(filePath)));

                        forwardMessageToMultipleContacts(10, null, null, Uri.fromFile(new File(filePath)));
                        break;
                    }
                }

            }


        }


    }


    @SuppressWarnings("TryWithIdenticalCatches")

//    private void forwardMessageToMultipleContacts(int messageType, int repType, String id, String thumbnail, Uri uri) {
//
    private void forwardMessageToMultipleContacts(int messageType, String id, String thumbnail, Uri uri) {


        Bundle extras = intent.getExtras();


        String payload = extras.getString("payload").trim();
        String tsForServer, tsForServerEpoch;

        if (id == null) {
            tsForServer = Utilities.tsInGmt();
            tsForServerEpoch = new Utilities().gmtToEpoch(tsForServer);
        } else {
            tsForServerEpoch = id;
            tsForServer = Utilities.epochtoGmt(id);

        }


        JSONObject obj;


        obj = new JSONObject();


        if (!intent.getExtras().getBoolean("toUpload")) {
            /*
             *
             * normal text message so payload field is set as well
             *
             * */

            switch (messageType) {


                case 0: {
                    /*
                     * Text message
                     */


                    try {


                        obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                        obj.put("from", AppController.getInstance().getUserId());

                        obj.put("payload", Base64.encodeToString(payload.getBytes("UTF-8"), Base64.DEFAULT).trim());


                        obj.put("timestamp", tsForServerEpoch);

                        obj.put("id", tsForServerEpoch);
                        obj.put("type", "0");

                        obj.put("name", AppController.getInstance().getUserName());


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {

                        e.printStackTrace();
                    }




                    /*
                     *
                     * location message so payload field is set as well
                     *
                     * */


                    break;
                }
                case 3: {

                    /*
                     * Location
                     */


                    try {


                        obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                        obj.put("from", AppController.getInstance().getUserId());

                        obj.put("payload", Base64.encodeToString(payload.getBytes("UTF-8"), Base64.DEFAULT).trim());


                        obj.put("timestamp", tsForServerEpoch);
                        obj.put("id", tsForServerEpoch);
                        obj.put("type", "3");

                        obj.put("name", AppController.getInstance().getUserName());


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {

                        e.printStackTrace();
                    }




                    /*
                     *
                     * contact message so payload field is set as well
                     *
                     * */


                    break;
                }
                case 4: {

                    /*
                     * Contacts
                     */
                    try {


                        obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                        obj.put("from", AppController.getInstance().getUserId());

                        obj.put("payload", Base64.encodeToString(payload.getBytes("UTF-8"), Base64.DEFAULT).trim());


                        obj.put("timestamp", tsForServerEpoch);
                        obj.put("id", tsForServerEpoch);
                        obj.put("type", "4");
                        obj.put("name", AppController.getInstance().getUserName());


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {

                        e.printStackTrace();
                    }


                    break;
                }
                case 8: {


                    /*
                     * Gifs
                     */

                    try {





                        /*
                         * Has removed the thumbnail key for now,which was added for the ios
                         */
                        obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                        obj.put("from", AppController.getInstance().getUserId());

                        obj.put("payload", Base64.encodeToString(payload.getBytes("UTF-8"), Base64.DEFAULT).trim());


                        obj.put("timestamp", tsForServerEpoch);
                        obj.put("id", tsForServerEpoch);
                        obj.put("type", "8");

                        obj.put("name", AppController.getInstance().getUserName());


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {

                        e.printStackTrace();
                    }


                    break;
                }
                case 6:

                {


                    /*
                     * Stickers
                     */

                    try {


                        obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                        obj.put("from", AppController.getInstance().getUserId());

                        obj.put("payload", Base64.encodeToString(payload.getBytes("UTF-8"), Base64.DEFAULT).trim());


                        obj.put("timestamp", tsForServerEpoch);
                        obj.put("id", tsForServerEpoch);
                        obj.put("type", "6");

                        obj.put("name", AppController.getInstance().getUserName());


                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {

                        e.printStackTrace();
                    }


                    break;
                }


                case 10: {


                    switch (Integer.parseInt(intent.getExtras().getString("replyType")))

                    {
                        case 0: {
                            /*
                             * Text message
                             */


                            try {


                                obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                                obj.put("from", AppController.getInstance().getUserId());

                                obj.put("payload", Base64.encodeToString(payload.getBytes("UTF-8"), Base64.DEFAULT).trim());


                                obj.put("timestamp", tsForServerEpoch);

                                obj.put("id", tsForServerEpoch);

                                obj.put("type", "10");
                                obj.put("replyType", "0");
                                obj.put("name", AppController.getInstance().getUserName());


                                /*
                                 * For the reply message specific content
                                 */


                                Bundle extras2 = intent.getExtras();

                                obj.put("previousFrom", extras2.getString("previousFrom"));


                                obj.put("previousType", extras2.getString("previousType"));


                                switch (Integer.parseInt(extras2.getString("previousType"))) {

                                    case 1: {


                                        /*
                                         *Image
                                         */
                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());


                                        break;

                                    }
                                    case 2: {

                                        /*
                                         * Video
                                         */
                                        Bitmap bm = ThumbnailUtils.createVideoThumbnail(extras2.getString("previousPayload"),
                                                MediaStore.Images.Thumbnails.MINI_KIND);


                                        bm = Bitmap.createScaledBitmap(bm, 180, 180, false);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;

                                    }
                                    case 7: {
                                        /*
                                         * Doodle
                                         */


                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;
                                    }
                                    default: {

                                        obj.put("previousPayload", extras2.getString("previousPayload"));


                                    }


                                }


                                obj.put("previousId", extras2.getString("previousId"));
                                obj.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));
                                if (extras2.getString("previousType").equals("9")) {

                                    obj.put("previousFileType", extras2.getString("previousFileType"));


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {

                                e.printStackTrace();
                            }




                            /*
                             *
                             * location message so payload field is set as well
                             *
                             * */


                            break;
                        }
                        case 3: {

                            /*
                             * Location
                             */


                            try {


                                obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                                obj.put("from", AppController.getInstance().getUserId());

                                obj.put("payload", Base64.encodeToString(payload.getBytes("UTF-8"), Base64.DEFAULT).trim());


                                obj.put("timestamp", tsForServerEpoch);
                                obj.put("id", tsForServerEpoch);
                                obj.put("type", "10");
                                obj.put("replyType", "3");
                                obj.put("name", AppController.getInstance().getUserName());


                                Bundle extras2 = intent.getExtras();

                                obj.put("previousFrom", extras2.getString("previousFrom"));

                                switch (Integer.parseInt(extras2.getString("previousType"))) {

                                    case 1: {


                                        /*
                                         *Image
                                         */
                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());


                                        break;

                                    }
                                    case 2: {

                                        /*
                                         * Video
                                         */
                                        Bitmap bm = ThumbnailUtils.createVideoThumbnail(extras2.getString("previousPayload"),
                                                MediaStore.Images.Thumbnails.MINI_KIND);


                                        bm = Bitmap.createScaledBitmap(bm, 180, 180, false);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;

                                    }
                                    case 7: {
                                        /*
                                         * Doodle
                                         */


                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;
                                    }
                                    default: {

                                        obj.put("previousPayload", extras2.getString("previousPayload"));


                                    }


                                }


                                obj.put("previousType", extras2.getString("previousType"));

                                obj.put("previousId", extras2.getString("previousId"));
                                obj.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));
                                if (extras2.getString("previousType").equals("9")) {

                                    obj.put("previousFileType", extras2.getString("previousFileType"));


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {

                                e.printStackTrace();
                            }




                            /*
                             *
                             * contact message so payload field is set as well
                             *
                             * */


                            break;
                        }
                        case 4: {

                            /*
                             * Contacts
                             */
                            try {


                                obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                                obj.put("from", AppController.getInstance().getUserId());

                                obj.put("payload", Base64.encodeToString(payload.getBytes("UTF-8"), Base64.DEFAULT).trim());


                                obj.put("timestamp", tsForServerEpoch);
                                obj.put("id", tsForServerEpoch);
                                obj.put("type", "10");
                                obj.put("replyType", "4");
                                obj.put("name", AppController.getInstance().getUserName());
                                Bundle extras2 = intent.getExtras();

                                obj.put("previousFrom", extras2.getString("previousFrom"));

                                switch (Integer.parseInt(extras2.getString("previousType"))) {

                                    case 1: {


                                        /*
                                         *Image
                                         */
                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());


                                        break;

                                    }
                                    case 2: {

                                        /*
                                         * Video
                                         */
                                        Bitmap bm = ThumbnailUtils.createVideoThumbnail(extras2.getString("previousPayload"),
                                                MediaStore.Images.Thumbnails.MINI_KIND);


                                        bm = Bitmap.createScaledBitmap(bm, 180, 180, false);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;

                                    }
                                    case 7: {
                                        /*
                                         * Doodle
                                         */


                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;
                                    }
                                    default: {

                                        obj.put("previousPayload", extras2.getString("previousPayload"));


                                    }


                                }


                                obj.put("previousType", extras2.getString("previousType"));

                                obj.put("previousId", extras2.getString("previousId"));
                                obj.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));
                                if (extras2.getString("previousType").equals("9")) {

                                    obj.put("previousFileType", extras2.getString("previousFileType"));


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {

                                e.printStackTrace();
                            }


                            break;
                        }
                        case 8: {


                            /*
                             * Gifs
                             */

                            try {





                                /*
                                 * Has removed the thumbnail key for now,which was added for the ios
                                 */
                                obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                                obj.put("from", AppController.getInstance().getUserId());

                                obj.put("payload", Base64.encodeToString(payload.getBytes("UTF-8"), Base64.DEFAULT).trim());


                                obj.put("timestamp", tsForServerEpoch);
                                obj.put("id", tsForServerEpoch);
                                obj.put("type", "10");
                                obj.put("replyType", "8");
                                obj.put("name", AppController.getInstance().getUserName());

                                Bundle extras2 = intent.getExtras();

                                obj.put("previousFrom", extras2.getString("previousFrom"));

                                switch (Integer.parseInt(extras2.getString("previousType"))) {

                                    case 1: {


                                        /*
                                         *Image
                                         */
                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());


                                        break;

                                    }
                                    case 2: {

                                        /*
                                         * Video
                                         */
                                        Bitmap bm = ThumbnailUtils.createVideoThumbnail(extras2.getString("previousPayload"),
                                                MediaStore.Images.Thumbnails.MINI_KIND);


                                        bm = Bitmap.createScaledBitmap(bm, 180, 180, false);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;

                                    }
                                    case 7: {
                                        /*
                                         * Doodle
                                         */


                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;
                                    }
                                    default: {

                                        obj.put("previousPayload", extras2.getString("previousPayload"));


                                    }


                                }


                                obj.put("previousType", extras2.getString("previousType"));

                                obj.put("previousId", extras2.getString("previousId"));
                                obj.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));
                                if (extras2.getString("previousType").equals("9")) {

                                    obj.put("previousFileType", extras2.getString("previousFileType"));


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {

                                e.printStackTrace();
                            }


                            break;
                        }
                        case 6:

                        {


                            /*
                             * Stickers
                             */

                            try {


                                obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                                obj.put("from", AppController.getInstance().getUserId());

                                obj.put("payload", Base64.encodeToString(payload.getBytes("UTF-8"), Base64.DEFAULT).trim());


                                obj.put("timestamp", tsForServerEpoch);
                                obj.put("id", tsForServerEpoch);
                                obj.put("type", "10");
                                obj.put("replyType", "6");

                                obj.put("name", AppController.getInstance().getUserName());

                                Bundle extras2 = intent.getExtras();

                                obj.put("previousFrom", extras2.getString("previousFrom"));

                                switch (Integer.parseInt(extras2.getString("previousType"))) {

                                    case 1: {


                                        /*
                                         *Image
                                         */
                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());


                                        break;

                                    }
                                    case 2: {

                                        /*
                                         * Video
                                         */
                                        Bitmap bm = ThumbnailUtils.createVideoThumbnail(extras2.getString("previousPayload"),
                                                MediaStore.Images.Thumbnails.MINI_KIND);


                                        bm = Bitmap.createScaledBitmap(bm, 180, 180, false);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;

                                    }
                                    case 7: {
                                        /*
                                         * Doodle
                                         */


                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;
                                    }
                                    default: {

                                        obj.put("previousPayload", extras2.getString("previousPayload"));


                                    }


                                }


                                obj.put("previousType", extras2.getString("previousType"));

                                obj.put("previousId", extras2.getString("previousId"));
                                obj.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));


                                if (extras2.getString("previousType").equals("9")) {

                                    obj.put("previousFileType", extras2.getString("previousFileType"));


                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {

                                e.printStackTrace();
                            }
                        }


                    }


                }


            }


            String receiverUid, documentId;

            Forward_ContactItem contactItem;
            for (int i = 0; i < mContactData.size(); i++) {
                contactItem = mContactData.get(i);


                if (contactItem.isSelected()) {
                    receiverUid = contactItem.getContactUid();


                    documentId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid, "");


                    if (documentId.isEmpty()) {


                        documentId = AppController.findDocumentIdOfReceiver(receiverUid, Utilities.tsInGmt(),
                                contactItem.getContactName(), contactItem.getContactImage(), "", false, contactItem.getContactIdentifier(), "", false);
                    }


                    try {
                        obj.put("to", receiverUid);
                        obj.put("toDocId", documentId);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    /*
                     * For saving the message into db
                     */
                    if (messageType == 0) {


                        /*
                         * Text message
                         */


                        Map<String, Object> map = new HashMap<>();
                        map.put("message", payload);
                        map.put("messageType", "0");
                        map.put("isSelf", true);
                        map.put("from", AppController.getInstance().getUserId());


                        map.put("Ts", tsForServer);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);


                        AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);

                        map = null;


                    } else if (messageType == 3) {


                        /*
                         * Location
                         */


                        Map<String, Object> map = new HashMap<>();


                        map.put("message", payload);

                        map.put("messageType", "3");
                        map.put("isSelf", true);
                        map.put("from", AppController.getInstance().getUserId());
                        map.put("Ts", tsForServer);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);


                        AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


                        map = null;


                    } else if (messageType == 4) {


                        /*
                         * Contact
                         */


                        Map<String, Object> map = new HashMap<>();
                        map.put("message", payload);

                        map.put("messageType", "4");
                        map.put("isSelf", true);
                        map.put("from", AppController.getInstance().getUserId());
                        map.put("Ts", tsForServer);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);


                        AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


                        map = null;


                    } else if (messageType == 6) {


                        /*
                         *Stickers
                         */


                        Map<String, Object> map = new HashMap<>();
                        map.put("message", payload);
                        map.put("messageType", "6");
                        map.put("isSelf", true);
                        map.put("from", AppController.getInstance().getUserId());
                        map.put("Ts", tsForServer);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);

                        AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);

                        map = null;


                    } else if (messageType == 8) {

                        /*
                         * Gif
                         */


                        Map<String, Object> map = new HashMap<>();
                        map.put("message", payload);
                        map.put("messageType", "8");
                        map.put("isSelf", true);
                        map.put("from", AppController.getInstance().getUserId());
                        map.put("Ts", tsForServer);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);


                        AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);

                        map = null;


                    } else if (messageType == 10) {

                        /*
                         * For the reply message
                         */
                        int replyType = Integer.parseInt(intent.getExtras().getString("replyType"));


                        if (replyType == 0) {


                            /*
                             * Text message
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);

                            map.put("messageType", "10");

                            map.put("replyType", "0");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());


                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);


                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));

                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }
                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);

                            map = null;


                        } else if (replyType == 3) {


                            /*
                             * Location
                             */


                            Map<String, Object> map = new HashMap<>();


                            map.put("message", payload);
                            map.put("messageType", "10");
                            map.put("replyType", "3");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));

                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }
                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


                            map = null;


                        } else if (replyType == 4) {


                            /*
                             * Contact
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "10");
                            map.put("replyType", "4");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }
                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


                            map = null;


                        } else if (replyType == 6) {


                            /*
                             *Stickers
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);


                            map.put("messageType", "10");
                            map.put("replyType", "6");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);
                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }
                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);

                            map = null;


                        } else if (replyType == 8) {

                            /*
                             * Gif
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);


                            map.put("messageType", "10");

                            map.put("replyType", "8");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }
                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);

                            map = null;


                        }


                    }







                    /*
                     *
                     *
                     *
                     * Need to store all the messages in db so that incase internet
                     * not present then has to resend all messages whenever internet comes back
                     *
                     *
                     * */


                    Map<String, Object> mapTemp = new HashMap<>();
                    mapTemp.put("from", AppController.getInstance().getUserId());
                    mapTemp.put("to", receiverUid);

                    mapTemp.put("toDocId", documentId);


                    mapTemp.put("id", tsForServerEpoch);

                    mapTemp.put("timestamp", tsForServerEpoch);


                    String type = Integer.toString(messageType);


                    mapTemp.put("type", type);


                    if (messageType == 10) {

                        /*
                         * For the previous message info to be put
                         */


                        /*
                         * For the message reply feature
                         */
                        Bundle extras2 = intent.getExtras();


                        mapTemp.put("previousFrom", extras2.getString("previousFrom"));
                        mapTemp.put("previousPayload", extras2.getString("previousPayload"));
                        mapTemp.put("previousType", extras2.getString("previousType"));
                        mapTemp.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                        mapTemp.put("previousId", extras2.getString("previousId"));


                        mapTemp.put("replyType", extras2.getString("replyType"));
                        if (extras2.getString("previousType").equals("9")) {

                            mapTemp.put("previousFileType", extras2.getString("previousFileType"));
                        }

                    }


                    mapTemp.put("name", AppController.getInstance().getUserName());


                    mapTemp.put("message", payload);

                    AppController.getInstance().getDbController().addUnsentMessage(AppController.getInstance().getunsentMessageDocId(), mapTemp);


                    /*s
                     *
                     *
                     * emit directly if not image or video or audio
                     *
                     *
                     * */


                    try {


                        obj.put("name", AppController.getInstance().getUserName());


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    HashMap<String, Object> map = new HashMap<>();
                    map.put("messageId", tsForServerEpoch);
                    map.put("docId", documentId);


                    AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + receiverUid, obj, 1, false, map);


                }


            }

            /*
             * For selecting chats in which to forward the given message
             */


            ChatListItem chatItem;


            boolean isSecretChat;

            String secretId;


            CouchDbController db = AppController.getInstance().getDbController();
            long dTime = -1;
            for (int i = 0; i < mChatData.size(); i++) {

                chatItem = mChatData.get(i);


                if (chatItem.isSelected()) {

                    /*
                     * If the selected chat is the group or the normal chat
                     */

                    isSecretChat = !chatItem.getSecretId().isEmpty();
                    secretId = chatItem.getSecretId();

                    receiverUid = chatItem.getReceiverUid();

                    documentId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid, secretId);


                    if (documentId.isEmpty()) {


                        documentId = AppController.findDocumentIdOfReceiver(receiverUid, Utilities.tsInGmt(),
                                chatItem.getReceiverName(), chatItem.getReceiverImage(), secretId, false, chatItem.getReceiverIdentifier(), "", false);
                    }

                    if (isSecretChat) {

                        Map<String, Object> chat_item = db.getParticularChatInfo(documentId);


                        try {

                            dTime = (long) (chat_item.get("dTime"));

                        } catch (ClassCastException e) {

                            dTime = (int) (chat_item.get("dTime"));

                        }

                    }
                    try {
                        obj.put("to", receiverUid);
                        obj.put("toDocId", documentId);
                        if (isSecretChat) {





                            /*
                             * For secret chat exclusively
                             */

                            obj.put("secretId", secretId);
                            obj.put("dTime", dTime);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    /*
                     * For saving the message into db
                     */
                    if (messageType == 0) {


                        /*
                         * Text message
                         */


                        Map<String, Object> map = new HashMap<>();
                        map.put("message", payload);
                        map.put("messageType", "0");
                        map.put("isSelf", true);
                        map.put("from", AppController.getInstance().getUserId());


                        map.put("Ts", tsForServer);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);


                        if (isSecretChat) {
                            /*
                             * For secret chat exclusively
                             */


                            map.put("isDTag", false);

                            map.put("dTime", dTime);
                            map.put("timerStarted", false);

                        }

                        AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);

                        map = null;


                    } else if (messageType == 3) {


                        /*
                         * Location
                         */


                        Map<String, Object> map = new HashMap<>();


                        map.put("message", payload);

                        map.put("messageType", "3");
                        map.put("isSelf", true);
                        map.put("from", AppController.getInstance().getUserId());
                        map.put("Ts", tsForServer);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);

                        if (isSecretChat) {
                            /*
                             * For secret chat exclusively
                             */

                            map.put("dTime", dTime);
                            map.put("timerStarted", false);

                        }
                        AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


                        map = null;


                    } else if (messageType == 4) {


                        /*
                         * Contact
                         */


                        Map<String, Object> map = new HashMap<>();
                        map.put("message", payload);

                        map.put("messageType", "4");
                        map.put("isSelf", true);
                        map.put("from", AppController.getInstance().getUserId());
                        map.put("Ts", tsForServer);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);

                        if (isSecretChat) {
                            /*
                             * For secret chat exclusively
                             */

                            map.put("dTime", dTime);
                            map.put("timerStarted", false);

                        }
                        AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


                        map = null;


                    } else if (messageType == 6) {


                        /*
                         *Stickers
                         */


                        Map<String, Object> map = new HashMap<>();
                        map.put("message", payload);
                        map.put("messageType", "6");
                        map.put("isSelf", true);
                        map.put("from", AppController.getInstance().getUserId());
                        map.put("Ts", tsForServer);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);
                        if (isSecretChat) {
                            /*
                             * For secret chat exclusively
                             */

                            map.put("dTime", dTime);
                            map.put("timerStarted", false);

                        }
                        AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);

                        map = null;


                    } else if (messageType == 8) {

                        /*
                         * Gif
                         */


                        Map<String, Object> map = new HashMap<>();
                        map.put("message", payload);
                        map.put("messageType", "8");
                        map.put("isSelf", true);
                        map.put("from", AppController.getInstance().getUserId());
                        map.put("Ts", tsForServer);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);

                        if (isSecretChat) {
                            /*
                             * For secret chat exclusively
                             */

                            map.put("dTime", dTime);
                            map.put("timerStarted", false);

                        }
                        AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);

                        map = null;


                    } else if (messageType == 10) {


                        String replyType = intent.getExtras().getString("replyType");




                        /*
                         * For saving the message into db
                         */
                        if (replyType.equals("0")) {


                            /*
                             * Text message
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);

                            map.put("messageType", "10");


                            map.put("replyType", "0");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());


                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);
                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }

                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);

                            map = null;


                        } else if (replyType.equals("3")) {


                            /*
                             * Location
                             */


                            Map<String, Object> map = new HashMap<>();

                            map.put("messageType", "10");
                            map.put("message", payload);

                            map.put("replyType", "3");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }
                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


                            map = null;


                        } else if (replyType.equals("4")) {


                            /*
                             * Contact
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "10");
                            map.put("replyType", "4");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }
                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


                            map = null;


                        } else if (replyType.equals("6")) {


                            /*
                             *Stickers
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);

                            map.put("messageType", "10");
                            map.put("replyType", "6");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);
                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }
                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);

                            map = null;


                        } else if (replyType.equals("8")) {

                            /*
                             * Gif
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);


                            map.put("messageType", "10");
                            map.put("replyType", "8");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }
                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);

                            map = null;


                        }


                    }







                    /*
                     *
                     *
                     *
                     * Need to store all the messages in db so that incase internet
                     * not present then has to resend all messages whenever internet comes back
                     *
                     *
                     * */


                    Map<String, Object> mapTemp = new HashMap<>();
                    mapTemp.put("from", AppController.getInstance().getUserId());
                    mapTemp.put("to", receiverUid);

                    mapTemp.put("toDocId", documentId);


                    mapTemp.put("id", tsForServerEpoch);

                    mapTemp.put("timestamp", tsForServerEpoch);


                    String type = Integer.toString(messageType);


                    mapTemp.put("type", type);


                    if (messageType == 10) {

                        /*
                         * For the previous message info to be put
                         */


                        /*
                         * For the message reply feature
                         */
                        Bundle extras2 = intent.getExtras();


                        mapTemp.put("previousFrom", extras2.getString("previousFrom"));
                        mapTemp.put("previousPayload", extras2.getString("previousPayload"));
                        mapTemp.put("previousType", extras2.getString("previousType"));
                        mapTemp.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                        mapTemp.put("previousId", extras2.getString("previousId"));

                        mapTemp.put("replyType", extras2.getString("replyType"));

                        if (extras2.getString("previousType").equals("9")) {

                            mapTemp.put("previousFileType", extras2.getString("previousFileType"));
                        }

                    }


                    mapTemp.put("name", AppController.getInstance().getUserName());

                    if (isSecretChat) {



                        /*
                         * Exclusively for secret chat
                         */
                        mapTemp.put("dTime", dTime);
                        mapTemp.put("secretId", secretId);
                    }
                    mapTemp.put("message", payload);

                    AppController.getInstance().getDbController().addUnsentMessage(AppController.getInstance().getunsentMessageDocId(), mapTemp);


                    /*
                     *
                     *
                     * emit directly if not image or video or audio
                     *
                     *
                     * */


                    try {


                        obj.put("name", AppController.getInstance().getUserName());


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    HashMap<String, Object> map = new HashMap<>();
                    map.put("messageId", tsForServerEpoch);
                    map.put("docId", documentId);


                    AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + receiverUid, obj, 1, false, map);

                    AppController.getInstance().getDbController().updateChatListOnViewingMessage(documentId);

                }

            }


        } else {

            /*
             *
             *
             * image message so payload field is not set
             *
             * */


            switch (messageType) {

                case 1: {
                    try {

                        obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                        obj.put("from", AppController.getInstance().getUserId());

                        obj.put("timestamp", tsForServerEpoch);

                        obj.put("id", tsForServerEpoch);
                        obj.put("type", "1");
//                        obj.put("name", AppController.getInstance().getUserName());
                        obj.put("thumbnail", thumbnail);
                        uploadFile(uri, AppController.getInstance().getUserId() + tsForServerEpoch, 1, obj, true, null);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }





                    /*
                     *
                     *
                     * video message so payload field is not set
                     *
                     * */


                    break;
                }
                case 2: {
                    try {

                        obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                        obj.put("from", AppController.getInstance().getUserId());

                        obj.put("timestamp", tsForServerEpoch);

                        obj.put("id", tsForServerEpoch);
                        obj.put("type", "2");
//                        obj.put("name", AppController.getInstance().getUserName());
                        obj.put("thumbnail", thumbnail);

                        uploadFile(uri, AppController.getInstance().getUserId() + tsForServerEpoch, 2, obj, true, null);


                    } catch (JSONException e) {
                        e.printStackTrace();


                    }




                    /*
                     *
                     *
                     * audio message so payload field is not set
                     *
                     * */


                    break;
                }
                case 5: {
                    try {

                        obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                        obj.put("from", AppController.getInstance().getUserId());

                        obj.put("timestamp", tsForServerEpoch);

                        obj.put("id", tsForServerEpoch);
                        obj.put("type", "5");
//                        obj.put("name", AppController.getInstance().getUserName());


                        uploadFile(uri, AppController.getInstance().getUserId() + tsForServerEpoch, 5, obj, true, null);


                    } catch (JSONException e) {
                        e.printStackTrace();


                    }

                    break;
                }
                case 7: {

                    /*
                     * Doodle
                     */
                    try {

                        obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                        obj.put("from", AppController.getInstance().getUserId());

                        obj.put("timestamp", tsForServerEpoch);


                        obj.put("id", tsForServerEpoch);
                        obj.put("type", "7");

                        obj.put("thumbnail", thumbnail);
//                        obj.put("name", AppController.getInstance().getUserName());


                        uploadFile(uri, AppController.getInstance().getUserId() + tsForServerEpoch, 7, obj, true, null);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    break;
                }
                case 9: {
                    try {

                        obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                        obj.put("from", AppController.getInstance().getUserId());

                        obj.put("timestamp", tsForServerEpoch);


                        obj.put("id", tsForServerEpoch);
                        obj.put("type", "9");
                        obj.put("mimeType", extras.getString("mimeType"));
                        obj.put("extension", extras.getString("extension"));
                        obj.put("fileName", extras.getString("fileName"));
//                        obj.put("name", AppController.getInstance().getUserName());


                        uploadFile(uri, AppController.getInstance().getUserId() + tsForServerEpoch, 9, obj, false, extras.getString("extension"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                }


                case 10: {
                    int replyType = Integer.parseInt(intent.getExtras().getString("replyType"));


                    switch (replyType) {


                        case 1: {
                            try {

                                obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                                obj.put("from", AppController.getInstance().getUserId());

                                obj.put("timestamp", tsForServerEpoch);

                                obj.put("id", tsForServerEpoch);
                                obj.put("type", "10");


                                obj.put("replyType", "1");

                                obj.put("thumbnail", thumbnail);

                                Bundle extras2 = intent.getExtras();

                                obj.put("previousFrom", extras2.getString("previousFrom"));


                                switch (Integer.parseInt(extras2.getString("previousType"))) {

                                    case 1: {


                                        /*
                                         *Image
                                         */
                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());


                                        break;

                                    }
                                    case 2: {

                                        /*
                                         * Video
                                         */
                                        Bitmap bm = ThumbnailUtils.createVideoThumbnail(extras2.getString("previousPayload"),
                                                MediaStore.Images.Thumbnails.MINI_KIND);


                                        bm = Bitmap.createScaledBitmap(bm, 180, 180, false);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;

                                    }
                                    case 7: {
                                        /*
                                         * Doodle
                                         */


                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;
                                    }
                                    default: {

                                        obj.put("previousPayload", extras2.getString("previousPayload"));


                                    }


                                }


                                obj.put("previousType", extras2.getString("previousType"));

                                obj.put("previousId", extras2.getString("previousId"));
                                obj.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));


                                if (extras2.getString("previousType").equals("9")) {

                                    obj.put("previousFileType", extras2.getString("previousFileType"));


                                }


                                //                                obj.put("name", AppController.getInstance().getUserName());
                                uploadFile(uri, AppController.getInstance().getUserId() + tsForServerEpoch, 1, obj, true, null);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }





                            /*
                             *
                             *
                             * video message so payload field is not set
                             *
                             * */


                            break;
                        }
                        case 2: {
                            try {

                                obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                                obj.put("from", AppController.getInstance().getUserId());

                                obj.put("timestamp", tsForServerEpoch);

                                obj.put("id", tsForServerEpoch);
                                obj.put("type", "10");


                                obj.put("replyType", "2");

                                obj.put("thumbnail", thumbnail);
                                Bundle extras2 = intent.getExtras();

                                obj.put("previousFrom", extras2.getString("previousFrom"));

                                switch (Integer.parseInt(extras2.getString("previousType"))) {

                                    case 1: {


                                        /*
                                         *Image
                                         */
                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());


                                        break;

                                    }
                                    case 2: {

                                        /*
                                         * Video
                                         */
                                        Bitmap bm = ThumbnailUtils.createVideoThumbnail(extras2.getString("previousPayload"),
                                                MediaStore.Images.Thumbnails.MINI_KIND);


                                        bm = Bitmap.createScaledBitmap(bm, 180, 180, false);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;

                                    }
                                    case 7: {
                                        /*
                                         * Doodle
                                         */


                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;
                                    }
                                    default: {

                                        obj.put("previousPayload", extras2.getString("previousPayload"));


                                    }


                                }


                                obj.put("previousType", extras2.getString("previousType"));

                                obj.put("previousId", extras2.getString("previousId"));
                                obj.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));


                                if (extras2.getString("previousType").equals("9")) {

                                    obj.put("previousFileType", extras2.getString("previousFileType"));


                                }
                                // obj.put("name", AppController.getInstance().getUserName());
                                uploadFile(uri, AppController.getInstance().getUserId() + tsForServerEpoch, 2, obj, true, null);


                            } catch (JSONException e) {
                                e.printStackTrace();


                            }




                            /*
                             *
                             *
                             * audio message so payload field is not set
                             *
                             * */


                            break;
                        }
                        case 5: {
                            try {

                                obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                                obj.put("from", AppController.getInstance().getUserId());

                                obj.put("timestamp", tsForServerEpoch);

                                obj.put("id", tsForServerEpoch);
                                obj.put("type", "10");


                                obj.put("replyType", "5");


                                Bundle extras2 = intent.getExtras();

                                obj.put("previousFrom", extras2.getString("previousFrom"));

                                switch (Integer.parseInt(extras2.getString("previousType"))) {

                                    case 1: {


                                        /*
                                         *Image
                                         */
                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());


                                        break;

                                    }
                                    case 2: {

                                        /*
                                         * Video
                                         */
                                        Bitmap bm = ThumbnailUtils.createVideoThumbnail(extras2.getString("previousPayload"),
                                                MediaStore.Images.Thumbnails.MINI_KIND);


                                        bm = Bitmap.createScaledBitmap(bm, 180, 180, false);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;

                                    }
                                    case 7: {
                                        /*
                                         * Doodle
                                         */


                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;
                                    }
                                    default: {

                                        obj.put("previousPayload", extras2.getString("previousPayload"));


                                    }


                                }


                                obj.put("previousType", extras2.getString("previousType"));

                                obj.put("previousId", extras2.getString("previousId"));
                                obj.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));


                                if (extras2.getString("previousType").equals("9")) {

                                    obj.put("previousFileType", extras2.getString("previousFileType"));


                                }

                                // obj.put("name", AppController.getInstance().getUserName());
                                uploadFile(uri, AppController.getInstance().getUserId() + tsForServerEpoch, 5, obj, true, null);


                            } catch (JSONException e) {
                                e.printStackTrace();


                            }

                            break;
                        }
                        case 7: {

                            /*
                             * Doodle
                             */
                            try {

                                obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                                obj.put("from", AppController.getInstance().getUserId());

                                obj.put("timestamp", tsForServerEpoch);


                                obj.put("id", tsForServerEpoch);
                                obj.put("type", "10");


                                obj.put("replyType", "7");

                                obj.put("thumbnail", thumbnail);

                                Bundle extras2 = intent.getExtras();

                                obj.put("previousFrom", extras2.getString("previousFrom"));

                                switch (Integer.parseInt(extras2.getString("previousType"))) {

                                    case 1: {


                                        /*
                                         *Image
                                         */
                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());


                                        break;

                                    }
                                    case 2: {

                                        /*
                                         * Video
                                         */
                                        Bitmap bm = ThumbnailUtils.createVideoThumbnail(extras2.getString("previousPayload"),
                                                MediaStore.Images.Thumbnails.MINI_KIND);


                                        bm = Bitmap.createScaledBitmap(bm, 180, 180, false);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;

                                    }
                                    case 7: {
                                        /*
                                         * Doodle
                                         */


                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;
                                    }
                                    default: {

                                        obj.put("previousPayload", extras2.getString("previousPayload"));


                                    }


                                }


                                obj.put("previousType", extras2.getString("previousType"));

                                obj.put("previousId", extras2.getString("previousId"));
                                obj.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));


                                if (extras2.getString("previousType").equals("9")) {

                                    obj.put("previousFileType", extras2.getString("previousFileType"));


                                }
// obj.put("name", AppController.getInstance().getUserName());
                                uploadFile(uri, AppController.getInstance().getUserId() + tsForServerEpoch, 7, obj, true, null);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            break;
                        }
                        case 9: {
                            try {

                                obj.put("receiverIdentifier", AppController.getInstance().getUserIdentifier());
                                obj.put("from", AppController.getInstance().getUserId());

                                obj.put("timestamp", tsForServerEpoch);


                                obj.put("id", tsForServerEpoch);
                                obj.put("type", "10");


                                obj.put("replyType", "9");
                                obj.put("mimeType", extras.getString("mimeType"));
                                obj.put("extension", extras.getString("extension"));
                                obj.put("fileName", extras.getString("fileName"));


                                Bundle extras2 = intent.getExtras();

                                obj.put("previousFrom", extras2.getString("previousFrom"));

                                switch (Integer.parseInt(extras2.getString("previousType"))) {

                                    case 1: {


                                        /*
                                         *Image
                                         */
                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());


                                        break;

                                    }
                                    case 2: {

                                        /*
                                         * Video
                                         */
                                        Bitmap bm = ThumbnailUtils.createVideoThumbnail(extras2.getString("previousPayload"),
                                                MediaStore.Images.Thumbnails.MINI_KIND);


                                        bm = Bitmap.createScaledBitmap(bm, 180, 180, false);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;

                                    }
                                    case 7: {
                                        /*
                                         * Doodle
                                         */


                                        Bitmap bm = decodeSampledBitmapFromResource(extras2.getString("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;
                                    }
                                    default: {

                                        obj.put("previousPayload", extras2.getString("previousPayload"));


                                    }


                                }


                                obj.put("previousType", extras2.getString("previousType"));

                                obj.put("previousId", extras2.getString("previousId"));
                                obj.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));


                                if (extras2.getString("previousType").equals("9")) {

                                    obj.put("previousFileType", extras2.getString("previousFileType"));


                                }
                                //obj.put("name", AppController.getInstance().getUserName());

                                uploadFile(uri, AppController.getInstance().getUserId() + tsForServerEpoch, 9, obj, false, extras.getString("extension"));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            break;
                        }

                    }

                    break;
                }


            }

            String receiverUid, documentId;

            Forward_ContactItem contactItem;
            for (int i = 0; i < mContactData.size(); i++) {
                contactItem = mContactData.get(i);


                if (contactItem.isSelected()) {
                    receiverUid = contactItem.getContactUid();


                    documentId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid, "");


                    if (documentId.isEmpty()) {


                        documentId = AppController.findDocumentIdOfReceiver(receiverUid, Utilities.tsInGmt(),
                                contactItem.getContactName(), contactItem.getContactImage(), "", false, contactItem.getContactIdentifier(), "", false);
                    }


                    try {
                        obj.put("to", receiverUid);
                        obj.put("toDocId", documentId);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    /*
                     * For saving the message into db
                     */


                    if (messageType == 1) {


                        /*
                         * receiverImage
                         */


                        Map<String, Object> map = new HashMap<>();
                        map.put("message", payload);
                        map.put("messageType", "1");
                        map.put("isSelf", true);


                        map.put("downloadStatus", 1);
                        map.put("from", AppController.getInstance().getUserId());
                        map.put("Ts", tsForServer);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);


                        AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);
                        map = null;


                    } else if (messageType == 2) {

                        /*
                         * Video
                         */


                        Map<String, Object> map = new HashMap<>();
                        map.put("message", payload);
                        map.put("messageType", "2");
                        map.put("isSelf", true);
                        map.put("downloadStatus", 1);
                        map.put("from", AppController.getInstance().getUserId());
                        map.put("Ts", tsForServer);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);


                        AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);
                        map = null;


                    } else if (messageType == 5) {

                        /*
                         * Audio
                         */


                        Map<String, Object> map = new HashMap<>();
                        map.put("message", payload);
                        map.put("messageType", "5");
                        map.put("isSelf", true);
                        map.put("from", AppController.getInstance().getUserId());
                        map.put("Ts", tsForServer);
                        map.put("downloadStatus", 1);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);


                        AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


                        map = null;


                    }
//


                    else if (messageType == 7) {


                        /*
                         * Doodle
                         */


                        Map<String, Object> map = new HashMap<>();
                        map.put("message", payload);
                        map.put("messageType", "7");
                        map.put("isSelf", true);
                        map.put("from", AppController.getInstance().getUserId());
                        map.put("Ts", tsForServer);


                        map.put("downloadStatus", 1);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);

                        AppController.getInstance().getDbController().
                                addNewChatMessageAndSort(documentId, map, tsForServer);
                        map = null;


                    } else if (messageType == 9) {


                        /*
                         * Document
                         */


                        Map<String, Object> map = new HashMap<>();
                        map.put("message", payload);
                        map.put("messageType", "9");
                        map.put("isSelf", true);
                        map.put("from", AppController.getInstance().getUserId());
                        map.put("Ts", tsForServer);


                        map.put("downloadStatus", 1);
                        map.put("deliveryStatus", "0");
                        map.put("id", tsForServerEpoch);


                        map.put("mimeType", extras.getString("mimeType"));
                        map.put("extension", extras.getString("extension"));
                        map.put("fileName", extras.getString("fileName"));


                        AppController.getInstance().getDbController().
                                addNewChatMessageAndSort(documentId, map, tsForServer);
                        map = null;


                    } else if (messageType == 10) {


                        int replyType = Integer.parseInt(intent.getExtras().getString("replyType"));


                        if (replyType == 1) {


                            /*
                             * receiverImage
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "10");

                            map.put("replyType", "1");
                            map.put("isSelf", true);


                            map.put("downloadStatus", 1);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }

                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);
                            map = null;


                        } else if (replyType == 2) {

                            /*
                             * Video
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "10");

                            map.put("replyType", "2");
                            map.put("isSelf", true);
                            map.put("downloadStatus", 1);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }

                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);
                            map = null;


                        } else if (replyType == 5) {

                            /*
                             * Audio
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "10");

                            map.put("replyType", "5");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("downloadStatus", 1);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }

                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


                            map = null;


                        }
//


                        else if (replyType == 7) {


                            /*
                             * Doodle
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "10");

                            map.put("replyType", "7");


                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);


                            map.put("downloadStatus", 1);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }
                            AppController.getInstance().getDbController().
                                    addNewChatMessageAndSort(documentId, map, tsForServer);
                            map = null;


                        } else if (replyType == 9) {


                            /*
                             * Document
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "10");


                            map.put("replyType", "9");

                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);


                            map.put("downloadStatus", 1);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);


                            map.put("mimeType", extras.getString("mimeType"));
                            map.put("extension", extras.getString("extension"));
                            map.put("fileName", extras.getString("fileName"));


                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            map.put("previousFrom", extras2.getString("previousFrom"));
                            map.put("previousPayload", extras2.getString("previousPayload"));
                            map.put("previousType", extras2.getString("previousType"));
                            map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                            map.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                map.put("previousFileType", extras2.getString("previousFileType"));
                            }
                            AppController.getInstance().getDbController().
                                    addNewChatMessageAndSort(documentId, map, tsForServer);
                            map = null;


                        }
                    }




                    /*
                     *
                     *
                     *
                     * Need to store all the messages in db so that incase internet
                     * not present then has to resend all messages whenever internet comes back
                     *
                     *
                     * */


                    Map<String, Object> mapTemp = new HashMap<>();
                    mapTemp.put("from", AppController.getInstance().getUserId());
                    mapTemp.put("to", receiverUid);

                    mapTemp.put("toDocId", documentId);


                    mapTemp.put("id", tsForServerEpoch);

                    mapTemp.put("timestamp", tsForServerEpoch);


                    String type = Integer.toString(messageType);


                    mapTemp.put("type", type);

                    if (messageType == 10) {



                        /*
                         * For the message reply feature
                         */
                        Bundle extras2 = intent.getExtras();


                        mapTemp.put("previousFrom", extras2.getString("previousFrom"));
                        mapTemp.put("previousPayload", extras2.getString("previousPayload"));
                        mapTemp.put("previousType", extras2.getString("previousType"));
                        mapTemp.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));
                        mapTemp.put("replyType", extras2.getString("replyType"));

                        mapTemp.put("previousId", extras2.getString("previousId"));
                        if (extras2.getString("previousType").equals("9")) {

                            mapTemp.put("previousFileType", extras2.getString("previousFileType"));
                        }

                    }
                    mapTemp.put("name", AppController.getInstance().getUserName());


                    mapTemp.put("message", payload);

                    AppController.getInstance().getDbController().addUnsentMessage(AppController.getInstance().getunsentMessageDocId(), mapTemp);


                }


            }


            /*
             * For sharing the file in the chats
             */


            boolean isSecretChat;


            String secretId;


            CouchDbController db = AppController.getInstance().getDbController();
            long dTime = -1;


            ChatListItem chatListItem;
            for (int i = 0; i < mChatData.size(); i++) {
                chatListItem = mChatData.get(i);


                if (chatListItem.isSelected()) {

                    if (chatListItem.isGroupChat()) {

                        /*
                         * If the selected chat is a groupchat
                         */


                        receiverUid = chatListItem.getReceiverUid();


                        documentId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid, "");


                        if (documentId.isEmpty()) {


                            documentId = AppController.findDocumentIdOfReceiver(receiverUid, Utilities.tsInGmt(),
                                    chatListItem.getReceiverName(), chatListItem.getReceiverImage(), "", false,
                                    chatListItem.getReceiverIdentifier(), "", false);
                        }


                        try {
                            obj.put("to", receiverUid);
                            obj.put("toDocId", documentId);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        /*
                         * For saving the message into db
                         */


                        if (messageType == 1) {


                            /*
                             * receiverImage
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "1");
                            map.put("isSelf", true);


                            map.put("downloadStatus", 1);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);


                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);
                            map = null;


                        } else if (messageType == 2) {

                            /*
                             * Video
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "2");
                            map.put("isSelf", true);
                            map.put("downloadStatus", 1);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);


                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);
                            map = null;


                        } else if (messageType == 5) {

                            /*
                             * Audio
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "5");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("downloadStatus", 1);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


                            map = null;


                        }
//


                        else if (messageType == 7) {


                            /*
                             * Doodle
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "7");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);


                            map.put("downloadStatus", 1);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            AppController.getInstance().getDbController().
                                    addNewChatMessageAndSort(documentId, map, tsForServer);
                            map = null;


                        } else if (messageType == 9) {


                            /*
                             * Document
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "9");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);

                            map.put("mimeType", extras.getString("mimeType"));
                            map.put("extension", extras.getString("extension"));
                            map.put("fileName", extras.getString("fileName"));


                            map.put("downloadStatus", 1);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            AppController.getInstance().getDbController().
                                    addNewChatMessageAndSort(documentId, map, tsForServer);
                            map = null;


                        } else if (messageType == 10) {






                            /*
                             * For saving the message into db
                             */


                            int replyType = Integer.parseInt(intent.getExtras().getString("replyType"));


                            if (replyType == 1) {


                                /*
                                 * receiverImage
                                 */


                                Map<String, Object> map = new HashMap<>();
                                map.put("message", payload);
                                map.put("messageType", "10");

                                map.put("replyType", "1");

                                map.put("isSelf", true);


                                map.put("downloadStatus", 1);
                                map.put("from", AppController.getInstance().getUserId());
                                map.put("Ts", tsForServer);
                                map.put("deliveryStatus", "0");
                                map.put("id", tsForServerEpoch);
                                Bundle extras2 = intent.getExtras();
                                map.put("previousFrom", extras2.getString("previousFrom"));
                                map.put("previousPayload", extras2.getString("previousPayload"));
                                map.put("previousType", extras2.getString("previousType"));
                                map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                                map.put("previousId", extras2.getString("previousId"));
                                if (extras2.getString("previousType").equals("9")) {

                                    map.put("previousFileType", extras2.getString("previousFileType"));
                                }
                                AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);
                                map = null;


                            } else if (replyType == 2) {

                                /*
                                 * Video
                                 */


                                Map<String, Object> map = new HashMap<>();
                                map.put("message", payload);
                                map.put("messageType", "10");

                                map.put("replyType", "2");
                                map.put("isSelf", true);
                                map.put("downloadStatus", 1);
                                map.put("from", AppController.getInstance().getUserId());
                                map.put("Ts", tsForServer);
                                map.put("deliveryStatus", "0");
                                map.put("id", tsForServerEpoch);
                                Bundle extras2 = intent.getExtras();
                                map.put("previousFrom", extras2.getString("previousFrom"));
                                map.put("previousPayload", extras2.getString("previousPayload"));
                                map.put("previousType", extras2.getString("previousType"));
                                map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                                map.put("previousId", extras2.getString("previousId"));
                                if (extras2.getString("previousType").equals("9")) {

                                    map.put("previousFileType", extras2.getString("previousFileType"));
                                }
                                AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);
                                map = null;


                            } else if (replyType == 5) {

                                /*
                                 * Audio
                                 */


                                Map<String, Object> map = new HashMap<>();
                                map.put("message", payload);
                                map.put("messageType", "10");

                                map.put("replyType", "5");
                                map.put("isSelf", true);
                                map.put("from", AppController.getInstance().getUserId());
                                map.put("Ts", tsForServer);
                                map.put("downloadStatus", 1);
                                map.put("deliveryStatus", "0");
                                map.put("id", tsForServerEpoch);
                                Bundle extras2 = intent.getExtras();
                                map.put("previousFrom", extras2.getString("previousFrom"));
                                map.put("previousPayload", extras2.getString("previousPayload"));
                                map.put("previousType", extras2.getString("previousType"));
                                map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                                map.put("previousId", extras2.getString("previousId"));
                                if (extras2.getString("previousType").equals("9")) {

                                    map.put("previousFileType", extras2.getString("previousFileType"));
                                }
                                AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


                                map = null;


                            }
//


                            else if (replyType == 7) {


                                /*
                                 * Doodle
                                 */


                                Map<String, Object> map = new HashMap<>();
                                map.put("message", payload);
                                map.put("messageType", "10");

                                map.put("replyType", "7");
                                map.put("isSelf", true);
                                map.put("from", AppController.getInstance().getUserId());
                                map.put("Ts", tsForServer);


                                map.put("downloadStatus", 1);
                                map.put("deliveryStatus", "0");
                                map.put("id", tsForServerEpoch);


                                Bundle extras2 = intent.getExtras();

                                map.put("previousFrom", extras2.getString("previousFrom"));
                                map.put("previousPayload", extras2.getString("previousPayload"));
                                map.put("previousType", extras2.getString("previousType"));
                                map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                                map.put("previousId", extras2.getString("previousId"));
                                if (extras2.getString("previousType").equals("9")) {

                                    map.put("previousFileType", extras2.getString("previousFileType"));
                                }
                                AppController.getInstance().getDbController().
                                        addNewChatMessageAndSort(documentId, map, tsForServer);
                                map = null;


                            } else if (replyType == 9) {


                                /*
                                 * Document
                                 */


                                Map<String, Object> map = new HashMap<>();
                                map.put("message", payload);
                                map.put("messageType", "10");
                                map.put("replyType", "9");

                                map.put("isSelf", true);
                                map.put("from", AppController.getInstance().getUserId());
                                map.put("Ts", tsForServer);

                                map.put("mimeType", extras.getString("mimeType"));
                                map.put("extension", extras.getString("extension"));
                                map.put("fileName", extras.getString("fileName"));


                                map.put("downloadStatus", 1);
                                map.put("deliveryStatus", "0");
                                map.put("id", tsForServerEpoch);

                                Bundle extras2 = intent.getExtras();

                                map.put("previousFrom", extras2.getString("previousFrom"));
                                map.put("previousPayload", extras2.getString("previousPayload"));
                                map.put("previousType", extras2.getString("previousType"));
                                map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                                map.put("previousId", extras2.getString("previousId"));
                                if (extras2.getString("previousType").equals("9")) {

                                    map.put("previousFileType", extras2.getString("previousFileType"));
                                }
                                AppController.getInstance().getDbController().
                                        addNewChatMessageAndSort(documentId, map, tsForServer);

                                map = null;


                            }


                        }



                        /*
                         *
                         *
                         *
                         * Need to store all the messages in db so that incase internet
                         * not present then has to resend all messages whenever internet comes back
                         *
                         *
                         * */


                        Map<String, Object> mapTemp = new HashMap<>();
                        mapTemp.put("from", AppController.getInstance().getUserId());
                        mapTemp.put("to", receiverUid);

                        mapTemp.put("toDocId", documentId);


                        mapTemp.put("id", tsForServerEpoch);

                        mapTemp.put("timestamp", tsForServerEpoch);


                        String type = Integer.toString(messageType);


                        mapTemp.put("type", type);
                        if (messageType == 10) {



                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            mapTemp.put("previousFrom", extras2.getString("previousFrom"));
                            mapTemp.put("previousPayload", extras2.getString("previousPayload"));
                            mapTemp.put("previousType", extras2.getString("previousType"));
                            mapTemp.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));
                            mapTemp.put("replyType", extras2.getString("replyType"));

                            mapTemp.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                mapTemp.put("previousFileType", extras2.getString("previousFileType"));
                            }

                        }


                        mapTemp.put("isGroupMessage", true);


                        mapTemp.put("groupMembersDocId", chatListItem.getGroupMembersDocId());


                        mapTemp.put("name", chatListItem.getReceiverName());


                        String receiverImage = chatListItem.getReceiverImage();
                        if (receiverImage == null || receiverImage.isEmpty()) {
                            mapTemp.put("userImage", "");
                        } else {
                            mapTemp.put("userImage", receiverImage);
                        }


                        mapTemp.put("message", payload);

                        AppController.getInstance().getDbController().addUnsentMessage(AppController.getInstance().getunsentMessageDocId(), mapTemp);


                    } else {


                        /*
                         *If the selected chat is a secret or the group chat
                         *
                         */


                        isSecretChat = !chatListItem.getSecretId().isEmpty();
                        secretId = chatListItem.getSecretId();

                        receiverUid = chatListItem.getReceiverUid();


                        documentId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid, secretId);


                        if (documentId.isEmpty()) {


                            documentId = AppController.findDocumentIdOfReceiver(receiverUid, Utilities.tsInGmt(),
                                    chatListItem.getReceiverName(), chatListItem.getReceiverImage(), secretId, false,
                                    chatListItem.getReceiverIdentifier(), "", false);
                        }
                        if (isSecretChat) {

                            Map<String, Object> chat_item = db.getParticularChatInfo(documentId);


                            try {

                                dTime = (long) (chat_item.get("dTime"));

                            } catch (ClassCastException e) {

                                dTime = (int) (chat_item.get("dTime"));

                            }

                        }

                        try {
                            obj.put("to", receiverUid);
                            obj.put("toDocId", documentId);

                            if (isSecretChat) {





                                /*
                                 * For secret chat exclusively
                                 */

                                obj.put("secretId", secretId);
                                obj.put("dTime", dTime);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        /*
                         * For saving the message into db
                         */


                        if (messageType == 1) {


                            /*
                             * receiverImage
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "1");
                            map.put("isSelf", true);


                            map.put("downloadStatus", 1);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            if (isSecretChat) {
                                /*
                                 * For secret chat exclusively
                                 */

                                map.put("dTime", dTime);
                                map.put("timerStarted", false);

                            }
                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);
                            map = null;


                        } else if (messageType == 2) {

                            /*
                             * Video
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "2");
                            map.put("isSelf", true);
                            map.put("downloadStatus", 1);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            if (isSecretChat) {
                                /*
                                 * For secret chat exclusively
                                 */

                                map.put("dTime", dTime);
                                map.put("timerStarted", false);

                            }
                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);
                            map = null;


                        } else if (messageType == 5) {

                            /*
                             * Audio
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "5");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);
                            map.put("downloadStatus", 1);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);

                            if (isSecretChat) {
                                /*
                                 * For secret chat exclusively
                                 */

                                map.put("dTime", dTime);
                                map.put("timerStarted", false);

                            }
                            AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


                            map = null;


                        }
//


                        else if (messageType == 7) {


                            /*
                             * Doodle
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "7");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);


                            map.put("downloadStatus", 1);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);
                            if (isSecretChat) {
                                /*
                                 * For secret chat exclusively
                                 */

                                map.put("dTime", dTime);
                                map.put("timerStarted", false);

                            }
                            AppController.getInstance().getDbController().
                                    addNewChatMessageAndSort(documentId, map, tsForServer);
                            map = null;


                        } else if (messageType == 9) {


                            /*
                             * Document
                             */


                            Map<String, Object> map = new HashMap<>();
                            map.put("message", payload);
                            map.put("messageType", "9");
                            map.put("isSelf", true);
                            map.put("from", AppController.getInstance().getUserId());
                            map.put("Ts", tsForServer);

                            map.put("mimeType", extras.getString("mimeType"));
                            map.put("extension", extras.getString("extension"));
                            map.put("fileName", extras.getString("fileName"));


                            map.put("downloadStatus", 1);
                            map.put("deliveryStatus", "0");
                            map.put("id", tsForServerEpoch);
                            if (isSecretChat) {
                                /*
                                 * For secret chat exclusively
                                 */

                                map.put("dTime", dTime);
                                map.put("timerStarted", false);

                            }
                            AppController.getInstance().getDbController().
                                    addNewChatMessageAndSort(documentId, map, tsForServer);
                            map = null;


                        } else if (messageType == 10) {






                            /*
                             * For saving the message into db
                             */


                            int replyType = Integer.parseInt(intent.getExtras().getString("replyType"));


                            if (replyType == 1) {


                                /*
                                 * receiverImage
                                 */


                                Map<String, Object> map = new HashMap<>();
                                map.put("message", payload);
                                map.put("messageType", "10");

                                map.put("replyType", "1");

                                map.put("isSelf", true);


                                map.put("downloadStatus", 1);
                                map.put("from", AppController.getInstance().getUserId());
                                map.put("Ts", tsForServer);
                                map.put("deliveryStatus", "0");
                                map.put("id", tsForServerEpoch);
                                Bundle extras2 = intent.getExtras();
                                map.put("previousFrom", extras2.getString("previousFrom"));
                                map.put("previousPayload", extras2.getString("previousPayload"));
                                map.put("previousType", extras2.getString("previousType"));
                                map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                                map.put("previousId", extras2.getString("previousId"));
                                if (extras2.getString("previousType").equals("9")) {

                                    map.put("previousFileType", extras2.getString("previousFileType"));
                                }
                                AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);
                                map = null;


                            } else if (replyType == 2) {

                                /*
                                 * Video
                                 */


                                Map<String, Object> map = new HashMap<>();
                                map.put("message", payload);
                                map.put("messageType", "10");

                                map.put("replyType", "2");
                                map.put("isSelf", true);
                                map.put("downloadStatus", 1);
                                map.put("from", AppController.getInstance().getUserId());
                                map.put("Ts", tsForServer);
                                map.put("deliveryStatus", "0");
                                map.put("id", tsForServerEpoch);
                                Bundle extras2 = intent.getExtras();
                                map.put("previousFrom", extras2.getString("previousFrom"));
                                map.put("previousPayload", extras2.getString("previousPayload"));
                                map.put("previousType", extras2.getString("previousType"));
                                map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                                map.put("previousId", extras2.getString("previousId"));
                                if (extras2.getString("previousType").equals("9")) {

                                    map.put("previousFileType", extras2.getString("previousFileType"));
                                }
                                AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);
                                map = null;


                            } else if (replyType == 5) {

                                /*
                                 * Audio
                                 */


                                Map<String, Object> map = new HashMap<>();
                                map.put("message", payload);
                                map.put("messageType", "10");

                                map.put("replyType", "5");
                                map.put("isSelf", true);
                                map.put("from", AppController.getInstance().getUserId());
                                map.put("Ts", tsForServer);
                                map.put("downloadStatus", 1);
                                map.put("deliveryStatus", "0");
                                map.put("id", tsForServerEpoch);
                                Bundle extras2 = intent.getExtras();
                                map.put("previousFrom", extras2.getString("previousFrom"));
                                map.put("previousPayload", extras2.getString("previousPayload"));
                                map.put("previousType", extras2.getString("previousType"));
                                map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                                map.put("previousId", extras2.getString("previousId"));
                                if (extras2.getString("previousType").equals("9")) {

                                    map.put("previousFileType", extras2.getString("previousFileType"));
                                }
                                AppController.getInstance().getDbController().addNewChatMessageAndSort(documentId, map, tsForServer);


                                map = null;


                            }
//


                            else if (replyType == 7) {


                                /*
                                 * Doodle
                                 */


                                Map<String, Object> map = new HashMap<>();
                                map.put("message", payload);
                                map.put("messageType", "10");

                                map.put("replyType", "7");
                                map.put("isSelf", true);
                                map.put("from", AppController.getInstance().getUserId());
                                map.put("Ts", tsForServer);


                                map.put("downloadStatus", 1);
                                map.put("deliveryStatus", "0");
                                map.put("id", tsForServerEpoch);


                                Bundle extras2 = intent.getExtras();

                                map.put("previousFrom", extras2.getString("previousFrom"));
                                map.put("previousPayload", extras2.getString("previousPayload"));
                                map.put("previousType", extras2.getString("previousType"));
                                map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                                map.put("previousId", extras2.getString("previousId"));
                                if (extras2.getString("previousType").equals("9")) {

                                    map.put("previousFileType", extras2.getString("previousFileType"));
                                }
                                AppController.getInstance().getDbController().
                                        addNewChatMessageAndSort(documentId, map, tsForServer);
                                map = null;


                            } else if (replyType == 9) {


                                /*
                                 * Document
                                 */


                                Map<String, Object> map = new HashMap<>();
                                map.put("message", payload);
                                map.put("messageType", "10");
                                map.put("replyType", "9");

                                map.put("isSelf", true);
                                map.put("from", AppController.getInstance().getUserId());
                                map.put("Ts", tsForServer);

                                map.put("mimeType", extras.getString("mimeType"));
                                map.put("extension", extras.getString("extension"));
                                map.put("fileName", extras.getString("fileName"));


                                map.put("downloadStatus", 1);
                                map.put("deliveryStatus", "0");
                                map.put("id", tsForServerEpoch);

                                Bundle extras2 = intent.getExtras();

                                map.put("previousFrom", extras2.getString("previousFrom"));
                                map.put("previousPayload", extras2.getString("previousPayload"));
                                map.put("previousType", extras2.getString("previousType"));
                                map.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));

                                map.put("previousId", extras2.getString("previousId"));
                                if (extras2.getString("previousType").equals("9")) {

                                    map.put("previousFileType", extras2.getString("previousFileType"));
                                }
                                AppController.getInstance().getDbController().
                                        addNewChatMessageAndSort(documentId, map, tsForServer);

                                map = null;


                            }


                        }



                        /*
                         *
                         *
                         *
                         * Need to store all the messages in db so that incase internet
                         * not present then has to resend all messages whenever internet comes back
                         *
                         *
                         * */


                        Map<String, Object> mapTemp = new HashMap<>();
                        mapTemp.put("from", AppController.getInstance().getUserId());
                        mapTemp.put("to", receiverUid);

                        mapTemp.put("toDocId", documentId);


                        mapTemp.put("id", tsForServerEpoch);

                        mapTemp.put("timestamp", tsForServerEpoch);


                        String type = Integer.toString(messageType);


                        mapTemp.put("type", type);
                        if (messageType == 10) {



                            /*
                             * For the message reply feature
                             */
                            Bundle extras2 = intent.getExtras();


                            mapTemp.put("previousFrom", extras2.getString("previousFrom"));
                            mapTemp.put("previousPayload", extras2.getString("previousPayload"));
                            mapTemp.put("previousType", extras2.getString("previousType"));
                            mapTemp.put("previousReceiverIdentifier", extras2.getString("previousReceiverIdentifier"));
                            mapTemp.put("replyType", extras2.getString("replyType"));

                            mapTemp.put("previousId", extras2.getString("previousId"));
                            if (extras2.getString("previousType").equals("9")) {

                                mapTemp.put("previousFileType", extras2.getString("previousFileType"));
                            }

                        }

                        mapTemp.put("name", AppController.getInstance().getUserName());

                        if (isSecretChat) {



                            /*
                             * Exclusively for secret chat
                             */
                            mapTemp.put("dTime", dTime);
                            mapTemp.put("secretId", secretId);
                        }
                        mapTemp.put("message", payload);

                        AppController.getInstance().getDbController().addUnsentMessage(AppController.getInstance().getunsentMessageDocId(), mapTemp);


                    }

                }


            }


        }


        if (pDialog != null && pDialog.isShowing()) {
            // pDialog.dismiss();
            Context context = ((ContextWrapper) (pDialog).getContext()).getBaseContext();


            if (context instanceof Activity) {


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                        pDialog.dismiss();
                    }
                } else {


                    if (!((Activity) context).isFinishing()) {
                        pDialog.dismiss();
                    }
                }
            } else {


                try {
                    pDialog.dismiss();
                } catch (final IllegalArgumentException e) {
                    e.printStackTrace();

                } catch (final Exception e) {
                    e.printStackTrace();

                }
            }


        }
        supportFinishAfterTransition();

    }




    /*
     * Utility methods
     */

    @SuppressWarnings("TryWithIdenticalCatches")
    private static byte[] convertFileToByteArray(File f) {


        byte[] byteArray = null;
        byte[] b;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {

            InputStream inputStream = new FileInputStream(f);
            b = new byte[2663];

            int bytesRead;

            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }


            inputStream = null;

            byteArray = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } finally {
            b = null;

            try {
                bos.close();

            } catch (IOException e) {
                e.printStackTrace();
            }


            bos = null;
        }


        return byteArray;
    }


    /*
     * To save the byte array received in to file
     */
    @SuppressWarnings("all")
    public File convertByteArrayToFile(byte[] data, String name, String extension) {


        File file = null;

        try {


            File folder = new File(Environment.getExternalStorageDirectory().getPath() + Config.CHAT_UPLOAD_THUMBNAILS_FOLDER);

            if (!folder.exists() && !folder.isDirectory()) {
                folder.mkdirs();
            }


            file = new File(Environment.getExternalStorageDirectory().getPath() + Config.CHAT_UPLOAD_THUMBNAILS_FOLDER, name + extension);
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);

            fos.write(data);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return file;

    }

    /*
     * To calculate the required dimensions of image withoutb actually loading the bitmap in to the memory
     */
    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {


        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;


            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private Bitmap decodeSampledBitmapFromResource(String pathName,
                                                   int reqWidth, int reqHeight) {


        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathName, options);


        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);


        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathName, options);

    }


    /*
     * Uploading images and video and audio to  the server
     */
    @SuppressWarnings("TryWithIdenticalCatches,all")

    private void uploadFile(final Uri fileUri, final String name, final int messageType,
                            final JSONObject obj, final boolean toDeleteFile, final String extension) {


        FileUploadService service =
                ServiceGenerator.createService(FileUploadService.class);


        final File file = FileUtils.getFile(this, fileUri);

        String url = null;
        if (messageType == 1) {

            url = name + ".jpg";


        } else if (messageType == 2) {

            url = name + ".mp4";


        } else if (messageType == 5) {

            url = name + ".mp3";


        } else if (messageType == 7) {

            url = name + ".jpg";


        } else if (messageType == 9) {

            url = name + extension;


        }


        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);


        MultipartBody.Part body =
                MultipartBody.Part.createFormData("photo", url, requestFile);


        String descriptionString = getString(R.string.string_803);
        RequestBody description =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), descriptionString);


        Call<ResponseBody> call = service.upload(description, body);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {

                /*
                 *
                 *
                 * has to get url from the server in response
                 *
                 *
                 * */


                try {


                    if (response.code() == 200) {


                        String url = null;
                        if (messageType == 1) {

                            url = name + ".jpg";


                        } else if (messageType == 2) {

                            url = name + ".mp4";


                        } else if (messageType == 5) {

                            url = name + ".mp3";


                        } else if (messageType == 7) {

                            url = name + ".jpg";


                        } else if (messageType == 9) {

                            url = name + extension;


                        }
                        obj.put("payload", Base64.encodeToString((ApiOnServer.CHAT_FETCH_PATH + url).getBytes("UTF-8"), Base64.DEFAULT));
                        obj.put("dataSize", file.length());
                        obj.put("timestamp", new Utilities().gmtToEpoch(Utilities.tsInGmt()));
                        if (toDeleteFile) {
                            File fdelete = new File(fileUri.getPath());
                            if (fdelete.exists()) fdelete.delete();

                        }
                        /*
                         *
                         *
                         * emitting to the server the values after the file has been uploaded
                         *
                         * */

                        String receiverUid, documentId;
                        Forward_ContactItem contactItem;

                        /*
                         *For figuring out the contacts whom to forward the messages
                         */

                        for (int i = 0; i < mContactData.size(); i++) {
                            contactItem = mContactData.get(i);


                            if (contactItem.isSelected()) {
                                receiverUid = contactItem.getContactUid();


                                documentId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid, "");

                                if (documentId.isEmpty()) {
                                    documentId = AppController.findDocumentIdOfReceiver(receiverUid, Utilities.tsInGmt(),
                                            contactItem.getContactName(), contactItem.getContactImage(), "",
                                            false, contactItem.getContactIdentifier(), "", false);
                                }

                                try {
                                    obj.put("to", receiverUid);
                                    obj.put("toDocId", documentId);
                                    obj.put("name", AppController.getInstance().getUserName());

                                    HashMap<String, Object> map = new HashMap<>();
                                    map.put("messageId", obj.getString("id"));
                                    map.put("docId", documentId);


                                    AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + receiverUid, obj, 1, false, map);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }
                        }


                        ChatListItem chatListItem;


                        boolean isSecretChat;
                        String secretId;
                        long dTime = -1;
                        CouchDbController db = AppController.getInstance().getDbController();
                        for (int i = 0; i < mChatData.size(); i++) {
                            chatListItem = mChatData.get(i);


                            if (chatListItem.isSelected()) {

                                /*
                                 * Chat to forward the message into is either the normal or the secret chat
                                 */

                                isSecretChat = !chatListItem.getSecretId().isEmpty();

                                secretId = chatListItem.getSecretId();
                                receiverUid = chatListItem.getReceiverUid();


                                documentId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid, secretId);


                                if (documentId.isEmpty()) {


                                    documentId = AppController.findDocumentIdOfReceiver(receiverUid, Utilities.tsInGmt(),
                                            chatListItem.getReceiverName(), chatListItem.getReceiverImage(), secretId, false,
                                            chatListItem.getReceiverIdentifier(), "", false);
                                }
                                if (isSecretChat) {

                                    Map<String, Object> chat_item = db.getParticularChatInfo(documentId);


                                    try {

                                        dTime = (long) (chat_item.get("dTime"));

                                    } catch (ClassCastException e) {

                                        dTime = (int) (chat_item.get("dTime"));

                                    }

                                }

                                try {
                                    obj.put("to", receiverUid);
                                    obj.put("toDocId", documentId);
                                    obj.put("name", AppController.getInstance().getUserName());


                                    if (isSecretChat) {








                                        /*
                                         * For secret chat exclusively
                                         */

                                        obj.put("secretId", secretId);
                                        obj.put("dTime", dTime);


                                    }


                                    HashMap<String, Object> map = new HashMap<>();
                                    map.put("messageId", obj.getString("id"));
                                    map.put("docId", documentId);


                                    AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + receiverUid, obj, 1, false, map);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    } else {

                        if (root != null) {
                            Snackbar snackbar = Snackbar.make(root, R.string.string_63, Snackbar.LENGTH_SHORT);
                            snackbar.show();
                            View view = snackbar.getView();
                            TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {


                t.printStackTrace();

            }
        });
    }


    @SuppressWarnings("unchecked,TryWithIdenticalCatches")
    private void addChats() {
        /*
         *To display the list of the chats to which message can be forwarded
         */
        final ProgressDialog pDialog = new ProgressDialog(ActivityForwardMessage.this, 0);

        pDialog.setCancelable(false);

        pDialog.setMessage(getString(R.string.Load_Chats));
        pDialog.show();

        ProgressBar bar = (ProgressBar) pDialog.findViewById(android.R.id.progress);

        bar.getIndeterminateDrawable().setColorFilter(
                ContextCompat.getColor(ActivityForwardMessage.this, R.color.color_black),
                android.graphics.PorterDuff.Mode.SRC_IN);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                mChatData.clear();
                mAdapter2.notifyDataSetChanged();
            }
        });


        boolean toSkip = false;

        if (intent.getExtras().getInt("messageType") == 10) {

            toSkip = true;

        }
        CouchDbController db = AppController.getInstance().getDbController();

        Map<String, Object> map = db.getAllChatDetails(AppController.getInstance().getChatDocId());

        if (map != null) {

            ArrayList<String> receiverUidArray = (ArrayList<String>) map.get("receiverUidArray");
            ArrayList<String> receiverDocIdArray = (ArrayList<String>) map.get("receiverDocIdArray");

            Map<String, Object> chat_item;

            ChatListItem chat;

            ArrayList<Map<String, Object>> chats = new ArrayList<>();
            for (int i = 0; i < receiverUidArray.size(); i++) {
                chats.add(db.getParticularChatInfo(receiverDocIdArray.get(i)));
                Collections.sort(chats, new TimestampSorter());

            }
            for (int i = 0; i < chats.size(); i++) {
                chat_item = chats.get(i);

//                if (((chat_item.get("selfUid")).equals(AppController.getInstance().getActiveReceiverId()) && (chat_item.get("secretId")).
//                        equals(AppController.getInstance().getActiveSecretId()))
//                        || ((chat_item.get("selfUid")).equals(AppController.getInstance().getActiveReceiverId())&&
//                        ((String)chat_item.get("secretId")).isEmpty()))

                /*
                 * To avoid forwarding into same chat to which the message being forwarded belongs
                 */

                if (((chat_item.get("selfUid")).equals(AppController.getInstance().getActiveReceiverId()) && (chat_item.get("secretId")).
                        equals(AppController.getInstance().getActiveSecretId()))
                        ) {
                    continue;
                }

                chat = new ChatListItem();

                try{
                    chat.setInitiated((Boolean) chat_item.get("initiated"));
                    chat.setMatchedUser((boolean) chat_item.get("isMatched"));
                    chat.setSuperlikedMe((boolean)chat_item.get("isSuperlikedMe"));
                }catch (Exception e){}
                boolean hasNewMessage = (Boolean) chat_item.get("hasNewMessage");
                chat.sethasNewMessage(hasNewMessage);

                try {
                    if (((String) chat_item.get("secretId")).isEmpty()) {
                        chat.setSecretChat(false);
                        chat.setSecretId("");

                    } else {
                        if (toSkip) {
                            continue;
                        }
                        chat.setSecretChat(true);
                        chat.setSecretId((String) chat_item.get("secretId"));
                    }
                } catch (NullPointerException e) {

                    chat.setSecretChat(false);
                    chat.setSecretId("");
                }
                if (hasNewMessage) {
                    chat.setNewMessageTime((String) chat_item.get("newMessageTime"));
                    chat.setNewMessage((String) chat_item.get("newMessage"));
                    chat.setNewMessageCount((String) chat_item.get("newMessageCount"));
                    chat.setShowTick(false);
                } else {

                    Map<String, Object> map2 = db.getLastMessageDetails((String) chat_item.get("selfDocId"));

                    String time = (String) map2.get("lastMessageTime");

                    String message = (String) map2.get("lastMessage");

                    chat.setShowTick((boolean) map2.get("showTick"));
                    if ((boolean) map2.get("showTick"))
                    {
                        chat.setTickStatus((int) map2.get("tickStatus"));

                    }
                    chat.setNewMessageTime(time);
                    chat.setNewMessage(message);

                    chat.setNewMessageCount("0");

                    if (message == null) {
                        chat.setNewMessage("");
                    }
                }

                chat.setReceiverIdentifier((String) chat_item.get("receiverIdentifier"));
                chat.setDocumentId((String) chat_item.get("selfDocId"));

                chat.setReceiverUid((String) chat_item.get("selfUid"));
                chat.setMatchedUser((boolean) chat_item.get("isMatched"));

                {
                    /*
                     *Given chat is the normal or the secret chat
                     */
                    /*
                     * If the uid exists in contacts
                     */
                    chat.setGroupChat(false);

                    chat.setReceiverInContacts(false);
                    /*
                     * If userId doesn't exists in contact
                     */
                    //Added name insted of Identifier.
                    chat.setReceiverName((String) chat_item.get("receiverName"));
                    //chat.setReceiverName((String) chat_item.get("receiverIdentifier"));
                    //  chat.setReceiverImage((String) chat_item.get("receiverImage"));
                    String image = (String) chat_item.get("receiverImage");
                    if (image != null && !image.isEmpty()) {
                        chat.setReceiverImage(image);
                    } else {

                        chat.setReceiverImage("");
                    }
                }
                mChatData.add(chat);
                mAdapter.notifyDataSetChanged();
            }
            //   recyclerViewChats.scrollToPosition(0);
        }

        if (mChatData.size() == 0) {

            chatsHeader.setVisibility(View.GONE);

        } else {
            hasAtleastOneChat = true;
        }
        if (pDialog.isShowing()) {


            Context context = ((ContextWrapper) (pDialog).getContext()).getBaseContext();


            if (context instanceof Activity) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                        pDialog.dismiss();
                    }
                } else {

                    if (!((Activity) context).isFinishing()) {
                        pDialog.dismiss();
                    }
                }
            } else {

                try {
                    pDialog.dismiss();
                } catch (final IllegalArgumentException e) {
                    e.printStackTrace();

                } catch (final Exception e) {
                    e.printStackTrace();

                }
            }
        }
    }


    private void manipulateAdapterContents() {


        if (showingAllContacts) {

            /*
             * To hide all contacts
             */


            mCollapsedContactData.clear();

            /*
             * Have intentionally commented out onc e time initialization as contens of mContactData might change later as well.
             */
            int size;

            if (mContactData.size() > 2) {

                viewAllHide_tv.setText(getString(R.string.ViewAll));
                size = 2;
            } else {


                size = 1;

                viewAll_rl.setVisibility(View.GONE);
            }


            for (int i = 0; i < size; i++) {
                try {
                    mCollapsedContactData.add(mContactData.get(i));
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }


            mAdapter = new Forward_ContactsAdapter(ActivityForwardMessage.this, mCollapsedContactData,typeFaceManager);
            recyclerViewContacts.setAdapter(mAdapter);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    mAdapter.notifyDataSetChanged();

                }
            });

            viewAllHide_tv.setText(getString(R.string.ViewAll));


        } else {
            /*
             * To show all groups
             */
            mAdapter = new Forward_ContactsAdapter(ActivityForwardMessage.this, mContactData,typeFaceManager);
            recyclerViewContacts.setAdapter(mAdapter);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    mAdapter.notifyDataSetChanged();

                }
            });

            viewAllHide_tv.setText(getString(R.string.Hide));

        }


        showingAllContacts = !showingAllContacts;
    }


    /*
     * To remove the blocked user from the list to whom the message can be forwarded
     *
     *
     * As of now,only feature to remove a contact from the list is there,not the option to update the list incase you are unblocked
     *
     */


    private void removeFromContactsAndChats(final String opponentId) {


        /*
         * For removing from the contacts list
         */

        for (int i = 0; i < mContactData.size(); i++) {

            if (mContactData.get(i).getContactUid().equals(opponentId)) {


                mContactData.remove(i);

                final int k = i;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (k == 0) {

                            mAdapter.notifyDataSetChanged();
                        } else {

                            mAdapter.notifyItemRemoved(k);
                        }
                    }
                });

                break;
            }
        }

        /*
         *From removing from the list of the contacts
         */

        /*
         * CAN'T SIMPLY REMOVE ITEMS ONE AT A TIME AS IT FAILS IF TWO SUCCESIVE CHATS FROM SAME USER
         */
        boolean exists = false;


        ArrayList<ChatListItem> chatList = new ArrayList<>();

        for (int i = 0; i < mChatData.size(); i++) {

            if (!mChatData.get(i).getReceiverUid().equals(opponentId)) {


                chatList.add(mChatData.get(i));

                exists = true;
            }

        }

        if (exists) {
            mChatData.clear();
            mChatData.addAll(chatList);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mAdapter2.notifyDataSetChanged();

                }
            });
        }

    }

    @Override
    public void onBackPressed() {

        try{
            if (AppController.getInstance().isActiveOnACall()) {
                if (AppController.getInstance().isCallMinimized()) {
                    super.onBackPressed();
                    supportFinishAfterTransition();
                }
            } else {
                super.onBackPressed();
                supportFinishAfterTransition();
            }}catch(Exception e){e.printStackTrace();}

    }
}
