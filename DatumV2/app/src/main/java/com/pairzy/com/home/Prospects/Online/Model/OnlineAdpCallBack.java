package com.pairzy.com.home.Prospects.Online.Model;

public interface OnlineAdpCallBack
{
    void openProfile(int position);

    void tryLoadAgain();
}
