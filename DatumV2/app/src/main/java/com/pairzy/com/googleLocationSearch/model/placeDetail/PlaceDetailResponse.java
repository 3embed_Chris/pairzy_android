package com.pairzy.com.googleLocationSearch.model.placeDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 23/5/18.
 */

public class PlaceDetailResponse {

    @SerializedName("result")
    @Expose
    private PlaceResult placeResult;
    @SerializedName("status")
    @Expose
    private String status;

    public PlaceResult getResult() {
        return placeResult;
    }

    public void setResult(PlaceResult result) {
        this.placeResult = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
