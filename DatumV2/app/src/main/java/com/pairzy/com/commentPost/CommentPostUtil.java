package com.pairzy.com.commentPost;

import android.app.Activity;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.commentPost.model.CommentPostAdapter;
import com.pairzy.com.commentPost.model.CommentPostDataPojo;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class CommentPostUtil {

    public static final String COMMENT_LAYOUT_MANAGER ="comment_layout_manager" ;
    public static final String COMMENT_POST = "comment_post";

    @Named(COMMENT_POST)
    @Provides
    @ActivityScoped
    ArrayList<CommentPostDataPojo> provideCommentPostPojos()
    {
        return  new ArrayList<>();
    }

    @Provides
    @ActivityScoped
    CommentPostAdapter getCommentPostAdapter(@Named(COMMENT_POST)ArrayList<CommentPostDataPojo> list, TypeFaceManager typeFaceManager, PreferenceTaskDataSource dataSource)
    {
        return new CommentPostAdapter(list,typeFaceManager,dataSource);
    }

    @Named(CommentPostUtil.COMMENT_LAYOUT_MANAGER)
    @Provides
    @ActivityScoped
    LinearLayoutManager provideLinearLayoutManager(Activity activity)
    {
        return  new LinearLayoutManager(activity);
    }

}
