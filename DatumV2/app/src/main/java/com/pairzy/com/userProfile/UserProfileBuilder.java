package com.pairzy.com.userProfile;

import android.app.Activity;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.userProfile.ImageItem.ImageItemBuilder;
import com.pairzy.com.userProfile.ImageItem.ImageItemFrg;
import com.pairzy.com.userProfile.VideoItem.VideoItemBuilder;
import com.pairzy.com.userProfile.VideoItem.VideoItemFrg;
import javax.inject.Named;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
/**
 *<h2>UserProfileBuilder</h2>
 * <P>
 *     This is the dagger builder class for the UserProfile page
 *     to build the page with the dagger.
 * </P>*/
@Module
public abstract class UserProfileBuilder
{
    public static final String USER_FRAGMENT_MANAGER = "UserProfile.FragmentManager";
    @ActivityScoped
    @Binds
    abstract Activity getActivity(UserProfilePage activity);

    @ActivityScoped
    @Binds
    abstract UserProfileContract.View getProfileView(UserProfilePage activity);

    @ActivityScoped
    @Binds
    abstract UserProfileContract.Presenter getUpdateProfilePresenter(UserProfilePresenter presenter);

    @Provides
    @Named(USER_FRAGMENT_MANAGER)
    @ActivityScoped
    static FragmentManager activityFragmentManager(Activity activity)
    {
        return ((AppCompatActivity)activity).getSupportFragmentManager();
    }

    @FragmentScoped
    @ContributesAndroidInjector(modules = {ImageItemBuilder.class})
    abstract ImageItemFrg getImageItemFrg();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {VideoItemBuilder.class})
    abstract VideoItemFrg getVideoItemFrg();
}
