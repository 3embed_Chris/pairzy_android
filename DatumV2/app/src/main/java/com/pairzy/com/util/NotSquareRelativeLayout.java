package com.pairzy.com.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by ankit on 24/4/18.
 */

public class NotSquareRelativeLayout extends RelativeLayout {

    public NotSquareRelativeLayout(Context context) {
        super(context);
    }

    public NotSquareRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NotSquareRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public NotSquareRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Set a square layout.
        super.onMeasure(widthMeasureSpec, widthMeasureSpec+20);
    }

}
