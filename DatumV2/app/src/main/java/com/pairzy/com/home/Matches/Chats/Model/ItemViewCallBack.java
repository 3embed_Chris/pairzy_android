package com.pairzy.com.home.Matches.Chats.Model;

import android.view.View;

interface ItemViewCallBack
{
    void onViewItemCallBack(View view, int position);
}
