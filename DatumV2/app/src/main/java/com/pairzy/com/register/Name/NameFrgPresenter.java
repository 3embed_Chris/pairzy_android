package com.pairzy.com.register.Name;

import android.app.Activity;
import com.pairzy.com.R;
import javax.inject.Inject;
/**
 * <h2>NameFrgPresenter</h2>
 * <P>
 *
 * </P>
 * @since  2/16/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class NameFrgPresenter implements NameContract.Presenter
{
    @Inject
    Activity activity;

    private NameContract.View view;
    @Inject
    NameModel nameModel;

    @Inject
    NameFrgPresenter()
    {}


    @Override
    public void takeView(NameContract.View view) {
        this.view= view;
    }

    @Override
    public void dropView()
    {
      this.view=null;
    }

    @Override
    public void validateName(String name)
    {
        if(nameModel.isValidName(name))
        {
              if(view!=null)
                  view.onValidName(name);
        }else
        {
            if(view!=null)
                view.invalidateName(activity.getString(R.string.invalid_name));
        }
    }
}
