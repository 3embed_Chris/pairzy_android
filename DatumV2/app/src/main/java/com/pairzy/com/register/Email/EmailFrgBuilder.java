package com.pairzy.com.register.Email;

import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;
/**
 * <h2>EmailFrgBuilder</h2>
 * <P>
 *
 * </P>
 * @since  2/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface EmailFrgBuilder
{
    @FragmentScoped
    @Binds
     EmailFragment getEmailFragment(EmailFragment emailFragment);

    @FragmentScoped
    @Binds
    EmailFrgContract.Presenter taskPresenter(EmailFrgPresenter presenter);
}
