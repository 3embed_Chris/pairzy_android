package com.pairzy.com.mobileverify.otp;
import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;
/**
 * <h1>OtpDaggerModule</h1>
 * <p>
 *
 * </p>
 * @author 3Embed
 * @since 13/08/17
 */
@Module
public abstract class OtpDaggerModule
{
    @FragmentScoped
    @Binds
    abstract Otp_Fragment phnoFragment(Otp_Fragment phnoFragment);

    @FragmentScoped
    @Binds
    abstract OtpContract.Presenter taskPresenter(OtpPresenter presenter);

}