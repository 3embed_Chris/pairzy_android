package com.pairzy.com.home.Matches.Chats;

import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

@Module
public interface ChatsFragModule {

    @FragmentScoped
    @Binds
    ChatsFragment getChatsFragment(ChatsFragment chatsFragment);

    @FragmentScoped
    @Binds
    ChatsFrgContract.Presenter chatFragPresenter(ChatsFrgPresenter presenter);
}
