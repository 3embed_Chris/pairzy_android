package com.pairzy.com.data.model.appversion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppVersionData {

    @SerializedName("appversion")
    @Expose
    private String appversion;
    @SerializedName("isMandatory")
    @Expose
    private Boolean isMandatory;

    public String getAppversion() {
        return appversion;
    }

    public void setAppversion(String appversion) {
        this.appversion = appversion;
    }

    public Boolean getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(Boolean isMandatory) {
        this.isMandatory = isMandatory;
    }
}