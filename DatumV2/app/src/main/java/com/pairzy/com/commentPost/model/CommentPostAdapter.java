package com.pairzy.com.commentPost.model;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;
import java.util.Date;

public class CommentPostAdapter extends RecyclerView.Adapter {


    private PreferenceTaskDataSource dataSource;
    private TypeFaceManager typeFaceManager;
    private ArrayList<CommentPostDataPojo> list;
    private  Context context;
    private ItemViewCallBack callBack;
    private CommentItemCallBack adapterItemCallback;

    public void setItemCallback(CommentItemCallBack  callback) {
        this.adapterItemCallback=callback;
    }

    public CommentPostAdapter(ArrayList<CommentPostDataPojo> list, TypeFaceManager typeFaceManager,PreferenceTaskDataSource dataSource)
    {
        this.dataSource=dataSource;
        this.typeFaceManager=typeFaceManager;
        this.list=list;
        intiCallback();
    }

    void intiCallback()
    {
        callBack= (view, position) -> {
//            switch (view.getId()) {
//                case R.id.CommentDelete:
//                    if (adapterItemCallback != null)
//                        adapterItemCallback.OnDelete(position);
//                    break;
//                case R.id.CommentEdit:
//                    if (adapterItemCallback != null)
//                        adapterItemCallback.onEdit(position);
//                    break;
//
//            }
        };

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_post_item,parent,false);
        context=parent.getContext();
        return new CommentPostViewHolder(view,callBack);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        CommentPostViewHolder viewHolder= (CommentPostViewHolder) holder;

        String sourceString = "<b>" + list.get(i).getCommenterName() + "</b> " + "<font size=\"-2\">"+list.get(i).getComment()+"</font>";

        viewHolder.userComment.setText(Html.fromHtml(sourceString));

        Glide.with(context)
                .load(list.get(i).getProfilePic())
                .transform(new CircleCrop())
                .into(viewHolder.userProfile);

        viewHolder.userCommentTime.setText(getdate(list.get(i).getCommentedOn()));
        /*if(dataSource.getUserId().equalsIgnoreCase(list.get(i).getPhoneNo())) {
            viewHolder.editComment.setVisibility(View.VISIBLE);
            viewHolder.deleteComment.setVisibility(View.VISIBLE);
        } else
        {
            viewHolder.editComment.setVisibility(View.GONE);
            viewHolder.deleteComment.setVisibility(View.GONE);
        }*/
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public String getdate(String stringDate)
    {
        Date startDate=new Date(Long.parseLong(stringDate));
        Date endDate = new Date();
        long different = endDate.getTime() - startDate.getTime();


        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        if(elapsedDays>0)
            return elapsedDays +" d";
        else if(elapsedHours>0)
            return elapsedHours+ " hr";
        else if(elapsedMinutes>0)
            return elapsedMinutes + " min";
        else return elapsedSeconds+ " sec";
    }


}
