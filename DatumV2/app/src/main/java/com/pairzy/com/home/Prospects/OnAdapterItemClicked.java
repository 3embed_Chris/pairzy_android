package com.pairzy.com.home.Prospects;

/**
 * @since  4/27/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface OnAdapterItemClicked
{
    void openUserProfile(int position);

    void tryLoadAgain();
}
