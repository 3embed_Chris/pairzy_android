package com.pairzy.com.boostDetail;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.boostDetail.model.BoostDetailSlideAdapter;
import com.pairzy.com.boostDetail.model.CoinPlan;
import com.pairzy.com.boostDetail.model.CoinPlanAdapter;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.boostDialog.BoostDialog;
import com.pairzy.com.util.boostDialog.Slide;
import com.pairzy.com.util.progressbar.LoadingProgress;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * <h>PassportUtilModule class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

@Module
public class BoostUtilModule {

    @ActivityScoped
    @Provides
    BoostDetailSlideAdapter provideDetailSlideAdapter(ArrayList<Slide> slideArrayList, BoostDetailActivity activity, TypeFaceManager typeFaceManager){
        return new BoostDetailSlideAdapter(typeFaceManager,activity,slideArrayList);
    }

    @Provides
    @ActivityScoped
    BoostDialog getBootsDialog(Activity activity, TypeFaceManager typeFaceManager, PreferenceTaskDataSource dataSource, Utility utility)
    {
        return new BoostDialog(activity,typeFaceManager,dataSource,utility);
    }

    @ActivityScoped
    @Provides
    LinearLayoutManager provideLinearLayoutManager(Activity activity){
        return new LinearLayoutManager(activity);
    }

    @ActivityScoped
    @Provides
    CoinPlanAdapter provideCoinPlanAdapter(ArrayList<CoinPlan> coinPlanList,TypeFaceManager typeFaceManager){
        return new CoinPlanAdapter(coinPlanList,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }

}
