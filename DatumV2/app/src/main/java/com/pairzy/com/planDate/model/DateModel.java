package com.pairzy.com.planDate.model;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.DateEvent;
import com.pairzy.com.data.model.ErrorMessageResponse;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigResponse;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.Model.DateType;
import com.pairzy.com.home.Dates.Pending_page.PendingFrgPresenter;
import com.pairzy.com.home.Discover.Model.superLike.SuperLikeResponse;
import com.pairzy.com.planDate.SelectedLocationHolder;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

/**
 *<h>CallDateModel class</h>
 * @author 3Embed.
 * @since 8/5/18.
 * @version 1.0.
 */

public class DateModel extends BaseModel{

    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Utility utility;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    private boolean dialogNeedToShow;


    @Inject
    DateModel(){
    }

    public boolean isMissingData(String userId, Long selectedTime, SelectedLocationHolder holder){

        if(userId == null || userId.isEmpty())
            return true;
        if(selectedTime == 0)
            return true;
        if(holder == null || holder.getLocationTitle() == null || holder.getLocationTitle().isEmpty())
            return true;
        return false;
    }

    public boolean isMissingData(SelectedLocationHolder holder){
        if(holder.getLocationTitle() == null || holder.getLocationTitle().isEmpty())
            return true;
        return false;
    }

    public Map<String, Object> getBodyMap(String userId, Long selectedTime, SelectedLocationHolder locationHolder){
        Map<String,Object> mapBody = new HashMap();
        mapBody.put("targetUserId",userId);
        mapBody.put("dateType", DateType.IN_PERSON_DATE.getValue());
        mapBody.put("proposedOn",selectedTime);
        mapBody.put("longitude",locationHolder.getLongitude());
        mapBody.put("latitude",locationHolder.getLatitude());
        mapBody.put("placeName",locationHolder.getLocationTitle());
        return mapBody;
    }

    public void saveEventId(DateEvent dateEvent) {
        ArrayList<DateEvent> savedEventList =  getEventId();
        if(!savedEventList.contains(dateEvent))
            savedEventList.add(dateEvent);

        try {
            ArrayList<String> finalEventList = new ArrayList<>();
            for (DateEvent dateEvent1 : savedEventList) {
                String json = utility.getGson().toJson(dateEvent1, DateEvent.class);
                finalEventList.add(json);
            }
            dataSource.setEvents(finalEventList);
        }catch (Exception e){}
    }

    private ArrayList<DateEvent> getEventId(){
        ArrayList<String> jsonEventList = dataSource.getEvents();
        ArrayList<DateEvent> dateEvents = new ArrayList<>();
        try {
            for (String json : jsonEventList) {
                DateEvent dateEvent = utility.getGson().fromJson(json, DateEvent.class);
                if (dateEvent != null)
                    dateEvents.add(dateEvent);
            }
        }catch (Exception e){}
        return dateEvents;
    }

    public String checkIfReminderExist(String userId) {
        ArrayList<DateEvent> savedEventList =  getEventId();
        boolean found = false;
        String eventId = "";
        for(DateEvent dateEvent : savedEventList){
            if(dateEvent.getUserId().equals(userId)){
                found = true;
                eventId = dateEvent.getEventId();
            }
        }
        if(found)
            return eventId;
        return "";
    }

    public boolean isDialogNeedToShow(String rescheduleTag) {
        if(rescheduleTag != null && rescheduleTag.equals(PendingFrgPresenter.RESCHEDULE_TAG)){
            return dataSource.getShowRescheduleDateCoinDialog();
        }
        else{
            return dataSource.getShowDateCoinDialog();
        }
    }

    public void parseCoinConfig(String response) {
        try{
            CoinConfigResponse coinConfigResponse =
                    utility.getGson().fromJson(response,CoinConfigResponse.class);
            if(!coinConfigResponse.getData().isEmpty())
                coinConfigWrapper.setCoinData(coinConfigResponse.getData().get(0));
        }catch (Exception e){}
    }

    public void updateRescheduleSpendCoinShowPref(boolean show) {
        dataSource.setShowRescheduleDateCoinDialog(show);
    }
    public void updateSpendCoinShowPref(boolean show) {
        dataSource.setShowDateCoinDialog(show);
    }

    public void parsePlanDateResponse(String response) {
        try
        {
            SuperLikeResponse superLikeResponse = utility.getGson().fromJson(response, SuperLikeResponse.class);
            if(superLikeResponse.getCoinWallet() != null) {
                coinBalanceHolder.setCoinBalance(String.valueOf(superLikeResponse.getCoinWallet().getCoin()));
                coinBalanceHolder.setUpdated(true);
            }
        }catch (Exception e){
            e.getMessage();
        }
    }

    public String getErrorMessage(String response) {
        ErrorMessageResponse errorMessageResponse = utility.getGson().fromJson(response, ErrorMessageResponse.class);
        return errorMessageResponse.getMessage();
    }

    public boolean isEnoughWalletBalance() {
        if(coinConfigWrapper.getCoinData() != null && coinBalanceHolder.getCoinBalance() != null){
            int requiredCoin = coinConfigWrapper.getCoinData().getInPersonDateInitiated().getCoin();
            int walletBalance = Integer.parseInt(coinBalanceHolder.getCoinBalance());
            if(requiredCoin <= walletBalance){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }
}
