package com.pairzy.com.editProfile.ProfileAlert;
/**
 * <h2>ChatMenuCallback</h2>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface ProfileAlertCallBack
{
    void onSetProfile(int pos);
    void onCancel();
}
