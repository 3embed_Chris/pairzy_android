package com.pairzy.com.util.Countrypicker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.util.TypeFaceManager;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import javax.inject.Inject;
/**
 *<h2>CountryPicker</h2>
 * <P>
 *
 * </P>
 * @author 3Embed.
 * * @since 17.01.2017.
 * @version 1.0.
 * */
@ActivityScoped
public class CountryPicker extends DialogFragment implements Comparator<Country>
{
    List<Country> allCountriesList;
    List<Country> selectedCountriesList;

    @Inject
    TypeFaceManager typeFaceManager;

    @Inject
    Context context;

    private CountryPickerListener listener;

    @Inject
    public CountryPicker(){}
    /**
     * To support show as dialog
     */
    public static CountryPicker newInstance(String dialogTitle)
    {
        CountryPicker picker = new CountryPicker();
        Bundle bundle = new Bundle();
        bundle.putString("dialogTitle", dialogTitle);
        picker.setArguments(bundle);
        return picker;
    }

    /*
    * Setting the listener.*/
    public void setListener(CountryPickerListener listener)
    {
        this.listener = listener;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        @SuppressLint("InflateParams")
        View view = inflater.inflate(R.layout.country_picker, null);
        Window window= getDialog().getWindow();
        assert window != null;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        window.requestFeature(Window.FEATURE_NO_TITLE);
        collectAllCountries();
        ListView countryListView = view.findViewById(R.id.country_code_picker_listview);
        CountryListAdapter adapter = new CountryListAdapter(getActivity(), selectedCountriesList,typeFaceManager);
        countryListView.setAdapter(adapter);
        countryListView.setOnItemClickListener((parent, view1, position, id) -> {
            if (listener != null)
            {
                Country country = selectedCountriesList.get(position);
                listener.onSelectCountry(country.getName(), country.getCode(), country.getDial_code(),
                        country.getFlag(), Integer.parseInt(country.getMax_digits()));
            }
        });
        return view;
    }

    /*
    *Get the country phno number code */
    public static Currency getCurrencyCode(String countryCode)
    {
        try {
            return Currency.getInstance(new Locale("en", countryCode));
        } catch (Exception ignored) {
        }
        return null;
    }
    /*
     *Decode the json stirng */
    private static String readEncodedJsonString() throws java.io.IOException
    {
        byte[] data = Base64.decode(Constants.ENCODED_COUNTRY_CODE, Base64.DEFAULT);
        return new String(data, "UTF-8");
    }
    /**
     *Collecting all the country list. */
    private void collectAllCountries()
    {
        if (allCountriesList == null) {
            try {
                allCountriesList = new ArrayList<>();
                String allCountriesCode = readEncodedJsonString();
                JSONArray countryArray = new JSONArray(allCountriesCode);
                for (int i = 0; i < countryArray.length(); i++) {
                    JSONObject jsonObject = countryArray.getJSONObject(i);
                    String countryName = jsonObject.getString("name");
                    String countryDialCode = jsonObject.getString("dial_code");
                    String countryCode = jsonObject.getString("code");
                    String min_digits = jsonObject.getString("min_digits");
                    String max_digits = jsonObject.getString("max_digits");
                    Country country = new Country();
                    country.setCode(countryCode);
                    country.setName(countryName);
                    country.setDial_code(countryDialCode);
                    country.setMin_digits(min_digits);
                    country.setMax_digits(max_digits);
                    allCountriesList.add(country);
                }
                Collections.sort(allCountriesList, this);
                selectedCountriesList = new ArrayList<>();
                selectedCountriesList.addAll(allCountriesList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int compare(Country lhs, Country rhs)
    {
        return lhs.getName().compareTo(rhs.getName());
    }

    public Country getUserCountryInfo(Context context)
    {
        this.context = context;
        collectAllCountries();
        String countryIsoCode;
        Country country;
        TelephonyManager telephonyManager =
                (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        assert telephonyManager != null;
        if (!(telephonyManager.getSimState() == TelephonyManager.SIM_STATE_ABSENT)) {
            countryIsoCode = telephonyManager.getSimCountryIso();
            for (int i = 0; i < allCountriesList.size(); i++)
            {
                Country country_temp = allCountriesList.get(i);
                if (country_temp.getCode().equalsIgnoreCase(countryIsoCode))
                {
                    country=country_temp;
                    country.setFlag(getFlagResId(country_temp.getCode()));
                    country.setMax_digits(country_temp.getMax_digits());
                    return country;
                }
            }
        }
        return indian();
    }

    /*
    * Setting the max digit for the country*/
    private Country indian()
    {
        Country country = new Country();
        country.setCode("IN");
        country.setName("India");
        country.setDial_code("+91");
        country.setMax_digits("10");
        country.setFlag(R.drawable.flag_in);
        return country;
    }

    private int getFlagResId(String drawable)
    {
        try {
            return context.getResources()
                    .getIdentifier("flag_" + drawable.toLowerCase(Locale.ENGLISH), "drawable",
                            context.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void getFlagByCountryCode(String countryCode) {
        for(Country country: allCountriesList){
            if(country.getDial_code().equals(countryCode)){
                if(listener != null)
                    listener.onSelectCountry(country.getName(),country.getCode(),country.getDial_code(),getFlagResId(country.getCode()),Integer.parseInt(country.getMax_digits()));
                break;
            }
        }
    }
}
