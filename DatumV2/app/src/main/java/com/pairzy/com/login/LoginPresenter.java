package com.pairzy.com.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;

import com.pairzy.com.R;
import com.pairzy.com.data.model.appversion.AppVersionData;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.fbRegister.FbRegisterPage;
import com.pairzy.com.mobileverify.MobileVerifyActivity;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.networking.RxNetworkObserver;
import com.pairzy.com.register.RegisterPage;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.accountKit.AccountKitManager;
import com.pairzy.com.util.accountKit.AccountKitManagerImpl;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.facebookmanager.com.FacebookManager;
import com.facebookmanager.com.FacebookUserDetails;
import com.facebookmanager.com.RxJava2FBObservable;

import java.io.IOException;
import java.util.Objects;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * <h2>LoginPresenter</h2>
 * <P>Implementation of {@link LoginContract.Presenter}</P>
 * @author 3Embed.
 * @version 1.0.
 * @see LoginContract.Presenter
 * @since 04/01/2018.
 */

public class LoginPresenter implements LoginContract.Presenter
{
    private static final String TAG = LoginPresenter.class.getName();

    @Inject
    NetworkStateHolder holder;
    @Inject
    RxNetworkObserver rxNetworkObserver;
    @Inject
    Activity activity;
    @Inject
    LoadingProgress progress;
    @Inject
    FacebookManager facebookManager;
    @Inject
    NetworkService service;
    @Inject
    LoginContract.View view;
    @Inject
    LoginModel model;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    AccountKitManager accountKitManager;

    private CompositeDisposable compositeDisposable;
    private boolean isFbIdExist = false;
    private String currentCountryCode;
    private String currentPhone;

    @Inject
    public LoginPresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }


    @Override
    public void initNetworkObserver()
    {
        Observer<NetworkStateHolder> observer = new Observer<NetworkStateHolder>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {
                compositeDisposable.add(d);
            }
            @Override
            public void onNext(NetworkStateHolder value)
            {
                view.updateInterNetStatus(value.isConnected());
            }
            @Override
            public void onError(Throwable e)
            {
                e.printStackTrace();
            }
            @Override
            public void onComplete()
            {}
        };
        rxNetworkObserver.getObservable()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Override
    public void initFacebook()
    {
        if(!holder.isConnected())
        {
            view.showMessage(activity.getString(R.string.no_internet_error));
        }else
        {
            progress.show();
            facebookManager.logout();
            facebookManager.faceBook_Login(activity, new FacebookManager.Facebook_callback() {
                @Override
                public void success(String id)
                {
                    callCheckFbIdExistApi(id);
                }
                @Override
                public void error(String error)
                {
                    progress.cancel();
                    view.facebookError(error);
                }

                @Override
                public void cancel(String cancel)
                {
                    progress.cancel();
                    view.facebookCancel(cancel);
                }
            });
        }
    }

    private void callCheckFbIdExistApi(String fbId){
        if(holder.isConnected()){
            service.checkFbIdExist(model.getAuthorization(),model.getLanguage(),fbId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if(value.code() == 200){
                                try {
                                    String data = Objects.requireNonNull(value.body()).string();
                                    Log.d(TAG, "onSuccess: "+data);
                                    isFbIdExist = true;
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                            else{
                                isFbIdExist = false;
                            }
                            facebookUserDetails(fbId);
                        }
                        @Override
                        public void onError(Throwable e) {
                        }
                    });
        }
        else{
            if(view != null)
                view.showMessage(activity.getString(R.string.no_internet_error));
        }
    }
    @Override
    public void facebookUserDetails(String id)
    {
        if(!holder.isConnected())
        {
            progress.cancel();
            view.showMessage(activity.getString(R.string.no_internet_error));
        }else
        {
            RxJava2FBObservable observable = facebookManager.collectUserDetails(activity);
            Observer<FacebookUserDetails> observer = new Observer<FacebookUserDetails>()
            {
                @Override
                public void onSubscribe(Disposable d)
                {
                    compositeDisposable.add(d);
                }

                @Override
                public void onNext(FacebookUserDetails value)
                {
                    //Log.e(TAG, "onNext : "+value.getEmail() );
                    if(value != null) {
                        if(isFbIdExist) {
                            facebookApiCall(value);
                        }
                        else {
                            Intent intent = new Intent(activity, FbRegisterPage.class);
                            intent.putExtra("fb_data", value);
                            if (view != null)
                                view.openRegisterPage(intent);
                        }
                    }
                    else{
                        if(view != null)
                            view.showMessage("failed to fetch email from facebook!!");
                    }
                    progress.cancel();
                }

                @Override
                public void onError(Throwable e) {
                    e.printStackTrace();
                    progress.cancel();
                    if(view!=null)
                        view.showMessage(e.getMessage());
                }

                @Override
                public void onComplete() {}
            };
            observable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(observer);
        }
    }


    @Override
    public void facebookApiCall(FacebookUserDetails userDetails)
    {
        if(!holder.isConnected())
        {
            if(view != null)
                view.showMessage(activity.getString(R.string.no_internet_error));
        }else {
            Observable<Response<ResponseBody>> body = service.facebookLogin(model.getAuthorization(), model.getLanguage(),
                    model.prepareFacebookRequest(userDetails));
            body.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> data) {
                            try {
                                if (data.code() == 200) {
                                    String response_data = Objects.requireNonNull(data.body()).string();
                                    model.parseLoginResponse(response_data);
                                    if (view != null)
                                        view.facebookLoginSuccess();
                                }else {
                                    if (view != null)
                                        view.showMessage(model.getError(data));
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                                if (view != null)
                                    view.showMessage(e.getMessage());
                            } catch (DataParsingException e) {
                                if (view != null)
                                    view.showMessage(e.getMessage());

                            } catch (EmptyData e) {
                                if (view != null)
                                    view.showMessage(e.getMessage());
                            }
                            progress.cancel();
                        }

                        @Override
                        public void onError(Throwable e) {
                            progress.cancel();
                            if (view != null)
                                view.showMessage(e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
    }
    @Override
    public void loadWebActivity(String title,@NonNull String url)
    {
        if(view != null)
            view.showWebActivity(title, url);
    }

    @Override
    public void showBottomView()
    {
        view.showBottomView();
    }

    @Override
    public void hideBottomView()
    {
        view.hideBottomView();
    }

    @Override
    public void dropView() {
        compositeDisposable.clear();
    }

    @Override
    public void initPhoneLogin() {
        accountKitManager.initPhoneLogin(activity, new AccountKitManagerImpl.GetPhoneDetailCallback() {
            @Override
            public void onSuccess(String countryCode, String phone) {
                currentCountryCode =countryCode;
                currentPhone=phone;
//                if(view != null)
//                    view.showMessage(countryCode+phone);
                sendOtp(countryCode+phone);
            }

            @Override
            public void onError(String error) {
                if(view != null)
                    view.showMessage(error);
            }
        });
    }

    private void sendOtp(String mobile_number)
    {
        if(!holder.isConnected())
        {
            if(view!=null)
            {
                view.showMessage(activity.getString(R.string.no_internet_error));
            }
        }else
        {
            progress.show();
            service.requestOtp(model.getAuthorization(), model.getLanguage(), model.requestOtpParams(mobile_number, false))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> result)
                        {
                            try
                            {
                                if (result.code() == 200)
                                {
                                    callVerifyOptApi(mobile_number);
                                }
                                else
                                {
                                    progress.cancel();
                                    if(view!=null)
                                        view.showMessage(model.getError(result));
                                }
                            }catch (Exception e)
                            {
                                progress.cancel();
                                if(view!=null)
                                    view.showMessage(activity.getString(R.string.failed_sendOtp));
                            }
                        }
                        @Override
                        public void onError(Throwable errorMsg)
                        {
                            progress.cancel();
                            if(view!=null)
                                view.showMessage(activity.getString(R.string.failed_sendOtp));
                        }
                        @Override
                        public void onComplete() {}
                    });
        }
    }

    private void callVerifyOptApi(String phno) {

        service.verifyOtp(model.getAuthorization(),model.getLanguage(),model.verifyOtpData(phno, AppConfig.DEFAULT_OTP))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> result)
                    {
                        try
                        {
                            if (result.code()==200)
                            {
                                String data= Objects.requireNonNull(result.body()).string();

                                boolean isSignUp = model.parseVerificationRes(data);
                                if (isSignUp) {
                                    Bundle bundle=new Bundle();
                                    bundle.putString(RegisterPage.COUNTRY_CODE,currentCountryCode);
                                    bundle.putString(RegisterPage.MOBILE_NUMBER,currentPhone);
                                    bundle.putString(RegisterPage.OTP,AppConfig.DEFAULT_OTP);
                                    if (view != null)
                                        view.openSingUp(bundle);
                                } else {
                                    Bundle bundle=new Bundle();
                                    bundle.putString(MobileVerifyActivity.param1,currentCountryCode);
                                    bundle.putString(MobileVerifyActivity.param2,currentPhone);
                                    bundle.putString(MobileVerifyActivity.param3,AppConfig.DEFAULT_OTP);
                                    if (view != null)
                                        view.onLogin(bundle);
                                }

                            } else if(view!=null)
                                view.showMessage(model.getError(result));
                        }catch (Exception e)
                        {
                            if(view!=null)
                                view.showMessage(activity.getString(R.string.failed_VerfyOtp));
                        }
                        progress.cancel();
                    }
                    @Override
                    public void onError(Throwable errorMsg)
                    {
                        progress.cancel();
                        if(view!=null)
                            view.showMessage(activity.getString(R.string.failed_VerfyOtp));
                    }
                    @Override
                    public void onComplete() {}
                });
    }

    @Override
    public void checkForForceUpdate() {
        if(model.isNeedToCallUpdateVersionApi()) {
            service.getAppVersion()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> responseBodyResponse) {
                            if (responseBodyResponse.code() == 200) {
                                try {
                                    AppVersionData appVersionData =
                                            model.parseAppVersionData(Objects.requireNonNull(responseBodyResponse.body()).string());
                                    if (model.shouldShowUpdateDialog(appVersionData)) {
                                        if (view != null)
                                            view.showUpdateDialog(model.isMandatory(appVersionData));
                                    }else{
                                        if(view != null)
                                            view.dismissUpdateDialog();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                        }
                    });
        }else{
            if (model.shouldShowUpdateDialog()) {
                if (view != null)
                    view.showUpdateDialog(model.getSavedIsMandatory());
            }
        }
    }

    @Override
    public void takeView(LoginContract.View view) {}
}
