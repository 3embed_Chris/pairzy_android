package com.pairzy.com.MqttManager;

//import org.eclipse.paho.android.service.MqttAndroidClient;

/**
 * <h2>MattConnectionManager</h2>
 * <P>
 *  Mqtt manager manager to manage the mqtt class to manage the
 *  mqtt all functionality.
 * </P>
 * @since  on 3/9/2018.
 * @version 1.0.
 * @author 3Embed.
 */
public class MattConnectionManager /*implements MqttManager*/
{
//    private MqttAndroidClient client;
//    private HashSet<IMqttDeliveryToken> deliverTokens;
//    private MqttConnectOptions options;
//    private Context context;
//    private MqttEventCallbackCallback mqttEventCallbackCallback;
//
//    public MattConnectionManager(Context context)
//    {
//        this.context=context;
//        deliverTokens =new HashSet<>();
//        initMqttData();
//        initMqttOption();
//    }
//
//
//    /*
//     *intialization of the xml content */
//    private void initMqttData()
//    {
//        String clientId= MqttClient.generateClientId();
//        String serverUri = "tcp://"+ AppConfig.MQTT_HOST_LINK;
//        client = new MqttAndroidClient(context,serverUri,clientId,new MemoryPersistence());
//        client.setCallback(initMqttListener());
//    }
//
//
//    private void initMqttOption()
//    {
//        options = new MqttConnectOptions();
//        options.setCleanSession(false);
//        options.setAutomaticReconnect(true);
//        options.setKeepAliveInterval(30);
//    }
//
//
//    public void changeMqttVersion()
//    {
//        options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);
//    }
//
//    @Override
//    public void setCredential(String userName, String password)
//    {
//        options.setUserName(userName);
//        options.setPassword(password.toCharArray());
//    }
//
//    @Override
//    public void setWillMessage(String topic,String payload,int qos,boolean isRetain)
//    {
//        options.setWill(topic,payload.getBytes(),qos,isRetain);
//    }
//
//    /*
//     * Connect mqtt data.*/
//    @Override
//    public void connect(MqttEventCallbackCallback connectionCallback)
//    {
//        if(client ==null)
//            throw new IllegalArgumentException("Please initialize the Mqttmanager fist!");
//        this.mqttEventCallbackCallback=connectionCallback;
//
//        try
//        {
//            client.connect(options,context, new IMqttActionListener()
//            {
//                @Override
//                public void onSuccess(IMqttToken asyncActionToken)
//                {
//                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
//                    disconnectedBufferOptions.setBufferEnabled(true);
//                    disconnectedBufferOptions.setBufferSize(100);
//                    disconnectedBufferOptions.setPersistBuffer(false);
//                    disconnectedBufferOptions.setDeleteOldestMessages(false);
//                    client.setBufferOpts(disconnectedBufferOptions);
//                    printMsg("Connected mqtt");
//                }
//                @Override
//                public void onFailure(IMqttToken asyncActionToken, Throwable exception)
//                {
//                    printMsg("Failed to connect to: " +exception.getMessage());
//                    if(mqttEventCallbackCallback!=null)
//                        mqttEventCallbackCallback.onConnectionFailed(exception.getMessage());
//
//                }
//            });
//        } catch (MqttException e)
//        {
//            printMsg("Failed to connect to: " +e.getMessage());
//            e.printStackTrace();
//            if(mqttEventCallbackCallback!=null)
//                mqttEventCallbackCallback.onConnectionFailed(e.getMessage());
//        }
//    }
//
//
//    @Override
//    public void reConnect()
//    {
//        if(client ==null)
//            throw new IllegalArgumentException("Please initialize the Mqttmanager fist!");
//
//        if(client.isConnected())
//        {
//            try
//            {
//                client.connect();
//            } catch (Exception e)
//            {
//                e.printStackTrace();
//                if(mqttEventCallbackCallback!=null)
//                    mqttEventCallbackCallback.onConnectionFailed(e.getMessage());
//            }
//        }
//    }
//
//    /*
//    * Disconnect the mqtt*/
//    @Override
//    public void disconnect()
//    {
//        if(client!=null&&client.isConnected())
//        {
//            try {
//                IMqttToken disconToken = client.disconnect();
//                disconToken.setActionCallback(new IMqttActionListener() {
//                    @Override
//                    public void onSuccess(IMqttToken asyncActionToken) {}
//                    @Override
//                    public void onFailure(IMqttToken asyncActionToken,Throwable exception)
//                    {}
//                });
//            } catch (MqttException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//    /**
//     *initialization of the MQtt call back.*/
//    private MqttCallbackExtended initMqttListener()
//    {
//        return new MqttCallbackExtended() {
//            @Override
//            public void connectComplete(boolean reconnect, String serverURI)
//            {
//                printMsg(""+reconnect+" "+serverURI);
//                if(mqttEventCallbackCallback!=null)
//                    mqttEventCallbackCallback.onMQttConnected(reconnect);
//            }
//
//            @Override
//            public void connectionLost(Throwable cause)
//            {
//                if(cause!=null)
//                {
//                    printMsg(""+cause.getMessage());
//                    if(mqttEventCallbackCallback!=null)
//                        mqttEventCallbackCallback.onConnectionFailed(cause.getMessage());
//                }else
//                {
//                    printMsg("Disconnected");
//                }
//
//            }
//            @Override
//            public void messageArrived(String topic, MqttMessage message) throws Exception
//            {
//                handelMessageReceive(topic,message);
//            }
//            @Override
//            public void deliveryComplete(IMqttDeliveryToken token)
//            {
//                onDelivered(token);
//            }
//        };
//    }
//
//
//    @Override
//    public boolean canPublish()
//    {
//        if(client==null)
//            throw new IllegalArgumentException("Initialize the Mqtt manager first!");
//        return client.isConnected();
//    }
//
//
//    @Override
//    public void publishData(String topic,String payload,boolean isRetain,int qos)
//    {
//        byte[] encodedPayload;
//        try {
//            encodedPayload = payload.getBytes("UTF-8");
//            MqttMessage message = new MqttMessage(encodedPayload);
//            message.setRetained(isRetain);
//            message.setQos(qos);
//            IMqttDeliveryToken token=client.publish(topic,message);
//            deliverTokens.add(token);
//        } catch (UnsupportedEncodingException | MqttException e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void publishData(String topic,String payload,boolean isRetain)
//    {
//        byte[] encodedPayload;
//        try {
//            encodedPayload = payload.getBytes("UTF-8");
//            MqttMessage message = new MqttMessage(encodedPayload);
//            message.setRetained(isRetain);
//            message.setQos(1);
//            client.publish(topic,message);
//        } catch (UnsupportedEncodingException | MqttException e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//    @Override
//    public void publishData(String topic,String payload)
//    {
//        byte[] encodedPayload;
//        try {
//            encodedPayload = payload.getBytes("UTF-8");
//            MqttMessage message = new MqttMessage(encodedPayload);
//            message.setRetained(false);
//            message.setQos(1);
//            client.publish(topic,message);
//        } catch (UnsupportedEncodingException | MqttException e)
//        {
//            e.printStackTrace();
//        }
//    }
//
//    /*
//     *Subscribe the topic */
//    public void subscribe(String topic,int qos)
//    {
//        try {
//            if (client != null)
//            {
//                client.subscribe(topic, qos);
//            }
//        } catch (MqttException e)
//        {
//            e.printStackTrace();
//        }
//        printMsg(""+topic);
//    }
//
//    /*
//    * Unsubscribe the toipc*/
//    @Override
//    public void unSubscribe(String topic)
//    {
//        try {
//            if (client != null) {
//                client.unsubscribe(topic);
//            }
//        } catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//    }
//    /*
//     *Decoding the response bite from the mqtt. */
//    private void handelMessageReceive(String topic,MqttMessage message)
//    {
//        JSONObject pay_load=convertMessageToJsonObject(message);
//        printMsg(""+topic+""+pay_load);
//        if(mqttEventCallbackCallback!=null)
//            mqttEventCallbackCallback.
// (topic,pay_load);
//    }
//
//    /*
//     * Converting the message to json object.*/
//    private JSONObject convertMessageToJsonObject(MqttMessage message) {
//
//        JSONObject obj = new JSONObject();
//        try {
//
//            obj = new JSONObject(new String(message.getPayload()));
//        } catch (JSONException e)
//        {
//            e.printStackTrace();
//        }
//        return obj;
//    }
//
//    /*
//     *on Message delivered.*/
//    private void onDelivered(IMqttDeliveryToken token)
//    {
//        deliverTokens.add(token);
//        if(mqttEventCallbackCallback!=null)
//            mqttEventCallbackCallback.onDelivered(token);
//    }
//
//    /*
//     *Print the log for mqtt connection */
//    private void printMsg(String messae)
//    {
//        if(BuildConfig.DEBUG)
//        {
//            Log.d("MqttPrint",""+messae);
//        }
//    }
//
}
