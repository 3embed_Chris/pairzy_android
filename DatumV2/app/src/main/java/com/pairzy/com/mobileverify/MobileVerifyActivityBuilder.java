package com.pairzy.com.mobileverify;

import android.app.Activity;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.mobileverify.main.PhnoDaggerModule;
import com.pairzy.com.mobileverify.main.PhnoFragment;
import com.pairzy.com.mobileverify.otp.OtpDaggerModule;
import com.pairzy.com.mobileverify.otp.Otp_Fragment;
import com.pairzy.com.mobileverify.otpreceive_error.ResentOtpFragment;
import com.pairzy.com.mobileverify.otpreceive_error.ResentOtpDaggerModule;
import com.pairzy.com.mobileverify.result.ResultDaggerModule;
import com.pairzy.com.mobileverify.result.ResultFragment;
import com.pairzy.com.util.Countrypicker.CountryPicker;
import javax.inject.Named;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
/**
 * <h1>PreferenceDaggerModule</h1>
 * <p>
 *
 * </p>
 * @author 3Embed
 * @since 13/08/17
 * @version 1.0.
 */
@Module
public abstract class MobileVerifyActivityBuilder
{

    public static final String ACTIVITY_FRAGMENT_MANAGER = "BaseActivity.FragmentManager";

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(MobileVerifyActivity splashActivity);

    @ActivityScoped
    @Binds
    abstract MobileVerifyContract.View provideView(MobileVerifyActivity mobileVerifyActivity);

    @ActivityScoped
    @Binds
    abstract MobileVerifyContract.Presenter taskPresenter(MobileVerifyPresenter presenter);

    @Provides
    @Named(ACTIVITY_FRAGMENT_MANAGER)
    @ActivityScoped
    static FragmentManager activityFragmentManager(Activity activity)
    {
        return ((AppCompatActivity)activity).getSupportFragmentManager();
    }

    @FragmentScoped
    @ContributesAndroidInjector(modules = {PhnoDaggerModule.class})
    abstract PhnoFragment contributeFragmentPhno();

    @FragmentScoped
    @ContributesAndroidInjector
    abstract CountryPicker contributeFragmentCountryPicker ();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {ResultDaggerModule.class})
    abstract ResultFragment contributeResultFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {OtpDaggerModule.class})
    abstract Otp_Fragment contributeOtpFragment();


    @FragmentScoped
    @ContributesAndroidInjector(modules = {ResentOtpDaggerModule.class})
    abstract ResentOtpFragment contributeResentOtpFragment();
}