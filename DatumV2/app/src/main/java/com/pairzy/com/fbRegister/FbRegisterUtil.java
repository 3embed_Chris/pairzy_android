package com.pairzy.com.fbRegister;

import android.app.Activity;
import android.content.Context;
import android.location.Geocoder;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.ImageChecker.ImproperImageAlert;
import com.pairzy.com.util.LocationProvider.Location_service;
import com.pairzy.com.util.MediaBottomSelector;
import com.pairzy.com.util.MediaPreview.ImagePreview;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.videocompressor.com.CompressImage;
import com.videocompressor.com.VideoCompressor;

import java.util.Locale;

import dagger.Module;
import dagger.Provides;

/**
 * <h2>FbRegisterUtil</h2>
 * <P>
 *
 * </P>
 * @since  2/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class FbRegisterUtil
{
    @Provides
    @ActivityScoped
    AnimatorHandler animatorHandler(Activity activity)
    {
        return new AnimatorHandler(activity);
    }

    @Provides
    @ActivityScoped
    DatumProgressDialog datumProgressDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        return  new DatumProgressDialog(activity,typeFaceManager);
    }

    @Provides
    @ActivityScoped
    App_permission getApp_permission(Activity activity,TypeFaceManager typeFaceManager)
    {
        return new App_permission(activity,typeFaceManager);
    }

    @Provides
    @ActivityScoped
    MediaBottomSelector getMediaBottomSelector(Activity activity,TypeFaceManager typeFaceManager)
    {
        return new MediaBottomSelector(activity,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    LoadingProgress getLoadingProgress(Activity activity)
    {
        return new LoadingProgress(activity);
    }

    @ActivityScoped
    @Provides
    Location_service getLocation_service(Activity activity)
    {
        return new Location_service(activity);
    }

    @ActivityScoped
    @Provides
    CompressImage compressImage()
    {
        return new CompressImage();
    }

    @ActivityScoped
    @Provides
    VideoCompressor getVideoCompressor(Context context)
    {
        return new VideoCompressor(context);
    }


    @ActivityScoped
    @Provides
    ImproperImageAlert getImproperImageAlert(Activity activity,TypeFaceManager typeFaceManager)
    {
        return new ImproperImageAlert(activity,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    ImagePreview getImagePreview(Activity activity)
    {
        return new ImagePreview(activity);
    }

    @ActivityScoped
    @Provides
    Geocoder provideGeocoder(Activity activity)
    {
        return new Geocoder(activity, Locale.getDefault());
    }
}
