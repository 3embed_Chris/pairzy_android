package com.pairzy.com.coinWallet;

import android.app.Activity;

import com.pairzy.com.coinWallet.allCoin.AllCoinFrag;
import com.pairzy.com.coinWallet.allCoin.AllCoinModule;
import com.pairzy.com.coinWallet.allCoin.AllCoinUtilModule;
import com.pairzy.com.coinWallet.coinIn.InCoinFrag;
import com.pairzy.com.coinWallet.coinIn.InCoinModule;
import com.pairzy.com.coinWallet.coinIn.InCoinUtilModule;
import com.pairzy.com.coinWallet.coinOut.OutCoinFrag;
import com.pairzy.com.coinWallet.coinOut.OutCoinModule;
import com.pairzy.com.coinWallet.coinOut.OutCoinUtilModule;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * <h>CoinWalletModule class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */
@Module
public abstract class CoinWalletModule {

    @ActivityScoped
    @Binds
    abstract CoinWalletContract.Presenter boostDetailPresenter(CoinWalletPresenter presenter);

    @ActivityScoped
    @Binds
    abstract CoinWalletContract.View boostDetailView(CoinWalletActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity boostDetailActivity(CoinWalletActivity activity);

    @FragmentScoped
    @ContributesAndroidInjector(modules = {AllCoinModule.class, AllCoinUtilModule.class})
    abstract AllCoinFrag allCoinFrag();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {InCoinModule.class, InCoinUtilModule.class})
    abstract InCoinFrag inCoinFrag();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {OutCoinModule.class, OutCoinUtilModule.class})
    abstract OutCoinFrag outCoinFrag();
}
