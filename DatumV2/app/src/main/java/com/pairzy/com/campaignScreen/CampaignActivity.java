package com.pairzy.com.campaignScreen;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import android.text.TextUtils;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.webPage.WebActivity;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.DaggerActivity;

/**
 * <h>CampaignActivity</h>
 * <p> shows the Campaign message by Admin.</p>
 * @author 3Embed.
 * @since 28/6/18.
 */
public class CampaignActivity extends DaggerActivity implements CampaignContract.View{

    @BindView(R.id.iv_cross)
    AppCompatImageView ivCross;
    @BindView(R.id.iv_campaign)
    SimpleDraweeView ivCampaign;
    @BindView(R.id.tv_campaign_title)
    TextView tvTitle;
    @BindView(R.id.tv_campaign_msg)
    TextView tvMessage;

    private Unbinder unbinder;
    private String campaignUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campaign);
        Fresco.initialize(this);
        unbinder = ButterKnife.bind(this);
        initData(getIntent());
        this.setFinishOnTouchOutside(true);
    }

    private void initData(Intent intent) {
        Bundle data = intent.getBundleExtra("data");
        String image = data.getString("image","");
        campaignUrl = data.getString("campaignUrl","");
        String title = data.getString("title","");
        String message = data.getString("message","");

        if(!TextUtils.isEmpty(image) && image.contains("http")){
            ivCampaign.setImageURI(image);
        }
        if(!TextUtils.isEmpty(title)){
            tvTitle.setText(title);
        }
        if(!TextUtils.isEmpty(message)){
            tvMessage.setText(message);
        }
    }

    @OnClick({R.id.root,R.id.btn_ok})
    public void campaign(){
        if(!TextUtils.isEmpty(campaignUrl)) {
            showWebActivity("Campaign", "http://" + campaignUrl);
        }
    }

    private void showWebActivity(String title, @NonNull String url)
    {
        Intent intent = new Intent(CampaignActivity.this, WebActivity.class);
        intent.putExtra("url",url);
        intent.putExtra("title",title);
        startActivity(intent);
    }
    @OnClick(R.id.iv_cross)
    public void close(){
//        Intent intent = new Intent(this, SplashActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(intent);
        this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(unbinder != null)
            unbinder.unbind();
    }
}
