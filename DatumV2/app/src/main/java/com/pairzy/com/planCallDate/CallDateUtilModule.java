package com.pairzy.com.planCallDate;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;

import java.util.Date;

import dagger.Module;
import dagger.Provides;

/**
 * <h>CallDateUtilModule class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

@Module
public class CallDateUtilModule {

    @ActivityScoped
    @Provides
    SingleDateAndTimePickerDialog.Builder provideDateTimePickerDialog(Activity activity){
        return new SingleDateAndTimePickerDialog.Builder(activity)
                .bottomSheet()
                .defaultDate(new Date(System.currentTimeMillis()))
                .minutesStep(AppConfig.DATE_PICKER_MINUTE_STEP_SIZE)
                .mustBeOnFuture()
                .curved();
    }

    @ActivityScoped
    @Provides
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }

    @ActivityScoped
    @Provides
    CoinDialog provideSpendCoinDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility){
        return new CoinDialog(activity,typeFaceManager,utility);
    }

    @ActivityScoped
    @Provides
    App_permission provideAppPermission(Activity activity, TypeFaceManager typeFaceManager){
        return new App_permission(activity,typeFaceManager);
    }

}
