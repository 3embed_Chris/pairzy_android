package com.pairzy.com.home.HomeModel;

public class CardVisibleStatus {
    boolean isCardVisible = false;

    public boolean isCardVisible() {
        return isCardVisible;
    }

    public void setCardVisible(boolean cardVisible) {
        isCardVisible = cardVisible;
    }
}
