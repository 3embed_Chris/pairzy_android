package com.pairzy.com.MyProfile.editPreference;

import android.content.Intent;

import com.pairzy.com.data.model.PrefData;

import dagger.android.support.DaggerFragment;

/**
 * <h>EditPrefContract</h>
 * @author 3Embed.
 * @since 23/4/18.
 */

public interface EditPrefContract {

    interface View{
        void applyFont();
        void showError(String errorMsg);
        void showMessage(String errorMsg);
        void showPrefData(PrefData prefData);
        void openFragment(DaggerFragment fragment, boolean keepBack);
        void saveNewPrefData(PrefData currentPrefData);
    }

    interface Presenter{

        void init();
        void loadPrefData(Intent intent);
        void showError(String errorMsg);
        void showMessage(String errorMsg);
        void updatePreferenceData(PrefData currentPrefData);
        boolean isChangeDetected(PrefData newPrefData, PrefData oldPrefData);
    }
}