package com.pairzy.com.coinWallet.allCoin;

import com.pairzy.com.coinWallet.allCoin.model.AllCoinModel;

import javax.inject.Inject;

/**
 *<h>AllCoinPresenter</h>
 * <p></p>
 *@author 3Embed.
 *@since 30/5/18.
 */
public class AllCoinPresenter implements AllCoinContract.Presenter {

    private AllCoinContract.View view;

    @Inject
    AllCoinModel model;

    @Inject
    public AllCoinPresenter(){
    }

    @Override
    public void takeView(AllCoinContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        this.view = null;
    }

    @Override
    public void loadCoinList() {
        for(int i =0;i<20;i++)
            model.addCoinItem();
        model.notifyAdapter();
    }

    @Override
    public boolean isCoinHistoryEmpty() {
        return model.isCoinHistoryEmpty();
    }
}
