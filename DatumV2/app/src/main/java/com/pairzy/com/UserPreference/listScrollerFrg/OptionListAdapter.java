package com.pairzy.com.UserPreference.listScrollerFrg;

import android.app.Activity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
import java.util.ArrayList;
/**
 * <h2>OptionListAdapter</h2>
 * <P>
 *
 * </P>
 * @since  2/22/2018.
 * @version 1.0.
 * @author 3Embed.
 */
class OptionListAdapter extends RecyclerView.Adapter<OptionItemsView>
{
    private Activity activity;
    private ArrayList<ListData> list_data;
    private TypeFaceManager typeFaceManager;
    OptionListAdapter(Activity activity, ArrayList<ListData> data, TypeFaceManager typeFaceManager)
    {
        this.activity=activity;
        this.list_data=data;
        this.typeFaceManager=typeFaceManager;
    }

    @Override
    public OptionItemsView onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.options_item_view,parent,false);
        return new OptionItemsView(view,typeFaceManager);
    }

    @Override
    public void onBindViewHolder(OptionItemsView holder, int position)
    {
        handelViewDetails(holder);
    }

    private void handelViewDetails(OptionItemsView holder)
    {
        int position=holder.getAdapterPosition();
        ListData data = list_data.get(position);
        holder.option_text.setText(data.getOption());

        if(data.isSelected()){
            holder.option_text.setTextColor(ContextCompat.getColor(activity,R.color.black));
            holder.option_tick.setVisibility(View.VISIBLE);
        }
        else{
            holder.option_tick.setVisibility(View.GONE);
            holder.option_text.setTextColor(ContextCompat.getColor(activity,R.color.softLightGray));
        }
    }

    @Override
    public int getItemCount() {
        return list_data.size();
    }
}
