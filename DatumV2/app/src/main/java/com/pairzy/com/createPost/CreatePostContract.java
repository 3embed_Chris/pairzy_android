package com.pairzy.com.createPost;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

public interface CreatePostContract {

    interface View extends BaseView
    {

    }

    interface Presenter extends BasePresenter
    {

    }

}
