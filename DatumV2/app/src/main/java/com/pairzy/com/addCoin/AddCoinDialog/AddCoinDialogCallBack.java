package com.pairzy.com.addCoin.AddCoinDialog;
/**
 * <h2>ChatMenuCallback</h2>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface AddCoinDialogCallBack
{
    void onOk();
}
