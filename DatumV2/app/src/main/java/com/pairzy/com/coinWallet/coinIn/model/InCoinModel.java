package com.pairzy.com.coinWallet.coinIn.model;

import com.pairzy.com.coinWallet.CoinWalletUtil;
import com.pairzy.com.coinWallet.Model.AllCoinAdapter;
import com.pairzy.com.coinWallet.Model.CoinPojo;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

/**
 *<h>InCoinModel</h>
 * <p></p>
 *@author 3Embed.
 *@since 30/5/18.
 */

public class InCoinModel {

    @Named(CoinWalletUtil.COIN_IN_LIST)
    @Inject
    ArrayList<CoinPojo> arrayList;

    @Inject
    AllCoinAdapter allCoinAdapter;

    @Inject
    public InCoinModel() {
    }

    public boolean isCoinHistoryEmpty(){
        return arrayList.size() == 0;
    }

    public void notifyAdapter(){
        allCoinAdapter.notifyDataSetChanged();
    }
}
