package com.pairzy.com.userProfile.AleretBox;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.Locale;
/**
 * <h2>ChatMenuDialog</h2>
 * <P>
 *
 * </P>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class BottomDialog
{
    private Activity activity;
    private BottomSheetDialog dialog;
    private TypeFaceManager typeFaceManager;
    private Utility utility;
    private AlertBoxCallBack callBack;

    public BottomDialog(Activity activity,TypeFaceManager typeFaceManager,Utility utility)
    {
        this.activity=activity;
        this.typeFaceManager=typeFaceManager;
        this.utility=utility;
        dialog = new BottomSheetDialog(activity,R.style.DatumBottomDialog);
    }


    public void showDialog(String user_name,Boolean isUserBlocked,boolean isMatched,boolean isChatDocExist, AlertBoxCallBack callBack)
    {
        this.callBack = callBack;
        if(dialog.isShowing())
        {
            dialog.cancel();
        }
        user_name=utility.formatString( user_name);
        @SuppressLint("InflateParams")
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.user_option_aleret_bg, null);
        Button recomend_button=dialogView.findViewById(R.id.recomend_button);
        recomend_button.setTypeface(typeFaceManager.getCircularAirBook());
        recomend_button.setOnClickListener(view -> {
            //pending
            if(callBack != null)
                callBack.onRecommendFriend();
            if(dialog!=null)
                dialog.cancel();
        });

        View blockButtonDivider = dialogView.findViewById(R.id.block_button_divider_line);

        Button report_button=dialogView.findViewById(R.id.report_button);
        if(isMatched || isChatDocExist){
            report_button.setBackgroundColor(activity.getResources().getColor(R.color.white));
            blockButtonDivider.setVisibility(View.VISIBLE);
        }else{
            report_button.setBackgroundResource(R.drawable.bottom_corner_white_bg);
            blockButtonDivider.setVisibility(View.GONE);
        }
        report_button.setText(String.format(Locale.ENGLISH,"Report %s",user_name));
        report_button.setTypeface(typeFaceManager.getCircularAirBook());
        report_button.setOnClickListener(view -> {
            //pending
            if(callBack != null)
                callBack.onReport();
            if(dialog!=null)
                dialog.cancel();
        });
        Button block_button = dialogView.findViewById(R.id.block_button);
        //if user is matched or user chatted ever then will allow to block.
        if(isMatched || isChatDocExist) {
            if (isUserBlocked)
                block_button.setText(String.format(Locale.ENGLISH, "Unblock %s", user_name));
            else
                block_button.setText(String.format(Locale.ENGLISH, "Block %s", user_name));

            block_button.setTypeface(typeFaceManager.getCircularAirBook());
            block_button.setOnClickListener(view -> {
                //pending
                if (isUserBlocked) {
                    if (callBack != null)
                        callBack.onUnblock();
                } else {
                    if (callBack != null)
                        callBack.onBlock();
                }
                if (dialog != null)
                    dialog.cancel();
            });
        }else{
            block_button.setVisibility(View.GONE);
        }

        Button onCanceled=dialogView.findViewById(R.id.onCanceled);
        onCanceled.setTypeface(typeFaceManager.getCircularAirBook());
        onCanceled.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();
        });
        dialog.setContentView(dialogView);
        dialog.show();

    }
}
