package com.pairzy.com.home.Matches.PostFeed.Model;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Matches.PostFeed.PostFeedFragUtil;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by ankit on 15/5/18.
 */

public class PostFeedModel extends BaseModel{

    @Inject
    @Named(PostFeedFragUtil.NEWS_FEED_LIST)
    ArrayList<PostListPojo> postList;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Utility utility;

    @Inject
    PostFeedModel(){
    }

    public int getListSize() {
        return postList.size();
    }

    public void clearList() {
        postList.clear();
    }

    public PostListPojo getPostItem(int position) {
        if(postList.size() > position)
            return postList.get(position);
        return null;
    }

    public boolean checkPostExist() {
        if(postList.size() > 0)
            return true;
        return false;
    }

    public void parseNewsFeedResponse(String data) throws DataParsingException{
        try
        {
            postList.clear();
            PostListResponse result_data= utility.getGson().fromJson(data,PostListResponse.class);
            ArrayList<PostListPojo> dataPojo=result_data.getData();
            postList.addAll(dataPojo);

        }catch (Exception e) {
            throw new DataParsingException(e.getMessage());
        }
    }

    public Map<String, Object> getLikeUnlikeData(double type, String postId) {
        Map<String, Object> map=new HashMap<>();
        map.put(ApiConfig.LIKE_UNLIKE.POST_ID,postId);
        map.put(ApiConfig.LIKE_UNLIKE.TYPE,type);
        return map;
    }

    public boolean removePostList(String postId,int position) {
        try{
            PostListPojo postListPojo = postList.get(position);
            if(postListPojo.getPostId().equals(postId)){
                postList.remove(position);
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public Map<String, Object> getDeletePostBody(String postId) {
        Map<String,Object> map = new HashMap<>();
        map.put(ApiConfig.DELETE_POST.POST_ID,postId);
        return map;
    }
}
