package com.pairzy.com.util.SuggestionAleret;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
/**
 * <h2>SuggestionDialog</h2>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class SuggestionDialog
{
    private SuggestionAlertCallback callback;
    private Activity activity;
    private Dialog  alert = null;
    private TypeFaceManager typeFaceManager;
    private Utility utility;
    private TextView title;
    private LinearLayout bottom_view;
    private ScrollView alertCenterView;
    private int selectedRequest=0;
    private TextInputLayout til_issue,til_email,til_name;
    private EditText input_name,input_email;
    private TextInputEditText input_issue;
    private PreferenceTaskDataSource dataSource;


    public  SuggestionDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility,
                               PreferenceTaskDataSource dataSource)
    {
        this.typeFaceManager=typeFaceManager;
        this.activity=activity;
        this.utility=utility;
        this.dataSource = dataSource;
    }

    /*
     * Showing the alert.*/
    public void showAlert(SuggestionAlertCallback callback1)
    {
        this.callback=callback1;
        if(alert!=null&&alert.isShowing())
        {
            alert.cancel();
        }

        AlertDialog.Builder builder=new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);
        LayoutInflater inflater =activity.getLayoutInflater();
        @SuppressLint("InflateParams")
        View alertLayout = inflater.inflate(R.layout.need_help_aleret_bg, null);
        builder.setView(alertLayout);
        Button report_issue=alertLayout.findViewById(R.id.report_issue);
        report_issue.setTypeface(typeFaceManager.getCircularAirBook());
        report_issue.setOnClickListener(view -> {
            selectedRequest=0;
            input_issue.setHint(R.string.your_issue_text);
            openUserIssueView(report_issue.getText().toString());
        });
        Button suggestion=alertLayout.findViewById(R.id.suggestion);
        suggestion.setTypeface(typeFaceManager.getCircularAirBook());
        suggestion.setOnClickListener(view -> {
            selectedRequest=1;
            input_issue.setHint(R.string.your_suggestion_text);
            openUserIssueView(suggestion.getText().toString());
        });
        Button question_data=alertLayout.findViewById(R.id.question_data);
        question_data.setTypeface(typeFaceManager.getCircularAirBook());
        question_data.setOnClickListener(view -> {
            selectedRequest=2;
            input_issue.setHint(R.string.your_question_text);
            openUserIssueView(question_data.getText().toString());
        });
        Button onCanceled=alertLayout.findViewById(R.id.onCanceled);
        onCanceled.setTypeface(typeFaceManager.getCircularAirBook());
        onCanceled.setOnClickListener(view -> {
            if(alert!=null&&alert.isShowing())
            {
                alert.cancel();
            }
            if(callback!=null)
                callback.onSuggestionCanceled();
        });
        alertLayout.findViewById(R.id.parent_layout).setOnClickListener(view -> {
            if(alert!=null&&alert.isShowing())
            {
                alert.cancel();
            }
            if(callback!=null)
                callback.onSuggestionCanceled();
        });

        alertLayout.findViewById(R.id.close_button).setOnClickListener(view -> {
            if(alert!=null&&alert.isShowing())
            {
                alert.cancel();
            }
            if(callback!=null)
                callback.onSuggestionCanceled();
        });
        title=alertLayout.findViewById(R.id.title);
        title.setTypeface(typeFaceManager.getCircularAirBold());
        til_name= alertLayout.findViewById(R.id.input_layout_name);
        input_name=alertLayout.findViewById(R.id.input_name);
        input_name.setTypeface(typeFaceManager.getCircularAirBook());
        input_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                til_name.setErrorEnabled(false);
                input_name.setTextColor(ContextCompat.getColor(activity,R.color.gray));
                input_name.setHintTextColor(ContextCompat.getColor(activity,R.color.hint_Text_color));
            }
            @Override
            public void afterTextChanged(Editable editable)
            {}
        });
        til_email= alertLayout.findViewById(R.id.input_layout_email);
        input_email=alertLayout.findViewById(R.id.input_email);
        input_email.setTypeface(typeFaceManager.getCircularAirBook());
        input_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                til_email.setErrorEnabled(false);
                input_email.setTextColor(ContextCompat.getColor(activity,R.color.gray));
                input_email.setHintTextColor(ContextCompat.getColor(activity,R.color.hint_Text_color));
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        til_issue= alertLayout.findViewById(R.id.input_layout_issue);
        input_issue=alertLayout.findViewById(R.id.input_issue);
        input_issue.setTypeface(typeFaceManager.getCircularAirBook());
        input_issue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                til_issue.setErrorEnabled(false);
                input_issue.setTextColor(ContextCompat.getColor(activity,R.color.gray));
                input_issue.setHintTextColor(ContextCompat.getColor(activity,R.color.hint_Text_color));
            }
            @Override
            public void afterTextChanged(Editable editable) {}
        });

        input_issue.setOnEditorActionListener((v, actionId, event) -> {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE))
            {
                validateInput(input_name,input_email,input_issue);
            }
            return false;
        });
        alertLayout.findViewById(R.id.tick_button).setOnClickListener(view ->
                validateInput(input_name,input_email,input_issue));

        alertCenterView=alertLayout.findViewById(R.id.alertCenterView);
        bottom_view=alertLayout.findViewById(R.id.bottom_view);
        alert=builder.create();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
        {
            Bitmap map=utility.takeScreenShot(activity);
            Bitmap fast= BlurBuilder.blur(activity,map);
            Drawable draw=new BitmapDrawable(activity.getResources(),fast);
            Window window = alert.getWindow();
            assert window != null;
            window.setBackgroundDrawable(draw);
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            window.setGravity(Gravity.CENTER);
            alert.show();
        }else
        {
            new BlurAsyncTask().execute();
        }
    }

    @SuppressLint("StaticFieldLeak")
    class BlurAsyncTask extends AsyncTask<Void, Integer, Bitmap>
    {
        protected Bitmap doInBackground(Void...arg0)
        {
            Bitmap map=utility.takeScreenShot(activity);
            return BlurBuilder.blur(activity,map);
        }

        protected void onPostExecute(Bitmap result)
        {
            try
            {
                if (result != null&&alert!=null&&alert.getWindow()!=null)
                {
                    Drawable draw=new BitmapDrawable(activity.getResources(),result);
                    Window window = alert.getWindow();
                    assert window != null;
                    window.setBackgroundDrawable(draw);
                    window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    window.setGravity(Gravity.CENTER);
                    alert.show();
                }
            }catch (Exception e){}
        }
    }

    /*
    * Open center view */
    private void openUserIssueView(String title_text)
    {
        input_email.setText(dataSource.getEmail());
        input_name.setText(dataSource.getName());
        title.setText(title_text);
        bottom_view.setVisibility(View.GONE);
        alertCenterView.setVisibility(View.VISIBLE);
    }

    /*
     * Validate user input.*/
    private void validateInput(EditText input_name,EditText input_email,EditText input_issue)
    {
        utility.closeSpotInputKey(activity,input_issue);
        boolean isErrorExist=false;
        if(TextUtils.isEmpty(input_name.getText()))
        {
            isErrorExist=true;
            til_name.setError(activity.getString(R.string.name_error_issue_text));
            doShakeAnimation(input_name,true);
        }

        if(!utility.isValidEmail(input_email.getText()))
        {
            isErrorExist=true;
            til_email.setError(activity.getString(R.string.email_error_issue_Text));
            doShakeAnimation(input_email,false);
        }
        if(TextUtils.isEmpty(input_issue.getText().toString().trim()))
        {
            isErrorExist=true;
            til_issue.setError(activity.getString(R.string.issue_error_text));
            doShakeAnimation(input_issue,true);
        }
        if(!isErrorExist)
        {
            if(alert!=null&&alert.isShowing())
            {
                alert.cancel();
            }
            switch (selectedRequest)
            {
                case 0:
                    if(callback!=null)
                        callback.onReport(input_name.getText().toString(),input_email.getText().toString(),input_issue.getText().toString());
                    break;
                case 1:
                    if(callback!=null)
                        callback.onSuggestion(input_name.getText().toString(),input_email.getText().toString(),input_issue.getText().toString());
                    break;
                case 2:
                    if(callback!=null)
                        callback.onQuestion(input_name.getText().toString(),input_email.getText().toString(),input_issue.getText().toString());
                    break;
            }
        }
    }

    /*
     *Do shaking animation for the given view and setting error color.
     **/
    private void doShakeAnimation(EditText edit_text,boolean isOnlyHint)
    {
        Animation  animation=AnimationUtils.loadAnimation(activity,R.anim.shake_translate_animation);
        animation.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                if(isOnlyHint)
                {
                    edit_text.setHintTextColor(ContextCompat.getColor(activity,R.color.red_color));
                }else
                {
                    edit_text.setTextColor(ContextCompat.getColor(activity,R.color.red_color));
                    edit_text.setHintTextColor(ContextCompat.getColor(activity,R.color.red_color));
                }
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        edit_text.startAnimation(animation);
    }
}
