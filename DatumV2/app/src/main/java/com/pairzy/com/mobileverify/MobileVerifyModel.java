package com.pairzy.com.mobileverify;

import com.pairzy.com.BaseModel;
import javax.inject.Inject;
/**
 * <h2>MobileVerifyModel</h2>
 * <p>
 * ListModel of @{@link MobileVerifyActivity}
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 06/01/2018.
 **/
class MobileVerifyModel extends BaseModel
{
    private String mobileNumber;
    @Inject
    MobileVerifyModel() {
    }

}
