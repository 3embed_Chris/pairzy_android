package com.pairzy.com.home.MakeMatch.swipeCardModel;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.R;
import com.pairzy.com.home.MakeMatch.model.MakeMatchDataPOJO;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.matchMakerSwipeCard.SwipeFlingAdapterView;

import java.util.ArrayList;

public class MatchMakerAdapter extends RecyclerView.Adapter {

    private  ArrayList<MakeMatchDataPOJO> arrayList;
    private TypeFaceManager typeFaceManager;
    private ItemViewCallBack callback;
    private AdapterItemCallback adapterItemCallback;

    public void setCallback(AdapterItemCallback adapterItemCallback){
        this.adapterItemCallback=adapterItemCallback;
    }

    public MatchMakerAdapter( TypeFaceManager typeFaceManager, ArrayList<MakeMatchDataPOJO> arrayList) {
        this.arrayList=arrayList;
        this.typeFaceManager=typeFaceManager;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.match_maker_item,parent,false);
        return new MatchMakerViewHolder(view,adapterItemCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        MatchMakerSwipeAdapter matchMakerSwipeAdapter =new MatchMakerSwipeAdapter(arrayList.get(position),typeFaceManager);
        final MatchMakerViewHolder viewHolder= (MatchMakerViewHolder) holder;

        viewHolder.flingContainer.setAdapter(matchMakerSwipeAdapter);
        viewHolder.flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {}

            @Override
            public void onLeftCardExit(Object dataObject) {
                //todo Do something on the left!
//                adapterItemCallback.leftSwipe(position);
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                //todo Do something on the right!
//                adapterItemCallback.rightSwipe(position);

            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
            }

            @Override
            public void onScroll(float scrollProgressPercent) {
                View view = viewHolder.flingContainer.getSelectedView();
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
            }
        });

        // todo Optionally add an OnItemClickListener
        viewHolder.flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
