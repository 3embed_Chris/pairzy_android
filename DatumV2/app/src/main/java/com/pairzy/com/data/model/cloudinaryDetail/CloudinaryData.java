package com.pairzy.com.data.model.cloudinaryDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CloudinaryData {

    @Expose
    @SerializedName("cloudName")
    String cloudName;

    @Expose
    @SerializedName("apiKey")
    String apiKey;

    @Expose
    @SerializedName("apiSecret")
    String apiSecret;

    public String getCloudName() {
        return cloudName;
    }

    public void setCloudName(String cloudName) {
        this.cloudName = cloudName;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getApiSecret() {
        return apiSecret;
    }

    public void setApiSecret(String apiSecret) {
        this.apiSecret = apiSecret;
    }
}
