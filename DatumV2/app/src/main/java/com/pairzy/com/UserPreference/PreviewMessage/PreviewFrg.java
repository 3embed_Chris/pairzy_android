package com.pairzy.com.UserPreference.PreviewMessage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.UserPreference.MyPreferencePageContract;
import com.pairzy.com.util.SpanFontStyle;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.webPage.WebActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * A simple {@link Fragment} subclass.
 */
public class PreviewFrg extends DaggerFragment implements PreviewFrgContract.View
{
    @Inject
    MyPreferencePageContract.Presenter mainpresenter;
    @Inject
    Activity activity;
    @Inject
    PreviewFrgContract.Presenter presenter;
    @Inject
    TypeFaceManager typeFaceManager;
    private Unbinder unbinder;
    @BindView(R.id.title_one)
    TextView title_one;
    @BindView(R.id.details_msg)
    TextView details_msg;
    @BindView(R.id.btnNext)
    RelativeLayout btnNext;
    public PreviewFrg() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_preview_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder= ButterKnife.bind(this,view);
        upDateUI();
        try {
//            textToLink();
        }catch (Exception e){}
    }

    private void textToLink() {
        String detailsString = activity.getString(R.string.community_details);
        String linkText = activity.getString(R.string.join_us_text);
        int start = detailsString.indexOf(linkText);
        int end = start+linkText.length()+1/*plus one for extra dot*/;
        SpannableString ss = new SpannableString(detailsString);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                String title = linkText;
                String url = activity.getResources().getString(R.string.url_join_us);
                launchJoinUsPage(title,url);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan,start,end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        ss.setSpan(new SpanFontStyle(typeFaceManager.getCircularAirBold(),activity.getResources().getColor(R.color.datum)),start,end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.datum)),start,end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        details_msg.setText(ss);
        details_msg.setMovementMethod(LinkMovementMethod.getInstance());
        details_msg.setHighlightColor(activity.getResources().getColor(R.color.transparent));
    }

    private void launchJoinUsPage(String title, String url) {
        Intent intent = new Intent(activity, WebActivity.class);
        intent.putExtra("title",title);
        intent.putExtra("url",url);
        startActivity(intent);
    }

    /*
     * Updating the required ui updating like fonts etc.*/
    private void upDateUI()
    {
        btnNext.setEnabled(false);
        title_one.setTypeface(typeFaceManager.getCircularAirBook());
        details_msg.setTypeface(typeFaceManager.getCircularAirBold());
    }

    @Override
    public void onResume()
    {
        super.onResume();
        presenter.takeView(this);
        handelNextButton(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
    }

    @OnClick(R.id.parent_view)
    void onParentClick(){}

    @OnClick(R.id.btnNext)
    void onNextClicked()
    {
        mainpresenter.hidePreview();
    }

    /*
     *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            btnNext.setEnabled(true);
        }
        else {
            btnNext.setEnabled(false);
        }
    }
}
