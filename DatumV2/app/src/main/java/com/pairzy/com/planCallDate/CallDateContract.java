package com.pairzy.com.planCallDate;

import android.content.Intent;

import com.pairzy.com.home.Dates.Model.DateListPojo;

/**
 * <h>CallDateContract interface</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public interface CallDateContract {

    interface View{
        void applyFont();
        void showError(String errorMsg);
        void showMessage(String errorMsg);
        void showMessage(int msgId);
        void showSelectedDate(String dateStr);
        void initUserData(String userName, String userImage,boolean isAudioDate);
        void initOwnData(String yourName, String profilePic);
        void initUserData(DateListPojo date_data,boolean isAudioDate);
        void returnFinalData(DateListPojo date_data);
        void launchCoinWallet();
    }

    interface Presenter{
        void init();
        void launchDateTimePicker();
        void initData(Intent intent);
        void loadCoinDialog();
        void dispose();
        void checkForValidInput();
    }
}