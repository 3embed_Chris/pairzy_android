package com.pairzy.com.MyProfile.editAge;

import android.app.Activity;
import android.app.DatePickerDialog;

import com.pairzy.com.R;
import com.pairzy.com.register.Userdob.DobModel;

import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

/**
 * Created by ankit on 27/4/18.
 */

public class EditDobPresenter implements EditDobContract.Presenter {

    @Inject
    EditDobContract.View view;

    @Inject
    Activity activity;

    @Inject
    DobModel dobModel;


    @Inject
    EditDobPresenter(){
    }

    @Override
    public boolean validateInput(String text)
    {
        return false;
    }

    @Override
    public String formatInput(String input)
    {
        return null;
    }

    @Override
    public void showMessage(String message)
    {}
    @Override
    public void onError(String message)
    {}

    @Override
    public void validateAge(String day, String month, String year)
    {
        if(dobModel.isAdult(day,month,year))
        {
            if(view!=null)
                view.finishActivity(dobModel.getDateInMilli(day,month,year));
        }else
        {
            if(view!=null)
                view.onError(view.getAdultErrorMessage());
        }
    }


    @Override
    public void openDatePicker()
    {
        final Calendar c = Calendar.getInstance();
        final Calendar mincalender = Calendar.getInstance();
        mincalender.set(1900,0,1);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(activity, R.style.DatePicker,
                (view, year, monthOfYear, dayOfMonth) -> {
                    String days=dayOfMonth<10?"0"+dayOfMonth:""+dayOfMonth;
                    monthOfYear=monthOfYear+1;
                    String months=monthOfYear<10?"0"+monthOfYear:""+monthOfYear;
                    if(this.view!=null)
                    {
                        this.view.onDateSelected(days,months,""+year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.getDatePicker().setMinDate(mincalender.getTime().getTime());
        datePickerDialog.show();
    }


}
