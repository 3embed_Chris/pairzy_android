package com.pairzy.com.home.Prospects.RecentVisitors;
import android.app.Activity;

import com.pairzy.com.AppController;
import com.pairzy.com.MatchedView.MatchAlertObserver;
import com.pairzy.com.MatchedView.OpponentDetails;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Prospects.OnAdapterItemClicked;
import com.pairzy.com.home.Prospects.RecentVisitors.Model.RecentVisiterDataModel;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import javax.inject.Inject;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * <h2>RecentVisitorsPresenter</h2>
 * <P>
 *
 * </P>
 * @since  3/23/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class RecentVisitorsPresenter implements RecentVisitorsContract.Presenter,OnAdapterItemClicked
{
    private RecentVisitorsContract.View view;
    @Inject
    RecentVisiterDataModel model;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    NetworkStateHolder holder;
    @Inject
    MatchAlertObserver matchAlertObserver;
    @Inject
    NetworkService service;
    @Inject
    Activity activity;

    public static final int LIMIT=10;
    public static int PAGE_COUNT=0;
    private boolean isTryAgain;

    private CompositeDisposable compositeDisposable;

    @Inject
    RecentVisitorsPresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(RecentVisitorsContract.View view) {
        this.view= view;
    }

    @Override
    public void dropView()
    {
        compositeDisposable.clear();
        this.view=null;
    }

    @Override
    public void initAdapterListener()
    {
        if(view!=null)
            view.initAdapterListener(this);
    }

    @Override
    public void initMatchListener()
    {
        Observer<OpponentDetails> observer = new Observer<OpponentDetails>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {
                compositeDisposable.add(d);
            }
            @Override
            public void onNext(OpponentDetails value)
            {
                model.changeTheStatus(value.getUser_id());
            }
            @Override
            public void onError(Throwable e)
            {
                e.printStackTrace();
            }
            @Override
            public void onComplete()
            {}
        };
        matchAlertObserver.getObservable().subscribeOn(Schedulers.newThread());
        matchAlertObserver.getObservable().observeOn(AndroidSchedulers.mainThread());
        matchAlertObserver.getObservable().subscribe(observer);
    }



    @Override
    public void getListData(boolean isLoadMore)
    {

        if(!holder.isConnected())
        {
            isTryAgain=false;
            if(!model.handelLoadMoreError())
            {
                if(view!=null)
                    view.onApiError(activity.getString(R.string.no_internet_error));
            }
        }else
        {
            if(!isTryAgain)
            {
                if(!isLoadMore)
                {
                    PAGE_COUNT=0;
                }else
                {
                    PAGE_COUNT+=LIMIT;
                }
            }

            isTryAgain=false;
            if(!model.handelLoadMore())
            {
                if(view!=null)
                    view.showProgress();
            }

            service.getrecentVisitors(dataSource.getToken(),model.getLanguage(),PAGE_COUNT,PAGE_COUNT+LIMIT)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> value)
                        {
                            try
                            {
                                if(value.code() == 200)
                                {
                                    String data=value.body().string();
                                    model.parseData(data);
                                    if(view!=null)
                                        view.onDataUpdate();

                                }
                                else if(value.code() == 401){
                                    AppController.getInstance().appLogout();
                                }
                                else if(value.code()==412)
                                {
                                    if(!model.handelLoadMoreError())
                                    {
                                        if(view!=null)
                                            view.emptyData();
                                    }
                                }else
                                {
                                    if(!model.handelLoadMoreError())
                                    {
                                        if(view!=null)
                                            view.onApiError(activity.getString(R.string.failed_get_members));
                                    }
                                }
                            } catch (Exception e)
                            {
                                if(!model.handelLoadMoreError())
                                {
                                    if(view!=null)
                                        view.onApiError(activity.getString(R.string.failed_get_members));
                                }
                            }
                        }
                        @Override
                        public void onError(Throwable e)
                        {
                            if(!model.handelLoadMoreError())
                            {
                                if(view!=null)
                                    view.onApiError(activity.getString(R.string.failed_get_members));
                            }
                        }
                        @Override
                        public void onComplete() {}
                    });
        }
    }

    @Override
    public void pee_fetchProfile(int position) {
        model.prefetchImage(position);
    }

    @Override
    public boolean checkLoadMore(int position)
    {
        return model.checkLoadMoreRequired(position);
    }

    @Override
    public void openUserProfile(int  position)
    {
        try {
            String user_Details=model.getUserDetails(position);
            if(view!=null)
            {
                view.openUserProfile(user_Details);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void tryLoadAgain()
    {
        isTryAgain=true;
        getListData(false);
    }
}
