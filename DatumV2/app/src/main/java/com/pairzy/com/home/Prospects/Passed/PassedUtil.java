package com.pairzy.com.home.Prospects.Passed;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.home.Prospects.Passed.Model.PassedItemPojo;
import com.pairzy.com.home.Prospects.Passed.Model.PassedUserAdapter;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import dagger.Module;
import dagger.Provides;
/**
 * @since   3/23/2018.
 * @author 3Embed.
 * @version 1.0
 */
@Module
public class PassedUtil
{
    @Provides
    @FragmentScoped
    ArrayList<PassedItemPojo> getList()
    {
        return new ArrayList<>();
    }

    @Provides
    @FragmentScoped
    PassedUserAdapter getUserListAdapter(ArrayList<PassedItemPojo> list, TypeFaceManager typeFaceManager, Utility utility)
    {
        return new PassedUserAdapter(list,typeFaceManager,utility);
    }

    @Provides
    @FragmentScoped
    LinearLayoutManager getLinearLayoutManager(Activity activity)
    {
        return new LinearLayoutManager(activity);
    }
}
