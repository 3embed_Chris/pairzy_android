package com.pairzy.com.coinWallet.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h>CoinPojo</h>
 * <p></p>
 * @author 3Embed.
 * @since 30/5/18.
 */
public class CoinPojo {

    @SerializedName("txnId")
    @Expose
    private String txnId;
    @SerializedName("txnType")
    @Expose
    private String txnType;
    @SerializedName("txnTypeCode")
    @Expose
    private Integer txnTypeCode;
    @SerializedName("trigger")
    @Expose
    private String trigger;
    @SerializedName("userPurchaseTime")
    @Expose
    private String userPurchaseTime;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("currencySymbol")
    @Expose
    private Object currencySymbol;
    @SerializedName("coinType")
    @Expose
    private String coinType;
    @SerializedName("coinOpeingBalance")
    @Expose
    private String coinOpeingBalance;
    @SerializedName("cost")
    @Expose
    private Integer cost;
    @SerializedName("coinAmount")
    @Expose
    private Integer coinAmount;
    @SerializedName("coinClosingBalance")
    @Expose
    private String coinClosingBalance;
    @SerializedName("paymentType")
    @Expose
    private String paymentType;
    @SerializedName("timestamp")
    @Expose
    private Long timestamp;
    @SerializedName("paymentTxnId")
    @Expose
    private String paymentTxnId;
    private boolean loading = false;
    private boolean loadingError = false;

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public Integer getTxnTypeCode() {
        return txnTypeCode;
    }

    public void setTxnTypeCode(Integer txnTypeCode) {
        this.txnTypeCode = txnTypeCode;
    }

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    public String getUserPurchaseTime() {
        return userPurchaseTime;
    }

    public void setUserPurchaseTime(String userPurchaseTime) {
        this.userPurchaseTime = userPurchaseTime;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Object getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(Object currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getCoinType() {
        return coinType;
    }

    public void setCoinType(String coinType) {
        this.coinType = coinType;
    }

    public String getCoinOpeingBalance() {
        return coinOpeingBalance;
    }

    public void setCoinOpeingBalance(String coinOpeingBalance) {
        this.coinOpeingBalance = coinOpeingBalance;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Integer getCoinAmount() {
        return coinAmount;
    }

    public void setCoinAmount(Integer coinAmount) {
        this.coinAmount = coinAmount;
    }

    public String getCoinClosingBalance() {
        return coinClosingBalance;
    }

    public void setCoinClosingBalance(String coinClosingBalance) {
        this.coinClosingBalance = coinClosingBalance;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getPaymentTxnId() {
        return paymentTxnId;
    }

    public void setPaymentTxnId(String paymentTxnId) {
        this.paymentTxnId = paymentTxnId;
    }

    public boolean isLoading() {
        return loading;
    }

    public boolean isLoadingError() {
        return loadingError;
    }
}
