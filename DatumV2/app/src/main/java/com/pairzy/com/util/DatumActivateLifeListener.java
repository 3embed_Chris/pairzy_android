package com.pairzy.com.util;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import com.pairzy.com.home.HomeActivity;
/**
 * <h2>DatumActivateLifeListener</h2>
 * <P>
 *
 * </P>
 * @since  4/13/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class DatumActivateLifeListener implements Application.ActivityLifecycleCallbacks
{
    private HomeActivity activity;

    public Activity isHomeActivityActive()
    {
        return activity;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle)
    {
        if(activity instanceof HomeActivity)
        {
            this.activity= (HomeActivity) activity;
        }

    }

    @Override
    public void onActivityStarted(Activity activity)
    {
        if(activity instanceof HomeActivity)
        {
            this.activity= (HomeActivity) activity;
        }
    }

    @Override
    public void onActivityResumed(Activity activity)
    {
        if(activity instanceof HomeActivity)
        {
            this.activity= (HomeActivity) activity;
        }
    }

    @Override
    public void onActivityPaused(Activity activity)
    {
        if(activity instanceof HomeActivity)
        {
            this.activity=null;
        }
    }

    @Override
    public void onActivityStopped(Activity activity)
    {
        if(activity instanceof HomeActivity)
        {
            this.activity=null;
        }
    }
    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle)
    {}

    @Override
    public void onActivityDestroyed(Activity activity)
    {
        if(activity instanceof HomeActivity)
        {
            this.activity=null;
        }
    }
}
