package com.pairzy.com.campaignScreen;

/**
 * <h>CampaignContract interface</h>
 * @author 3Embed.
 * @since 28/6/18.
 * @version 1.0.
 */
public interface CampaignContract {
    interface View{

    }

    interface Presenter{

    }
}
