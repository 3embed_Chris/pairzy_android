package com.pairzy.com.LeaderBoad;

import android.app.Activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.LeaderBoad.model.LeaderBoadAdapter;
import com.pairzy.com.LeaderBoad.model.LeaderBoardDataPOJO;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.progressbar.LoadingProgress;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class LeaderBoadUtil {

    @ActivityScoped
    @Provides
    ArrayList<LeaderBoardDataPOJO> provideArrayList(){
        return  new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    LeaderBoadAdapter provideLeaderBoadAdapter(Activity activity,ArrayList<LeaderBoardDataPOJO> arrayList, TypeFaceManager typeFaceManager){

        return new LeaderBoadAdapter(activity,arrayList,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    LinearLayoutManager provideLayoutManager(Activity activity){
        return new LinearLayoutManager(activity, RecyclerView.VERTICAL,false);
    }

    @Provides
    @ActivityScoped
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }
}
