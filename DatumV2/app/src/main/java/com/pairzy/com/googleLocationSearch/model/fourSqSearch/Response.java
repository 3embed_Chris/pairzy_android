package com.pairzy.com.googleLocationSearch.model.fourSqSearch;

import com.pairzy.com.data.model.fourSq.Venue;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * <h>Response pojo class</h>
 * @author 3Embed.
 * @since 9/5/18.
 * @version 1.0.
 */

public class Response {
    @SerializedName("venues")
    @Expose
    private ArrayList<Venue> venues;

    public ArrayList<Venue> getVenues() {
        return venues;
    }

    public void setVenues(ArrayList<Venue> venues) {
        this.venues = venues;
    }
}
