package com.pairzy.com.splash;

import android.app.Activity;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.splash.Model.SplashModel;
import com.pairzy.com.util.AppConfig;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h2>SplashPresenter</h2>
 *
 * @author 3Embed.
 * @version 1.0.
 * @since 04/01/2018.
 */
public class SplashPresenter implements SplashContract.Presenter
{
    @Inject
    NetworkService service;
    @Inject
    SplashContract.View view;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    SplashModel model;
    @Inject
    Activity activity;
    @Inject
    CoinConfigWrapper coinConfigWrapper;

    private boolean timerCompleted = false;
    private boolean updateCoinBalance = false;
    private boolean updateCoinConfig = false;
    private CompositeDisposable compositeDisposable;

    @Inject
    SplashPresenter() {
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void redirect()
    {
        boolean isFirstTime = dataSource.isSplashFirstTime();
        int loadTime = isFirstTime?AppConfig.SPLASH_SCREEN_DURATION:AppConfig.LOAD_SCREEN;
        if(dataSource.isSplashFirstTime()){
            dataSource.setSplashFirstTimeDone();
        }
        Completable.
                timer(loadTime, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe(new CompletableObserver() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onComplete() {
                        timerCompleted = true;
                        if(updateCoinBalance && updateCoinConfig){
                            if(view != null)
                                view.move();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }
    @Override
    public void checkAndGetAppData() {
        if(dataSource.isLoggedIn()){
            if(model.isCoinBalanceEmpty()) {
                loadCoinBalance();
            } else{
                updateCoinBalance = true;
            }

            if(coinConfigWrapper.getCoinData() == null) {
                callCoinConfigApi();
            }
            else{
                updateCoinConfig = true;
            }
        }
        else{
            updateCoinBalance = true;
            updateCoinConfig = true;
        }
    }


    @Override
    public void loadCoinBalance()
    {
        if(networkStateHolder.isConnected()) {
            if(view != null)
                view.showLoading();
            service.getCoinBalance(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                try {
                                    model.parseCoinBalance(value.body().string());
                                } catch (Exception e) {
                                }
                            } else if (value.code() == 401) {
                                AppController.getInstance().appLogout();
                            } else {
                                //TODO:show error and retry btn;
                            }
                            if(!model.isCoinBalanceEmpty()) {
                                updateCoinBalance = true;

                                if(timerCompleted && updateCoinConfig){
                                    if(view != null)
                                        view.move();
                                }
                            }
                            else{
                                if(view != null)
                                    view.showError(activity.getString(R.string.error_getting_data));
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            //TODO:show error and retry btn;
                            if(!model.isCoinBalanceEmpty()) {
                                if(timerCompleted){
                                    if(view != null)
                                        view.move();
                                }
                            }
                            else{
                                if(view != null)
                                    view.showError(activity.getString(R.string.error_getting_data));
                            }
                        }
                    });
        }
        else{
            if(view != null)
                view.showInternetError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void dispose() {
        compositeDisposable.clear();
    }


    private void callCoinConfigApi()
    {
        if(networkStateHolder.isConnected())
        {
            if(view != null)
                view.showLoading();
            service.getCoinConfig(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }
                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                try {
                                    model.parseCoinConfig(value.body().string());
                                } catch (Exception e) {
                                }
                            } else if(value.code() == 401)
                            {
                                AppController.getInstance().appLogout();
                            } else {
                                if (view != null)
                                    view.showError(activity.getString(R.string.failed_to_get_coin_config_data));
                            }

                            if (coinConfigWrapper.getCoinData() == null) {
                                if (view != null)
                                    view.showError(activity.getString(R.string.failed_to_get_coin_config_data));
                            } else {
                                updateCoinConfig = true;
                                if(timerCompleted && updateCoinBalance){
                                    if(view != null)
                                        view.move();
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null)
                                view.showError(activity.getString(R.string.failed_to_get_coin_config_data));
                        }
                    });
        }
        else {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


}
