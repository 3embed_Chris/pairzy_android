package com.pairzy.com.mobileverify;

import android.app.Activity;
import android.content.Context;
import android.location.Geocoder;

import com.pairzy.com.dagger.ActivityScoped;

import java.util.Locale;

import dagger.Module;
import dagger.Provides;
/**
 * @since  2/6/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class MobileVerifyUtil
{
    @ActivityScoped
    @Provides
    AnimatorHandler getAnimationHandler(Context context)
    {
        return new AnimatorHandler(context);
    }

    @ActivityScoped
    @Provides
    Geocoder provideGeocoder(Activity activity)
    {
        return new Geocoder(activity, Locale.getDefault());
    }

}
