package com.pairzy.com.mobileverify.result;

import android.app.Activity;
import android.text.TextUtils;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.mobileverify.MobileVerifyActivity;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.AppConfig;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import butterknife.BindString;
import io.reactivex.Completable;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * <h2>ResultPresenter</h2>
 * <p>
 * Implementation of  @{@link ResultContract.Presenter}
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 06/01/2018.
 **/
public class ResultPresenter implements ResultContract.Presenter
{
    private CompositeDisposable compositeDisposable;
    @Inject
    NetworkStateHolder holder;
    @Inject
    ResultModel model;
    @BindString(R.string.enter_valid_phno)
    String error_on_phno;
    @Inject
    NetworkService service;
    @Inject
    Activity activity;
    @Inject
    PreferenceTaskDataSource dataSource;

    private ResultContract.View view;

    @Inject
    ResultPresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(Object view)
    {
        this.view = (ResultContract.View) view;
    }

    @Override
    public void dropView()
    {
        this.view =null;
        compositeDisposable.clear();
    }

    @Override
    public void RequestOtpMobile(String countryCode, String mobile_number)
    {
        if(view==null)
            return;
        if(TextUtils.isEmpty(countryCode)||TextUtils.isEmpty(mobile_number))
        {
            view.showMessage(error_on_phno);
        }else
        {
            doApiCall(countryCode+""+mobile_number);
        }
    }

    /*
     *Doing api call*/
    private void doApiCall(String mobile_number)
    {
        if(!holder.isConnected())
        {
            if(view!=null)
            {
                view.updateResponseUI(activity.getString(R.string.failed_verify));
                view.showMessage(activity.getString(R.string.no_internet_error));
            }
        }else
        {
            if(view!=null)
                view.updateMessageText(activity.getString(R.string.verifying_your_number_text));
            service.verifyMobile(model.getAuthorization(), model.getLanguage(),model.verifyParams(mobile_number))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> result)
                        {
                            try
                            {
                                if(result.code()==200)
                                {
                                    if(MobileVerifyActivity.EDIT_PROFILE_TAG != null) {
                                        //show message already number exist.
                                        if(view != null)
                                            view.showMessage("Phone number already exist!!");
                                        if(view != null)
                                            view.goBackToMainPage();
                                    }
                                    else{
                                        if (view != null)
                                            view.mobileAvailable();
                                    }
                                }else if(result.code() == 412)
                                {
                                    if(MobileVerifyActivity.EDIT_PROFILE_TAG != null) {
                                        if(view != null)
                                            view.onlyRequestForOtp();
                                    }
                                    else {
                                        if (view != null)
                                            view.mobileNotAvailable();
                                    }
                                }else
                                {
                                    if(view!=null)
                                        view.updateResponseUI(model.getError(result));
                                }
                            }catch (Exception e)
                            {
                                e.printStackTrace();
                                if(view!=null)
                                    view.updateResponseUI(activity.getString(R.string.failed_verify));

                            }
                        }
                        @Override
                        public void onError(Throwable errorMsg) {
                            if(view!=null)
                                view.updateResponseUI(activity.getString(R.string.failed_verify));

                        }
                        @Override
                        public void onComplete() {}
                    });
        }
    }

    @Override
    public void sendOtp(String mobile_number)
    {
        if(!holder.isConnected())
        {
            if(view!=null)
            {
                view.updateResponseUI(activity.getString(R.string.failed_sendOtp));
                view.showMessage(activity.getString(R.string.no_internet_error));
            }
        }else
        {
            service.requestOtp(model.getAuthorization(), model.getLanguage(), model.requestOtpParams(mobile_number,MobileVerifyActivity.EDIT_PROFILE_TAG != null))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> result)
                        {
                            try
                            {
                                if (result.code() == 200)
                                {
                                    if (view != null)
                                        view.otpSent();
                                }
                                else
                                {
                                    if(view!=null)
                                        view.updateResponseUI(model.getError(result));
                                }
                            }catch (Exception e)
                            {
                                if(view!=null)
                                    view.updateResponseUI(activity.getString(R.string.failed_sendOtp));
                            }

                        }
                        @Override
                        public void onError(Throwable errorMsg)
                        {
                            if(view!=null)
                                view.updateResponseUI(activity.getString(R.string.failed_sendOtp));
                        }
                        @Override
                        public void onComplete() {}
                    });
        }
    }

    @Override
    public void verifyOtp(String countryCode, String phno, String otp)
    {
        if(!holder.isConnected())
        {
            if(view!=null)
            {
                view.updateResponseUI(activity.getString(R.string.failed_VerfyOtp));
                view.showMessage(activity.getString(R.string.no_internet_error));
            }
        }else
        {

            if(view!=null)
                view.updateMessageText(activity.getString(R.string.veryfying_otp_text));

            if(MobileVerifyActivity.EDIT_PROFILE_TAG != null){
                //for verifying otp for change number
                callVerifyOptApiToChangeNum(countryCode,phno,otp);
            }else {
                //normal sign up flow
                callVerifyOptApi(countryCode+""+phno, otp);

            }
        }
    }

    private void callVerifyOptApiToChangeNum(String countryCode, String phno, String otp) {
        service.verifyOtpToChangeNumber(dataSource.getToken(),model.getLanguage(),model.getVerifyOptForChangeNumData(countryCode,phno,otp))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(Response<ResponseBody> result) {
                        try
                        {
                            if (result.code()==200)
                            {
                                String data=result.body().string();
                                if(view != null)
                                    view.finishActivity(phno);
                            } else if(view!=null)
                                view.updateResponseUI(model.getError(result));
                        }catch (Exception e)
                        {
                            if(view!=null)
                                view.updateResponseUI(activity.getString(R.string.failed_VerfyOtp));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view!=null)
                            view.updateResponseUI(activity.getString(R.string.failed_sendOtp));
                    }
                });
    }

    private void callVerifyOptApi(String phno,String otp) {
        service.verifyOtp(model.getAuthorization(),model.getLanguage(),model.verifyOtpData(phno,otp))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> result)
                    {
                        try
                        {
                            if (result.code()==200)
                            {
                                String data=result.body().string();

                                boolean isSignUp = model.parseVerificationRes(data);
                                if (isSignUp) {
                                    if (view != null)
                                        view.openSingUp();
                                } else {
                                    if (view != null)
                                        view.onLogin();
                                }

                            } else if(view!=null)
                                view.updateResponseUI(model.getError(result));
                        }catch (Exception e)
                        {
                            if(view!=null)
                                view.updateResponseUI(activity.getString(R.string.failed_VerfyOtp));
                        }
                    }
                    @Override
                    public void onError(Throwable errorMsg)
                    {
                        if(view!=null)
                            view.updateResponseUI(activity.getString(R.string.failed_VerfyOtp));
                    }
                    @Override
                    public void onComplete() {}
                });
    }


    @Override
    public void openOtpPage()
    {
        Completable.timer(AppConfig.LOAD_SCREEN, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe(() ->{
                    if(view!=null)
                        view.openOtpFrg();
                });
    }

    @Override
    public void showMessage(String string)
    {
        if(view!=null)
            view.showMessage(string);
    }
}
