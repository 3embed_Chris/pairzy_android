package com.pairzy.com.UserPreference.EditInputFrg;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatRadioButton;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.UserPreference.Model.PreferenceItem;
import com.pairzy.com.UserPreference.MyPreferencePageContract;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * <h2>UserInputFrg</h2>
 * <P>
 * A simple {@link Fragment} subclass.
 * </P>
 * @author 3embed.
 * @version 1.0.
 * @since 17-02-2017.
 */
@ActivityScoped
public class UserInputFrg extends DaggerFragment implements UserInputContract.View,TextWatcher
{
    public static final String ITEM_POSITION="pref_item";
    @Inject
    Utility utility;
    @Inject
    Activity activity;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    MyPreferencePageContract.Presenter mainpresenter;
    @Inject
    UserInputContract.Presenter presenter;
    private Unbinder unbinder;
    @BindView(R.id.skip_page)
    TextView skip_page;
    @BindView(R.id.first_title)
    TextView first_title;
    @BindView(R.id.second_title)
    TextView second_title;
    @BindView(R.id.input_name)
    EditText input_name;
    @BindView(R.id.btnNext)
    RelativeLayout btnNext;
    @BindView(R.id.back_button_img)
    ImageView back_button_img;
    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;
    private PreferenceItem current_item;
    private int item_position,list_number;
    @BindView(R.id.not_to_say)
    AppCompatRadioButton not_to_say;
    private boolean IsPereferedNotSay;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle bundle=getArguments();
        assert bundle != null;
        item_position=bundle.getInt(ITEM_POSITION);
    }

    @Inject
    public UserInputFrg() {}
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_user_input_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder= ButterKnife.bind(this,view);
        presenter.takeView(this);
        upDateUI();
        collectData();
    }
    /*
    * Setting the current preference.*/
    public void setPreferenceItem(PreferenceItem item)
    {
        current_item=item;
        list_number=current_item.getList_no();
    }

    /*
   * Updating the required ui updating like fonts etc.*/
    private void upDateUI()
    {
        if(item_position==0)
        {
            back_button_img.setImageDrawable(utility.getVectorDrawable(R.drawable.ic_cross_svg));
        }
        btnNext.setEnabled(false);
        skip_page.setTypeface(typeFaceManager.getCircularAirBook());
        first_title.setTypeface(typeFaceManager.getCircularAirBold());
        second_title.setTypeface(typeFaceManager.getCircularAirBold());
        input_name.setTypeface(typeFaceManager.getCircularAirBook());
        input_name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        input_name.addTextChangedListener(this);
        not_to_say.setTypeface(typeFaceManager.getCircularAirBook());
        input_name.setOnEditorActionListener((v, actionId, event) -> {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE))
            {
                if(input_name.getText().toString().length()>0)
                {
                    presenter.updatePreference(current_item.getId(),input_name.getText().toString());
                }else
                {
                    presenter.showError();
                }
            }
            return false;
        });
        utility.openSoftInputKeyInDelay(activity,input_name);
    }

    /*
     *collecting the details. */
    private void collectData()
    {
        first_title.setText(utility.formatString(current_item.getTitle()));
        second_title.setText(utility.formatString(current_item.getLabel()));
        progress_bar.setMax(current_item.getMax_count());
        progress_bar.setProgress(item_position+1);
        if(current_item.getOptions().size()>0)
        {
            input_name.setHint(current_item.getOptions().get(0));
        }

    }

    @Override
    public void onResume()
    {
        super.onResume();
        input_name.requestFocus();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
    }

    @OnClick(R.id.close_button)
    void onBackClicked()
    {
        utility.closeSpotInputKey(activity,input_name);
        activity.onBackPressed();
    }
    @OnClick(R.id.parentLayout)
    void onParentClicked()
    {
        utility.closeSpotInputKey(activity,input_name);
    }
    @OnClick(R.id.skip_page)
    void onSkip()
    {
        mainpresenter.openHomePage();
    }

    @OnClick(R.id.btnNext)
    void onNextClicked()
    {
        if(IsPereferedNotSay)
        {
            mainpresenter.openNextFrag(list_number,item_position+1);
        }else
        {
            if(input_name.getText().toString().length()>0)
            {
                presenter.updatePreference(current_item.getId(),input_name.getText().toString());
            }else
            {
                presenter.showError();
            }
        }
    }


    @OnClick(R.id.not_to_say)
    void onRadioButtonChecked(RadioButton radioButton)
    {
        if(radioButton.isSelected())
        {
            radioButton.setSelected(false);
            radioButton.setChecked(false);
        }else
        {
            radioButton.setSelected(true);
            radioButton.setChecked(true);
        }
        IsPereferedNotSay=radioButton.isSelected();
        if(!IsPereferedNotSay)
        {
            if(input_name.getText().length()>0)
            {
                handelNextButton(true);
            }else
            {
                handelNextButton(false);
            }

        }else
        {
            input_name.getText().clear();
            handelNextButton(true);
        }
    }


    @Override
    public void updateUserInput(String data)
    {
        //Store this data for later selection
        mainpresenter.openNextFrag(list_number,item_position+1);
    }

    @Override
    public void showMessage(String message)
    {
        mainpresenter.showMessage(message);
    }

    @Override
    public void showError(String error)
    {
        mainpresenter.showError(error);
    }

    @Override
    public String getErrorTitle() {
        return second_title.getText().toString();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2){}
    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {
        handelNextButton(charSequence.toString().length()>0);
    }
    @Override
    public void afterTextChanged(Editable editable) {}
    /*
    *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            btnNext.setEnabled(true);
        }
        else {
            btnNext.setEnabled(false);
        }
    }
}
