package com.pairzy.com.dublyCamera.CameraInFragments;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.pairzy.com.PostMoments.PostActivity;
import com.pairzy.com.R;
import com.pairzy.com.Utilities.Constants;
import com.pairzy.com.dublyCamera.ResultHolder;
import com.pairzy.com.util.AppConfig;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.File;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class PreviewFragmentVideoActivity extends AppCompatActivity {

    private static final String TAG = PreviewFragmentVideoActivity.class.getSimpleName();

    @BindView(R.id.flContainer)
    FrameLayout flContainer;
    @BindView(R.id.sdv_post_review)
    SimpleDraweeView  mediaCoverImage;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.media_container)
    FrameLayout mediaContainer;
    @BindView(R.id.ivVolumeControl)
    ImageView volumeControl;
    @BindView(R.id.cl_root)
    CoordinatorLayout parent;

    private int videoSurfaceDefaultHeight;
    private int screenDefaultHeight;

    private PlayerView videoSurfaceView;
    private SimpleExoPlayer videoPlayer;
    private boolean isVideoViewAdded;
    private VolumeState volumeState = VolumeState.ON;
    private RequestManager requestManager;

    enum VolumeState{
        ON,OFF
    }

    private View.OnClickListener videoViewClickListener = v -> toggleVolume();

    private void toggleVolume() {
        if (videoPlayer != null) {
            if (volumeState == VolumeState.OFF) {
                Log.d(TAG, "togglePlaybackState: enabling volume.");
                setVolumeControl(VolumeState.ON);
            } else if (volumeState == VolumeState.ON) {
                Log.d(TAG, "togglePlaybackState: disabling volume.");
                setVolumeControl(VolumeState.OFF);
            }
        }
    }

    private Unbinder unbinder;
    private String videoPath;
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_fragmentvideo_preview);
        unbinder = ButterKnife.bind(this);
        onNewIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        videoPath = getIntent().getStringExtra("videoPath");
        Log.d(TAG, "onCreate: videoPath: "+videoPath);
        if (videoPath == null) {
            //video
            supportFinishAfterTransition();
        }
        initGlide();
        initVideoPlayer();
    }

    private void initGlide() {
        requestManager = Glide.with(this).setDefaultRequestOptions(new RequestOptions());
    }

    private void initVideoPlayer() {
        Display display = ((WindowManager) Objects.requireNonNull(getSystemService(Context.WINDOW_SERVICE))).getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);

        videoSurfaceDefaultHeight = point.x;
        screenDefaultHeight = point.y;

        videoSurfaceView = new PlayerView(this);
        videoSurfaceView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

        //Create the player using ExoPlayerFactory
        videoPlayer = ExoPlayerFactory.newSimpleInstance(this,trackSelector);
        //Disable Player Control
        videoSurfaceView.setUseController(false);
        //Bind the player to the view.
        videoSurfaceView.setPlayer(videoPlayer);
        //Turn on Volume.
        setVolumeControl(VolumeState.ON);

        //TODO set file uri.
        Uri imageUri= Uri.fromFile(new File(videoPath));// For files on device
        mediaCoverImage.setImageURI(imageUri);
        mediaCoverImage.setVisibility(VISIBLE);

        videoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState){
                    case Player.STATE_BUFFERING:
                        Log.e(TAG, "onPlayerStateChanged: Buffering video.");
                        if(progressBar != null){
                            progressBar.setVisibility(VISIBLE);
                        }
                        break;
                    case Player.STATE_ENDED:
                        Log.d(TAG, "onPlayerStateChanged: Video ended.");
                        videoPlayer.seekTo(0);
                        break;
                    case Player.STATE_IDLE:
                        break;
                    case Player.STATE_READY:
                        Log.e(TAG, "onPlayerStateChanged: Ready to Play.");
                        if(progressBar != null){
                            progressBar.setVisibility(GONE);
                        }
                        if(!isVideoViewAdded){
                            addVideoView();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });
    }

    private void addVideoView() {
        mediaContainer.addView(videoSurfaceView);
        isVideoViewAdded = true;
        videoSurfaceView.requestFocus();
        videoSurfaceView.setVisibility(VISIBLE);
        videoSurfaceView.setAlpha(1);
        mediaCoverImage.setVisibility(GONE);
    }

    private void resetVideoView() {
        if (isVideoViewAdded) {
            removeVideoView(videoSurfaceView);
            videoSurfaceView.setVisibility(INVISIBLE);
            mediaCoverImage.setVisibility(VISIBLE);
        }
    }

    // Remove the old player
    private void removeVideoView(PlayerView videoView) {
        ViewGroup parent = (ViewGroup) videoView.getParent();
        if (parent == null) {
            return;
        }
        int index = parent.indexOfChild(videoView);
        if (index >= 0) {
            parent.removeViewAt(index);
            isVideoViewAdded = false;
            this.parent.setOnClickListener(null);
        }
    }


    private void setVolumeControl(VolumeState state) {
        volumeState = state;
        if (state == VolumeState.OFF) {
            videoPlayer.setVolume(0f);
            animateVolumeControl();
        } else if (state == VolumeState.ON) {
            videoPlayer.setVolume(1f);
            animateVolumeControl();
        }
    }

    private void animateVolumeControl(){

        if (volumeControl != null) {
            volumeControl.bringToFront();
            if (volumeState == VolumeState.OFF) {
                requestManager.load(R.drawable.ic_volume_off)
                        .into(volumeControl);
            } else if (volumeState == VolumeState.ON) {
                requestManager.load(R.drawable.ic_volume_on)
                        .into(volumeControl);
            }
            volumeControl.animate().cancel();

            volumeControl.setAlpha(1f);

            volumeControl.animate()
                    .alpha(0f)
                    .setDuration(600).setStartDelay(1000);
        }
        playVideo();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void playVideo(){

        if(videoPath == null){
            videoPlayer.setPlayWhenReady(false);
            return;
        }

        //set the position of the list-item that is to be played
        if(videoSurfaceView == null){
            return;
        }

        //remove any old surface views from previously playing videos
        videoSurfaceView.setVisibility(INVISIBLE);
        removeVideoView(videoSurfaceView);

        videoSurfaceView.setPlayer(videoPlayer);
        parent.setOnClickListener(videoViewClickListener);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this,getString(R.string.app_name)));

        if(videoPath != null){
            MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                    .createMediaSource(Uri.parse(videoPath));
            videoPlayer.prepare(videoSource);
            videoPlayer.setPlayWhenReady(true);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        resetVideoView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (videoPlayer != null)
                videoPlayer.setPlayWhenReady(true);
        }catch (Exception e){}
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (videoPlayer != null)
                videoPlayer.setPlayWhenReady(false);
        }catch (Exception e){
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ResultHolder.dispose();
        if(videoPlayer != null)
            videoPlayer.release();
        unbinder.unbind();
    }

    @OnClick(R.id.ivNext)
    public void next() {
        startNextActivity();
    }

    @OnClick(R.id.back_button)
    public void back() {
        finish();
    }


    private void startNextActivity() {
        Intent intent;
        intent = new Intent(this, PostActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(Constants.Post.PATH, videoPath);
        intent.putExtra(Constants.Post.TYPE, Constants.Post.VIDEO);
        startActivityForResult(intent, AppConfig.POST_ACTIVITY_REQ_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK && requestCode == AppConfig.POST_ACTIVITY_REQ_CODE){
            setResult(RESULT_OK,getIntent());
            finish();
        }
    }

}
