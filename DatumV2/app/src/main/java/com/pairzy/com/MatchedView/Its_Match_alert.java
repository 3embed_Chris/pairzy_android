package com.pairzy.com.MatchedView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.pairzy.com.AppController;
import com.pairzy.com.MqttChat.Calls.CallingApis;
import com.pairzy.com.MqttChat.Calls.Common;
import com.pairzy.com.MqttChat.Utilities.Utilities;
import com.pairzy.com.R;
import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.Model.DateType;
import com.pairzy.com.home.HomeActivity;
import com.pairzy.com.planCallDate.CallDateActivity;
import com.pairzy.com.planDate.DateActivity;
import com.pairzy.com.splash.SplashActivity;
import com.pairzy.com.util.ImageChecker.ImageLoader;
import com.pairzy.com.util.TypeFaceManager;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.NOTIFICATION_SERVICE;
import static com.pairzy.com.util.AppConfig.NOTIFICATION_CHANNEL_ID_DATUM_NOTIFICATION;
import static com.pairzy.com.util.AppConfig.NOTIFICATION_CHANNEL_NAME_DATUM_NOTIFICATION;

/**
 * <h2>Its_Match_alert</h2>
 * <P>
 *
 * </P>
 * @since  3/21/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class Its_Match_alert
{
    private static NotificationManager notificationManager;
    private static Notification notification;
    private Dialog dialog_of_match;
    private Context context;
    private TypeFaceManager typeFaceManager;
    private PreferenceTaskDataSource dataSource;
    private MatchAnimation animatorHandler;
    private  RelativeLayout get_on_date_view,chat_with_user_view,keep_swipping_view;
    private TextView get_on_date_text,chat_with_user_text,keep_swipping_text;
    private DateAlertDialog dateAlertDialog;
    private int notification_id=201;
    private Activity activity;
    String user_image, user_id,user_name;
    private String chatId;
    private boolean isSuperliked;

    public Its_Match_alert(Context context,PreferenceTaskDataSource dataSource,TypeFaceManager typeFaceManager,MatchAnimation animationHandler,DateAlertDialog dateAlertDialog)
    {
        this.context =context;
        this.typeFaceManager=typeFaceManager;
        this.dataSource=dataSource;
        this.animatorHandler=animationHandler;
        this.dateAlertDialog=dateAlertDialog;
    }

    /*
     *Showing the match alert */
    public void show_info_alert(Activity activity, String user_image, String user_id, String user_name,String chatId, boolean isSuperlikedMe)
    {
        this.activity = activity;
        this.user_image = user_image;
        this.user_id = user_id;
        this.user_name = user_name;
        this.chatId = chatId;
        this.isSuperliked = isSuperliked;

        if(dialog_of_match !=null)
        {
            if(dialog_of_match.isShowing())
            {
                dialog_of_match.dismiss();
            }
        }
        ColorDrawable cd=new ColorDrawable();
        cd.setColor(Color.TRANSPARENT);
        dialog_of_match = new Dialog(activity);
        Window window1=dialog_of_match.getWindow();
        assert window1 != null;
        window1.setBackgroundDrawable(cd);
        dialog_of_match.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.its_matched_alert_layout,null);
        ImageView heart_1=dialogView.findViewById(R.id.heart_1);
        doSlowBlinkAnimation(heart_1);
        ImageView heart_2=dialogView.findViewById(R.id.heart_2);
        doBitSlowBlinkAnimation(heart_2);
        ImageView heart_3=dialogView.findViewById(R.id.heart_3);
        doSlowBlinkAnimation(heart_3);
        ImageView heart_4=dialogView.findViewById(R.id.hart_4);
        doBitSlowBlinkAnimation(heart_4);
        ImageView heart_5=dialogView.findViewById(R.id.heart_5);
        doFastBlinkAnimation(heart_5);
        ImageView heart_7=dialogView.findViewById(R.id.hart_7);
        doBitSlowBlinkAnimation(heart_7);
        ImageView heart_16=dialogView.findViewById(R.id.heart_16);
        doSlowBlinkAnimation(heart_16);

        TextView title=dialogView.findViewById(R.id.title);
        title.setTypeface(typeFaceManager.getCircularAirBold());
        TextView sub_title=dialogView.findViewById(R.id.sub_title);
        sub_title.setTypeface(typeFaceManager.getCircularAirBook());
        sub_title.setText(String.format(Locale.getDefault(), "you and %s like each other!",user_name));

        SimpleDraweeView my_profile_pic=dialogView.findViewById(R.id.my_profile_pic);
        my_profile_pic.setImageURI(dataSource.getProfilePicture());
        SimpleDraweeView user_profile_pic=dialogView.findViewById(R.id.user_profile_pic);
        user_profile_pic.setImageURI(user_image);

        get_on_date_view=dialogView.findViewById(R.id.get_on_date_view);
        get_on_date_text=dialogView.findViewById(R.id.get_on_date_text);
        get_on_date_text.setTypeface(typeFaceManager.getCircularAirBold());
        get_on_date_view.setOnClickListener(view -> {
            handelSelection(0);
            handelDateVisible(activity);
        });

        chat_with_user_view=dialogView.findViewById(R.id.chat_with_user_view);
        chat_with_user_text=dialogView.findViewById(R.id.chat_with_user_text);
        chat_with_user_text.setText(String.format(Locale.ENGLISH,"%s %s",activity.getString(R.string.chat_with_text),user_name));
        chat_with_user_text.setTypeface(typeFaceManager.getCircularAirBold());
        chat_with_user_view.setOnClickListener(view -> {
            handelSelection(1);
            //handelDateVisible(activity);
            //Intent intent = new Intent(activity ,null);
            //activity.startActivity(intent);
            launchChat();
            dismiss();
        });

        keep_swipping_view=dialogView.findViewById(R.id.keep_swipping_view);
        keep_swipping_text=dialogView.findViewById(R.id.keep_swipping_text);
        keep_swipping_text.setTypeface(typeFaceManager.getCircularAirBold());
        keep_swipping_view.setOnClickListener(view -> {
            handelSelection(2);
            dismiss();
            //need to launch home screen or just need to dismiss this dialog.

            Intent intent = new Intent(activity,HomeActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("type",72);
            intent.putExtra("data",bundle);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(intent);
        });



        dialog_of_match.setContentView(dialogView);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog_of_match.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        dialog_of_match.setCancelable(false);
        handelSelection(0);
        dialog_of_match.show();
    }

    private void launchChat() {

        Intent intent = new Intent(activity, ChatMessageActivity.class);
        intent.putExtra("receiverUid", user_id);
        intent.putExtra("receiverName", user_name);
        String docId = AppController.getInstance().findDocumentIdOfReceiver(user_id, "");
        if (docId.isEmpty()) {
            docId = AppController.findDocumentIdOfReceiver(user_id, Utilities.tsInGmt(), user_name,
                    user_image, "", false, user_id, chatId, false);
            AppController.getInstance().getDbController().updateChatIntiated(docId, true);
        }
        intent.putExtra("documentId", docId);
        intent.putExtra("receiverIdentifier", user_id);
        intent.putExtra("receiverImage", user_image);
        intent.putExtra("colorCode", AppController.getInstance().getColorCode(0 % 19));

        AppController.getInstance().getDbController().updateChatUserMatched(docId, true);
        AppController.getInstance().getDbController().updateChatUserOnline(docId, true);
        AppController.getInstance().getDbController().updateChatUserSuperlikedMe(docId, isSuperliked);
        launchChatScreen(intent);

        ((HomeActivity)activity).setNeedToUpdateChat(true);
    }

    private void launchChatScreen(Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        activity.startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(activity).toBundle());
    }


    private void dismiss(){
        if(dialog_of_match !=null)
        {
            if(dialog_of_match.isShowing())
            {
                dialog_of_match.dismiss();
            }
        }
    }
    /*
     *handling the button selector */
    private void handelSelection(int position)
    {
        switch (position)
        {
            case 0:
                get_on_date_view.setSelected(true);
                get_on_date_text.setTextColor(ContextCompat.getColor(context,R.color.softDeepGray));
                chat_with_user_view.setSelected(false);
                chat_with_user_text.setTextColor(ContextCompat.getColor(context,R.color.white));
                keep_swipping_view.setSelected(false);
                keep_swipping_text.setTextColor(ContextCompat.getColor(context,R.color.white));
                break;
            case 1:
                get_on_date_view.setSelected(false);
                get_on_date_text.setTextColor(ContextCompat.getColor(context,R.color.white));
                chat_with_user_view.setSelected(true);
                chat_with_user_text.setTextColor(ContextCompat.getColor(context,R.color.softDeepGray));
                keep_swipping_view.setSelected(false);
                keep_swipping_text.setTextColor(ContextCompat.getColor(context,R.color.white));
                break;
            case 2:
                get_on_date_view.setSelected(false);
                get_on_date_text.setTextColor(ContextCompat.getColor(context,R.color.white));
                chat_with_user_view.setSelected(false);
                chat_with_user_text.setTextColor(ContextCompat.getColor(context,R.color.white));
                keep_swipping_view.setSelected(true);
                keep_swipping_text.setTextColor(ContextCompat.getColor(context,R.color.softDeepGray));
                break;
        }
    }

    /*
     *Handling the alert view form. */
    private void handelDateVisible(Activity activity)
    {
        dateAlertDialog.showAlert(activity,animatorHandler.getPlaceAnimation(),new DateAlertCallback() {
            @Override
            public void onCallDate() {
                //doVoiceCall();
                //dialog_of_match.dismiss();
                Intent intent = new Intent(activity, CallDateActivity.class);
                intent.putExtra("user_id", user_id);
                intent.putExtra("user_image", user_image);
                intent.putExtra("user_name", user_name);
                intent.putExtra("date_type", DateType.AUDIO_DATE.getValue());
                activity.startActivity(intent);
                dismiss();
            }

            @Override
            public void onPhysicalDate()
            {
                Intent intent = new Intent(activity, DateActivity.class);
                intent.putExtra("user_id", user_id);
                intent.putExtra("user_image", user_image);
                intent.putExtra("user_name", user_name);
                activity.startActivity(intent);
                dismiss();
            }

            @Override
            public void onVideoDate() {
                //doVideoCall();
                Intent intent = new Intent(activity, CallDateActivity.class);
                intent.putExtra("user_id", user_id);
                intent.putExtra("user_image", user_image);
                intent.putExtra("user_name", user_name);
                intent.putExtra("date_type", DateType.VIDEO_DATE.getValue());
                activity.startActivity(intent);
                dismiss();
            }
        });
    }

    private void doVideoCall() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(activity) || !Settings.canDrawOverlays(activity)) {
                if (!Settings.System.canWrite(activity)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                    intent.setData(Uri.parse("package:" + activity.getPackageName()));
                    activity.startActivity(intent);
                }


                //If the draw over permission is not available open the settings screen
                //to grant the permission.

                if (!Settings.canDrawOverlays(activity)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + activity.getPackageName()));
                    activity.startActivity(intent);
                }
            } else {
                //showCallTypeChooserPopup(initiateDate);
                initiateVideoCall();
            }
        } else {
            //showCallTypeChooserPopup(initiateDate);
            initiateVideoCall();
        }
    }

    private void initiateVideoCall(){
        ArrayList<String> arr1 = new ArrayList<>();
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            arr1.add(Manifest.permission.CAMERA);
        }


        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {


            arr1.add(Manifest.permission.RECORD_AUDIO);

        }


        if (arr1.size() > 0) {

            ActivityCompat.requestPermissions(activity, arr1.toArray(new String[arr1.size()]),
                    72);
        } else {
            requestVideoCall();
        }
    }


    private void requestVideoCall() {

        Map<String, Object> callItem = new HashMap<>();
        String callId = AppController.getInstance().randomString();

        callItem.put("receiverName", user_name);
        callItem.put("receiverImage", user_image);
        callItem.put("receiverUid", user_id);
        callItem.put("callTime", Utilities.tsInGmt());
        callItem.put("callInitiated", true);
        callItem.put("callId", callId);
        callItem.put("callType", activity.getResources().getString(R.string.VideoCall));
        callItem.put("receiverIdentifier", user_id);
        //AppController.getInstance().getDbController().addNewCall(AppController.getInstance().getCallsDocId(), callItem);
        Common.callerName = user_name;
//        Common.callerName = user_id;
        //Date id
        CallingApis.initiateCall(activity,"", user_id, user_name, user_image,
                "1", user_id, callId);

    }

    private void doVoiceCall() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(activity) || !Settings.canDrawOverlays(activity)) {
                if (!Settings.System.canWrite(activity)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                    intent.setData(Uri.parse("package:" + activity.getPackageName()));
                    activity.startActivity(intent);
                }


                //If the draw over permission is not available open the settings screen
                //to grant the permission.

                if (!Settings.canDrawOverlays(activity)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + activity.getPackageName()));
                    activity.startActivity(intent);
                }
            } else {
                //showCallTypeChooserPopup(initiateDate);
                intiateAudioCall();
            }
        } else {
            //showCallTypeChooserPopup(initiateDate);
            intiateAudioCall();
        }
        //dismiss();
    }

    private void intiateAudioCall(){
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.RECORD_AUDIO},
                    71);
        } else {
            requestAudioCall();
        }
    }

    private void requestAudioCall() {
        Map<String, Object> callItem = new HashMap<>();

        String callId = AppController.getInstance().randomString();
        callItem.put("receiverName", user_name);
        callItem.put("receiverImage", user_image);
        callItem.put("receiverUid", user_id);
        callItem.put("callTime", Utilities.tsInGmt());
        callItem.put("callInitiated", true);
        callItem.put("callId", callId);
        callItem.put("callType", activity.getResources().getString(R.string.AudioCall));
        callItem.put("receiverIdentifier", user_id);
        //AppController.getInstance().getDbController().addNewCall(AppController.getInstance().getCallsDocId(), callItem);
        Common.callerName = user_name;

        CallingApis.initiateCall(activity,"",user_id, user_name, user_image,
                "0", user_id, callId);

    }


    /*
     *Doing slow blinking of the heart animation */
    private void doSlowBlinkAnimation(View view)
    {
        Animation animation=animatorHandler.getSlowBlinkingAnimation();
        view.setAnimation(animation);
        animation.start();
    }

    /*
    *Doing slow blinking of the heart animation */
    private void doBitSlowBlinkAnimation(View view)
    {
        Animation animation=animatorHandler.getBitSlowBlinkingAnimation();
        view.setAnimation(animation);
        animation.start();
    }

    /*
     *Doing slow blinking of the heart animation */
    private void doFastBlinkAnimation(View view)
    {
        Animation animation=animatorHandler.getFastBlinkingAnimation();
        view.setAnimation(animation);
        animation.start();
    }

    /*
     *Showing the notification */
    public void showNotification(String user_image, String user_id, String user_name)
    {
        RemoteViews viewBig = new RemoteViews(context.getPackageName(), R.layout.big_notification_view);
        RemoteViews viewSmall = new RemoteViews(context.getPackageName(), R.layout.match_small_notification_view);
        //Notification

        //open app pending intent
        Intent intent = new Intent(context, SplashActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("user_id",user_id);
        bundle.putString("user_image",user_image);
        bundle.putString("user_name",user_name);
        bundle.putString("chat_id",chatId);
        bundle.putBoolean("is_superliked",isSuperliked);
        bundle.putInt("type",51);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("data",bundle);
        PendingIntent pendingIntent = PendingIntent.getActivity(context.getApplicationContext(),(int) System.currentTimeMillis(),intent,0);


        //open physical date pending intent
//        Intent intent1 = new Intent(context, SplashActivity.class);
//        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        Bundle bundle1 = new Bundle();
//        bundle1.putString("user_id",user_id);
//        bundle1.putString("user_image",user_image);
//        bundle1.putString("user_name",user_name);
//        bundle1.putInt("type",51);
//        intent1.putExtra("data",bundle1);
//        PendingIntent physicalDatePendingIntent = PendingIntent.getActivity(context.getApplicationContext(), 11 ,intent1,PendingIntent.FLAG_UPDATE_CURRENT);

        //open call date pending intent
        Intent intent2 = new Intent(context, SplashActivity.class);
        intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bundle bundle2 = new Bundle();
        bundle2.putString("user_id",user_id);
        bundle2.putString("user_image",user_image);
        bundle2.putString("user_name",user_name);
        bundle2.putInt("type",61);
        intent2.putExtra("data",bundle2);
        PendingIntent callDatePendingIntent = PendingIntent.getActivity(context.getApplicationContext(), 22 ,intent2,PendingIntent.FLAG_UPDATE_CURRENT);


        //open profile to chat  pending intent
        Intent intent3 = new Intent(context, SplashActivity.class);
        intent3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bundle bundle3 = new Bundle();
        bundle3.putString("user_id",user_id);
        bundle3.putString("user_image",user_image);
        bundle3.putString("user_name",user_name);
        bundle3.putInt("type",71);
        intent3.putExtra("data",bundle3);
        PendingIntent profilePendingIntent = PendingIntent.getActivity(context.getApplicationContext(),33,intent3,PendingIntent.FLAG_UPDATE_CURRENT);

        notificationManager = (NotificationManager)context.getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,NOTIFICATION_CHANNEL_ID_DATUM_NOTIFICATION)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContent(viewSmall)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_DATUM_NOTIFICATION, NOTIFICATION_CHANNEL_NAME_DATUM_NOTIFICATION, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100});
            assert notificationManager != null;
            builder.setChannelId(NOTIFICATION_CHANNEL_ID_DATUM_NOTIFICATION);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        notification = builder.build();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notification.bigContentView = viewBig;
        }

        try{
//            viewBig.setOnClickPendingIntent(R.id.call , callDatePendingIntent);
          //  viewBig.setOnClickPendingIntent(R.id.fix_date , physicalDatePendingIntent);
            //viewBig.setOnClickPendingIntent(R.id.chat , profilePendingIntent);

//            viewSmall.setOnClickPendingIntent(R.id.btn_call , callDatePendingIntent);
            //viewSmall.setOnClickPendingIntent(R.id.btn_phy_date , physicalDatePendingIntent);
            //viewSmall.setOnClickPendingIntent(R.id.btn_chat , profilePendingIntent);
        }catch (Exception e){
            e.printStackTrace();
        }

        notificationManager.notify(notification_id, notification);
        /*Adding images details*/

        try
        {
            //Bitmap oponentBitmap = new ImageLoader(user_image).execute().get();
            //oponentBitmap=getCircularBitmap(oponentBitmap);
            Glide.with(context).asBitmap().
                    transform(new CircleCrop())
                    .addListener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                            Log.d("ItsMatchAlert", "showNotification: opponentBitmap "+resource);
                            if(viewBig != null) {
                                viewBig.setImageViewBitmap(R.id.oponent_pic, resource);
                                viewSmall.setImageViewBitmap(R.id.oponent_image, resource);
                            }
                            return true;
                        }
                    }).load(user_image).submit();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try
        {
            Bitmap myBitmap = new ImageLoader(dataSource.getProfilePicture()).execute().get();
            myBitmap=getCircularBitmap(myBitmap);
            viewBig.setImageViewBitmap(R.id.my_pic, myBitmap);
            viewSmall.setImageViewBitmap(R.id.my_image, myBitmap);
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        notificationManager.notify(notification_id, notification);
    }


    protected Bitmap getCircularBitmap(Bitmap srcBitmap)
    {
        int squareBitmapWidth = Math.min(srcBitmap.getWidth(), srcBitmap.getHeight());
        Bitmap dstBitmap = Bitmap.createBitmap(
                squareBitmapWidth, // Width
                squareBitmapWidth, // Height
                Bitmap.Config.ARGB_8888 // Config
        );
        Canvas canvas = new Canvas(dstBitmap);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        Rect rect = new Rect(0, 0, squareBitmapWidth, squareBitmapWidth);
        RectF rectF = new RectF(rect);
        canvas.drawOval(rectF, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        float left = (squareBitmapWidth-srcBitmap.getWidth())/2;
        float top = (squareBitmapWidth-srcBitmap.getHeight())/2;
        canvas.drawBitmap(srcBitmap, left, top, paint);
        srcBitmap.recycle();
        return dstBitmap;
    }

}
