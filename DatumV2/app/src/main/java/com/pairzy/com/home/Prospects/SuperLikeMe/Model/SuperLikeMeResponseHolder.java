package com.pairzy.com.home.Prospects.SuperLikeMe.Model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * <h2>SuperLikeMeResponseHolder</h2>
 * <P>
 *
 * </P>
 *@since  3/27/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class SuperLikeMeResponseHolder
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<SuperLikedMeItemPojo> data = null;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public ArrayList<SuperLikedMeItemPojo> getData()
    {
        return data;
    }

    public void setData(ArrayList<SuperLikedMeItemPojo> data)
    {
        this.data = data;
    }
}
