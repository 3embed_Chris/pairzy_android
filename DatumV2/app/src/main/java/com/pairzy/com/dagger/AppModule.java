package com.pairzy.com.dagger;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
/**
 * <h1>AppModule</h1>
 * Used to inject the dependency
 * @author 3Embed
 * @since 03-Nov-17
 */
@Module
abstract class AppModule
{
    @Singleton
    @Binds
    abstract Context bindContext(Application application);
}
