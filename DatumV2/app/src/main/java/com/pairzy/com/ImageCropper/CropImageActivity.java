package com.pairzy.com.ImageCropper;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import com.pairzy.com.ImageCropper.Cropper.CropImage;
import com.pairzy.com.ImageCropper.Cropper.CropImageOptions;
import com.pairzy.com.ImageCropper.Cropper.CropImageView;
import com.pairzy.com.R;
import com.pairzy.com.util.localization.activity.BaseDaggerActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 *<h2>CropImageActivity</h2>
 * <P>
 *
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 12-02-2018.
 * */
public class CropImageActivity extends BaseDaggerActivity implements CropImageView.OnSetImageUriCompleteListener, CropImageView.OnCropImageCompleteListener
{
    private Uri mCropImageUri;
    private CropImageOptions mOptions;
    Unbinder unbinder;
    @BindView(R.id.cropImageView)
    CropImageView mCropImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image_actvity);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder= ButterKnife.bind(this);
        initData();
    }

    /*
     * Intialization of the xml data.*/
    private void initData()
    {
        Bundle bundle = getIntent().getBundleExtra(CropImage.CROP_IMAGE_EXTRA_BUNDLE);
        mCropImageUri = bundle.getParcelable(CropImage.CROP_IMAGE_EXTRA_SOURCE);
        mOptions = bundle.getParcelable(CropImage.CROP_IMAGE_EXTRA_OPTIONS);
        mCropImageView.setImageUriAsync(mCropImageUri);
    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        setResultCancel();
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        mCropImageView.setOnSetImageUriCompleteListener(this);
        mCropImageView.setOnCropImageCompleteListener(this);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        mCropImageView.setOnSetImageUriCompleteListener(null);
        mCropImageView.setOnCropImageCompleteListener(null);
    }


    public void cropImage()
    {
        mCropImageView.getCroppedImageAsync();
    }


    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error)
    {
        if (error == null) {
            if (mOptions.initialCropWindowRectangle != null)
            {
                mCropImageView.setCropRect(mOptions.initialCropWindowRectangle);
            }
            if (mOptions.initialRotation > -1)
            {
                mCropImageView.setRotatedDegrees(mOptions.initialRotation);
            }
        } else
        {
            setResult(null, error, 1);
        }
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result)
    {
        setResult(result.getUri(), result.getError(), result.getSampleSize());
    }

    /**
     * Cancel of cropping activity. */
    protected void setResultCancel()
    {
        setResult(RESULT_CANCELED);
        finish();
    }


    /** Result with cropped image data or error if failed. */
    protected void setResult(Uri uri, Exception error, int sampleSize)
    {
        int resultCode = error == null ? RESULT_OK : CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE;
        setResult(resultCode, getResultIntent(uri, error, sampleSize));
        finish();
    }

    /** Get intent instance to be used for the result of this activity. */
    private Intent getResultIntent(Uri uri, Exception error, int sampleSize)
    {
        CropImage.ActivityResult result =
                new CropImage.ActivityResult(
                        mCropImageView.getImageUri(),
                        uri,
                        error,
                        mCropImageView.getCropPoints(),
                        mCropImageView.getCropRect(),
                        mCropImageView.getRotatedDegrees(),
                        mCropImageView.getWholeImageRect(),
                        sampleSize);
        Intent intent = new Intent();
        intent.putExtras(getIntent());
        intent.putExtra(CropImage.CROP_IMAGE_EXTRA_RESULT, result);
        return intent;
    }
}
