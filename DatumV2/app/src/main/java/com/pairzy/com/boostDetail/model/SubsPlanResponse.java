package com.pairzy.com.boostDetail.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ankit on 1/6/18.
 */

public class SubsPlanResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<SubsPlan> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<SubsPlan> getData() {
        return data;
    }

    public void setData(ArrayList<SubsPlan> data) {
        this.data = data;
    }
}
