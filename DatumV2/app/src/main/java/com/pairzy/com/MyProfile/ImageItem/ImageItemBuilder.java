package com.pairzy.com.MyProfile.ImageItem;

import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * @since  4/4/2018.
 * @version 1.0.
 * @author 3Embed.
 */
@Module
public interface ImageItemBuilder
{
    @FragmentScoped
    @Binds
    ProfileImageItemFrg getProspectItemFragment(ProfileImageItemFrg likesMeFrg);

    @FragmentScoped
    @Binds
    ImageItemContract.Presenter taskPresenter(ImageItemPresenter presenter);
}
