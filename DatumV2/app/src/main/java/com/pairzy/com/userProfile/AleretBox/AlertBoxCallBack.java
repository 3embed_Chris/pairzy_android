package com.pairzy.com.userProfile.AleretBox;
/**
 * <h2>ChatMenuCallback</h2>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface AlertBoxCallBack
{
    void onRecommendFriend();
    void onReport();
    void onBlock();
    void onUnblock();
}
