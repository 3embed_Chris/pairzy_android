package com.pairzy.com.MatchedView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * <h2>MatchResponseData</h2>
 * @since  4/13/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MatchResponseData
{
    @SerializedName("matchDate")
    @Expose
    private Double matchDate;
    @SerializedName("firstLikedBy")
    @Expose
    private String firstLikedBy;
    @SerializedName("firstLikedByName")
    @Expose
    private String firstLikedByName;
    @SerializedName("firstLikedByPhoto")
    @Expose
    private String firstLikedByPhoto;
    @SerializedName("isFirstSuperLiked")
    @Expose
    private Integer isFirstSuperLiked = 0;
    @SerializedName("secondLikedBy")
    @Expose
    private String secondLikedBy;
    @SerializedName("SecondLikedByName")
    @Expose
    private String secondLikedByName;
    @SerializedName("secondLikedByPhoto")
    @Expose
    private String secondLikedByPhoto;
    @SerializedName("isSecondSuperLiked")
    @Expose
    private Integer isSecondSuperLiked = 0;
    @SerializedName("chatId")
    @Expose
    private String chatId ="";

    public Double getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(Double matchDate) {
        this.matchDate = matchDate;
    }

    public String getFirstLikedBy() {
        return firstLikedBy;
    }

    public void setFirstLikedBy(String firstLikedBy) {
        this.firstLikedBy = firstLikedBy;
    }

    public String getFirstLikedByName() {
        return firstLikedByName;
    }

    public void setFirstLikedByName(String firstLikedByName) {
        this.firstLikedByName = firstLikedByName;
    }

    public String getFirstLikedByPhoto() {
        return firstLikedByPhoto;
    }

    public void setFirstLikedByPhoto(String firstLikedByPhoto) {
        this.firstLikedByPhoto = firstLikedByPhoto;
    }

    public String getSecondLikedBy() {
        return secondLikedBy;
    }

    public void setSecondLikedBy(String secondLikedBy) {
        this.secondLikedBy = secondLikedBy;
    }

    public String getSecondLikedByName() {
        return secondLikedByName;
    }

    public void setSecondLikedByName(String secondLikedByName) {
        this.secondLikedByName = secondLikedByName;
    }

    public String getSecondLikedByPhoto() {
        return secondLikedByPhoto;
    }

    public void setSecondLikedByPhoto(String secondLikedByPhoto) {
        this.secondLikedByPhoto = secondLikedByPhoto;
    }

    public Integer getIsFirstSuperLiked() {
        return isFirstSuperLiked;
    }

    public void setIsFirstSuperLiked(Integer isFirstSuperLiked) {
        this.isFirstSuperLiked = isFirstSuperLiked;
    }

    public Integer getIsSecondSuperLiked() {
        return isSecondSuperLiked;
    }

    public void setIsSecondSuperLiked(Integer isSecondSuperLiked) {
        this.isSecondSuperLiked = isSecondSuperLiked;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }
}
