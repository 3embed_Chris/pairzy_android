package com.pairzy.com.data.source;

import com.pairzy.com.data.model.Subscription;
import com.pairzy.com.data.model.coinCoinfig.CoinData;

import java.util.ArrayList;

public interface PreferenceTaskDataSource
{
    void clear();

    String  getUserId();

    void setUserId(String userId);

    String getProfilePicture();

    void setProfilePicture(String url);

    String getToken();

    void setToken(String token);

    String getName();

    void setName(String name);

    String getMobileNumber();

    void setMobileNumber(String mobileNumber);

    String getCountryCode();

    void setCountryCode(String countryCode);

    boolean isSignUp();

    void setIsSignUp(boolean isSignUp);

    String getOtp();

    void setOtp(String otp);

    String getGender();

    void setGender(String gender);

    String getEmail();

    void setEmail(String email);

    int getUserAge();

    void serUserAge(int age);

    String getBirthDate();

    void setBirthDate(String birthDate);

    void setPushToken(String pushToken);

    String getPushToken();

    void setLoggedIn(boolean loggedIn);

    boolean isLoggedIn();

    void setSearchPreference(String preference);

    String getSearchPreference();

    void setUserVideo(String video_url);

    String getUserVideoUrl();

    void setUserVideoHeight(int height);
    void setUserVideoWidth(int width);
    int getUserVideoHeight();
    int getUserVideoWidth();

    void setUserVideoThumbnail(String videoThumbnail);

    String getUserVideoThumbnail();

    void setUserOtherImages(ArrayList<String> otherImages);

    ArrayList<String> getUserOtherImages();

    void setMyDetails(String data);

    String getMyDetails();

    void setIsMile(boolean isMile);

    boolean getIsMile();

    void setIsFacebookLogin(boolean isFbLogin);

    boolean getIsFacebookLogin();

    void setMyPreference(String preference);

    String getSMyPreference();

    void setMyAbout(String about);

    String getMyAbout();

    void setMyEducation(String education);

    String getMyEducation();

    void setMyWorkPlace(String work);

    String getMyWorkPlace();

    void setMyJob(String education);

    String getMyJob();

    void setEvents(ArrayList<String> eventList);

    ArrayList<String> getEvents();

    void setShowChatCoinDialog(boolean show);
    Boolean getShowChatCoinDialog();
    void setShowSuperlikeCoinDialog(boolean show);
    Boolean getShowSuperlikeCoinDialog();
    void setShowDateCoinDialog(boolean show);
    Boolean getShowDateCoinDialog();
    void setShowCallDateCoinDialog(boolean show);
    Boolean getShowRescheduleDateCoinDialog();
    void setShowRescheduleDateCoinDialog(boolean show);
    Boolean getShowVideoDateCoinDialog();
    void setShowVideoDateCoinDialog(boolean show);
    Boolean getShowCallDateCoinDialog();

    void setSubscription(String subs);
    Subscription getSubscription();

    void setCoinConfig(String subs);
    CoinData getCoinConfigData();

    boolean getShowCardTutorialView();
    void setShowCardTutorialView(boolean show);

    void setBoostExpireTime(long boostExpireTime);
    long getBoostExpireTime();

    void setRemainsLinksCount(String likeCount);
    String getRemainsLikesCount();

    void setRemainsRewindsCount(String rewindCount);
    String getRemainsRewindsCount();

    void setNextLikeTime(long nextLikeTime);
    long getNextLikeTime();

    void setDuringBoostViewCount(int viewCount);
    int getDuringBoostViewCount();

    boolean isSplashFirstTime();
    void setSplashFirstTimeDone();

    void setUserActionCount(int actionsCount);
    int getUserActionCount();

    String getSelectedLanguageIso();
    void setSelectedLanguageIso(String iso);

    //update related data
    void setUpdateDialogShowed(boolean isShowed);
    boolean isUpdatedDialogShowed();

    void setNewVersionData(String appVersion);
    String getNewVersion();

    void setNewVersionUpdatedTime(long milli);
    long getNewVersionUpdatedTime();

    void setIsMandatory(boolean isMandatory);
    boolean isMandatory();

    void setProfileUpdateType(int type);
    int getProfileUpdateType();

    //clodinaryDetails
    void setCloudName(String cloudName);
    String getCloudName();

    void setCloudinaryApiKey(String apiKey);
    String getCloudinaryApiKey();

    void setClodinaryApiSecret(String apiSecret);
    String getCloudinaryApiSecret();

    boolean getSwipeCardTutorialView();
    void setSwipeCardTutorialView(boolean show);
}
