package com.pairzy.com.createPost.galleryFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.pairzy.com.R;


public class GalleryPostFragment extends Fragment {


    public GalleryPostFragment() {
        // Required empty public constructor
    }
@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_gallery_post, container, false);
    }





    @Override
    public void onDetach() {
        super.onDetach();

    }


}
