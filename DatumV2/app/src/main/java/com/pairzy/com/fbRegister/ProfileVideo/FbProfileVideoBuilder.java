package com.pairzy.com.fbRegister.ProfileVideo;

import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * @since 1/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface FbProfileVideoBuilder
{
    @FragmentScoped
    @Binds
    FbProfileVideoFrg getProfileVideo(FbProfileVideoFrg profileVideoFrg);

    @FragmentScoped
    @Binds
    FbProfileVideoContact.Presenter taskPresenter(FbProfileVideoFrgPresenter presenter);
}
