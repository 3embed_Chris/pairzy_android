package com.pairzy.com.MyProfile.editPreference.newListScrollerFrag;
import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

import java.util.ArrayList;

/**
 * @since  2/22/2018.
 */
public interface NewListdataFrgContract
{
    interface View extends BaseView
    {
        void showMessage(String message);
        void showError(String message);
        void updatePreferenceData(ArrayList<String> selectedValue);
    }

    interface Presenter extends BasePresenter
    {
        boolean addSelection(String data);
        ArrayList<String> getMultiSelectedData();
        String getSingleSelectedData();
        boolean isDataExist();
        void removeSelection(String data);
        void addSingleSelection(String data);
        void  updatePreference(String pref_Id, int selectionType,boolean clear);
        void initSelectionList(ArrayList<String> selectedValues, int seletionType);
        void updateSelection(ArrayList<ListData> options_list_data, int seletionType);
    }

}
