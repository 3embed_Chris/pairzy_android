package com.pairzy.com.util.CustomObserver;

import com.pairzy.com.data.model.BoostCountData;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;

/**
 * Created by ankit on 25/5/18.
 */

public class BoostViewCountObserver {

    private ConnectableObservable<BoostCountData> connectableObservable;
    private ObservableEmitter<BoostCountData> emitor;
    public BoostViewCountObserver()
    {
        Observable<BoostCountData> observable=Observable.create(e -> emitor=e);
        connectableObservable=observable.publish();
        connectableObservable.share();
        connectableObservable.replay();
        connectableObservable.connect();
    }

    public ConnectableObservable<BoostCountData> getObservable()
    {
        return connectableObservable;
    }

    public void publishData(BoostCountData data)
    {
        if(emitor!=null)
        {
            emitor.onNext(data);
        }
    }
}

