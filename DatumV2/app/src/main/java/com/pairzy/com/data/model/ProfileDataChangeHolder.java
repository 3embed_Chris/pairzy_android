package com.pairzy.com.data.model;

/**
 * Created by ankit on 28/5/18.
 */

public class ProfileDataChangeHolder {
    public boolean isProfilePicChange = false;
    public boolean isProfileNameChange = false;
    public boolean isProfileUserAgeChange = false;
    public boolean isWorkChanged = false;
    public boolean isJobChanged = false;
    public boolean isGenderChanged = false;
}
