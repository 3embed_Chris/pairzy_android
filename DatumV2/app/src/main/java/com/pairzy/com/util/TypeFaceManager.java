package com.pairzy.com.util;

import android.content.Context;
import android.graphics.Typeface;

/**
 * <h2>TypeFaceManager</h2>
 * <P>
 *
 * </P>
 * @author 3Embed
 * @since  09/02/2018.
 */
public class TypeFaceManager
{
    private Typeface circularAirBold;
    private Typeface circularAirBook;
    private Typeface circularAirLight;

    public TypeFaceManager(Context context)
    {
        circularAirBold=Typeface.createFromAsset(context.getAssets(),"fonts/CircularAir-Bold.otf");
        circularAirBook=Typeface.createFromAsset(context.getAssets(),"fonts/CircularAir-Book.otf");
        circularAirLight=Typeface.createFromAsset(context.getAssets(),"fonts/CircularAir-Light.otf");
    }

    public Typeface getCircularAirBold() {
        return circularAirBold;
    }

    public Typeface getCircularAirBook() {
        return circularAirBook;
    }

    public Typeface getCircularAirLight() {
        return circularAirLight;
    }
}
