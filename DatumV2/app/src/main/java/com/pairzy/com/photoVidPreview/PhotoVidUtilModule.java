package com.pairzy.com.photoVidPreview;

import android.app.Activity;

import com.androidinsta.com.ImageData;
import com.pairzy.com.dagger.ActivityScoped;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class PhotoVidUtilModule {

    @ActivityScoped
    @Provides
    ArrayList<ImageData> photoList(){
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    PhotoPagerAdapter photoPagerAdapter(Activity activity,ArrayList<ImageData> photoList ){
        return new PhotoPagerAdapter(activity,photoList);
    }

}
