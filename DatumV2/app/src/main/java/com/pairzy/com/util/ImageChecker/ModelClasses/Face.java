package com.pairzy.com.util.ImageChecker.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * <h2>Face</h2>
 * <P>
 *
 * </P>
 * @since  4/17/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class Face
{
    @SerializedName("x1")
    @Expose
    private Double x1;
    @SerializedName("y1")
    @Expose
    private Double y1;
    @SerializedName("x2")
    @Expose
    private Double x2;
    @SerializedName("y2")
    @Expose
    private Double y2;

    @SerializedName("celebrity")
    @Expose
    private ArrayList<Celebrity> celebrity = null;

    public Double getX1() {
        return x1;
    }

    public void setX1(Double x1) {
        this.x1 = x1;
    }

    public Double getY1() {
        return y1;
    }

    public void setY1(Double y1) {
        this.y1 = y1;
    }

    public Double getX2() {
        return x2;
    }

    public void setX2(Double x2) {
        this.x2 = x2;
    }

    public Double getY2() {
        return y2;
    }

    public void setY2(Double y2) {
        this.y2 = y2;
    }

    public ArrayList<Celebrity> getCelebrity() {
        return celebrity;
    }

    public void setCelebrity(ArrayList<Celebrity> celebrity) {
        this.celebrity = celebrity;
    }
}
