package com.pairzy.com.home.Matches;

import android.content.Intent;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

public interface MatchesContract {

    interface View extends BaseView{

        void showError(int id);
        void showError(String error);
        void showMessage(String message);
        void showCoinBalance(String coinBalance);
        void launchUserProfile(Intent intent);
    }

    interface Presenter extends BasePresenter<View> {

    }
}
