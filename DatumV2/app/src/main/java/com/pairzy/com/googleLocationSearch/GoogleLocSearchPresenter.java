package com.pairzy.com.googleLocationSearch;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.R;
import com.pairzy.com.googleLocationSearch.model.AddressListPojo;
import com.pairzy.com.googleLocationSearch.model.DataHolder;
import com.pairzy.com.googleLocationSearch.model.GoogleLocSearchModel;
import com.pairzy.com.googleLocationSearch.model.LocationAdapter;
import com.pairzy.com.googleLocationSearch.model.fourSqSearch.FourSearchResponse;
import com.pairzy.com.locationScreen.locationMap.CustomLocActivity;
import com.pairzy.com.locationScreen.model.LocationHolder;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.passportLocation.model.PassportLocation;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.LocationProvider.LocationApiCallback;
import com.pairzy.com.util.LocationProvider.LocationApiManager;
import com.pairzy.com.util.LocationProvider.Location_service;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h>GoogleLocSearchPresenter class</h>
 * @author 3Embed.
 * @since 22/05/2018.
 * @version 1.0.
 */

public class GoogleLocSearchPresenter implements GoogleLocSearchContract.Presenter,App_permission.Permission_Callback,Location_service.GetLocationListener,LocationAdapter.OnItemClickListener{

    private  final String LOCATION_TAG="location_tag";

    @Inject
    GoogleLocSearchContract.View view;

    @Inject
    App_permission app_permission;

    @Inject
    Activity activity;

    @Inject
    Location_service location_service;

    @Inject
    LocationApiManager locationApiManager;

    @Inject
    NetworkService service;

    @Inject
    GoogleLocSearchModel model;

    @Inject
    LinearLayoutManager linearLayoutManager;
    @Inject
    LocationHolder locationHolder;
    @Inject
    NetworkStateHolder networkStateHolder;

    private int position = -1;
    private CompositeDisposable compositeDisposable;

    @Inject
    GoogleLocSearchPresenter(){
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void init() {
        if(view != null){
            view.applyFont();
        }
    }

    private void callFoursquareApi(String url){
        if(networkStateHolder.isConnected()) {
            if (view != null)
                view.showLoading();
            service.getNearByPlaces(url)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<FourSearchResponse>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<FourSearchResponse> value) {

                            if (value.code() == 200) {
                                FourSearchResponse fourResponse = value.body();
                                if (fourResponse != null) {
                                    model.filterResponse(fourResponse);
                                }
                                if (view != null)
                                    view.showData();
                            } else {
                                if (view != null)
                                    view.showNetworkError(activity.getString(R.string.api_server_error));
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.showError(e.getMessage());
                                view.showNetworkError(e.getMessage());
                            }
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }else {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    /*
     * getting user location from GPS
     * */
    private void getUserLocationFromService()
    {
        location_service.setListener(this);
        location_service.checkLocationSettings();
    }

    @Override
    public void askLocationPermission() {
        if(view != null)
            view.showLoading();
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.LOCATION);
        app_permission.getPermission(LOCATION_TAG,permissions,this);
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        if (tag.equalsIgnoreCase(LOCATION_TAG) && isAllGranted) {
            getUserLocationFromService();
        }
    }


    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        if(tag.equals(LOCATION_TAG))
        {
            String[] stringArray = deniedPermission.toArray(new String[0]);
            app_permission.show_Alert_Permission(activity.getString(R.string.location_access_text),activity.getString(R.string.location_acess_subtitle),
                    activity.getString(R.string.location_acess_message),stringArray);
        }
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean parmanent) {
        if(tag.equals(LOCATION_TAG))
        {
            getUserLocationFromApi();
        }
    }


    /*
    * getting user location from link*/
    private void getUserLocationFromApi()
    {
        locationApiManager.getLocation(new LocationApiCallback()
        {
            @Override
            public void onSuccess(Double lat, Double lng, double accuracy)
            {
                locationHolder.setLatitude(lat);
                locationHolder.setLongitude(lng);
                DataHolder dataHolder = new DataHolder();
                dataHolder.isNearest_data = 0;
                dataHolder.latitude = lat;
                dataHolder.logitude = lng;
                callPlacesApi(dataHolder);
            }

            @Override
            public void onError(String error)
            {
                if(view!=null) {
                    view.showError(error);
                    view.showNetworkError(error);
                }
            }
        });
    }


    @Override
    public void updateLocation(Location location) {
        location_service.stop_Location_Update();
        locationHolder.setLatitude(location.getLatitude());
        locationHolder.setLongitude(location.getLongitude());
        DataHolder dataHolder = new DataHolder();
        dataHolder.isNearest_data = 0;
        dataHolder.latitude = location.getLatitude();
        dataHolder.logitude = location.getLongitude();
        callPlacesApi(dataHolder);
    }

    @Override
    public void callPlacesApi(DataHolder dataHolder) {
        int isFor_what = 0;
        //int index_data = 0;
        String data = "";
        isFor_what = dataHolder.isNearest_data;
        //index_data = dataHolder.index_number;
        String url_link;
        if (isFor_what == 0) {
            url_link = model.getUrl_forsquare(dataHolder.latitude, dataHolder.logitude);
            callFoursquareApi(url_link);
        } else if (isFor_what == 1) {
            url_link = model.getUrl_Google_search(dataHolder.Search_text_data, dataHolder.latitude, dataHolder.logitude);
            callGoogleSearchApi(url_link);
        }
        else{
            url_link = model.getPlaceDetailsUrl(dataHolder.Search_text_data);
            callPlaceDetailApi(url_link);
        }
    }

    private void callPlaceDetailApi(String url) {
        if(networkStateHolder.isConnected()) {
            service.getPlaceDetail(url)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                try {
                                    LocationHolder holder = model.parsePlaceDetailResponse(value.body().string());
                                    if (view != null && position > -1) {
                                        AddressListPojo addressPojo = model.getAddressPojo(position);
                                        PassportLocation passportLocation = new PassportLocation();
                                        passportLocation.setLocationName(addressPojo.getAddress_title());
                                        passportLocation.setSubLocationName(addressPojo.getSub_Address());
                                        passportLocation.setLatitude(holder.getLatitude());
                                        passportLocation.setLongitude(holder.getLongitude());
                                        Intent data = new Intent();
                                        data.putExtra("passport_location", passportLocation);
                                        view.setData(data);
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if (view != null)
                                    view.showNetworkError(activity.getString(R.string.api_server_error));
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.showError(e.getMessage());
                                view.showNetworkError(e.getMessage());
                            }
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
        else {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    private void callGoogleSearchApi(String url) {
        if(networkStateHolder.isConnected() ) {
            if (view != null)
                view.showLoading();
            service.getGoogleSearchResult(url)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {

                            if (value.code() == 200) {
                                try {
                                    model.parseGoogleResponse(value.body().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                if (view != null)
                                    view.showData();
                            } else {
                                if (view != null)
                                    view.showNetworkError(activity.getString(R.string.api_server_error));
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null) {
                                view.showError(e.getMessage());
                                view.showNetworkError(e.getMessage());
                            }
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        } else {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void location_Error(String error) {
        getUserLocationFromApi();
    }

    @Override
    public void dispose(){
        compositeDisposable.clear();
        this.view = null;
    }

    @Override
    public void parseLocation(int requestCode, int resultCode, Intent intent)
    {
        if(requestCode == CustomLocActivity.CHOOSE_LOC_REQ_CODE && resultCode == -1){
            if(view != null){
                view.setData(intent);
            }
        }
    }

    @Override
    public void onItemClick(AddressListPojo addressPojo,int position)
    {
        this.position = position;
        if(view != null && addressPojo != null)
        {
            if(addressPojo.getLatitude() > 0 && addressPojo.getLogitude() > 0) {
                PassportLocation passportLocation = new PassportLocation();
                passportLocation.setLocationName(addressPojo.getAddress_title());
                passportLocation.setSubLocationName(addressPojo.getSub_Address());
                passportLocation.setLatitude(addressPojo.getLatitude());
                passportLocation.setLongitude(addressPojo.getLogitude());
                Intent data = new Intent();
                data.putExtra("passport_location", passportLocation);
                view.setData(data);
            }
            else{
                DataHolder dataHolder = new DataHolder();
                dataHolder.isNearest_data = 2;
                dataHolder.Search_text_data = model.getPrefText(position);
                dataHolder.index_number = position;
                callPlacesApi(dataHolder);
            }
        }
    }

    public boolean isListEmpty() {
        return model.getListSize() == 0;
    }

}
