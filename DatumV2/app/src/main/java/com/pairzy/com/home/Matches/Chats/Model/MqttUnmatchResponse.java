package com.pairzy.com.home.Matches.Chats.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 30/7/18.
 */

public class MqttUnmatchResponse {

    @SerializedName("targetId")
    @Expose
    String targetId;

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }
}
