package com.pairzy.com.momentGrid;

import android.app.Activity;

import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.momentGrid.model.MomentsGridAdapter;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * <h2>MomentsGridUtils</h2>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */
@Module
public class MomentsGridUtils {

    @Provides
    @ActivityScoped
    ArrayList<MomentsData> provideMomentList()
    {
        return  new ArrayList<>();
    }

    @Provides
    @ActivityScoped
    MomentsGridAdapter momentsGridAdapter(Activity activity,ArrayList<MomentsData> momentsDataList){
        return new MomentsGridAdapter(activity,momentsDataList);
    }
}
