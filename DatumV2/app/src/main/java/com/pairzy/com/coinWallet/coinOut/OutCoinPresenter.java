package com.pairzy.com.coinWallet.coinOut;

import com.pairzy.com.coinWallet.coinOut.model.OutCoinModel;

import javax.inject.Inject;

/**
 *<h>OutCoinPresenter</h>
 * <p></p>
 *@author 3Embed.
 *@since 30/5/18.
 */

public class OutCoinPresenter implements OutCoinContract.Presenter {

    private OutCoinContract.View view;

    @Inject
    OutCoinModel model;

    @Inject
    public OutCoinPresenter(){
    }

    @Override
    public void takeView(OutCoinContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        this.view = null;
    }

    @Override
    public boolean isCoinHistoryEmpty() {
        return model.isCoinHistoryEmpty();
    }
}
