package com.pairzy.com.commentPost.model;
/**
 * <h2>CommentItemCallBack</h2>
 * <P> this is interface for call back on click FOR THE adapter</P>
 * @since 16-07-2019
 * @author Hemanth.
 * @version 1.0.
 */
public interface CommentItemCallBack {
    /* <h2> onEdit</h2>
     * <p>this is method for editing comment </p>
     */
    void onEdit(int position);

    /* <h2> OnDelete</h2>
     * <p>this is method for deleting Comment </p>
     */
    void OnDelete(int position);


}
