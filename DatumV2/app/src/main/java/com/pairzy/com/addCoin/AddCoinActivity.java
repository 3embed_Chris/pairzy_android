package com.pairzy.com.addCoin;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.ads.mediation.tapjoy.TapjoyAdapter;
import com.google.android.material.appbar.AppBarLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.addCoin.model.CoinPlanListAdapter;
import com.pairzy.com.coinWallet.CoinWalletActivity;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.suresh.innapp_purches.InnAppSdk;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h>BoostDetail activity</h>
 *
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */
public class AddCoinActivity extends BaseDaggerActivity implements AddCoinContract.View,
        SwipeRefreshLayout.OnRefreshListener, AppBarLayout.OnOffsetChangedListener, RewardedVideoAdListener{

    private static final String TAG = AddCoinActivity.class.getName();
    @Inject
    AddCoinPresenter presenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    Activity activity;
    @Inject
    CoinPlanListAdapter planListAdapter;
    @Inject
    LinearLayoutManager linearLayoutManager;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    Utility utility;
    @Inject
    PreferenceTaskDataSource dataSource;

    @BindView(R.id.parent_layout)
    CoordinatorLayout parentLayout;

    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.page_title)
    TextView tvPageTitle;
    @BindView(R.id.tv_your_balance)
    TextView tvYourBalance;
    @BindView(R.id.btn_recent_transaction)
    Button btnRecentTransaction;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.loading_progress)
    ProgressBar pbLoading;
    @BindView(R.id.loading_text)
    TextView tvLoading;
    @BindView(R.id.error_icon)
    ImageView ivErrorIcon;
    @BindView(R.id.error_msg)
    TextView tvErrorMsg;
    @BindView(R.id.empty_data)
    RelativeLayout rlEmptyData;
    @BindView(R.id.loading_view)
    RelativeLayout rlLoadingView;
    @BindView(R.id.no_more_data)
    TextView tvNoMoreDataTitle;
    @BindView(R.id.empty_details)
    TextView tvEmptyDetail;
    @BindView(R.id.recycler_coin_plan)
    RecyclerView recyclerCoinPlan;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.header_title_tv)
    TextView tvHeaderTitle;
    @BindView(R.id.header_msg_tv)
    TextView tvHeaderMsg;
    @BindView(R.id.btn_retry)
    Button btnRetry;
    @BindView(R.id.btn_earn_coin)
    Button btnEarnCoin;

    private Unbinder unbinder;
    private Handler handler;
    boolean isShow = false;
    int scrollRange = -1;
    private RewardedVideoAd mRewardedVideoAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_coin);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        presenter.init();
        initView();
        presenter.observeCoinBalanceChange();
        initAdds();
        loadRewardedVideoAd();
        presenter.observeAdminCoinAdd();

        presenter.updateCoinBalance();
        presenter.getCoinPlans();
    }

    private void initAdds() {
        // Use an activity context to get the rewarded video instance.
        mRewardedVideoAd = MobileAds.getRewardedVideoAdInstance(this);
        mRewardedVideoAd.setRewardedVideoAdListener(this);
    }

    private void loadRewardedVideoAd() {
        btnEarnCoin.setEnabled(false);
        Bundle bundle = new TapjoyAdapter.TapjoyExtrasBundleBuilder()
                .setDebug(true)
                .build();
        mRewardedVideoAd.loadAd(getString(R.string.AD_UNIT_ID),
                new AdRequest.Builder()
                        .addNetworkExtrasBundle(TapjoyAdapter.class,bundle)
                        .build());
    }

    private void initView() {
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh2, R.color.refresh1, R.color.refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerCoinPlan.setLayoutManager(linearLayoutManager);
        recyclerCoinPlan.setHasFixedSize(true);
        recyclerCoinPlan.setNestedScrollingEnabled(false);
        recyclerCoinPlan.setAdapter(planListAdapter);
        planListAdapter.setClickCallback(presenter);
        appBarLayout.addOnOffsetChangedListener(this);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if(scrollRange == -1){
            scrollRange = appBarLayout.getTotalScrollRange();
        }
        if(scrollRange + verticalOffset == 0){
            isShow = true;
            showToolbarTitle(true);
        } else if(isShow){
            isShow = false;
            showToolbarTitle(false);
        }
    }

    private void showToolbarTitle(boolean show) {

        if(handler == null)
            handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(show){
                    tvToolbarTitle.setVisibility(View.VISIBLE);
                }
                else {
                    tvToolbarTitle.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void applyFont() {
        btnRetry.setTypeface(typeFaceManager.getCircularAirBold());
        tvYourBalance.setTypeface(typeFaceManager.getCircularAirBook());
        tvToolbarTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvPageTitle.setTypeface(typeFaceManager.getCircularAirBold());
        btnRecentTransaction.setTypeface(typeFaceManager.getCircularAirBook());
        btnEarnCoin.setTypeface(typeFaceManager.getCircularAirBook());
        tvNoMoreDataTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvEmptyDetail.setTypeface(typeFaceManager.getCircularAirBook());
        tvHeaderTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvHeaderMsg.setTypeface(typeFaceManager.getCircularAirBook());
    }


    @OnClick(R.id.btn_earn_coin)
    public void earnFreeCoin(){
//        if(tjPlacement != null && tjPlacement.isContentReady() && tjPlacement.getName().equalsIgnoreCase(AppConfig.placementName)) {
//            tjPlacement.showContent();
//            //btnEarnCoin.setEnabled(true);
//        }
//        else{
//            //btnEarnCoin.setEnabled(true);
//            Log.d(TAG, "ad content not available or not ready!!");
//        }
        //show video add if available
        if (mRewardedVideoAd != null && mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.show();
            //CustomVideoSurfaceView.getInstance(this, dataSource).stopPlayer();
        }
        else{
            Log.e(TAG, "earnFreeCoin: "+"reward video ad not loaded yet!!" );
        }
    }

    @OnClick(R.id.back_button)
    public void close(){
        onBackPressed();
    }

    @OnClick(R.id.btn_recent_transaction)
    public void addCoin() {
        Intent intent = new Intent(activity, CoinWalletActivity.class);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @OnClick(R.id.swipe_refresh_layout)
    public void parentLayout(){}

    @Override
    public void onBackPressed() {
        this.finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        super.onBackPressed();
    }

    @Override
    public void showError(String errorMsg) {
        if(parentLayout != null) {
            Snackbar snackbar = Snackbar
                    .make(parentLayout, "" + errorMsg, Snackbar.LENGTH_LONG);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorAccent));
            TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(ContextCompat.getColor(activity, R.color.white));
            snackbar.show();
        }
    }

    @Override
    public void showMessage(String msg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+msg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(int msgId) {
        String message=getString(msgId);
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showLoading() {
        btnRetry.setVisibility(View.GONE);
        tvErrorMsg.setVisibility(View.GONE);
        ivErrorIcon.setVisibility(View.GONE);
        tvLoading.setVisibility(View.VISIBLE);
        pbLoading.setVisibility(View.VISIBLE);
        rlEmptyData.setVisibility(View.GONE);
        recyclerCoinPlan.setVisibility(View.GONE);
        rlLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showData() {
        if(rlEmptyData != null) {
            rlEmptyData.setVisibility(View.GONE);
            recyclerCoinPlan.setVisibility(View.VISIBLE);
            rlLoadingView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showEmptyData() {
        if(rlEmptyData != null) {
            rlEmptyData.setVisibility(View.VISIBLE);
            recyclerCoinPlan.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showNetworkError(String errorMsg) {
        btnRetry.setVisibility(View.VISIBLE);
        tvErrorMsg.setVisibility(View.VISIBLE);
        tvErrorMsg.setText(errorMsg);
        ivErrorIcon.setVisibility(View.VISIBLE);
        tvLoading.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        recyclerCoinPlan.setVisibility(View.GONE);
        rlEmptyData.setVisibility(View.GONE);
        rlLoadingView.setVisibility(View.VISIBLE);
    }


    //need to remove this.
    @Override
    public void showBalanceLoading(boolean show) {
        if(tvYourBalance != null) {
            pbLoading.setVisibility(View.GONE);
            if (show) {
                tvYourBalance.setVisibility(View.VISIBLE);
            } else {
                tvYourBalance.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void showCoinBalance(String coinBalance) {
        if(tvYourBalance != null)
            tvYourBalance.setText(""+coinBalance);
    }


    @Override
    public void onResume() {
        super.onResume();
        mRewardedVideoAd.resume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mRewardedVideoAd.pause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRewardedVideoAd.destroy(this);
        presenter.dispose();
        appBarLayout.removeOnOffsetChangedListener(this);
        unbinder.unbind();
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        presenter.getCoinBalance();
        presenter.getCoinPlans();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(!InnAppSdk.getVendor().onActivity_result(requestCode,resultCode,data))
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @OnClick(R.id.btn_retry)
    public void onRetry(){
        onRefresh();
    }


    @Override
    public void onRewardedVideoAdLoaded() {
        Log.w(TAG , "onRewardedVideoAdLoaded()");
        btnEarnCoin.setEnabled(true);
    }

    @Override
    public void onRewardedVideoAdOpened() {
        Log.w(TAG , "onRewardedVideoAdOpened()");
    }

    @Override
    public void onRewardedVideoStarted() {
        Log.w(TAG , "onRewardedVideoStarted()");
    }

    @Override
    public void onRewardedVideoAdClosed() {
        Log.d(TAG , "onRewardedVideoAdClosed()");
        // Load the next rewarded video ad.
        loadRewardedVideoAd();
    }

    @Override
    public void onRewarded(RewardItem reward) {
        Log.d(TAG,"onRewarded! currency: " + reward.getType() + "  amount: " +
                reward.getAmount());
        presenter.callAddCoinApi(reward.getAmount());
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Log.d(TAG , "onRewardedVideoAdLeftApplication()");
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        Log.d(TAG , "onRewardedVideoAdFailedToLoad( "+i);
    }

    @Override
    public void onRewardedVideoCompleted() {
        Log.d(TAG , "onRewardedVideoCompleted()");
    }

}
