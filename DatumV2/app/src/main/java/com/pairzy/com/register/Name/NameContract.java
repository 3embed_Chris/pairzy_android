package com.pairzy.com.register.Name;
import com.pairzy.com.BasePresenter;

/**
 * @since  2/16/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface NameContract
{
    interface View
    {
        void invalidateName(String message);

        void onValidName(String name);

        void showMessage(String message);

        void onError(String message);
    }

    interface Presenter extends BasePresenter<View>
    {
         void validateName(String name);
    }
}
