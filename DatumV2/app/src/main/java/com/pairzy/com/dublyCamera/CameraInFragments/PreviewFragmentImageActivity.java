package com.pairzy.com.dublyCamera.CameraInFragments;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.pairzy.com.PostMoments.PostActivity;
import com.pairzy.com.R;
import com.pairzy.com.Utilities.Constants;
import com.pairzy.com.dublyCamera.ResultHolder;
import com.pairzy.com.util.AppConfig;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PreviewFragmentImageActivity extends AppCompatActivity {
    private final String TAG = PreviewFragmentImageActivity.class.getSimpleName();

    @BindView(R.id.image_edit_layout)
    View flContainer;
    @BindView(R.id.iv_picture)
    ImageView ivPreview;
    @BindString(R.string.app_name)
    String appName;

    private DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
    private int screenHeight = displayMetrics.widthPixels;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_fragmentimage_preview);
        ButterKnife.bind(this);
        ViewGroup.LayoutParams params = flContainer.getLayoutParams();
        params.height = screenHeight;
        flContainer.setLayoutParams(params);

        if(ResultHolder.getPath() == null ){
            Log.e(TAG, "onCreate: image path can not be null!!");
            finish();
            return;
        }

        if (!ResultHolder.getType().equals("image")) {
            supportFinishAfterTransition();
        }else{
            String imagePath = ResultHolder.getPath();
            Log.d(TAG, "onCreate: previewImagePath "+imagePath);
            ivPreview.setImageBitmap(BitmapFactory.decodeFile(imagePath));
        }
    }

    @OnClick(R.id.ivNext)
    public void next() {
        startNextActivity(ResultHolder.getPath());
    }

    private void startNextActivity(String path) {
        Intent intent;
        intent = new Intent(this, PostActivity.class);
        intent.putExtra(Constants.Post.PATH, path);
        intent.putExtra(Constants.Post.TYPE, Constants.Post.IMAGE);
        startActivityForResult(intent,222);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK && requestCode == AppConfig.POST_ACTIVITY_REQ_CODE){
            setResult(RESULT_OK,getIntent());
            finish();
        }
    }

    @OnClick(R.id.fl_close)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}