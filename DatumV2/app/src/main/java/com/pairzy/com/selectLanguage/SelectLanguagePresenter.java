package com.pairzy.com.selectLanguage;

import android.content.Context;
import android.text.TextUtils;

import com.androidinsta.com.InstagramManger;
import com.pairzy.com.AppController;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.selectLanguage.model.AppLanguageAdapter;
import com.pairzy.com.selectLanguage.model.LanguageAdapterCallback;
import com.pairzy.com.selectLanguage.model.SelectLanguageModel;
import com.pairzy.com.util.DeleteAccountDialog.DeleteAccDialogCallBack;
import com.pairzy.com.util.DeleteAccountDialog.DeleteAccountDialog;
import com.pairzy.com.util.LogoutDialog.LogoutAlertCallback;
import com.pairzy.com.util.LogoutDialog.LogoutDialog;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.progressbar.LoadingProgress;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h>PassportPresenter class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public class SelectLanguagePresenter implements SelectLanguageContract.Presenter , LogoutAlertCallback, DeleteAccDialogCallBack, LanguageAdapterCallback {

    @Inject
    SelectLanguageContract.View view;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    InstagramManger instagramManger;
    @Inject
    LogoutDialog logoutDialog;
    @Inject
    DeleteAccountDialog deleteAccountDialog;
    @Inject
    Context context;
    @Inject
    SelectLanguageModel model;
    @Inject
    Utility utility;
    @Inject
    NetworkService service;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    LoadingProgress loadingProgress;

    private CompositeDisposable compositeDisposable;

    @Inject
    public SelectLanguagePresenter(){
        compositeDisposable = new CompositeDisposable();
    }


    @Override
    public void loadAppVersion() {
    }

    @Override
    public void init() {
        if(view != null)
            view.applyFont();
    }

    @Override
    public void setAdapterCallback(AppLanguageAdapter appLanguageAdapter) {
        appLanguageAdapter.setAdapterCallback(this);
    }

    @Override
    public void onLogout() {
//        model.deleteCalenderEvent();
//        dataSource.clear();
//        instagramManger.loggedOut();
//        AppController.getInstance().disconnect();
//        AppController.getInstance().unsubscribeToFirebaseTopic();
//        if(view != null)
//            view.showSplashScreen();
        AppController.getInstance().appLogout();
        if(view != null)
            view.finishActivity();
    }

    @Override
    public void onLogoutCancel() {
        //Nothing
    }

    @Override
    public void onDeleteAccount() {
        callDeleteAccountApi();
    }

    private void callDeleteAccountApi() {
        if(networkStateHolder.isConnected()){
            loadingProgress.show();
            service.deleteAcoount(dataSource.getToken(),model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if(value.code() == 200){
                                onLogout();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                        }
                    });
        }else {
//            if(view != null)
//                view.showError();
        }
    }

    public void dispose() {
        compositeDisposable.clear();
    }

    @Override
    public void selectTheCurrentLanguage() {
        model.selectCurrentAppLanguage();
        if(view != null)
            view.notifyAdapter();
    }

    @Override
    public void onLanguageClick(int position) {
        String selectedLanguage = model.selectAppLanguage(position);
        if(!TextUtils.isEmpty(selectedLanguage)){
            if(view != null)
                view.setNewLanguage(selectedLanguage);
        }
        if(view != null)
            view.notifyAdapter();
    }
}
