package com.pairzy.com.UserPreference.PreviewMessage;

import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * <h2>{@link PreviewFrgBuilder}</h2>
 * <P>
 *     Builder class for the Preview page.
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 26/2/2018.
* */
@Module
public interface PreviewFrgBuilder
{
    @FragmentScoped
    @Binds
    PreviewFrg getConChoiceFragment(PreviewFrg previewFrg);

    @FragmentScoped
    @Binds
    PreviewFrgContract.Presenter taskPresenter(PreviewFrgPresenter presenter);
}
