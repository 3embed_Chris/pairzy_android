package com.pairzy.com.util.boostDialog;

/**
 * <h>Slide data class</h>
 * @author 3Embed.
 * @since 1/5/18.
 */

public class Slide
{
    private int backgroundColor;
    private int imageRes;
    private String subTitle;
    private String msg;

    public Slide(int backgroundColor,int imageRes, String subTitle, String msg) {
        this.imageRes = imageRes;
        this.subTitle = subTitle;
        this.msg = msg;
        this.backgroundColor = backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public int getImageRes() {
        return imageRes;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public String getMsg() {
        return msg;
    }
}
