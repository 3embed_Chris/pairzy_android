package com.pairzy.com.networking;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.pairzy.com.AppController;

/**
 * <h2>NetworkStateHolder</h2>
 * <P>
 *   it is holder class for the network service class
 *   data provider to carry the network info from the required class.
 * </P>
 * @version 1.0.
 * @author 3Embed.
 * */
public class NetworkStateHolder
{
    private String message;
    private boolean isConnected;
    private ConnectionType connectionType;
    private ConnectivityManager cm;


    public NetworkStateHolder() {
        cm = (ConnectivityManager) AppController.getInstance().getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public ConnectionType getConnectionType()
    {
        return connectionType;
    }

    public void setConnectionType(ConnectionType connectionType)
    {
        this.connectionType = connectionType;
    }

    public String getMessage()
    {
        return message;
    }
    public void setMessage(String message)
    {
        this.message = message;
    }
    public boolean isConnected()
    {
        if (cm == null)
            cm = (ConnectivityManager) AppController.getInstance().getApplicationContext()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }
    public void setConnected(boolean connected)
    {
        isConnected = connected;
    }
}
