package com.pairzy.com.locationScreen.model;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pairzy.com.R;
import com.pairzy.com.data.model.fourSq.Location;
import com.pairzy.com.data.model.fourSq.Venue;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

import javax.inject.Named;

/**
 * <h>LocationAdapter class.</h>
 * @author 3Embed.
 * @since 10/5/18.
 * @version 1.0.
 */
public class AddressAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int LOADING = 0, ITEM = 1;
    private TypeFaceManager typeFaceManager;
    private ItemActionCallBack listener;
    private ArrayList<Venue> addressList;

    public void setListener(ItemActionCallBack listener) {
        this.listener = listener;
    }

    public ArrayList<Venue> getAddressList() {
        return addressList;
    }

    public AddressAdapter(@Named("ADDRESS_LIST") ArrayList<Venue> addressList, TypeFaceManager typeFaceManager) {
        this.addressList =addressList;
        this.typeFaceManager = typeFaceManager;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case LOADING:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.load_more_layout, parent, false);
                return new LoadMoreLocationViewHolder(v,typeFaceManager,listener);
            case ITEM:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.address_item, parent, false);
                return new AddressItemViewHolder(v,typeFaceManager,listener);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        Venue venue = addressList.get(position);
        if(venue.isLoading())
            return LOADING;
        return ITEM;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        Venue venue = addressList.get(position);
        if(venue.isLoading()){
            try {
                initLoadingView((LoadMoreLocationViewHolder)holder);
            } catch (Exception e){
            }
        }
        else{
            try {
                initItemView((AddressItemViewHolder) holder);
            } catch (Exception e){
            }

        }
    }

    private void initLoadingView(LoadMoreLocationViewHolder holder) {
        Venue venue = addressList.get(holder.getAdapterPosition());
        if(venue.isLoadingError()){
            holder.setFailed();
        }else {
            holder.showLoadingLoadingAgain();
        }
    }

    private void initItemView(AddressItemViewHolder holder) {
        Venue venue = addressList.get(holder.getAdapterPosition());
        holder.locationName.setText(venue.getName());
        Location location = venue.getLocation();
        if(location != null){
            holder.addressTextview.setText(location.getAddress());
            holder.tvDistance.setText(String.valueOf(location.getDistance()>0?(location.getDistance()/1000)+"km":""));
        }
        holder.tvRating.setText(venue.getRating()==null?"":venue.getRating());
    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }
}