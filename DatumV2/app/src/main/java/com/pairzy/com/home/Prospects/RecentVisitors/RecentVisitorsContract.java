package com.pairzy.com.home.Prospects.RecentVisitors;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.home.Prospects.OnAdapterItemClicked;

/**
 * @since  3/23/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface RecentVisitorsContract
{
    interface View extends BaseView
    {
        void onDataUpdate();
        void showProgress();
        void onApiError(String message);
        void emptyData();
        void openUserProfile(String user_details);
        void initAdapterListener(OnAdapterItemClicked callBack);
    }

    interface Presenter extends BasePresenter<View>
    {
        void initAdapterListener();
        void initMatchListener();
        void  getListData(boolean isLoadMore);
        void pee_fetchProfile(int position);
        boolean checkLoadMore(int position);
    }
}
