package com.pairzy.com.data.model;

import android.content.Context;

import android.text.TextUtils;
import android.util.Log;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.CustomObserver.BoostViewCountObserver;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.notificationHelper.NotificationHelper;

import org.json.JSONObject;

/**
 * Created by ankit on 29/8/18.
 */

public class ProUserModel {

    private static final String TAG = ProUserModel.class.getName();

    private Context context;
    private PreferenceTaskDataSource dataSource;
    private Utility utility;
    private BoostViewCountObserver viewCountObserver;
    private NotificationHelper notificationHelper;

    public ProUserModel(Context context,
                        PreferenceTaskDataSource dataSource,
                        Utility utility,
                        BoostViewCountObserver viewCountObserver,
                        NotificationHelper notificationHelper) {
        this.context = context;
        this.dataSource = dataSource;
        this.utility = utility;
        this.viewCountObserver = viewCountObserver;
        this.notificationHelper = notificationHelper;
    }

    public void parseResponse(String response) {
        try{
            ProUserResponse proUserResponse = utility.getGson().fromJson(response,ProUserResponse.class);
            Subscription subs = proUserResponse.getData();
            Subscription savedSubsciption = dataSource.getSubscription();
            if(subs != null){
                if(savedSubsciption == null || !savedSubsciption.getSubscriptionId().equals(subs.getSubscriptionId())) {
                    notificationHelper.showNotification(context.getString(R.string.app_name), context.getString(R.string.admin_made_you_pro_msg), null);
                }
                dataSource.setSubscription(utility.getGson().toJson(subs,Subscription.class));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void parseLikeRefresh(String response) {
        try{
            LikeMqttResponse likeMqttResponse = utility.getGson().fromJson(response,LikeMqttResponse.class);
            dataSource.setRemainsLinksCount(likeMqttResponse.getNewLikes());
            dataSource.setNextLikeTime(0);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void parseLikeCount(String response) {
        try{
            Log.d(TAG, "parseLikeCount: response "+response);
            BoostCountMqttResponse boostCountMqttResponse =
                    utility.getGson().fromJson(response,BoostCountMqttResponse.class);
            BoostCountData boostCountData = boostCountMqttResponse.getBoost();
            Log.d(TAG, "parseLikeCount: viewCount "+boostCountData.getViews());
            dataSource.setDuringBoostViewCount(boostCountData.getViews());
            viewCountObserver.publishData(boostCountData);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void parseAddCoinResponse(JSONObject response) {
        try{
            Log.d(TAG, "parseAddCoinResponse: "+response.toString());
            int coin = response.getInt("data");
//            PendingIntent pendingIntent = PendingIntent.getActivity(context,0,new Intent(),PendingIntent.FLAG_ONE_SHOT);
//            showNotification(context.getString(R.string.app_name), String.format(Locale.getDefault(),"%s %d %s",
//                    context.getString(R.string.admin_added_text),
//                    coin,
//                    context.getString(R.string.coin_to_your_wallet_text)
//                    ),pendingIntent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void parseUserUnblocked(String response) {
        Log.d(TAG, "parseUserUnblocked: response "+response);
        try{
            JSONObject jsonObject = new JSONObject(response);
            String userId = jsonObject.getString("userId");
            if(!TextUtils.isEmpty(userId)){
                boolean isBlockedByMe = AppController.getInstance().getDbController().checkIfBlockedByMe(AppController.getInstance().getBlockedDocId(),userId);
                if(isBlockedByMe) {
                    AppController.getInstance().getDbController().addBlockedUser(AppController.getInstance().getBlockedDocId(), userId,userId,true,false);
                }
                else {
                    AppController.getInstance().getDbController().removeBlockedUser(AppController.getInstance().getBlockedDocId(), userId);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void parseUserBlocked(String response) {
        Log.d(TAG, "parseUserBlocked: response "+response);
        try{
            JSONObject jsonObject = new JSONObject(response);
            String userId = jsonObject.getString("userId");
            boolean isBlockedByMe = AppController.getInstance().getDbController().checkIfBlockedByMe(AppController.getInstance().getBlockedDocId(),userId);
            if(!TextUtils.isEmpty(userId)){
                AppController.getInstance().getDbController().addBlockedUser(AppController.getInstance().getBlockedDocId(),userId,userId,isBlockedByMe,true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
