package com.pairzy.com.momentGrid;

import android.app.Activity;
import android.content.Intent;

import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.momentGrid.model.MomentClickCallback;
import com.pairzy.com.momentGrid.model.MomentsGridAdapter;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.AppConfig;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * <h2>MomentsGridPresenter</h2>
 * <P> this is a presenter class consists bussiness logic</P>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */
public class MomentsGridPresenter implements MomentsGridContract.Presenter, MomentClickCallback {

    final static String TAG = MomentsGridPresenter.class.getName();

    @Inject
    PreferenceTaskDataSource dataSource;

    @Inject
    NetworkService networkService;
    @Inject
    MomentsGridModel model;
    @Inject
    MomentsGridContract.View view;

    @Inject
    MomentsGridPresenter() { }


    @Override
    public void takeView(MomentsGridContract.View view) {
        //already injected
    }

    @Override
    public void dropView() {
        this.view = null;
    }

    @Override
    public void onMomentClick(int position) {
        MomentsData postListDataPojo = model.getPostItem(position);
        if(postListDataPojo != null){
            if(view != null);
                view.launchMomentListScreen(position);
            //TODO launch vertical moment
        }
    }

    public void setAdapterCallback(MomentsGridAdapter momentsAdapter) {
        momentsAdapter.setClickCallback(this);
    }

    @Override
    public void handleOnActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == AppConfig.MOMENT_VERTICAL_REQ_CODE){
            if(resultCode == Activity.RESULT_OK) {
                ArrayList<MomentsData> momentsData = data.getParcelableArrayListExtra(ApiConfig.MOMENTS.MOMENTS_LIST);
                model.updateMomentData(momentsData);
                if(view != null)
                    view.notifyAdapter();
            }
        }
    }
}
