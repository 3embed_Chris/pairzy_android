package com.pairzy.com.home.AppSetting;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
/**
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface AppSettingContract
{
    interface View extends BaseView
    {
        void openBootDialog();
        void showError(String error);
        void initDataChange();
    }

    interface Presenter extends BasePresenter
    {

        void initSettingsDataChangeObserver();
        void showSuggestionAlert();
        void showBoostDialog();
    }
}
