package com.pairzy.com.util.notificationHelper;

import android.os.Bundle;

public interface NotificationHelper {

    void showLikeNotification(String title,String message); //open opponent profile page directly
    /*
     *open home page with discover tab selected.
     */
    void showNotification(String title, String message, Bundle bundle);
    /*
     * open campaign activity with parent home.
     */
    void showCampaignNotification(String title, String message, Bundle bundle);
    /*
     *open chatMessageActivity with home parent with chat tab.
     */
    void showChatNotification(String title,String message);
    /*
     *open match dialog with with home screen active.
     */
    void showMatchNotification(String title, String message);

}
