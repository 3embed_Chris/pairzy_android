package com.pairzy.com.home;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.home.AppSetting.AppSettingContract;
import com.pairzy.com.home.AppSetting.AppSettingFrag;
import com.pairzy.com.home.AppSetting.AppSettingPresenter;
import com.pairzy.com.home.Dates.DatesFrag;
import com.pairzy.com.home.Dates.DatesFragContract;
import com.pairzy.com.home.Dates.DatesFragPresenter;
import com.pairzy.com.home.Dates.Pending_page.PendingFrg;
import com.pairzy.com.home.Dates.Pending_page.PendingFrgBuilder;
import com.pairzy.com.home.Dates.Pending_page.PendingPageUtil;
import com.pairzy.com.home.Dates.pastDatePage.PastDateFrg;
import com.pairzy.com.home.Dates.pastDatePage.PastDateFrgBuilder;
import com.pairzy.com.home.Dates.pastDatePage.PastDatePageUtil;
import com.pairzy.com.home.Dates.upcomingPage.UpcomingFrg;
import com.pairzy.com.home.Dates.upcomingPage.UpcomingFrgBuilder;
import com.pairzy.com.home.Dates.upcomingPage.UpcomingUtil;
import com.pairzy.com.home.Discover.DiscoveryFrag;
import com.pairzy.com.home.Discover.DiscoveryFragContract;
import com.pairzy.com.home.Discover.DiscoveryFragPresenter;
import com.pairzy.com.home.Discover.GridFrg.GridDataViewContract;
import com.pairzy.com.home.Discover.GridFrg.GridDataViewFrg;
import com.pairzy.com.home.Discover.GridFrg.GridDataViewFrgBuilder;
import com.pairzy.com.home.Discover.GridFrg.GridDataViewPresenter;
import com.pairzy.com.home.Discover.ListFrg.ListDataViewFrg;
import com.pairzy.com.home.Discover.ListFrg.ListDataViewFrgBuilder;
import com.pairzy.com.home.Discover.ListFrg.ListDataViewFrgContract;
import com.pairzy.com.home.Discover.ListFrg.ListDataViewFrgPresenter;
import com.pairzy.com.home.MakeMatch.MakeMatchFrag;
import com.pairzy.com.home.Matches.Chats.ChatFragUtil;
import com.pairzy.com.home.Matches.Chats.ChatsFragModule;
import com.pairzy.com.home.Matches.Chats.ChatsFragment;
import com.pairzy.com.home.Matches.MatchesContract;
import com.pairzy.com.home.Matches.MatchesFrag;
import com.pairzy.com.home.Matches.MatchesPresenter;
import com.pairzy.com.home.Matches.PostFeed.PostFeedFrag;
import com.pairzy.com.home.Matches.PostFeed.PostFeedFragBuilder;
import com.pairzy.com.home.Matches.PostFeed.PostFeedFragUtil;
import com.pairzy.com.home.Prospects.LikesMe.LikeMeUtil;
import com.pairzy.com.home.Prospects.LikesMe.LikesMeBuilder;
import com.pairzy.com.home.Prospects.LikesMe.LikesMeFrg;
import com.pairzy.com.home.Prospects.MyLikes.MyLikesBuilder;
import com.pairzy.com.home.Prospects.MyLikes.MyLikesFrg;
import com.pairzy.com.home.Prospects.MyLikes.MyLikesUtil;
import com.pairzy.com.home.Prospects.MySuperlikes.MySuperlikesBuilder;
import com.pairzy.com.home.Prospects.MySuperlikes.MySuperlikesFrg;
import com.pairzy.com.home.Prospects.MySuperlikes.MySuperlikesUtil;
import com.pairzy.com.home.Prospects.Online.OnlineBuilder;
import com.pairzy.com.home.Prospects.Online.Online_frg;
import com.pairzy.com.home.Prospects.Online.Online_util;
import com.pairzy.com.home.Prospects.Passed.PassedBuilder;
import com.pairzy.com.home.Prospects.Passed.PassedFrg;
import com.pairzy.com.home.Prospects.Passed.PassedUtil;
import com.pairzy.com.home.Prospects.ProspectsContract;
import com.pairzy.com.home.Prospects.ProspectsFrg;
import com.pairzy.com.home.Prospects.ProspectsFrgPresenter;
import com.pairzy.com.home.Prospects.RecentVisitors.RecentVisitorsBuilder;
import com.pairzy.com.home.Prospects.RecentVisitors.RecentVisitorsFrg;
import com.pairzy.com.home.Prospects.RecentVisitors.RecentVisitorsUtil;
import com.pairzy.com.home.Prospects.SuperLikeMe.SuperLikeMeBuilder;
import com.pairzy.com.home.Prospects.SuperLikeMe.SuperLikeMeFrg;
import com.pairzy.com.home.Prospects.SuperLikeMe.SuperLikeMeUtil;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
/**
 * <h2>HomeActivityBuilder</h2>
 * <P>
 *
 * </P>
 * @since  2/21/2018.
 */
@Module
public abstract class HomeActivityBuilder
{
 @ActivityScoped
 @Binds
 abstract Activity provideActivity(HomeActivity homeActivity);

 @ActivityScoped
 @Binds
 abstract HomeContract.View provideView(HomeActivity homeActivity);

 @ActivityScoped
 @Binds
 abstract HomeContract.Presenter homePresenter(HomePresenter presenter);

 //tab1
 @FragmentScoped
 @ContributesAndroidInjector()
 abstract DiscoveryFrag getDiscoveryFrg();

 @ActivityScoped
 @Binds
 abstract DiscoveryFragContract.Presenter taskDiscoveryPrsenter(DiscoveryFragPresenter presenter);

 @FragmentScoped
 @ContributesAndroidInjector(modules={GridDataViewFrgBuilder.class})
 abstract GridDataViewFrg getGridDataViewFrg();

 @ActivityScoped
 @Binds
 abstract GridDataViewContract.Presenter taskGridPrsenter(GridDataViewPresenter presenter);

 @FragmentScoped
 @ContributesAndroidInjector(modules={ListDataViewFrgBuilder.class})
 abstract ListDataViewFrg getListDataViewFrg();

 @ActivityScoped
 @Binds
 abstract ListDataViewFrgContract.Presenter taskListPrsenter(ListDataViewFrgPresenter presenter);



 //tab2
 @FragmentScoped
 @ContributesAndroidInjector
 abstract ProspectsFrg getProspectsFrg();

 @ActivityScoped
 @Binds
 abstract ProspectsContract.Presenter taskPresenter(ProspectsFrgPresenter presenter);

 @FragmentScoped
 @ContributesAndroidInjector(modules = {OnlineBuilder.class,Online_util.class})
 abstract Online_frg getProspectItem_frg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {LikesMeBuilder.class,LikeMeUtil.class})
 abstract LikesMeFrg getLikesMe_frg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {MyLikesBuilder.class,MyLikesUtil.class})
 abstract MyLikesFrg getMyLikes_frg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {MySuperlikesBuilder.class,MySuperlikesUtil.class})
 abstract MySuperlikesFrg getMySuperlikes_frg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {PassedBuilder.class,PassedUtil.class})
 abstract PassedFrg getPassed_frg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {RecentVisitorsBuilder.class,RecentVisitorsUtil.class})
 abstract RecentVisitorsFrg getRecentVisitors_frg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {SuperLikeMeBuilder.class,SuperLikeMeUtil.class})
 abstract SuperLikeMeFrg getSuperLikeMeFrg();


 //tab3
 @FragmentScoped
 @ContributesAndroidInjector()
 abstract DatesFrag getDatesFrg();

 @ActivityScoped
 @Binds
 abstract DatesFragContract.Presenter getDatesPresenter(DatesFragPresenter presenter);

 @FragmentScoped
 @ContributesAndroidInjector(modules = {PendingFrgBuilder.class,PendingPageUtil.class})
 abstract PendingFrg getPendingFrg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {UpcomingFrgBuilder.class,UpcomingUtil.class})
 abstract UpcomingFrg getUpcomingFrg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {PastDateFrgBuilder.class, PastDatePageUtil.class})
 abstract PastDateFrg getPastDateFrg();

//tab4
 @FragmentScoped
 @ContributesAndroidInjector()
 abstract MatchesFrag getMatchesFrg();

 @ActivityScoped
 @Binds
 abstract MatchesContract.Presenter bindMatchesFragPresenter(MatchesPresenter presenter);

 @FragmentScoped
 @ContributesAndroidInjector(modules = {ChatsFragModule.class, ChatFragUtil.class})
 abstract ChatsFragment getChatFrg();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {PostFeedFragBuilder.class, PostFeedFragUtil.class})
 abstract PostFeedFrag getNewsFeedFrg();

 //tab5
 @FragmentScoped
 @ContributesAndroidInjector()
 abstract MakeMatchFrag makeMatchFrag();

 @FragmentScoped
 @Binds
 abstract AppSettingContract.Presenter providePresenter(AppSettingPresenter presenter);
}
