package com.pairzy.com.networking;

import android.annotation.SuppressLint;

import com.pairzy.com.BuildConfig;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule
{
    @Named("NODE")
    @Provides
    @Singleton
    Retrofit provideCall()
    {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    //Log.d("Intercepter", "ActualUrl: "+original.url() );
                    @SuppressLint("DefaultLocale")
                    Request request = original.newBuilder()
                            .header("Content-Type", "application/json")
                            .header("Cache-Control",String.format("max-age=%d", BuildConfig.CACHETIME))
                            .build();
                    Response response = chain.proceed(request);
                    response.cacheResponse();
                    return response;
                })
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASEURL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Named("PYTHON")
    @Provides
    @Singleton
    Retrofit provideCall2()
    {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    //Log.d("Intercepter", "ActualUrl: "+original.url() );
                    @SuppressLint("DefaultLocale")
                    Request request = original.newBuilder()
                            .header("Content-Type", "application/json")
                            .header("Cache-Control",String.format("max-age=%d", BuildConfig.CACHETIME))
                            .build();
                    Response response = chain.proceed(request);
                    response.cacheResponse();
                    return response;
                })
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASEURL2)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    NetworkService providesNetworkService( @Named("NODE")Retrofit retrofit) {
        return retrofit.create(NetworkService.class);
    }

    @Provides
    @Singleton
    @SuppressWarnings("unused")
    NetworkServicePy providesNetworkServiceForPy( @Named("PYTHON")Retrofit retrofit) {
        return retrofit.create(NetworkServicePy.class);
    }
}

