package com.pairzy.com.settings;

import android.content.Context;
import androidx.annotation.NonNull;

import com.androidinsta.com.InstagramManger;
import com.pairzy.com.AppController;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.settings.model.SettingsModel;
import com.pairzy.com.util.DeleteAccountDialog.DeleteAccDialogCallBack;
import com.pairzy.com.util.DeleteAccountDialog.DeleteAccountDialog;
import com.pairzy.com.util.LogoutDialog.LogoutAlertCallback;
import com.pairzy.com.util.LogoutDialog.LogoutDialog;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.progressbar.LoadingProgress;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h>PassportPresenter class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public class SettingsPresenter implements SettingsContract.Presenter ,LogoutAlertCallback ,DeleteAccDialogCallBack{

    @Inject
    SettingsContract.View view;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    InstagramManger instagramManger;
    @Inject
    LogoutDialog logoutDialog;
    @Inject
    DeleteAccountDialog deleteAccountDialog;
    @Inject
    Context context;
    @Inject
    SettingsModel model;
    @Inject
    Utility utility;
    @Inject
    NetworkService service;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    LoadingProgress loadingProgress;

    private CompositeDisposable compositeDisposable;

    @Inject
    public SettingsPresenter(){
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void loadLogoutDialog() {
        logoutDialog.showAlert(this);
    }

    @Override
    public void loadAppVersion() {
        if(view != null)
            view.showAppVersion(utility.getAppCurrentVersion());
    }

    @Override
    public void init() {
        if(view != null)
            view.applyFont();
    }

    @Override
    public void onLogout() {
//        model.deleteCalenderEvent();
//        dataSource.clear();
//        instagramManger.loggedOut();
//        AppController.getInstance().disconnect();
//        AppController.getInstance().unsubscribeToFirebaseTopic();
//        if(view != null)
//            view.showSplashScreen();
        AppController.getInstance().appLogout();
        if(view != null)
            view.finishActivity();
    }

    @Override
    public void onLogoutCancel() {
        //Nothing
    }

    @Override
    public void launchWebActivity(String title, @NonNull String url) {
        if(view != null)
            view.launchWebActivity(title,url);
    }

    void loadDeleteAccountDialog() {
        deleteAccountDialog.showDialog(dataSource.getName(),this);
    }

    @Override
    public void onDeleteAccount() {
        callDeleteAccountApi();
    }

    private void callDeleteAccountApi() {
        if(networkStateHolder.isConnected()){
            loadingProgress.show();
            service.deleteAcoount(dataSource.getToken(),model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if(value.code() == 200){
                                onLogout();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                        }
                    });
        }else {
//            if(view != null)
//                view.showError();
        }
    }

    public void dispose() {
        compositeDisposable.clear();
    }

}
