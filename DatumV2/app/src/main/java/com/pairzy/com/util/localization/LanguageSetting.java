/*
 * Copyright (c) 2015 Akexorcist
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.pairzy.com.util.localization;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.pairzy.com.data.local.PreferenceTaskKey;
import com.pairzy.com.util.AppConfig;

import java.util.Locale;

/**
 * Created by Akexorcist on 7/20/15 AD.
 */
public class LanguageSetting {
    //private static final String PREFERENCE_LANGUAGE = "pref_language";
    //private static final String KEY_LANGUAGE = "key_language";
    private static Locale DEFAULT_LANGUAGE = Locale.getDefault();
    public static void setDefaultLanguage(Locale locale) {
        DEFAULT_LANGUAGE = locale;
    }

    public static Locale getDefaultLanguage() {
        return DEFAULT_LANGUAGE;
    }

    public static void setLanguage(Context context, Locale locale) {
        Locale.setDefault(locale);
        SharedPreferences.Editor editor = getLanguagePreference(context).edit();
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.APP_LANGUAGE_ISO, locale.getLanguage());
        editor.apply();
    }

    public static Locale getLanguage(Context context) {

        boolean foundLanguage = false;
        for(String languageCode: AppConfig.APP_LANGUAGES){
            if(languageCode.equals(DEFAULT_LANGUAGE.getLanguage().split("_")[0])){
                foundLanguage = true;
            }
        }
        if(!foundLanguage) {
            Locale locale = new Locale(AppConfig.DEFAULT_LANGUAGE);
            setDefaultLanguage(locale);
        }

        String[] language = getLanguagePreference(context)
                .getString(PreferenceTaskKey.PreferenceTaskEntry.APP_LANGUAGE_ISO, DEFAULT_LANGUAGE.toString())
                .split("_");
        Locale locale;
        if (language.length == 1) {
            locale = new Locale(language[0]);
        } else if (language.length == 2) {
            locale = new Locale(language[0], language[1].toUpperCase());
        } else {
            locale = DEFAULT_LANGUAGE;
        }
        return locale;
    }

    private static SharedPreferences getLanguagePreference(Context context) {
        return context.getSharedPreferences(PreferenceTaskKey.PreferenceTaskEntry.PREFERENCE_NAME, Activity.MODE_PRIVATE);
    }
}
