package com.pairzy.com.home.Matches.Chats.Model;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.pairzy.com.AppController;
import com.pairzy.com.BaseModel;
import com.pairzy.com.MatchedView.MatchResponseData;
import com.pairzy.com.MqttChat.Database.CouchDbController;
import com.pairzy.com.MqttChat.Utilities.Utilities;
import com.pairzy.com.R;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.coinBalance.CoinResponse;
import com.pairzy.com.data.model.coinBalance.Coins;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigResponse;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.Exception.NoMoreDataException;
import com.pairzy.com.util.Exception.SimilarDataException;
import com.pairzy.com.util.ReportUser.ReportReasonResponse;
import com.pairzy.com.util.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import javax.inject.Inject;

/**
 * @author 3Embed.
 * @version 1.0.
 * @since 4/27/2018.
 */
public class ChatListModel extends BaseModel {

    private final String TAG = ChatListModel.class.getName();

    @Inject
    ChatListAdapter chatListAdapter;
    @Inject
    ArrayList<ChatListData> matchedList;
    @Inject
    ArrayList<ChatListItem> chatList;
    @Inject
    Utility utility;
    @Inject
    CouchDbController couchDbController;
    @Inject
    Activity context;
    @Inject
    ArrayList<String> reportReasonList;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    CoinConfigWrapper coinConfigWrapper;

    private int total_unread_message = 0;
    final String[] dTimeForDB = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "15", "30", "60", "3600", "86400", "604800"};//getResources().getStringArray(R.array.dTimeForDB);
    final String[] dTimeOptions = {"off", "1 second", "2 seconds", "3 seconds", "4 seconds", "5 seconds", "6 seconds", "7 seconds", "8 seconds", "9 seconds",
            "10 seconds", "15 seconds", "30 seconds", "1 minute", "1 hour", "1 day", "1 week"};

    @Inject
    ChatListModel() {
    }

    /*
     * gettning total unread message count.
     */
    public int getTotal_unread_message() {
        return total_unread_message;
    }

    public String getUnreadChatCount() {
        if(getTotal_unread_message() == 0)
            return "";
        if(getTotal_unread_message() > 9)
            return "9+";
        if(getTotal_unread_message() < 9)
            return String.valueOf(getTotal_unread_message());
        return "";
    }


    private void addFirstBoostItem(){
        ChatListData firstBoostItem = new ChatListData();
        firstBoostItem.setForBoost(true);
        matchedList.add(0,firstBoostItem);
    }

    /*
     * Parsing the data*/
    public void parseResponse(String response) throws Exception {
        try {
            matchedList.clear();
            addFirstBoostItem();
            chatList.clear();

            ChatListResponse result_data = utility.getGson().fromJson(response, ChatListResponse.class);
            ArrayList<ChatListData> temp = result_data.getData();

            if (temp != null && temp.size() > 0) {

                //couchDbController.updateSavedChatList(temp,AppController.getInstance().getChatDocId());
                for (int i = 0; i < temp.size(); i++) {
                    ChatListData chatListData = temp.get(i);
                    String docId = AppController.getInstance().findDocumentIdOfReceiver(chatListData.getRecipientId(), "");

                    //add in db if not exist;
                    if (TextUtils.isEmpty(docId)) {
                        docId = AppController.findDocumentIdOfReceiver(chatListData.getRecipientId(), Utilities.tsInGmt(), chatListData.getFirstName(),
                                chatListData.getProfilePic(), "", false, chatListData.getRecipientId(), chatListData.getChatId(), false);
                        chatListData.setTotalUnread(0);
                    }else{
                        Map<String,Object> chatMap = AppController.getInstance().getDbController().getParticularChatInfo(docId);
                        if(chatMap != null) {
                            chatListData.setTotalUnread(Integer.parseInt((String)chatMap.get("newMessageCount")));
                        }else{
                            chatListData.setTotalUnread(0);
                        }
                    }
                    if ((chatListData.getIsMatchedUser() == 1 && chatListData.getPayload().equals(AppConfig.DEFAULT_MESSAGE))) {
                        matchedList.add(temp.get(i));
                    } else {
                        //method
                        if (TextUtils.isEmpty(docId)) {
                            docId = AppController.findDocumentIdOfReceiver(chatListData.getRecipientId(), Utilities.tsInGmt(), chatListData.getFirstName(),
                                    chatListData.getProfilePic(), "", false, chatListData.getRecipientId(), chatListData.getChatId(), false);
                        }
                        addChatsToList(docId, chatListData, chatListData.getPayload().equals(AppConfig.DEFAULT_MESSAGE));
                    }

                    AppController.getInstance().getDbController().updateChatUserData(
                            docId,
                            chatListData.getFirstName(),
                            chatListData.getProfilePic(),
                            chatListData.getIsMatchedUser() > 0,
                            chatListData.getSuperlikedMe() > 0,
                            chatListData.isInitiated(),
                            chatListData.getPayload().equals(AppConfig.DEFAULT_MESSAGE)
                    );

                    if(chatListData.getIsBlocked() > 0 || chatListData.getIsBlockedByMe() > 0) {
                        AppController.getInstance().getDbController().addBlockedUser(AppController.getInstance().getBlockedDocId(), chatListData.getRecipientId(), chatListData.getRecipientId(), chatListData.getIsBlockedByMe() > 0, chatListData.getIsBlocked() > 0);
                    }else{
                        AppController.getInstance().getDbController().removeBlockedUser(AppController.getInstance().getBlockedDocId(),chatListData.getRecipientId());
                    }
                }
            } else {
                throw new EmptyData("Active ChatList is empty!");
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        //matchListAdapter.notifyDataSetChanged();
        notifyAdapter();
    }

    private void addChatsToList(String docId, ChatListData chatListData, boolean emptyChat) {
        ChatListItem chatListItem = new ChatListItem();

        chatListData.setHasDefaultMessage(chatListData.getPayload().equals(AppConfig.DEFAULT_MESSAGE));

        chatListItem.setBlocked(chatListData.getIsBlocked()>0);
        chatListItem.setBlockedByMe(chatListData.getIsBlockedByMe()>0);
        chatListItem.setInitiated(chatListData.isInitiated());
        chatListItem.setReceiverUid(chatListData.getRecipientId());
        chatListItem.setDocumentId(docId);
        chatListItem.setChatId(chatListData.getChatId());
        chatListItem.setSuperlikedMe(chatListData.getSuperlikedMe() > 0);

        //need to verify and change accordingly..
        String text = context.getString(R.string.new_message_text);
        if(emptyChat){
            text = context.getString(R.string.no_message_to_show_text);
            chatListItem.setShowTick(false);
        } else if (chatListData.getMessageType().equals("0")) {

            try {
                text = new String(Base64.decode(chatListData.getPayload(), Base64.DEFAULT), "UTF-8");
                chatListItem.setNewMessage(text);
            } catch (UnsupportedEncodingException ignored) { }

            if(chatListData.getSenderId().equals(dataSource.getUserId())) {
                chatListItem.setShowTick(true);
                chatListItem.setTickStatus(chatListData.getStatus());
            }
        } else {
            if(chatListData.getSenderId().equals(dataSource.getUserId())) {
                chatListItem.setShowTick(true);
                chatListItem.setTickStatus(chatListData.getStatus());
            }
        }

        chatListItem.setNewMessage(text);
        String tsInGmt = Utilities.epochtoGmt(chatListData.getTimestamp());

        chatListItem.setNewMessageCount(String.valueOf(chatListData.getTotalUnread()));
        chatListItem.setNewMessageTime(tsInGmt);
        chatListItem.setReceiverInContacts(false);
        chatListItem.setReceiverIdentifier(chatListData.getReceiverId());

        //chatListItem.setTickStatus(chatListData.getStatus());
        //need to verify and change accordingly.
        //chatListItem.setShowTick(false);

        chatListItem.setReceiverImage(chatListData.getProfilePic());
        chatListItem.sethasNewMessage(chatListData.getTotalUnread() > 0);
        chatListItem.setReceiverName(chatListData.getFirstName());

        chatListItem.setSecretChat(!(chatListData.getSecretId().equals("") || chatListData.getSecretId().isEmpty()));
        chatListItem.setSecretId(chatListData.getSecretId());
        chatListItem.setGroupChat(chatListData.isGroupChat());
        chatListItem.setMatchedUser(chatListData.getIsMatchedUser()>0);
        chatListItem.setUserOnline(chatListData.getOnlineStatus()>0);
        chatListItem.setSuperlikedMe(chatListData.getSuperlikedMe()>0);
        AppController.getInstance().getDbController().updateChatListForNewMessage(docId, text, chatListItem.hasNewMessage(), tsInGmt, tsInGmt);

        chatList.add(chatListItem);
    }


    /*
     * remove chatListData from matchList
     */
    private ChatListItem removeActiveChatListItem(String userId){
        for(ChatListItem chatListItem: chatList){
            if(chatListItem.getReceiverUid().equals(userId)){
                if(chatList.remove(chatListItem)){
                    return chatListItem;
                }
            }
        }
        return null;
    }
    /*
     * remove chatListData from matchList
     */
    private ChatListData removeMatchListItem(String userId){
        for(ChatListData chatListData: matchedList){
            if(chatListData.getRecipientId().equals(userId)){
                if(matchedList.remove(chatListData)){
                    return chatListData;
                }
            }
        }
        return null;
    }

    /*
     * adding the new matched chat item in Active chat list
     * */
    private void addNewActiveMatchChatItemFromDb(String userId) {
        try {

            String docId = AppController.getInstance().findDocumentIdOfReceiver(userId,"");
            //AppController.getInstance().getDbController().updateChatUserOnline(docId, chatListData1.getOnlineStatus() > 0);
            Map<String, Object> chat_item = AppController.getInstance().getDbController().getParticularChatInfo(docId);
            Log.d(TAG, "addNewFriendUser: chatMap: " + chat_item);

            String receiverUid = (String) chat_item.get("selfUid");

            boolean isBlocked = AppController.getInstance().getDbController().checkIfBlocked(AppController.getInstance().getBlockedDocId(),receiverUid);
            boolean isBlockedByMe = AppController.getInstance().getDbController().checkIfBlockedByMe(AppController.getInstance().getBlockedDocId(),receiverUid);

            ChatListItem chat = removeActiveChatListItem(receiverUid);
            if(chat == null)
                chat = new ChatListItem();
            if(chat_item.containsKey("initiated"))
                chat.setInitiated((Boolean) chat_item.get("initiated"));

            boolean hasDefaultMessage = (boolean)chat_item.get("hasDefaultMessage");
            chat.sethasNewMessage(hasDefaultMessage);

            boolean hasNewMessage = (Boolean) chat_item.get("hasNewMessage");
            chat.sethasNewMessage(hasNewMessage);
            if (hasNewMessage) {
                total_unread_message++;
                chat.setNewMessageTime((String) chat_item.get("newMessageTime"));
                chat.setNewMessage((String) chat_item.get("newMessage"));
                chat.setNewMessageCount((String) chat_item.get("newMessageCount"));
                chat.setShowTick(false);
            }else {
                Map<String, Object> map2 = couchDbController.getLastMessageDetails((String) chat_item.get("selfDocId"));
                String time = (String) map2.get("lastMessageTime");
                String message = (String) map2.get("lastMessage");
                if(message != null)  //was causing crash some time
                    if(message.equalsIgnoreCase("No messages to show!!"));

                chat.setShowTick((boolean) map2.get("showTick"));
                if ((boolean) map2.get("showTick")) {
                    chat.setTickStatus((int) map2.get("tickStatus"));
                }
                chat.setNewMessageTime(time);
                chat.setNewMessage(message);
                chat.setNewMessageCount("0");
                if (message == null) {
                    chat.setNewMessage("");
                }
            }
            chat.setReceiverIdentifier((String) chat_item.get("receiverIdentifier"));
            chat.setDocumentId((String) chat_item.get("selfDocId"));
            chat.setReceiverUid(receiverUid);
            chat.setMatchedUser((boolean) chat_item.get("isMatched"));
            chat.setSuperlikedMe((boolean)chat_item.get("isSuperlikedMe"));
            Object isOnline = chat_item.get("isOnline");
            if(isOnline != null)
                chat.setUserOnline((boolean)isOnline);

            /*
             *Given chat is the normal or the secret chat
             */
            /*
             * If the uid exists in contacts
             */
            chat.setGroupChat(false);
            chat.setReceiverInContacts(false);
            /*
             * If userId doesn't exists in contact
             */
            //chat.setReceiverName((String) chat_item.get("receiverIdentifier"));
            chat.setReceiverName((String) chat_item.get("receiverName"));
            chat.setReceiverImage((String) chat_item.get("receiverImage"));
            String image = (String) chat_item.get("receiverImage");
            if (image != null && !image.isEmpty()) {
                chat.setReceiverImage(image);
            } else {
                chat.setReceiverImage("");
            }

            chat.setBlocked(isBlocked);
            chat.setBlockedByMe(isBlockedByMe);

            //remove from chat list if exist
            for(ChatListItem item : chatList){
                if(item.getReceiverUid().equals(chat.getReceiverUid())){
                    chatList.remove(item);
                    break;
                }
            }
            chatList.add(0,chat);
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    /*
     * adding the new matched user
     * */
    public boolean addNewMatchedUser(ChatListData chatListData) {
        boolean isAddedToMatchList = false;

        try {
            String docId = null;
            if(chatListData != null)
                docId = AppController.getInstance().findDocumentIdOfReceiver(chatListData.getRecipientId(),"");
            if(!TextUtils.isEmpty(docId)) {

                Map<String, Object> chat_item = couchDbController.getParticularChatInfo(docId);

                boolean isMatch = (boolean) chat_item.get("isMatched");
                boolean hasDefaultMessage = (boolean)chat_item.get("hasDefaultMessage");

                boolean isInitiated = (Boolean) chat_item.get("initiated");
                //boolean hasNewMessage = (Boolean) chat_item.get("hasNewMessage");
                boolean isSuperlikedMe = (boolean) chat_item.get("isSuperlikedMe");

                String receiverUid = (String) chat_item.get("selfUid");
                String receiverIdentifier = (String) chat_item.get("receiverIdentifier");
                String receiverName = (String) chat_item.get("receiverName");
                String receiverImage = (String) chat_item.get("receiverImage");
                String chatId = (String) chat_item.get("chatId");
                String newMessageCount = (String) chat_item.get("newMessageCount");

                boolean isBlocked = AppController.getInstance().getDbController().checkIfBlocked(AppController.getInstance().getBlockedDocId(),receiverUid);
                boolean isBlockedByMe = AppController.getInstance().getDbController().checkIfBlockedByMe(AppController.getInstance().getBlockedDocId(),receiverUid);

                /*
                 * case 1 match list contain (update just data like "profilePic", "ChatId" and "isSuperLikedMe".
                 * case 2 chat list contain (update status to match and update others data as above.
                 * case 3: match list does not contain "add to match list" else
                 */
                if(hasDefaultMessage && isMatch){
                    //no need to add
                    ChatListData chatListData1 = removeMatchListItem(receiverUid);
                    if(chatListData1 == null)
                        chatListData1 = new ChatListData();

                    chatListData1.setChatId(chatId);
                    chatListData1.setRecipientId(receiverUid);
                    chatListData1.setReceiverIdentifier(receiverIdentifier);
                    chatListData1.setFirstName(receiverName);
                    chatListData1.setProfilePic(receiverImage);
                    chatListData1.setTotalUnread(Integer.parseInt(newMessageCount));

                    chatListData1.setIsMatchedUser(1);
                    chatListData1.setSuperlikedMe(isSuperlikedMe?1:0);
                    chatListData1.setInitiated(isInitiated);
                    chatListData1.setHasDefaultMessage(hasDefaultMessage);

                    chatListData1.setIsBlocked(isBlocked?1:0);
                    chatListData1.setIsBlockedByMe(isBlockedByMe?1:0);

                    matchedList.add(1,chatListData1);

                    isAddedToMatchList = true;
                    Log.d(TAG, "addNewMatchedUser: matchListItem came");
                }
                else{
                    addNewActiveMatchChatItemFromDb(receiverUid);
                    Log.d(TAG, "addNewMatchedUser: chatListItem came");
                }
            }
        }catch (Exception e){
            e.getMessage();
        }
        return isAddedToMatchList;
    }

    private boolean isMatchListContain(String recipientId) {
        boolean found= false;
        for(ChatListData chatListData : matchedList){
            if(chatListData.getRecipientId().equals(recipientId)) {
                found = true;
                break;
            }
        }
        return found;
    }

    private boolean isChatListContain(String recipientId) {
        String docId = AppController.getInstance().findDocumentIdOfReceiver(recipientId,"");
        if(TextUtils.isEmpty(docId))
            return false;
        return true;
    }

    /*
     * return collected chatList.
     */
    public ArrayList<ChatListItem> getChatList() {
        return chatList;
    }


    /*
     * validating the matched list.*/
    public void collectChatListDetails() {
        chatList.clear();
        matchedList.clear();
        addFirstBoostItem();
        total_unread_message = 0;

        Map<String, Object> map = couchDbController.getAllChatDetails(AppController.getInstance().getChatDocId());
        if (map != null) {
            ArrayList<String> receiverUidArray = (ArrayList<String>) map.get("receiverUidArray");
            ArrayList<String> receiverDocIdArray = (ArrayList<String>) map.get("receiverDocIdArray");
            Map<String, Object> chat_item;
            ChatListItem chat;
            ArrayList<Map<String, Object>> chats = new ArrayList<>();
            for (int i = 0; i < receiverUidArray.size(); i++) {
                chats.add(couchDbController.getParticularChatInfo(receiverDocIdArray.get(i)));
                Collections.sort(chats, new com.pairzy.com.MqttChat.Utilities.TimestampSorter());
            }

            for (int i = 0; i < chats.size(); i++) {
                chat_item = chats.get(i);
                boolean isMatch = (boolean) chat_item.get("isMatched");
                boolean hasDefaultMessage = (boolean)chat_item.get("hasDefaultMessage");

                boolean isInitiated = (Boolean) chat_item.get("initiated");
                boolean hasNewMessage = (Boolean) chat_item.get("hasNewMessage");
                boolean isSuperlikedMe = (boolean) chat_item.get("isSuperlikedMe");

                String receiverUid = (String) chat_item.get("selfUid");
                String receiverIdentifier = (String) chat_item.get("receiverIdentifier");
                String receiverName = (String) chat_item.get("receiverName");
                String receiverImage = (String) chat_item.get("receiverImage");
                String chatId = (String) chat_item.get("chatId");
                String newMessageCount = (String) chat_item.get("newMessageCount");

                boolean isBlocked = AppController.getInstance().getDbController().checkIfBlocked(AppController.getInstance().getBlockedDocId(),receiverUid);
                boolean isBlockedByMe = AppController.getInstance().getDbController().checkIfBlockedByMe(AppController.getInstance().getBlockedDocId(),receiverUid);

                if(isMatch && hasDefaultMessage){
                    //belong to match list
                    ChatListData chatListData = new ChatListData();
                    chatListData.setRecipientId(receiverUid);
                    chatListData.setReceiverIdentifier(receiverIdentifier);
                    chatListData.setFirstName(receiverName);
                    chatListData.setProfilePic(receiverImage);
                    chatListData.setTotalUnread(Integer.parseInt(newMessageCount));
                    chatListData.setIsMatchedUser(1);
                    chatListData.setHasDefaultMessage(hasDefaultMessage);
                    chatListData.setInitiated(isInitiated);
                    chatListData.setSuperlikedMe(isSuperlikedMe?1:0);

                    chatListData.setIsBlocked(isBlocked?1:0);
                    chatListData.setIsBlockedByMe(isBlockedByMe?1:0);

                matchedList.add(chatListData);
                }
                else {
                    chat = new ChatListItem();

                    chat.setBlocked(isBlocked);
                    chat.setBlockedByMe(isBlockedByMe);

                    chat.setInitiated(isInitiated);
                    chat.sethasNewMessage(hasNewMessage);

                    try {
                        if (((String) chat_item.get("secretId")).isEmpty()) {
                            chat.setSecretChat(false);
                            chat.setSecretId("");
                        } else {
                            chat.setSecretChat(true);
                            chat.setSecretId((String) chat_item.get("secretId"));
                        }
                    } catch (NullPointerException e) {
                        chat.setSecretChat(false);
                        chat.setSecretId("");
                    }

                    if (hasNewMessage) {
                        total_unread_message++;
                        chat.setNewMessageTime((String) chat_item.get("newMessageTime"));
                        chat.setNewMessage((String) chat_item.get("newMessage"));
                        chat.setNewMessageCount((String) chat_item.get("newMessageCount"));
                        chat.setShowTick(false);

                    } else {
                        Map<String, Object> map2 = couchDbController.getLastMessageDetails((String) chat_item.get("selfDocId"));
                        String time = (String) map2.get("lastMessageTime");
                        String message = (String) map2.get("lastMessage");

                        if (message != null)  //was causing crash some time
                            if (message.equalsIgnoreCase("No messages to show!!"))
                                continue;

                        chat.setShowTick((boolean) map2.get("showTick"));
                        if ((boolean) map2.get("showTick")) {
                            chat.setTickStatus((int) map2.get("tickStatus"));
                        }
                        chat.setNewMessageTime(time);
                        chat.setNewMessage(message);
                        chat.setNewMessageCount("0");
                        if (message == null) {
                            chat.setNewMessage("");
                        }
                    }
                    chat.setReceiverIdentifier(receiverIdentifier);
                    chat.setDocumentId((String) chat_item.get("selfDocId"));
                    chat.setReceiverUid(receiverUid);
                    chat.setMatchedUser(isMatch);
                    chat.setSuperlikedMe(isSuperlikedMe);

                    boolean isOnline = (boolean)chat_item.get("isOnline");
                    chat.setUserOnline(isOnline);

                    /*
                     *Given chat is the normal or the secret chat
                     */
                    /*
                     * If the uid exists in contacts
                     */
                    chat.setGroupChat(false);
                    chat.setReceiverInContacts(false);
                    /*
                     * If userId doesn't exists in contact
                     */
                    //chat.setReceiverName((String) chat_item.get("receiverIdentifier"));
                    chat.setReceiverName(receiverName);
                    chat.setReceiverImage(receiverImage);
                    chat.setChatId(chatId);
                    chatList.add(chat);
                }
            }
        }
    }

    /*
     * get the user from the given position from the chat list.*/
    public ChatListItem getChatUser(int position) throws NoMoreDataException {
        try {
            if (chatList.size() > 0) {
                return chatList.get(position);
            } else {
                throw new NoMoreDataException("There is no data in position " + position + "!");
            }
        } catch (Exception e) {
            throw new NoMoreDataException(e.getMessage());
        }
    }

    public ChatListData getMatchItem(int position) {
        if (position < matchedList.size())
            return matchedList.get(position);
        return null;
    }


    /*
     * helper method
     */
    private ArrayList<Integer> findContactPositionInList(String contactUid) {

        ArrayList<Integer> positions = new ArrayList<>();

        for (int i = 0; i < chatList.size(); i++) {
            if (chatList.get(i).getReceiverUid().equals(contactUid)) {
                positions.add(i);
            }
        }
        return positions;
    }

    public void updateProfilePic(JSONObject object) {
        /*
         * Profile pic update
         */
        try {
            String profilePic = object.getString("profilePic");
            ArrayList<Integer> arr = findContactPositionInList(object.getString("userId"));
            if (arr.size() > 0) {
                for (int i = 0; i < arr.size(); i++) {
                    ChatListItem item = chatList.get(arr.get(i));
                    item.setReceiverImage(profilePic);
                    chatList.set(arr.get(i), item);
                }
            }
        } catch (Exception e) {
        }
    }

    public void updateContactName(JSONObject object) {
        /*
         * Contact name or number changed but number still valid
         */
        try {
            ArrayList<Integer> arr2 = findContactPositionInList(object.getString("contactUid"));
            if (arr2.size() > 0) {
                for (int i = 0; i < arr2.size(); i++) {
                    ChatListItem item = chatList.get(arr2.get(i));
                    item.setReceiverName(object.getString("contactName"));
                    chatList.set(arr2.get(i), item);
                }
            }
        } catch (Exception e) {
        }
    }

    public void updateActiveContactNumber(JSONObject object) {
        /*
         * Number of active contact changed and new number not in contact
         */
        try {
            ArrayList<Integer> arr3 = findContactPositionInList(object.getString("contactUid"));
            if (arr3.size() > 0) {
                for (int i = 0; i < arr3.size(); i++) {
                    ChatListItem item = chatList.get(arr3.get(i));
                    item.setReceiverName(item.getReceiverIdentifier());
                    chatList.set(arr3.get(i), item);
                }
            }
        } catch (Exception e) {
        }
    }

    public void updateNewContact(JSONObject object) {
        /*
         * New contact added
         */
        try {
            ArrayList<Integer> arr4 = findContactPositionInList(object.getString("contactUid"));
            if (arr4.size() > 0) {
                for (int i = 0; i < arr4.size(); i++) {
                    ChatListItem item = chatList.get(arr4.get(i));

                    String profilePic2 = "";

                    if (object.has("contactPicUrl")) {
                        profilePic2 = object.getString("contactPicUrl");
                    }
                    item.setReceiverImage(profilePic2);
                    item.setReceiverName(object.getString("contactName"));
                    chatList.set(arr4.get(i), item);
                }
            }
        } catch (Exception e) {
        }
    }

    public void addActiveContactNumber(JSONObject object) {
        /*
         * Number was in active contact
         */
        try {
            object = object.getJSONArray("contacts").getJSONObject(0);
            if (object.has("status") && object.getInt("status") == 0) {

                ArrayList<Integer> arr2 = findContactPositionInList(object.getString("userId"));

                if (arr2.size() > 0) {
                    for (int i = 0; i < arr2.size(); i++) {
                        ChatListItem item = chatList.get(arr2.get(i));
                        item.setReceiverName(item.getReceiverIdentifier());
                        chatList.set(arr2.get(i), item);
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    /*
     * helper method.
     */
    private ArrayList<Integer> findInactiveContactPositionInList(String contactNumber) {

        ArrayList<Integer> positions = new ArrayList<>();

        for (int i = 0; i < chatList.size(); i++) {
            if (chatList.get(i).getReceiverIdentifier().equals(contactNumber)) {
                positions.add(i);
            }
        }

        return positions;
    }

    public void updateInactiveNameAndNumber(JSONObject object) {
        /*
         * need to add proper onComment
         */
        try {
            ArrayList<Integer> arr2 = findInactiveContactPositionInList(object.getString("number"));
            if (arr2.size() > 0) {
                for (int i = 0; i < arr2.size(); i++) {
                    ChatListItem item = chatList.get(arr2.get(i));
                    item.setReceiverName(object.getString("name"));
                    chatList.set(arr2.get(i), item);
                }
            }
        } catch (Exception e) {
        }
    }


    /*
     * To add the chats fetched from the server
     */
    public void addChatsFetchedFromServer(JSONArray chats) {
        total_unread_message = 0;
        chatList.clear();
        JSONObject chat;
        ChatListItem item;
        String message, receiverName, profilePic;
        int unreadCount;
        for (int j = chats.length() - 1; j >= 0; j--) {
            try {
                chat = chats.getJSONObject(j);

                /*
                 * If the corresponding chat is a normal or a secret chat
                 */
                unreadCount = chat.getInt("totalUnread");


                if (unreadCount > 0) {
                    total_unread_message++;
                }


                item = new ChatListItem();
                item.setReceiverUid(chat.getString("recipientId"));
                try {
                    item.setMatchedUser(chat.getInt("isMatchedUser")>0);
                } catch (Exception e){
                    e.printStackTrace();
                }

                item.setReceiverIdentifier(chat.getString("number"));
//                item.setReceiverImage(chat.getString("profilePic"));

                receiverName = chat.getString("number");


                if (chat.has("profilePic")) {

                    profilePic = chat.getString("profilePic");
                } else {


                    profilePic = "";
                }
                item.setReceiverImage(profilePic);
                item.setReceiverInContacts(false);

                item.setReceiverName(receiverName);
                item.setNewMessageTime(Utilities.epochtoGmt(String.valueOf(chat.getLong("timestamp"))));

                item.setDocumentId(AppController.getInstance().findDocumentIdOfReceiver(chat.getString("recipientId"), chat.getString("secretId")));

                if (chat.getString("senderId").equals(AppController.getInstance().getUserId())) {
                    item.setShowTick(true);
                    item.setTickStatus(chat.getInt("status"));
                } else {
                    item.setShowTick(false);
                }
                switch (Integer.parseInt(chat.getString("messageType"))) {

                    case 0:

                        message = chat.getString("payload").trim();

                        if (message.isEmpty()) {

                            String message_dTime = String.valueOf(chat.getLong("dTime"));


                            if (!message_dTime.equals("-1")) {

                                for (int i = 0; i < dTimeForDB.length; i++) {
                                    if (message_dTime.equals(dTimeForDB[i])) {

                                        if (i == 0) {


                                            message = context.getString(R.string.Timer_set_off);
                                        } else {


                                            message = context.getString(R.string.Timer_set_to) + " " + dTimeOptions[i];

                                        }
                                        break;
                                    }
                                }

                            } else {

                                if (chat.getString("senderId").equals(AppController.getInstance().getUserId())) {

                                    message = context.getResources().getString(R.string.YouInvited) + " " + receiverName + " " +
                                            context.getResources().getString(R.string.JoinSecretChat);

                                } else {
                                    message = context.getResources().getString(R.string.youAreInvited) + " " + receiverName + " " +
                                            context.getResources().getString(R.string.JoinSecretChat);
                                }

                                item.setShowTick(false);
                            }
                        } else {
                            try {
                                message = new String(Base64.decode(message, Base64.DEFAULT), "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                        }
                        break;

                    case 1:
                        if (unreadCount > 0) {
                            message = context.getString(R.string.NewImage);
                        } else {

                            message = context.getString(R.string.Image);
                        }
                        break;
                    case 2:

                        if (unreadCount > 0) {
                            message = context.getString(R.string.NewVideo);
                        } else {
                            message = context.getString(R.string.Video);

                        }
                        break;
                    case 3:
                        if (unreadCount > 0) {
                            message = context.getString(R.string.NewLocation);
                        } else {

                            message = context.getString(R.string.Location);
                        }
                        break;
                    case 4:
                        if (unreadCount > 0) {
                            message = context.getString(R.string.NewContact);
                        } else {

                            message = context.getString(R.string.Contact);
                        }
                        break;
                    case 5:
                        if (unreadCount > 0) {
                            message = context.getString(R.string.NewAudio);
                        } else {

                            message = context.getString(R.string.Audio);
                        }
                        break;
                    case 6:
                        if (unreadCount > 0) {
                            message = context.getString(R.string.NewSticker);
                        } else {

                            message = context.getString(R.string.Stickers);
                        }
                        break;
                    case 7:
                        if (unreadCount > 0) {
                            message = context.getString(R.string.NewDoodle);
                        } else {


                            message = context.getString(R.string.Doodle);
                        }
                        break;
                    case 8:
                        if (unreadCount > 0) {
                            message = context.getString(R.string.NewGiphy);
                        } else {

                            message = context.getString(R.string.Giphy);
                        }
                        break;


                    case 9:
                        if (unreadCount > 0) {
                            message = context.getString(R.string.NewDocument);
                        } else {

                            message = context.getString(R.string.Document);
                        }
                        break;

                    case 11: {

                        message = chat.getString("payload").trim();

                        item.setNewMessageTime(Utilities.epochtoGmt(chat.getString("removedAt")));

                        try {
                            message = new String(Base64.decode(message, Base64.DEFAULT), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                        break;
                    }
                    default: {


                        switch (Integer.parseInt(chat.getString("replyType"))) {


                            case 0:
                                message = chat.getString("payload").trim();


                                if (message.isEmpty()) {


                                    String message_dTime = String.valueOf(chat.getLong("dTime"));


                                    if (!message_dTime.equals("-1")) {

                                        for (int i = 0; i < dTimeForDB.length; i++) {
                                            if (message_dTime.equals(dTimeForDB[i])) {

                                                if (i == 0) {


                                                    message = context.getString(R.string.Timer_set_off);
                                                } else {


                                                    message = context.getString(R.string.Timer_set_to) + " " + dTimeOptions[i];

                                                }
                                                break;
                                            }
                                        }

                                    } else {

                                        if (chat.getString("senderId").equals(AppController.getInstance().getUserId())) {

                                            message = context.getResources().getString(R.string.YouInvited) + " " + receiverName + " " +
                                                    context.getResources().getString(R.string.JoinSecretChat);

                                        } else {
                                            message = context.getResources().getString(R.string.youAreInvited) + " " + receiverName + " " +
                                                    context.getResources().getString(R.string.JoinSecretChat);
                                        }

                                        item.setShowTick(false);
                                    }
                                } else {


                                    try {
                                        message = new String(Base64.decode(message, Base64.DEFAULT), "UTF-8");
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                }


                                break;
                            case 1:
                                if (unreadCount > 0) {
                                    message = context.getString(R.string.NewImage);
                                } else {

                                    message = context.getString(R.string.Image);
                                }
                                break;
                            case 2:

                                if (unreadCount > 0) {
                                    message = context.getString(R.string.NewVideo);
                                } else {
                                    message = context.getString(R.string.Video);

                                }
                                break;
                            case 3:
                                if (unreadCount > 0) {
                                    message = context.getString(R.string.NewLocation);
                                } else {

                                    message = context.getString(R.string.Location);
                                }
                                break;
                            case 4:
                                if (unreadCount > 0) {
                                    message = context.getString(R.string.NewContact);
                                } else {

                                    message = context.getString(R.string.Contact);
                                }
                                break;
                            case 5:
                                if (unreadCount > 0) {
                                    message = context.getString(R.string.NewAudio);
                                } else {

                                    message = context.getString(R.string.Audio);
                                }
                                break;
                            case 6:
                                if (unreadCount > 0) {
                                    message = context.getString(R.string.NewSticker);
                                } else {

                                    message = context.getString(R.string.Stickers);
                                }
                                break;
                            case 7:
                                if (unreadCount > 0) {
                                    message = context.getString(R.string.NewDoodle);
                                } else {


                                    message = context.getString(R.string.Doodle);
                                }
                                break;
                            case 8:
                                if (unreadCount > 0) {
                                    message = context.getString(R.string.NewGiphy);
                                } else {

                                    message = context.getString(R.string.Giphy);
                                }
                                break;


                            default:
                                if (unreadCount > 0) {
                                    message = context.getString(R.string.NewDocument);
                                } else {

                                    message = context.getString(R.string.Document);
                                }
                                break;


                        }


                    }


                }

                item.setNewMessage(message);
                item.setNewMessageCount(String.valueOf(chat.getInt("totalUnread")));


                item.sethasNewMessage(chat.getInt("totalUnread") > 0);

                item.setSecretId(chat.getString("secretId"));


                item.setSecretChat(!chat.getString("secretId").isEmpty());

                chatList.add(0, item);
            } catch (
                    JSONException e) {
                e.printStackTrace();
            }
        }
        //Show Empty Screen if
        //Show unreadCountMessge

    }

    public int removeUserFromMatchList(JSONObject object) {
        int pos = -1;
        try{
            String sender = object.getString("from");
            for(int i = 0; i < matchedList.size();i++){
                ChatListData chatListData = matchedList.get(i);
                if(chatListData.getRecipientId().equals(sender)){
                    matchedList.remove(chatListData);
                    pos = i;
                    break;
                }
            }
        }catch (Exception e){}
        return pos;
    }

    public void parseMessageReceived(JSONObject object) {
        try {
            Log.d(TAG, "parseMessageReceived: "+object.toString());
            String sender = object.getString("from");
            String timestamp = object.getString("timestamp");
            String messageType = object.getString("type");
            String name = object.getString("name");

            String message;

            switch (Integer.parseInt(messageType)) {
                case 0:
                    message = object.getString("payload").trim();

                    if (message.isEmpty()) {
                        String message_dTime = String.valueOf(object.getLong("dTime"));

                        if (!message_dTime.equals("-1")) {

                            for (int i = 0; i < dTimeForDB.length; i++) {
                                if (message_dTime.equals(dTimeForDB[i])) {

                                    if (i == 0) {
                                        message = context.getString(R.string.Timer_set_off);
                                    } else {
                                        message = context.getString(R.string.Timer_set_to) + " " + dTimeOptions[i];
                                    }
                                    break;
                                }
                            }

                        } else {

                            message = context.getResources().getString(R.string.youAreInvited) + " " + object.getString("contactName") + " " +
                                    context.getResources().getString(R.string.JoinSecretChat);

                        }
                    } else {

                        try {
                            message = new String(Base64.decode(message, Base64.DEFAULT), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    break;
                case 1:

                    message = context.getString(R.string.NewImage);
                    break;
                case 2:
                    message = context.getString(R.string.NewVideo);
                    break;
                case 3:
                    message = context.getString(R.string.NewLocation);
                    break;
                case 4:
                    message = context.getString(R.string.NewContact);
                    break;
                case 5:
                    message = context.getString(R.string.NewAudio);
                    break;
                case 6:
                    message = context.getString(R.string.NewSticker);
                    break;
                case 7:
                    message = context.getString(R.string.NewDoodle);
                    break;

                case 8:
                    message = context.getString(R.string.NewGiphy);
                    break;
                case 9:
                    message = context.getString(R.string.NewDocument);
                    break;

                case 11: {

                    /*
                     * Message removed
                     */
                    message = object.getString("payload").trim();
                    try {
                        message = new String(Base64.decode(message, Base64.DEFAULT), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    handleMessageRemovedSignal(message, sender, object.getString("removedAt"), object);
                    return;
                }

                case 12: {

                    /*
                     * Message edited
                     */

                    message = object.getString("payload").trim();
                    try {
                        message = new String(Base64.decode(message, Base64.DEFAULT), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    handleMessageEditedSignal(message, sender, object.getString("editedAt"), object);
                    return;
                }


                default: {

                    switch (Integer.parseInt(object.getString("replyType"))) {

                        case 0:
                            message = object.getString("payload").trim();


                            try {
                                message = new String(Base64.decode(message, Base64.DEFAULT), "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 1:

                            message = context.getString(R.string.NewImage);
                            break;
                        case 2:
                            message = context.getString(R.string.NewVideo);
                            break;
                        case 3:
                            message = context.getString(R.string.NewLocation);
                            break;
                        case 4:
                            message = context.getString(R.string.NewContact);
                            break;
                        case 5:
                            message = context.getString(R.string.NewAudio);
                            break;
                        case 6:
                            message = context.getString(R.string.NewSticker);
                            break;
                        case 7:
                            message = context.getString(R.string.NewDoodle);
                            break;

                        case 8:
                            message = context.getString(R.string.NewGiphy);
                            break;


                        default:
                            message = context.getString(R.string.NewDocument);
                            break;
                    }
                }
            }

            String secretId = "";
            if (object.has("secretId")) {
                secretId = object.getString("secretId");
            }

            ChatListItem chat = new ChatListItem();
            chat.setReceiverUid(sender);
            chat.setNewMessage(message);
            chat.setSecretId(secretId);
            chat.sethasNewMessage(true);
            chat.setDocumentId(AppController.getInstance().findDocumentIdOfReceiver(sender, secretId));
            chat.setSecretChat(!secretId.isEmpty());
            chat.setReceiverIdentifier(object.getString("receiverIdentifier"));
            chat.setUserOnline(true);
            chat.sethasNewMessage(false);
            /*
             * If the uid exists in contacts
             */

            /*
             * If userId doesn't exists in contact
             */
            chat.setReceiverName(object.getString("name"));
            //chat.setReceiverName(object.getString("receiverIdentifier"));
            if (object.has("userImage")) {
                chat.setReceiverImage(object.getString("userImage"));
            } else {
                chat.setReceiverImage("");
            }
            chat.setReceiverInContacts(false);


            chat.setNewMessageTime(Utilities.epochtoGmt(timestamp));
            chat.sethasNewMessage(true);
            chat.setNewMessageCount(AppController.getInstance().getDbController().getNewMessageCount(
                    AppController.getInstance().findDocumentIdOfReceiver(sender, secretId)));


            if (object.has("chatId")) {
                chat.setChatId(object.getString("chatId"));
            } else {
                chat.setChatId("");
            }

            if (object.has("isSuperlikedMe")) {
                chat.setSuperlikedMe(object.getBoolean("isSuperlikedMe"));
            }

            if (object.has("isMatchedUser")) {
                chat.setMatchedUser(object.getInt("isMatchedUser")>0);
            }
            if (object.has("chatId")) {
                String chatId = object.getString("chatId");
                if(!TextUtils.isEmpty(chatId))
                    chat.setChatId(chatId);
            }

            int alreadyInContact = alreadyInContact(sender, secretId);

            if (alreadyInContact == -1) {
                total_unread_message++;
                chat.setBlocked(AppController.getInstance().getDbController().checkIfBlocked(AppController.getInstance().getBlockedDocId(),chat.getReceiverUid()));
                chat.setBlockedByMe(AppController.getInstance().getDbController().checkIfBlockedByMe(AppController.getInstance().getBlockedDocId(),chat.getReceiverUid()));
                chatList.add(0, chat);
            } else {
                if (!chatList.get(alreadyInContact).hasNewMessage()) {
                    total_unread_message++;
                }
                //added extra
                chat.setSuperlikedMe(chatList.get(alreadyInContact).isSuperlikedMe());
                chat.setMatchedUser(chatList.get(alreadyInContact).isMatchedUser());
                String chatId = chatList.get(alreadyInContact).getChatId();
                if(!TextUtils.isEmpty(chatId))
                    chat.setChatId(chatId);
                chat.setUserOnline(true);
                chatList.remove(alreadyInContact);
                chatList.add(0, chat);
            }
        } catch (Exception e) {
        }
        //Update chat count.
        //notify data change.
    }

    private void handleMessageRemovedSignal(String message, String sender, String timestamp, JSONObject object) {
        try {
            String secretId = "";
            if (object.has("secretId")) {
                secretId = object.getString("secretId");
            }

            ChatListItem chat = new ChatListItem();
            chat.setReceiverUid(sender);
            chat.setNewMessage(message);
            chat.setSecretId(secretId);

            chat.setDocumentId(AppController.getInstance().findDocumentIdOfReceiver(sender, secretId));
            chat.setSecretChat(!secretId.isEmpty());
            chat.setReceiverIdentifier(object.getString("receiverIdentifier"));

            /*
             * If the uid exists in contacts
             */

            /*
             * If userId doesn't exists in contact
             */
            chat.setReceiverName(object.getString("name"));
            //chat.setReceiverName(object.getString("receiverIdentifier"));
            if (object.has("userImage")) {
                chat.setReceiverImage(object.getString("userImage"));
            } else {
                chat.setReceiverImage("");
            }
            chat.setReceiverInContacts(true);

            chat.setNewMessageTime(Utilities.epochtoGmt(timestamp));

            int alreadyInContact = alreadyInContact(sender, secretId);

            if (alreadyInContact == -1) {
                chat.sethasNewMessage(false);
                chatList.add(0, chat);
            } else {
                chat.sethasNewMessage(chatList.get(alreadyInContact).hasNewMessage());
                chat.setNewMessageCount(chatList.get(alreadyInContact).getNewMessageCount());

                //added extra
                chat.setSuperlikedMe(chatList.get(alreadyInContact).isSuperlikedMe());
                chat.setSuperlikedMe(chatList.get(alreadyInContact).isMatchedUser());
                chat.setUserOnline(true);

                chatList.remove(alreadyInContact);
                chatList.add(0, chat);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void handleMessageEditedSignal(String message, String sender, String timestamp, JSONObject object) {
        try {
            String secretId = "";
            if (object.has("secretId")) {
                secretId = object.getString("secretId");
            }
            ChatListItem chat = new ChatListItem();
            chat.setReceiverUid(sender);
            chat.setNewMessage(message);
            chat.setSecretId(secretId);
            chat.setDocumentId(AppController.getInstance().findDocumentIdOfReceiver(sender, secretId));
            chat.setSecretChat(!secretId.isEmpty());
            chat.setReceiverIdentifier(object.getString("receiverIdentifier"));
            /*
             * If the uid exists in contacts
             */

            /*
             * If userId doesn't exists in contact
             */
            chat.setReceiverName(object.getString("name"));
            //chat.setReceiverName(object.getString("receiverIdentifier"));
            if (object.has("userImage")) {
                chat.setReceiverImage(object.getString("userImage"));
            } else {
                chat.setReceiverImage("");
            }

            chat.setReceiverInContacts(true);
            chat.setNewMessageTime(Utilities.epochtoGmt(timestamp));

            int alreadyInContact = alreadyInContact(sender, secretId);

            if (alreadyInContact == -1) {
                chat.sethasNewMessage(false);
                chatList.add(0, chat);
            } else {
                /*
                 * Not to increment the unread messages count
                 *
                 */
                chat.sethasNewMessage(chatList.get(alreadyInContact).hasNewMessage());
                chat.setNewMessageCount(chatList.get(alreadyInContact).getNewMessageCount());

                //added extra
                chat.setSuperlikedMe(chatList.get(alreadyInContact).isSuperlikedMe());
                chat.setMatchedUser(chatList.get(alreadyInContact).isMatchedUser());
                chat.setUserOnline(true);
                chat.setChatId(chatList.get(alreadyInContact).getChatId());

                chatList.remove(alreadyInContact);
                chatList.add(0, chat);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * To fetch the chats from the couchdb, stored locally
     */
    private int alreadyInContact(String sender, String secretId) {
        int j = -1;
        for (int i = 0; i < chatList.size(); i++) {
            if (chatList.get(i).getReceiverUid().equals(sender)) {
//                if (chatList.get(i).getSecretId().equals(secretId)) {
//                    j = i;
//                    break;
//                }
                j = i;
                break;
            }
        }
        return j;
    }

    public void parseStatusUpdate(JSONObject object) {
        try {
            /*
             * Will update status only if not active on any of the chats
             */
            if (AppController.getInstance().getActiveReceiverId().isEmpty()) {

                String receiverId = object.getString("from");
                String secretId = "";

                if (object.has("secretId")) {
                    secretId = object.getString("secretId");
                }

                /*
                 * Although NOT THE BEST WAY,CAUSE NEED LAST MESSAGE ID FROM THE SERVER,but have done it for the time being
                 *
                 */
                updateLastMessageDeliveryStatus(receiverId, secretId, object.getString("msgId"), Integer.parseInt(object.getString("status")));
            }

        } catch (Exception e) {
        }
    }


    private void updateLastMessageDeliveryStatus(String receiverId, String secretId, String messageId, int status) {
        try {

            ChatListItem item;
            for (int i = 0; i < chatList.size(); i++) {

                item = chatList.get(i);

                if (item.getReceiverUid().equals(receiverId) && item.getSecretId().equals(secretId)) {

                    if (item.isShowTick()) {

                        if (AppController.getInstance().getDbController().checkIfLastMessage(item.getDocumentId(), messageId)) {

                            item.setTickStatus(status);
                            final int k = i;
                            try {
                                context.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        chatListAdapter.notifyItemChanged(k);

                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }


                    }
                    break;
                }

            }

        } catch (
                Exception e)

        {
            e.printStackTrace();
        }

    }

    /*
     * helper method
     */
    public void updateLastMessageDeliveryStatus(String docId, String messageId) {

        try {
            ChatListItem item;
            for (int i = 0; i < chatList.size(); i++) {

                item = chatList.get(i);

                if (item.getDocumentId().equals(docId)) {
                    if (item.isShowTick()) {

                        if (AppController.getInstance().getDbController().checkIfLastMessage(docId, messageId)) {
                            item.setTickStatus(1);
                            final int k = i;
                            try {
                                context.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        chatListAdapter.notifyItemChanged(k);
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }
                    break;
                }

            }
        } catch (
                Exception e)

        {
            e.printStackTrace();
        }
    }

    //helper methods
    public void notifyAdapter() {
        chatListAdapter.notifyDataSetChanged();
    }

    public void notifyAdapter(int position) {
        if (position > -1 && position < chatList.size()) {
            chatListAdapter.notifyItemChanged(position);
        }
    }

    public void removeFromMatchList(int position) {
        try {
            if (position < matchedList.size()) {
                matchedList.remove(position);
            }
        }catch (Exception e){}
    }


    //report user methods
    public boolean isReportReasonsEmpty() {
        return reportReasonList.isEmpty();
    }

    public ArrayList<String> getReasonsList() {
        return reportReasonList;
    }

    public void parseReportReasonData(String response) {
        try {
            ReportReasonResponse reasonResponse = utility.getGson().fromJson(response, ReportReasonResponse.class);
            if (reasonResponse.getData() != null) {
                reportReasonList = (ArrayList<String>) reasonResponse.getData();
            }
        } catch (Exception e) {
        }
    }

    public boolean removeFromChatList(ChatListItem chatListItem) {
        try {
            return chatList.remove(chatListItem);
        } catch (Exception e) {
        }
        return false;
    }

    public int removeFromMatchList(String userId){
        int pos = -1;
        for(int i = 0; i < matchedList.size(); i++){
            ChatListData chatListData = matchedList.get(i);
            if(chatListData.getRecipientId().equals(userId)){
                pos = i;
                matchedList.remove(pos);
                break;
            }
        }
        return pos;
    }

    public int removeFromChatList(String userId){
        int pos = -1;
        for(int i = 0; i < chatList.size(); i++){
            ChatListItem chatListItem= chatList.get(i);
            if(chatListItem.getReceiverUid().equals(userId)){
                pos = i;
                chatList.remove(pos);
                break;
            }
        }
        return pos;
    }


    public boolean removeFromMatchList(ChatListData matchListItemPojo) {
        try {
            return matchedList.remove(matchListItemPojo);
        } catch (Exception e) {
        }
        return false;
    }

    public boolean isMatchListEmpty() {
        return matchedList.isEmpty();
    }

    public boolean isChatListEmpty() {
        return chatList.isEmpty();
    }

    public String getCoinBalance() {
        return coinBalanceHolder.getCoinBalance();
    }


    public void parseCoinBalance(String response)
    {
        try
        {
            CoinResponse coinResponse = utility.getGson().fromJson(response,CoinResponse.class);
            Coins coins = coinResponse.getData().getCoins();
            coinBalanceHolder.setCoinBalance(String.valueOf(coins.getCoin()));
        }catch (Exception e){}
    }

    public ChatListData parseMatchedMessage(JSONObject obj) throws Exception,SimilarDataException,EmptyData{
        try
        {
            MatchResponseData data=utility.getGson().fromJson(obj.toString(),MatchResponseData.class);
            if(data==null)
            {
                throw  new EmptyData("CoinData is empty!");
            }
            ChatListData chatListData = new ChatListData();
            if(data.getFirstLikedBy().equals(dataSource.getUserId()))
            {
                chatListData.setFirstName(data.getSecondLikedByName());
                chatListData.setProfilePic(data.getSecondLikedByPhoto());
                chatListData.setRecipientId(data.getSecondLikedBy());
                chatListData.setSuperlikedMe(data.getIsSecondSuperLiked());

            }else
            {
                chatListData.setFirstName(data.getFirstLikedByName());
                chatListData.setProfilePic(data.getFirstLikedByPhoto());
                chatListData.setRecipientId(data.getFirstLikedBy());
                chatListData.setSuperlikedMe(data.getIsFirstSuperLiked());
            }
            return chatListData;
        }catch (Exception e)
        {
            throw new DataParsingException(e.getMessage());
        }
    }

    public void setChatUserAsBlocked(ChatListItem chatListItem) {
        try{
            AppController.getInstance().getDbController().addBlockedUser(AppController.getInstance().getBlockedDocId(),
                    chatListItem.getReceiverUid(),
                    chatListItem.getReceiverUid(),
                    true,
                    chatListItem.isBlocked()
                    );
            if(!chatList.isEmpty()){
                int pos = chatList.indexOf(chatListItem);
                chatListItem.setBlockedByMe(true);
                chatList.set(pos,chatListItem);
                chatListAdapter.notifyItemChanged(pos);
            }
        } catch (Exception e){}
    }

    public void setChatUserAsUnblocked(ChatListItem chatListItem) {
        try{
            if(chatListItem.isBlocked()){
                AppController.getInstance().getDbController().addBlockedUser(AppController.getInstance().getBlockedDocId(),
                        chatListItem.getReceiverUid(),
                        chatListItem.getReceiverUid(),
                        false,
                        chatListItem.isBlocked()
                        );
            }
            else{
                AppController.getInstance().getDbController().removeBlockedUser(AppController.getInstance().getBlockedDocId(),
                        chatListItem.getReceiverUid()
                        );
            }
            if(!chatList.isEmpty()){
                int pos = chatList.indexOf(chatListItem);
                chatListItem.setBlockedByMe(false);
                chatList.set(pos,chatListItem);
                chatListAdapter.notifyItemChanged(pos);
            }
        } catch (Exception e){}
    }

    public int getChatItemPosition(ChatListItem chatListItem) {
        try{
            if(!chatList.isEmpty()){
                return chatList.indexOf(chatListItem);
            }
        }catch (Exception e){}
        return -1;
    }

    public ChatListItem getChatItem(int currentPosition) {
        ChatListItem chatListItem = null;
        try{
            if(currentPosition > -1 && currentPosition < chatList.size())
                chatListItem = chatList.get(currentPosition);
        }catch (Exception e){}
        return chatListItem;
    }


    public String parseUserId(String response) {
        try{
            MqttUnmatchResponse mqttUnmatchResponse = utility.getGson().fromJson(response,MqttUnmatchResponse.class);
            return mqttUnmatchResponse.getTargetId();
        }catch (Exception e){

        }
        return "";
    }


    public void parseCoinConfig(String response)
    {
        try
        {
            CoinConfigResponse coinConfigResponse =
                    utility.getGson().fromJson(response,CoinConfigResponse.class);
            if(!coinConfigResponse.getData().isEmpty())
                coinConfigWrapper.setCoinData(coinConfigResponse.getData().get(0));
        }catch (Exception e){}
    }

    public void updateShowCoinDialogForChat(boolean dontShowAgain) {
        //update don't show pref
        dataSource.setShowChatCoinDialog(dontShowAgain);
    }


    public boolean isEnoughWalletBalanceToChat() {

        if(coinBalanceHolder.getCoinBalance() != null && coinConfigWrapper.getCoinData() != null){
            int coinRequired = coinConfigWrapper.getCoinData().getPerMsgWithoutMatch().getCoin();
            int walletBalance = Integer.parseInt(coinBalanceHolder.getCoinBalance());

            if(coinRequired <= walletBalance){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public boolean isDialogDontNeedToShowForChat() {
        return dataSource.getShowChatCoinDialog();
    }

    public String getCoinForUnmatchChat() {
        try{
            return ""+coinConfigWrapper.getCoinData().getPerMsgWithoutMatch().getCoin();
        }catch (Exception ignored){}
        return "";
    }
}
