package com.pairzy.com.MyProfile.Model;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.androidinsta.com.ImageData;
import com.pairzy.com.R;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.List;

public class GrideAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private int DATA=1,CLICK_VIew=2;
    private List<ImageData> list;
    private OpenInstagram instagram;
    private InstaMediaClickCallback callback;
    private ItemActionCallBack vhCallback;


    public void setCallback(InstaMediaClickCallback callback) {
        this.callback = callback;
    }

    public GrideAdapter(List<ImageData> list, OpenInstagram instagram)
    {
        this.instagram=instagram;
        this.list=list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        if(viewType==DATA)
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.insta_image_view,parent, false);
            return new InstaIamge(view);
        }else
        {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.opern_insta_view,parent, false);
            return new ViewInstaHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {

       int type=holder.getItemViewType();
       if(type==DATA)
       {
           try
           {
               InstaIamge data_holder= (InstaIamge) holder;
               data_holder.imageView.setImageURI(list.get(position).getThumbnail());
               data_holder.imageView.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       if(callback != null)
                           callback.onInstaPhotoClick(list.get(position).getPagePosition(),position);
                   }
               });
           }catch (Exception e)
           {
               e.printStackTrace();
           }
       }else
       {
           try
           {
               ViewInstaHolder data_holder= (ViewInstaHolder) holder;
               data_holder.openView.setOnClickListener(v -> {
                   if(instagram!=null)
                   {
                       instagram.openInstagram();
                   }
               });
           }catch (Exception e)
           {
               e.printStackTrace();
           }
       }

    }

    @Override
    public int getItemCount()
    {
        return list.size();
    }

    public class InstaIamge extends RecyclerView.ViewHolder
    {
        SimpleDraweeView imageView;
        public InstaIamge(View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.thumb_nail);
        }
    }

    @Override
    public int getItemViewType(int position)
    {
        if(list.get(position).getThumbnail()==null)
        {
          return CLICK_VIew;
        }else
        {
            return DATA;
        }
    }


    public class ViewInstaHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        LinearLayout openView;
        public ViewInstaHolder(View itemView)
        {
            super(itemView);
            openView=itemView.findViewById(R.id.openView);

        }

        @Override
        public void onClick(View v) {

        }
    }
}
