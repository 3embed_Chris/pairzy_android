package com.pairzy.com.userProfile.ImageItem;
import javax.inject.Inject;
/**
 * <h2>ImageItemPresenter</h2>
 * <P>
 *
 * </P>
 * @since /4/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ImageItemPresenter implements ImageItemContract.Presenter
{
    private ImageItemContract.View view;
    @Inject
    ImageItemPresenter()
    {}


    @Override
    public void takeView(ImageItemContract.View view)
    {
      this.view=view;
    }


    @Override
    public void dropView()
    {
        this.view=null;
    }

}
