package com.pairzy.com.util.SpendCoinDialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.Locale;

/**
 * <h2>ChatMenuDialog</h2>
 * <P>
 *
 * </P>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class CoinDialog
{
    private Activity activity;
    private Dialog dialog;
    private TypeFaceManager typeFaceManager;
    private Utility utility;
    private CoinDialogCallBack callBack;
    private CoinDialogBoostCallBack boostCallBack;
    private  AppCompatCheckBox checkBox;
    private WalletEmptyDialogCallback walletEmptyCallback;
    private CoinSpendDialogCallback coinSpendDialogCallback;

    public CoinDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility)
    {
        this.activity=activity;
        this.typeFaceManager=typeFaceManager;
        this.utility=utility;
    }

    public void showWalletEmptyDialog(String title, String msg,WalletEmptyDialogCallback callback)
    {
        this.walletEmptyCallback = callback;
        if(dialog != null && dialog.isShowing())
        {
            dialog.cancel();
        }
        @SuppressLint("InflateParams")
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.spend_coin_dialog, null);
        builder.setView(dialogView);

        RelativeLayout imageContainer = dialogView.findViewById(R.id.rl_image_container);
        ImageView ivEmptyWallet = dialogView.findViewById(R.id.iv_empty_wallet);
        //isWalletEmpty
        ivEmptyWallet.setVisibility(View.VISIBLE);
        imageContainer.setVisibility(View.GONE);
        TextView tvTitle =dialogView.findViewById(R.id.title);
        tvTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvTitle.setText(title);
        TextView tvMsg =dialogView.findViewById(R.id.msg);
        tvMsg.setText(msg);
        tvTitle.setTypeface(typeFaceManager.getCircularAirBook());
        Button btnCoin=dialogView.findViewById(R.id.coin_button);
        btnCoin.setTypeface(typeFaceManager.getCircularAirBook());
        btnCoin.setText(R.string.buy_coins_text);

        btnCoin.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();
                if (walletEmptyCallback != null)
                    walletEmptyCallback.onByMoreCoin();
        });

        RelativeLayout btnClose = dialogView.findViewById(R.id.close_button);
        btnClose.setOnClickListener(view ->{
            if(dialog != null)
                dialog.cancel();
        });
        checkBox = dialogView.findViewById(R.id.donot_show_check_box);
        LinearLayout llDonotShow = dialogView.findViewById(R.id.ll_do_not_show);
        llDonotShow.setVisibility(View.GONE);
        dialog = builder.create();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
    }


    public void showCoinSpendDialog(String title, String msg,String coinSpendButtonText,CoinSpendDialogCallback coinSpendDialogCallback)
    {
        this.coinSpendDialogCallback = coinSpendDialogCallback;
        if(dialog != null && dialog.isShowing())
        {
            dialog.cancel();
        }
        @SuppressLint("InflateParams")
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.spend_coin_dialog, null);
        builder.setView(dialogView);

        RelativeLayout imageContainer = dialogView.findViewById(R.id.rl_image_container);
        ImageView ivEmptyWallet = dialogView.findViewById(R.id.iv_empty_wallet);

        ivEmptyWallet.setVisibility(View.GONE);
        imageContainer.setVisibility(View.VISIBLE);

        TextView tvTitle =dialogView.findViewById(R.id.title);
        tvTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvTitle.setText(title);
        TextView tvMsg =dialogView.findViewById(R.id.msg);
        tvMsg.setText(msg);
        tvTitle.setTypeface(typeFaceManager.getCircularAirBook());
        Button btnCoin=dialogView.findViewById(R.id.coin_button);
        btnCoin.setTypeface(typeFaceManager.getCircularAirBook());
        btnCoin.setText(coinSpendButtonText);
        btnCoin.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();

            if (coinSpendDialogCallback != null)
                coinSpendDialogCallback.onOkToSpendCoin(checkBox.isChecked());
        });

        RelativeLayout btnClose = dialogView.findViewById(R.id.close_button);
        btnClose.setOnClickListener(view ->{
            if(dialog != null)
                dialog.cancel();
        });

        checkBox = dialogView.findViewById(R.id.donot_show_check_box);

        LinearLayout llDonotShow = dialogView.findViewById(R.id.ll_do_not_show);
        llDonotShow.setVisibility(View.VISIBLE);
        llDonotShow.setOnClickListener(view ->{
            handleCheckUncheck();
        });
        dialog = builder.create();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
    }




    public void showDialog(String userName,boolean isfemale,String title, String msg, String coinSpend , CoinDialogCallBack callBack, boolean isWalletEmpty,boolean isForSuperlike)
    {
        this.callBack = callBack;
        if(dialog != null && dialog.isShowing())
        {
            dialog.cancel();
        }
        @SuppressLint("InflateParams")
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.spend_coin_dialog, null);
        builder.setView(dialogView);

        RelativeLayout imageContainer = dialogView.findViewById(R.id.rl_image_container);
        ImageView ivEmptyWallet = dialogView.findViewById(R.id.iv_empty_wallet);

        if(isWalletEmpty){
            ivEmptyWallet.setVisibility(View.VISIBLE);
            imageContainer.setVisibility(View.GONE);
        }
        else{
            ivEmptyWallet.setVisibility(View.GONE);
            imageContainer.setVisibility(View.VISIBLE);
        }

        TextView tvTitle =dialogView.findViewById(R.id.title);
        tvTitle.setTypeface(typeFaceManager.getCircularAirBold());
        if(isWalletEmpty){
            tvTitle.setText(R.string.wallet_empty_title);
        }
        else {
            if(isForSuperlike){
                tvTitle.setText(String.format(Locale.ENGLISH,"Spend %s coins to let %s know you really like %s by super liking.",coinSpend,userName,isfemale?"her":"him"));
            }
            else {
                tvTitle.setText(title);
            }
        }

        TextView tvMsg =dialogView.findViewById(R.id.msg);
        tvMsg.setText(msg);
        if(isWalletEmpty){
            //tvMsg.setText("what msg to show here.");
        }
        else{
            //tvMsg.setText("what msg to show here.");
        }
        tvTitle.setTypeface(typeFaceManager.getCircularAirBook());

        Button btnCoin=dialogView.findViewById(R.id.coin_button);
        btnCoin.setTypeface(typeFaceManager.getCircularAirBook());
        if(isWalletEmpty) {
            btnCoin.setText(R.string.buy_coins_text);
        }
        else {
            btnCoin.setText(String.format(Locale.ENGLISH, " I am OK to spend %s coins", coinSpend));
        }
        btnCoin.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();
            if(isWalletEmpty){
                if (callBack != null)
                    callBack.onBuyCoin();
            }
            else {
                if (callBack != null)
                    callBack.onCoinButton(checkBox.isChecked());
            }
        });

        RelativeLayout btnClose = dialogView.findViewById(R.id.close_button);
        btnClose.setOnClickListener(view ->{
            if(dialog != null)
                dialog.cancel();
        });

        checkBox = dialogView.findViewById(R.id.donot_show_check_box);

        LinearLayout llDonotShow = dialogView.findViewById(R.id.ll_do_not_show);
        if(isWalletEmpty){
            llDonotShow.setVisibility(View.GONE);
        }
        else{
            llDonotShow.setVisibility(View.VISIBLE);
        }
        llDonotShow.setOnClickListener(view ->{
            handleCheckUncheck();
        });

        dialog = builder.create();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
    }


    public void showDialog(String title, String msg, String coinSpend ,CoinDialogBoostCallBack boostCallBack)
    {
        this.boostCallBack = boostCallBack;
        if(dialog != null && dialog.isShowing())
        {
            dialog.cancel();
        }
        @SuppressLint("InflateParams")
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.spend_coin_dialog, null);
        builder.setView(dialogView);

        RelativeLayout imageContainer = dialogView.findViewById(R.id.rl_image_container);
        ImageView ivEmptyWallet = dialogView.findViewById(R.id.iv_empty_wallet);

        ivEmptyWallet.setVisibility(View.GONE);
        imageContainer.setVisibility(View.VISIBLE);

        TextView tvTitle =dialogView.findViewById(R.id.title);
        tvTitle.setTypeface(typeFaceManager.getCircularAirBold());

        tvTitle.setText(String.format(Locale.ENGLISH,"Spend %s coins to boost your profile.",coinSpend));
        //tvTitle.setText(title);

        TextView tvMsg =dialogView.findViewById(R.id.msg);
        tvMsg.setText(msg);
        tvTitle.setTypeface(typeFaceManager.getCircularAirBook());
        Button btnCoin=dialogView.findViewById(R.id.coin_button);
        btnCoin.setTypeface(typeFaceManager.getCircularAirBook());

        btnCoin.setText(String.format(Locale.ENGLISH, " I am OK to spend %s coins", coinSpend));

        btnCoin.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();
            if (boostCallBack != null)
                boostCallBack.onBoostContinue();

        });

        RelativeLayout btnClose = dialogView.findViewById(R.id.close_button);
        btnClose.setOnClickListener(view ->{
            if(dialog != null)
                dialog.cancel();
        });

        checkBox = dialogView.findViewById(R.id.donot_show_check_box);

        LinearLayout llDonotShow = dialogView.findViewById(R.id.ll_do_not_show);
        llDonotShow.setVisibility(View.GONE);

        llDonotShow.setOnClickListener(view ->{
            handleCheckUncheck();
        });
        dialog = builder.create();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
    }

    public void showDialog(String coinAmount)
    {

        this.boostCallBack = boostCallBack;
        if(dialog != null && dialog.isShowing())
        {
            dialog.cancel();
        }
        @SuppressLint("InflateParams")
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.spend_coin_dialog, null);
        builder.setView(dialogView);

        RelativeLayout imageContainer = dialogView.findViewById(R.id.rl_image_container);
        ImageView ivEmptyWallet = dialogView.findViewById(R.id.iv_empty_wallet);

        ivEmptyWallet.setVisibility(View.GONE);
        imageContainer.setVisibility(View.VISIBLE);

        TextView tvTitle =dialogView.findViewById(R.id.title);
        tvTitle.setTypeface(typeFaceManager.getCircularAirBold());

        tvTitle.setText(String.format(Locale.ENGLISH,"Congratulations"));
        //tvTitle.setText(title);

        TextView tvMsg =dialogView.findViewById(R.id.msg);
        tvMsg.setText(String.format(Locale.ENGLISH, "Coins added to wallet successfully!!"));
        tvTitle.setTypeface(typeFaceManager.getCircularAirBook());
        Button btnCoin=dialogView.findViewById(R.id.coin_button);
        btnCoin.setTypeface(typeFaceManager.getCircularAirBook());

        btnCoin.setText(R.string.done);

        btnCoin.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();

        });

        RelativeLayout btnClose = dialogView.findViewById(R.id.close_button);
        btnClose.setOnClickListener(view ->{
            if(dialog != null)
                dialog.cancel();
        });

        checkBox = dialogView.findViewById(R.id.donot_show_check_box);

        LinearLayout llDonotShow = dialogView.findViewById(R.id.ll_do_not_show);
        llDonotShow.setVisibility(View.GONE);

        llDonotShow.setOnClickListener(view ->{
            handleCheckUncheck();
        });
        dialog = builder.create();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
    }


    private void handleCheckUncheck() {
        if(checkBox != null){
            if(checkBox.isChecked())
                checkBox.setChecked(false);
            else
                checkBox.setChecked(true);
        }
    }

}
