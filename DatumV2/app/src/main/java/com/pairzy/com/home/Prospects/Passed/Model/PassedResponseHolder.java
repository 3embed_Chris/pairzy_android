package com.pairzy.com.home.Prospects.Passed.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * @since 1/29/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class PassedResponseHolder
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<PassedItemPojo> data = null;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public ArrayList<PassedItemPojo> getData()
    {
        return data;
    }

    public void setData(ArrayList<PassedItemPojo> data)
    {
        this.data = data;
    }
}
