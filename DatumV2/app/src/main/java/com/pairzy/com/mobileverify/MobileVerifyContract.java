package com.pairzy.com.mobileverify;

import android.os.Bundle;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.mobileverify.Model.TimerCommunicator;

import dagger.android.support.DaggerFragment;
/**
 * <h2>PreferenceContract</h2>
 * <p>
 * Contains @{@link MobileVerifyActivity}'s @{@link View} and @{@link Presenter}
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 06/01/2018.
 **/
public interface MobileVerifyContract
{
    interface View
    {
        void updateInterNetStatus(boolean status);

        void openMainPhnoAgainFrg();
        /**
         * <p>Redirects to class</p>
         * @param data : intent data
         */
        void openSignUP(Bundle data);


        void openHomePage(Bundle data);
        /**
         * <p>Redirects to class</p>
         * @param fragment : destination fragment
         * @param isBackRequired : is to be kept in back stack or not
         */
        void moveFragment(DaggerFragment fragment, boolean isBackRequired);
        /**
         * <p>Displays message</p>
         * @param message : String message
         */
        void showMessage(String message);

        void setResultData(String phno);
    }


    interface Presenter extends BasePresenter
    {
        void initNetworkObserver();

        void setTimerListener(TimerCommunicator timerListener);

        void startTimer();

        void moveFragment(DaggerFragment fragment, boolean isBaclrequired);

        void doLogin(Bundle build);

        void doSingUp(Bundle build);

        void showMessage(String message);

        void openPhnoFrgAgain();

        void setResultData(String phno);
    }
}
