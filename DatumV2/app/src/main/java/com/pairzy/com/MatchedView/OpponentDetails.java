package com.pairzy.com.MatchedView;

/**
 * @since  4/13/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class OpponentDetails
{
    private String name;
    private String user_id;
    private String profile_pic;
    private Integer isSuperlikedMe;
    private String chatId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }


    public Integer getIsSuperlikedMe() {
        return isSuperlikedMe;
    }

    public void setIsSuperlikedMe(Integer isSuperlikedMe) {
        this.isSuperlikedMe = isSuperlikedMe;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }
}
