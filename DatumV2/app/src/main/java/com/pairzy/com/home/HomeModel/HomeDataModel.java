package com.pairzy.com.home.HomeModel;

import android.util.Log;

import com.pairzy.com.BaseModel;
import com.pairzy.com.boostDetail.model.SubsPlan;
import com.pairzy.com.boostDetail.model.SubsPlanResponse;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.Subscription;
import com.pairzy.com.data.model.SubscriptionResponse;
import com.pairzy.com.data.model.appversion.AppVersionData;
import com.pairzy.com.data.model.appversion.AppVersionResponse;
import com.pairzy.com.data.model.coinBalance.CoinResponse;
import com.pairzy.com.data.model.coinBalance.Coins;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigResponse;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Discover.DiscoveryFragUtil;
import com.pairzy.com.home.Discover.Model.UserItemPojo;
import com.pairzy.com.home.Discover.Model.superLike.SuperLikeResponse;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.Utility;
import com.suresh.innapp_purches.SkuDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * @since  4/12/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class HomeDataModel extends BaseModel
{
    private static final String TAG = HomeDataModel.class.getSimpleName();

    @Inject
    Utility utility;
    @Inject
    ArrayList<SubsPlan> subsPlanList;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    @Named(DiscoveryFragUtil.USER_LIST)
    ArrayList<UserItemPojo> userList;

    @Inject
    HomeDataModel(){}

    /*
     *Creating the form data for like service. */
    public Map<String, Object> setUserDetails(String user_id)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.DoLikeService.USER_ID, user_id);
        return map;
    }


    public boolean isSubsPlanEmpty()
    {
        return  subsPlanList.isEmpty();
    }



    public void selectMiddleItem()
    {
        int mid_pos = getSubsPlanListSize() / 2;
        for(int count=0;count<subsPlanList.size();count++)
        {
            SubsPlan subsPlan = subsPlanList.get(count);
            if(mid_pos==count)
            {
                subsPlan.setSelected(true);
                subsPlan.setHeaderTag("MOST POPULAR");
            }else
            {
                subsPlan.setSelected(false);
            }
        }
    }


    public List<String> collectsIds()
    {
        List<String> items=new ArrayList<>();
        for(SubsPlan item :subsPlanList)
        {
            items.add(item.getActualIdForAndroid());
        }
        return items;
    }


    public void updateDetailsData(List<SkuDetails> details)
    {
        for(SkuDetails item:details)
        {
            Log.d("8ry", ":"+item.toString());
            String id=item.getProductId();
            for(SubsPlan product :subsPlanList)
            {
                Log.d("8ry", ":"+product.toString());
                if(id.equals(product.getActualIdForAndroid()))
                {
                    product.setPrice_text(item.getPriceText());
                    product.setCurrencySymbol(item.getCurrency());
                }
            }
        }
    }


    public int getSubsPlanListSize()
    {
        return subsPlanList.size();
    }


    public ArrayList<SubsPlan> getSubsPlanList()
    {
        return subsPlanList;
    }

    public void clearProductList()
    {
        subsPlanList.clear();
    }

    public void parseSubsPlanList(String response)
    {
        subsPlanList.clear();
        SubsPlanResponse subsPlanResponse = utility.getGson().fromJson(response,SubsPlanResponse.class);
        ArrayList<SubsPlan> subsPlans = subsPlanResponse.getData();
//        List<String> ids=new ArrayList<>();
//        for(SubsPlan item:subsPlans)
//        {
//            ids.add(item.getActualId());
//        }
        subsPlanList.addAll(subsPlans);
    }


    public String getSubsPurchaseId(int position)
    {
        try {
            if (position<getSubsPlanListSize())
            {
                return subsPlanList.get(position).getActualIdForAndroid();
            }
        }catch (Exception ignored){}
        return "";

    }

    public String extractIDFromKey(String productID)
    {
        for(SubsPlan plan:subsPlanList)
        {
            if(plan.getActualId().equals(productID))
            {
                return plan.getId();
            }
        }
        return "";
    }

    public void parseSuperLike(String response)
    {
        try
        {
            SuperLikeResponse superLikeResponse = utility.getGson().fromJson(response, SuperLikeResponse.class);
            if(superLikeResponse.getCoinWallet() != null) {
                coinBalanceHolder.setCoinBalance(String.valueOf(superLikeResponse.getCoinWallet().getCoin()));
                coinBalanceHolder.setUpdated(true);
            }
        }catch (Exception e){}

    }


    public void parseBoostResponse(String response)
    {
        try
        {
            BoostResponse boostResponse = utility.getGson().fromJson(response, BoostResponse.class);
            if(boostResponse.getCoinWallet() != null) {;
                coinBalanceHolder.setCoinBalance(String.valueOf(boostResponse.getCoinWallet().getCoin()));
                coinBalanceHolder.setUpdated(true);
            }
            dataSource.setBoostExpireTime(boostResponse.getBoostExpireTime());
        }catch (Exception e){}
    }


    public void parseSubscription(String response)
    {
        try{
            SubscriptionResponse subsResponse = utility.getGson().fromJson(response,SubscriptionResponse.class);
            if(subsResponse != null)
            {
                Subscription subscription = subsResponse.getData().getSubscription().get(0);
                if(subscription != null){
                    String jsonSubs = utility.getGson().toJson(subscription,Subscription.class);
                    Log.d("23ert", ":"+jsonSubs);
                    dataSource.setSubscription(jsonSubs);
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    public void parseCoinBalance(String response) {
        try{
            CoinResponse coinResponse = utility.getGson().fromJson(response,CoinResponse.class);
            Coins coins = coinResponse.getData().getCoins();
            coinBalanceHolder.setCoinBalance(String.valueOf(coins.getCoin()));
            coinBalanceHolder.setUpdated(true);
        }catch (Exception e){
        }
    }

    public String getCoinBalance() {
        return coinBalanceHolder.getCoinBalance();
    }

    public void parseGetBoostApiData(String response) {
        try{
            GetBoostResponse boostResponse = utility.getGson().fromJson(response,GetBoostResponse.class);
            GetBoostData boostData = boostResponse.getData();
            dataSource.setBoostExpireTime(boostData.getExpiryTime());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public long getBoostExpireTime() {
        return dataSource.getBoostExpireTime();
    }

    public void parseLikeResponse(String response) {
        try{
            LikeResponse likeResponse = utility.getGson().fromJson(response,LikeResponse.class);
            dataSource.setRemainsLinksCount(likeResponse.getRemainsLikesInString());
            dataSource.setNextLikeTime(likeResponse.getNextLikeTime());
        }catch (Exception e){

        }
    }

    public int getRemainsLikeCount() {
        try {
            String str = dataSource.getRemainsLikesCount();
            if(str.equalsIgnoreCase("unlimited"))
                return 1;
            return Integer.parseInt(str);
        }catch (Exception e){
            return 0;
        }
    }

    public void parseCoinConfig(String response)
    {
        try
        {
            CoinConfigResponse coinConfigResponse =
                    utility.getGson().fromJson(response,CoinConfigResponse.class);
            if(!coinConfigResponse.getData().isEmpty())
                coinConfigWrapper.setCoinData(coinConfigResponse.getData().get(0));
        }catch (Exception e){}
    }

    public boolean isEnoughCoinForBoostProfile() {
        if(coinConfigWrapper.getCoinData() != null && coinBalanceHolder.getCoinBalance() != null) {
            int spendCoin = coinConfigWrapper.getCoinData().getBoostProfileForADay().getCoin();
            int walletBalance = Integer.parseInt(coinBalanceHolder.getCoinBalance());
            if(spendCoin <= walletBalance){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public boolean removeUserProfile(String user_id) {
        boolean isRemoved = false;
        try{
            for(UserItemPojo userItemPojo : userList){
                if(userItemPojo.getOpponentId().equals(user_id)){
                    if(userList.remove(userItemPojo)){
                        isRemoved = true;
                    }
                    break;
                }
            }
        }catch (Exception e){}
        return isRemoved;
    }

    /**
     *app update related data parse and methods
     */
    public AppVersionData parseAppVersionData(String response) {
        try{
            AppVersionResponse appVersionResponse = utility.getGson().fromJson(response,AppVersionResponse.class);
            AppVersionData appVersionData =  appVersionResponse.getData();

            //save the app version data
            if(!dataSource.getNewVersion().equals(appVersionData.getAppversion())){
                dataSource.setUpdateDialogShowed(false);
            }
            dataSource.setNewVersionData(appVersionData.getAppversion());
            dataSource.setIsMandatory(appVersionData.getIsMandatory());
            dataSource.setNewVersionUpdatedTime(System.currentTimeMillis());
            return appVersionData;
        }catch (Exception e){
        }
        return null;
    }

    public boolean isMandatory(AppVersionData appVersionData){
        try{
            return appVersionData.getIsMandatory();
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public boolean shouldShowUpdateDialog(AppVersionData appVersionData) {
        try {
            if (appVersionData == null) {
                Log.e(TAG, "shouldShowUpdateDialog:  appVersionData can not be null!");
                return false;
            }
            String appVersion = appVersionData.getAppversion();
            int lestestVersionSum = getVersionString(appVersion.trim());
            int currentVersionSum = getVersionString(utility.getAppCurrentVersion());
            boolean isMandatory = appVersionData.getIsMandatory();
            Log.d(TAG, "shouldShowUpdateDialog: lestestVersionSum, currentVersionSum"+lestestVersionSum+" , "+currentVersionSum);
            if(lestestVersionSum > currentVersionSum){
                if(isMandatory){
                    return true;
                }
                else{
                    return !dataSource.isUpdatedDialogShowed();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public boolean getSavedIsMandatory(){
        return dataSource.isMandatory();
    }
    public boolean shouldShowUpdateDialog() {
        try {
            String appVersion = dataSource.getNewVersion();
            int lestestVersionSum = getVersionString(appVersion.trim());
            int currentVersionSum = getVersionString(utility.getAppCurrentVersion());
            boolean isMendatory = getSavedIsMandatory();
            Log.d(TAG, "shouldShowUpdateDialog saved_data: lestestVersionSum, currentVersionSum"+lestestVersionSum+" , "+currentVersionSum);
            if(lestestVersionSum > currentVersionSum){
                if(isMendatory){
                    return true;
                }
                else{
                    return !dataSource.isUpdatedDialogShowed();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    private int getVersionString(String appVersion) {
        try {
            String version = appVersion.replaceAll("[.]","");
            return Integer.parseInt(version);
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    public boolean isNeedToCallUpdateVersionApi() {
//        if( (System.currentTimeMillis() - dataSource.getNewVersionUpdatedTime() >= AppConfig.UPDATE_CHECK_INTERVAL) || dataSource.getNewVersionUpdatedTime() == 0L){
//            //time is expired or first time
//            return true;
//        }else if(dataSource.getNewVersion().isEmpty()){
//            return true;
//        }
        return true;
    }
}
