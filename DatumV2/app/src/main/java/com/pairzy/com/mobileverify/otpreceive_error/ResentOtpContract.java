package com.pairzy.com.mobileverify.otpreceive_error;

import com.pairzy.com.BasePresenter;

public interface ResentOtpContract
{
    interface View
    {
        void showMessage(String message);

        void activeSMSButton(boolean isToActive);

        void verifyNumber();

        void checkMsgInbox();

        void sendFacebookNotification();

        void sendSMSAgain();

        void doPhnoCall();

        void updateTimer(String time);

        void showResentButton();
    }

    interface Presenter extends BasePresenter
    {
        void listenTimer();

        void doEditPhno();

        void doSmsAgain();

        void sendOtpOnFB();

        void doOtpCall();

        void showError(String message);
    }
}
