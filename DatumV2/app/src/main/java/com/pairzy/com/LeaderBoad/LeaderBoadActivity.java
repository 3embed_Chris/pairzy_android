package com.pairzy.com.LeaderBoad;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.pairzy.com.LeaderBoad.model.LeaderBoadAdapter;
import com.pairzy.com.LeaderBoad.model.LeaderBoardDataPOJO;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatActivity;

public class LeaderBoadActivity extends DaggerAppCompatActivity implements LeaderBoadContract.View,SwipeRefreshLayout.OnRefreshListener {

    @Inject
    LeaderBoadAdapter adapter;

    @Inject
    TypeFaceManager typeFaceManager;

    @Inject
    LinearLayoutManager  layoutManager;

    @Inject
    LeaderBoadContract.Presenter presenter;

    @Inject
    ArrayList<LeaderBoardDataPOJO> arrayList;

    @BindView(R.id.rvLeaderBoad)
    RecyclerView recyclerView;

    @BindView(R.id.tvLeaderBoardTitle)
    TextView title;

    @BindView(R.id.tvLeaderBoardCity)
    TextView city;

    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout refreshLayout;

    @BindView(R.id.parentLayout)
    ConstraintLayout layout;

    @BindView(R.id.iv_empty_item)
    ImageView ivEmpty;

    @BindView(R.id.tv_empty_item)
    TextView tvEmpty;

    @BindView(R.id.tv_empty_item1)
    TextView tvEmpty1;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_boad);
        unbinder= ButterKnife.bind(this);
        presenter.fetchDataFromApi();
        init();
        initFontFamily();
        initRecycler();
        setCity();

    }

    private void setCity() {
        if(arrayList.size()>0)
            city.setText(arrayList.get(0).getCity());
    }

    private void initFontFamily() {
        title.setTypeface(typeFaceManager.getCircularAirBold());
        city.setTypeface(typeFaceManager.getCircularAirBook());
        tvEmpty.setTypeface(typeFaceManager.getCircularAirBook());
        tvEmpty1.setTypeface(typeFaceManager.getCircularAirLight());
    }

    void showData(){
        recyclerView.setVisibility(View.VISIBLE);
        ivEmpty.setVisibility(View.GONE);
        tvEmpty.setVisibility(View.GONE);
        tvEmpty1.setVisibility(View.GONE);
    }

    void hideData(){
        recyclerView.setVisibility(View.GONE);
        ivEmpty.setVisibility(View.VISIBLE);
        tvEmpty1.setVisibility(View.VISIBLE);
        tvEmpty.setVisibility(View.VISIBLE);
    }

    public  void isEmpty() {
        if(arrayList!=null) {
            if(arrayList.size()>0)
                showData();
            else
                hideData();
        }
    }

    private void initRecycler() {
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void init() {
        refreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void showError(String error) {
        Snackbar snackbar=Snackbar.make(layout,error,Snackbar.LENGTH_SHORT);
        View view=snackbar.getView();
        view.setBackgroundColor(getResources().getColor(R.color.red_color));
        TextView textView=view.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this,R.color.white));
        snackbar.show();
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(false);
        presenter.fetchDataFromApi();
        setCity();
    }


    @OnClick(R.id.ivLeaderBoardBack)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
