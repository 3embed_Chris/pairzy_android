package com.pairzy.com.MyProfile.Model;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.androidinsta.com.ImageData;
import com.pairzy.com.R;

import java.util.List;

public class InstaMediaAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private List<List<ImageData>> media_list;
    private Context mcontext;
    private OpenInstagram instagram;
    private InstaMediaClickCallback callback;


    public void setCallback(InstaMediaClickCallback callback) {
        this.callback = callback;
    }

    public InstaMediaAdapter(Context context, OpenInstagram openInstagram , List<List<ImageData>> lists)
    {
        this.instagram=openInstagram;
        this.media_list=lists;
        this.mcontext=context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.insta_media_view,parent, false);
        return new InstaMediaHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        try
        {
            handeData((InstaMediaHolder) holder);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return media_list.size();
    }

    private void handeData(InstaMediaHolder holder)
    {
        int position=holder.getAdapterPosition();
        for(ImageData imageData :media_list.get(position)){
            imageData.setPagePosition(position);
        }
        GrideAdapter grideAdapter=new GrideAdapter(media_list.get(position),instagram);
        grideAdapter.setCallback(callback);
        holder.item_grid.setAdapter(grideAdapter);
        holder.item_grid.setLayoutManager(new GridLayoutManager(mcontext,3));
    }
}
