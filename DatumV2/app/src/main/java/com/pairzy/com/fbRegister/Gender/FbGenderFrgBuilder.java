package com.pairzy.com.fbRegister.Gender;

import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.register.RegisterPage;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>FbGenderFrgBuilder</h2>
 * <P>
 *     Dagger module class for the Gender selection and it is
 *     part of the registration activity.
 *     {@link RegisterPage}
 * </P>
 * @since  2/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface FbGenderFrgBuilder
{
    @FragmentScoped
    @Binds
    FbGenderFragment getEmailFragment(FbGenderFragment genderFragment);

    @FragmentScoped
    @Binds
    FbGenderContract.Presenter taskPresenter(FbGenderPresenter presenter);
}
