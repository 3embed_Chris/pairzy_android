package com.pairzy.com.MqttChat.Service;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.IBinder;
import android.os.SystemClock;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.pairzy.com.AppController;
import com.pairzy.com.MqttChat.MQtt.MqttService;
import com.pairzy.com.R;
import com.pairzy.com.splash.SplashActivity;
import com.pairzy.com.util.AppConfig;

import static com.pairzy.com.util.AppConfig.NOTIFICATION_CHANNEL_ID_FOURGROUND_SERVICE;
import static com.pairzy.com.util.AppConfig.NOTIFICATION_CHANNEL_NAME_CHAT_SERVICE;


/**
 * Created by moda on 21/06/17.
 */

public class AppKilled extends Service {

    private NotificationManager mNotificationManager;

    @Override
    public void onCreate() {
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

//        if((intent.getAction() != null && intent.getAction().equals(AppConfig.ACTION_START_FOURGROUND)) || Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ){
//            attachNotification();
//        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    public void onTaskRemoved(Intent rootIntent) {

        if (AppController.getInstance().isActiveOnACall()) {
/*
 *If active on a call and the app is slided off,then have to make myself available
 */
            AppController.getInstance().cutCallOnKillingApp(false);
            Log.w(AppKilled.class.getSimpleName(),"appkilled service started as foreground!!");
        }
        AppController.getInstance().disconnect();
        AppController.getInstance().setApplicationKilled(true);
        AppController.getInstance().createMQttConnection(AppController.getInstance().getUserId(),true);
        /*
         * For sticky service not restarting on KITKAT devices
         */
        Intent restartService = new Intent(getApplicationContext(),
                MqttService.class);
        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(
                getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);

        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 1000, restartServicePI);
        stopSelf();
    }

    private void attachNotification() {
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent  = PendingIntent.getActivity(getApplicationContext(),112,intent,0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_launcher);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.foreground_service_title))
                .setTicker("Datum chat")
                .setContentText(getString(R.string.app_name))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(
                        Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true);
        //.build();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_FOURGROUND_SERVICE, NOTIFICATION_CHANNEL_NAME_CHAT_SERVICE, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID_FOURGROUND_SERVICE);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        startForeground(AppConfig.NOTIFICATION_ID.FOREGROUND_SERVICE,
                mBuilder.build());
    }
}