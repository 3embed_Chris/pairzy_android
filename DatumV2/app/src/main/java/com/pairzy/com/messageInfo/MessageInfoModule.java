package com.pairzy.com.messageInfo;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

@Module
public interface MessageInfoModule {

    @ActivityScoped
    @Binds
    MessageInfoContract.View bindsView(MessageInfoActivity messageInfo);

    @ActivityScoped
    @Binds
    MessageInfoContract.Presenter bindsPresenter(MessageInfoPresenter presenter);

    @ActivityScoped
    @Binds
    Activity bindsActivity(MessageInfoActivity messageInfoActivity);
}
