package com.pairzy.com.home.Discover.Model;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.appcompat.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import com.pairzy.com.R;
import java.util.ArrayList;
/**
 *<h2>DiscoveryPagerAdapter</h2>
 * <P>
 *
 * </P>
 * @since 3/3/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class DiscoveryPagerAdapter extends FragmentPagerAdapter
{
    private Context context;
    private ArrayList<Fragment> list_frg;

    public DiscoveryPagerAdapter(Context context, FragmentManager fm, ArrayList<Fragment> list_data)
    {
        super(fm);
        this.context = context;
        this.list_frg=list_data;
    }

    @Override
    public int getCount()
    {
        return list_frg.size();
    }

    @Override
    public Fragment getItem(int position)
    {
       return  list_frg.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return list_frg.get(position).getClass().getName();
    }
    /*
     * Getting the custom adapter.*/
    public View getTabView(int position)
    {
        @SuppressLint("InflateParams")
        View tab = LayoutInflater.from(context).inflate(R.layout.home_tab_one, null);
        AppCompatImageView tab_img=tab.findViewById(R.id.tab_img);
        if(position==0)
            tab_img.setImageResource(R.drawable.home_tab_img_two_selector);
        else
            tab_img.setImageResource(R.drawable.hom_tab_img_one_selector);
        return tab;
    }
}