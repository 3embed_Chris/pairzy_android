package com.pairzy.com.momentGrid.model;

public interface MomentClickCallback {
    void onMomentClick(int position);
}
