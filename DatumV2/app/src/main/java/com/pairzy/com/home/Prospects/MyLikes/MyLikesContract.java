package com.pairzy.com.home.Prospects.MyLikes;
import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.home.Prospects.OnAdapterItemClicked;

/**
 * @since  3/23/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface MyLikesContract
{
    interface View extends BaseView
    {
        void openUserProfile(String data);
        void onDataUpdate();
        void showProgress();
        void onApiError(String message);
        void emptyData();
        void adapterListener(OnAdapterItemClicked adapterCallabck);
    }

    interface Presenter extends BasePresenter<View>
    {
        void initAdapterListener();
        void  getListData(boolean isLoadMore);
        void pee_fetchProfile(int position);
        boolean checkLoadMore(int position);
        void initMatchListener();
    }
}
