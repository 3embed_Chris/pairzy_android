package com.pairzy.com.messageInfo;

import javax.inject.Inject;

public class MessageInfoPresenter implements MessageInfoContract.Presenter {

    @Inject
    MessageInfoContract.View view;

    @Inject
    public MessageInfoPresenter() {
    }

    @Override
    public void takeView(MessageInfoContract.View view) {
        //directly injected
    }

    @Override
    public void dropView() {
        this.view = null;
    }
}
