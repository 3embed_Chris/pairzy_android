package com.pairzy.com.home.MakeMatch;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.google.android.material.snackbar.Snackbar;
import com.pairzy.com.LeaderBoad.LeaderBoadActivity;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.HomeContract;
import com.pairzy.com.home.MakeMatch.model.MakeMatchDataPOJO;
import com.pairzy.com.home.MakeMatch.model.MakeMatchModel;
import com.pairzy.com.home.MakeMatch.model.RecyclerViewSwipeDecorator;
import com.pairzy.com.home.MakeMatch.swipeCardModel.MatchMakerAdapter;
import com.pairzy.com.home.MakeMatch.swipeCardModel.PairMakerAdapter;
import com.pairzy.com.home.MakeMatch.swipeCardModel.PairMakerViewHolder;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

public class MakeMatchFrag extends DaggerFragment implements MakeMatchContract.View{

    private static final String TAG =MakeMatchFrag.class.getSimpleName() ;

    @Inject
    Activity activity;

    @Inject
    HomeContract.Presenter homePresenter;

    @Inject
    MakeMatchPresenter presenter;

    @Inject
    PairMakerAdapter matchMakerAdapter;

    @Named(MakeMatchUtil.MAKE_MATCH)
    @Inject
    ArrayList<MakeMatchDataPOJO> arrayList;

    @Inject
    TypeFaceManager typeFaceManager;

    @Inject
    PreferenceTaskDataSource  dataSource;

    @BindView(R.id.iv_profile_pic)
    SimpleDraweeView profilePic;

    @BindView(R.id.tv_coin_balance)
    TextView coinBalance;

    @BindView(R.id.parentLayout)
    ConstraintLayout parentLayout;

    @BindView(R.id.iv_empty_item)
    ImageView ivEmpty;

    @BindView(R.id.tv_empty_item)
    TextView tvEmpty;

    @BindView(R.id.tv_empty_item1)
    TextView tvEmpty1;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.tutorial_overlay_view)
    FrameLayout flTutorialView;

    @BindView(R.id.tvTitle)
    TextView title;

    @BindView(R.id.rvMatchMaker)
    RecyclerView recyclerView;

    private Unbinder unbinder;
    private int INDEX =0,OFFSET=0,LIMIT=2,MAX_INDEX=0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_make_match, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder= ButterKnife.bind(this,view);
        presenter.takeView(this);
//        presenter.loadSavedLocations(OFFSET,LIMIT);
        presenter.getUserLocation(OFFSET,LIMIT);
        MAX_INDEX=LIMIT;
        initUI();
        hideAll();
        initRv();
    }

    private void initRv() {
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity,RecyclerView.VERTICAL,false));
        matchMakerAdapter.setCallback(presenter);

        recyclerView.addItemDecoration(new DividerItemDecoration(activity, LinearLayoutManager.VERTICAL));

        // Create and add a callback
        ItemTouchHelper.SimpleCallback callback = new ItemTouchHelper.SimpleCallback(0,  ItemTouchHelper.LEFT| ItemTouchHelper.RIGHT)
        {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
//                ((PairMakerViewHolder)target).item_swipe_left_indicator.setVisibility(View.VISIBLE);
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                final int position = viewHolder.getAdapterPosition();
                if(direction==ItemTouchHelper.LEFT) {
                    Log.e(TAG, "onSwiped: left ");
                    presenter.leftSwipe(position);
                }
                if(direction==ItemTouchHelper.RIGHT){
                    Log.e(TAG, "onSwiped: right " );
                    presenter.rightSwipe(position);}
//                ((PairMakerViewHolder)viewHolder).item_swipe_left_indicator.setVisibility(View.GONE);
            }

            @Override
            public boolean isItemViewSwipeEnabled() {
                return true;
            }


            @Override
            public void onChildDraw (Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX , float dY, int actionState, boolean isCurrentlyActive){
                final int position = viewHolder.getAdapterPosition();
                PairMakerViewHolder holder  = (PairMakerViewHolder) viewHolder;
                if ( dX <-2) // Swiping Left
                {
                    Log.e(TAG, "onChildDraw: left " );
//                    arrayList.get(position).setStatus(-1);
//                    matchMakerAdapter.notifyItemChanged(position);
                    holder.item_swipe_left_indicator.setVisibility(View.GONE);
                    holder.item_swipe_right_indicator.setVisibility(View.VISIBLE);
                }
                else if ( dX > 2) // Swiping Right
                { Log.e(TAG, "onChildDraw: right " );
//                    arrayList.get(position).setStatus(1);
//                    matchMakerAdapter.notifyItemChanged(position);
                    holder.item_swipe_left_indicator.setVisibility(View.VISIBLE);
                    holder.item_swipe_right_indicator.setVisibility(View.GONE);

                } else{
//                    arrayList.get(position).setStatus(0);
//                    matchMakerAdapter.notifyItemChanged(position);
                    holder.item_swipe_left_indicator.setVisibility(View.GONE);
                    holder.item_swipe_right_indicator.setVisibility(View.GONE);
                }

                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        recyclerView.setAdapter(matchMakerAdapter);
    }


    private void hideAll() {
        progressBar.setVisibility(View.VISIBLE);
        ivEmpty.setVisibility(View.GONE);
        tvEmpty1.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        tvEmpty.setText(getString(R.string.loading_text));
    }

    void hideData() {
        recyclerView.setVisibility(View.GONE);
        ivEmpty.setVisibility(View.VISIBLE);
        tvEmpty.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        tvEmpty.setText("Please try a different location.");
        tvEmpty1.setVisibility(View.VISIBLE);
    }

    void showData() {
        recyclerView.setVisibility(View.VISIBLE);
        ivEmpty.setVisibility(View.GONE);
        tvEmpty.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    private void initUI() {
        profilePic.setImageURI(dataSource.getProfilePicture());
        coinBalance.setTypeface(typeFaceManager.getCircularAirLight());
        tvEmpty.setTypeface(typeFaceManager.getCircularAirLight());
        title.setTypeface(typeFaceManager.getCircularAirBold());
        if(dataSource.getSwipeCardTutorialView()){
            flTutorialView.setVisibility(View.VISIBLE);
            flTutorialView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flTutorialView.setVisibility(View.GONE);
                    dataSource.setSwipeCardTutorialView(false);
                }
            });
        }
    }


    @Override
    public void onDestroy() {
        clearFrescoChache();
        presenter.dropView();
        unbinder.unbind();
        super.onDestroy();

    }

    @OnClick(R.id.iv_profile_pic)
    void appSettings(){
        homePresenter.openAppSettingPage();
    }

    private void clearFrescoChache(){
        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        imagePipeline.clearCaches();
    }

    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(parentLayout,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showCoinBalance(String Balance)
    {
        if(coinBalance != null)
            coinBalance.setText(""+Balance);
    }

    @OnClick(R.id.coin_view)
    void onCoin_View() {
        homePresenter.launchCoinWallet();
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @OnClick(R.id.ivLeaderBoard)
    void openLeaderBoad() {
        Intent intent=new Intent(activity, LeaderBoadActivity.class);
        startActivity(intent);
    }

    @Override
    public void notifyRvAdapter(int position) {
        matchMakerAdapter.notifyItemRemoved(position);
        matchMakerAdapter.notifyItemRangeChanged(position, arrayList.size());
    }

}
