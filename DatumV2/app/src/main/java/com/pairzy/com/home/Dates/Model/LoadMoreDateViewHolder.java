package com.pairzy.com.home.Dates.Model;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.util.LoadingViews.DatumLoadingView;
import com.pairzy.com.util.TypeFaceManager;

/**
 * Created by ankit on 16/5/18.
 */

public class LoadMoreDateViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private ImageView heart_img;
    private TextView loading_text,retry_text;
    private ItemActionCallBack itemActionCallBack;
    public DatumLoadingView loading_view;
    private Context context;

    public LoadMoreDateViewHolder(View itemView,ItemActionCallBack callback,TypeFaceManager typeFaceManager)
    {
        super(itemView);
        this.context=itemView.getContext();
        this.itemActionCallBack=callback;
        this.heart_img=itemView.findViewById(R.id.heart_img);
        this.loading_text=itemView.findViewById(R.id.loading_text);
        loading_view=itemView.findViewById(R.id.load_bubble);
        retry_text=itemView.findViewById(R.id.retry_text);
        this.loading_text.setTypeface(typeFaceManager.getCircularAirBold());
        this.retry_text.setTypeface(typeFaceManager.getCircularAirBold());
        itemView.findViewById(R.id.load_more_view).setOnClickListener(this);
    }

    /*
     *Showing loading failed view */
    public void setFailed()
    {
        loading_text.setVisibility(View.GONE);
        loading_view.hide();
        loading_view.setVisibility(View.GONE);
        retry_text.setVisibility(View.VISIBLE);
        try
        {
            Animation animation= AnimationUtils.loadAnimation(context, R.anim.heart_blink_fast);
            heart_img.setAnimation(animation);
            animation.start();
        }catch (Exception e){}
    }

    /*
     *Showing the loading more view */
    public void showLoadingLoadingAgain()
    {
        loading_text.setVisibility(View.VISIBLE);
        loading_view.show();
        loading_view.setVisibility(View.VISIBLE);
        retry_text.setVisibility(View.GONE);
        try
        {
            Animation animation= AnimationUtils.loadAnimation(context, R.anim.heart_blink_fast);
            heart_img.setAnimation(animation);
            animation.start();
        }catch (Exception e){}
    }

    @Override
    public void onClick(View view)
    {
        if(retry_text.getVisibility()==View.VISIBLE)
        {
            if(itemActionCallBack!=null)
                itemActionCallBack.onClick(R.id.retry_text,this.getAdapterPosition());
        }
    }
}
