package com.pairzy.com.util.SpendCoinDialog;

public interface CoinSpendDialogCallback {
    void onOkToSpendCoin(boolean dontShowAgain);
}
