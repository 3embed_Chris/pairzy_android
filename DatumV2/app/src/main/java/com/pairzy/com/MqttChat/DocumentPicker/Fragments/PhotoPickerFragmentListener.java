package com.pairzy.com.MqttChat.DocumentPicker.Fragments;

/**
 * Created by moda on 22/08/17.
 */

public interface PhotoPickerFragmentListener {
    void onItemSelected();
}