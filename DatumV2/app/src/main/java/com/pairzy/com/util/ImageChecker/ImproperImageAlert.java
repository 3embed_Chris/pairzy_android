package com.pairzy.com.util.ImageChecker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
/**
 * <h2>ImproperImageAlert</h2>
 * <P>
 *
 * </P>
 * @since  4/18/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ImproperImageAlert
{
    private Dialog dialog_parent=null;
    private TypeFaceManager typeFaceManager;
    private Activity mactivity;

    public ImproperImageAlert(Activity activity, TypeFaceManager typeFaceManager)
    {
        this.mactivity=activity;
        this.typeFaceManager=typeFaceManager;
    }

    /**
     * <h2>show_Alert_Permission</h2>
     * <P>
     *
     * </P>
     * @param message contains the aleret box message.
     **/
    public void showProfilePicError(String message)
    {
        if(dialog_parent!=null&&dialog_parent.isShowing())
        {
            dialog_parent.dismiss();
            dialog_parent.cancel();
        }
        dialog_parent = new Dialog(mactivity,android.R.style.Theme_Translucent);
        dialog_parent.requestWindowFeature(Window.FEATURE_NO_TITLE);
        @SuppressLint("InflateParams")
        View dialogView = LayoutInflater.from(mactivity).inflate(R.layout.permission_aleret_layout, null);
        TextView heading_text=dialogView.findViewById(R.id.heading_text);
        heading_text.setText(mactivity.getString(R.string.profile_pic_error_text));
        heading_text.setTypeface(typeFaceManager.getCircularAirBold());
        TextView sub_title=dialogView.findViewById(R.id.sub_title);
        sub_title.setTypeface(typeFaceManager.getCircularAirLight());
        sub_title.setText(mactivity.getString(R.string.profile_pic_error_subtitle));
        TextView sub_title_two=dialogView.findViewById(R.id.sub_title_two);
        sub_title_two.setTypeface(typeFaceManager.getCircularAirLight());
        sub_title_two.setText(message);
        Button ok_button=dialogView.findViewById(R.id.ok_button);
        ok_button.setTypeface(typeFaceManager.getCircularAirBook());
        dialog_parent.setContentView(dialogView);
        dialog_parent.setCancelable(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog_parent.getWindow();
        assert window != null;
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        ok_button.setOnClickListener(view ->
        {
            dialog_parent.dismiss();
            dialog_parent.cancel();
        });
        dialog_parent.show();
    }
}
