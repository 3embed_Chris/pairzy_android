package com.pairzy.com.MyProfile.editAge;

/**
 * Created by ankit on 27/4/18.
 */

public interface EditDobContract {

    interface View{

        String getAdultErrorMessage();
        void  onDateSelected(String day, String month, String year);
        void showMessage(String message);
        void onError(String error);
        void finishActivity(double dateInMilli);
    }

    interface Presenter{

        boolean validateInput(String text);
        String formatInput(String input);
        void showMessage(String message);
        void openDatePicker();
        void onError(String message);
        void validateAge(String day, String month, String year);
    }
}
