package com.pairzy.com.MqttChat.ViewHolders;

import android.animation.ObjectAnimator;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.MqttChat.Utilities.RingProgressBar;
import com.pairzy.com.util.TypeFaceManager;


/**
 * Created by moda on 23/08/17.
 */

public class ViewHolderDocumentSent extends RecyclerView.ViewHolder {

    private TypeFaceManager typeFaceManager;

    public TextView time, date, fileName, fileType, fnf, previousMessage_head, previousMessage_content;

    public ImageView singleTick, doubleTickGreen, doubleTickBlue, clock, fileImage, download, cancel,forward,previousMessage_iv;//, blocked;

    public RingProgressBar progressBar;


    public ProgressBar progressBar2;

    public RelativeLayout messageRoot, previousMessage_rl,documentLayout;

    public ViewHolderDocumentSent(View view, TypeFaceManager typeFaceManager) {
        super(view);
        this.typeFaceManager = typeFaceManager;

        forward= (ImageView) view.findViewById(R.id.forward_iv);
        messageRoot=(RelativeLayout) view.findViewById(R.id.message_root);

          /*
         * For message reply feature
         */
        previousMessage_rl = (RelativeLayout) view.findViewById(R.id.initialMessage_rl);
        previousMessage_head = (TextView) view.findViewById(R.id.senderName_tv);
        previousMessage_iv= (ImageView) view.findViewById(R.id.initialMessage_iv);
        previousMessage_content = (TextView) view.findViewById(R.id.message_tv);


        ObjectAnimator animation = ObjectAnimator.ofFloat(forward, "rotationY", 0.0f, 180f);
        animation.setDuration(0);

        animation.start();

        documentLayout = (RelativeLayout) view.findViewById(R.id.rl);
        fileName = (TextView) view.findViewById(R.id.fileName);
        fileType = (TextView) view.findViewById(R.id.fileType);

        fileImage = (ImageView) view.findViewById(R.id.fileImage);
        date = (TextView) view.findViewById(R.id.date);

        time = (TextView) view.findViewById(R.id.ts);

        singleTick = (ImageView) view.findViewById(R.id.single_tick_green);

        doubleTickGreen = (ImageView) view.findViewById(R.id.double_tick_green);

        doubleTickBlue = (ImageView) view.findViewById(R.id.double_tick_blue);

        clock = (ImageView) view.findViewById(R.id.clock);


        fnf = (TextView) view.findViewById(R.id.fnf);


        progressBar2 = (ProgressBar) view.findViewById(R.id.progress2);
        progressBar = (RingProgressBar) view.findViewById(R.id.progress);
        download = (ImageView) view.findViewById(R.id.download);


        cancel = (ImageView) view.findViewById(R.id.cancel);


        //Typeface tf = AppController.getInstance().getRobotoCondensedFont();
        time.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.ITALIC);
        fileName.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.NORMAL);
        fileType.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.NORMAL);
        date.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.ITALIC);
        fnf.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.NORMAL);
        if (previousMessage_head != null) {
            previousMessage_head.setTypeface(typeFaceManager.getCircularAirBold());
            previousMessage_content.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.NORMAL);
        }
    }
}