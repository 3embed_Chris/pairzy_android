package com.pairzy.com.home.HomeModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBoostData {
@SerializedName("start")
@Expose
private long start = 0;
@SerializedName("expiryTime")
@Expose
private long expiryTime = 0;

public long getStart() {
    return start;
}

public void setStart(long start) {
    this.start = start;
}

public long getExpiryTime() {
    return expiryTime;
}

public void setExpiryTime(long expiryTime) {
    this.expiryTime = expiryTime;
}

}