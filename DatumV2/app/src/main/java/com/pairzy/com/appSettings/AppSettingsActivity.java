package com.pairzy.com.appSettings;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.pairzy.com.R;
import com.pairzy.com.home.AppSetting.AppSettingFrag;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatActivity;

public class AppSettingsActivity extends DaggerAppCompatActivity implements AppSettingsContract.View{

    private Unbinder unbinder;
    @BindView(R.id.fragment_container)
    FrameLayout fragContainer;

    @Inject
    AppSettingFrag appSettingFrag;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_settings);
        unbinder = ButterKnife.bind(this);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container,appSettingFrag,"AppSettingsTag")
                .commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar.make(fragContainer,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(this,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this,R.color.white));
        snackbar.show();
    }
    @Override
    public void showMessage(String message) {
        Snackbar snackbar = Snackbar.make(fragContainer,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(this,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this,R.color.white));
        snackbar.show();
    }
}
