package com.pairzy.com.home.Matches.PostFeed.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*
 * <h>PostListDataPojo</h>
 * <p> this is pojo class</P>
 * */
public class PostListPojo {

    @SerializedName("liked")
    @Expose
    private Boolean liked;

    public Boolean getLiked() {
        return liked;
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    @SerializedName("profilePic")
    @Expose
    private String profilePic;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("likeCount")
    @Expose
    private String likeCount;

    @SerializedName("postId")
    @Expose
    private String postId;

    @SerializedName("userName")
    @Expose
    private String userName;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("userId")
    @Expose
    private String userId;

    @SerializedName("createdOn")
    @Expose
    private String createdOn;

    @SerializedName("url")
    @Expose
    private ArrayList<String> url = null;

    @SerializedName("typeFlag")
    @Expose
    private String typeFlag;


    @SerializedName("isPairSuccess")
    @Expose
    private boolean isPairSuccess;

    @SerializedName("commentCount")
    @Expose
    private Integer commentCount;

    @SerializedName("Likers")
    @Expose
    private String[] Likers;

    @SerializedName("postedOn")
    @Expose
    private String postedOn;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("distance")
    @Expose
    private String distance;

    public PostListPojo(PostListPojo dataPojo) {
        this.date = dataPojo.date;
        this.description = dataPojo.description;
        this.likeCount = dataPojo.likeCount;
        this.postId = dataPojo.postId;
        this.userName = dataPojo.userName;
        this.type = dataPojo.type;
        this.userId = dataPojo.userId;
        this.createdOn = dataPojo.createdOn;
        this.url = dataPojo.url;
        this.typeFlag = dataPojo.typeFlag;
        this.commentCount = dataPojo.commentCount;
        Likers = dataPojo.Likers;
        this.postedOn = dataPojo.postedOn;
        this.status = dataPojo.status;
        this.distance=dataPojo.distance;
        this.isPairSuccess=dataPojo.isPairSuccess;
    }

    public boolean isPairSuccess() {
        return isPairSuccess;
    }

    public void setPairSuccess(boolean pairSuccess) {
        isPairSuccess = pairSuccess;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public ArrayList<String> getUrl() {
        return url;
    }

    public void setUrl(ArrayList<String> url) {
        this.url = url;
    }

    public String getTypeFlag() {
        return typeFlag;
    }

    public void setTypeFlag(String typeFlag) {
        this.typeFlag = typeFlag;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public String[] getLikers() {
        return Likers;
    }

    public void setLikers(String[] likers) {
        Likers = likers;
    }

    public String getPostedOn() {
        return postedOn;
    }

    public void setPostedOn(String postedOn) {
        this.postedOn = postedOn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

