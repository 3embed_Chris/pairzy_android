package com.pairzy.com.moments.model;

/**
 * <h2>MomentPostClickCallBack</h2>
 * <P> this is interface for call back on click FOR THE adapter</P>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */
public interface MomentPostClickCallBack {
    /* <h2> onLikePost</h2>
     * <p>this is method for liking the post </p>
     */
    void onLikePost(int position);

    /* <h2> onUnlikePost</h2>
     * <p>this is method for unliking the post </p>
     */
    void onUnlikePost(int position);

    /* <h2> onComment</h2>
     * <p>this is method for onComment the post </p>
     */
    void onComment(int position);
    /* <h2> onViewAllComment</h2>
     * <p>this is method for view All Comment the post </p>
     */
    void onViewAllComment(int position);

    /* <h2> onViewAllLikes</h2>
     * <p>this is method for view All Likes the post </p>
     */
    void onViewAllLikes(int position);

    /* <h2> onChatMessage</h2>
     * <p>this is method for chat Message on  post </p>
     */
    void onChatMessage(int position);

    /**
     *
     * @param position index of post in a Post List.
     */
    void onDeletePost(int position);

}
