package com.pairzy.com.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 24/7/18.
 */

//used to fetch error message
public class ErrorMessageResponse {

    @SerializedName("message")
    @Expose
    String message;

    public String getMessage() {
        return message;
    }
}
