package com.pairzy.com.home.Prospects.LikesMe.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 * @since  3/27/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class LikesMeResponseHolder
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<LikesMeItemPojo> data = null;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public ArrayList<LikesMeItemPojo> getData()
    {
        return data;
    }

    public void setData(ArrayList<LikesMeItemPojo> data)
    {
        this.data = data;
    }
}
