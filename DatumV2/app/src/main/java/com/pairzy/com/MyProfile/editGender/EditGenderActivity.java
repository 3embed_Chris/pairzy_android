package com.pairzy.com.MyProfile.editGender;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.MyProfile.MyProfilePagePresenter;
import com.pairzy.com.R;
import com.pairzy.com.register.AnimatorHandler;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.localization.activity.BaseDaggerActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by ankit on 27/4/18.
 */

public class EditGenderActivity extends BaseDaggerActivity implements EditGenderContract.View{

    @Inject
    EditGenderPresenter presenter;

    @Inject
    AnimatorHandler animatorHandler;

    @Inject
    Utility utility;

    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    Activity activity;

    @BindView(R.id.close_button)
    RelativeLayout close_button;
    @BindView(R.id.skip_page)
    TextView skip_page;
    @BindView(R.id.first_title)
    TextView first_title;
    @BindView(R.id.second_title)
    TextView second_title;

    @BindView(R.id.btnNext)
    RelativeLayout btnNext;

    @BindView(R.id.male_selection)
    RelativeLayout male_selection;
    @BindView(R.id.male_text)
    TextView male_text;
    @BindView(R.id.male_tick)
    ImageView male_tick;
    @BindView(R.id.female_selection)
    RelativeLayout female_selection;
    @BindView(R.id.female_text)
    TextView female_text;
    @BindView(R.id.female_tick)
    ImageView female_tick;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_gender);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        btnNext.setEnabled(false);
        first_title.setTypeface(typeFaceManager.getCircularAirBold());
        second_title.setTypeface(typeFaceManager.getCircularAirBold());
        male_text.setTypeface(typeFaceManager.getCircularAirBook());
        female_text.setTypeface(typeFaceManager.getCircularAirBook());
        initData(getIntent());
    }

    private void initData(Intent intent) {
        String genderStr = intent.getStringExtra(MyProfilePagePresenter.GENDER_DATA);
        if(genderStr.equalsIgnoreCase("female")){
            scaleUp(female_text,female_tick);
        }
        else{
            setInitialSelection();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        utility.closeSpotInputKey(activity,male_text);
        //setInitialSelection();
    }

    /*
     * Setting the initial selection*/
    private void setInitialSelection()
    {
        utility.closeSpotInputKey(activity,first_title);
        scaleUp(male_text,male_tick);
    }

    private void scaleUp(TextView textView,ImageView imageView)
    {
        textView.setTextColor(ContextCompat.getColor(activity,R.color.black));
        imageView.setVisibility(View.VISIBLE);
        Animation animation=animatorHandler.getItemScaleUp();
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                handelNextButton(true);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        imageView.setAnimation(animation);
        animation.start();
    }

    /*
  *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            btnNext.setEnabled(true);
        }
        else {
            btnNext.setEnabled(false);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


    @OnClick(R.id.male_selection)
    void onMaleSelection()
    {
        if(male_tick.getVisibility()!=View.VISIBLE)
        {
            switchScale(female_text,female_tick,male_text,male_tick);
        }
    }

    @OnClick(R.id.female_selection)
    void onFemaleSelection()
    {
        if(female_tick.getVisibility()!=View.VISIBLE)
        {
            switchScale(male_text,male_tick,female_text,female_tick);
        }
    }

    /*
   * Switching the animation*/
    private void switchScale(TextView fist,ImageView firstImg,TextView second,ImageView secImg)
    {
        fist.setTextColor(ContextCompat.getColor(activity,R.color.softLightGray));
        Animation animation=animatorHandler.getItemScaleDown();
        firstImg.setVisibility(View.INVISIBLE);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                scaleUp(second,secImg);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        firstImg.setAnimation(animation);
        animation.start();
    }

    @OnClick(R.id.parent_layout)
    void onParentClick(){}

    @OnClick(R.id.close_button)
    void onClose()
    {
        onBackPressed();
    }

    @OnClick(R.id.skip_page)
    void onSkipClicked()
    {
        onBackPressed();
    }

    @OnClick(R.id.btnNext)
    void onNext()
    {
        int gender=1;
        if(female_tick.getVisibility()==View.VISIBLE)
        {
            gender=2;;
        }
        Intent intent = new Intent();
        intent.putExtra(MyProfilePagePresenter.GENDER_DATA,gender);
        setResult(RESULT_OK,intent);
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        this.finish();
        activity.overridePendingTransition(R.anim.slide_from_left,R.anim.slide_to_right);
        super.onBackPressed();
    }
}
