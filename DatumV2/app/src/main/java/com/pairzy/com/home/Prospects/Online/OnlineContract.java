package com.pairzy.com.home.Prospects.Online;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.home.Prospects.Online.Model.OnlineAdpCallBack;

/**
 * @since  3/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface OnlineContract
{
    interface View extends BaseView
    {
        void addAdapterCallback(OnlineAdpCallBack callBack);
        void showError(int id);
        void showError(String message);
        void showMessage(int message);
        void onDataUpdate();
        void showProgress();
        void onApiError(String message);
        void emptyData();
        void openUserProfile(String data);
    }

    interface Presenter extends BasePresenter<View>
    {
        void tryAgain();
        void  getListData(boolean isLoadMore);
        void pee_fetchProfile(int position);
        boolean canDoLoadMore(int position);
        void setAdapterCallabck();
    }
}



