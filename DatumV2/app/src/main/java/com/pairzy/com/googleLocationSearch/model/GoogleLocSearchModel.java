package com.pairzy.com.googleLocationSearch.model;
import com.pairzy.com.BuildConfig;
import com.pairzy.com.data.model.fourSq.Location;
import com.pairzy.com.data.model.fourSq.Venue;
import com.pairzy.com.googleLocationSearch.model.fourSqSearch.FourSearchResponse;
import com.pairzy.com.googleLocationSearch.model.fourSqSearch.Response;
import com.pairzy.com.googleLocationSearch.model.googleSearch.GoogleSearchResponse;
import com.pairzy.com.googleLocationSearch.model.googleSearch.Prediction;
import com.pairzy.com.googleLocationSearch.model.placeDetail.PlaceDetailResponse;
import com.pairzy.com.googleLocationSearch.model.placeDetail.PlaceLocation;
import com.pairzy.com.locationScreen.model.LocationHolder;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.Utility;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * <h>GoogleLocSearchModel class</h>
 * @author 3Embed.
 * @since 9/5/18.
 * @version 1.0.
 */

public class GoogleLocSearchModel {

    @Named("ADDRESS_LIST_POJO")
    @Inject
    ArrayList<AddressListPojo> addressList;
    @Named("PREF_LIST")
    @Inject
    ArrayList<String> preferanceList;
    @Inject
    LocationAdapter locationAdapter;
    @Inject
    Utility utility;

    private int PROXIMITY_RADIUS = 500000;

    @Inject
    GoogleLocSearchModel(){
    }

    public void filterResponse(FourSearchResponse fourResponse) {

        addressList.clear();
       Response response = fourResponse.getResponse();
           ArrayList<Venue> venueList = response.getVenues();
           for(Venue venue: venueList){
               if(venue != null) {
                   AddressListPojo addressPojo = new AddressListPojo();
                   addressPojo.setAddress_title(venue.getName());
                   Location location = venue.getLocation();
                   if(location != null){
                       addressPojo.setSub_Address(location.getAddress());
                       addressPojo.setLatitude(location.getLat());
                       addressPojo.setLogitude(location.getLng());
                       addressList.add(addressPojo);
                   }
               }
           }
       locationAdapter.notifyDataSetChanged();
    }

    public int getResonseSize(FourSearchResponse fourResponse) {
        int total = 0;
        Response response = fourResponse.getResponse();
        if(response != null) {
            ArrayList<Venue> venueList = response.getVenues();
            if(venueList != null)
                total += venueList.size();
        }
        return total;
    }

    public void parseGoogleResponse(String response) {
        addressList.clear();
        preferanceList.clear();
        GoogleSearchResponse googleSearchResponse = utility.getGson().fromJson(response,GoogleSearchResponse.class);
        try {
            if (googleSearchResponse != null) {
                List<Prediction> predictions = googleSearchResponse.getPredictions();
                if (predictions != null) {
                    for (Prediction prediction : predictions) {
                        String header_name[] = prediction.getDescription().split(",");
                        AddressListPojo temp_item = new AddressListPojo();
                        temp_item.setAddress_title(header_name[0]);
                        temp_item.setSub_Address(prediction.getDescription());
                        addressList.add(temp_item);
                        preferanceList.add(prediction.getReference());
                    }
                }
            }
            locationAdapter.notifyDataSetChanged();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public LocationHolder parsePlaceDetailResponse(String response) {
        LocationHolder holder = new LocationHolder();
        try {
            if (response != null) {
                PlaceDetailResponse placeDetailResponse = utility.getGson().fromJson(response,PlaceDetailResponse.class);
                if(placeDetailResponse != null){
                    PlaceLocation placeLocation =  placeDetailResponse.getResult().getGeometry().getLocation();
                    holder.setLatitude(placeLocation.getLat());
                    holder.setLongitude(placeLocation.getLng());
                    return holder;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return holder;
    }

    public void clearAddressList() {
        addressList.clear();
        locationAdapter.notifyDataSetChanged();
    }

    public int getListSize() {
        return addressList.size();
    }

    public String getPrefText(int position) {
        if(preferanceList.size() > position)
            return preferanceList.get(position);
        return "";
    }

    /**
     * Api helper methods.
     */
    public String getUrl_forsquare(double latitude, double longitude) {
        StringBuilder googlePlacesUrl = new StringBuilder("https://api.foursquare.com/v2/venues/search?");
        googlePlacesUrl.append("client_id=");
        googlePlacesUrl.append(AppConfig.Foursquare.CLIENT_ID);
        googlePlacesUrl.append("&client_secret=");
        googlePlacesUrl.append(AppConfig.Foursquare.CLIENT_SECRET);
        googlePlacesUrl.append("&v=");
        googlePlacesUrl.append("20170202");
        googlePlacesUrl.append("&ll=");
        googlePlacesUrl.append(latitude);
        googlePlacesUrl.append(",");
        googlePlacesUrl.append(longitude);
        googlePlacesUrl.append("&sortByDistance=");
        googlePlacesUrl.append("1");
        googlePlacesUrl.append("&radius=");
        googlePlacesUrl.append(PROXIMITY_RADIUS);
        googlePlacesUrl.append("&limit=");
        googlePlacesUrl.append(30);
        googlePlacesUrl.append("&locale=");
        googlePlacesUrl.append("en");
        return new String(googlePlacesUrl);
    }

    /**
     * <h2>getUrl_Google_search</h2>
     * <p>
     * Google location search data.
     * </P>
     */
    public String getUrl_Google_search(String search_Data, double currentLatitude, double currentLongitude) {
        String key = "key=" + BuildConfig.GOOGLE_API_KEY;

        String input = "";
        try {
            input = "input=" + URLEncoder.encode(search_Data, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        String types = "establishment|geocode&location=" + currentLatitude + "," + currentLongitude + "&radius=500000po&language=en";
        String parameters = input + "&" + types + "&limit=" + 50 + "&" + key;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;
        return url;
    }


    /**
     * Creating the request list.
     */
    public String getPlaceDetailsUrl(String ref) {
        //TODO we may do not need this method. need to remove.
        String key = "key=" +BuildConfig.GOOGLE_API_KEY;
        // reference of place
        String reference = "reference=" + ref;
        // Sensor enabled
        String sensor = "sensor=false";
        // Building the parameters to the web service
        String parameters = reference + "&" + sensor + "&" + key;
        // Output format
        String output = "json";
        // Building the url to the web service
        return "https://maps.googleapis.com/maps/api/place/details/" + output + "?" + parameters;
    }

    public AddressListPojo getAddressPojo(int position) {
        if(getListSize() > position)
            return addressList.get(position);
        return null;
    }

}
