package com.pairzy.com.util.FileUtil;

/**
 * <h2>Config</h2>
 * <P>
 *
 * </P>
 * @since  2/20/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface Config
{
    String APP_FOLDER="Datum";
    String VIDEOS_DIR ="/Videos/";
    String IMAGE_DIR="/Images/";

    //chat folder
    String CHAT_DOODLES_FOLDER = "/"+APP_FOLDER+"/chat/doodles";
    String CHAT_RECEIVED_THUMBNAILS_FOLDER = "/"+APP_FOLDER+"/chat/receivedThumbnails";
    String CHAT_UPLOAD_THUMBNAILS_FOLDER = "/"+APP_FOLDER+"/chat/upload";
    String CHAT_DOWNLOADS_FOLDER = "/"+APP_FOLDER+"/chat/";
    String IMAGE_CAPTURE_URI = "/"+APP_FOLDER+"/chat";

}
