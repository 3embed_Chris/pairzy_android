package com.pairzy.com.home.Matches.PostFeed.Model;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;

import java.util.ArrayList;

public class MediaRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<PostListPojo> mediaObjects;
    private RequestManager requestManager;

    public MediaRecyclerAdapter(ArrayList<PostListPojo> mediaObjects, RequestManager requestManager) {
        this.mediaObjects = mediaObjects;
        this.requestManager = requestManager;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       // return new PlayerViewHolder(LayoutInflater.from(parent.getContext())
        //.inflate(R.layout.layout_media_list_item, parent, false));
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        //((PlayerViewHolder)holder).onBind(mediaObjects.get(position),requestManager);
    }

    @Override
    public int getItemCount() {
        return mediaObjects.size();
    }
}
