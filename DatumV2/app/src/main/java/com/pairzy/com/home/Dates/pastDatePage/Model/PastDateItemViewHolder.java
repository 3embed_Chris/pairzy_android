package com.pairzy.com.home.Dates.pastDatePage.Model;

import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.util.TypeFaceManager;
import com.facebook.drawee.view.SimpleDraweeView;

/**
 * <h2>PastDateItemViewHolder</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class PastDateItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public SimpleDraweeView simpleDraweeView;
    public TextView user_name;
    public TextView date_name;
    public TextView time_view;
    public TextView tvRateTheDate;
    public AppCompatRatingBar ratingBar;
    private ItemActionCallBack callBack;
    public RelativeLayout rlRateTheDate;
    public FrameLayout flRating;
    private LinearLayout llRoot;

    public PastDateItemViewHolder(View itemView,ItemActionCallBack callBack, TypeFaceManager typeFaceManager)
    {
        super(itemView);
        this.callBack = callBack;
        simpleDraweeView=itemView.findViewById(R.id.user_profile_pic);
        user_name=itemView.findViewById(R.id.user_name);
        user_name.setTypeface(typeFaceManager.getCircularAirBold());
        date_name=itemView.findViewById(R.id.date_type_text);
        date_name.setTypeface(typeFaceManager.getCircularAirBook());
        time_view=itemView.findViewById(R.id.time_view);
        time_view.setTypeface(typeFaceManager.getCircularAirBook());
        tvRateTheDate = itemView.findViewById(R.id.rate_the_date_text);
        tvRateTheDate.setTypeface(typeFaceManager.getCircularAirBook());
        ratingBar = itemView.findViewById(R.id.rating_bar);
        rlRateTheDate = itemView.findViewById(R.id.rate_the_date_rl);
        rlRateTheDate.setOnClickListener(this);
        flRating = itemView.findViewById(R.id.fl_rating);
        itemView.setOnClickListener(this);
        simpleDraweeView.setOnClickListener(this);
        user_name.setOnClickListener(this);
        date_name.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rate_the_date_rl:
            if (callBack != null)
                callBack.onClick(v.getId(), this.getAdapterPosition());
            break;
            default:
                if (callBack != null)
                    callBack.onClick(R.id.root, this.getAdapterPosition());

        }
    }
}
