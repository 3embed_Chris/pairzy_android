package com.pairzy.com.MqttManager;

/**
 * @since  3/9/2018.
 * @version 1.0.
 */
public enum MqttTopics
{
    LIKE_COUNT("boost"),
    LIKE_REFRESH("newLikes"),
    PRO_USER("proUser"),
    DELETE_USER("deleteUser"),
    BLOCKED_USER("blockedUser"),
    ONLINE_STATUS("OnlineStatus"),
    GET_USERS("searchResult"),
    MATCH("match/user"),
    UN_MATCH("unMatch");
    public String value;
    MqttTopics(String value)
    {
        this.value = value;
    }
}
