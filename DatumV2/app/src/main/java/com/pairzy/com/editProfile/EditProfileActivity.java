package com.pairzy.com.editProfile;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.editProfile.Model.ProfilePhotoAdapter;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.GridSpacingItemDecoration;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.facebook.drawee.view.SimpleDraweeView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h>EditProfile activity</h>
 * <p> This EditProfile activity handle editing of user Video and Photos.</p>
 *
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public class EditProfileActivity extends BaseDaggerActivity implements EditProfileContract.View{

    @Inject
    EditProfilePresenter presenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    Utility utility;
    @Inject
    ProfilePhotoAdapter adapter;
    @Inject
    GridLayoutManager gridLayoutManager;
    @Inject
    GridSpacingItemDecoration decoration;
    @Inject
    App_permission app_permission;
    @Inject
    Activity activity;

    @BindView(R.id.first_title)
    TextView tvFirstTitle;
    @BindView(R.id.second_title)
    TextView tvSecondTitle;
    @BindView(R.id.tv_video_title)
    TextView tvVideoTitle;
    @BindView(R.id.btn_add_video)
    Button btnAddVideo;
    @BindView(R.id.card_view)
    CardView cvVideo;
    @BindView(R.id.card_view_loading)
    CardView cvLoading;
    @BindView(R.id.iv_profile_pic)
    ImageView ivVideoPreview;
    @BindView(R.id.tv_photo_title)
    TextView tvPhotoTitle;
    @BindView(R.id.recycler_view_photo)
    RecyclerView recyclerViewPhoto;
    @BindView(R.id.ll_video_title)
    LinearLayout llVideoTitle;
    @BindView(R.id.rl_close)
    RelativeLayout rlClose;
    @BindView(R.id.sdv_profile_pic)
    SimpleDraweeView sdvVideoPreview;
    @BindView(R.id.tv_item_title)
    TextView tvVideoItemTitle;
    @BindView(R.id.tick_mark)
    RelativeLayout rlTickMark;
    @BindView(R.id.parent_layout)
    CoordinatorLayout parentLayout;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        presenter.init();
        adapter.setAdapterClickCallback(presenter);
        presenter.loadUserVideo();
        presenter.loadUserPicture();
    }

    @Override
    public void makeVideoThumbSquare()
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        ivVideoPreview.getLayoutParams().width = (width-70) / 3;
        sdvVideoPreview.getLayoutParams().width = (width-70) / 3;
    }

    @Override
    public void recyclerViewSetup() {
        recyclerViewPhoto.addItemDecoration(decoration);
        recyclerViewPhoto.setLayoutManager(gridLayoutManager);
        recyclerViewPhoto.setHasFixedSize(false);
        recyclerViewPhoto.setAdapter(adapter);
    }


    @Override
    public void applyFont() {
        tvFirstTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvSecondTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvVideoTitle.setTypeface(typeFaceManager.getCircularAirBold());
        btnAddVideo.setTypeface(typeFaceManager.getCircularAirBook());
        tvPhotoTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvVideoItemTitle.setTypeface(typeFaceManager.getCircularAirBook());
    }

    @Override
    public void showAddVideoButton(boolean show) {
        if(show){
            cvLoading.setVisibility(View.GONE);
            cvVideo.setVisibility(View.GONE);
            btnAddVideo.setVisibility(View.VISIBLE);
        }
        else{
            //show video layer.
            cvLoading.setVisibility(View.GONE);
            cvVideo.setVisibility(View.VISIBLE);
            btnAddVideo.setVisibility(View.GONE);
        }

    }

    @Override
    public void openCamera(Uri uri,int mediaType) {
        Intent intent;
        if(mediaType == 0)
            intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        else
            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
        startActivityForResult(intent, AppConfig.CAMERA_CODE);
    }

    @Override
    public void openGallery(int mediaType) {
        String chooseTitle = "";
        Intent intent = new Intent();
        if(mediaType == 0) {
            intent.setType("video/*");
            chooseTitle = "Select Video";
        }
        else {
            intent.setType("image/*");
            chooseTitle = "select Image";
        }
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, chooseTitle),AppConfig.GALLERY_CODE);
    }

    @OnClick(R.id.rl_close)
    public void removeVideo(){
        ivVideoPreview.setImageBitmap(null);
        sdvVideoPreview.setImageURI(Uri.EMPTY);
        presenter.removeVideo();
        showAddVideoButton(true);
        showTickMark(true);
    }

    @OnClick({R.id.iv_profile_pic,R.id.sdv_profile_pic} )
    public void videoPreview(){
        presenter.loadVideoPreview(cvVideo);
    }

    @OnClick({R.id.card_view_loading} )
    public void videoLoading(){
    }

    @OnClick({R.id.btn_add_video})
    public void addVideo(){
        presenter.launchVideoChooser();
    }

    @OnClick(R.id.close_button)
    public void back() {
        setResult(RESULT_CANCELED);
        onBackPressed();
    }

    @OnClick(R.id.tick_mark)
    public void tickMark()
    {
        presenter.saveEditedData();
        //need to fix this method
        if(presenter.isProfileChanged()) {
            setResult(RESULT_OK);
            Log.w("isProfileChanged: ", "true");
        }
        if(rlTickMark.getVisibility() == View.VISIBLE){
            setResult(RESULT_OK);
        }
        onBackPressed();
    }


    @Override
    public void onBackPressed() {
        this.finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode==AppConfig.CAMERA_CODE&&resultCode== Activity.RESULT_OK)
        {
            if(presenter.isValidMediaSize()) {
                presenter.upDateToGallery();
                presenter.compressedMedia(presenter.getRecentTempVideo(), ivVideoPreview);
            }

        }else if(requestCode==AppConfig.GALLERY_CODE&&resultCode==Activity.RESULT_OK)
        {
            if(data!=null&&data.getData() != null)
            {
                Uri uri = data.getData();
                String file_path=utility.getRealPathFromURI(uri);
                if(!TextUtils.isEmpty(file_path))
                {
                    if(presenter.isValidMediaSize(file_path)) {
                        presenter.compressedMedia(file_path, ivVideoPreview);
                    }
                }else
                {
                    showError("file_path_error");
                }
            }else
            {
                showError("file_path_error");
            }
        }else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(!app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults))
        {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void showError(String errorMsg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+errorMsg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(String msg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+msg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(int msgId) {
        String message=getString(msgId);
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }


    @Override
    public void showVideoLoading(boolean show) {
        if(cvVideo != null) {
            cvVideo.setVisibility(View.VISIBLE);
            if (show) {
                cvLoading.setVisibility(View.VISIBLE);
            } else {
                cvLoading.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void showVideoThumb(String videoUrl) {
        sdvVideoPreview.setImageURI(Uri.parse(videoUrl));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.w(EditProfileActivity.class.getName(),"onDestroy() is called!!" );
        unbinder.unbind();
    }

    @Override
    public void showTickMark(boolean show) {
        if(rlTickMark != null) {
            if (show) {
                rlTickMark.setVisibility(View.VISIBLE);
            } else {
                rlTickMark.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void launchPreviewScreen(Intent intent, View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, view, activity.getString(R.string.profile_transition));
            ActivityCompat.startActivity(activity,intent,options.toBundle());
        }
        else{
            startActivity(intent);
        }
    }

}
