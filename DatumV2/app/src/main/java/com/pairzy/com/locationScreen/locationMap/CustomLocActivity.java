package com.pairzy.com.locationScreen.locationMap;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.pairzy.com.BuildConfig;
import com.pairzy.com.R;
import com.pairzy.com.planDate.SelectedLocationHolder;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.LocationUtil;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;


/**
 * <h>CustomLocActivity class</h>
 * @author 3Embed.
 * @since 8/5/18.
 * @version 1.0.
 */

public class CustomLocActivity extends BaseDaggerActivity implements CustomLocContract.View, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, LocationListener, GoogleApiClient.OnConnectionFailedListener {

    private static final int FINE_LOC_REQ_CODE = 22;
    public static final int CHOOSE_LOC_REQ_CODE = 11;

    @Inject
    CustomLocPresenter presenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    Activity mActivity;
    @Inject
    LocationUtil locationUtil;

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private static String TAG = CustomLocActivity.class.getSimpleName();
    private LatLng mCenterLatLong;
    private String selectedLat = "", selectedLng = "";
    private View mapView;

    protected String mAddressOutput;
    protected String mAreaOutput;
    protected String mCityOutput;
    protected String mStateOutput;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    private App_permission app_permission;
    private String[] permissionsArray;
    private SelectedLocationHolder locationHolder;

    @BindView(R.id.tv_page_title)
    TextView tvPageTitle;
    @BindView(R.id.locality)
    TextView tvLocationText;
    @BindView(R.id.confirm_btn)
    Button btnConfirm;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_location);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        presenter.init();
        initVariables();
        checkLocationSetting();
    }

    private void checkLocationSetting() {
        if (locationUtil.checkPlayServices()) {
            // If this check succeeds, proceed with normal processing.
            // Otherwise, prompt user to get valid Play Services APK.
            if (!locationUtil.isLocationEnabled()) {
                // notify user
                AlertDialog.Builder dialog = new AlertDialog.Builder(mActivity);
                dialog.setMessage("Location not enabled!");
                dialog.setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                });
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub
                    }
                });
                dialog.show();
            }
            buildGoogleApiClient();
        } else {
            Toast.makeText(mActivity, "Location not supported in this device", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.rl_search_loc)
    public void searchLocation(){
        openAutocompleteActivity();
    }

    @OnClick(R.id.confirm_btn)
    public void confirmButton(){
        if(locationHolder != null){
            Intent intent = new Intent();
            intent.putExtra("location_holder",locationHolder);
            setResult(RESULT_OK,intent);
            onBackPressed();
        }
    }


    /**
     * <h>InitVariables</h>
     * <p>
     *     In this method we used to initialize all data member.
     * </p>
     */
    private void initVariables() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        permissionsArray = new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION};
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near own current location.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "OnMapReady");
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            mMap.setMyLocationEnabled(false);
        }
        // Change MyLocationButton position
        if (mapView != null && mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 30, 30);
        }


        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                mCenterLatLong = mMap.getCameraPosition().target;
                mMap.clear();
                try {
                    Location mLocation = new Location("");
                    mLocation.setLatitude(mCenterLatLong.latitude);
                    mLocation.setLongitude(mCenterLatLong.longitude);

                    startIntentService(mLocation);
                    System.out.println(TAG + " " + "map lat=" + mCenterLatLong.latitude + " " + "lng=" + mCenterLatLong.longitude);
                    selectedLat = String.valueOf(mCenterLatLong.latitude);
                    selectedLng = String.valueOf(mCenterLatLong.longitude);
                    String[] params = new String[]{"" +selectedLat, "" + selectedLng};
                    new BackgroundGetAddress().execute(params);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity,permissionsArray,FINE_LOC_REQ_CODE);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(mActivity,permissionsArray,FINE_LOC_REQ_CODE);
        }
        else {
            onConnectWithLoc();
        }
    }

    private void onConnectWithLoc() {
        System.out.println(TAG + " " + "fused location onConnected...");
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            System.out.println(TAG+" "+"location onConnected="+mLastLocation.getLatitude()+" "+"");
            changeMap(mLastLocation);
            Log.d(TAG, "ON connected");

        } else
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient, this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        try {
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        System.out.println(TAG+" "+"fused location onLocationChanged...");

        try {
            if (location != null)
                changeMap(location);
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mGoogleApiClient.connect();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void changeMap(Location location)
    {
        Log.d(TAG, "Reaching map" + mMap);

        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(mActivity,permissionsArray,FINE_LOC_REQ_CODE);
            return;
        }


        // check if map is created successfully or not
        if (mMap != null) {
            mMap.getUiSettings().setZoomControlsEnabled(false);
            LatLng latLong;

            latLong = new LatLng(location.getLatitude(), location.getLongitude());

            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLong).zoom(17f).tilt(0).build();

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            System.out.println(TAG+" "+"map lat change map="+location.getLatitude() +" "+"lng="+location.getLongitude());
            selectedLat=String.valueOf(location.getLatitude());
            selectedLng=String.valueOf(location.getLongitude());
            startIntentService(location);

        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }
    }


    @OnClick(R.id.rL_confirm_location)
    public void onConfirmLocation(){
        Intent intent=new Intent();
        intent.putExtra("lat",selectedLat);
        intent.putExtra("lng",selectedLng);
        intent.putExtra("address",tvLocationText.getText().toString());
        setResult(CHOOSE_LOC_REQ_CODE,intent);
        System.out.println(TAG+" "+"lat="+selectedLat+" "+"lng="+selectedLng+" "+"address="+tvLocationText.getText().toString());
        onBackPressed();
    }

    @OnClick(R.id.rl_back_btn)
    public void back(){
        onBackPressed();
    }

    @Override
    public void applyFont() {
        tvPageTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvLocationText.setTypeface(typeFaceManager.getCircularAirBook());
        btnConfirm.setTypeface(typeFaceManager.getCircularAirBook());
    }


    /**
     * Receiver for data send from FetchAddressIntentService.
     */
    private class AddressResultReceiver extends ResultReceiver
    {
        AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data send from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

//            // Display the address string or an error message send from the intent service.
//            mAddressOutput = resultData.getString(AppUtils.LocationConstants.RESULT_DATA_KEY);
//
//            mAreaOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_AREA);
//
//            mCityOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_CITY);
//            mStateOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_STREET);
//            System.out.println(TAG+" "+"message="+mAddressOutput+" "+"address="+mAreaOutput+" "+"mCityOutput="+mCityOutput+" "+"mStateOutput="+mStateOutput);

            displayAddressOutput();
        }

    }

    /**
     * Updates the address in the UI.
     */
    protected void displayAddressOutput() {
        try {
            if (mAreaOutput != null && !mAddressOutput.isEmpty())
                tvLocationText.setText(mAddressOutput);
            else tvLocationText.setText(mStateOutput);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates an intent, adds location data to it as an extra, and starts the intent service for
     * fetching an address.
     */
    protected void startIntentService(Location mLocation) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        //Intent intent = new Intent(this, FetchAddressIntentService.class);

        //Pass the result receiver as an extra to the service.
        //intent.putExtra(AppUtils.LocationConstants.RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        //intent.putExtra(AppUtils.LocationConstants.LOCATION_DATA_EXTRA, mLocation);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        //startService(intent);
    }


    private void openAutocompleteActivity() {
        try {
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            if (!Places.isInitialized()) {
                Places.initialize(getApplicationContext(), BuildConfig.GOOGLE_API_KEY);
            }

            List<Place.Field> fields = Arrays.asList(Place.Field.ID,Place.Field.NAME,Place.Field.LAT_LNG,Place.Field.ADDRESS);
            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY,fields).build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (Exception e) {
            // Indicates that Google Play Services is either not installed or not up to date. Prompt
            // the user to correct the issue.
//            GoogleApiAvailability.getInstance().getErrorDialog(this, Integer.parseInt(e.getMessage()),
//                    0 /* requestCode */).show();
            e.printStackTrace();
        }
    }

    /**
     * Called after the autocomplete activity has finished to return its result.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check that the result was from the autocomplete widget.
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                // Get the user's selected place from the Intent.
                Place place = Autocomplete.getPlaceFromIntent(data);
                // TODO call location based filter.
                LatLng latLong;
                latLong = place.getLatLng();
                locationHolder = new SelectedLocationHolder();
                locationHolder.setLocationTitle(place.getName().toString());
                locationHolder.setLatitude(latLong.latitude);
                locationHolder.setLongitude(latLong.longitude);
                tvLocationText.setText(place.getName() + "");
                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLong).zoom(17f).tilt(0).build();

                if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    mMap.setMyLocationEnabled(true);
                }

                mMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case FINE_LOC_REQ_CODE:
                System.out.println("grant result=" + grantResults.length);
                boolean granted = true;
                if (grantResults.length > 0) {
                    for (int count = 0; count < grantResults.length; count++) {
                        if (grantResults[count] != PackageManager.PERMISSION_GRANTED)
                            granted = false;
                    }
                    if (granted) {
                        onConnectWithLoc();
                    }
                }
        }
    }

    private class BackgroundGetAddress extends AsyncTask<String, Void, String> {
        List<Address> address;
        String lat, lng;
        Geocoder geocoder;
        @Override
        protected String doInBackground(String... params) {
            try {
                lat = params[0];
                lng = params[1];

                if (lat != null && lng != null) {
                    if (mActivity != null) {
                        geocoder = new Geocoder(mActivity);
                    }
                    if (geocoder != null) {
                        address = geocoder.getFromLocation(Double.parseDouble(params[0]), Double.parseDouble(params[1]), 1);
                    } else {
                        //Error
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (address != null && address.size() > 0) {
                locationHolder  =  new SelectedLocationHolder();
                String formatedAddress = locationUtil.removeNullFromAddress(address.get(0).getAddressLine(0) + ", " + address.get(0).getAddressLine(1));
                locationHolder.setLatitude(Double.parseDouble(lat));
                locationHolder.setLongitude(Double.parseDouble(lng));
                locationHolder.setLocationTitle(formatedAddress);
                tvLocationText.setText(formatedAddress);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        presenter.dropView();
    }
}
