package com.pairzy.com.home.Dates;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Dates.Model.DateModel;
import com.pairzy.com.home.Dates.Model.LoadMorePastDateStatus;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;
import com.pairzy.com.home.HomeModel.LoadMoreStatus;
import com.pairzy.com.home.HomeUtil;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.userProfile.Model.UserDataDetails;
import com.pairzy.com.userProfile.UserProfilePage;
import com.pairzy.com.util.CustomObserver.DateRefreshObserver;
import com.pairzy.com.util.Utility;

import java.io.IOException;
import javax.inject.Inject;
import javax.inject.Named;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * <h2>DatesFragPresenter</h2>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class DatesFragPresenter implements DatesFragContract.Presenter
{
    public static boolean pendingDateListUpdated;
    public static boolean upcomingDateListUpdated;
    @Inject
    NetworkService service;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    DateModel model;
    @Inject
    NetworkStateHolder holder;
    @Inject
    Activity activity;
    @Named(HomeUtil.LOAD_MORE_DATE_STATUS)
    @Inject
    LoadMoreStatus loadMoreStatus;
    @Named(HomeUtil.LOAD_MORE_PAST_DATE_STATUS)
    @Inject
    LoadMorePastDateStatus loadMorePastDateStatus;
    @Inject
    Utility utility;
    /* change this approach to mqtt*/
    @Inject
    DateRefreshObserver observer;

    private DatesFragContract.View view;
    public static final int LIMIT=10;
    public static int PAGE_SIZE=0;
    public static int PAGE_PAST_DATE=0;
    private boolean isRefresh=true;
    private CompositeDisposable compositeDisposable;

    private boolean refreshAfterDate = false;

    @Inject
    DatesFragPresenter(){
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void takeView(DatesFragContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        this.view =null;
        compositeDisposable.clear();
    }


    @Override
    public void initDateRefreshObserver() {
        observer.getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Boolean value) {
                        if(value){
                            fetchList(true,true);
                            fetchPastDateList(true);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void notifyPendingAdapter() {
        if(view != null)
            view.notifyAdapter();
    }

    @Override
    public void refreshDateList() {
        refreshAfterDate = false;
        observer.publishData(true);
    }

    @Override
    public boolean isDateRefreshReq() {
       return refreshAfterDate;
    }

    @Override
    public void fetchList(boolean isPending,boolean isRefresh){
        this.isRefresh=isRefresh;
        if(isRefresh){
            loadMoreStatus.setNo_more_data(false);
            PAGE_SIZE = 0;
            if(view != null){
                view.showLoading(isPending);
            }
        }
        else{
            PAGE_SIZE += LIMIT;
        }
        loadCurrentDate(PAGE_SIZE,PAGE_SIZE+ LIMIT);
    }

    @Override
    public void loadCurrentDate(int offset,int limit) {
        if(holder.isConnected()) {
            dataSource.getUserId();
            service.getCurrentDateList(dataSource.getToken(), model.getLanguage(), offset, limit)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {

                            if (value.code() == 200) {
                                try {
                                    model.parseDateResponse(value.body().string());
                                    if (view != null)
                                        view.notifyAdapter();
                                    updatePendingDateCount(model.getPendingDateListSize());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else {
                                handleErrorForDateApi(activity.getResources().getString(R.string.failed_to_get_date_list));
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            handleErrorForDateApi(activity.getResources().getString(R.string.failed_to_get_date_list));
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
        else{
            handleErrorForDateApi(activity.getResources().getString(R.string.no_internet_error));
        }
    }

    /*
    * handle error for api call
    */
    private void handleErrorForDateApi(String error){

        boolean showErrorToast = true;
        boolean showFailItem = false;
        if(isRefresh || !model.checkPendingDateExist()){
            if(view!=null)
                view.showNetworkError(error,
                        true);
            showErrorToast =false;
        }
        else{
           showFailItem = true;
        }

        if(isRefresh || !model.checkUpcomingDateExist()) {
            if(view!=null)
                view.showNetworkError(error, false);
            showErrorToast =false;
        }
        else{
            showFailItem = true;
        }

        if(showErrorToast) {
            if(view!=null)
                view.showError(error);
        }
        if(showFailItem){
            model.handleFailedLoadMore();
        }
    }

    @Override
    public void fetchPastDateList(boolean isRefresh) {
        if(holder.isConnected()) {
            if (isRefresh) {
                loadMorePastDateStatus.setNo_more_data(false);
                PAGE_PAST_DATE = 0;
                if(view != null){
                    view.showPastDateLoading();
                }
            } else {
                PAGE_PAST_DATE += LIMIT;
            }

            service.getPastDateList(dataSource.getToken(), model.getLanguage(), PAGE_PAST_DATE, PAGE_PAST_DATE + LIMIT)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                try {
                                    model.parsePastDateResponse(value.body().string());
                                    if (view != null) {
                                        view.notifyPastDateAdapter();
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else {
                                handlePastDateError(isRefresh,activity.getResources().getString(R.string.api_server_error));
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            handlePastDateError(isRefresh,activity.getResources().getString(R.string.failed_to_get_date_list));

                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
        else{
            handlePastDateError(isRefresh,activity.getResources().getString(R.string.no_internet_error));
        }
    }

    @Override
    public void addToTheUpcomingList(DateListPojo removedDate) {
        model.addToTheUpcomingList(removedDate);
        if(view != null)
            view.notifyAdapter();
    }

    @Override
    public void addToThePastDate(PastDateListPojo pastDateListPojo) {
        model.addToThePastDate(pastDateListPojo);
        if(view != null)
            view.notifyPastDateAdapter();
    }

    @Override
    public void updatePendingDateCount(int pendingDateCount) {
        if(view != null)
            view.showPendingDateCount(pendingDateCount);
    }



    @Override
    public void launchUserProfile(PastDateListPojo pastDateListPojo) {
        if(view != null) {
            UserDataDetails dataDetails = new UserDataDetails();
            dataDetails.setProfilePic(pastDateListPojo.getOpponentProfilePic());
            dataDetails.setOpponentId(pastDateListPojo.getOpponentId());
            Intent intent=new Intent(activity,UserProfilePage.class);
            Bundle intent_data=new Bundle();
            intent_data.putString(UserProfilePage.USER_DATA,utility.getGson().toJson(dataDetails,UserDataDetails.class));
            intent.putExtras(intent_data);
            view.launchUserProfile(intent);
        }
    }

    @Override
    public void launchUserProfile(DateListPojo dateListPojo) {
        if(view != null) {
            UserDataDetails dataDetails = new UserDataDetails();
            dataDetails.setProfilePic(dateListPojo.getOpponentProfilePic());
            dataDetails.setOpponentId(dateListPojo.getOpponentId());
            Intent intent=new Intent(activity,UserProfilePage.class);
            Bundle intent_data=new Bundle();
            intent_data.putString(UserProfilePage.USER_DATA,utility.getGson().toJson(dataDetails,UserDataDetails.class));
            intent.putExtras(intent_data);
            view.launchUserProfile(intent);
        }
    }


    /*
     * handle error for past date api
     */
    private void handlePastDateError(boolean isRefresh, String error) {
        if(isRefresh || !model.checkPastDateExist()) {
            if(view!=null)
                view.showNetworkError(error);
        }
        else {
            model.handlePastdateFailedLoadMore();
            if(view!=null)
                view.showError(error);
        }
    }


    @Override
    public void preFetchImage(boolean fromPendingList, int position)
    {
        model.prefetchImage(fromPendingList, position);
    }

    @Override
    public void preFetchPastDateListImage(int position) {
        model.prefetchPastDateImage(position);
    }

}
