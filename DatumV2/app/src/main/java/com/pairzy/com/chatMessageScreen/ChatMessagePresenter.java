package com.pairzy.com.chatMessageScreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.pairzy.com.AppController;
import com.pairzy.com.chatMessageScreen.Model.ChatMessageModel;
import com.pairzy.com.MqttManager.Model.MqttMessage;
import com.pairzy.com.MqttManager.MqttRxObserver;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Matches.Chats.Model.ChatListData;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.CustomObserver.CoinBalanceObserver;
import com.pairzy.com.util.CustomObserver.dataChangeObserver.DataChangeType;
import com.pairzy.com.util.CustomObserver.dataChangeObserver.DatumDataChangeObserver;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.SpendCoinDialog.CoinDialogCallBack;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.timerDialog.TimerDialog;

import org.json.JSONException;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.pairzy.com.util.AppConfig.PROFILE_REQUEST;


/**
 * <h>ChatMessagePresenter class</h>
 * @author 3Embed.
 * @since 26/12/18.
 * @version 1.0.
 */

public class ChatMessagePresenter implements ChatMessageContract.Presenter , CoinDialogCallBack {

    @Inject
    ChatMessageContract.View view;

    @Inject
    CoinBalanceObserver coinBalanceObserver;
    @Inject
    ChatMessageModel model;
    @Inject
    Utility utility;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    NetworkService service;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Activity activity;
    @Inject
    CoinDialog spendCoinDialog;
    @Inject
    TimerDialog timerDialog;
    @Inject
    MqttRxObserver mqttRxObserver;
    @Inject
    DatumDataChangeObserver datumDataChangeObserver;

    private CompositeDisposable compositeDisposable;
    private String userId;
    private boolean forSuperlike;

    @Inject
    public ChatMessagePresenter(){
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void initDataChangeObserver(){
        datumDataChangeObserver.getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<DataChangeType>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(DataChangeType dataChangeType) {
                        switch (dataChangeType){
                            case BLOCK_USER:
                            case UN_BLOCK_USER:
                                if(view != null)
                                    view.refreshUserBlocked();
                                break;
                        }
                    }
                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
    @Override
    public void initMatchAndUnmatchUserObserver() {
        mqttRxObserver.getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MqttMessage>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(MqttMessage value) {
                        if(value != null && value.getData() != null){
                            try {
                                if (value.getData().has("messageType") && value.getData().getString("messageType").equals(com.pairzy.com.MqttManager.MessageType.MATCH)) {
                                    try {
                                        ChatListData chatListData = model.parseMatchedMessage(value.getData());
                                        if(chatListData != null){
                                            if(view != null){
                                                view.makeUserMatched(chatListData.getRecipientId());
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                else if ( value.getData().has("messageType") && value.getData().getString("messageType").equals(com.pairzy.com.MqttManager.MessageType.UN_MATCH)){
                                    Log.w("UN_MATCH RECEIVED:", value.getData().toString());
                                    String userId = model.parseUserId(value.getData().toString());
                                    if(!TextUtils.isEmpty(userId)){
                                        if(view != null)
                                            view.unMatchUser(userId);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    @Override
    public void observeCoinBalanceChange()
    {
        coinBalanceObserver.getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Boolean value) {
                        if(value){
                            if (view != null)
                                view.showCoinBalance(utility.formatCoinBalance(model.getCoinBalance()));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {}

                    @Override
                    public void onComplete() {}
                });
    }

    @Override
    public void takeView(ChatMessageContract.View view) {
        //directly injected in case of Activity
    }

    @Override
    public void dropView() {
        this.view = null;
    }

    @Override
    public void updateCoinBalance() {
        if(!TextUtils.isEmpty(model.getCoinBalance())){
            if(view != null)
                view.showCoinBalance(utility.formatCoinBalance(model.getCoinBalance()));
        }
        else{
            loadWalletBalance();
        }
    }


    @Override
    public void callCoinConfigApi()
    {
        if(networkStateHolder.isConnected())
        {
            service.getCoinConfig(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                try {
                                    model.parseCoinConfig(value.body().string());
                                    if (model.getCoinConfigData() == null) {
                                        if(view != null)
                                            view.showError("failed to fetch coinConfig!!");
                                    }
                                } catch (Exception e) {
                                }
                            } else if(value.code() == 401)
                            {
                                AppController.getInstance().appLogout();
                            } else {
                                try {
                                    if(view != null)
                                        view.showError(value.errorBody().string());
                                } catch (Exception e) {
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if(view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void dispose() {
        compositeDisposable.clear();
    }

    @Override
    public void loadWalletBalance()
    {
        if(networkStateHolder.isConnected()) {
            service.getCoinBalance(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                try {
                                    model.parseCoinBalance(value.body().string());
                                    if(model.getCoinConfigData() == null){
                                        //get coin config
                                        callCoinConfigApi();
                                    }
                                    //TODO movein presenter
//                                    if(forSuperlike){
//                                        forSuperlike = false;
//                                        doSuperLike(userId);
//                                    }
                                } catch (Exception e) {
                                }
                            } else if (value.code() == 401) {
                                AppController.getInstance().appLogout();
                            } else {
                                try {
                                    if(view != null)
                                        view.showError(value.errorBody().string());
                                } catch (Exception e) {
                                }
                            }
                            coinBalanceObserver.publishData(true);
                        }

                        @Override
                        public void onError(Throwable e) {
                            if(view != null)
                                view.showError(activity.getString(R.string.failed_to_get_wallet_balance));
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }

    }

    private void launchWalletEmptyDialogForSuperlike(){
        spendCoinDialog.showDialog("",true,"",activity.getString(R.string.empty_wallet_superlike_msg),"",this,true,false);
    }

    @Override
    public void doSuperLike(String user_id)
    {
        if(networkStateHolder.isConnected())
        {
            service.doSupperLike(dataSource.getToken(), AppConfig.DEFAULT_LANGUAGE, model.getUserDetails(user_id))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            try {
                                if (value.code() == 401) {
                                    AppController.getInstance().appLogout();
                                } else if (value.code() == 405) {
                                    if(view != null)
                                        view.showMessage("Already superliked!!");
                                } else if (value.code() != 200 && value.code() != 201) {
                                    if (value.code() == 402) {
                                        try {
//                                            if (view != null)
//                                                view.showError(activity.getString(R.string.insufficient_balance_msg));
                                            /**
                                             * Opening wallet
                                             */
                                            launchWalletEmptyDialogForSuperlike();
                                        } catch (Exception ignored) {
                                        }
                                    }

                                } else {

                                    model.parseSuperLike(value.body().string());
                                    coinBalanceObserver.publishData(true);
                                    if(view != null)
                                        view.startCoinAnimation();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                           if(view != null)
                               view.showError(activity.getString(R.string.server_error));
                        }

                        @Override
                        public void onComplete() {
                        }
                    });

        }else
        {
            if(view != null)
                view.showError(activity.getString(R.string.internet_error_Text));
        }
    }

    private void checkForWalletThenSuperlike(String user_id){
        if(!TextUtils.isEmpty(model.getCoinBalance())) {
            if(model.getCoinBalance().equals("0")){
                //launch empty wallet dialog
                //launchWalletEmptyDialogForSuperlike();
            }
            else {
                doSuperLike(user_id);
            }
        }
        else{
            //
            forSuperlike = true;
            this.userId= user_id;
            loadWalletBalance();
        }
    }

    private void launchLikesTimerDialog() {
        //launchBoostDialog(false,true);
        timerDialog.showDialog(false);
    }


    @Override
    public void doLiked(final String user_id) {
        if (model.getRemainsLikeCount() > 0) {
            if (networkStateHolder.isConnected()) {
                service.doLikeService(dataSource.getToken(), AppConfig.DEFAULT_LANGUAGE, model.getUserDetails(user_id))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Response<ResponseBody>>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                compositeDisposable.add(d);
                            }

                            @Override
                            public void onNext(Response<ResponseBody> value) {
                                try {
                                    if (value.code() == 405) {
                                        if(view != null)
                                            view.showMessage("Already liked!!");
                                    } else if (value.code() == 401) {
                                        AppController.getInstance().appLogout();
                                    } else if (value.code() != 200 && value.code() != 201) {
                                        if (value.code() == 409) {
                                            /**
                                             * Open boost dialog
                                             */
                                            launchLikesTimerDialog();
                                        }
                                    } else {
                                        if (value.code() == 200) {
                                            model.parseLikeResponse(value.body().string());
                                        }
                                    }
                                } catch (Exception e) {
                                    if(view != null)
                                        view.showError(e.getMessage());
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                if(view != null)
                                    view.showError(activity.getString(R.string.server_error));
                            }

                            @Override
                            public void onComplete() {
                            }
                        });
            } else {
                if(view != null)
                    view.showError(activity.getString(R.string.internet_error_Text));
            }
        } else {
            launchLikesTimerDialog();
        }
    }

    @Override
    public void doDislike(String user_id)
    {
        if(networkStateHolder.isConnected())
        {
            service.doUnLikeService(dataSource.getToken(), AppConfig.DEFAULT_LANGUAGE, model.getUserDetails(user_id))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> value)
                        {
                            try
                            {
                                if(value.code() == 405) {
                                    if(view != null)
                                        view.showMessage("Already disliked!!");
                                }
                                else if(value.code() == 200)
                                {

                                } else if(value.code() == 401)
                                {
                                    AppController.getInstance().appLogout();
                                } else
                                {
                                }
                            } catch (Exception e)
                            {
                                if(view != null)
                                    view.showError(e.getMessage());
                            }
                        }
                        @Override
                        public void onError(Throwable e)
                        {
                            if(view != null)
                                view.showError(activity.getString(R.string.server_error));
                        }
                        @Override
                        public void onComplete() {}
                    });
        }else
        {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    /*
     *On Handel result *
     * */
    @Override
    public boolean onHandelActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode== PROFILE_REQUEST&&resultCode== Activity.RESULT_OK)
        {
            Bundle result_data=data.getExtras();
            if(result_data!=null&&result_data.containsKey(AppConfig.USER_ID))
            {
                String user_id=result_data.getString(AppConfig.USER_ID);
                int result_action=result_data.getInt(AppConfig.RESULT_DATA);
                if(result_action==AppConfig.ON_SUPER_LIKE)
                {
                    checkForWalletThenSuperlike(user_id);
                }else if(result_action==AppConfig.ON_LIKE)
                {
                    doLiked(user_id);
                }else if(result_action==AppConfig.ON_DISLIKE)
                {
                    doDislike(user_id);
                }
                else if(result_action==AppConfig.ON_CHAT)
                {
                    //setNeedToUpdateChat(true);
                }
                return true;
            }
            return false;
        }else
        {
            return false;
        }
    }

    @Override
    public void launchWalletEmptyDialog(){
        spendCoinDialog.showDialog("",true,"",activity.getString(R.string.empty_wallet_chat_msg),"",this,true,false);
    }


    @Override
    public void onCoinButton(boolean dontShowAgain) {

    }

    @Override
    public void onBuyCoin() {
        if(view!= null)
            view.launchWalletScreen();
    }
}
