package com.pairzy.com.coinWallet.coinIn;

import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 *<h>InCoinModule</h>
 * <p></p>
 *@author 3Embed.
 *@since 30/5/18.
 */

@Module
public abstract class InCoinModule {

    @FragmentScoped
    @Binds
    abstract InCoinFrag inCoinFrag(InCoinFrag inCoinFrag);

    @FragmentScoped
    @Binds
    abstract InCoinContract.Presenter bindsInCoinPresenter(InCoinPresenter presenter);

}
