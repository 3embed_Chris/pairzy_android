package com.pairzy.com.googleLocationSearch;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.googleLocationSearch.model.DataHolder;
import com.pairzy.com.googleLocationSearch.model.LocationAdapter;
import com.pairzy.com.locationScreen.model.LocationHolder;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h2>GoogleLocSearchActivity</h2>
 * <p>
 * handle location search via google and near location via fourSquare.
 * </P>

 * @author 3Embed.
 * @since 22/05/2018.
 * @version 1.0.
 */
public class GoogleLocSearchActivity extends BaseDaggerActivity implements GoogleLocSearchContract.View,SwipeRefreshLayout.OnRefreshListener{

    @Inject
    GoogleLocSearchPresenter presenter;
    @Inject
    App_permission app_permission;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    Utility utility;
    @Inject
    Activity activity;
    @Inject
    LinearLayoutManager linearLayoutManager;
    @Inject
    LocationAdapter adapter;
    @Inject
    LocationHolder locationHolder;

    @BindView(R.id.location_list)
    RecyclerView location_list;
    @BindView(R.id.tv_page_title)
    TextView title;
    @BindView(R.id.search_et)
    EditText etSearch;
    @BindView(R.id.search_close_iv)
    ImageView btnSearchClose;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.parent_layout)
    CoordinatorLayout parentLayout;
    @BindView(R.id.search_layout_rl)
    RelativeLayout rlSearchLayout;
    @BindView(R.id.rl_search_button)
    RelativeLayout rlSearchButton;
    @BindView(R.id.loading_progress)
    ProgressBar pbLoading;
    @BindView(R.id.loading_text)
    TextView tvLoading;
    @BindView(R.id.error_icon)
    ImageView ivErrorIcon;
    @BindView(R.id.error_msg)
    TextView tvErrorMsg;
    @BindView(R.id.empty_data)
    RelativeLayout rlEmptyData;
    @BindView(R.id.loading_view)
    RelativeLayout rlLoadingView;
    @BindView(R.id.no_more_data)
    TextView tvNoMoreDataTitle;
    @BindView(R.id.empty_details)
    TextView tvEmptyDetail;

    private Unbinder unbinder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_loc_search);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        initView();
        setSearchQueryListener();
        presenter.init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.askLocationPermission();
    }

    @Override
    public void applyFont() {
        title.setTypeface(typeFaceManager.getCircularAirBold());
        etSearch.setTypeface(typeFaceManager.getCircularAirBook());
        tvNoMoreDataTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvEmptyDetail.setTypeface(typeFaceManager.getCircularAirBook());
    }

    @Override
    public void setData(Intent data) {
        setResult(RESULT_OK,data);
        onBackPressed();
    }

    public void setSearchQueryListener() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                String data = charSequence.toString().trim();
                if (!data.isEmpty()) {
                    DataHolder dataHolder = new DataHolder();
                    dataHolder.isNearest_data = 1;
                    dataHolder.latitude = locationHolder.getLatitude();
                    dataHolder.logitude = locationHolder.getLongitude();
                    dataHolder.Search_text_data = data;
                    presenter.callPlacesApi(dataHolder);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @OnClick(R.id.rl_search_button)
    public void search(){
        showSearchLayout(true);
        utility.openSpotInputKey(activity,etSearch);
    }

    @OnClick(R.id.search_close_iv)
    public void searchClose(){
        showSearchLayout(false);
        utility.closeSpotInputKey(activity,etSearch);
        onRefresh();
    }

    private void showSearchLayout(boolean show) {
        if(show){
            rlSearchLayout.setVisibility(View.VISIBLE);
        }
        else{
            rlSearchLayout.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.back_button)
    public void back(){
        onBackPressed();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * <h2>initView</h2>
     * <p>
     * Initializing all the xml content.
     * </P>
     */
    private void initView() {
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh2, R.color.refresh1,R.color.refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        location_list.setLayoutManager(linearLayoutManager);
        location_list.setItemAnimator(new DefaultItemAnimator());
        location_list.setHasFixedSize(false);
        location_list.setAdapter(adapter);
        adapter.setListener(presenter);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.parseLocation(requestCode,resultCode,data);
    }

    @Override
    public void onBackPressed(){
        utility.closeSpotInputKey(activity,etSearch);
        super.onBackPressed();
        this.finish();
        activity.overridePendingTransition(R.anim.slide_from_left,R.anim.slide_to_right);
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        DataHolder dataHolder = new DataHolder();
        dataHolder.isNearest_data = 0;
        dataHolder.latitude = locationHolder.getLatitude();
        dataHolder.logitude = locationHolder.getLongitude();
        presenter.callPlacesApi(dataHolder);
    }

    @Override
    public void showLoading() {
        tvErrorMsg.setVisibility(View.GONE);
        ivErrorIcon.setVisibility(View.GONE);
        tvLoading.setVisibility(View.VISIBLE);
        pbLoading.setVisibility(View.VISIBLE);
        rlLoadingView.setVisibility(View.VISIBLE);
        location_list.setVisibility(View.GONE);
        rlEmptyData.setVisibility(View.GONE);
    }


    @Override
    public void showMessage(String msg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+msg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(int msgId) {
        String message=getString(msgId);
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showError(String errorMsg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+errorMsg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showError(int id)
    {
        String message=getString(id);
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showNetworkError(String errorMsg) {
        if(tvErrorMsg != null) {
            tvErrorMsg.setVisibility(View.VISIBLE);
            tvErrorMsg.setText(errorMsg);
            ivErrorIcon.setVisibility(View.VISIBLE);
            tvLoading.setVisibility(View.GONE);
            pbLoading.setVisibility(View.GONE);
            location_list.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void emptyData()
    {
        if(location_list != null) {
            location_list.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showData() {
        if(presenter.isListEmpty()){
            emptyData();
        }
        else {
            rlLoadingView.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
            location_list.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.dispose();
        unbinder.unbind();
    }
}
