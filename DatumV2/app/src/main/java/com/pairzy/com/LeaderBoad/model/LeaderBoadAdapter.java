package com.pairzy.com.LeaderBoad.model;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

public class LeaderBoadAdapter extends RecyclerView.Adapter {


    private TypeFaceManager typeFaceManager;
    private ArrayList<LeaderBoardDataPOJO> arrayList;

    private Activity activity;
    public LeaderBoadAdapter(Activity activity, ArrayList<LeaderBoardDataPOJO> arrayList, TypeFaceManager typeFaceManager) {
        this.typeFaceManager = typeFaceManager;
        this.activity=activity;
        this.arrayList=arrayList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.leader_boad_item,parent,false);
        return new LeaderBoadViewHolder(view,typeFaceManager);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        LeaderBoadViewHolder viewHolder= (LeaderBoadViewHolder) holder;
        switch (position)
        {
            case 0:viewHolder.tvIndex.setVisibility(View.INVISIBLE);
                viewHolder.ivIndex.setVisibility(View.VISIBLE);
                viewHolder.ivIndex.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_top_1));
                break;
            case 1:viewHolder.tvIndex.setVisibility(View.INVISIBLE);
                viewHolder.ivIndex.setVisibility(View.VISIBLE);
                viewHolder.ivIndex.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_top_2));
                break;
            case 2:viewHolder.tvIndex.setVisibility(View.INVISIBLE);
                viewHolder.ivIndex.setVisibility(View.VISIBLE);
                viewHolder.ivIndex.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_top_3));
                break;
            default:
                viewHolder.tvIndex.setVisibility(View.VISIBLE);
                viewHolder.ivIndex.setVisibility(View.INVISIBLE);
                viewHolder.tvIndex.setText(position+1+"");
        }

        viewHolder.sdvProfile.setImageURI(arrayList.get(position).getProfilePic());
        viewHolder.tvProfile.setText(arrayList.get(position).getUserName());
        viewHolder.count.setText(arrayList.get(position).getUserpairCount()+"");
        viewHolder.coin.setText(arrayList.get(position).getCoinEarnByVote()+"");
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
