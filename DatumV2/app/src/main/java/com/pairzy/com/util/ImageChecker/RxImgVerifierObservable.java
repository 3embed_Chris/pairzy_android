package com.pairzy.com.util.ImageChecker;
import io.reactivex.Observable;
import io.reactivex.Observer;
/**
 * <h2>RxImgVerifierObservable</h2>
 * <P>
 *
 * </P>
 * @since  4/17/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class RxImgVerifierObservable extends Observable<VerifierResult>
{
    private Observer<? super VerifierResult> observer;
    private static RxImgVerifierObservable rxJava2Observable = null;
    private RxImgVerifierObservable() {}
    private VerifierResult lastData;

    static RxImgVerifierObservable getInstance()
    {
        if (rxJava2Observable == null) {
            rxJava2Observable = new RxImgVerifierObservable();
        }
        return rxJava2Observable;
    }
    @Override
    protected void subscribeActual(Observer<? super VerifierResult> observer)
    {
        this.observer = observer;
        if(lastData!=null)
        {
            this.observer.onNext(lastData);
            lastData=null;
        }
    }


    public void publishData(VerifierResult data)
    {
        if (observer != null && data != null)
        {
            if (data.isError()) {
                observer.onError(new Throwable(data.getMessage()));
            } else {
                observer.onNext(data);
            }
            observer.onComplete();
        }else
        {
            this.lastData=data;
        }
    }
}
