package com.pairzy.com.planDate;

import android.content.Intent;

import com.pairzy.com.BaseView;
import com.pairzy.com.home.Dates.Model.DateListPojo;

/**
 * <h>CallDateContract interface</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public interface DateContract {

    interface View extends BaseView{
        void applyFont();
        void showError(String errorMsg);
        void showMessage(String msg);
        void showMessage(int msgId);
        void showSelectedDate(String dateStr);
        void showSelectedAddress(SelectedLocationHolder locationHolder);
        void initUserData(String userName, String userImage);
        void initOwnData(String yourName, String profilePic);
        void initUserData(DateListPojo date_data);
        void returnFinalData(DateListPojo date_data);
        void launchCoinWallet();
    }

    interface Presenter{
        void init();
        void launchDateTimePicker();
        void handleOnActivityResult(int requestCode, int resultCode, Intent data);
        void parseLocation(int requestCode, int resultCode, Intent data);
        void initData(Intent intent);
        void checkForCalenderPermission();
        void dispose();
        void checkForValidInput();
        void loadCoinDialog();
    }
}