package com.pairzy.com.home.Dates.upcomingPage;

import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * <h2>PostFeedFragBuilder</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface UpcomingFrgBuilder
{
    @FragmentScoped
    @Binds
    UpcomingFrg getProspectItemFragment(UpcomingFrg availableFrg);

    @FragmentScoped
    @Binds
    UpcomingFrgContract.Presenter upcomingFragPresenter(UpcomingFrgPresenter presenter);

}

