package com.pairzy.com.coinWallet.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * <h>WalletData</h>
 * <p></p>
 * @author 3Embed.
 * @since 6/6/18.
 */
public class Data {
    @SerializedName("allCoins")
    @Expose
    private List<CoinPojo> allCoins = null;
    @SerializedName("coinIn")
    @Expose
    private List<CoinPojo> coinIn = null;
    @SerializedName("coinOut")
    @Expose
    private List<CoinPojo> coinOut = null;

    public List<CoinPojo> getAllCoins() {
        return allCoins;
    }

    public void setAllCoins(List<CoinPojo> allCoins) {
        this.allCoins = allCoins;
    }

    public List<CoinPojo> getCoinIn() {
        return coinIn;
    }

    public void setCoinIn(List<CoinPojo> coinIn) {
        this.coinIn = coinIn;
    }

    public List<CoinPojo> getCoinOut() {
        return coinOut;
    }

    public void setCoinOut(List<CoinPojo> coinOut) {
        this.coinOut = coinOut;
    }

}
