package com.pairzy.com.util.Exception;

/**
 * <h2>DataParsingException</h2>
 * <P>
 *     CoinData parsing Exception on the Response data.
 * </P>
 * @since  4/3/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class DataParsingException extends Exception
{
    public DataParsingException() {
        super();
    }

    public DataParsingException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
