package com.pairzy.com.data.model;

/**
 * Created by ankit on 5/7/18.
 */

public class DateEvent {

    private String userId;
    private String eventId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
