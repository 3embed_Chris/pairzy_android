package com.pairzy.com.MyProfile.Model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pairzy.com.R;
import com.pairzy.com.data.model.PrefData;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

/**
 * <h2>MyPreferenceAdapter</h2>
 * <P>
 *
 * </P>
 * @since  4/11/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MyPreferenceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private ArrayList<PrefData> prefDataList;
    private TypeFaceManager typeFaceManager;
    private AdapterClickCallback callback;

    public interface AdapterClickCallback{
        void onPreferenceClick(View itemView, PrefData prefData);
    }

    public void setCallback(AdapterClickCallback callback) {
        this.callback = callback;
    }

    public MyPreferenceAdapter(ArrayList<PrefData> prefDataList, TypeFaceManager typeFaceManager)
    {
        this.prefDataList = prefDataList;
        this.typeFaceManager=typeFaceManager;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_profile_category_row, parent, false);
        return new RowItemHolder(itemView,typeFaceManager);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        RowItemHolder item_view= (RowItemHolder) holder;
        handelView(item_view);

        item_view.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(callback != null){
                    callback.onPreferenceClick(item_view.itemView,prefDataList.get(position));
                }
            }
        });
    }

    /*
     * Managing the item details*/
    private void handelView(RowItemHolder holder)
    {
        int position=holder.getAdapterPosition();
        PrefData prefData = prefDataList.get(position);
        holder.title.setText(prefData.getLabel());
        try
        {
            if (prefData.getIsDone()) {
                if (prefData.getSelectedValues().size() > 1) {
                    String text_data = prefData.getSelectedValues().get(0);
                    holder.selected_test.setText(text_data);
                    holder.count_data.setVisibility(View.VISIBLE);
                    holder.count_data.setText(String.valueOf((prefData.getSelectedValues().size()-1==0?"":"+"+(prefData.getSelectedValues().size()-1))));
                } else if (prefData.getSelectedValues().size() > 0) {
                    holder.selected_test.setText(prefData.getSelectedValues().get(0));
                    holder.count_data.setVisibility(View.GONE);
                }
                else{
                    holder.selected_test.setText(R.string.not_set_Text);
                    holder.count_data.setVisibility(View.GONE);
                    holder.selected_test.setVisibility(View.VISIBLE);
                }
            } else {
                holder.selected_test.setText(R.string.not_set_Text);
                holder.count_data.setVisibility(View.GONE);
                holder.selected_test.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){}
    }

    @Override
    public int getItemCount()
    {
        return prefDataList.size();
    }
}
