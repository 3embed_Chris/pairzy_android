package com.pairzy.com.MyProfile.editPreference;
import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;

import com.pairzy.com.R;

/**
 * @since  2/6/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class EditAnimatorHandler
{
    private Context context;

     EditAnimatorHandler(Context context)
    {
        this.context=context;
    }

    public Animation getItemScaleUp()
    {
        return AnimationUtils.loadAnimation(context, R.anim.item_selection_scal_up);
    }

    public Animation getItemScaleDown()
    {
        return AnimationUtils.loadAnimation(context,R.anim.item_selection_scal_down);
    }

    public Animation getScaleUp()
    {
        return AnimationUtils.loadAnimation(context, R.anim.scal_center_up);
    }

    public Animation getScaleDown()
    {
        return AnimationUtils.loadAnimation(context,R.anim.scal_center_down);
    }

    public Animation getViewScaleUp()
    {
        Animation  anim1 = new ScaleAnimation(
                1f, 0.85f,
                1f, 0.85f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        anim1.setFillAfter(true);
        anim1.setDuration(100);
        return anim1;
    }

    public Animation getViewScaleDown()
    {
        Animation anim2 = new ScaleAnimation(
                0.85f, 1f,
                0.85f, 1f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        anim2.setFillAfter(true);
        anim2.setDuration(100);
        return anim2;
    }
}
