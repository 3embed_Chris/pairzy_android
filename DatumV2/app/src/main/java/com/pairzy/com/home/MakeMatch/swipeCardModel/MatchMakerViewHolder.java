package com.pairzy.com.home.MakeMatch.swipeCardModel;

import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.R;
import com.pairzy.com.util.CustomView.SquizeButton;
import com.pairzy.com.util.matchMakerSwipeCard.SwipeFlingAdapterView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MatchMakerViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.frame)
    SwipeFlingAdapterView flingContainer;

    @BindView(R.id.dislike_view)
    SquizeButton dislike;

    @BindView(R.id.like_view)
    SquizeButton like;

    private AdapterItemCallback callback;

     MatchMakerViewHolder(@NonNull View itemView, AdapterItemCallback callback) {
        super(itemView);
        ButterKnife.bind(this,itemView);
         this.callback=callback;
         dislike.setClickListener(this::doDislike);
         like.setClickListener(this::doLike);
}

    private void doLike() {
        if(callback!=null)
            callback.rightSwipe(this.getAdapterPosition());
    }

    private void doDislike() {
        if(callback!=null)
            callback.leftSwipe(this.getAdapterPosition());
    }
}
