package com.pairzy.com.moments;

import android.app.Activity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.pairzy.com.moments.model.MomentsVerticalAdapter;
import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Matches.PostFeed.deletePostDialog.DeletePostDialog;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.progressbar.LoadingProgress;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

@Module
public class MomentUtilModule {

    @ActivityScoped
    @Provides
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }

    @ActivityScoped
    @Provides
    ArrayList<MomentsData> provideMomentsDataList(){
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    RequestManager provideRequestManager(Activity activity){
        return Glide.with(activity).setDefaultRequestOptions(new RequestOptions());
    }

    @ActivityScoped
    @Provides
    MomentsVerticalAdapter provideMomentsAdapter(Utility utility, PreferenceTaskDataSource dataSource,TypeFaceManager typeFaceManager, Activity activity,
                                                 RequestManager requestManager, ArrayList<MomentsData> momentsData){
        return new MomentsVerticalAdapter(utility,dataSource,
                typeFaceManager,activity,requestManager,momentsData);
    }

    @ActivityScoped
    @Provides
    DeletePostDialog deletePostDialog(Activity activity,TypeFaceManager typeFaceManager){
        return new DeletePostDialog(activity,typeFaceManager);
    }
}
