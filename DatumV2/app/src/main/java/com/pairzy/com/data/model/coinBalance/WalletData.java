package com.pairzy.com.data.model.coinBalance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 4/6/18.
 */

public class WalletData {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("coins")
    @Expose
    private Coins coins;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Coins getCoins() {
        return coins;
    }

    public void setCoins(Coins coins) {
        this.coins = coins;
    }
}
