package com.pairzy.com.home.Dates.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h2>PastDateListPojo</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class PastDateListPojo
{

    public PastDateListPojo() {
    }

    public PastDateListPojo(DateListPojo removedDate, String userName) {
        this.id = removedDate.getDataId();
        this.dataId = removedDate.getDataId();
        this.opponentId = removedDate.getOpponentId();
        this.opponentName = removedDate.getOpponentName();
        this.opponentProfilePic = removedDate.getOpponentProfilePic();
        this.opponentResponse = "Denied";
        this.proposedOn = removedDate.getProposedOn();
        this.requestedFor = removedDate.getRequestedFor();
        this.status = "Canceled by ".concat(userName);

    }

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("acceptedDateTimeStamp")
    @Expose
    private Long acceptedDateTimeStamp;

    @SerializedName("data_id")
    @Expose
    private String dataId;


    @SerializedName("opponentId")
    @Expose
    private String opponentId;

    @SerializedName("opponentName")
    @Expose
    private String opponentName;

    @SerializedName("opponentProfilePic")
    @Expose
    private String opponentProfilePic;

    @SerializedName("opponentResponse")
    @Expose
    private String opponentResponse;

    @SerializedName("proposedOn")
    @Expose
    private Long proposedOn;

    @SerializedName("requestedFor")
    @Expose
    private String requestedFor;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("rate")
    @Expose
    private Integer rating = 0;

    private boolean loading = false;
    private boolean loadingFailed = false;


    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getAcceptedDateTimeStamp() {
        return acceptedDateTimeStamp;
    }

    public void setAcceptedDateTimeStamp(Long acceptedDateTimeStamp) {
        this.acceptedDateTimeStamp = acceptedDateTimeStamp;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getOpponentId() {
        return opponentId;
    }

    public void setOpponentId(String opponentId) {
        this.opponentId = opponentId;
    }

    public String getOpponentName() {
        return opponentName;
    }

    public void setOpponentName(String opponentName) {
        this.opponentName = opponentName;
    }

    public String getOpponentProfilePic() {
        return opponentProfilePic;
    }

    public void setOpponentProfilePic(String opponentProfilePic) {
        this.opponentProfilePic = opponentProfilePic;
    }

    public String getOpponentResponse() {
        return opponentResponse;
    }

    public void setOpponentResponse(String opponentResponse) {
        this.opponentResponse = opponentResponse;
    }

    public Long getProposedOn() {
        return proposedOn;
    }

    public void setProposedOn(Long proposedOn) {
        this.proposedOn = proposedOn;
    }

    public String getRequestedFor() {
        return requestedFor;
    }

    public void setRequestedFor(String requestedFor) {
        this.requestedFor = requestedFor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public void setLoadingFailed(boolean loadingFailed) {
        this.loadingFailed = loadingFailed;
    }

    public boolean isLoadingFailed() {
        return loadingFailed;
    }
}
