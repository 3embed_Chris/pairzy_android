package com.pairzy.com.MyProfile;

import android.app.Activity;

import androidx.fragment.app.FragmentManager;

import com.androidinsta.com.ImageData;
import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.MyProfile.Model.ProfileMediaAdapter;
import com.pairzy.com.MyProfile.Model.ProfileMediaPojo;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.boostDialog.BoostDialog;
import com.pairzy.com.util.progressbar.LoadingProgress;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
/**
 * <h2>ProfileUtil</h2>
 * <P>
 *
 * </P>
 * @since  4/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class ProfileUtil
{
    @Provides
    @ActivityScoped
    ArrayList<ImageData> getInstaMediaList()
    {
        return new ArrayList<>();
    }

    @Provides
    @ActivityScoped
    ArrayList<ProfileMediaPojo> getMediaList()
    {
        return new ArrayList<>();
    }


    @Provides
    @ActivityScoped
    ProfileMediaAdapter getMediaAdapter(@Named(MyProfileBuilder.PROFILE_FRAGMENT_MANAGER)FragmentManager fm,ArrayList<ProfileMediaPojo> list)
    {
        return new ProfileMediaAdapter(fm,list);
    }

    @Provides
    @ActivityScoped
    DatumProgressDialog getDatumProgressDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new DatumProgressDialog(activity,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    LoadingProgress getLoadingProgress(Activity activity)
    {
        return new LoadingProgress(activity);
    }

    @Provides
    @ActivityScoped
    BoostDialog getBootsDialog(Activity activity, TypeFaceManager typeFaceManager, PreferenceTaskDataSource dataSource, Utility utility)
    {
        return new BoostDialog(activity,typeFaceManager,dataSource,utility);
    }

    @Provides
    @ActivityScoped
    ArrayList<MomentsData> provideProfileData()
    {return  new ArrayList<>();}

//    @Provides
//    @ActivityScoped
//    MomentsGridAdapter getMomentsDataPic(Activity activity, ArrayList<MomentsData> list)
//    {
//        return new MomentsGridAdapter(activity,list);
//    }
}
