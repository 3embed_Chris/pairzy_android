package com.pairzy.com.home.Dates.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>PastDateListPojo</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class DateListPojo implements Serializable
{
    @SerializedName("onlineStatus")
    @Expose
    private Integer onlineStatus = 0;  //offline

    @SerializedName("createdTimestamp")
    @Expose
    private Long createdTimestamp;

    @SerializedName("data_id")
    @Expose
    private String dataId;

    @SerializedName("isInitiatedByMe")
    @Expose
    private Integer isInitiatedByMe;

    @SerializedName("latitude")
    @Expose
    private Double latitude;

    @SerializedName("longitude")
    @Expose
    private Double longitude;

    @SerializedName("opponentId")
    @Expose
    private String opponentId;

    @SerializedName("opponentName")
    @Expose
    private String opponentName;

    @SerializedName("opponentProfilePic")
    @Expose
    private String opponentProfilePic;

    @SerializedName("placeName")
    @Expose
    private String placeName;

    @SerializedName("proposedOn")
    @Expose
    private Long proposedOn;

    @SerializedName("proposedOnInMyTimeZone")
    @Expose
    private String proposedOnInMyTimeZone;

    @SerializedName("requestedFor")
    @Expose
    private String requestedFor;

    @SerializedName("timeZone")
    @Expose
    private String timeZone;

    @SerializedName("dateType")
    @Expose
    private Integer dateType;
    @SerializedName("isLiveDate")
    @Expose
    private boolean isLiveDate = false;

    @SerializedName("isReschedule")
    @Expose
    private boolean isReschedule = false;

    /*
     * observer use to identify belong to pending (true) or upcoming(false) list.
     */
    boolean isPendingDate = true;

    private boolean loading = false;
    private boolean loadingFailed = false;

    public Integer getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(Integer onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public Integer getDateType() {
        return dateType;
    }

    public void setDateType(Integer dateType) {
        this.dateType = dateType;
    }
    public Long getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(Long createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public Integer getIsInitiatedByMe() {
        return isInitiatedByMe;
    }

    public void setIsInitiatedByMe(Integer isInitiatedByMe) {
        this.isInitiatedByMe = isInitiatedByMe;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getOpponentId() {
        return opponentId;
    }

    public void setOpponentId(String opponentId) {
        this.opponentId = opponentId;
    }

    public String getOpponentName() {
        return opponentName;
    }

    public void setOpponentName(String opponentName) {
        this.opponentName = opponentName;
    }

    public String getOpponentProfilePic() {
        return opponentProfilePic;
    }

    public void setOpponentProfilePic(String opponentProfilePic) {
        this.opponentProfilePic = opponentProfilePic;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public Long getProposedOn() {
        return proposedOn;
    }

    public void setProposedOn(Long proposedOn) {
        this.proposedOn = proposedOn;
    }

    public String getProposedOnInMyTimeZone() {
        return proposedOnInMyTimeZone;
    }

    public void setProposedOnInMyTimeZone(String proposedOnInMyTimeZone) {
        this.proposedOnInMyTimeZone = proposedOnInMyTimeZone;
    }

    public String getRequestedFor() {
        return requestedFor;
    }

    public void setRequestedFor(String requestedFor) {
        this.requestedFor = requestedFor;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public void setLoadingFailed(boolean loadingFailed) {
        this.loadingFailed = loadingFailed;
    }

    public boolean isLoadingFailed() {
        return loadingFailed;
    }

    public boolean isPendingDate() {
        return isPendingDate;
    }

    public void setPendingDate(boolean pendingDate) {
        isPendingDate = pendingDate;
    }

    public boolean isLiveDate() {
        return isLiveDate;
    }

    public void setLiveDate(boolean liveDate) {
        isLiveDate = liveDate;
    }

    public boolean isReschedule() {
        return isReschedule;
    }

    public void setReschedule(boolean reschedule) {
        isReschedule = reschedule;
    }
}
