package com.pairzy.com.util.LoadingViews;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.util.DatumCallbacks.ListItemClick;
import com.pairzy.com.util.TypeFaceManager;
/**
 * <h2>LoadMoreViewHolder</h2>
 * <P>
 *    Load more view for the datum app.
 *    this class mange he load more and failed view
 *    on loading.
 * </P>
 * @since  3/23/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class LoadMoreViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    private ImageView heart_img;
    private TextView loading_text,retry_text;
    private ListItemClick listItemClick;
    public DatumLoadingView loading_view;
    private Context context;

    public LoadMoreViewHolder(View itemView,TypeFaceManager typeFaceManager,ListItemClick callback)
    {
        super(itemView);
        this.context=itemView.getContext();
        this.listItemClick=callback;
        this.heart_img=itemView.findViewById(R.id.heart_img);
        this.loading_text=itemView.findViewById(R.id.loading_text);
        loading_view=itemView.findViewById(R.id.load_bubble);
        retry_text=itemView.findViewById(R.id.retry_text);
        this.loading_text.setTypeface(typeFaceManager.getCircularAirBold());
        this.retry_text.setTypeface(typeFaceManager.getCircularAirBold());
        itemView.findViewById(R.id.load_more_view).setOnClickListener(this);
    }

    /*
     *Showing loading failed view */
    public void setFailed()
    {
        loading_text.setVisibility(View.GONE);
        loading_view.hide();
        loading_view.setVisibility(View.GONE);
        retry_text.setVisibility(View.VISIBLE);
        try
        {
            Animation animation= AnimationUtils.loadAnimation(context, R.anim.heart_blink_fast);
            heart_img.setAnimation(animation);
            animation.start();
        }catch (Exception e){}
    }

    /*
     *Showing the loading more view */
    public void showLoadingLoadingAgain()
    {
        loading_text.setVisibility(View.VISIBLE);
        loading_view.show();
        loading_view.setVisibility(View.VISIBLE);
        retry_text.setVisibility(View.GONE);
        try
        {
            Animation animation= AnimationUtils.loadAnimation(context, R.anim.heart_blink_fast);
            heart_img.setAnimation(animation);
            animation.start();
        }catch (Exception e){}
    }

    @Override
    public void onClick(View view)
    {
        if(retry_text.getVisibility()==View.VISIBLE)
        {
            if(listItemClick!=null)
                listItemClick.onClicked(R.id.retry_text,this.getAdapterPosition());
        }
    }
}
