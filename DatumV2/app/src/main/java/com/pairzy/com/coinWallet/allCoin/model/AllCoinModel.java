package com.pairzy.com.coinWallet.allCoin.model;

import com.pairzy.com.coinWallet.CoinWalletUtil;
import com.pairzy.com.coinWallet.Model.AllCoinAdapter;
import com.pairzy.com.coinWallet.Model.CoinPojo;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

/**
 *<h>AllCoinUtilModule</h>
 * <p></p>
 *@author 3Embed.
 *@since 30/5/18.
 */
public class AllCoinModel {

    @Named(CoinWalletUtil.COIN_ALL_LIST)
    @Inject
    ArrayList<CoinPojo> arrayList;

    @Inject
    AllCoinAdapter allCoinAdapter;

    @Inject
    public AllCoinModel() {
    }


    public void addCoinItem() {
//        CoinPojo coinPojo = new CoinPojo();
//        coinPojo.setTitle("Add 150 to your wallet");
//        coinPojo.setTime(2823578985L);
//        coinPojo.setCoin(150.00);
//        arrayList.add(coinPojo);
    }

    public boolean isCoinHistoryEmpty(){
        return arrayList.size() == 0;
    }

    public void notifyAdapter(){
        allCoinAdapter.notifyDataSetChanged();
    }
}
