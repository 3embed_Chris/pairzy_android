package com.pairzy.com.register;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;

import com.pairzy.com.AppController;
import com.pairzy.com.MqttChat.Database.CouchDbController;
import com.pairzy.com.R;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.model.coinCoinfig.CoinData;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.networking.RxNetworkObserver;
import com.pairzy.com.register.Model.ProfileData;
import com.pairzy.com.register.Model.ProfileReponsePojo;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.LocationProvider.LocationApiCallback;
import com.pairzy.com.util.LocationProvider.LocationApiManager;
import com.pairzy.com.util.LocationProvider.Location_service;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.Utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * <h2>RegisterPagePresenter</h2>
 * <P>
 *
 * </P>
 * @since  2/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class RegisterPagePresenter implements RegisterContact.Presenter,App_permission.Permission_Callback,Location_service.GetLocationListener
{
    @Inject
    NetworkStateHolder holder;
    @Inject
    RxNetworkObserver rxNetworkObserver;
    @Inject
    Utility utility;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    DatumProgressDialog progressDialog;
    @Inject
    RegisterModel registerModel;
    @Inject
    NetworkService service;
    @Inject
    RegisterContact.View view;
    @Inject
    LocationApiManager locationApiManager;
    @Inject
    Location_service location_service;
    @Inject
    App_permission app_permission;
    @Inject
    Activity activity;
    @Inject
    CouchDbController couchDbController;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    private Bundle bundle;
    private CompositeDisposable compositeDisposable;

    @Inject
    RegisterPagePresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(Object view)
    {}
    @Override
    public void dropView()
    {
        location_service.stop_Location_Update();
        compositeDisposable.clear();
    }

    @Override
    public void updateProgress(int progress) {
        view.updateProgress(progress);
    }

    @Override
    public void showError(String message)
    {
        if(view==null)
            return;
        view.showError(message);
    }

    @Override
    public void showMessage(String message)
    {
        if(view==null)
            return;
        view.showMessage(message);
    }

    @Override
    public void launchFragment(DaggerFragment fragment, boolean keepBack)
    {
        if(view==null)
            return;
        view.openFragment(fragment,keepBack);
    }

    @Override
    public void updateProfile(Bundle data)
    {
        bundle=data;
        askForLocationPermission();
    }

    @Override
    public void initNetworkObserver()
    {
        view.updateInterNetStatus(holder.isConnected());
        Observer<NetworkStateHolder> observer = new Observer<NetworkStateHolder>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {
                compositeDisposable.add(d);
            }
            @Override
            public void onNext(NetworkStateHolder value)
            {
                view.updateInterNetStatus(value.isConnected());
            }
            @Override
            public void onError(Throwable e)
            {
                e.printStackTrace();
            }
            @Override
            public void onComplete()
            {}
        };
        rxNetworkObserver.getObservable().subscribeOn(Schedulers.newThread());
        rxNetworkObserver.getObservable().observeOn(AndroidSchedulers.mainThread());
        rxNetworkObserver.getObservable().subscribe(observer);
    }


    @Override
    public void updateUserDetails(Bundle data)
    {
        if(!holder.isConnected())
        {
            progressDialog.cancel();
            if(view!=null)
                view.showError(activity.getString(R.string.no_internet_error));
        }else
        {
            service.signUp(registerModel.getAuthorization(),
                    registerModel.getLanguage(),
                    registerModel.signUpData(data,view.getCountryCode(),view.getMobileNumber(),view.getEnteredOtp()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> result)
                        {
                            progressDialog.cancel();
                            try {
                                if(result.code()==200)
                                {
                                    String data=result.body().string();
                                    ProfileReponsePojo result_data=utility.getGson().fromJson(data,ProfileReponsePojo.class);
                                    saveUserDetails(result_data);
                                }else
                                {
                                    if(view!=null)
                                        view.showError(registerModel.getError(result));
                                    dataSource.setLoggedIn(false);
                                }
                            } catch (IOException e)
                            {
                                view.showError(e.getMessage());
                            }
                        }
                        @Override
                        public void onError(Throwable errorMsg)
                        {
                            progressDialog.cancel();
                            if(view!=null)
                                view.showError(errorMsg.getMessage());
                        }
                        @Override
                        public void onComplete()
                        {}
                    });
        }
    }

    @Override
    public boolean onHandelActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode==Location_service.REQUEST_CHECK_SETTINGS)
        {
            if(resultCode== Activity.RESULT_OK)
            {
                location_service.checkLocationSettings();
            }else
            {
                getUserLocation();
            }
            return true;
        }else
        {
            return false;
        }
    }

    @Override
    public void askForLocationPermission()
    {
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.LOCATION);
        app_permission.getPermission("location",permissions,this);
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag)
    {
        getUserLocationApi();
    }


    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag)
    {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        app_permission.show_Alert_Permission(activity.getString(R.string.location_access_text),activity.getString(R.string.location_acess_subtitle),
                activity.getString(R.string.location_acess_message),stringArray);
    }


    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag)
    {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean parmanent)
    {
        getUserLocation();
    }

    @Override
    public void getUserLocation()
    {
        progressDialog.show();
        locationApiManager.getLocation(new LocationApiCallback() {
            @Override
            public void onSuccess(Double lat, Double lng, double accuracy)
            {
                bundle.putDouble(LAT,lat);
                bundle.putDouble(LNG,lng);
                updateUserDetails(bundle);
            }

            @Override
            public void onError(String error)
            {
                if(view!=null)
                    view.showError(error);
            }
        });
    }

    @Override
    public void getUserLocationApi()
    {
        progressDialog.show();
        location_service.setListener(this);
        location_service.checkLocationSettings();
    }

    @Override
    public void updateLocation(Location location)
    {
        if(location!=null)
        {
            location_service.stop_Location_Update();
            bundle.putDouble(LAT,location.getLatitude());
            bundle.putDouble(LNG,location.getLongitude());
            updateUserDetails(bundle);
        }
    }

    @Override
    public void location_Error(String error)
    {
        getUserLocation();
    }

    /*
     *Saving the user details */
    private void saveUserDetails(ProfileReponsePojo reponsePojo)
    {
        ProfileData data = reponsePojo.getData();

        dataSource.setToken(data.getToken());
        dataSource.setName(data.getFirstName());
        dataSource.setGender(data.getGender());
        dataSource.setBirthDate(data.getDob());
        dataSource.setMobileNumber(data.getContactNumber());
        dataSource.setCountryCode(data.getCountryCode());
        dataSource.setUserId(data.getId());
        dataSource.setProfilePicture(data.getProfilePic());

        /*
         *coin config data
         */
        CoinData coinData = reponsePojo.getCoinConfigData();
        coinConfigWrapper.setCoinData(coinData);

        /*
         *
         */

        dataSource.setLoggedIn(true);
        initialChatSetup(data);
        if(view!=null)
            view.openPreferencePage();
    }
    /*
     * Code from hola live on success of mob verification.
     */
    private void initialChatSetup(ProfileData data){
        String profilePic = "", userName = "";
        CouchDbController db = AppController.getInstance().getDbController();

        Map<String, Object> map = new HashMap<>();
        if (!TextUtils.isEmpty(data.getProfilePic())) {
            profilePic = data.getProfilePic();
            map.put("userImageUrl", profilePic);
        } else {
            map.put("userImageUrl", "");
        }
        if (!TextUtils.isEmpty(data.getFirstName())) {
            userName = data.getFirstName();
            map.put("userName", userName);
        }

        map.put("userId", data.getId());
        map.put("userIdentifier", data.getId());
        map.put("apiToken", data.getToken());

        AppController.getInstance().getSharedPreferences().edit().putString("token", data.getToken()).apply();
        /*
         * By phone number verification
         */
        map.put("userLoginType", 1);
        if (!db.checkUserDocExists(AppController.getInstance().getIndexDocId(), data.getId())) {
            String userDocId = db.createUserInformationDocument(map);
            db.addToIndexDocument(AppController.getInstance().getIndexDocId(), data.getId(), userDocId);
        } else {
            db.updateUserDetails(db.getUserDocId(data.getId(), AppController.getInstance().getIndexDocId()), map);
        }
        if (!userName.isEmpty()) {
            db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(), data.getId(), 1, true);
        } else {
            db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(), data.getId(), 1, false);
        }
        /*
         * To update myself as available for call
         */
        AppController.getInstance().setSignedIn(false,true,data.getId() , userName, data.getId());
    }

}
