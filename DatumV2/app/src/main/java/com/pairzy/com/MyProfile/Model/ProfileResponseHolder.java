package com.pairzy.com.MyProfile.Model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * <h2>ProfileResponseHolder</h2>
 * <P>
 *
 * </P>
 * @since  4/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ProfileResponseHolder
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ProfileData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public ProfileData getData()
    {
        return data;
    }

    public void setData(ProfileData data)
    {
        this.data = data;
    }
}
