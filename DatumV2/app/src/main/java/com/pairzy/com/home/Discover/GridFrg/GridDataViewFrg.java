package com.pairzy.com.home.Discover.GridFrg;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.pairzy.com.R;
import com.pairzy.com.addCoin.AddCoinActivity;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Discover.DiscoveryFragContract;
import com.pairzy.com.home.Discover.DiscoveryFragUtil;
import com.pairzy.com.home.Discover.GridFrg.Model.CardDeckViewAdapter;
import com.pairzy.com.home.Discover.GridFrg.Model.DeckCardItemClicked;
import com.pairzy.com.home.Discover.ListFrg.ListDataViewFrgContract;
import com.pairzy.com.home.Discover.Model.UserItemPojo;
import com.pairzy.com.home.HomeContract;
import com.pairzy.com.home.HomeModel.HomeAnimation;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.userProfile.UserProfilePage;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.CardDeckView.SwipeFlingAdapterView;
import com.pairzy.com.util.CustomView.SquizeButton;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * <h2>GridDataViewFrg</h2>
 * A simple {@link Fragment} subclass.
 * @author 3Embed.
 * @version 1.0.
 * @since 02-03-2018.
 */
@ActivityScoped
public class GridDataViewFrg extends DaggerFragment implements GridDataViewContract.View
{
    @Inject
    Activity activity;
    @Inject
    NetworkStateHolder holder;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    TypeFaceManager typeFaceManager;

    @BindView(R.id.cardviewlayout)
    RelativeLayout cardviewlayout;
    @BindView(R.id.frameLayout)
    FrameLayout frameLayout;
    @BindView(R.id.frame)
    SwipeFlingAdapterView flingContainer;
    @BindView(R.id.boost_view)
    SquizeButton boost_view;
    @BindView(R.id.rewind_view)
    SquizeButton rewind_view;
    @BindView(R.id.dislike_view)
    SquizeButton dislike_view;
    @BindView(R.id.like_view)
    SquizeButton like_view;
    @BindView(R.id.super_like_view)
    SquizeButton super_like_view;
    @BindView(R.id.tutorial_overlay_view)
    FrameLayout flTutorialView;
    @BindView(R.id.pb_boost_progress)
    ProgressBar pbBoostProgress;
    @BindView(R.id.rl_boost_view)
    RelativeLayout rlBoostView;
    @BindView(R.id.tv_boost_counter)
    TextView tvBoostCounter;

    @BindView(R.id.fl_coin_one)
    FrameLayout flCoinOne;
    @BindView(R.id.fl_coin_two)
    FrameLayout flCoinTwo;
    @BindView(R.id.fl_coin_three)
    FrameLayout flCoinThree;
    @BindView(R.id.tv_coin)
    TextView tvCoin;

    @Inject
    GridDataViewContract.Presenter presenter;
    @Inject
    DiscoveryFragContract.Presenter discoveryPresenter;
    @Inject
    ListDataViewFrgContract.Presenter listPresenter;
    @Inject
    HomeContract.Presenter homePresenter;

    @Inject
    CardDeckViewAdapter adapter;

    @Inject
    @Named(DiscoveryFragUtil.USER_LIST)
    ArrayList<UserItemPojo> userList;
    @Inject
    HomeAnimation homeAnimation;
    @Inject
    Utility utility;
    private Unbinder unbinder;
    private int scrollPosition = 0;
    boolean isSuperLikedByButton = false;
    public static int viewWidth = 500;
    public static int viewHeight = 600;

    public GridDataViewFrg() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_grid_data_view_frg, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder=ButterKnife.bind(this,view);
        presenter.takeView(this);
        initUi();
        viewWidth = utility.getTheScreenWidth(activity,95);
        viewHeight = utility.getTheScreenHeight(activity,80);
    }

    @Override
    public void onDestroy()
    {
        unbinder.unbind();
        presenter.dropView();
        super.onDestroy();
    }

    /*
     * initialization of the xml content*/
    private void initUi()
    {
        tvCoin.setTypeface(typeFaceManager.getCircularAirBold());
        tvBoostCounter.setTypeface(typeFaceManager.getCircularAirBook());


        if(dataSource.getShowCardTutorialView()){
            flTutorialView.setVisibility(View.VISIBLE);
            flTutorialView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flTutorialView.setVisibility(View.GONE);
                    dataSource.setShowCardTutorialView(false);
                }
            });
        }

        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter()
            {}
            @Override
            public void onLeftCardExit(Object dataObject)
            {
                if(holder.isConnected())
                {
                    UserItemPojo temp=userList.remove(0);

                    adapter.notifyDataSetChanged();
                    listPresenter.updateDataChanged();
                    if(!temp.isAdView())
                        discoveryPresenter.doDisLike(temp);
                    discoveryPresenter.preFetchImage(0);

                    if(userList.isEmpty()){
                        homePresenter.stopPlayer();
                    }
                }else
                {
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onRightCardExit(Object dataObject)
            {
                if(holder.isConnected())
                {
                    UserItemPojo temp = userList.remove(0);
                    adapter.notifyDataSetChanged();
                    listPresenter.updateDataChanged();
                    if(!temp.isAdView())
                        discoveryPresenter.doLike(temp);
                    discoveryPresenter.preFetchImage(0);

                    if(userList.isEmpty()){
                        homePresenter.stopPlayer();
                    }
                }else
                {
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onUpperCardExit(Object dataObject)
            {
                if(holder.isConnected())
                {
                    //change this approach miss handle can cause recursion
                    UserItemPojo temp=userList.remove(0);
                    if(isSuperLikedByButton){
                        isSuperLikedByButton = false;
                        callSuperLikeApi(temp);
                    }
                    else{
                        if(!temp.isAdView()) //no need to show spend dialog for ad.
                            superLikeBySwipe(temp);
                        presenter.updateDataChanged();
                    }
                    adapter.notifyDataSetChanged();
                    listPresenter.updateDataChanged();
                    discoveryPresenter.preFetchImage(0);

                    if(userList.isEmpty()){
                        homePresenter.stopPlayer();
                    }
                }else
                {
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter)
            {
                try
                {
                    int size=userList.size();
                    if(size<5&&size>1)
                    {
                        int last_pos=size-1;
                        UserItemPojo last_item=userList.get(last_pos);
                        if(!last_item.isLoading() && !presenter.isNoMoreData())
                        {
                            UserItemPojo loading_item=new UserItemPojo();
                            loading_item.setLoading(true);
                            userList.add(loading_item);
                            presenter.loadMore();
                        }
                    }
                }catch (Exception ignored){}

                int size=userList.size();
                if(size == 0)
                {
                    presenter.noData();
                }
            }

            @Override
            public void onAdapterItemClick(Object dataObject)
            {}

            @Override
            public void onNotify() {
                Log.d("notified", "onNotify:");
            }

            @Override
            public void onScroll(float scrollProgressPercent, int isHorizontal, float scrollHorizontalProgressPercent)
            {
                if (scrollProgressPercent == 0.0)
                {
                    scrollPosition = 0;
                } else {
                    scrollPosition = scrollPosition +1;
                }
                View view = flingContainer.getSelectedView();
                if(view==null)
                    return;

                if (isHorizontal == 1 && scrollHorizontalProgressPercent < (-1.7))
                {
                    view.findViewById(R.id.super_like_indicator).setAlpha(1);
                    view.findViewById(R.id.item_swipe_right_indicator).setAlpha(0);
                    view.findViewById(R.id.item_swipe_left_indicator).setAlpha(0);

                } else if (scrollProgressPercent < -0.1 && isHorizontal == 0)
                {
                    view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                    view.findViewById(R.id.super_like_indicator).setAlpha(0);
                    view.findViewById(R.id.item_swipe_left_indicator).setAlpha(0);
                } else if (scrollProgressPercent > 0.1 && isHorizontal == 0) {
                    view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
                    view.findViewById(R.id.super_like_indicator).setAlpha(0);
                    view.findViewById(R.id.item_swipe_right_indicator).setAlpha(0);
                } else {
                    view.findViewById(R.id.super_like_indicator).setAlpha(0);
                    view.findViewById(R.id.item_swipe_right_indicator).setAlpha(0);
                    view.findViewById(R.id.item_swipe_left_indicator).setAlpha(0);
                }
            }
        });
        adapter.setPareContainer(flingContainer);
        flingContainer.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        /*
         * Custom viw listener*/
        boost_view.setClickListener(() ->{
            //presenter.showBoostDialog();
            homePresenter.checkForProfileBoost();
        });
        rewind_view.setClickListener(this::doRewind);
        dislike_view.setClickListener(this::doDislike);
        like_view.setClickListener(this::doLike);
        super_like_view.setClickListener(this::superLikeByButton);
        /*
         *Initializing the Listener */
        presenter.initListener();
    }


    @Override
    public void showError(String error)
    {}
    @Override
    public void showMessage(String message) {}

    @Override
    public void notifyDataChanged()
    {
        flingContainer.notifyi_data_setChanged();
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onLikeEventError(UserItemPojo temp)
    {
        userList.add(0,temp);
        notifyDataChanged();
        listPresenter.updateDataChanged();
    }


    @Override
    public void showLoadingView()
    {
        discoveryPresenter.showSearchView();
    }

    @Override
    public void adapterClickListener(DeckCardItemClicked itemClicked)
    {
        adapter.setItemClickListener(itemClicked);
    }

    @Override
    public void doRewind(){
        if(!userList.isEmpty()) {
            UserItemPojo temp = userList.get(0);
            /*
             * not letting rewinds the ads by button
             */
            if(temp.isAdView())
                return;
            if(!temp.isAdView())
                discoveryPresenter.onRewind();
        }
    }

    @Override
    public void doLike()
    {
        UserItemPojo temp = userList.get(0);
        /*
         * not letting like the ads by button
         */
//        if(temp.isAdView())
//            return;
        flingContainer.getTopCardListener().selectRight();
        View view = flingContainer.getSelectedView();
        if(view != null) {
            view.findViewById(R.id.background).setAlpha(0);
            view.findViewById(R.id.item_swipe_right_indicator).setAlpha(0);
            view.findViewById(R.id.item_swipe_left_indicator).setAlpha(1);
            view.findViewById(R.id.super_like_indicator).setAlpha(0);
        }
        else{
            showError(activity.getString(R.string.selected_view_null_msg));
        }
    }

    @Override
    public void doDislike()
    {
        UserItemPojo temp = userList.get(0);
        /*
         * not letting dislike the ads by button
         */
        if(temp.isAdView())
            return;
        flingContainer.getTopCardListener().selectLeft();
        View view = flingContainer.getSelectedView();
        if(view != null) {
            view.findViewById(R.id.background).setAlpha(0);
            view.findViewById(R.id.item_swipe_right_indicator).setAlpha(1);
            view.findViewById(R.id.item_swipe_left_indicator).setAlpha(0);
            view.findViewById(R.id.super_like_indicator).setAlpha(0);
        }
        else{
            showError(activity.getString(R.string.selected_view_null_msg));
        }
    }

    @Override
    public void doSuperLike()
    {
        UserItemPojo temp = userList.get(0);

        /*
         * not letting superLike the ads by button
         */
//        if(temp.isAdView())
//            return;
        isSuperLikedByButton = true;
        flingContainer.getTopCardListener().selectUpper();
        View view = flingContainer.getSelectedView();
        if(view != null) {
            view.findViewById(R.id.background).setAlpha(0);
            view.findViewById(R.id.item_swipe_right_indicator).setAlpha(0);
            view.findViewById(R.id.item_swipe_left_indicator).setAlpha(0);
            view.findViewById(R.id.super_like_indicator).setAlpha(1);
        }
        else{
            showError(activity.getString(R.string.selected_view_null_msg));
        }
    }

    @Override
    public void doLoadMore()
    {
        discoveryPresenter.getUsers();
    }

    @Override
    public void openBoostDialog()
    {
        homePresenter.openBoostDialog();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(!presenter.onHandelActivityResult(requestCode,resultCode,data))
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void openUerProfile(String data,View view)
    {
        Intent intent=new Intent(activity,UserProfilePage.class);
        Bundle intent_data=new Bundle();
        intent_data.putString(UserProfilePage.USER_DATA,data);
        intent.putExtras(intent_data);
        this.startActivityForResult(intent,AppConfig.PROFILE_REQUEST);
    }

    @Override
    public void superLikeByButton() {
        //discoveryPresenter.loadSpendCoinDialog(true);
        UserItemPojo temp = userList.get(0);
        if(temp!= null)
            presenter.checkWalletForSuperlike(temp);
    }

    @Override
    public void superLikeBySwipe(UserItemPojo userItemPojo){
        if(userItemPojo != null)
            presenter.checkWalletForSuperlikeBySwipe(userItemPojo);
    }

    @Override
    public void openChat(UserItemPojo userItemPojo) {
        discoveryPresenter.openChat(userItemPojo);
    }

    @Override
    public void setChatListNeedToUpdate(boolean yes) {
        homePresenter.setNeedToUpdateChat(yes);
    }

    @Override
    public void showBoostViewCounter(boolean show, String viewsText) {
        if(show) {
            tvBoostCounter.setText(viewsText);
            if (pbBoostProgress.getVisibility() == View.GONE) {
                pbBoostProgress.setVisibility(View.VISIBLE);
                homeAnimation.startPopUpAnimation(rlBoostView);
            }
        }
        else{
            if (pbBoostProgress.getVisibility() == View.VISIBLE) {
                pbBoostProgress.setVisibility(View.GONE);
                homeAnimation.startPopDownAnimation(rlBoostView);
            }
        }
    }

    @Override
    public void startCoinAnimation() {
        homeAnimation.startCoinAnimation(tvCoin, flCoinOne, flCoinTwo, flCoinThree);
    }

    @Override
    public void onPause() {
        super.onPause();
        //discoveryPresenter.saveCurrentBoostViewCount(0);
    }

    @Override
    public void launchCoinWallet() {
        Intent intent = new Intent(activity,AddCoinActivity.class);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @Override
    public void callSuperLikeApi(UserItemPojo userItemPojo) {
        discoveryPresenter.doSuperLike(userItemPojo);
    }

    @Override
    public void notifyFirstItem() {
        flingContainer.notifyi_data_setChanged();
        adapter.notifyDataSetChanged();
    }
}
