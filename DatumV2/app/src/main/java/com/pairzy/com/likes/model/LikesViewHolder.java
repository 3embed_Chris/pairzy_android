package com.pairzy.com.likes.model;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LikesViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.LikesUserProfile)
    SimpleDraweeView profilePic;

    @BindView(R.id.LikesUserText)
    TextView profileName;



    public LikesViewHolder(@NonNull View itemView, TypeFaceManager typeFaceManager) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}
