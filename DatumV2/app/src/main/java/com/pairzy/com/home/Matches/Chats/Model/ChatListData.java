package com.pairzy.com.home.Matches.Chats.Model;

import java.io.Serializable;

public class ChatListData implements Serializable
{
    private boolean isForBoost = false;

    private String initiatedBy ="";

    private String chatType ="";

    private String dTime ="";

    private String payload ="";

    private String senderId ="";

    private String messageType ="" ;

    private String timestamp ="";

    private String recipientId ="";

    private String secretId ="";

    private String receiverId = "";

    private String profilePic ="";

    private String messageId ="";

    private int totalUnread = 0;

    private int isMatchedUser = 1;


    private String chatId ="";
    private String firstName = "Xyz";
    private int onlineStatus = 0;
    private int status = 0;

    private boolean initiated = true, groupChat = true;
    private String receiverIdentifier ="";

    private int isSupperLikedMe = 1;
    private boolean hasDefaultMessage = true;

    private int isBlocked = 0;

    private int isBlockedByMe = 0;

    public String getInitiatedBy ()
    {
        return initiatedBy;
    }

    public void setInitiatedBy (String initiatedBy)
    {
        this.initiatedBy = initiatedBy;
    }

    public String getChatType ()
    {
        return chatType;
    }



    public void setChatType (String chatType)
    {
        this.chatType = chatType;
    }

    public String getDTime ()
    {
        return dTime;
    }

    public void setDTime (String dTime)
    {
        this.dTime = dTime;
    }

    public String getPayload ()
    {
        return payload;
    }

    public void setPayload (String payload)
    {
        this.payload = payload;
    }

    public String getSenderId ()
    {
        return senderId;
    }

    public void setSenderId (String senderId)
    {
        this.senderId = senderId;
    }

    public String getMessageType ()
    {
        return messageType;
    }

    public void setMessageType (String messageType)
    {
        this.messageType = messageType;
    }

    public String getTimestamp ()
    {
        return timestamp;
    }

    public void setTimestamp (String timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getRecipientId ()
    {
        return recipientId;
    }

    public void setRecipientId (String recipientId)
    {
        this.recipientId = recipientId;
    }

    public String getSecretId ()
    {
        return secretId;
    }

    public void setSecretId (String secretId)
    {
        this.secretId = secretId;
    }

    public String getReceiverId ()
    {
        return receiverId;
    }

    public void setReceiverId (String receiverId)
    {
        this.receiverId = receiverId;
    }

    public String getProfilePic ()
    {
        return profilePic;
    }

    public void setProfilePic (String profilePic)
    {
        this.profilePic = profilePic;
    }

    public String getMessageId ()
    {
        return messageId;
    }

    public void setMessageId (String messageId)
    {
        this.messageId = messageId;
    }

    public int getTotalUnread ()
    {
        return totalUnread;
    }

    public void setTotalUnread (int totalUnread)
    {
        this.totalUnread = totalUnread;
    }

    public int getIsMatchedUser ()
    {
        return isMatchedUser;
    }

    public void setIsMatchedUser (int isMatchedUser)
    {
        this.isMatchedUser = isMatchedUser;
    }

    public String getChatId ()
    {
        return chatId;
    }

    public void setChatId (String chatId)
    {
        this.chatId = chatId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [initiatedBy = "+initiatedBy+", chatType = "+chatType+", dTime = "+dTime+", payload = "+payload+", senderId = "+senderId+", messageType = "+messageType+", timestamp = "+timestamp+", recipientId = "+recipientId+", secretId = "+secretId+", receiverId = "+receiverId+", profilePic = "+profilePic+", messageId = "+messageId+", totalUnread = "+totalUnread+", isMatchedUser = "+isMatchedUser+", chatId = "+chatId+"]";
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public int getOnlineStatus() {
        return onlineStatus;
    }

    public boolean isInitiated() {
        return initiated;
    }

    public void setInitiated(boolean initiated) {
        this.initiated = initiated;
    }

    public String getReceiverIdentifier() {
        return receiverIdentifier;
    }

    public void setReceiverIdentifier(String receiverIdentifier) {
        this.receiverIdentifier = receiverIdentifier;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isGroupChat() {
        return groupChat;
    }

    public void setGroupChat(boolean groupChat) {
        this.groupChat = groupChat;
    }

    public int getSuperlikedMe() {
        return isSupperLikedMe;
    }

    public void setSuperlikedMe(int superlikedMe) {
        isSupperLikedMe = superlikedMe;
    }

    public void setOnlineStatus(int onlineStatus) {
        this.onlineStatus = onlineStatus;
    }


    public boolean isForBoost() {
        return isForBoost;
    }

    public void setForBoost(boolean forBoost) {
        isForBoost = forBoost;
    }

    public void setHasDefaultMessage(boolean hasDefaultMessage) {
        this.hasDefaultMessage = hasDefaultMessage;
    }

    public boolean isHasDefaultMessage() {
        return hasDefaultMessage;
    }

    public int getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(int isBlocked) {
        this.isBlocked = isBlocked;
    }

    public int getIsBlockedByMe() {
        return isBlockedByMe;
    }

    public void setIsBlockedByMe(int isBlockedByMe) {
        this.isBlockedByMe = isBlockedByMe;
    }

}