package com.pairzy.com.home.Matches.PostFeed;
import android.content.Intent;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Matches.PostFeed.Model.PostListAdapter;
import com.pairzy.com.home.Matches.PostFeed.Model.PostListPojo;

/**
 * @since  3/17/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface PostFeedFragContract
{
    interface View extends BaseView
    {
        void showLoading();
        void showData();
        void showError(int errorId);
        void showError(String error);
        void showMessage(String message);
        void showNetworkError(String error);
        void emptyData();
        void launchUserProfile(DateListPojo upcomingItemPojo);
        void notifyDataAdapter(int position);
        void launchWriteCommit(String postId);
        void launchCommentView(String postId);
        void launchLikeView(String postId);
        void launchChatScreen(Intent intent);
        void notifyDataAdapter();
        void pausePlayer();
        void updatePostfeedList();
    }

    interface Presenter extends BasePresenter
    {
        void setAdapterCallBack(PostListAdapter newsFeedAdapter);
        boolean checkDateExist();
        void parseDateData(int requestCode, int resultCode, Intent data);
        void checkAndLoadPost();
        void likeDislikeUserPost(double type, String postId);
        void launchChat(PostListPojo postListPojo);
        void deletePost(String postId);
        void observeMatchAndUnmatchUser();
    }
}
