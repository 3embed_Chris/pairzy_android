package com.pairzy.com.selectLanguage;

import android.app.Activity;

import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.selectLanguage.model.AppLanguageAdapter;
import com.pairzy.com.selectLanguage.model.LanguageItem;
import com.pairzy.com.util.DeleteAccountDialog.DeleteAccountDialog;
import com.pairzy.com.util.LogoutDialog.LogoutDialog;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.progressbar.LoadingProgress;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * <h>PassportUtilModule class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

@Module
public class SelectLanguageUtil {

    @ActivityScoped
    @Provides
    LogoutDialog provideLogoutDialog(Activity activity, TypeFaceManager typeFaceManager){
        return new LogoutDialog(activity,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    DeleteAccountDialog provideDeleteAccountDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility){
        return new DeleteAccountDialog(activity,typeFaceManager,utility);
    }

    @ActivityScoped
    @Provides
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }

    @ActivityScoped
    @Provides
    ArrayList<LanguageItem> languageItems(Activity activity){
        ArrayList<LanguageItem> languageItems = new ArrayList<>();
        String languages[] = activity.getResources().getStringArray(R.array.app_languages);
        String languageCodes[] = activity.getResources().getStringArray(R.array.app_language_code);
        for(int i = 0; i < languages.length;i++){
            languageItems.add(new LanguageItem(languages[i],languageCodes[i],false));
        }
        return languageItems;
    }
    @ActivityScoped
    @Provides
    AppLanguageAdapter provideAppLanguageAdapter(ArrayList<LanguageItem> languageItems, TypeFaceManager typeFaceManager, Utility utility){
        return new AppLanguageAdapter(languageItems,typeFaceManager,utility);
    }
}
