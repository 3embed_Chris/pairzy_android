package com.pairzy.com.boostDetail;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.boostDetail.model.BoostDetailSlideAdapter;
import com.pairzy.com.boostDetail.model.CoinPlanAdapter;
import com.pairzy.com.coinWallet.CoinWalletActivity;
import com.pairzy.com.util.CustomObserver.CoinBalanceObserver;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.boostDialog.CustomScroller;
import com.pairzy.com.util.boostDialog.Slide;
import com.pairzy.com.util.boostDialog.SlideOffPagerTransformer;
import com.pairzy.com.util.boostDialog.SlideTimerTask;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Timer;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h>BoostDetail activity</h>
 *
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public class BoostDetailActivity extends BaseDaggerActivity implements BoostDetailContract.View,SwipeRefreshLayout.OnRefreshListener{

    @Inject
    BoostDetailPresenter presenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    BoostDetailSlideAdapter slideAdapter;
    @Inject
    ArrayList<Slide> slideArrayList;
    @Inject
    Activity activity;
    @Inject
    LinearLayoutManager linearLayoutManager;
    @Inject
    CoinPlanAdapter coinPlanAdapter;
    @Inject
    CoinBalanceObserver coinBalanceObserver;

    @BindView(R.id.tv_page_title)
    TextView tvPageTitle;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.btn_subscribe)
    Button btnSubscribe;
    @BindView(R.id.header_title_tv)
    TextView tvHeaderRowTitle;
    @BindView(R.id.header_msg_tv)
    TextView tvHeaderRowMsg;

    @BindView(R.id.parent_layout)
    RelativeLayout parentLayout;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.loading_progress)
    ProgressBar pbLoading;
    @BindView(R.id.loading_text)
    TextView tvLoading;
    @BindView(R.id.error_icon)
    ImageView ivErrorIcon;
    @BindView(R.id.error_msg)
    TextView tvErrorMsg;
    @BindView(R.id.empty_data)
    RelativeLayout rlEmptyData;
    @BindView(R.id.loading_view)
    RelativeLayout rlLoadingView;
    @BindView(R.id.no_more_data)
    TextView tvNoMoreDataTitle;
    @BindView(R.id.empty_details)
    TextView tvEmptyDetail;
    @BindView(R.id.recycler_coin_plan)
    RecyclerView recyclerCoinPlan;

    private Unbinder unbinder;
    private Timer timer;
    private SlideTimerTask slideTimerTask;
    private int duration = 550;
    private int currentPage = 0;
    private int pageSize = 0;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (currentPage == pageSize) {
                currentPage = 0;
            }
            viewPager.setCurrentItem(currentPage++, true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boost_detail);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        presenter.init();
        initView();
        presenter.loadCoinPlanIfRequired();
        presenter.loadSubsPlanIfRequired();
    }

    @OnClick(R.id.parent_layout)
    public void parentLayout(){}

    private void initView() {
        pageSize = slideArrayList.size();
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(slideAdapter);
        try {
            Field mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            mScroller.set(viewPager, new CustomScroller(viewPager.getContext(),new DecelerateInterpolator(),duration));
        } catch (Exception e) {
            e.printStackTrace();
        }
        timer = new Timer();
        slideTimerTask = new SlideTimerTask(runnable);
        timer.schedule(slideTimerTask,2000,3000);
        viewPager.setPageTransformer(true,new SlideOffPagerTransformer());

        swipeRefreshLayout.setColorSchemeResources(R.color.refresh2, R.color.refresh1,R.color.refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerCoinPlan.setLayoutManager(linearLayoutManager);
        recyclerCoinPlan.setHasFixedSize(true);
        recyclerCoinPlan.setNestedScrollingEnabled(false);
        recyclerCoinPlan.setAdapter(coinPlanAdapter);
        coinPlanAdapter.setClickCallback(presenter);
    }

    @Override
    public void applyFont() {
        tvPageTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvHeaderRowTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvHeaderRowMsg.setTypeface(typeFaceManager.getCircularAirBook());
        btnSubscribe.setTypeface(typeFaceManager.getCircularAirBook());
        tvNoMoreDataTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvEmptyDetail.setTypeface(typeFaceManager.getCircularAirBook());
    }

//    @OnClick(R.id.coin_button_one)
//    public void firstRowCoinBtn(){
//        Intent intent = new Intent(activity, AddCoinActivity.class);
//        activity.startActivity(intent);
//        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
//
//    }

    @OnClick(R.id.btn_subscribe)
    public void launchBoostDialog(){
        presenter.launchBoostDialog();
    }

    @OnClick(R.id.close_button)
    public void close(){
        onBackPressed();
    }


    @Override
    public void onBackPressed() {
        this.finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        super.onBackPressed();
    }

    @Override
    public void showError(String errorMsg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+errorMsg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(String msg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+msg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(int msgId) {
        String message=getString(msgId);
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showLoading() {
        tvErrorMsg.setVisibility(View.GONE);
        ivErrorIcon.setVisibility(View.GONE);
        tvLoading.setVisibility(View.VISIBLE);
        pbLoading.setVisibility(View.VISIBLE);
        rlEmptyData.setVisibility(View.GONE);
        recyclerCoinPlan.setVisibility(View.GONE);
        rlLoadingView.setVisibility(View.VISIBLE);

    }

    @Override
    public void showData() {
        rlEmptyData.setVisibility(View.GONE);
        recyclerCoinPlan.setVisibility(View.VISIBLE);
        rlLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyData() {
        rlEmptyData.setVisibility(View.VISIBLE);
        recyclerCoinPlan.setVisibility(View.GONE);
        rlLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showNetworkError(String errorMsg) {
        tvErrorMsg.setVisibility(View.VISIBLE);
        tvErrorMsg.setText(errorMsg);
        ivErrorIcon.setVisibility(View.VISIBLE);
        tvLoading.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        recyclerCoinPlan.setVisibility(View.GONE);
        rlEmptyData.setVisibility(View.GONE);
        rlLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void enableSubsButton(boolean enable) {
        btnSubscribe.setEnabled(enable);
    }

    @Override
    public void launchAddCoinScreen(Intent intent) {
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @Override
    public void launchCoinWallet() {
        Intent intent = new Intent(activity, CoinWalletActivity.class);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        if(slideTimerTask != null)
            slideTimerTask.cancel();
        if(timer != null)
            timer.cancel();
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        presenter.getCoinsPlan();
        presenter.getSubsPlan();
    }
}
