package com.pairzy.com.register.Userdob;
import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * @since  2/16/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface DobFrgBuilder
{
    @FragmentScoped
    @Binds
   DobFragment getEmailFragment(DobFragment dobFragment);

    @FragmentScoped
    @Binds
    DobContract.Presenter taskPresenter(DobFrgPresenter presenter);
}
