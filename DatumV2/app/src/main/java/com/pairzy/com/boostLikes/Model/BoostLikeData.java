package com.pairzy.com.boostLikes.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 7/9/18.
 */

public class BoostLikeData{
    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("contactNumber")
    @Expose
    private String contactNumber;

    @SerializedName("countryCode")
    @Expose
    private String countryCode;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("firstName")
    @Expose
    private String firstName;

    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    private boolean loading = false;
    private boolean loadingFailed = false;
    public int item_actual_pos = -1;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public boolean isLoading() {
        return loading;
    }

    public boolean isLoadingFailed() {
        return loadingFailed;
    }
}
