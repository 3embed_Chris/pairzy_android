package com.pairzy.com.selectLanguage.model;

public interface LanguageAdapterCallback {
    void onLanguageClick(int position);
}
