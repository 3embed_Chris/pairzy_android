package com.pairzy.com.home.Matches.PostFeed.Model;

import android.view.View;

interface ItemViewCallBack
{
    void onViewItemCallBack(View view, int position);
}
