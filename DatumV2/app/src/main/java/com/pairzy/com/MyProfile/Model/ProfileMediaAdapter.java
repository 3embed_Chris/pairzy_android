package com.pairzy.com.MyProfile.Model;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.pairzy.com.MyProfile.ImageItem.ProfileImageItemFrg;
import com.pairzy.com.MyProfile.VideoItem.ProfileVideoItemFrg;
import com.pairzy.com.editProfile.Model.ProfilePhotoAdapter;

import java.util.ArrayList;
/**
 * <h2>ProfileMediaAdapter</h2>
 * <P>
 *     User profile media adapter.
 * </P>
 * @since  4/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ProfileMediaAdapter extends FragmentStatePagerAdapter
{
    private static final String TAG = ProfilePhotoAdapter.class.getSimpleName();

    private ProfileVideoItemFrg videoItemFrg=null;
    private ArrayList<ProfileMediaPojo> item_list;

    public void playVideo(String tag){
        if(videoItemFrg != null)
            videoItemFrg.onPlay(tag);
    }

    public void pauseVideo(String tag){
        if(videoItemFrg != null){
            videoItemFrg.onPause();
        }
    }

    public ProfileMediaAdapter(FragmentManager fm, ArrayList<ProfileMediaPojo> item_list)
    {
        super(fm);
        this.item_list=item_list;
    }

    @Override
    public Fragment getItem(int position)
    {
        ProfileMediaPojo temp=item_list.get(position);
        if(temp.isVideo())
        {
            videoItemFrg=new ProfileVideoItemFrg();

            //temp.setVideo_url(temp.getVideo_url().replace("upload/","upload/ar_239:270/"));
            String uri = temp.getVideo_url();
            String thumb = temp.getVideo_thumbnail();
            if(uri != null)
                thumb = uri.replace(".mp4",".jpg").replace(".mov",".jpg");
            videoItemFrg.setMediaFile(thumb,temp.getVideo_url());
            return videoItemFrg;
        }else
        {
            ProfileImageItemFrg frg=new ProfileImageItemFrg();
            frg.setMediaFile(temp.getImage_url());
            return frg;
        }
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount()
    {
        return item_list.size();
    }
}
