package com.pairzy.com.UserPreference.CanditionChoice;
import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * <h2>ConChoiceBuilder</h2>
 * @since  2/21/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface ConChoiceBuilder
{
    @FragmentScoped
    @Binds
    ConChoiceFrg getConChoiceFragment(ConChoiceFrg conChoiceFrg);
    @FragmentScoped
    @Binds
    ConChoiceContract.Presenter taskPresenter(ConChoicePresenter presenter);
}
