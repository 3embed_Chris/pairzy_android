package com.pairzy.com.addCoin.model;

import android.text.TextUtils;
import android.util.Log;
import com.pairzy.com.BaseModel;
import com.pairzy.com.boostDetail.model.CoinPlan;
import com.pairzy.com.boostDetail.model.CoinPlanResponse;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.coinBalance.CoinResponse;
import com.pairzy.com.data.model.coinBalance.Coins;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.Utility;
import com.suresh.innapp_purches.SkuDetails;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * <h>AddCoinModel</h>
 * @author 3Embed.
 * @since 1/6/18.
 * @version 1.0.
 */
public class AddCoinModel extends BaseModel{

    @Inject
    ArrayList<CoinPlan> coinPlanList;

    @Inject
    CoinPlanListAdapter planListAdapter;
    @Inject
    Utility utility;
    @Inject
    CoinBalanceHolder coinBalanceHolder;

    @Inject
    AddCoinModel(){}


    public void parseCoinPlanList(String response)
    {
        try {
            coinPlanList.clear();
            Log.d("dsft2", ":"+response);
            CoinPlanResponse coinPlanResponse =  utility.getGson().fromJson(response,CoinPlanResponse.class);
            ArrayList<CoinPlan> coinPlans = coinPlanResponse.getData();
            if(coinPlans != null)
            {
                coinPlanList.addAll(coinPlans);
            }
        } catch (Exception e){}
    }

    public int getCoinPlanListSize() {
        return coinPlanList.size();
    }

    public boolean isCoinPlanEmpty() {
        return getCoinPlanListSize() == 0;
    }

    public ArrayList<CoinPlan> getCoinPlanList() {
        return coinPlanList;
    }

    public void notifyPlanListAdapter() {
        planListAdapter.notifyDataSetChanged();
    }

    public CoinPlan getCoinPlan(int position) {
        try {
            if (position < getCoinPlanListSize()) {
                return coinPlanList.get(position);
            }
        }catch (Exception ignored){}
        return null;
    }

    public void parseCoinBalance(String response)
    {
        try
        {
            CoinResponse coinResponse = utility.getGson().fromJson(response,CoinResponse.class);
            Coins coins = coinResponse.getData().getCoins();
            coinBalanceHolder.setCoinBalance(String.valueOf(coins.getCoin()));
        }catch (Exception e){}
    }


    public String getCoinBalance()
    {
        return coinBalanceHolder.getCoinBalance();
    }

    public void addToCoinBalance(Integer coin)
    {
        try
        {
            coinBalanceHolder.setCoinBalance(String.valueOf(Integer.parseInt(coinBalanceHolder.getCoinBalance()) + coin));
        }catch (Exception e){}
    }


    public List<String> collectsIds()
    {
        List<String> items=new ArrayList<>();
        for(CoinPlan item :coinPlanList)
        {
            items.add(item.getActualIdForAndroid());
        }
        return items;
    }

    public boolean isSubsPlanEmpty()
    {
        return  coinPlanList.isEmpty();
    }


    public void updateDetailsData(List<SkuDetails> details)
    {
        for(SkuDetails item:details)
        {
            Log.d("8ry", ":"+item.toString());
            String id=item.getProductId();
            for(CoinPlan product :coinPlanList)
            {
                Log.d("8ry", ":"+product.toString());
                if(id.equals(product.getActualIdForAndroid()))
                {
                    product.setPrice_text(item.getPriceText());
                    product.setCurrencySymbole(item.getCurrency());
                }
            }
        }
    }

    public void clearProductList()
    {
        coinPlanList.clear();
    }

    public Map<String, Object> getAddCoinToWalletBody(int coinValue) {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.ADD_COIN_TO_WALLET.COIN_AMOUNT,coinValue);
        return map;
    }

    public void updateCoinWalletBalanceBy(int coinValue) {
       String walletBalance =  coinBalanceHolder.getCoinBalance();
       if(TextUtils.isEmpty(walletBalance)){
           coinBalanceHolder.setCoinBalance(String.valueOf(coinValue));

       }else{
           int actualBalance = Integer.parseInt(walletBalance);
           actualBalance += coinValue;
           coinBalanceHolder.setCoinBalance(String.valueOf(actualBalance));
       }
    }
}
