package com.pairzy.com.selectLanguage;

import com.pairzy.com.selectLanguage.model.AppLanguageAdapter;
/**
 * <h>PassportContract interface</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public interface SelectLanguageContract {

    interface View{
        void applyFont();
        void finishActivity();
        void showSplashScreen();
        void notifyAdapter();
        void setNewLanguage(String selectedLanguage);
    }

    interface Presenter{
        void loadAppVersion();
        void init();
        void setAdapterCallback(AppLanguageAdapter appLanguageAdapter);
        void dispose();
        void selectTheCurrentLanguage();
    }
}