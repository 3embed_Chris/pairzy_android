package com.pairzy.com.register.ProfilePic;
import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * @since 1/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface ProfilePicBuilder
{
    @FragmentScoped
    @Binds
    ProfilePicFrg getEmailFragment(ProfilePicFrg profilePicFrg);

    @FragmentScoped
    @Binds
    ProfilePicContact.Presenter taskPresenter(ProfilePicFrgPresenter presenter);
}
