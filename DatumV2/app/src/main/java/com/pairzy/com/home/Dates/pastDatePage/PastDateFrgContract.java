package com.pairzy.com.home.Dates.pastDatePage;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;
import com.pairzy.com.home.Dates.pastDatePage.Model.PastDateAdapter;

/**
 * <h2>PastDateFrgContract</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface PastDateFrgContract
{
    interface View extends BaseView
    {
        void notifyAdapter();

        void doLoadMore();

        void showMessage(String s);

        void refreshList();

        void showError(String error);
        void showError(int errorId);

        void showNetworkError(String error);
        void emptyData();
        void showLoading();
        void launchUserProfile(PastDateListPojo pastDateListPojo);
    }

    interface Presenter extends BasePresenter
    {
        void setAdapterCallBack(PastDateAdapter pastDateAdapter);

        void doLoadMore(int positionView);

        void notifyPastdateAdapter();

        boolean checkDateExist();
    }
}
