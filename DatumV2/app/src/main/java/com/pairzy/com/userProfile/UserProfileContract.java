package com.pairzy.com.userProfile;
import android.content.Intent;
import androidx.viewpager.widget.ViewPager;

import com.androidinsta.com.ImageData;
import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.userProfile.Model.MomentsGridAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * <h2>UserProfileContract</h2>
 * <P>
 *     Use page skeleton for this page.
 * </P>
 * @since  3/26/2018.
 * @author 3Embed.
 * @version 2.0.
 */
public interface UserProfileContract
{
    interface View extends BaseView
    {
        void intPageChangeListener(ViewPager.OnPageChangeListener listener);
        void addChildView(ArrayList<android.view.View> views);
        void movePagerPosition(int position);
        void updateSegmentBar(int total, int selected);
        void updatePrimeryData(String name, String school_name, String distance, int online_status);
        void addLoadingView(android.view.View view);
        void showError(String message);
        void showMessage(String message);
        void showMessage(int msgId);
        void closePage();
        void updateUserAge(String details);
        void shareDeepLink(String deepLink);
        void hideUserDistance();
        void showDistance(String away_text);
        void showUserSchool(String school);
        void hideUserSchool();
        void showOnlineStatus(boolean userOnline);
        void showButtonsState(boolean unliked, boolean superliked, boolean liked,boolean isMatched);
        void launchChatScreen(Intent intent);
        void showWorkIcon();
        void hideInstadetails();
        void showInsaPost(List<List<ImageData>> lists);
        void handleInstagramDetails();
        void setReportText(String userName);
        void superLikeUser();
        void showAboutText(String aboutText);
        void initLikeUser();
        void initDislikeUser();
        void launchCoinWallet();

        void showUserLocation(String userLocationString, boolean locationNeedToShow);

        void showMomentData(List<MomentsData> momentList);
        void launchMomentScreen(ArrayList<MomentsData> momentsDataList, int position);
        void launchMomentVerticalScreen(ArrayList<MomentsData> momentsDataList, int position);
    }


    interface Presenter extends BasePresenter<View>
    {
        void initDataChangeObserver();
        void updateInstagramDetails(String id,String name ,String token);
        void checkIntent(Intent intent);
        void getUserDetails();
        void getUserInstaPost();
        void getUserDetails(String profileId);
        void initListener();
        void showOptionAlert();
        String collectUserId();
        void launchBoostDialog();
        void openChat();
        void shareProfile();
        void reportProfile();
        String getUserInstName();
        String getUserInstaId();
        void getOtherUserInstPost(String user_id,String userInstaToken);
        void doInstagramLogin();
        String getUserName();
        void setOnRetryCallback();
        void superLikeThisUser();
        void checkForLikeAvail();
        void checkForInternetThenDislike();
        String getUserInstaToken();
        void loadMomentScreen();
        void setMomentClickCallback(MomentsGridAdapter momentsAdapter);
    }
}
