package com.pairzy.com.home.MakeMatch.swipeCardModel;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.pairzy.com.R;
import com.pairzy.com.util.CustomView.SquizeButton;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PairMakerViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.dislike_view)
    SquizeButton dislike;

    @BindView(R.id.like_view)
    SquizeButton like;

    TextView userOneName,userOneAge,userTwoName,userTwoAge;
    SimpleDraweeView userOne,userTwo;
    public ImageView item_swipe_left_indicator,item_swipe_right_indicator;

    AdapterItemCallback callback;

    public PairMakerViewHolder(@NonNull View itemView, AdapterItemCallback callback) {
        super(itemView);
        ButterKnife.bind(this,itemView);

        userTwoName=itemView.findViewById(R.id.tv_profile_two_name);
        userTwoAge=itemView.findViewById(R.id.tv_profile_two_age);
        userTwo=itemView.findViewById(R.id.sdv_profile_two);

        userOneName=itemView.findViewById(R.id.tv_profile_one_name);
        userOneAge=itemView.findViewById(R.id.tv_profile_one_age);
        userOne=itemView.findViewById(R.id.sdv_profile_one);

        item_swipe_left_indicator=itemView.findViewById(R.id.item_swipe_left_indicator);
        item_swipe_right_indicator=itemView.findViewById(R.id.item_swipe_right_indicator);
        this.callback=callback;
        dislike.setClickListener(this::doDislike);
        like.setClickListener(this::doLike);
    }

    private void doLike() {
        if(callback!=null)
            callback.rightSwipe(this.getAdapterPosition());
    }

    private void doDislike() {
        if(callback!=null)
            callback.leftSwipe(this.getAdapterPosition());
    }
}
