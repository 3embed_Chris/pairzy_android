package com.pairzy.com.mobileverify.otpreceive_error;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.mobileverify.AnimatorHandler;
import com.pairzy.com.mobileverify.MobileVerifyContract;
import com.pairzy.com.mobileverify.otp.Otp_Fragment;
import com.pairzy.com.mobileverify.result.ResultFragment;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import java.util.Locale;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * A simple {@link Fragment} subclass.
 */
public class ResentOtpFragment extends DaggerFragment implements ResentOtpContract.View
{
    @Inject
    AnimatorHandler animatorHandler;
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String COUNTRY_CODE= "countryCode";
    @Inject
    Activity activity;

    @Inject
    Utility utility;

    @Inject
    TypeFaceManager typeFaceManager;

    @Inject
    ResendOtpPresenter presenter;

    @Inject
    MobileVerifyContract.Presenter  main_presenter;

    @Inject
    Otp_Fragment otp_fragment;

    @BindView(R.id.titleOne)
    TextView titleOne;

    @BindView(R.id.titleTwo)
    TextView titleTwo;

    @BindView(R.id.tvPhnoTitle)
    TextView tvPhnoTitle;

    @BindView(R.id.tvPhnoNumber)
    TextView tvPhnoNumber;

    @BindView(R.id.tvInboxText)
    TextView tvInboxText;

    @BindView(R.id.tvTimer)
    TextView tvTimer;

    @BindView(R.id.btnResendOtp)
    Button btnResendOtp;

    private Unbinder unbinder;

    private String country_code;
    private String phone_number;

    @Inject
    public ResentOtpFragment() {}


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        try
        {
            Bundle bundle=getArguments();
            assert bundle != null;
            country_code=bundle.getString(COUNTRY_CODE);
            phone_number=bundle.getString(MOBILE_NUMBER);

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_resent_otp, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
         unbinder=ButterKnife.bind(this,view);
        tvPhnoNumber.setText(String.format(Locale.ENGLISH,"%s%s", country_code, phone_number));
        applyingFont();
    }

    /*
    * applying the fonts.*/
    private void applyingFont()
    {
        titleOne.setTypeface(typeFaceManager.getCircularAirBold());
        titleTwo.setTypeface(typeFaceManager.getCircularAirBold());
        tvPhnoTitle.setTypeface(typeFaceManager.getCircularAirBook());
        tvPhnoNumber.setTypeface(typeFaceManager.getCircularAirBook());
        tvInboxText.setTypeface(typeFaceManager.getCircularAirBook());
        tvTimer.setTypeface(typeFaceManager.getCircularAirBold());
        btnResendOtp.setTypeface(typeFaceManager.getCircularAirBold());
    }


    @Override
    public void onResume()
    {
        super.onResume();
        titleOne.requestFocus();
        presenter.takeView(this);
        utility.closeSpotInputKey(activity,titleOne);
        presenter.listenTimer();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        presenter.dropView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(unbinder!=null)
            unbinder.unbind();
    }


    @Override
    public void showMessage(String message)
    {
      main_presenter.showMessage(message);
    }

    @OnClick(R.id.layoutPhnoAgain)
    public void openEdit()
    {
        presenter.doEditPhno();
    }

    @OnClick(R.id.btnResendOtp)
    public void resentOtpAgain()
    {
     presenter.doSmsAgain();
    }

    @OnClick(R.id.ibClose)
    void onBack()
    {
        activity.onBackPressed();
    }

    @Override
    public void activeSMSButton(boolean isToActive)
    {

    }

    @Override
    public void verifyNumber()
    {
        main_presenter.openPhnoFrgAgain();
    }

    @Override
    public void checkMsgInbox()
    {
        Bundle data=new Bundle();
        data.putString(Otp_Fragment.COUNTRY_CODE,country_code);
        data.putString(Otp_Fragment.MOBILE_NUMBER,phone_number);
        ResultFragment resultFragment=new ResultFragment();
        resultFragment.setArguments(data);
        main_presenter.moveFragment(resultFragment,true);
    }

    @Override
    public void sendSMSAgain()
    {
        Bundle data=new Bundle();
        data.putString(Otp_Fragment.COUNTRY_CODE,country_code);
        data.putString(Otp_Fragment.MOBILE_NUMBER,phone_number);
        data.putString(ResultFragment.CODE,ResultFragment.REQUEST_OTP_OPERATION);
        ResultFragment resultFragment=new ResultFragment();
        resultFragment.setArguments(data);
        main_presenter.moveFragment(resultFragment,true);
    }

    @Override
    public void doPhnoCall()
    {
        Bundle data=new Bundle();
        data.putString(Otp_Fragment.COUNTRY_CODE,country_code);
        data.putString(Otp_Fragment.MOBILE_NUMBER,phone_number);
        ResultFragment resultFragment=new ResultFragment();
        resultFragment.setArguments(data);
        main_presenter.moveFragment(resultFragment,true);
    }

    @Override
    public void updateTimer(String time)
    {
        if(btnResendOtp.getVisibility()==View.VISIBLE)
            btnResendOtp.setVisibility(View.GONE);

        if(tvTimer.getVisibility()==View.GONE)
            tvTimer.setVisibility(View.VISIBLE);

        tvTimer.setText(time);
    }

    @Override
    public void showResentButton()
    {
        setResentButtonVisibility();
    }

    @Override
    public void sendFacebookNotification()
    {
        Bundle data=new Bundle();
        data.putString(Otp_Fragment.COUNTRY_CODE,country_code);
        data.putString(Otp_Fragment.MOBILE_NUMBER,phone_number);
        ResultFragment resultFragment=new ResultFragment();
        resultFragment.setArguments(data);
        main_presenter.moveFragment(resultFragment,true);
    }

    /*
    * Resent button visibility.*/
    private void setResentButtonVisibility()
    {
        tvTimer.setVisibility(View.GONE);
        Animation animation=animatorHandler.getShakingAnimation();
        btnResendOtp.setVisibility(View.VISIBLE);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation) {
                btnResendOtp.setTextColor(ContextCompat.getColor(activity,R.color.colorAccent));
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        btnResendOtp.setAnimation(animation);
        animation.start();
    }
}
