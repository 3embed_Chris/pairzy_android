package com.pairzy.com.home.MakeMatch.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MakeMatchPOJO {
//    @SerializedName("recordTotal")
//    @Expose
//    private Integer recordTotal;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<MakeMatchDataPOJO> data = null;

//    public Integer getRecordTotal() {
//        return recordTotal;
//    }
//
//    public void setRecordTotal(Integer recordTotal) {
//        this.recordTotal = recordTotal;
//    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<MakeMatchDataPOJO> getData() {
        return data;
    }

    public void setData(ArrayList<MakeMatchDataPOJO> data) {
        this.data = data;
    }
}
