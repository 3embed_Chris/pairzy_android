package com.pairzy.com.MyProfile;
import android.app.Activity;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;

import com.pairzy.com.MyProfile.ImageItem.ImageItemBuilder;
import com.pairzy.com.MyProfile.ImageItem.ProfileImageItemFrg;
import com.pairzy.com.MyProfile.VideoItem.ProfileVideoItemFrg;
import com.pairzy.com.MyProfile.VideoItem.VideoItemBuilder;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.dagger.FragmentScoped;

import javax.inject.Named;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

/**
 *<h2>MyProfileBuilder</h2>
 * <P>
 *     This is the dagger builder class for the MyProfile page
 *     to build the page with the dagger.
 * </P>
 * @author 3Embed.
 * @version 1.0.*/
@Module
public abstract class MyProfileBuilder
{
    public static final String PROFILE_FRAGMENT_MANAGER = "MyProfile.FragmentManager";
    @ActivityScoped
    @Binds
    abstract Activity getActivity(MyProfilePage activity);

    @ActivityScoped
    @Binds
    abstract MyProfilePageContract.View getProfileView(MyProfilePage activity);

    @ActivityScoped
    @Binds
    abstract MyProfilePageContract.Presenter getUpdateProfilePresenter(MyProfilePagePresenter presenter);

    @Provides
    @Named(PROFILE_FRAGMENT_MANAGER)
    @ActivityScoped
    static FragmentManager activityFragmentManager(Activity activity)
    {
        return ((AppCompatActivity)activity).getSupportFragmentManager();
    }


    @FragmentScoped
    @ContributesAndroidInjector(modules = {ImageItemBuilder.class})
    abstract ProfileImageItemFrg getProfileImageItemFrg();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {VideoItemBuilder.class})
    abstract ProfileVideoItemFrg getProfileVideoItemFrg();
}
