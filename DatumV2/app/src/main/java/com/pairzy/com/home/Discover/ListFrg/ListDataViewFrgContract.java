package com.pairzy.com.home.Discover.ListFrg;
import android.content.Intent;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.home.Discover.Model.AdapterCallback;
import com.pairzy.com.home.Discover.Model.UserItemPojo;
/**
 * <h2>ListDataViewFrgContract</h2>
 * <P>
 *
 * </P>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface ListDataViewFrgContract
{
    interface View extends BaseView
    {
        void showError(String error);
        void showMessage(String message);
        void showLoadingView();
        void doLoadMore();
        void notifyDataChanged(boolean isScrollTop);
        void onLike(int position);
        void onDislike(int position);
        void onSuperLike(int position);
        void openUserProfile(String data, android.view.View view);
        void setAdapterListen(AdapterCallback callback);
        void openBoost();
        void updateBothViewData();
        void openChat(UserItemPojo userItemPojo);
        void setChatListNeedToUpdate(boolean yes);
        void showBoostViewCounter(boolean show, int viewCount);
        void startCoinAnimation();
        void launchCoinWallet();
    }

    interface Presenter extends BasePresenter<View>
    {
        void initListener();
        void updateDataChanged();
        void doLoadMore(int currentPos);
        void isEmpty();
        boolean onHandelActivityResult(int requestCode, int resultCode, Intent data);
        void revertAction(UserItemPojo item);
        void initiateSuperlike();
        int getCurrentProfilePosition();
        void showBoostViewCounter(boolean show, int viewCount);
        void startCoinAnimation();
    }
}
