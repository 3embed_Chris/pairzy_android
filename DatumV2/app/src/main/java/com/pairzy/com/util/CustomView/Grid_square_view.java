package com.pairzy.com.util.CustomView;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class Grid_square_view extends RelativeLayout
{
    public Grid_square_view(Context context)
    {
        super(context);
    }

    public Grid_square_view(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public Grid_square_view(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public Grid_square_view(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
