package com.pairzy.com.util.DatumCallbacks;

/**
 * @since  3/20/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface ListItemClick
{
    void onClicked(int id, int position);
}
