package com.pairzy.com.home.Matches;

import javax.inject.Inject;

public class MatchesPresenter implements MatchesContract.Presenter{

    private MatchesContract.View view;

    @Inject
    public MatchesPresenter() {
    }

    @Override
    public void takeView(MatchesContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        this.view = null;
    }
}
