package com.pairzy.com.MatchedView;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;
/**
 * <h2>MatchAlertObserver</h2>
 * <P>
 *
 * </P>
 * @since  4/27/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MatchAlertObserver
{
    private ConnectableObservable<OpponentDetails> connectableObservable;
    private ObservableEmitter<OpponentDetails> emitor;
    public MatchAlertObserver()
    {
        Observable<OpponentDetails> observable=Observable.create(e -> emitor=e);
        connectableObservable=observable.publish();
        connectableObservable.share();
        connectableObservable.replay();
        connectableObservable.connect();
    }

    public ConnectableObservable<OpponentDetails> getObservable()
    {
        return connectableObservable;
    }

    public void publishData(OpponentDetails data)
    {
        if(emitor!=null)
        {
            emitor.onNext(data);
        }
    }
}
