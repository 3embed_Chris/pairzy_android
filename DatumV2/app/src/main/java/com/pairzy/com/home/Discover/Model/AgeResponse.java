package com.pairzy.com.home.Discover.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 4/6/18.
 */

public class AgeResponse {

    @SerializedName("value")
    @Expose
    Integer value;

    @SerializedName("isHidden")
    @Expose
    Integer isHidden;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(Integer isHidden) {
        this.isHidden = isHidden;
    }
}
