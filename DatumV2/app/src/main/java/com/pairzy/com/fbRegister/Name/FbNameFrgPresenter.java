package com.pairzy.com.fbRegister.Name;

import android.app.Activity;

import com.pairzy.com.R;

import javax.inject.Inject;

/**
 * <h2>FbNameFrgPresenter</h2>
 * <P>
 *
 * </P>
 * @since  2/16/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class FbNameFrgPresenter implements FbNameContract.Presenter
{
    @Inject
    Activity activity;

    private FbNameContract.View view;
    @Inject
    FbNameModel nameModel;

    @Inject
    FbNameFrgPresenter()
    {}


    @Override
    public void takeView(FbNameContract.View view) {
        this.view= view;
    }

    @Override
    public void dropView()
    {
      this.view=null;
    }

    @Override
    public void validateName(String name)
    {
        if(nameModel.isValidName(name))
        {
              if(view!=null)
                  view.onValidName(name);
        }else
        {
            if(view!=null)
                view.invalidateName(activity.getString(R.string.invalid_name));
        }
    }
}
