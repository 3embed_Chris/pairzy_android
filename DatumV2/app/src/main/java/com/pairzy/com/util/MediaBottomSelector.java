package com.pairzy.com.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.pairzy.com.R;
/**
 * @since  2/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MediaBottomSelector
{
    private Activity activity;
    private TypeFaceManager typeFaceManager;
    private  BottomSheetDialog bottomSheetDialog;
    private Callback callback;
    public MediaBottomSelector(Activity activity, TypeFaceManager typeFaceManager)
    {
        this.activity=activity;
        this.typeFaceManager=typeFaceManager;
    }

    public void showBottomSheet(Callback mcallback)
    {
        this.callback=mcallback;
        if(bottomSheetDialog!=null&&bottomSheetDialog.isShowing())
        {
            bottomSheetDialog.dismiss();
            bottomSheetDialog.cancel();
        }
        bottomSheetDialog=new BottomSheetDialog(activity);
        @SuppressLint("InflateParams")
        View sheetView =activity.getLayoutInflater().inflate(R.layout.pic_chooser_bottomsheet, null);
        TextView camera_option=sheetView.findViewById(R.id.camera_option);
        camera_option.setTypeface(typeFaceManager.getCircularAirBook());
        TextView gallery_option=sheetView.findViewById(R.id.gallery_option);
        gallery_option.setTypeface(typeFaceManager.getCircularAirBook());
        LinearLayout camera_view=sheetView.findViewById(R.id.camera_view);
        camera_view.setOnClickListener(view ->
        {
            bottomSheetDialog.dismiss();
            bottomSheetDialog.cancel();
            if(callback!=null)
            {
                callback.onCamera();
            }
        });
        LinearLayout gallery_view=sheetView.findViewById(R.id.gallery_view);
        gallery_view.setOnClickListener(view -> {
            bottomSheetDialog.dismiss();
            bottomSheetDialog.cancel();
            if(callback!=null)
            {
                callback.onGallery();
            }
        });
        bottomSheetDialog.setContentView(sheetView);
        WindowManager.LayoutParams lp =  bottomSheetDialog.getWindow().getAttributes();
        lp.dimAmount=0.0f;
        bottomSheetDialog.getWindow().setAttributes(lp);
        bottomSheetDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
        bottomSheetDialog.show();
    }


    public interface Callback
    {
        void onCamera();

        void onGallery();
    }
}
