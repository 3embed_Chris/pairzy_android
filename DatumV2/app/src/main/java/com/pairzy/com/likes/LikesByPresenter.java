package com.pairzy.com.likes;

import android.app.Activity;

import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.likes.model.LikesModel;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.util.ApiConfig;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h2>LikesByPresenter</h2>
 * <P> this is a  presenter consists bussiness class </P>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */
public class LikesByPresenter implements LikesByContract.Presenter {

    @Inject
    NetworkService networkService;

    @Inject
    PreferenceTaskDataSource dataSource;

    @Inject
    Activity activity;

    @Inject
    LikesModel model;

    @Inject
    LikesByContract.View view;

    String postId;

    @Inject
    public LikesByPresenter() {
    }

    @Override
    public void getUserLikes() {

        postId= activity.getIntent().getStringExtra(ApiConfig.COMMENT.POST_ID);
        networkService.getUserPostLike(dataSource.getToken(),model.getLanguage(),postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {

                    }
                    @Override
                    public void onNext(Response<ResponseBody> value)
                    {
                        try
                        {
                            if(value.code() == 200)
                            {
                                String data=value.body().string();
                                model.parseResponse(data);

                            }


                        } catch (Exception e)
                        {
                            if(view!=null)
                                view.showMessage(" "+value.code());
                        }
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                    }
                    @Override
                    public void onComplete() {}
                });
    }
}
