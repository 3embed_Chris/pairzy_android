package com.pairzy.com.util.CloudManager;

public interface UploaderCallback
{
    void onSuccess(String main_url,String thumb_nail,int height,int width);
    void onError(String error);
}
