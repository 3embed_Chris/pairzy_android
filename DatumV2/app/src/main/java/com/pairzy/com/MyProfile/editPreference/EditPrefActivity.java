package com.pairzy.com.MyProfile.editPreference;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;

import android.view.View;
import android.widget.TextView;

import com.pairzy.com.MyProfile.editPreference.newEditInputFrag.NewUserInputFrg;
import com.pairzy.com.MyProfile.editPreference.newListScrollerFrag.NewListdataFrg;
import com.pairzy.com.R;
import com.pairzy.com.data.model.PrefData;
import com.pairzy.com.util.TypeFaceManager;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * <h>EditProfile activity</h>
 * <p> This EditProfile activity handle editing of user Video and Photos.</p>
 *
 * @author 3Embed.
 * @since 23/4/18.
 */

public class EditPrefActivity extends BaseDaggerActivity implements EditPrefContract.View{

    @Inject
    EditPrefPresenter presenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    Activity activity;

    @BindView(R.id.parent_layout)
    CoordinatorLayout parent_view;
    private Unbinder unbinder;
    private PrefData oldPrefData , newPrefData;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_preference);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        presenter.init();
        presenter.loadPrefData(getIntent());
    }

    @Override
    public void applyFont() {

    }

    @Override
    public void showError(String errorMsg) {
        Snackbar snackbar = Snackbar
                .make(parent_view,""+errorMsg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(String msg) {
        Snackbar snackbar = Snackbar
                .make(parent_view,""+msg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showPrefData(PrefData prefData) {
        oldPrefData = new PrefData(prefData);
        newPrefData = prefData;
        DaggerFragment fragment = openGetRequiredFragment(prefData);
        openFragment(fragment,false);
    }

    /*
    * Open required fragment.*/
    private DaggerFragment openGetRequiredFragment(PrefData prefData)
    {
        Bundle data;
        int item_type=prefData.getType();;
        if(item_type==5)
        {
            NewUserInputFrg newUserInputFrg=new NewUserInputFrg();
            data=new Bundle();
            data.putSerializable("pref_data",prefData);
            newUserInputFrg.setArguments(data);
            return newUserInputFrg;
        }else
        {
            NewListdataFrg newListdataFrg=new NewListdataFrg();
            data=new Bundle();
            data.putSerializable("pref_data",prefData);
            newListdataFrg.setArguments(data);
            return newListdataFrg;
        }
    }

    @Override
    public void openFragment(DaggerFragment fragment, boolean keepBack)
    {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.frg_enter_from_right, R.anim.frg_exit_from_left);
        fragmentTransaction.replace(R.id.parent_container, fragment);
        if(keepBack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
        fragmentTransaction.commit();
    }

    @Override
    public void saveNewPrefData(PrefData currentPrefData) {
        newPrefData = currentPrefData;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onBackPressed() {
        if(presenter.isChangeDetected(newPrefData,oldPrefData)) {
            Intent intent = new Intent();
            intent.putExtra("pref_data",newPrefData);
            setResult(RESULT_OK,intent);
        }
        else {
            setResult(RESULT_CANCELED);
        }
        this.finish();
        activity.overridePendingTransition(R.anim.slide_from_left,R.anim.slide_to_right);
        super.onBackPressed();
    }
}
