package com.pairzy.com.createPost.VideoFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.pairzy.com.R;


public class VideoPostFragment extends Fragment {

   public VideoPostFragment() {
        // Required empty public constructor
    }

       @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video_post, container, false);
    }



    @Override
    public void onDetach() {
        super.onDetach();
    }

}
