package com.pairzy.com.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Subscription
{
    @SerializedName("planId")
    @Expose
    private String planId;
    @SerializedName("subscriptionId")
    @Expose
    private String subscriptionId;
    @SerializedName("purchaseDate")
    @Expose
    private String purchaseDate;
    @SerializedName("purchaseTime")
    @Expose
    private String purchaseTime;
    @SerializedName("userPurchaseTime")
    @Expose
    private String userPurchaseTime;
    @SerializedName("durationInMonths")
    @Expose
    private String durationInMonths;
    @SerializedName("expiryTime")
    @Expose
    private String expiryTime;
    @SerializedName("likeCount")
    @Expose
    private String likeCount;
    @SerializedName("rewindCount")
    @Expose
    private String rewindCount;
    @SerializedName("superLikeCount")
    @Expose
    private String superLikeCount;
    @SerializedName("whoLikeMe")
    @Expose
    private Boolean whoLikeMe;
    @SerializedName("whoSuperLikeMe")
    @Expose
    private Boolean whoSuperLikeMe;
    @SerializedName("recentVisitors")
    @Expose
    private Boolean recentVisitors;
    @SerializedName("readreceipt")
    @Expose
    private Boolean readreceipt;
    @SerializedName("passport")
    @Expose
    private Boolean passport;
    @SerializedName("noAdds")
    @Expose
    private Boolean noAdds;
    @SerializedName("hideDistance")
    @Expose
    private Boolean hideDistance;
    @SerializedName("hideAge")
    @Expose
    private Boolean hideAge;

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getPurchaseTime() {
        return purchaseTime;
    }

    public void setPurchaseTime(String purchaseTime) {
        this.purchaseTime = purchaseTime;
    }

    public String getUserPurchaseTime() {
        return userPurchaseTime;
    }

    public void setUserPurchaseTime(String userPurchaseTime) {
        this.userPurchaseTime = userPurchaseTime;
    }

    public String getDurationInMonths() {
        return durationInMonths;
    }

    public void setDurationInMonths(String durationInMonths) {
        this.durationInMonths = durationInMonths;
    }

    public String getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(String expiryTime) {
        this.expiryTime = expiryTime;
    }

    public String getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(String likeCount) {
        this.likeCount = likeCount;
    }

    public String getRewindCount() {
        return rewindCount;
    }

    public void setRewindCount(String rewindCount) {
        this.rewindCount = rewindCount;
    }

    public String getSuperLikeCount() {
        return superLikeCount;
    }

    public void setSuperLikeCount(String superLikeCount) {
        this.superLikeCount = superLikeCount;
    }

    public Boolean getWhoLikeMe() {
        return whoLikeMe;
    }

    public void setWhoLikeMe(Boolean whoLikeMe) {
        this.whoLikeMe = whoLikeMe;
    }

    public Boolean getWhoSuperLikeMe() {
        return whoSuperLikeMe;
    }

    public void setWhoSuperLikeMe(Boolean whoSuperLikeMe) {
        this.whoSuperLikeMe = whoSuperLikeMe;
    }

    public Boolean getRecentVisitors() {
        return recentVisitors;
    }

    public void setRecentVisitors(Boolean recentVisitors) {
        this.recentVisitors = recentVisitors;
    }

    public Boolean getReadreceipt() {
        return readreceipt;
    }

    public void setReadreceipt(Boolean readreceipt) {
        this.readreceipt = readreceipt;
    }

    public Boolean getPassport() {
        return passport;
    }

    public void setPassport(Boolean passport) {
        this.passport = passport;
    }

    public Boolean getNoAdds() {
        return noAdds;
    }

    public void setNoAdds(Boolean noAdds) {
        this.noAdds = noAdds;
    }

    public Boolean getHideDistance() {
        return hideDistance;
    }

    public void setHideDistance(Boolean hideDistance) {
        this.hideDistance = hideDistance;
    }

    public Boolean getHideAge() {
        return hideAge;
    }

    public void setHideAge(Boolean hideAge) {
        this.hideAge = hideAge;
    }
}

