package com.pairzy.com.LeaderBoad;

public interface LeaderBoadContract {
    interface View {
        void showError(String string);

        void isEmpty();
    }

    interface Presenter
    {

        void fetchDataFromApi();



    }
}
