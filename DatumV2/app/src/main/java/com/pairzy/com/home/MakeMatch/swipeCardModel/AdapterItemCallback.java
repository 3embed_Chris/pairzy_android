package com.pairzy.com.home.MakeMatch.swipeCardModel;


public interface AdapterItemCallback {
    void leftSwipe(int position);
    void rightSwipe(int position);
}
