package com.pairzy.com.locationScreen;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.locationScreen.locationMap.CustomLocActivity;
import com.pairzy.com.locationScreen.model.AddressAdapter;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.lang.reflect.Field;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h2>GoogleLocSearchActivity</h2>
 * <p>
 * handle location search via fourSquare.
 * </P>
 *
 * @author 3Embed.
 * @since 02/02/2017.
 * @version 1.0.
 */
public class LocationSearchActivity extends BaseDaggerActivity implements LocationSearchContract.View,SwipeRefreshLayout.OnRefreshListener,SearchView.OnQueryTextListener{

    @Inject
    LocationSearchPresenter presenter;
    @Inject
    App_permission app_permission;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    Utility utility;
    @Inject
    Activity activity;
    @Inject
    LinearLayoutManager linearLayoutManager;
    @Inject
    AddressAdapter adapter;

    @BindView(R.id.location_list)
    RecyclerView location_list;
    @BindView(R.id.tv_page_title)
    TextView title;
    @BindView(R.id.tv_custom_location)
    TextView tvAddCustomLocation;
    @BindView(R.id.search_view)
    SearchView searchView;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.parent_layout)
    CoordinatorLayout parentLayout;
    @BindView(R.id.ll_add_custom_location)
    LinearLayout llAddCustomLocation;
    @BindView(R.id.rl_search_button)
    RelativeLayout rlSearchButton;
    @BindView(R.id.action_bar_layout)
    RelativeLayout rlActionBar;
    @BindView(R.id.loading_progress)
    ProgressBar pbLoading;
    @BindView(R.id.loading_text)
    TextView tvLoading;
    @BindView(R.id.error_icon)
    ImageView ivErrorIcon;
    @BindView(R.id.error_msg)
    TextView tvErrorMsg;
    @BindView(R.id.empty_data)
    RelativeLayout rlEmptyData;
    @BindView(R.id.loading_view)
    RelativeLayout rlLoadingView;
    @BindView(R.id.no_more_data)
    TextView tvNoMoreDataTitle;
    @BindView(R.id.empty_details)
    TextView tvEmptyDetail;

    private Unbinder unbinder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_search);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        initView();
        searchViewInit();
        setSearchQueryListener();
        presenter.init();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(adapter.getAddressList().size() == 0);
            presenter.loadPlaces();
    }

    @Override
    public void applyFont() {
        title.setTypeface(typeFaceManager.getCircularAirBold());
        tvAddCustomLocation.setTypeface(typeFaceManager.getCircularAirBook());
        tvNoMoreDataTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvEmptyDetail.setTypeface(typeFaceManager.getCircularAirBook());
    }

    @Override
    public void setData(Intent data) {
        setResult(RESULT_OK,data);
        onBackPressed();
    }

    public void setSearchQueryListener() {
        searchView.setOnQueryTextListener(this);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(newText.length() >=2) {
            presenter.searchPlace(newText);
            return true;
        }
        return false;
    }

    public void searchViewInit() {
        EditText txtSearch = ((EditText) searchView.findViewById(com.google.android.material.R.id.search_src_text));
        txtSearch.setHint("Search Location Category..");
        txtSearch.setHintTextColor(getResources().getColor(R.color.softLightGray));
        txtSearch.setTextColor(getResources().getColor(R.color.softDeepGray));
        // Get the search close button image view
        ImageView closeButton = (ImageView)searchView.findViewById(R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                presenter.loadPlaces();
                circleReveal(R.id.search_view,1,true,false);
            }
        });

        try {
            AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(com.google.android.material.R.id.search_src_text);
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.pink_coursour_drawable); //This sets the cursor resource ID to 0 or @null which will make it visible on white background

            rlSearchButton.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    circleReveal(R.id.toolbar,1,true,true);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.back_button)
    public void back(){
        onBackPressed();
    }

    @OnClick(R.id.ll_add_custom_location)
    public void customLocation(){
        Intent intent = new Intent(activity, CustomLocActivity.class);
        startActivityForResult(intent,CustomLocActivity.CHOOSE_LOC_REQ_CODE);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void circleReveal(int viewID, int posFromRight, boolean containsOverflow, final boolean isShow)
    {
        final View myView = findViewById(viewID);

        int width=myView.getWidth();

        if(isShow) {
            if (posFromRight > 0)
                width -= (posFromRight * getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material)) - (getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) / 2);

            if (containsOverflow)
                width -= getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material);
        }
        int cx=width;
        int cy=myView.getHeight()/2;

        Animator anim;
        if(isShow)
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0,(float)width);
        else
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, (float)width, 0);

        anim.setDuration((long)250);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if(!isShow)
                {
                    super.onAnimationEnd(animation);
                    //myView.setVisibility(View.INVISIBLE);
                    rlActionBar.setVisibility(View.VISIBLE);
                    llAddCustomLocation.setVisibility(View.VISIBLE);
                    searchView.setVisibility(View.GONE);
                    searchView.setIconified(true);
                }
                else{
                    rlActionBar.setVisibility(View.GONE);
                    llAddCustomLocation.setVisibility(View.GONE);
                    searchView.setVisibility(View.VISIBLE);
                    searchView.setIconified(false);
                }
            }
        });

        // make the view visible and start the animation
        if(isShow)
            myView.setVisibility(View.VISIBLE);

        // start the animation
        anim.start();

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * <h2>initView</h2>
     * <p>
     * Initializing all the xml content.
     * </P>
     */
    private void initView() {
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh2, R.color.refresh1,R.color.refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        location_list.setLayoutManager(linearLayoutManager);
        location_list.setItemAnimator(new DefaultItemAnimator());
        location_list.setHasFixedSize(false);
        location_list.setAdapter(adapter);
        adapter.setListener(presenter);
        location_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                presenter.handleOnScroll(recyclerView,dx,dy);

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.parseLocation(requestCode,resultCode,data);
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        this.finish();
        activity.overridePendingTransition(R.anim.slide_from_left,R.anim.slide_to_right);
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        presenter.loadPlaces();
    }

    @Override
    public void showMessage(String msg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+msg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(int msgId) {
        String message=getString(msgId);
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showError(String errorMsg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+errorMsg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showLoading() {
        tvErrorMsg.setVisibility(View.GONE);
        ivErrorIcon.setVisibility(View.GONE);
        tvLoading.setVisibility(View.VISIBLE);
        pbLoading.setVisibility(View.VISIBLE);
        rlLoadingView.setVisibility(View.VISIBLE);
        location_list.setVisibility(View.GONE);
        rlEmptyData.setVisibility(View.GONE);
    }

    @Override
    public void showNetworkError(String errorMsg) {
        if(tvErrorMsg != null) {
            tvErrorMsg.setVisibility(View.VISIBLE);
            tvErrorMsg.setText(errorMsg);
            ivErrorIcon.setVisibility(View.VISIBLE);
            tvLoading.setVisibility(View.GONE);
            pbLoading.setVisibility(View.GONE);
            location_list.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void emptyData()
    {
        if(location_list != null) {
            location_list.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showData() {
        if(presenter.isListEmpty()){
            emptyData();
        }
        else {
            rlLoadingView.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
            location_list.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.dispose();
        unbinder.unbind();
    }
}
