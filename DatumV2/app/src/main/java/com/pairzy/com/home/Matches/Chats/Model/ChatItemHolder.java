package com.pairzy.com.home.Matches.Chats.Model;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.util.CustomView.SwipeRevealLayout;
import com.pairzy.com.util.TypeFaceManager;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;

/*
 * Chat item holder*/
class ChatItemHolder  extends RecyclerView.ViewHolder implements View.OnClickListener
{

    SwipeRevealLayout swipe_layout;
    private FrameLayout frontLayout;
    private View deleteLayout;
    private ItemViewCallBack callBack;

    @BindView(R.id.container_rl)
    RelativeLayout rlContainer;
    @BindView(R.id.user_profile_pic)
    SimpleDraweeView ivUserProfilePic;
    @BindView(R.id.user_status_dot)
    ImageView ivOnlineStatusDot;
    @BindView(R.id.iv_unmatch_user)
    ImageView ivUnmatchUser;
    @BindView(R.id.secretLockIv)
    ImageView ivSecretLock;
    @BindView(R.id.newMessage)
    TextView tvNewMessage;
    @BindView(R.id.newMessageTime)
    TextView tvNewMessageTime;
    @BindView(R.id.newMessageDate)
    TextView tvNewMessageDate;
    @BindView(R.id.rl)
    RelativeLayout rlCount;
    @BindView(R.id.newMessageCount)
    TextView tvNewMessageCount;
    @BindView(R.id.storeName)
    TextView tvStoreName;
    @BindView(R.id.tick)
    AppCompatImageView ivTick;
    @BindView(R.id.iv_super_liked_me)
    ImageView ivSuperlikedMe;
    TextView unmatch_text;
    TextView block_text;

    public ChatItemHolder(View itemView, TypeFaceManager typeFaceManager,ItemViewCallBack callBack)
    {
        super(itemView);
        ButterKnife.bind(this,itemView);
        this.callBack=callBack;
        rlContainer.setOnClickListener(this);
        swipe_layout=itemView.findViewById(R.id.swipe_layout);
        //swipe_layout.setOnClickListener(this);
        swipe_layout.setSwipeListener(new SwipeRevealLayout.SwipeListener()
        {
            @Override
            public void onClosed(SwipeRevealLayout view)
            {}
            @Override
            public void onOpened(SwipeRevealLayout view)
            {}
            @Override
            public void onSlide(SwipeRevealLayout view, float slideOffset)
            {
                try
                {
                    handelView(view,slideOffset);
                }catch (Exception e){}
            }
        });
        frontLayout = itemView.findViewById(R.id.front_layout);
        frontLayout.setOnClickListener(this);
        deleteLayout = itemView.findViewById(R.id.delete_layout);

        TextView report_text=itemView.findViewById(R.id.report_text);
        report_text.setTypeface(typeFaceManager.getCircularAirBold());
        block_text=itemView.findViewById(R.id.block_text);
        block_text.setTypeface(typeFaceManager.getCircularAirBold());
        unmatch_text=itemView.findViewById(R.id.unmatch_text);
        unmatch_text.setTypeface(typeFaceManager.getCircularAirBold());
        itemView.findViewById(R.id.report_view).setOnClickListener(this);
        itemView.findViewById(R.id.block_view).setOnClickListener(this);
        itemView.findViewById(R.id.unmatched_view).setOnClickListener(this);

        tvStoreName.setTypeface(typeFaceManager.getCircularAirBold());
        tvNewMessage.setTypeface(typeFaceManager.getCircularAirBook());
        tvNewMessageCount.setTypeface(typeFaceManager.getCircularAirBook());
        tvNewMessageDate.setTypeface(typeFaceManager.getCircularAirBook());
        tvNewMessageTime.setTypeface(typeFaceManager.getCircularAirBook());
    }

    @Override
    public void onClick(View v)
    {
        if(callBack!=null)
            callBack.onViewItemCallBack(v,this.getAdapterPosition());
    }


    /*
    * Handling teh back view */
    private void handelView(SwipeRevealLayout view, float slideOffset)
    {
        RelativeLayout back_shadow_view=view.findViewById(R.id.back_shadow_view);
        if(slideOffset>0.3)
        {
            if(back_shadow_view.getVisibility()!=View.VISIBLE)
            {
                back_shadow_view.setVisibility(View.VISIBLE);
            }

        }else
        {
            if(back_shadow_view.getVisibility()!=View.INVISIBLE)
            {
                back_shadow_view.setVisibility(View.INVISIBLE);
            }
        }
    }
}
