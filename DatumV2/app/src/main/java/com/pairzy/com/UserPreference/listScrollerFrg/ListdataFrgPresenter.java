package com.pairzy.com.UserPreference.listScrollerFrg;

import com.pairzy.com.data.model.Generic;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import java.util.Arrays;
import javax.inject.Inject;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * <h2>ListdataFrgPresenter</h2>
 * <P>
 *
 * </P>
 *@since  2/22/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ListdataFrgPresenter implements ListdataFrgContract.Presenter
{
    private ArrayList<String> data_array;
    private String selected_data="";
    private ListdataFrgContract.View view;
    private CompositeDisposable compositeDisposable;
    @Inject
    DatumProgressDialog progressDialog;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    ListdataFrgModel  model;
    @Inject
    NetworkService service;
    @Inject
    Utility utility;
    @Inject
    ListdataFrgPresenter()
    {
        compositeDisposable=new CompositeDisposable();
        data_array=new ArrayList<>();
    }

    @Override
    public void takeView(Object view)
    {
        this.view= (ListdataFrgContract.View) view;
    }

    @Override
    public void dropView()
    {
        view=null;
        compositeDisposable.clear();
    }

    @Override
    public boolean addSelection(String data)
    {
        if(!data_array.contains(data))
        {
            data_array.add(data);
            return true;
        }
        return false;
    }

    @Override
    public ArrayList<String> getMultiSelectedData()
    {
        return data_array;
    }

    @Override
    public String getSingleSelectedData()
    {
        return selected_data;
    }

    @Override
    public boolean isDataExist()
    {
        return data_array.size()>0;
    }

    @Override
    public void removeSelection(String data)
    {
        if(data_array.contains(data))
        {
            data_array.remove(data);
        }
    }

    @Override
    public void addSingleSelection(String data)
    {
        selected_data=data;
    }

    @Override
    public void updatePreference(String pref_Id, int selectionType)
    {
        progressDialog.show();
        String values;
        if(selectionType==1)
        {
            values=model.multiSelectionValues(data_array);
        }else
        {
            values=model.seingleSelectionValues(selected_data);
        }
        service.setPreferences(dataSource.getToken(),model.getLanguage(),model.getParams(pref_Id,values))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> result)
                    {
                        progressDialog.cancel();
                        try {
                            if(result.code()==200)
                            {
                                String data=result.body().string();
                                Generic result_data=utility.getGson().fromJson(data,Generic.class);
                                ArrayList<String> data_list=selectionType==2?data_array:new ArrayList<>(Arrays.asList(selected_data));
                                if(view!=null)
                                    view.updateTheSelection(data_list);
                            }else
                            {
                                if(view!=null)
                                    view.showError(model.getError(result));
                            }
                        } catch (Exception e)
                        {
                            if(view!=null)
                            view.showError(e.getMessage());
                        }
                    }
                    @Override
                    public void onError(Throwable errorMsg)
                    {
                        progressDialog.cancel();
                        if(view==null)
                            return;
                        view.showError(errorMsg.getMessage());

                    }
                    @Override
                    public void onComplete()
                    {}
                });
    }

    @Override
    public void updateSelection(ArrayList<ListData> options_list_data, int selectionType) {
        ArrayList<String> selectedValues = new ArrayList<>();
        for(ListData listData:options_list_data){
            if(listData.isSelected()){
                selectedValues.add(listData.getOption());
            }
        }
        data_array.clear();
        if(selectionType == 1){
            data_array.addAll(selectedValues);
        }
        else{
            if(!selectedValues.isEmpty())
                selected_data = selectedValues.get(0);
        }
    }
}
