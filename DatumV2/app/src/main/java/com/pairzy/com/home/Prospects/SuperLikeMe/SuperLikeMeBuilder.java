package com.pairzy.com.home.Prospects.SuperLikeMe;
import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * @since  3/23/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface SuperLikeMeBuilder
{
    @FragmentScoped
    @Binds
    SuperLikeMeFrg getProspectItemFragment(SuperLikeMeFrg prospectItem_frg);

    @FragmentScoped
    @Binds
    SuperLikeMeContract.Presenter taskPresenter(SuperLikeMePresenter presenter);
}
