package com.pairzy.com.util;
import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
/**
 * <h2>GridSpacingItemDecoration</h2>
 * <P>
 *     RecyclerView item decoration - give equal margin around grid item
 * </P>
 * @since 2/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration
{
    private int spanCount;
    private int spacing;
    private boolean includeEdge;

    public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge)
    {
        this.spanCount = spanCount;
        this.spacing = spacing;
        this.includeEdge = includeEdge;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state)
    {
        int position = parent.getChildAdapterPosition(view);
        int column = position % spanCount;
        if (includeEdge) {
            outRect.left = spacing - column * spacing / spanCount;
            outRect.right = (column + 1) * spacing / spanCount;

            if (position < spanCount) {
                outRect.top = spacing;
            }
            outRect.bottom = spacing;
        } else {
            outRect.left = column * spacing / spanCount;
            outRect.right = spacing - (column + 1) * spacing / spanCount;
            if (position >= spanCount) {
                outRect.top = spacing;
            }
        }
    }
}

