package com.pairzy.com.createPost;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.pairzy.com.createPost.VideoFragment.VideoPostFragment;
import com.pairzy.com.createPost.cameraFragment.CameraPostFragment;
import com.pairzy.com.createPost.galleryFragment.GalleryPostFragment;


public class CreatePostAdapter extends FragmentStatePagerAdapter {

    private String tile_list[];

    public CreatePostAdapter(FragmentManager fm, String[] tile_list)
    {
        super(fm);
        this.tile_list = tile_list;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position)
        {
            case 0:
                fragment= new GalleryPostFragment();
                break;
            case 1:
                fragment= new CameraPostFragment();
                break;
            case 2:
                fragment= new VideoPostFragment();
                break;

            default:
                fragment=new CameraPostFragment();

        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tile_list.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position)
    {
        return tile_list[position];
    }
}
