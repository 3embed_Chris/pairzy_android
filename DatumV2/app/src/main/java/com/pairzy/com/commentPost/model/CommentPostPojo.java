package com.pairzy.com.commentPost.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CommentPostPojo {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<CommentPostDataPojo> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CommentPostDataPojo> getData() {
        return data;
    }

    public void setData(ArrayList<CommentPostDataPojo> data) {
        this.data = data;
    }
}
