package com.pairzy.com.register.Userdob;

import com.pairzy.com.BasePresenter;

/**
 * @since  2/16/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface DobContract
{
    interface View
    {
        String getAdultErrorMessage();
        void  onDateSelected(String day, String month, String year);

        void moveNextFragment(Double date_imliSec);

        void showMessage(String message);

        void onError(String error);
    }

    interface Presenter extends BasePresenter<View>
    {
        void showMessage(String message);

        void openDatePicker();

        void onError(String message);

        void validateAge(String day, String month, String year);

        int[] getCurrentYear();

    }
}
