package com.pairzy.com.util.boostDialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.boostDetail.model.SubsPlan;
import com.pairzy.com.data.model.Subscription;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
/**
 * <h2>BoostDialog class</h2>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class BoostDialog implements BoostContract.View, SlideItemClickCallback
{
    private BoostContract.Presenter presenter;
    private Activity activity;
    private Dialog  alert = null;
    private TypeFaceManager typeFaceManager;
    private BoostOfferAdapter boostDialogAdapter;
    private BoostAlertCallback callback;
    private SlideTimerTask slideTimerTask;
    private Button btnSubscribe;
    private Button btnNoThanks;
    private ViewPager viewPager;
    private Timer timer;
    private int currentPage = 0;
    private int pageSize;
    private PreferenceTaskDataSource dataSource;
    private Utility utility;

    private Runnable runnable = new Runnable()
    {
        @Override
        public void run() {
            if (currentPage == pageSize) {
                currentPage = 0;
            }
            viewPager.setCurrentItem(currentPage++, true);
        }
    };

    private final int duration = 550;


    public BoostDialog(Activity activity,
                       TypeFaceManager typeFaceManager,
                       PreferenceTaskDataSource dataSource,
                       Utility utility
                       )
    {
        presenter  = new BoostPresenter();
        this.typeFaceManager=typeFaceManager;
        this.activity=activity;
        this.dataSource=dataSource;
        this.utility = utility;
    }

    /*
     * Showing the alert.*/
    @Override
    public void showAlert(BoostAlertCallback callback1, ArrayList<SubsPlan> offerList, ArrayList<Slide> slideList)
    {
        if(dataSource.getSubscription() != null){
            showAlert(dataSource.getSubscription());
            return;
        }

        currentPage = 0;
        pageSize = slideList.size();
        this.callback=callback1;
        dismiss();
        ColorDrawable cd=new ColorDrawable();
        cd.setColor(Color.TRANSPARENT);
        alert =new Dialog(activity,R.style.Datum_AlertDialog);
        Window window1=alert.getWindow();
        assert window1 != null;
        window1.setBackgroundDrawable(cd);
        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater =activity.getLayoutInflater();
        @SuppressLint("InflateParams")
        View alertLayout = inflater.inflate(R.layout.dialog_boost, null);
        alert.setContentView(alertLayout);

        btnSubscribe=alertLayout.findViewById(R.id.btn_subscribe);
        btnSubscribe.setOnClickListener(view -> {
            dismiss();
            if(callback != null);
                callback.onInappSubscribe(boostDialogAdapter==null?-1:boostDialogAdapter.getSelectedPostion());
        });
        btnNoThanks=alertLayout.findViewById(R.id.btn_no_thanks);
        btnNoThanks.setOnClickListener(view -> {
            dismiss();
            if(callback!=null);
                callback.onNoThanks();
        });

        presenter.takeView(this);
        presenter.init();
        //offer recycler setup;
        RecyclerView recyclerOffer = alertLayout.findViewById(R.id.recycler_offer);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(activity,3);
        recyclerOffer.setLayoutManager(gridLayoutManager);
        recyclerOffer.setHasFixedSize(false);
        boostDialogAdapter = new BoostOfferAdapter(activity,typeFaceManager,offerList);
        recyclerOffer.setAdapter(boostDialogAdapter);
        //view pager setup
        Drawable roundDrawable = activity.getResources().getDrawable(R.drawable.round_top_bg);
        viewPager = alertLayout.findViewById(R.id.view_pager);
        TabLayout tabIndicator = alertLayout.findViewById(R.id.tab_layout);
        tabIndicator.setupWithViewPager(viewPager,true);
        BoostSlideAdapter boostPagerAdapter = new BoostSlideAdapter(this,typeFaceManager,activity,slideList,utility);
        viewPager.setAdapter(boostPagerAdapter);
        try {
            Field mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            mScroller.set(viewPager, new CustomScroller(viewPager.getContext(),new DecelerateInterpolator(),duration));
        } catch (Exception e) {
            e.printStackTrace();
        }
        viewPager.setPageTransformer(true,new SlideOffPagerTransformer());

        timer = new Timer();
        slideTimerTask = new SlideTimerTask(runnable);
        timer.schedule(slideTimerTask, 2000, 3000);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = alert.getWindow();
        lp.copyFrom(window.getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        lp.width = (int) ((int)displaymetrics.widthPixels * 0.93);
        lp.height = (int) ((int)displaymetrics.heightPixels * 0.93);
        window.setAttributes(lp);
        alert.show();
    }

    @Override
    public void applyFont()
    {
        btnSubscribe.setTypeface(typeFaceManager.getCircularAirBold());
        btnNoThanks.setTypeface(typeFaceManager.getCircularAirBold());
    }


    /*
 * Showing the alert.*/
    @Override
    public void showAlert(Subscription subs)
    {
        dismiss();
        ColorDrawable cd=new ColorDrawable();
        cd.setColor(Color.TRANSPARENT);
        alert =new Dialog(activity,R.style.Datum_AlertDialog);
        Window window1=alert.getWindow();
        assert window1 != null;
        window1.setBackgroundDrawable(cd);
        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater =activity.getLayoutInflater();
        @SuppressLint("InflateParams")
        View alertLayout = inflater.inflate(R.layout.dialog_boost, null);
        alert.setContentView(alertLayout);

        Button btnOk=alertLayout.findViewById(R.id.ok_button);
        btnOk.setTypeface(typeFaceManager.getCircularAirBook());
        btnOk.setOnClickListener(view -> {
            dismiss();
        });
        presenter.takeView(this);
        //offer recycler setup;
        RelativeLayout activePlanName = alertLayout.findViewById(R.id.active_plan_layout);
        activePlanName.setVisibility(View.VISIBLE);
        TextView tvTitle = alertLayout.findViewById(R.id.tv_title);
        tvTitle.setTypeface(typeFaceManager.getCircularAirBold());
        TextView tvPlanName = alertLayout.findViewById(R.id.tv_plan_name);
        tvPlanName.setTypeface(typeFaceManager.getCircularAirBold());
        tvPlanName.setText(String.format(Locale.ENGLISH,"%s month plan",subs.getDurationInMonths()));

        TextView tvFeatOne = alertLayout.findViewById(R.id.tv_feature_one);
        tvFeatOne.setTypeface(typeFaceManager.getCircularAirBook());

        TextView tvFeatTwo = alertLayout.findViewById(R.id.tv_feature_two);
        tvFeatTwo.setTypeface(typeFaceManager.getCircularAirBook());

        TextView tvFeatThree = alertLayout.findViewById(R.id.tv_feature_three);
        tvFeatThree.setTypeface(typeFaceManager.getCircularAirBook());

        TextView tvFeatFour = alertLayout.findViewById(R.id.tv_feature_four);
        tvFeatFour.setTypeface(typeFaceManager.getCircularAirBook());

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = alert.getWindow();
        lp.copyFrom(window.getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        lp.width = (int) ((int)displaymetrics.widthPixels * 0.93);
        lp.height = (int) ((int)displaymetrics.heightPixels * 0.93);
        window.setAttributes(lp);
        alert.show();
    }


    @Override
    public void dismiss()
    {
        if(alert!=null&&alert.isShowing())
        {
            alert.cancel();
            presenter.dropView();
            if(slideTimerTask != null)
                slideTimerTask.cancel();
            if(timer != null)
                timer.cancel();
        }
    }

    @Override
    public void onSlideItemClick(int position)
    {
        Log.d("45rrt", "onSlideItemClick: "+position);
    }


}
