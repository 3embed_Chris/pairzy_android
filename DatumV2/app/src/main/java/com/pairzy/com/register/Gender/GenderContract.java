package com.pairzy.com.register.Gender;
import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
/**
 * <h2>{@link GenderContract}</h2>
 * @since  2/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface GenderContract
{
    interface View extends BaseView
    {
        void showMessage(String message);

        void showError(String message);

        void moverFragment(int gender);

        void upDateSelection(int position, boolean isSelected);
    }

    interface Presenter extends BasePresenter<View>
    {
        void handleSelection(int position, boolean isSelected);
    }
}
