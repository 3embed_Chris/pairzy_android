package com.pairzy.com.boostLikes.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ankit on 7/9/18.
 */

public class BoostLikeResponse  {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<BoostLikeData> dataList;

    public ArrayList<BoostLikeData> getDataList() {
        return dataList;
    }

    public void setData(ArrayList<BoostLikeData> dataList) {
        this.dataList = dataList;
    }
}
