package com.pairzy.com.UserPreference.EditInputFrg;

import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * @since  2/26/2018.
 * @version 1.0.
 * @author 3Embed.
 */
@Module
public interface UserInputFrgBuilder
{
    @FragmentScoped
    @Binds
    UserInputFrg getConChoiceFragment(UserInputFrg userInputFrg);
    @FragmentScoped
    @Binds
    UserInputContract.Presenter taskPresenter(UserInputPresenter presenter);
}
