package com.pairzy.com.locationScreen.locationMap;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.util.LocationUtil;

import dagger.Module;
import dagger.Provides;

/**
 * <h>CustomLocUtilModule class</h>
 * @author 3Embed.
 * @since 11/5/18.
 * @version 1.0.
 */

@Module
public class CustomLocUtilModule {

    @ActivityScoped
    @Provides
    LocationUtil provideLocationUtil(Activity activity){
        return new LocationUtil(activity);
    }

}
