package com.pairzy.com.home;

import android.app.Activity;
import android.content.Context;
import android.location.Geocoder;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.model.DiscoverTabPositionHolder;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.AppSetting.AppSettingFrag;
import com.pairzy.com.home.Dates.DatesFrag;
import com.pairzy.com.home.Dates.Model.LoadMorePastDateStatus;
import com.pairzy.com.home.Discover.DiscoveryFrag;
import com.pairzy.com.home.HomeModel.LoadMoreStatus;
import com.pairzy.com.home.MakeMatch.MakeMatchFrag;
import com.pairzy.com.home.Matches.MatchesFrag;
import com.pairzy.com.home.Prospects.ProspectsFrg;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.LocationProvider.Location_service;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.SuggestionAleret.SuggestionDialog;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.boostDialog.BoostDialog;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.pairzy.com.util.timerDialog.TimerDialog;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
/**
 * <h2>HomeUtil</h2>
 * <P>
 *     Home page util for the home page data and the details .
 * </P>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class HomeUtil
{
    public static final String HOME_FRAGMENT_MANAGER = "HOME_FM";
    public static final String LOAD_MORE_STATUS ="load_more";
    public static final String LOAD_MORE_DATE_STATUS ="load_date_more";
    public static final String LOAD_MORE_PAST_DATE_STATUS ="load_past_date_more";

    public static final String DISCOVER_FRAG_TAG = "discoverFragTag";
    public static final String PROSPECT_FRAG_TAG = "prospectFragTag";
    public static final String DATE_FRAG_TAG = "dateFragTag";
    public static final String MATCHES_FRAG_TAG = "matchesFragTag";
    public static final String SETTINGS_FRAG_TAG = "settingsFragTag";
    public static final String MACTCH_MAKE_FRAG_TAG = "matchesMakeFragTag";

    @ActivityScoped
    @Provides
    DiscoveryFrag provideDiscoveryFrg(@Named(HOME_FRAGMENT_MANAGER) FragmentManager fragmentManager){
        DiscoveryFrag discoveryFrag = (DiscoveryFrag) fragmentManager.findFragmentByTag(DISCOVER_FRAG_TAG);
        if(discoveryFrag == null)
            discoveryFrag = new DiscoveryFrag();
        return discoveryFrag;
    }

    @ActivityScoped
    @Provides
    ProspectsFrg provideProspectsFrg(@Named(HOME_FRAGMENT_MANAGER)FragmentManager fragmentManager){
        ProspectsFrg prospectsFrg = (ProspectsFrg) fragmentManager.findFragmentByTag(PROSPECT_FRAG_TAG);
        if(prospectsFrg == null)
            prospectsFrg = new ProspectsFrg();
        return prospectsFrg;
    }

    @ActivityScoped
    @Provides
    DatesFrag provideDateFrag(@Named(HOME_FRAGMENT_MANAGER)FragmentManager fragmentManager){
        DatesFrag datesFrag = (DatesFrag) fragmentManager.findFragmentByTag(DATE_FRAG_TAG);
        if(datesFrag == null)
            datesFrag = new DatesFrag();
        return datesFrag;
    }

    @ActivityScoped
    @Provides
    MatchesFrag provideMatchesFragment(@Named(HOME_FRAGMENT_MANAGER)FragmentManager fragmentManager){
        MatchesFrag matchesFragment = (MatchesFrag) fragmentManager.findFragmentByTag(MATCHES_FRAG_TAG);
        if(matchesFragment == null)
            matchesFragment = new MatchesFrag();
        return matchesFragment;
    }

  @ActivityScoped
    @Provides
  MakeMatchFrag provideAppSettingsFrag(@Named(HOME_FRAGMENT_MANAGER)FragmentManager fragmentManager){
      MakeMatchFrag makeMatchFrag = (MakeMatchFrag) fragmentManager.findFragmentByTag(MACTCH_MAKE_FRAG_TAG);
        if(makeMatchFrag == null)
            makeMatchFrag = new MakeMatchFrag();
        return makeMatchFrag;
    }

    @Named(HOME_FRAGMENT_MANAGER)
    @Provides
    @ActivityScoped
    FragmentManager activityFragmentManager(Activity activity)
    {
        return ((AppCompatActivity)activity).getSupportFragmentManager();
    }

    @Provides
    @ActivityScoped
    SuggestionDialog getSuggestionAlert(Activity activity,TypeFaceManager typeFaceManager,Utility utility,PreferenceTaskDataSource dataSource)
    {
        return new SuggestionDialog(activity,typeFaceManager,utility,dataSource);
    }

    @ActivityScoped
    @Provides
    Location_service getLocation_service(Activity activity)
    {
        return new Location_service(activity);
    }

    @Provides
    @ActivityScoped
    App_permission getApp_permission(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new App_permission(activity,typeFaceManager);
    }

    @Provides
    @ActivityScoped
    AnimationHandler getAnimationHandler(Context context)
    {
        return new AnimationHandler(context);
    }

    @Named(LOAD_MORE_STATUS)
    @Provides
    @ActivityScoped
    LoadMoreStatus dataPresentStatus()
    {
        return new LoadMoreStatus();
    }

    @Named(LOAD_MORE_DATE_STATUS)
    @Provides
    @ActivityScoped
    LoadMoreStatus dateLoadMoreStatus()
    {
        return new LoadMoreStatus();
    }

    @Named(LOAD_MORE_PAST_DATE_STATUS)
    @Provides
    @ActivityScoped
    LoadMorePastDateStatus pastDateLoadMoreStatus()
    {
        return new LoadMorePastDateStatus();
    }

    @Provides
    @ActivityScoped
    BoostDialog getBootsDialog(Activity activity, TypeFaceManager typeFaceManager, PreferenceTaskDataSource dataSource,Utility utility)
    {
       return new BoostDialog(activity,typeFaceManager,dataSource,utility);
    }

    @Provides
    @ActivityScoped
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }


    @ActivityScoped
    @Provides
    Geocoder provideGeocoder(Activity activity)
    {
        return new Geocoder(activity, Locale.getDefault());
    }


    @ActivityScoped
    @Provides
    CoinDialog provideCoinDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility){
        return new CoinDialog(activity,typeFaceManager,utility);
    }


    @ActivityScoped
    @Provides
    DiscoverTabPositionHolder ProvideDiscoverTabPostionHolder(){
        return new DiscoverTabPositionHolder();
    }


    @ActivityScoped
    @Provides
    List<UnifiedNativeAd> provideAdList(){
        return new ArrayList<>();
    }


    @ActivityScoped
    @Provides
    TimerDialog provideTimerDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility,PreferenceTaskDataSource dataSource){
        return new TimerDialog(activity,typeFaceManager,utility,dataSource);
    }

}
