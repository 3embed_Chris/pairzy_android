package com.pairzy.com.home.Matches.Chats;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pairzy.com.AppController;
import com.pairzy.com.MqttChat.Utilities.CustomLinearLayoutManager;
import com.pairzy.com.R;
import com.pairzy.com.addCoin.AddCoinActivity;
import com.pairzy.com.boostLikes.BoostLikeActivity;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.home.HomeContract;
import com.pairzy.com.home.Matches.Chats.Model.ChatAdapterItemCallback;
import com.pairzy.com.home.Matches.Chats.Model.ChatListAdapter;
import com.pairzy.com.home.Matches.Chats.Model.MatchAdapterCallback;
import com.pairzy.com.home.Matches.Chats.Model.MatchListAdapter;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.TypeFaceManager;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONObject;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * Created by moda on 05/08/17.
 */
@ActivityScoped
public class ChatsFragment extends DaggerFragment implements ChatsFrgContract.View ,SwipeRefreshLayout.OnRefreshListener{

    @Inject
    MatchListAdapter matchListAdapter;
    @Inject
    ChatsFrgPresenter presenter;
    @Inject
    HomeContract.Presenter homePresenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    ChatListAdapter chatListAdapter;
    @Inject
    Activity activity;

    public ChatsFragment() {
    }

    private static Bus bus = AppController.getBus();

    //private MQttResponseHandler handler;
    private View view;
//    private boolean firstTime = true;
//    private String contactNumberToSave;
//    private int unreadChatsCount = 0;
    private Unbinder unbinder;

//    @BindView(R.id.title_text)
//    TextView tvTitle;

    @BindView(R.id.chat_loading_view)
    RelativeLayout chat_loading_view;
    @BindView(R.id.chat_progress)
    ProgressBar chat_progress;
    @BindView(R.id.chat_error_icon)
    ImageView chat_error_icon;
    @BindView(R.id.chat_loading_text)
    TextView chat_loading_text;
    @BindView(R.id.chat_error_msg)
    TextView chat_error_msg;
    @BindView(R.id.empty_data)
    RelativeLayout empty_data;
    @BindView(R.id.no_more_data)
    TextView no_more_dara;
    @BindView(R.id.empty_details)
    TextView empty_details;
    @BindView(R.id.matched_found_view)
    LinearLayout matched_found_view;
    @BindView(R.id.match_text_view)
    TextView match_text_view;
//    @BindView(R.id.close)
//    RelativeLayout close;
    @BindView(R.id.matched_list)
    RecyclerView recycler_match_list;
    @BindView(R.id.chat_list)
    RecyclerView recyclerView_chat;
    @BindView(R.id.tv_active_chats)
    TextView tvActiveChats;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.ll_match_list)
    LinearLayout llMatchListLayout;
    @BindView(R.id.tv_no_active_chat)
    TextView tvNoActiveChat;
    @BindView(R.id.ll_empty_active_chat)
    LinearLayout llEmptyActiveChat;
//    @BindView(R.id.tv_coin_balance)
//    TextView tvYourBalance;
    @BindView(R.id.btn_retry)
    Button btnRetry;

    private boolean isFromChatFrag = false;
    @Override
    @SuppressWarnings("unchecked")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.fragment_chatlist, container, false);
        } else {
            if (view.getParent() != null)
                ((ViewGroup) view.getParent()).removeView(view);
        }
        presenter.takeView(this);
        //presenter.observeMatchAndUnmatchUser();
        unbinder = ButterKnife.bind(this,view);
        //close.setVisibility(View.GONE);
        applyFont();
        initView();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.observeMatchAndUnmatchUser();
    }

//    @OnClick(R.id.coin_view)
//    void onCoin_View()
//    {
//        homePresenter.launchCoinWallet();
//    }


    public void refresh(){
        /*
         * To avoid the bug of progress bar being shown continously incase of poor or no internet,
         * when refresh chats has been clicked
         */
//        if (AppController.getInstance().canPublish()) {
//            try {
//                AppController.getInstance().setChatSynced(false);
//
//                if (!AppController.getInstance().getChatSynced()) {
//
//                    JSONObject obj = new JSONObject();
//
//                    obj.put("eventName", "SyncChats");
//                    bus.post(obj);
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//        } else {
//
//            if (root != null) {
//                Snackbar snackbar = Snackbar.make(root, getString(R.string.No_Internet_Connection_Available), Snackbar.LENGTH_SHORT);
//                snackbar.show();
//                View view2 = snackbar.getView();
//                TextView txtv = (TextView) view2.findViewById(com.google.android.material.R.id.snackbar_text);
//                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//            }
//        }


        presenter.getMatchedUser();

    }

//    @OnClick(R.id.close)
//    public void back(){
//        if (getActivity() != null)
//            getActivity().onBackPressed();
//    }

    private void initView() {
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh2, R.color.refresh1,R.color.refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        recycler_match_list.setHasFixedSize(true);
        recycler_match_list.setLayoutManager(new CustomLinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        recycler_match_list.setAdapter(matchListAdapter);
        setMatchListListener(presenter);

        recyclerView_chat.setHasFixedSize(true);
        recyclerView_chat.setNestedScrollingEnabled(false);
        recyclerView_chat.setLayoutManager(new CustomLinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        recyclerView_chat.setItemAnimator(new DefaultItemAnimator());
        recyclerView_chat.setAdapter(chatListAdapter);
        setChatListListener(presenter);

        presenter.getMatchedUser();
    }

    /*
     *apply font method.
     */
    private void applyFont() {
        match_text_view.setTypeface(typeFaceManager.getCircularAirBold());
       // tvTitle.setTypeface(typeFaceManager.getCircularAirBold());
        no_more_dara.setTypeface(typeFaceManager.getCircularAirBold());
        empty_details.setTypeface(typeFaceManager.getCircularAirBook());
        tvActiveChats.setTypeface(typeFaceManager.getCircularAirBold());
        tvNoActiveChat.setTypeface(typeFaceManager.getCircularAirBook());
        //tvYourBalance.setTypeface(typeFaceManager.getCircularAirLight());
    }

//    public void showNoSearchResults(CharSequence constraint) {
//
//        TSnackbar snackbar = TSnackbar
//                .make(searchRoot, getString(R.string.string_948) + " " + constraint, TSnackbar.LENGTH_SHORT);
//
//
//        snackbar.setMaxWidth(3000); //for fullsize on tablets
//        View snackbarView = snackbar.getView();
//        snackbarView.setBackgroundColor(Color.WHITE);
//
//
//        TextView textView = (TextView) snackbarView.findViewById(R.id.snackbar_text);
//        textView.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_text_black));
//        snackbar.show();
//    }

    @SuppressWarnings("unchecked")
    @Override
    public void onResume() {
        super.onResume();
        presenter.loadChatList();
        homePresenter.updateCoinBalance();
    }

    /*
     * Have to register the bus for updating the profile pic in the calls list
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bus.register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
        unbinder.unbind();
        presenter.dropView();
    }

    @Subscribe
    public void getMessage(JSONObject object) {
        presenter.handleGetMessage(object);
    }

//    private void requestWriteContactsPermission() {
//
//
//        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
//                Manifest.permission.WRITE_CONTACTS)) {
//
//
//            Snackbar snackbar = Snackbar.make(root, R.string.string_66,
//                    Snackbar.LENGTH_INDEFINITE).setAction(getString(R.string.string_580), new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    requestPermissions(new String[]{Manifest.permission.WRITE_CONTACTS},
//                            29);
//                }
//            });
//
//
//            snackbar.show();
//
//
//            View view = snackbar.getView();
//            TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
//
//            txtv.setMaxLines(3);
//
//
//            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//
//
//        } else {
//
//
//            requestPermissions(new String[]{Manifest.permission.WRITE_CONTACTS},
//                    29);
//        }
//
//    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == 29) {
//
//            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CONTACTS)
//                        == PackageManager.PERMISSION_GRANTED) {
//                    //saveContact(contactNumberToSave);
//                } else {
//                    if(getActivity() != null)
//                        showError(getActivity().getString(R.string.string_60));
//                }
//            } else {
//                if(getActivity() != null)
//                    showError(getActivity().getString(R.string.string_60));
//            }
//        }
//    }

//    private class ChatMessageTouchHelper extends ItemTouchHelper.Callback {
//
//        private final ChatlistAdapter mAdapter2;
//
//        private ChatMessageTouchHelper(ChatlistAdapter adapter) {
//            mAdapter2 = adapter;
//        }
//
//
//        @Override
//        public boolean isLongPressDragEnabled() {
//            return false;
//        }
//
//        @Override
//        public boolean isItemViewSwipeEnabled() {
//            return true;
//        }
//
//        @Override
//        public void onSwiped(RecyclerView.ProfilePicItemViewHolder viewHolder, int direction) {
//            final int position = viewHolder.getAdapterPosition();
//            if (!mChatData.get(position).isGroupChat()) {
//                deleteChat(0, position, null);
//            } else {
//                /*
//                 *If group chat then only allow to delete if current user is no longer a member
//                 */
//                try {
//
//                    String groupMembersDocId = AppController.getInstance().getDbController().fetchGroupChatDocumentId(
//                            AppController.getInstance().getGroupChatsDocId(), mChatData.get(position).getReceiverUid());
//                    if (!AppController.getInstance().getDbController().checkIfActive(
//                            groupMembersDocId)) {
//
//                        deleteChat(1, position, groupMembersDocId);
//                    } else {
//
//
//                        if (root != null) {
//                            Snackbar snackbar = Snackbar.make(root, R.string.LeaveGroup, Snackbar.LENGTH_SHORT);
//                            snackbar.show();
//                            View view = snackbar.getView();
//                            TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
//                            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//                        }
//
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                mAdapter.notifyItemChanged(position);
//                            }
//                        });
//                    }
//                } catch (Exception e) {
//
//                    /*
//                     * Incase groupchat document doesnt exists
//                     */
//                    if (root != null) {
//
//                        Snackbar snackbar = Snackbar.make(root, R.string.delete_failed, Snackbar.LENGTH_SHORT);
//
//
//                        snackbar.show();
//                        View view = snackbar.getView();
//                        TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
//                        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//
//
//                    }
//
//
//                }
//            }
//        }
//
//        @Override
//        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ProfilePicItemViewHolder
//                viewHolder) {
//            int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
//            int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
//            return makeMovementFlags(dragFlags, swipeFlags);
//        }
//
//        @Override
//        public boolean onMove(RecyclerView recyclerView, RecyclerView.ProfilePicItemViewHolder
//                viewHolder, RecyclerView.ProfilePicItemViewHolder target) {
//            return false;
//        }
//    }


//    private void deleteChatFromServer(final ChatListItem chat) {
//
//        String secretId = chat.getSecretId();
//
//        if (secretId == null || secretId.isEmpty()) {
//            secretId = null;
//        }
//
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.DELETE,
//                ApiOnServer.FETCH_CHATS + "/" + chat.getReceiverUid() + "/" + secretId, null, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject response) {
//
//                try {
//                    if (response.getInt("code") == 200) {
//                        AppController.getInstance().getDbController().deleteParticularChatDetail(AppController.getInstance().getChatDocId(),
//                                AppController.getInstance().findDocumentIdOfReceiver(chat.getReceiverUid(),
//                                        chat.getSecretId()), false, null);
//
////                        AppController.getInstance().getDbController().deleteParticularChatDetail(chat.getDocumentId());
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//
//                if (root != null) {
//
//
//                    Snackbar snackbar = Snackbar.make(root, R.string.delete_failed, Snackbar.LENGTH_SHORT);
//
//
//                    snackbar.show();
//                    View view = snackbar.getView();
//                    TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
//                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//
//
//                }
//            }
//        }
//
//
//        ) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Authorization", "KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj");
//
//
//                headers.put("token", AppController.getInstance().getApiToken());
//
//                return headers;
//            }
//        };
//
//
//        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
//                20 * 1000, 0,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
///* Add the request to the RequestQueue.*/
//        AppController.getInstance().addToRequestQueue(jsonObjReq, "deleteChatApiRequest");
//
//    }


//    private void deleteChat(final int type, final int position, final String groupMembersDocId) {
//          /*
//           * Allowing only to delete a non-group chat or secret chat by swiping
//           */
//        android.support.v7.app.AlertDialog.Builder builder =
//                new android.support.v7.app.AlertDialog.Builder(getActivity(), 0);
//        builder.setTitle(getResources().getString(R.string.DeleteConfirmation));
//        builder.setMessage(getResources().getString(R.string.DeleteChat));
//        builder.setPositiveButton(getResources().getString(R.string.Continue), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int id) {
//
//                try {
//                    ChatListItem item = mChatData.get(position);
//                    if (type == 0) {
//                        /*
//                         * To delete the normal or the secret chat from the server
//                         */
//                        deleteChatFromServer(item);
//                    } else {
///*
// * To delete the group chat from the server
// */
//                        deleteGroup(item, groupMembersDocId);
//                    }
//                    mChatData.remove(position);
//                    if (position == 0) {
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                mAdapter.notifyDataSetChanged();
//                            }
//                        });
//                    } else {
//
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                mAdapter.notifyItemRemoved(position);
//                            }
//                        });
//                    }
//
//                } catch (IndexOutOfBoundsException e)
//
//                {
//                    e.printStackTrace();
//                }
//
//                dialog.dismiss();
//            }
//        });
//
//        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int id) {
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        // mAdapter.notifyItemChanged(position);
//
//
//                        mAdapter.notifyDataSetChanged();
//                    }
//                });
//                dialog.cancel();
//
//            }
//        });
//        android.support.v7.app.AlertDialog alertDialog = builder.create();
//
//
//        alertDialog.setOnCancelListener(
//                new DialogInterface.OnCancelListener() {
//                    @Override
//                    public void onCancel(DialogInterface dialog) {
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                mAdapter.notifyItemChanged(position);
//                            }
//                        });
//
//
//                    }
//                });
//        alertDialog.show();
//
//        Button b_pos;
//        b_pos = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
//        if (b_pos != null) {
//            b_pos.setTextColor(
//
//
//                    ContextCompat.getColor(getActivity(), R.color.color_black)
//
//            );
//        }
//        Button n_pos;
//        n_pos = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
//        if (n_pos != null) {
//            n_pos.setTextColor(ContextCompat.getColor(getActivity(), R.color.color_black));
//        }
//
//
//    }


//    private void deleteGroup(final ChatListItem chatListItem, final String groupMembersDocId) {
//
//
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.DELETE,
//                ApiOnServer.DELETE_GROUP + "/" + chatListItem.getReceiverUid(), null, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject response) {
//
//                try {
//                    if (response.getInt("code") == 200) {
//
//
////                        AppController.getInstance().getDbController().deleteParticularChatDetail(chatListItem.getDocumentId());
//
//                        AppController.getInstance().getDbController().deleteParticularChatDetail(
//                                AppController.getInstance().getChatDocId(),
//                                AppController.getInstance().findDocumentIdOfReceiver(chatListItem.getReceiverUid(), ""), true, groupMembersDocId);
//
//
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//
//                if (root != null) {
//
//
//                    Snackbar snackbar = Snackbar.make(root, R.string.delete_group, Snackbar.LENGTH_SHORT);
//
//
//                    snackbar.show();
//                    View view = snackbar.getView();
//                    TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
//                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//
//
//                }
//            }
//        }
//
//
//        ) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Authorization", "KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj");
//
//
//                headers.put("token", AppController.getInstance().getApiToken());
//
//                return headers;
//            }
//        };
//
//
//        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
//                20 * 1000, 0,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
///* Add the request to the RequestQueue.*/
//        AppController.getInstance().addToRequestQueue(jsonObjReq, "deleteGroupApiRequest");
//    }


    /**
     * Have to add the functionality for allowing the direct scroll to the selected message once it has been searched
     */
//    private void showLocalSearchResults(ArrayList<Map<String, Object>> arr) {
//
//        if (getActivity() != null) {
//            mChatData.clear();
//            mAdapter.notifyDataSetChanged();
//            ChatListItem chatlist;
//            Map<String, Object> message;
//            for (int i = 0; i < arr.size(); i++) {
//                chatlist = new ChatListItem();
//                message = arr.get(i);
//                chatlist.setFromSearchMessage(true);
//                chatlist.setMessagePosition((int) message.get("position"));
//                chatlist.setDocumentId((String) message.get("docId"));
//                chatlist.setReceiverImage((String) message.get("receiverImage"));
//                chatlist.setReceiverName((String) message.get("receiverName"));
//                chatlist.setGroupChat((boolean) message.get("groupChat"));
//                chatlist.setNewMessageTime((String) message.get("timestamp"));
//                chatlist.setReceiverInContacts(true);
//                chatlist.sethasNewMessage(false);
//                chatlist.setShowTick(false);
//                chatlist.setNewMessage((String) message.get("message"));
//                if (!(boolean) message.get("groupChat")) {
//                    chatlist.setSecretId((String) message.get("secretId"));
//                    chatlist.setSecretChat(((String) message.get("secretId")).isEmpty());
//                } else {
//                    chatlist.setReceiverIdentifier((String) message.get("receiverIdentifier"));
//
//                }
//                mChatData.add(chatlist);
//                mAdapter.notifyDataSetChanged();
//            }
//        }
//    }

    //extra contract impl
    @Override
    public void launchChatScreen(Intent intent) {
        isFromChatFrag = true;
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult( intent, AppConfig.CHAT_SCREEN_REQ_CODE,ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity()).toBundle());
    }

    @Override
    public void showLoadingView() {
        btnRetry.setVisibility(View.GONE);
        empty_data.setVisibility(View.GONE);
        matched_found_view.setVisibility(View.GONE);
        chat_error_icon.setVisibility(View.GONE);
        chat_error_msg.setVisibility(View.GONE);
        chat_progress.setVisibility(View.VISIBLE);
        chat_loading_text.setVisibility(View.VISIBLE);
        chat_loading_view.setVisibility(View.VISIBLE);
    }

    @Override
    public void showData() {
        empty_data.setVisibility(View.GONE);
        chat_loading_view.setVisibility(View.GONE);
        matched_found_view.setVisibility(View.VISIBLE);
    }

    @Override
    public void showEmptyData(){
        matched_found_view.setVisibility(View.GONE);
        chat_loading_view.setVisibility(View.GONE);
        empty_data.setVisibility(View.VISIBLE);
    }

    @Override
    public void onNoMatchFound() {
        matched_found_view.setVisibility(View.GONE);
        chat_loading_view.setVisibility(View.GONE);
        empty_data.setVisibility(View.VISIBLE);
    }

    @Override
    public void onMatchedFound() {
        empty_data.setVisibility(View.GONE);
        chat_loading_view.setVisibility(View.GONE);
        matched_found_view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onInternetError() {
        btnRetry.setVisibility(View.VISIBLE);
        empty_data.setVisibility(View.GONE);
        matched_found_view.setVisibility(View.GONE);
        chat_progress.setVisibility(View.GONE);
        chat_loading_text.setVisibility(View.GONE);
        chat_error_icon.setVisibility(View.VISIBLE);
        chat_error_msg.setText(R.string.no_internet_error);
        chat_error_msg.setVisibility(View.VISIBLE);
        chat_loading_view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onMatchedApiFailed(String errorMessage) {
        btnRetry.setVisibility(View.VISIBLE);
        empty_data.setVisibility(View.GONE);
        matched_found_view.setVisibility(View.GONE);
        chat_progress.setVisibility(View.GONE);
        chat_loading_text.setVisibility(View.GONE);
        chat_error_icon.setVisibility(View.VISIBLE);
        chat_error_msg.setText(errorMessage);
        chat_error_msg.setVisibility(View.VISIBLE);
        chat_loading_view.setVisibility(View.VISIBLE);
    }

    @Override
    public void openChatPage() {

    }

    @Override
    public void setChatListListener(ChatAdapterItemCallback callback) {
        chatListAdapter.setItemCallback(callback);
    }

    @Override
    public void setMatchListListener(MatchAdapterCallback callback) {
        matchListAdapter.setAAdapterCallBack(callback);
    }

    @Override
    public void showError(String errorMsg) {
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+errorMsg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(String msg) {
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+msg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(int msgId) {
        String message=getString(msgId);
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }


    /*
     * if match list is empty and chat list is not empty then we need to hide match list.
     */
    @Override
    public void showMatchList(boolean show) {
        if(show){
            llMatchListLayout.setVisibility(View.VISIBLE);
        }
        else{
            llMatchListLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void showEmptyActiveChatLayout(boolean show) {
        if(show){
            llEmptyActiveChat.setVisibility(View.VISIBLE);
        }
        else{
            llEmptyActiveChat.setVisibility(View.GONE);
        }
    }

    @Override
    public void showCoinBalance(String coinBalance) {
//        if(tvYourBalance != null)
//            tvYourBalance.setText(coinBalance);
    }

    @Override
    public void setNeedToUpdateChat(boolean yes) {
        homePresenter.setNeedToUpdateChat(yes);
    }

    @Override
    public boolean isNeedToUpdateList() {
        return homePresenter.isNeedToUpdateChatList();
    }

    @Override
    public boolean isFromChatFragment() {
        return isFromChatFrag;
    }

    @Override
    public void setFromChatFragment(boolean fromChatFrag) {
        isFromChatFrag = fromChatFrag;
    }

    @Override
    public void openBoostDialog() {
        homePresenter.openBoostDialog();
    }

    @Override
    public void openProspectScreen() {
        homePresenter.openProspectScreen();
    }

    @Override
    public void openBoostLikeList() {
        Intent intent = new Intent(activity, BoostLikeActivity.class);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @Override
    public void updateChatBadgeCount(String unreadChatCount) {
        homePresenter.updateUnreadChatBadgeCount(unreadChatCount);
    }

    @Override
    public void checkAndLaunchProfileBoostDialog() {
        homePresenter.checkForProfileBoost();
    }

    @Override
    public void launchCoinWallet() {
        Intent intent = new Intent(activity, AddCoinActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @Override
    public void notifyMatchList(int position) {
        matchListAdapter.notifyItemChanged(position);
    }

    @Override
    public void notifyChatList() {
        chatListAdapter.notifyDataSetChanged();
    }

    @Override
    public void notifyChatList(int position) {
        chatListAdapter.notifyItemChanged(position);
    }

    @Override
    public void notifyMatchList() {
        matchListAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.btn_retry)
    public  void onRetry(){
        onRefresh();
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        refresh();
        homePresenter.loadCoinBalance();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.parseActivityResult(requestCode, resultCode, data);
    }
}

