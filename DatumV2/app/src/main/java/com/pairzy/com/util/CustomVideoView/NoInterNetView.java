package com.pairzy.com.util.CustomVideoView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pairzy.com.R;
/**
 * <h2>NoInterNetView</h2>
 * <P>
 *
 * </P>
 * @since  4/24/2018.
 * @author Suresh.
 * @version 1.0.
 */
public class NoInterNetView extends RelativeLayout
{
    private boolean isVisible;
    private View messageView;
    private Context context;
    public NoInterNetView(Context context)
    {
        super(context);
        initView(context);
    }

    public NoInterNetView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initView(context);
    }

    public NoInterNetView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public NoInterNetView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    @SuppressLint("InflateParams")
    private void initView(Context context)
    {
        this.context=context;
        this.setBackgroundColor(ContextCompat.getColor(context,R.color.transparent));
        messageView=LayoutInflater.from(context).inflate(R.layout.internet_issue,null);
        removeAllViews();
        addView(messageView);
    }

    /*
     *No internet option view */
    public void showNoInternet()
    {
        if(isVisible)
        {
           return;
        }
        isVisible=true;
        this.setVisibility(VISIBLE);
        messageView.setBackgroundColor(ContextCompat.getColor(context,R.color.soft_red_color));
        TextView textView=messageView.findViewById(R.id.internet_issue_text);
        textView.setText(R.string.offline_text);
        textView.setTextColor(ContextCompat.getColor(context,R.color.white));
    }


    /*
     *Showing the internet online option. */
    public void internetFound()
    {
        if(!isVisible)
        {
            return;
        }
        isVisible=false;
        messageView.setBackgroundColor(ContextCompat.getColor(context,R.color.green_light));
        TextView textView=messageView.findViewById(R.id.internet_issue_text);
        textView.setText(R.string.online_text);
        textView.setTextColor(ContextCompat.getColor(context,R.color.white));
        TranslateAnimation anim = new TranslateAnimation(0,0,0,-messageView.getHeight());
        anim.setDuration(500);
        anim.setAnimationListener(new TranslateAnimation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation)
            {
                NoInterNetView.this.setVisibility(GONE);
            }
        });
        messageView.startAnimation(anim);
    }
}
