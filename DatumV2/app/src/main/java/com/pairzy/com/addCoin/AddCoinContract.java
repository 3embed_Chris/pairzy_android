package com.pairzy.com.addCoin;

/**
 * <h>PassportContract interface</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */
public interface AddCoinContract {

    interface View{
        void applyFont();
        void showError(String errorMsg);
        void showMessage(String msg);
        void showMessage(int msgId);
        void showLoading();
        void showData();
        void showEmptyData();
        void showNetworkError(String errorMsg);
        void showBalanceLoading(boolean show);
        void showCoinBalance(String coinBalance);
    }

    interface Presenter{
        void init();
        void getCoinBalance();
        void getCoinPlans();
        void observeCoinBalanceChange();
        void updateCoinBalance();
        void loadCoinPlanIfRequired();
        void dispose();
        void observeAdminCoinAdd();
        void callAddCoinApi(int coinValue);
    }
}