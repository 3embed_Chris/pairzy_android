package com.pairzy.com.home.Prospects.RecentVisitors;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.home.Prospects.RecentVisitors.Model.RecentUserItemPojo;
import com.pairzy.com.home.Prospects.RecentVisitors.Model.RecentVisitorAdapter;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import dagger.Module;
import dagger.Provides;
/**
 * @since   3/23/2018.
 * @author 3Embed.
 * @version 1.0
 */
@Module
public class RecentVisitorsUtil
{
    @Provides
    @FragmentScoped
    ArrayList<RecentUserItemPojo> getList()
    {
        return new ArrayList<>();
    }
    @Provides
    @FragmentScoped
    RecentVisitorAdapter getUserListAdapter(ArrayList<RecentUserItemPojo> list, TypeFaceManager typeFaceManager, Utility utility)
    {
        return new RecentVisitorAdapter(list,typeFaceManager,utility);
    }

    @Provides
    @FragmentScoped
    LinearLayoutManager getLinearLayoutManager(Activity activity)
    {
        return new LinearLayoutManager(activity);
    }
}
