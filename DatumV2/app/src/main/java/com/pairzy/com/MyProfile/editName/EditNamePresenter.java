package com.pairzy.com.MyProfile.editName;

import com.pairzy.com.R;

import javax.inject.Inject;

import butterknife.BindString;

/**
 * Created by ankit on 27/4/18.
 */

public class EditNamePresenter implements EditNameContract.Presenter {

    @Inject
    EditNameContract.View view;
    @BindString(R.string.invalid_name)
    String invalid_name;
    @Inject
    NameModel nameModel;

    @Inject
    EditNamePresenter(){
    }

    @Override
    public void validateName(String name)
    {
        if( nameModel.isValidName(name))
        {
            if(view!=null)
                view.onValidName(name);

        }else
        {
            if(view!=null)
                view.invalidateName(invalid_name);
        }
    }
}
