package com.pairzy.com.mobileverify.main;
import android.os.Bundle;
import com.pairzy.com.mobileverify.result.ResultFragment;
import javax.inject.Inject;
/**
 * <h2>PhnoPresenter</h2>
 * <p>
 * Implementation of  @{@link PhnoContract.Presenter}
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 06/01/2018.
 **/
public class PhnoPresenter implements PhnoContract.Presenter
{
    private PhnoContract.ViewIml viewIml;


    @Inject
    PhnoPresenter() {}

    @Override
    public void takeView(Object view)
    {
        viewIml= (PhnoContract.ViewIml) view;
    }

    @Override
    public void dropView()
    {
        viewIml=null;
    }

    @Override
    public void validateNumber(String code,String number)
    {
        if(viewIml==null)
            return;

        Bundle bundle = new Bundle();
        bundle.putString(ResultFragment.COUNTRY_CODE,code);
        bundle.putString(ResultFragment.MOBILE_NUMBER,number);
        bundle.putString(ResultFragment.CODE,ResultFragment.REQUEST_OTP_OPERATION);
        ResultFragment resultFragment=new ResultFragment();
        resultFragment.setArguments(bundle);
        viewIml.openVerifyPage(resultFragment,true);
    }
}
