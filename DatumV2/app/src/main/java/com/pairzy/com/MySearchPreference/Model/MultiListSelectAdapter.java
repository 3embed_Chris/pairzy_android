package com.pairzy.com.MySearchPreference.Model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.data.model.SearchPreference;

import java.util.ArrayList;
import java.util.List;

public class MultiListSelectAdapter extends ArrayAdapter<StateVO> {
    private Context mContext;
    private ArrayList<StateVO> listState;
    private MultiListSelectAdapter multiListSelectAdapter;
    private boolean isFromView = false;
    private ArrayList<String> selectValue =new ArrayList<String>();
    private SearchPreference preferences;

    public MultiListSelectAdapter(Context context, int resource, List<StateVO> objects, SearchPreference preferences) {

        super(context, resource, objects);
        selectValue.clear();
        this.mContext = context;
        this.listState = (ArrayList<StateVO>) objects;
        this.multiListSelectAdapter = this;
        this.preferences=preferences;
        this.preferences.getSelectedValue().clear();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.pref_spinner_mutli_item, null);
            holder = new ViewHolder();
            holder.mTextView = (TextView) convertView.findViewById(R.id.text);
            holder.mCheckBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.mTextView.setText(listState.get(position).getTitle());

        // To check weather checked event fire from getview() or user input
        isFromView = true;
        holder.mCheckBox.setChecked(listState.get(position).isSelected());
        isFromView = false;
        holder.mCheckBox.setTag(position);
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int getPosition = (Integer) buttonView.getTag();

                if (!isFromView) {
                    listState.get(position).setSelected(isChecked);
                    if(holder.mCheckBox.isChecked())
                    {
                        selectValue.add(listState.get(position).getTitle());
                    }
                    else {
                        if(selectValue.contains(listState.get(position).getTitle()))
                        {
                            selectValue.remove(listState.get(position).getTitle());
                        }
                    }
                }

            }

        });
        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
    }


    public ArrayList<String> getSelectedValues()
    {
        return selectValue;
    }
}