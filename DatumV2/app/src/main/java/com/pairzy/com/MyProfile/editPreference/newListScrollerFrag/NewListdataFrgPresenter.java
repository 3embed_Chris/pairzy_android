package com.pairzy.com.MyProfile.editPreference.newListScrollerFrag;

import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * <h2>NewListdataFrgPresenter</h2>
 * <P>
 *
 * </P>
 *@since  2/22/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class NewListdataFrgPresenter implements NewListdataFrgContract.Presenter
{
    private ArrayList<String> data_array;
    private String selected_data="";
    private NewListdataFrgContract.View view;
    private CompositeDisposable compositeDisposable;
    @Inject
    DatumProgressDialog progressDialog;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    NewListdataFrgModel model;
    @Inject
    NetworkService service;
    @Inject
    Utility utility;
    @Inject
    NewListdataFrgPresenter()
    {
        compositeDisposable=new CompositeDisposable();
        data_array=new ArrayList<>();
    }

    @Override
    public void takeView(Object view)
    {
        this.view= (NewListdataFrgContract.View) view;
    }

    @Override
    public void dropView()
    {
        view=null;
        compositeDisposable.clear();
    }

    @Override
    public boolean addSelection(String data)
    {
        if(!data_array.contains(data))
        {
            data_array.add(data);
            return true;
        }
        return false;
    }

    @Override
    public ArrayList<String> getMultiSelectedData()
    {
        return data_array;
    }

    @Override
    public String getSingleSelectedData()
    {
        return selected_data;
    }

    @Override
    public boolean isDataExist()
    {
        return data_array.size()>0;
    }

    @Override
    public void removeSelection(String data)
    {
        if(data_array.contains(data))
        {
            data_array.remove(data);
        }
    }

    @Override
    public void addSingleSelection(String data)
    {
        selected_data=data;
    }

    @Override
    public void updatePreference(String pref_Id, int selectionType,boolean clear)
    {
        ArrayList<String> selectedValue = new ArrayList<>();
        if(selectionType==1)
        {
            if(!clear)
                selectedValue.addAll(data_array);
            else
                selectedValue.addAll(new ArrayList<>());
        }else
        {
            if(!clear)
                selectedValue.add(selected_data);
            else
                selectedValue.add("");
        }
        if(view != null){
            view.updatePreferenceData(selectedValue);
        }
    }

    @Override
    public void initSelectionList(ArrayList<String> selectedValues,int selectionType) {
        if(selectedValues != null && !selectedValues.isEmpty()){
            if(selectionType == 1){
                data_array.addAll(selectedValues);
            }
            else{
                selected_data = selectedValues.get(0);
            }
        }
    }

    @Override
    public void updateSelection(ArrayList<ListData> options_list_data,int selectionType) {
        ArrayList<String> selectedValues = new ArrayList<>();
        for(ListData listData:options_list_data){
            if(listData.isSelected()){
                selectedValues.add(listData.getOption());
            }
        }
        data_array.clear();
        if(selectionType == 1){
            data_array.addAll(selectedValues);
        }
        else{
            if(!selectedValues.isEmpty())
                selected_data = selectedValues.get(0);
        }
    }
}
