package com.pairzy.com.util.progressbar;
import android.graphics.Typeface;
/**
 * <h2>CircleDialog</h2>
 * @since  12/13/2017.
 * @version 1.0.
 * @author 3Embed.
 */
public interface CircleDialog
{
    void show();
    void cancel();
    void setTitle(String title);
    void setTitleColor(int id);
    void setTypeface(Typeface title_text_style);
    void showTitle(boolean siVisible);
    void isCancelable(boolean isCancel);
}
