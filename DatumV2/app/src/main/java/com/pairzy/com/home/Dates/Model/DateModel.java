package com.pairzy.com.home.Dates.Model;

import android.net.Uri;

import com.pairzy.com.BaseModel;
import com.pairzy.com.home.Dates.DatesFragPresenter;
import com.pairzy.com.home.Dates.DatesFragUtil;
import com.pairzy.com.home.HomeModel.LoadMoreStatus;
import com.pairzy.com.home.HomeUtil;
import com.pairzy.com.util.Utility;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by ankit on 15/5/18.
 */

public class DateModel extends BaseModel{

    @Named(DatesFragUtil.UPCOMING_LIST)
    @Inject
    ArrayList<DateListPojo> upcomingList;

    @Named(DatesFragUtil.PENDING_LIST)
    @Inject
    ArrayList<DateListPojo>  pendingList;

    @Named(DatesFragUtil.PAST_LIST)
    @Inject
    ArrayList<PastDateListPojo>  pastDateList;

    @Inject
    Utility utility;
    @Named(HomeUtil.LOAD_MORE_DATE_STATUS)
    @Inject
    LoadMoreStatus loadMoreStatus;
    @Named(HomeUtil.LOAD_MORE_PAST_DATE_STATUS)
    @Inject
    LoadMorePastDateStatus loadMorePastDateStatus;

    @Inject
    public DateModel(){
    }

    public void parseDateResponse(String response){
        try {
            DateResponse dateResponse = utility.getGson().fromJson(response, DateResponse.class);
            ArrayList<DateListPojo> list1 = null;
            ArrayList<DateListPojo> list2 = null;
            if (dateResponse.getData() != null) {
                Data data = dateResponse.getData();
                list1 = data.getPendingDates();
                list2 = data.getUpcomingDates();
                boolean  no_more = false;
                if( list1 != null && list2 != null){
                    no_more = Math.max(list1.size(),list2.size()) != DatesFragPresenter.LIMIT;
                }
                loadMoreStatus.setNo_more_data(no_more);
                if(DatesFragPresenter.PAGE_SIZE==0){
                    pendingList.clear();
                    upcomingList.clear();
                }
                else{
                    removeLoadMore();
                }
                if (list1 != null)
                    pendingList.addAll(list1);
                if (list2 != null)
                    upcomingList.addAll(list2);
            } else {
                loadMoreStatus.setNo_more_data(true);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /*
    * removing the load more data.*/
    private void removeLoadMore()
    {
        try
        {
            if (pendingList.size() > 0) {
                DateListPojo last_item = pendingList.get(pendingList.size() - 1);
                if (last_item.isLoading()) {
                    pendingList.remove(last_item);
                }
            }

            if (upcomingList.size() > 0) {
                DateListPojo last_item = upcomingList.get(upcomingList.size() - 1);
                if (last_item.isLoading()) {
                    upcomingList.remove(last_item);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     *Method is controller for the image pre fetch.
     * @param position contains the position from where we start prefatch.*/
    public void prefetchImage(boolean fromPendingList,int position)
    {
        if(fromPendingList) {
            if (position < 0) {
                position = 0;
            }
            int end_position = position + 5;
            if (end_position > pendingList.size()) {
                end_position = pendingList.size();
            }

            try {
                List<DateListPojo> temp_list = pendingList.subList(position, end_position);
                for (DateListPojo item : temp_list) {
                    if (!item.isLoading()) {
                        prefetch_Image(item.getOpponentProfilePic());
                    }
                }
            } catch (Exception e) {
            }

            int start = position - 5;
            if (start < 0) {
                start = 0;
            }

            try {
                if (start < position) {
                    List<DateListPojo> temp_list = pendingList.subList(start, position);
                    for (DateListPojo item : temp_list) {
                        if (!item.isLoading()) {
                            prefetch_Image(item.getOpponentProfilePic());
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
        else{ //from upcoming list

            if (position < 0) {
                position = 0;
            }

            int end_position = position + 5;
            if (end_position > upcomingList.size()) {
                end_position = upcomingList.size();
            }

            try {
                List<DateListPojo> temp_list = upcomingList.subList(position, end_position);
                for (DateListPojo item : temp_list) {
                    if (!item.isLoading()) {
                        prefetch_Image(item.getOpponentProfilePic());
                    }
                }
            } catch (Exception e) {
            }

            int start = position - 5;
            if (start < 0) {
                start = 0;
            }

            try {
                if (start < position) {
                    List<DateListPojo> temp_list = upcomingList.subList(start, position);
                    for (DateListPojo item : temp_list) {
                        if (!item.isLoading()) {
                            prefetch_Image(item.getOpponentProfilePic());
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     * It prefetch the images for loaded item
     * @see Fresco#getImagePipeline()
     */
    private void prefetch_Image(String pic_url)
    {
        ImagePipeline pipeline = Fresco.getImagePipeline();
        Uri mainUri = Uri.parse(pic_url);
        ImageRequest mainImageRequest = ImageRequestBuilder
                .newBuilderWithSource(mainUri)
                .build();
        pipeline.prefetchToDiskCache(mainImageRequest, null);
    }


    public void parsePastDateResponse(String response) {
        try{
            PastDateResponse pastDateResponse = utility.getGson().fromJson(response,PastDateResponse.class);
            if(pastDateResponse.getData() != null){
                boolean no_more =  pastDateResponse.getData().size() != DatesFragPresenter.LIMIT;
                loadMorePastDateStatus.setNo_more_data(no_more);

                if(DatesFragPresenter.PAGE_PAST_DATE == 0){
                    pastDateList.clear();
                }
                else{
                    removeLoadMoreFromPastList();
                }
                pastDateList.addAll(pastDateResponse.getData());
            }
            else{
                loadMorePastDateStatus.setNo_more_data(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /*
     * removing load more from past date list.
     */
    private void removeLoadMoreFromPastList() {
        try
        {
            if (pastDateList.size() > 0) {
                PastDateListPojo last_item = pastDateList.get(pastDateList.size() - 1);
                if (last_item.isLoading()) {
                    pastDateList.remove(last_item);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /*
     * prefetch list for past date list.
     */
    public void prefetchPastDateImage(int position) {
        if (position < 0) {
            position = 0;
        }
        int end_position = position + 5;
        if (end_position > pastDateList.size()) {
            end_position = pastDateList.size();
        }

        try {
            List<PastDateListPojo> temp_list = pastDateList.subList(position, end_position);
            for (PastDateListPojo item : temp_list) {
                if (!item.isLoading()) {
                    prefetch_Image(item.getOpponentProfilePic());
                }
            }
        } catch (Exception e) {
        }

        int start = position - 5;
        if (start < 0) {
            start = 0;
        }
        try {
            if (start < position) {
                List<PastDateListPojo> temp_list = pastDateList.subList(start, position);
                for (PastDateListPojo item : temp_list) {
                    if (!item.isLoading()) {
                        prefetch_Image(item.getOpponentProfilePic());
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    public boolean checkPastDateExist() {
        if(pastDateList.size() > 0)
            return true;
        return false;
    }

    public boolean checkUpcomingDateExist() {
        if(upcomingList.size() > 0)
            return true;
        return false;
    }

    public boolean checkPendingDateExist() {
        if(pendingList.size() > 0)
            return true;
        return false;
    }

    /*
     * Failed on load more.*/
    public boolean handleFailedLoadMore()
    {
        try
        {
            if(pendingList.size()>0)
            {
                DateListPojo last_item= pendingList.get(pendingList.size()-1);
                if(last_item.isLoading())
                {
                    last_item.setLoadingFailed(true);
                    return true;
                }
            }

            if(upcomingList.size()>0)
            {
                DateListPojo last_item = upcomingList.get(upcomingList.size()-1);
                if(last_item.isLoading())
                {
                    last_item.setLoadingFailed(true);
                    return true;
                }
            }
        }catch (Exception e){}
        return false;
    }

    /*
     * Failed on load more.*/
    public boolean handlePastdateFailedLoadMore()
    {
        try
        {
            if(pastDateList.size()>0)
            {
                PastDateListPojo last_item= pastDateList.get(pastDateList.size()-1);
                if(last_item.isLoading())
                {
                    last_item.setLoadingFailed(true);
                    return true;
                }
            }
        }catch (Exception e){}
        return false;
    }

    public void addToTheUpcomingList(DateListPojo removedDate) {
        if(removedDate != null){
            upcomingList.add(0,removedDate);
        }
    }

    public void addToPendingList(DateListPojo datePojo) {
        pendingList.add(0,datePojo);
    }

    public void addToUpcomingList(DateListPojo datePojo) {
        upcomingList.add(0,datePojo);
    }

    public int getPendingDateListSize() {
        return pendingList.size();
    }

    public void addToThePastDate(PastDateListPojo pastDateListPojo) {
        if(pastDateListPojo != null){
            pastDateList.add(0,pastDateListPojo);
        }
    }
}
