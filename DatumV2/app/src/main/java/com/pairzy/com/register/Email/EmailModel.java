package com.pairzy.com.register.Email;

import com.pairzy.com.BaseModel;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.Utility;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
/**
 * <h2>EmailModel</h2>
 * <p>
 * ListModel of @{@link EmailFragment}
 * </P>
 *
 * @author 3Embed.
 * @version 1.0.
 * @since 06/01/2018.
 **/
public class EmailModel extends BaseModel
{
    @Inject
    Utility utility;
    @Inject
    EmailModel() {}
    /**
     * @param email :email address
     * @return parameters
     */
    public Map<String, Object> verifyParams(String email) {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.VerifyEmailKey.EMAIL, email);
        return map;
    }
    /**
     * <p>Checks email is in valid format or not</p>
     *
     * @param charSequence : entered characters (email address)
     * @return true/false
     */
    public boolean isValidEmail(String charSequence)
    {
        return utility.isValidEmail(charSequence);
    }
}
