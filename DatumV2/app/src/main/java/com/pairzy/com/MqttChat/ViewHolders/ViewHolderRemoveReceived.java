package com.pairzy.com.MqttChat.ViewHolders;

import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;

import com.pairzy.com.util.TypeFaceManager;

/**
 * Created by moda on 12/01/18.
 */

public class ViewHolderRemoveReceived extends RecyclerView.ViewHolder {

    public TextView senderName;
    public TextView message, time, date;
    public RelativeLayout messageRoot;

    public ViewHolderRemoveReceived(View view, TypeFaceManager typeFaceManager) {
        super(view);
        messageRoot = (RelativeLayout) view.findViewById(R.id.message_root);
        date = (TextView) view.findViewById(R.id.date);
        senderName = (TextView) view.findViewById(R.id.lblMsgFrom);
        message = (TextView) view.findViewById(R.id.txtMsg);
        time = (TextView) view.findViewById(R.id.ts);

        //Typeface tf = AppController.getInstance().getRobotoCondensedFont();
        time.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.ITALIC);
        date.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.ITALIC);
        message.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.ITALIC);
    }
}