package com.pairzy.com.appSettings;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

public interface AppSettingsContract {

    interface View extends BaseView {

        void showError(String string);

        void showMessage(String string);
    }

    interface Presenter extends BasePresenter<View> {

        void openBoostDialog();
    }
}
