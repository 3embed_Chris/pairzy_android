package com.pairzy.com.mobileverify.otp;
import android.text.InputFilter;
import android.text.Spanned;
import android.widget.EditText;
/**
 * <h2>InputOtpFilter</h2>
 * <P>
 *
 * </P>
 * @since  2/13/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class InputOtpFilter implements InputFilter
{
    private EditText editText;
    InputOtpFilter(EditText editText)
    {
        this.editText=editText;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end,Spanned dest, int dstart, int dend)
    {
        if(editText.getText().toString().equals("")||editText.getText().toString().length()==0)
        {
            return source;
        }

        boolean canEnterSpace;
        StringBuilder builder = new StringBuilder();
        for (int i = start; i < end; i++)
        {
            canEnterSpace=false;
            char currentChar = source.charAt(i);
            if (Character.isLetterOrDigit(currentChar))
            {
                builder.append(currentChar);
                canEnterSpace = true;
            }

            if(canEnterSpace)
            {
                builder.append(currentChar);
            }
        }
        return builder.toString();
    }
}
