package com.pairzy.com.home.Discover.Model;
/**
 * <h2>BoostLikeAdapterCallback</h2>
 * <P>
 *
 * </P>
 * @since  3/20/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface AdapterCallback
{
    void onLike(int position);
    void onDislike(int position);
    void onSuperLike(int position);
    void onBoots(int position);
    void openUserDetails(int position, android.view.View view);
    void openChatScreen(UserItemPojo userItemPojo);
}
