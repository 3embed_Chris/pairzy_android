package com.pairzy.com.MySearchPreference.Model;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import com.pairzy.com.BaseModel;
import com.pairzy.com.MySearchPreference.MySearchPrefPresenter;
import com.pairzy.com.boostDetail.model.SubsPlan;
import com.pairzy.com.boostDetail.model.SubsPlanResponse;
import com.pairzy.com.data.model.SearchPreference;
import com.pairzy.com.data.model.Subscription;
import com.pairzy.com.data.model.SubscriptionResponse;
import com.pairzy.com.data.source.PassportLocationDataSource;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.passportLocation.model.PassportLocation;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.Utility;
import com.google.gson.reflect.TypeToken;
import com.suresh.innapp_purches.SkuDetails;
import org.json.JSONArray;
import org.json.JSONObject;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
/**
 * <h2>SearchPrefModel</h2>
 * <P>
 *
 * </P>
 * @since  3/7/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class SearchPrefModel extends BaseModel
{
    @Inject
    ArrayList<SubsPlan> subsPlanList;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Utility utility;
    @Inject
    ViewModel viewModel;
    @Inject
    PassportLocationDataSource locationDataSource;
    private ArrayList<SearchPreference> current_data;
    private String last_updated;

    private String savedLocation;

    @Inject
    SearchPrefModel(){}

    /*
     *Parsing the saved data */
    public ArrayList<SearchPreference> parserSavedData() throws DataParsingException
    {
        List<SearchPreference> data_list;
        try
        {
            last_updated=dataSource.getSearchPreference();
            Type type = new TypeToken<List<SearchPreference>>(){}.getType();
            data_list =utility.getGson().fromJson(last_updated,type);
        }catch (Exception e)
        {
            current_data=null;
            throw new DataParsingException(e.getMessage());
        }
        if(data_list==null)
        {
            current_data=null;
            throw new DataParsingException("Failed to parse!");
        }
        current_data=new ArrayList<>(data_list);
        return current_data;
    }

    /*
     * Parsing the server data*/
    public ArrayList<SearchPreference> parseResponse(String data) throws EmptyData,DataParsingException
    {
        try
        {
            SearchPreferenceMain result_data=utility.getGson().fromJson(data,SearchPreferenceMain.class);
            PreferenceItem data_item=result_data.getData().get(0);
            if(data_item!=null)
            {
                /*
                 * Saving the latest data.*/
                current_data=data_item.getSearchPreferences();
                last_updated=utility.getGson().toJson(current_data);
                dataSource.setSearchPreference(last_updated);
                return current_data;
            }else
            {
                throw new EmptyData("CoinData is empty!");
            }
        }catch (Exception e)
        {
            throw new DataParsingException(e.getMessage());
        }
    }

    /*
     * provide location view.
     */
    public View parseLocationView(String locationName, MySearchPrefPresenter presenter) {
        return viewModel.getLocationView(locationName,presenter);
    }

    /*
    * provide view*/
    public List<View> parseRelatedView(ArrayList<SearchPreference> data, MySearchPrefPresenter presenter)
    {

        List<View> views=new ArrayList<>();
        for(SearchPreference item:data)
        {
            switch (item.getTypeOfPreference()) {
                case "1":
                    views.add(viewModel.getSelectionView(item));
                    break;
                case "2":
                    views.add(viewModel.getMultiSelectionView(item,presenter));
                    break;
                case "3":
                    views.add(viewModel.getRangeBarView(item, true));
                    break;
                default:
                    views.add(viewModel.getRangeBarView(item, false));
                    break;
            }
        }
        return views;
    }

    /*
     * Checking user did any change or not.*/
    public boolean isUpdateRequired()
    {
        String current=utility.getGson().toJson(current_data);
        Log.d("ad23", ":"+current);
        if(!last_updated.equals(current))
        {
            storeUpdatedData();
            return true;

        }
        return false;
    }

    /*
     *Storing the updated data. */
    private void storeUpdatedData()
    {
        if(current_data!=null)
        {
            String jsonData=utility.getGson().toJson(current_data);
            Log.d("sda", ""+jsonData);
            dataSource.setSearchPreference(jsonData);
        }
    }


    public Map<String, Object> prepareUpdateData()
    {
        Map<String, Object> map = new HashMap<>();
        String data=createUpdateData();
        map.put(ApiConfig.SearchPrefUpdateReqKey.PREFERENCE,data);
        PassportLocation passportLocation=null;
        if(locationDataSource.isFeaturesLocActive())
        {
            map.put(ApiConfig.SearchPrefUpdateReqKey.IS_PASSPORT_LOCATION,true);
            passportLocation=getFeatureLocation();
        }else
        {
            map.put(ApiConfig.SearchPrefUpdateReqKey.IS_PASSPORT_LOCATION,false);
            passportLocation=parseCurrentLocation();
        }
        if(passportLocation!=null)
        {
            map.put(ApiConfig.SearchPrefUpdateReqKey.LATITUDE,passportLocation.getLatitude());
            map.put(ApiConfig.SearchPrefUpdateReqKey.LONGITUDE,passportLocation.getLongitude());
            map.put(ApiConfig.SearchPrefUpdateReqKey.ADDRESS,passportLocation.getSubLocationName());
        }
        return map;
    }


    /*
     * it will check for feature location first then saved current location.
     */
    public PassportLocation getFeatureLocation()
    {
        try {
            String locationJson = locationDataSource.getFeatureLocation();
            if(!TextUtils.isEmpty(locationJson))
            {
                PassportLocation featureLocation = utility.getGson().fromJson(locationJson, PassportLocation.class);
                if(!TextUtils.isEmpty(featureLocation.getLocationName()))
                {
                    return featureLocation;
                }
            }
        }catch (Exception e)
        {}
        return null;
    }




    /*
    * Creating the uploading data*/
    private String createUpdateData()
    {
        JSONArray update_data=new JSONArray();
        for(SearchPreference data:current_data)
        {
            JSONObject item=new JSONObject();
            try {
                //just saveing user pref distance unit show that show distance in userProfile.
                if(data.getSelectedUnit() != null && data.getSelectedUnit().equalsIgnoreCase(AppConfig.KM)) {
                    dataSource.setIsMile(false);
                }else if(data.getSelectedUnit() != null && data.getSelectedUnit().contains(AppConfig.MI) ){
                    dataSource.setIsMile(true);
                }

                item.put("pref_id",data.getId());
                if(data.getTypeOfPreference().equals("1"))
                {
                    JSONArray jsonArray = new JSONArray(data.getSelectedValue());
                    item.put("selectedValue",jsonArray);
                }else
                {
                    ArrayList<Integer> numbers_data= new ArrayList<>();
                    for(int i = 0;i<data.getSelectedValue().size(); i++)
                    {
                        numbers_data.add((int)Double.parseDouble(data.getSelectedValue().get(i).trim()));
                    }
                    JSONArray jsonArray = new JSONArray(numbers_data);
                    item.put("selectedValue",jsonArray);
                    item.put("selectedUnit",""+data.getSelectedUnit());
                }
                update_data.put(item);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return update_data.toString();
    }

    public String getSavedLocation() {
        try {
            String locationJson = locationDataSource.getFeatureLocation();
            boolean currentLoc = false;
            if(!TextUtils.isEmpty(locationJson)) {
                PassportLocation featureLocation = utility.getGson().fromJson(locationJson, PassportLocation.class);
                if(!TextUtils.isEmpty(featureLocation.getSubLocationName())){
                    return featureLocation.getSubLocationName();
                }
                else {
                    currentLoc = true;
                }
            }
            else{
                currentLoc = true;
            }

            if(currentLoc){ //return saved current location.
                PassportLocation currentLocation = parseCurrentLocation();
                return currentLocation.getSubLocationName();
            }
        }catch (Exception e){
        }
        return "";
    }

    private PassportLocation parseCurrentLocation() {
        try {
            String currentLocationJson = locationDataSource.getCurrentLocation();
            PassportLocation currentLocation = utility.getGson().fromJson(currentLocationJson, PassportLocation.class);
            return currentLocation;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    public boolean isSubsPlanEmpty()
    {
        return  subsPlanList.isEmpty();
    }



    public void selectMiddleItem()
    {
        int mid_pos = getSubsPlanListSize() / 2;
        for(int count=0;count<subsPlanList.size();count++)
        {
            SubsPlan subsPlan = subsPlanList.get(count);
            if(mid_pos==count)
            {
                subsPlan.setSelected(true);
                subsPlan.setHeaderTag("MOST POPULAR");
            }else
            {
                subsPlan.setSelected(false);
            }
        }
    }

    public int getSubsPlanListSize()
    {
        return subsPlanList.size();
    }

    public ArrayList<SubsPlan> getSubsPlanList()
    {
        return subsPlanList;
    }

    public void clearProductList()
    {
        subsPlanList.clear();
    }

    public void parseSubsPlanList(String response)
    {
        subsPlanList.clear();
        SubsPlanResponse subsPlanResponse = utility.getGson().fromJson(response,SubsPlanResponse.class);
        ArrayList<SubsPlan> subsPlans = subsPlanResponse.getData();
        List<String> ids=new ArrayList<>();
        for(SubsPlan item:subsPlans)
        {
            ids.add(item.getActualId());
        }
        subsPlanList.addAll(subsPlans);
    }


    public String getSubsPurchaseId(int position)
    {
        try {
            if (position<getSubsPlanListSize())
            {
                return subsPlanList.get(position).getActualIdForAndroid();
            }
        }catch (Exception ignored){}
        return "";

    }

    public String extractIDFromKey(String productID)
    {
        for(SubsPlan plan:subsPlanList)
        {
            if(plan.getActualId().equals(productID))
            {
                return plan.getId();
            }
        }
        return "";
    }


    public List<String> collectsIds()
    {
        List<String> items=new ArrayList<>();
        for(SubsPlan item :subsPlanList)
        {
            items.add(item.getActualIdForAndroid());
        }
        return items;
    }

    public void updateDetailsData(List<SkuDetails> details)
    {
        for(SkuDetails item:details)
        {
            Log.d("8ry", ":"+item.toString());
            String id=item.getProductId();
            for(SubsPlan product :subsPlanList)
            {
                Log.d("8ry", ":"+product.toString());
                if(id.equals(product.getActualIdForAndroid()))
                {
                    product.setPrice_text(item.getPriceText());
                    product.setCurrencySymbol(item.getCurrency());
                }
            }
        }
    }

    public void parseSubscription(String response) {
        try{
            SubscriptionResponse subsResponse = utility.getGson().fromJson(response,SubscriptionResponse.class);
            if(subsResponse != null){
                Subscription subscription = subsResponse.getData().getSubscription().get(0);
                if(subscription != null){
                    String jsonSubs = utility.getGson().toJson(subscription,Subscription.class);
                    dataSource.setSubscription(jsonSubs);
                }
            }
        }catch (Exception e){}
    }

}
