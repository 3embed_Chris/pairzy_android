package com.pairzy.com.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <h2>LoginData</h2>
 *
 * @author 3Embed
 * @version 1.0
 * @since 1/19/2018.
 */
public class LoginData implements Serializable
{
    @SerializedName("isNewUser")
    @Expose
    private Boolean isNewUser;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("contactNumber")
    @Expose
    private String contactNumber;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;

    @SerializedName("otherImages")
    @Expose
    private ArrayList<String> otherImages=null;

    @SerializedName("profileVideo")
    @Expose
    private String profileVideo;

    @SerializedName("profileVideoThumbnail")
    @Expose
    private String profileVideoThumbnail;

    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("about")
    @Expose
    private String about;

    @SerializedName("instaGramProfileId")
    @Expose
    private String instaGramProfileId;

    @SerializedName("searchPreferences")
    @Expose
    private ArrayList<SearchPreference> searchPreferences = null;

    @SerializedName("myPreferences")
    @Expose
    private ArrayList<MyPrefrance> myPreferences = null;

    //TODO: spelling mistake currect with api.
    @SerializedName("eduation")
    @Expose
    private String education;

    @SerializedName("work")
    @Expose
    private String work;

    @SerializedName("job")
    @Expose
    private String job;
    @SerializedName("location")
    @Expose
    private LocationResponse locationResponse;
    @SerializedName("isPassportLocation")
    private boolean isPassportLocation;
    @SerializedName("subscription")
    @Expose
    private ArrayList<Subscription> subscription;



    public Boolean getIsNewUser()
    {
        return isNewUser;
    }

    public void setIsNewUser(Boolean isNewUser)
    {
        this.isNewUser = isNewUser;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<SearchPreference> getSearchPreferences() {
        return searchPreferences;
    }

    public void setSearchPreferences(ArrayList<SearchPreference> searchPreferences) {
        this.searchPreferences = searchPreferences;
    }

    public ArrayList<MyPrefrance> getmyPreferences() {
        return myPreferences;
    }

    public void setmyPreferences(ArrayList<MyPrefrance> myPreferences) {
        this.myPreferences = myPreferences;
    }

    public ArrayList<String> getOtherImages() {
        return otherImages;
    }

    public void setOtherImages(ArrayList<String> otherImages) {
        this.otherImages = otherImages;
    }

    public String getProfileVideo() {
        return profileVideo;
    }

    public void setProfileVideo(String profileVideo) {
        this.profileVideo = profileVideo;
    }

    public String getProfileVideoThumbnail() {
        return profileVideoThumbnail;
    }

    public void setProfileVideoThumbnail(String profileVideoThumbnail) {
        this.profileVideoThumbnail = profileVideoThumbnail;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getInstaGramProfileId() {
        return instaGramProfileId;
    }

    public void setInstaGramProfileId(String instaGramProfileId) {
        this.instaGramProfileId = instaGramProfileId;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Boolean getNewUser() {
        return isNewUser;
    }

    public void setNewUser(Boolean newUser) {
        isNewUser = newUser;
    }

    public LocationResponse getLocationResponse() {
        return locationResponse;
    }

    public void setLocationResponse(LocationResponse locationResponse) {
        this.locationResponse = locationResponse;
    }

    public boolean isPassportLocation() {
        return isPassportLocation;
    }

    public void setPassportLocation(boolean passportLocation) {
        isPassportLocation = passportLocation;
    }

    public ArrayList<Subscription> getSubscription() {
        return subscription;
    }

    public void setSubscription(ArrayList<Subscription> subscription) {
        this.subscription = subscription;
    }
}
