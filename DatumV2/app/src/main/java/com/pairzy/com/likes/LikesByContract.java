package com.pairzy.com.likes;
/**
 * <h2>LikesByContract</h2>
 * <P> this is a LikesByContract presenter to view and presenter</P>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */

public interface LikesByContract {

    interface View
    {
        void showMessage(String s);
    }

    interface Presenter
    {
        void getUserLikes();
    }
}
