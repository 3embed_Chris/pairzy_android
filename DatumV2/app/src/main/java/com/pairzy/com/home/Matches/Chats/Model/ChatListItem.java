package com.pairzy.com.home.Matches.Chats.Model;

/**
 * Created by moda on 26/07/17.
 */

public class ChatListItem {


    private String chatId ="";
    private String receiverUid;
    private String documentId;
    private String newMessage;
    private String newMessageCount;
    private String newMessageTime;
    private boolean receiverInContacts;
    private String receiverIdentifier;
    private int tickStatus;
    private boolean showTick;
    private String receiverImage;
    private boolean isNewMessage;
    private String receiverName;
    private boolean isSecretChat;
    private String secretId;
    private boolean fromSearchMessage = false;
    private String groupMembersDocId = "";
    private boolean isGroupChat;
    private boolean isSelected = false;
    private int messagePosition;
    private boolean isMatchedUser;
    private boolean isSuperlikedMe = true;
    private boolean isUserOnline = true;
    private boolean isBlocked = false;
    private boolean isBlockedByMe = false;
    private boolean initiated = false;


    public int getTickStatus() {
        return tickStatus;
    }

    public void setTickStatus(int tickStatus) {
        this.tickStatus = tickStatus;
    }

    public boolean isShowTick() {
        return showTick;
    }

    public void setShowTick(boolean showTick) {
        this.showTick = showTick;
    }



    public boolean isReceiverInContacts() {
        return receiverInContacts;
    }

    public void setReceiverInContacts(boolean receiverInContacts) {
        this.receiverInContacts = receiverInContacts;
    }

    /**
     * Can be receiver phoneNumber,email or the userName
     */



    public String getReceiverIdentifier() {
        return receiverIdentifier;
    }

    public void setReceiverIdentifier(String receiverIdentifier) {
        this.receiverIdentifier = receiverIdentifier;
    }


    public String getReceiverImage() {
        return receiverImage;
    }

    public void setReceiverImage(String receiverImage) {
        this.receiverImage = receiverImage;
    }



    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String ReceiverName) {
        this.receiverName = ReceiverName;
    }


    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String DocumentId) {
        this.documentId = DocumentId;
    }


    public String getNewMessageCount() {
        return newMessageCount;
    }

    public void setNewMessageCount(String newMessageCount) {
        this.newMessageCount = newMessageCount;
    }


    public String getNewMessage() {
        return newMessage;
    }

    public void setNewMessage(String newMessage) {
        this.newMessage = newMessage;
    }


    public String getNewMessageTime() {
        return newMessageTime;
    }

    public void setNewMessageTime(String newMessageTime) {
        this.newMessageTime = newMessageTime;
    }


    public boolean hasNewMessage() {
        return isNewMessage;
    }

    public void sethasNewMessage(boolean isNewMessage) {
        this.isNewMessage = isNewMessage;
    }


    public String getReceiverUid() {
        return receiverUid;
    }

    public void setReceiverUid(String ReceiverUid) {
        this.receiverUid = ReceiverUid;
    }


    public boolean isSecretChat() {
        return isSecretChat;
    }

    public void setSecretChat(boolean secretChat) {
        isSecretChat = secretChat;
    }

    public String getSecretId() {
        return secretId;
    }

    public void setSecretId(String secretId) {
        this.secretId = secretId;
    }


    /*
     * For allowing the message forwarding
     */



    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }



    public boolean isGroupChat() {
        return isGroupChat;
    }

    public void setGroupChat(boolean groupChat) {
        isGroupChat = groupChat;
    }


    /*
     * For the option of the message forwarding int he group chat
     */




    public String getGroupMembersDocId() {
        return groupMembersDocId;
    }

    public void setGroupMembersDocId(String groupMembersDocId) {
        this.groupMembersDocId = groupMembersDocId;
    }


    /*
     * For the option of the direct scroll to the specified message in the chat
     */


    public boolean isFromSearchMessage() {
        return fromSearchMessage;
    }

    public void setFromSearchMessage(boolean fromSearchMessage) {
        this.fromSearchMessage = fromSearchMessage;
    }


    public int getMessagePosition() {
        return messagePosition;
    }

    public void setMessagePosition(int messagePosition) {
        this.messagePosition = messagePosition;
    }


    public boolean isMatchedUser() {
        return isMatchedUser;
    }

    public void setMatchedUser(boolean matchedUser) {
        isMatchedUser = matchedUser;
    }

    public boolean isSuperlikedMe() {
        return isSuperlikedMe;
    }
    public void setSuperlikedMe(boolean superlikedMe) {
        isSuperlikedMe = superlikedMe;
    }

    public boolean isUserOnline() {
        return isUserOnline;
    }

    public void setUserOnline(boolean userOnline) {
        isUserOnline = userOnline;
    }

    public boolean isNewMessage() {
        return isNewMessage;
    }

    public void setNewMessage(boolean newMessage) {
        isNewMessage = newMessage;
    }

    public boolean isBlockedByMe() {
        return isBlockedByMe;
    }

    public void setBlockedByMe(boolean blockedByMe) {
        isBlockedByMe = blockedByMe;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public void setInitiated(boolean initiated) {
        this.initiated = initiated;
    }
    public boolean isInitiated() {
        return initiated;
    }

    public boolean isBlocked() {
        return isBlocked;
    }


    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }
}
