package com.pairzy.com.data.model;

import com.pairzy.com.data.source.PreferenceTaskDataSource;

public class UserActionHolder {

    private static final int MAX_ACTION_COUNT = 50;
    private PreferenceTaskDataSource dataSource;
    private int actionCount;


    public UserActionHolder(PreferenceTaskDataSource dataSource) {
        this.dataSource = dataSource;
        actionCount = dataSource.getUserActionCount();
    }

    public void incrementUserActionCount(){
        actionCount++;
        dataSource.setUserActionCount(actionCount);
    }

    public boolean canShowAds(){
        if(actionCount >= MAX_ACTION_COUNT){
            actionCount = 0;
            dataSource.setUserActionCount(actionCount);
            return true;
        }
        return false;
    }
}
