package com.pairzy.com.editProfile.Model;

/**
 * <h>ProfilePicture data class</h>
 * @author 3Embed.
 * @since 24/4/18.
 * @version 1.0.
 */

public class ProfilePicture {

    public static final int PROFILE_PIC = 0;
    public static final int PROFILE_PIC_LOADING = 1;
    public static final int PROFILE_PIC_EMPTY = 2;
    private int type = PROFILE_PIC;
    private String profilePic;
    private String profilePicPath;
    private boolean isProfilePic = false;

    public void setType(int type) {
        this.type = type;
    }
    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public int getType() {
        return type;
    }
    public String getProfilePic() {
        return profilePic;
    }

    public String getProfilePicPath() {
        return profilePicPath;
    }

    public void setProfilePicPath(String profilePicPath) {
        this.profilePicPath = profilePicPath;
    }

    public boolean isProfilePic() {
        return isProfilePic;
    }

    public void setAsProfilePic(boolean profilePic) {
        isProfilePic = profilePic;
    }
}
