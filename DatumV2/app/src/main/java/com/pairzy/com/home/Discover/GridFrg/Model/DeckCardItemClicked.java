package com.pairzy.com.home.Discover.GridFrg.Model;

import android.view.View;

import com.pairzy.com.home.Discover.Model.UserItemPojo;
/**
 * @since  4/4/2018.
 * @version 1.0.
 * @author 3Embed.
 */
public interface DeckCardItemClicked
{
    void openUserProfile(String data, View view);
    void openChatScreen(UserItemPojo userItemPojo, View view);
}
