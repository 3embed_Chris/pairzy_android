package com.pairzy.com.home.HomeModel;

import com.pairzy.com.home.Discover.Model.superLike.CoinWallet;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BoostResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("coinWallet")
    @Expose
    private CoinWallet coinWallet;
    @SerializedName("expire")
    @Expose
    private long boostExpireTime = 0;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CoinWallet getCoinWallet() {
        return coinWallet;
    }

    public void setCoinWallet(CoinWallet coinWallet) {
        this.coinWallet = coinWallet;
    }

    public long getBoostExpireTime() {
        return boostExpireTime;
    }

    public void setBoostExpireTime(long boostExpireTime) {
        this.boostExpireTime = boostExpireTime;
    }
}

