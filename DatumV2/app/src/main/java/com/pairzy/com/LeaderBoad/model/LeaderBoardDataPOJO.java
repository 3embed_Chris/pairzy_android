package com.pairzy.com.LeaderBoad.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LeaderBoardDataPOJO {

    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("userpairCount")
    @Expose
    private Integer userpairCount;
    @SerializedName("coinEarnByVote")
    @Expose
    private Integer coinEarnByVote;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Integer getUserpairCount() {
        return userpairCount;
    }

    public void setUserpairCount(Integer userpairCount) {
        this.userpairCount = userpairCount;
    }

    public Integer getCoinEarnByVote() {
        return coinEarnByVote;
    }

    public void setCoinEarnByVote(Integer coinEarnByVote) {
        this.coinEarnByVote = coinEarnByVote;
    }
}
