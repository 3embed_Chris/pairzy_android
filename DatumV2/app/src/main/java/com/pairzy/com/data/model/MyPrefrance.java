package com.pairzy.com.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * <h2>MyPrefrance</h2>
 *
 * @author 3Embed
 * @version 1.0
 * @since 1/19/2018.
 */
public class MyPrefrance
{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("data")
    @Expose
    private ArrayList<PrefData> data = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<PrefData> getData() {
        return data;
    }

    public void setData(ArrayList<PrefData> data) {
        this.data = data;
    }
}
