package com.pairzy.com.fbRegister.ProfileVideo;
import android.net.Uri;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

/**
 * <h2>FbProfileVideoContact</h2>
 * @since  2/19/2018.
 * @version 1.0.
 * @author 3Embed.
 */
public interface FbProfileVideoContact
{
    interface Presenter extends BasePresenter
    {
        String getRecentTempVideo();
        String getActualVIdeo();
        void upDateToGallery();
        void openChooser();
        void showError(String message);
        void uploadVideo(String filePath,boolean firstTimeUpload);
        void compressedVideo(String file_path);
        void getCloudinaryDetails(String filePath);

    }

    interface View extends BaseView
    {
        void onVideoCompressed(String file_path);
        void mediaUploaded(String video_url, String thumb_url);
        void imageCollectError();
        void stopVideoPlay();
        void openCamera(Uri uri);
        void openGallery();
        void showError(String message);
        void showMessage(String messaage);
        void showChangeButton(boolean show);
    }
}
