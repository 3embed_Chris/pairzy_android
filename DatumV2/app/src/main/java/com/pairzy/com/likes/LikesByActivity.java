package com.pairzy.com.likes;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pairzy.com.R;
import com.pairzy.com.likes.model.LikesAdapter;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h2>LikesByActivity</h2>
 * <P> this is a activity provide user like view</P>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */
public class LikesByActivity extends DaggerAppCompatActivity implements LikesByContract.View, SwipeRefreshLayout.OnRefreshListener{

    @BindView(R.id.LikesRefreshLayout)
    SwipeRefreshLayout refreshLayout;

    @BindView(R.id.rvLikes)
    RecyclerView recyclerView;

    Unbinder unbinder;

    @Inject
    LikesByContract.Presenter presenter;

    @Inject
    LikesAdapter adapter;

    @Named(LikesByUtil.LIKES_LAYOUT_MANAGER)
    @Inject
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_likes_by);
        unbinder= ButterKnife.bind(this);
        presenter.getUserLikes();
        init();
    }

    private void init() {
        refreshLayout.setOnRefreshListener(this);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(false);
        presenter.getUserLikes();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void showMessage(String s) {
    }

    @OnClick(R.id.fl_back_button)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

