package com.pairzy.com.home.Discover.Model;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * <h2>UserItemPojo</h2>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class UserItemPojo
{
    private  int item_remove_side=0;
    public int item_actual_pos=0;
    private int currentImagePos =0;
    private boolean isLoading;
    private boolean isLoadingFailed;
    private boolean isAdView = false;
    private ArrayList<UserMediaPojo> media_list;
    private UnifiedNativeAd ad;

    @SerializedName("opponentId")
    @Expose
    private String opponentId;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dateOfBirth")
    @Expose
    private Double dateOfBirth;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("distance")
    @Expose
    private DistanceResponse distance;
    @SerializedName("age")
    @Expose
    private AgeResponse age;
    @SerializedName("onlineStatus")
    @Expose
    private int onlineStatus;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("instaGramProfileId")
    @Expose
    private String instaGramProfileId;

    @SerializedName("profileVideo")
    @Expose
    private String profileVideo;

    @SerializedName("otherImages")
    @Expose
    private ArrayList<String> otherImages;

    @SerializedName("profileVideoThumbnail")
    @Expose
    private String profileVideoThumbnail;

    @SerializedName("work")
    @Expose
    private String work;

    @SerializedName("job")
    @Expose
    private String job;

    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("superliked")
    @Expose
    private Integer isSuperLikedMe = 1;

    @SerializedName("yesVotes")
    private  Integer yesVotes=0;

    @SerializedName("noVotes")
    private Integer noVotes;


    public Integer getYesVotes() {
        return yesVotes;
    }

    public void setYesVotes(Integer yesVotes) {
        this.yesVotes = yesVotes;
    }

    public Integer getNoVotes() {
        return noVotes;
    }

    public void setNoVotes(Integer noVotes) {
        this.noVotes = noVotes;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getEducation() {
        return education;
    }

    public boolean isLoadingFailed()
    {
        return isLoadingFailed;
    }

    public void setLoadingFailed(boolean loadingFailed)
    {
        isLoadingFailed = loadingFailed;
    }

    public ArrayList<UserMediaPojo> getMedia_list()
    {
        return media_list;
    }

    public void setMedia_list(ArrayList<UserMediaPojo> media_list) {
        this.media_list = media_list;
    }


    public int getCurrentImagePos() {
        return currentImagePos;
    }

    public void setCurrentImagePos(int currentImagePos) {
        this.currentImagePos = currentImagePos;
    }

    public String getProfileVideoThumbnail() {
        return profileVideoThumbnail;
    }

    public void setProfileVideoThumbnail(String profileVideoThumbnail)
    {
        this.profileVideoThumbnail = profileVideoThumbnail;
    }


    public String getProfileVideo() {
        return profileVideo;
    }

    public void setProfileVideo(String profileVideo) {
        this.profileVideo = profileVideo;
    }

    public ArrayList<String> getOtherImages()
    {
        return otherImages;
    }

    public void setOtherImages(ArrayList<String> otherImages)
    {
        this.otherImages = otherImages;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading)
    {
        isLoading = loading;
    }

    public String getOpponentId() {
        return opponentId;
    }

    public void setOpponentId(String opponentId) {
        this.opponentId = opponentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Double getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Double dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public DistanceResponse getDistance() {
        return distance;
    }

    public void setDistance(DistanceResponse distance) {
        this.distance = distance;
    }

    public AgeResponse getAge() {
        return age;
    }

    public void setAge(AgeResponse age) {
        this.age = age;
    }

    public int getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(int onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getInstaGramProfileId() {
        return instaGramProfileId;
    }

    public void setInstaGramProfileId(String instaGramProfileId)
    {
        this.instaGramProfileId = instaGramProfileId;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Integer isSuperlikedMe() {
        return isSuperLikedMe;
    }

    public boolean isAdView() {
        return isAdView;
    }

    public void setAdView(boolean adView) {
        isAdView = adView;
    }

    public UnifiedNativeAd getAd() {
        return ad;
    }

    public void setAd(UnifiedNativeAd ad) {
        this.ad = ad;
    }
}
