package com.pairzy.com.home.MakeMatch;

import android.app.Activity;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PassportLocationDataSource;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.MakeMatch.model.MakeMatchDataPOJO;
import com.pairzy.com.home.MakeMatch.model.MakeMatchModel;
import com.pairzy.com.home.MakeMatch.swipeCardModel.AdapterItemCallback;
import com.pairzy.com.locationScreen.model.LocationHolder;
import com.pairzy.com.networking.NetworkServicePy;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.passportLocation.model.PassportLocation;
import com.pairzy.com.util.LocationProvider.LocationApiCallback;
import com.pairzy.com.util.LocationProvider.LocationApiManager;
import com.pairzy.com.util.progressbar.LoadingProgress;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class MakeMatchPresenter implements MakeMatchContract.Presenter , AdapterItemCallback {

    @Inject
    NetworkServicePy service;

    @Inject
    PreferenceTaskDataSource dataSource;

    @Inject
    NetworkStateHolder networkStateHolder;

    @Inject
    MakeMatchModel model;

    @Inject
    LoadingProgress loadingProgress;

    @Inject
    PassportLocationDataSource locationDataSource;

    @Inject
    Activity activity;

    @Inject
    LocationApiManager locationApiManager;

    @Named(MakeMatchUtil.MAKE_MATCH)
    @Inject
    ArrayList<MakeMatchDataPOJO> arrayList;

    private MakeMatchFrag view;
    private CompositeDisposable compositeDisposable;
    private String city;
    private Double latitude, longitude;
    private int INDEX =0,OFFSET=0,LIMIT=2,MAX_INDEX=0;

    @Inject
    public MakeMatchPresenter() {
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(Object view) {
        this.view= (MakeMatchFrag) view;
    }

    @Override
    public void dropView() {
        view=null;
        compositeDisposable.clear();
    }

    /*    @Override
        public void loadSavedLocations(int OFFSET, int LIMIT) {
            ArrayList<String> locationArray = locationDataSource.getPassportLocations();
            city= model.parsePassportLocationList(locationArray);
            fetchDataFromApi(OFFSET,LIMIT);
            if(city.equalsIgnoreCase("")) {
                getUserLocation(OFFSET,LIMIT);
            }else {
                getUserLocation(OFFSET,LIMIT);
            }
        }*/


    @Override
    public void getUserLocation(int OFFSET, int LIMIT) {
        locationApiManager.getLocation(new LocationApiCallback() {
            @Override
            public void onSuccess(Double lat, Double lng, double accuracy) {
                LocationHolder locationHolder = new LocationHolder();
                locationHolder.setLatitude(lat);
                locationHolder.setLongitude(lng);
                latitude=lat;
                longitude=lng;
                PassportLocation currentLocation = model.getLocationName(locationHolder);
                city =currentLocation.getCity();
                fetchDataFromApi(OFFSET,LIMIT);
            }

            @Override
            public void onError(String error) {
                if(view!=null)
                    view.showError(error);
            }
        });
    }

    @Override
    public void fetchDataFromApi(int offSet, int limit) {
        if(networkStateHolder.isConnected()) {
            service.getMatchedPairs(dataSource.getToken(), city,String.valueOf(latitude),String.valueOf(longitude),offSet,50)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> value) {

                            if (value.code() == 200) {
                                try {
                                    String data=value.body().string();
                                    model.parseMakeMatchResponse(data);
                                    initData();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else if(value.code() == 404){
                               initData();
                            }
                            else {
                                if(view != null)
                                    view.showError(model.getError(value));
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if(view != null)
                                view.showError(activity.getString(R.string.failed_to_get_post_list));
                        }

                        @Override
                        public void onComplete() { }
                    });
        } else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    public void sendUserAction(int userAction,String pairId) {
        if(networkStateHolder.isConnected()) {
            loadingProgress.show();
            service.sentUserActions(dataSource.getToken(),model.sendUserAction(userAction,pairId))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                loadingProgress.cancel();
                            }else {
                                if(view != null)
                                    view.showError(model.getError(value));
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view != null)
                                view.showError(activity.getString(R.string.failed_to_get_post_list));
                        }

                        @Override
                        public void onComplete() { }
                    });
        } else{
            loadingProgress.cancel();
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    @Override
    public void leftSwipe(int position) {
        if(arrayList!=null){
            MakeMatchDataPOJO makeMatchDataPOJO=arrayList.get(position);
            arrayList.remove(position);
           sendUserAction(2,makeMatchDataPOJO.getPairId());
            initData();
           /* if(INDEX==MAX_INDEX-1){
                OFFSET=INDEX;
                MAX_INDEX+=LIMIT;
                fetchDataFromApi(OFFSET,LIMIT);
            }
            else{
                INDEX++;
                initData();
            }*/
            view.notifyRvAdapter(position);
        }
    }

    @Override
    public void rightSwipe(int position) {
        if(arrayList!=null){
            MakeMatchDataPOJO makeMatchDataPOJO=arrayList.get(position);
            arrayList.remove(position);
            sendUserAction(1,makeMatchDataPOJO.getPairId());
            initData();
        /*    if(INDEX==MAX_INDEX-1){
                OFFSET=INDEX;
                MAX_INDEX+=LIMIT;
                fetchDataFromApi(OFFSET,LIMIT);
            }
            else{
                INDEX++;
                initData();
            }*/
            view.notifyRvAdapter(position);
        }
    }

    public void initData() {
        if(arrayList.size()> INDEX) {
            view.showData();
        }else {
            view.hideData();
        }
    }
}
