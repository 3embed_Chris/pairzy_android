package com.pairzy.com.commentPost;

import android.app.Activity;
import android.util.Log;

import com.pairzy.com.R;
import com.pairzy.com.commentPost.model.CommentItemCallBack;
import com.pairzy.com.commentPost.model.CommentModel;
import com.pairzy.com.commentPost.model.CommentPostAdapter;
import com.pairzy.com.commentPost.model.CommentPostDataPojo;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.util.ApiConfig;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class CommentPostPresenter implements CommentPostContract.Presenter, CommentItemCallBack {
    String TAG=CommentPostPresenter.class.getSimpleName();
    @Inject
    CommentPostPresenter(){}

    @Inject
    CommentPostContract.View view;

    @Inject
    NetworkService networkService;

    @Inject
    PreferenceTaskDataSource dataSource;

    @Inject
    Activity activity;
    @Inject
    CommentModel model;

    String comment,postId;
    @Override
    public void validateComment(String comment,String postId) {

        this.comment=comment;
        this.postId=postId;
        if(comment.isEmpty())
        {
            view.showMessage(activity.getString(R.string.comment_error));
        }
        else
        {
            postUserComment();
        }
    }

    @Override
    public void getUserComment() {

        postId= activity.getIntent().getStringExtra(ApiConfig.COMMENT.POST_ID);
        networkService.getUserPostCommment(dataSource.getToken(),model.getLanguage(),postId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {

                    }
                    @Override
                    public void onNext(Response<ResponseBody> value)
                    {
                        try
                        {
                            if(value.code() == 200)
                            {
                                String data=value.body().string();
                                model.parseResponse(data);

                            }


                        } catch (Exception e)
                        {
                            if(view!=null)
                                view.showMessage(" "+value.code());
                        }
                    }
                    @Override
                    public void onError(Throwable e)
                    {


                    }
                    @Override
                    public void onComplete() {}
                });
    }

    @Override
    public void postUserComment() {

        networkService.postUserPostCommment(dataSource.getToken(),model.getLanguage(),model.Credentials(comment,postId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {

                    }
                    @Override
                    public void onNext(Response<ResponseBody> value)
                    {
                        try
                        {
                            if(value.code() == 200)
                            {
                                String data=value.body().string();
                                view.CallGetUserComment();
                            }
                            else
                                view.showMessage(" "+value.code());


                        } catch (Exception e)
                        {
                            if(view!=null)
                                view.showMessage(" "+value.code());
                        }
                    }
                    @Override
                    public void onError(Throwable e)
                    {


                    }
                    @Override
                    public void onComplete() {}
                });
    }

    @Override
    public void setAdapterCallBack(CommentPostAdapter postAdapter) {
        postAdapter.setItemCallback(this);
    }

    @Override
    public void onEdit(int position) {
        Log.e(TAG, "onEdit: ");
        CommentPostDataPojo postListPojo = model.getCommentItem(position);
        if(postListPojo != null){
           String comment= postListPojo.getComment();
            if(view != null) {
                view.editComment(comment);
                view.notifyDataAdapter(position);
            }
        }
    }

    @Override
    public void OnDelete(int position) {
        Log.e(TAG, "OnDelete: " );
        CommentPostDataPojo postListPojo = model.getCommentItem(position);
        if(postListPojo != null){
            model.deletedComment(position);
            if(view != null) {
                view.notifyDataAdapter(position);
            }
        }
    }
}
