package com.pairzy.com.editProfile.ProfileAlert;

import android.annotation.SuppressLint;
import android.app.Activity;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;

/**
 * <h2>ChatMenuDialog</h2>
 * <P>
 *
 * </P>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ProfileAlertDialog
{
    private Activity activity;
    private BottomSheetDialog dialog;
    private TypeFaceManager typeFaceManager;
    private ProfileAlertCallBack callBack;

    public ProfileAlertDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        this.activity=activity;
        this.typeFaceManager=typeFaceManager;
        dialog = new BottomSheetDialog(activity,R.style.DatumBottomDialog);
    }


    public void showDialog(int pos, ProfileAlertCallBack callBack)
    {
        this.callBack = callBack;
        if(dialog.isShowing())
        {
            dialog.cancel();
        }
        @SuppressLint("InflateParams")
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.user_profile_aleret, null);
        Button recomend_button=dialogView.findViewById(R.id.btn_set_as_profile);
        recomend_button.setTypeface(typeFaceManager.getCircularAirBook());
        recomend_button.setOnClickListener(view -> {
            //pending
            if(callBack != null)
                callBack.onSetProfile(pos);
            if(dialog!=null)
                dialog.cancel();
        });
        
        Button onCanceled=dialogView.findViewById(R.id.onCanceled);
        onCanceled.setTypeface(typeFaceManager.getCircularAirBook());
        onCanceled.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();
        });
        dialog.setContentView(dialogView);
        dialog.show();
    }
}
