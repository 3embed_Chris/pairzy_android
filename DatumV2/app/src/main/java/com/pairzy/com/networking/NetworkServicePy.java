package com.pairzy.com.networking;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface NetworkServicePy {
    @GET("/matchedPairs")
    Observable<Response<ResponseBody>> getMatchedPairs(@Header("authorization") String auth,
                                                        @Header("city") String city,@Header("latitude") String latitude,
                                                       @Header("longitude") String longitude,@Query("offset") int offset, @Query("limit") int limit);

    @POST("/userActions/")
    Observable<Response<ResponseBody>> sentUserActions(@Header("authorization") String auth,@Body Map<String,Object> mapBody);

    @GET("/leaderboard/")
    Observable<Response<ResponseBody>> getLeaderBoard(@Header("authorization") String auth,@Query("offset") int offset, @Query("limit") int limit);
}
