package com.pairzy.com.settings;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.util.DeleteAccountDialog.DeleteAccountDialog;
import com.pairzy.com.util.LogoutDialog.LogoutDialog;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.progressbar.LoadingProgress;

import dagger.Module;
import dagger.Provides;

/**
 * <h>PassportUtilModule class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

@Module
public class SettingsUtilModule {

    @ActivityScoped
    @Provides
    LogoutDialog provideLogoutDialog(Activity activity, TypeFaceManager typeFaceManager){
        return new LogoutDialog(activity,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    DeleteAccountDialog  provideDeleteAccountDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility){
        return new DeleteAccountDialog(activity,typeFaceManager,utility);
    }
    @ActivityScoped
    @Provides
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }
}
