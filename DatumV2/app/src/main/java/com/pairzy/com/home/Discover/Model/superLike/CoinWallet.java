package com.pairzy.com.home.Discover.Model.superLike;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CoinWallet {

@SerializedName("Coin")
@Expose
private Integer coin;

public Integer getCoin() {
return coin;
}

public void setCoin(Integer coin) {
this.coin = coin;
}

}