package com.pairzy.com.home.Matches.Chats;

import android.content.Intent;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.home.Matches.Chats.Model.ChatAdapterItemCallback;
import com.pairzy.com.home.Matches.Chats.Model.MatchAdapterCallback;

/**
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface ChatsFrgContract
{
    interface View extends BaseView
    {
        void launchChatScreen(Intent intent);
        void showLoadingView();
        void showData();
        void showEmptyData();
        void onNoMatchFound();
        void onMatchedFound();
        void onInternetError();
        void onMatchedApiFailed(String errorMessage);
        void openChatPage();
        void setChatListListener(ChatAdapterItemCallback callback);
        void setMatchListListener(MatchAdapterCallback callback);
        void showError(String error);
        void showMessage(String error);
        void showMessage(int errorId);
        void showMatchList(boolean show);
        void showEmptyActiveChatLayout(boolean show);

        void showCoinBalance(String coinBalance);

        void setNeedToUpdateChat(boolean yes);

        boolean isNeedToUpdateList();

        boolean isFromChatFragment();

        void setFromChatFragment(boolean fromChatFrag);

        void openBoostDialog();

        void openProspectScreen();

        void openBoostLikeList();

        void updateChatBadgeCount(String unreadChatCount);

        void checkAndLaunchProfileBoostDialog();

        void launchCoinWallet();

        void notifyMatchList(int position);

        void notifyChatList();

        void notifyChatList(int position);

        void notifyMatchList();
    }

    interface Presenter extends BasePresenter<View>
    {
        void  getMatchedUser();
        void observeMatchAndUnmatchUser();
        void loadChatList();
        void parseActivityResult(int requestCode, int resultCode, Intent data);
    }
}
