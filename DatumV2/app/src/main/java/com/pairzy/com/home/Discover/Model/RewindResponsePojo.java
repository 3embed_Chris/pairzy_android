package com.pairzy.com.home.Discover.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RewindResponsePojo
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private UserItemPojo data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserItemPojo getData() {
        return data;
    }

    public void setData(UserItemPojo data) {
        this.data = data;
    }
}
