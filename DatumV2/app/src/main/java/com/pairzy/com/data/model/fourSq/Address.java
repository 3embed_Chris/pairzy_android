package com.pairzy.com.data.model.fourSq;

/**
 * <h>Address data class</h>
 * @author 3Embed.
 * @since  10/5/18.
 * @version 1.0.
 */

public class Address
{
    String Address_title;
    String sub_Address;
    String latitude;
    String logitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLogitude() {
        return logitude;
    }

    public void setLogitude(String logitude) {
        this.logitude = logitude;
    }

    public String getAddress_title() {
        return Address_title;
    }

    public void setAddress_title(String address_title) {
        Address_title = address_title;
    }

    public String getSub_Address() {
        return sub_Address;
    }

    public void setSub_Address(String sub_Address) {
        this.sub_Address = sub_Address;
    }

    @Override
    public String toString() {
        return "Address{" +
                "Address_title='" + Address_title + '\'' +
                ", sub_Address='" + sub_Address + '\'' +
                ", latitude='" + latitude + '\'' +
                ", logitude='" + logitude + '\'' +
                '}';
    }
}
