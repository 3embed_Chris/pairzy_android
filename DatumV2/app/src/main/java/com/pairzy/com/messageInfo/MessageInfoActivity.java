package com.pairzy.com.messageInfo;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.pairzy.com.AppController;
import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.pairzy.com.MqttChat.BlurTransformation.BlurTransformation;
import com.pairzy.com.MqttChat.DocumentPicker.FilePickerConst;
import com.pairzy.com.MqttChat.Utilities.AdjustableImageView;
import com.pairzy.com.MqttChat.Utilities.MqttEvents;
import com.pairzy.com.MqttChat.Utilities.Utilities;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;
import static com.pairzy.com.MqttChat.Utilities.Utilities.convert24to12hourformat;

/**
 * Created by moda on 04/09/17.
 */

public class MessageInfoActivity extends BaseDaggerActivity implements OnMapReadyCallback,MessageInfoContract.View {

    @Inject
    MessageInfoContract.Presenter presenter;
    private Bus bus = AppController.getBus();
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    Activity activity;


    @BindView(R.id.root)
    RelativeLayout root;

    @BindView(R.id.deliveredAt)
    TextView tvDeliveredAt;
    @BindView(R.id.delivered)
    TextView tvDelivered;
    @BindView(R.id.read)
    TextView tvRead;
    @BindView(R.id.readAt)
    TextView tvReadAt;
    @BindView(R.id.message_rl)
    RelativeLayout message_rl;
    @BindView(R.id.image_rl)
    RelativeLayout image_rl;
    @BindView(R.id.video_rl)
    RelativeLayout video_rl;
    @BindView(R.id.location_rl)
    RelativeLayout location_rl;
    @BindView(R.id.contact_rl)
    RelativeLayout contact_rl;
    @BindView(R.id.audio_rl)
    RelativeLayout audio_rl;
    @BindView(R.id.sticker_rl)
    RelativeLayout sticker_rl;
    @BindView(R.id.doodle_rl)
    RelativeLayout doodle_rl;
    @BindView(R.id.gif_rl)
    RelativeLayout gif_rl;
    @BindView(R.id.document_rl)
    RelativeLayout document_rl;

    @BindView(R.id.txtMsg)
    TextView tvMessage;


    private GoogleMap mMap;

    private LatLng positionSelected;

    @BindView(R.id.single_tick_green1)
    ImageView ivSingleTick;
    @BindView(R.id.double_tick_green1)
    ImageView doubleTick;
    @BindView(R.id.double_tick_blue1)
    ImageView blueTick;
    @BindView(R.id.clock1)
    ImageView clock;

    @BindView(R.id.chatBackground)
    ImageView ivChatBackground;

    private String messageId, documentId;

    @BindView(R.id.date1)
    TextView messageDate;

    @BindView(R.id.ts1)
    TextView messageTime;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_info);
        unbinder = ButterKnife.bind(this);
        applyFonts();
        setupActivity(getIntent());

        bus.register(this);
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_container);
        collapsingToolbarLayout.setTitleEnabled(true);
        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.transparent));
        collapsingToolbarLayout.setCollapsedTitleTextColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.datum));
        collapsingToolbarLayout.setTitle(getString(R.string.MessageInfo));

    }



    @Override
    public void applyFonts() {
        tvDelivered.setTypeface(typeFaceManager.getCircularAirBook());
        tvDeliveredAt.setTypeface(typeFaceManager.getCircularAirBook());
        tvRead.setTypeface(typeFaceManager.getCircularAirBook());
        tvReadAt.setTypeface(typeFaceManager.getCircularAirBook());

        messageTime.setTypeface(typeFaceManager.getCircularAirLight());
        messageDate.setTypeface(typeFaceManager.getCircularAirLight());
    }

    @OnClick(R.id.close)
    public void back(){
       onBackPressed();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setupActivity(intent);
    }


    private void minimizeCallScreen(JSONObject obj) {
        try {
            Intent intent = new Intent(MessageInfoActivity.this, ChatMessageActivity.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra("receiverUid", obj.getString("receiverUid"));
            intent.putExtra("receiverName", obj.getString("receiverName"));
            intent.putExtra("documentId", obj.getString("documentId"));

            intent.putExtra("receiverImage", obj.getString("receiverImage"));
            intent.putExtra("colorCode", obj.getString("colorCode"));

            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        presenter.dropView();
        bus.unregister(this);
    }

    @Override
    public void onBackPressed() {
        try {

            if (AppController.getInstance().isActiveOnACall()) {
                if (AppController.getInstance().isCallMinimized()) {
                    super.onBackPressed();
                    supportFinishAfterTransition();
                }
            } else {
                super.onBackPressed();
                supportFinishAfterTransition();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Subscribe
    public void getMessage(JSONObject object) {
        try {
            if (object.getString("eventName").equals("callMinimized")) {
                minimizeCallScreen(object);
            } else if (object.getString("eventName").equals(MqttEvents.Acknowledgement.value + "/" + AppController.getInstance().getUserId())) {
                /*
                 * If for the current tvMessage i have received the delivery acknowledgement
                 */

                if (object.getString("msgId").equals(messageId)) {

/*
 * For type 2 or type 3
 */

                    int status = Integer.parseInt(object.getString("status"));
                    updateMessageDeliveryStatus(status);


                    if (status == 2) {


                        updateDeliveryTime(object.getString("deliveryTime"));
                    } else if (status == 3) {
                        updateReadTime(object.getString("readTime"));
                    }


                }


            } else if (object.getString("eventName").equals(MqttEvents.MessageResponse.value)) {


                if (object.getString("messageId").equals(messageId) && object.getString("docId").equals(documentId)) {

                    updateMessageDeliveryStatus(1);


                }


            }


        } catch (JSONException e)

        {
            e.printStackTrace();
        }
    }


    @SuppressWarnings("TryWithIdenticalCatches,unchecked")

    private void setupActivity(Intent intent) {
        message_rl.setVisibility(View.GONE);
        image_rl.setVisibility(View.GONE);
        video_rl.setVisibility(View.GONE);
        location_rl.setVisibility(View.GONE);
        contact_rl.setVisibility(View.GONE);
        audio_rl.setVisibility(View.GONE);
        sticker_rl.setVisibility(View.GONE);
        doodle_rl.setVisibility(View.GONE);
        gif_rl.setVisibility(View.GONE);
        document_rl.setVisibility(View.GONE);

        Bundle extras = intent.getExtras();

        if (extras != null) {
            documentId = extras.getString("documentId");
            new getWallpaperDetails().execute();
            messageId = extras.getString("messageId");

            Map<String, Object> map = AppController.getInstance()
                    .getDbController().getMessageDetails(documentId, messageId);
            if (map != null) {
                int deliveryStatus = Integer.parseInt((String) map.get("deliveryStatus"));

                String messageContent = (String) map.get("message");

                switch (Integer.parseInt((String) Objects.requireNonNull(map.get("messageType")))) {

                    case 0: {
                    /*
                     * Text tvMessage
                     */

                        message_rl.setVisibility(View.VISIBLE);
                        tvMessage.setText(messageContent);
                        break;

                    }
                    case 1: {
                        /*
                         * Image
                         */
                        image_rl.setVisibility(View.VISIBLE);


                        final AdjustableImageView imageView = (AdjustableImageView) findViewById(R.id.imgshow);

                        ImageView download2 = (ImageView) findViewById(R.id.download);

                        int density = (int) getResources().getDisplayMetrics().density;

                        TextView fnf = (TextView) findViewById(R.id.fnf);
                        int downloadStatus = (int) map.get("downloadStatus");
                        if (downloadStatus == 1) {
/*
 * Already downloaded
 */
                            download2.setVisibility(View.GONE);

                            if (messageContent != null) {


                                try {
                                    if (ActivityCompat.checkSelfPermission(MessageInfoActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                                            == PackageManager.PERMISSION_GRANTED) {

                                        final BitmapFactory.Options options = new BitmapFactory.Options();
                                        options.inJustDecodeBounds = true;
                                        BitmapFactory.decodeFile(messageContent, options);

                                        int height = options.outHeight;
                                        int width = options.outWidth;

                                        int reqHeight;

                                        if (width == 0) {
                                            reqHeight = 150;
                                        } else {
                                            reqHeight = ((150 * height) / width);

                                            if (reqHeight > 150) {
                                                reqHeight = 150;
                                            }
                                        }

                                        try {
                                            Glide
                                                    .with(MessageInfoActivity.this)
                                                    .load(messageContent)
                                                    .override((150 * density), (reqHeight * density))
                                                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
//                                .transition(withCrossFade())
                                                    .centerCrop()
                                                    .placeholder(R.drawable.home_grid_view_image_icon)

                                                    .addListener(new RequestListener<Drawable>() {
                                                        @Override
                                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                            return false;
                                                        }

                                                        @Override
                                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                            imageView.setBackgroundColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.color_white));
                                                            return false;
                                                        }
                                                    })

                                                    .into(imageView);

                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        } catch (NullPointerException e) {
                                            e.printStackTrace();
                                        }


                                    } else {
                                        fnf.setVisibility(View.VISIBLE);
                                        fnf.setText(R.string.string_211);
                                        Glide.with(activity).clear(imageView);
                                        imageView.setImageDrawable(ContextCompat.getDrawable(MessageInfoActivity.this, R.drawable.chat_white_circle));
                                        imageView.setBackgroundColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.color_white));

                                    }


                                } catch (OutOfMemoryError e) {
                                    e.printStackTrace();
                                } catch (Exception e) {


                                    fnf.setVisibility(View.VISIBLE);
                                    Glide.with(activity).clear(imageView);
                                    imageView.setImageDrawable(ContextCompat.getDrawable(MessageInfoActivity.this, R.drawable.chat_white_circle));
                                    imageView.setBackgroundColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.color_white));


                                }
                            } else {


                                Glide.with(activity).clear(imageView);
                                imageView.setImageDrawable(ContextCompat.getDrawable(MessageInfoActivity.this, R.drawable.chat_white_circle));
                                imageView.setBackgroundColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.color_white));
                            }


                        } else {



/*
 *
 *To allow an option to download
 *
 */


                            download2.setVisibility(View.GONE);


                            final BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;


                            BitmapFactory.decodeFile(messageContent, options);


                            int height = options.outHeight;
                            int width = options.outWidth;


                            int reqHeight;


                            if (width == 0) {
                                reqHeight = 150;
                            } else {


                                reqHeight = ((150 * height) / width);


                                if (reqHeight > 150) {
                                    reqHeight = 150;
                                }
                            }

                            try {
                                Glide
                                        .with(MessageInfoActivity.this)
                                        .asBitmap()
                                        .load((String) map.get("thumbnailPath"))
                                        .transform(new CenterCrop(), new BlurTransformation(MessageInfoActivity.this))
                                        .override((150 * density), (density * reqHeight))
                                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                        .placeholder(R.drawable.home_grid_view_image_icon)
                                        .addListener(new RequestListener<Bitmap>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                                                imageView.setBackgroundColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.color_white));
                                                return false;
                                            }
                                        })
                                        .into(imageView);

                            } catch (IllegalArgumentException e) {
                                e.printStackTrace();
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }


                        }


                        messageTime = (TextView) findViewById(R.id.ts2);

                        messageDate = (TextView) findViewById(R.id.date2);


                        ivSingleTick = (ImageView) findViewById(R.id.single_tick_green2);

                        doubleTick = (ImageView) findViewById(R.id.double_tick_green2);

                        blueTick = (ImageView) findViewById(R.id.double_tick_blue2);

                        clock = (ImageView) findViewById(R.id.clock2);




                        break;
                    }
                    case 2: {

                        /*
                         * Video
                         */
                        video_rl.setVisibility(View.VISIBLE);

                        final AdjustableImageView imageView = (AdjustableImageView) findViewById(R.id.vidshow);

                        ImageView download3 = (ImageView) findViewById(R.id.download2);


                        TextView fnf = (TextView) findViewById(R.id.fnf2);

                        int downloadStatus = (int) map.get("downloadStatus");
                        if (downloadStatus == 1) {
                 /*
                     *
                     * image already downloaded
                     *
                     * */
                            download3.setVisibility(View.GONE);

                            try {


                                if (ActivityCompat.checkSelfPermission(MessageInfoActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                                        == PackageManager.PERMISSION_GRANTED) {


                                    final File file = new File(messageContent);


                                    if (file.exists()) {


                                        imageView.setImageBitmap(ThumbnailUtils.createVideoThumbnail(messageContent,
                                                MediaStore.Images.Thumbnails.MINI_KIND));


                                    } else {


                                        Glide.with(activity).clear(imageView);
                                        fnf.setVisibility(View.VISIBLE);
                                        // vh2.fnf.setTypeface(tf, Typeface.NORMAL);
                                        imageView.setImageDrawable(ContextCompat.getDrawable(MessageInfoActivity.this, R.drawable.chat_white_circle));
                                        imageView.setBackgroundColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.color_white));

                                    }
                                } else {

                                    fnf.setVisibility(View.VISIBLE);
                                    Glide.with(activity).clear(imageView);
                                    //   vh2.fnf.setTypeface(tf, Typeface.NORMAL);
                                    fnf.setText(R.string.string_211);
                                    imageView.setImageDrawable(ContextCompat.getDrawable(MessageInfoActivity.this, R.drawable.chat_white_circle));
                                    imageView.setBackgroundColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.color_white));


                                }
                            } catch (OutOfMemoryError e) {
                                e.printStackTrace();
                            } catch (Exception e) {


                                e.printStackTrace();

                            }

                        } else {

                            download3.setVisibility(View.GONE);

                            try {
                                Glide
                                        .with(MessageInfoActivity.this)
                                        .load((String) map.get("thumbnailPath"))
                                        .transform(new CenterCrop(), new BlurTransformation(MessageInfoActivity.this))
                                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                        .placeholder(R.drawable.home_grid_view_image_icon)
                                        .addListener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                imageView.setBackgroundColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.color_white));
                                                return false;
                                            }
                                        })
                                        .into(imageView);

                            } catch (IllegalArgumentException e) {
                                e.printStackTrace();
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }


                        }


                        messageTime = (TextView) findViewById(R.id.ts3);

                        messageDate = (TextView) findViewById(R.id.date3);


                        ivSingleTick = (ImageView) findViewById(R.id.single_tick_green3);

                        doubleTick = (ImageView) findViewById(R.id.double_tick_green3);

                        blueTick = (ImageView) findViewById(R.id.double_tick_blue3);

                        clock = (ImageView) findViewById(R.id.clock3);







                        break;
                    }

                    case 3: {

                        /*
                         * Location
                         */
                        location_rl.setVisibility(View.VISIBLE);


                        MapView mapView = (MapView) findViewById(R.id.map);


                        String args[] = messageContent.split("@@");
                        String LatLng = args[0];

                        String[] parts = LatLng.split(",");

                        String lat = parts[0].substring(1);
                        String lng = parts[1].substring(0, parts[1].length() - 1);

                        parts = null;
                        args = null;
                        positionSelected = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                        mapView.getMapAsync(this);

                        messageTime = (TextView) findViewById(R.id.ts4);

                        messageDate = (TextView) findViewById(R.id.date4);


                        ivSingleTick = (ImageView) findViewById(R.id.single_tick_green4);

                        doubleTick = (ImageView) findViewById(R.id.double_tick_green4);

                        blueTick = (ImageView) findViewById(R.id.double_tick_blue4);

                        clock = (ImageView) findViewById(R.id.clock4);







                        break;
                    }

                    case 4: {

                        /*
                         * contact
                         */

                        contact_rl.setVisibility(View.VISIBLE);


                        TextView contactName = (TextView) findViewById(R.id.contactName);

                        TextView contactNumber = (TextView) findViewById(R.id.contactNumber);


                        String contactNameS, contactNumberS;

                        try {


                            String parts[] = messageContent.split("@@");


                            contactNameS = parts[0];


                            String arr[] = parts[1].split("/");


                            contactNumberS = arr[0];
                            arr = null;
                            parts = null;

                            contactName.setText(contactNameS);

                            contactNumber.setText(contactNumberS);
                            if (contactNameS == null || contactNameS.isEmpty()) {
                                contactName.setText(R.string.string_247);
                            } else if (contactNumberS == null || contactNumberS.isEmpty()) {
                                contactNumber.setText(R.string.string_246);
                            }
                        } catch (StringIndexOutOfBoundsException e) {
                            contactNumber.setText(R.string.string_246);
                        } catch (Exception e) {
                            contactNumber.setText(R.string.string_246);
                        }


                        messageTime = (TextView) findViewById(R.id.ts);

                        messageDate = (TextView) findViewById(R.id.date);


//                    ImageView singleTick5 = (ImageView) findViewById(R.id.single_tick_green);
//
//                    ImageView doubleTickGreen5 = (ImageView) findViewById(R.id.double_tick_green);
//
//                    ImageView doubleTickBlue5 = (ImageView) findViewById(R.id.double_tick_blue);
//
//                    ImageView clock5 = (ImageView) findViewById(R.id.clock);


                        ivSingleTick = (ImageView) findViewById(R.id.single_tick_green);

                        doubleTick = (ImageView) findViewById(R.id.double_tick_green);

                        blueTick = (ImageView) findViewById(R.id.double_tick_blue);

                        clock = (ImageView) findViewById(R.id.clock);







                        break;

                    }
                    case 5: {
                        /*
                         * Audio
                         */

                        audio_rl.setVisibility(View.VISIBLE);
                        messageTime = (TextView) findViewById(R.id.ts6);

                        messageDate = (TextView) findViewById(R.id.date6);


                        ivSingleTick = (ImageView) findViewById(R.id.single_tick_green6);

                        doubleTick = (ImageView) findViewById(R.id.double_tick_green6);

                        blueTick = (ImageView) findViewById(R.id.double_tick_blue6);

                        clock = (ImageView) findViewById(R.id.clock6);


                        ImageView download6 = (ImageView) findViewById(R.id.download6);

                        int downloadStatus = (int) map.get("downloadStatus");
                        if (downloadStatus == 0) {

                            download6.setVisibility(View.VISIBLE);
                        } else {
                            download6.setVisibility(View.GONE);
                        }







                        break;
                    }

                    case 6: {
                        /*
                         * Sticker
                         */

                        sticker_rl.setVisibility(View.VISIBLE);

                        ImageView stickerImage = (ImageView) findViewById(R.id.imgshow2);


                        try {


                            Glide.with(MessageInfoActivity.this)
                                    .load(messageContent)
                                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                    .into(stickerImage);

                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }


                        messageTime = (TextView) findViewById(R.id.ts7);

                        messageDate = (TextView) findViewById(R.id.date7);


                        ivSingleTick = (ImageView) findViewById(R.id.single_tick_green7);

                        doubleTick = (ImageView) findViewById(R.id.double_tick_green7);

                        blueTick = (ImageView) findViewById(R.id.double_tick_blue7);

                        clock = (ImageView) findViewById(R.id.clock7);


                        break;
                    }
                    case 7: {
                        /*
                         * Doodle
                         */
                        doodle_rl.setVisibility(View.VISIBLE);


                        final ImageView doodleImage = (ImageView) findViewById(R.id.imgshow10);

                        ImageView download8 = (ImageView) findViewById(R.id.download10);

                        TextView fnf = (TextView) findViewById(R.id.fnf5);


                        int downloadStatus = (int) map.get("downloadStatus");

                        if (downloadStatus == 1) {


                            if (messageContent != null) {


                                try {

                                    download8.setVisibility(View.GONE);


                                    if (ActivityCompat.checkSelfPermission(MessageInfoActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                                            == PackageManager.PERMISSION_GRANTED) {


                                        try {
                                            Glide
                                                    .with(MessageInfoActivity.this)
                                                    .load(messageContent)

                                                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
//                                .transition(withCrossFade())
                                                    .centerCrop()
                                                    .placeholder(R.drawable.home_grid_view_image_icon)

                                                    .addListener(new RequestListener<Drawable>() {
                                                        @Override
                                                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                            return false;
                                                        }

                                                        @Override
                                                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                            doodleImage.setBackgroundColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.color_white));
                                                            return false;
                                                        }
                                                    })
                                                    .into(doodleImage);

                                        } catch (IllegalArgumentException e) {
                                            e.printStackTrace();
                                        } catch (NullPointerException e) {
                                            e.printStackTrace();
                                        }


                                    } else {

                                        fnf.setVisibility(View.VISIBLE);

                                        // vh2.fnf.setTypeface(tf, Typeface.NORMAL);
                                        fnf.setText(R.string.string_211);
                                        Glide.with(activity).clear(doodleImage);
                                        doodleImage.setImageDrawable(ContextCompat.getDrawable(MessageInfoActivity.this, R.drawable.chat_white_circle));
                                        doodleImage.setBackgroundColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.color_white));


                                    }


                                } catch (OutOfMemoryError e) {
                                    e.printStackTrace();
                                } catch (Exception e) {


                                    fnf.setVisibility(View.VISIBLE);
                                    Glide.with(activity).clear(doodleImage);
                                    doodleImage.setImageDrawable(ContextCompat.getDrawable(MessageInfoActivity.this, R.drawable.chat_white_circle));
                                    doodleImage.setBackgroundColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.color_white));
                                    //  vh2.fnf.setTypeface(tf, Typeface.NORMAL);


                                }
                            } else {
                                Glide.with(activity).clear(doodleImage);
                                doodleImage.setImageDrawable(ContextCompat.getDrawable(MessageInfoActivity.this, R.drawable.chat_white_circle));
                                doodleImage.setBackgroundColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.color_white));
                            }
                        } else {


                            download8.setVisibility(View.GONE);


                            try {
                                Glide
                                        .with(MessageInfoActivity.this)
                                        .asBitmap()
                                        .load((String) map.get("thumbnailPath"))
                                        .transform(new CenterCrop(), new BlurTransformation(MessageInfoActivity.this))
                                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                        .placeholder(R.drawable.home_grid_view_image_icon)
                                        .addListener(new RequestListener<Bitmap>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                                                doodleImage.setBackgroundColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.color_white));
                                                return false;
                                            }
                                        })
                                        .into(doodleImage);

                            } catch (IllegalArgumentException e) {
                                e.printStackTrace();
                            } catch (NullPointerException e) {
                                e.printStackTrace();
                            }


                        }


                        messageTime = (TextView) findViewById(R.id.ts10);

                        messageDate = (TextView) findViewById(R.id.date10);


                        ivSingleTick = (ImageView) findViewById(R.id.single_tick_green10);

                        doubleTick = (ImageView) findViewById(R.id.double_tick_green10);

                        blueTick = (ImageView) findViewById(R.id.double_tick_blue10);

                        clock = (ImageView) findViewById(R.id.clock10);







                        break;
                    }
                    case 8: {
                        /*
                         * Gif
                         */
                        gif_rl.setVisibility(View.VISIBLE);


                        ImageView stillGifImage = (ImageView) findViewById(R.id.stillGifImage);


                        try {
                            Glide.with(MessageInfoActivity.this)
                                    .asBitmap()
                                    .load(messageContent)
                                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                                    .placeholder(R.drawable.home_grid_view_image_icon)
                                    .into(stillGifImage);

                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                        } catch (NullPointerException e) {
                            e.printStackTrace();

                        }


                        messageTime = (TextView) findViewById(R.id.ts9);

                        messageDate = (TextView) findViewById(R.id.date9);


                        ivSingleTick = (ImageView) findViewById(R.id.single_tick_green9);

                        doubleTick = (ImageView) findViewById(R.id.double_tick_green9);

                        blueTick = (ImageView) findViewById(R.id.double_tick_blue9);

                        clock = (ImageView) findViewById(R.id.clock9);






                        break;

                    }
                    case 9: {
                        /*
                         * Document
                         */
                        document_rl.setVisibility(View.VISIBLE);

                        ImageView fileImage = (ImageView) findViewById(R.id.fileImage);


                        TextView fileName = (TextView) findViewById(R.id.fileName);

                        TextView fileType = (TextView) findViewById(R.id.fileType);
                        ImageView download10 = (ImageView) findViewById(R.id.download4);
                        RelativeLayout documentLayout = (RelativeLayout) findViewById(R.id.rl);


                        TextView fnf = (TextView) findViewById(R.id.fnf4);

                        int downloadStatus = (int) map.get("downloadStatus");
                        if (downloadStatus == 1) {


/*
 * Already downloaded
 */

                            download10.setVisibility(View.GONE);


                            if (ActivityCompat.checkSelfPermission(MessageInfoActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                                    == PackageManager.PERMISSION_GRANTED) {


                                final File file = new File(messageContent);


                                if (file.exists()) {
                                    fileName.setText((String) map.get("fileName"));


                                    String fileTypeS = findFileTypeFromExtension((String) map.get("extension"));


                                    fileType.setText(fileTypeS);


                                    fileType.setVisibility(View.VISIBLE);


                                    if (fileTypeS.equals(FilePickerConst.PDF)) {

                                        fileImage.setImageResource(R.drawable.ic_pdf);

                                    } else if (fileTypeS.equals(FilePickerConst.DOC)) {

                                        fileImage.setImageResource(R.drawable.ic_word);
                                    } else if (fileTypeS.equals(FilePickerConst.PPT)) {
                                        fileImage.setImageResource(R.drawable.ic_ppt);
                                    } else if (fileTypeS.equals(FilePickerConst.XLS)) {
                                        fileImage.setImageResource(R.drawable.ic_excel);
                                    } else if (fileTypeS.equals(FilePickerConst.TXT)) {
                                        fileImage.setImageResource(R.drawable.ic_txt);
                                    }
                                    documentLayout.setVisibility(View.VISIBLE);


                                } else {


                                    fnf.setVisibility(View.VISIBLE);
                                    documentLayout.setVisibility(View.GONE);


                                    fileType.setVisibility(View.GONE);
                                }
                            } else {

                                documentLayout.setVisibility(View.GONE);


                                fileType.setVisibility(View.GONE);
                                fnf.setVisibility(View.VISIBLE);
                                fnf.setText(R.string.string_211);


                            }

                        } else {

/*
 *
 *To allow an option to download
 *
 */


                            String fileTypeS = findFileTypeFromExtension((String) map.get("extension"));

                            fileName.setText((String) map.get("fileName"));

                            fileType.setText(fileTypeS);
                            if (fileTypeS.equals(FilePickerConst.PDF)) {

                                fileImage.setImageResource(R.drawable.ic_pdf);

                            } else if (fileTypeS.equals(FilePickerConst.DOC)) {

                                fileImage.setImageResource(R.drawable.ic_word);
                            } else if (fileTypeS.equals(FilePickerConst.PPT)) {
                                fileImage.setImageResource(R.drawable.ic_ppt);
                            } else if (fileTypeS.equals(FilePickerConst.XLS)) {
                                fileImage.setImageResource(R.drawable.ic_excel);
                            } else if (fileTypeS.equals(FilePickerConst.TXT)) {
                                fileImage.setImageResource(R.drawable.ic_txt);
                            }
                            download10.setVisibility(View.GONE);

                        }


                        messageTime = (TextView) findViewById(R.id.ts8);

                        messageDate = (TextView) findViewById(R.id.date8);


                        ivSingleTick = (ImageView) findViewById(R.id.single_tick_green8);

                        doubleTick = (ImageView) findViewById(R.id.double_tick_green8);

                        blueTick = (ImageView) findViewById(R.id.double_tick_blue8);

                        clock = (ImageView) findViewById(R.id.clock8);







                        break;
                    }


                }

                if (deliveryStatus == 3) {

                    clock.setVisibility(View.GONE);
                    ivSingleTick.setVisibility(View.GONE);

                    doubleTick.setVisibility(View.GONE);
                    blueTick.setVisibility(View.VISIBLE);

                } else if (deliveryStatus == 2) {
                    clock.setVisibility(View.GONE);
                    ivSingleTick.setVisibility(View.GONE);

                    doubleTick.setVisibility(View.VISIBLE);
                    blueTick.setVisibility(View.GONE);
                } else if (deliveryStatus == 1) {

                    clock.setVisibility(View.GONE);
                    ivSingleTick.setVisibility(View.VISIBLE);

                    doubleTick.setVisibility(View.GONE);
                    blueTick.setVisibility(View.GONE);
                } else {


                    clock.setVisibility(View.VISIBLE);
                    ivSingleTick.setVisibility(View.GONE);

                    doubleTick.setVisibility(View.GONE);
                    blueTick.setVisibility(View.GONE);
                }


                setMessageTime((String) map.get("Ts"));


                updateDeliveryTime((String) map.get("deliveryTime"));

                updateReadTime((String) map.get("readTime"));

            } else {
                showMessage(getString(R.string.NotFound));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                }, 500);
            }
        }
    }

    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(root,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        mMap.getUiSettings().setMapToolbarEnabled(true);

        mMap.addMarker(new MarkerOptions().position(positionSelected).title(""));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(positionSelected, 16.0f));


    }


    private void updateMessageDeliveryStatus(int deliveryStatus) {
        try {
            if (deliveryStatus == 3) {

                clock.setVisibility(View.GONE);
                ivSingleTick.setVisibility(View.GONE);

                doubleTick.setVisibility(View.GONE);
                blueTick.setVisibility(View.VISIBLE);

            } else if (deliveryStatus == 2) {
                clock.setVisibility(View.GONE);
                ivSingleTick.setVisibility(View.GONE);

                doubleTick.setVisibility(View.VISIBLE);
                blueTick.setVisibility(View.GONE);
            } else if (deliveryStatus == 1) {

                clock.setVisibility(View.GONE);
                ivSingleTick.setVisibility(View.VISIBLE);

                doubleTick.setVisibility(View.GONE);
                blueTick.setVisibility(View.GONE);
            } else {


                clock.setVisibility(View.VISIBLE);
                ivSingleTick.setVisibility(View.GONE);

                doubleTick.setVisibility(View.GONE);
                blueTick.setVisibility(View.GONE);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }


    private void updateDeliveryTime(String deliveryTime) {


        if (deliveryTime != null) {

            try {
                String tempDeliveryTime = Utilities.changeStatusDateFromGMTToLocal(Utilities.epochtoGmt(deliveryTime));

                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);

                Date date2 = new Date(System.currentTimeMillis() - AppController.getInstance().getTimeDelta());
                String current_date = sdf.format(date2);

                current_date = current_date.substring(0, 8);


                if (tempDeliveryTime != null) {


                    if (current_date.equals(tempDeliveryTime.substring(0, 8))) {

                        tempDeliveryTime = convert24to12hourformat(tempDeliveryTime.substring(8, 10) + ":" + tempDeliveryTime.substring(10, 12));


                        deliveryTime = "Today " + tempDeliveryTime;


                    } else {

                        String last = convert24to12hourformat(tempDeliveryTime.substring(8, 10) + ":" + tempDeliveryTime.substring(10, 12));


                        String date = tempDeliveryTime.substring(6, 8) + "-" + tempDeliveryTime.substring(4, 6) + "-" + tempDeliveryTime.substring(0, 4);

                        deliveryTime = date + " " + last;


                        last = null;
                        date = null;

                    }


                }


                tvDeliveredAt.setText(deliveryTime);
                tempDeliveryTime = null;
                sdf = null;
                date2 = null;
                current_date = null;
            } catch (Exception e) {

                tvDeliveredAt.setText(deliveryTime);
            }
        } else {
            tvDeliveredAt.setText(getString(R.string.NotDelivered));

        }

    }

    private void updateReadTime(String readTime) {
        if (readTime != null) {
            try {


                String tempReadTime = Utilities.changeStatusDateFromGMTToLocal(Utilities.epochtoGmt(readTime));

                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);

                Date date2 = new Date(System.currentTimeMillis() - AppController.getInstance().getTimeDelta());
                String current_date = sdf.format(date2);

                current_date = current_date.substring(0, 8);


                if (tempReadTime != null) {


                    if (current_date.equals(tempReadTime.substring(0, 8))) {

                        tempReadTime = convert24to12hourformat(tempReadTime.substring(8, 10) + ":" + tempReadTime.substring(10, 12));


                        readTime = "Today " + tempReadTime;


                    } else {

                        String last = convert24to12hourformat(tempReadTime.substring(8, 10) + ":" + tempReadTime.substring(10, 12));


                        String date = tempReadTime.substring(6, 8) + "-" + tempReadTime.substring(4, 6) + "-" + tempReadTime.substring(0, 4);

                        readTime = date + " " + last;


                        last = null;
                        date = null;

                    }


                }


                tvReadAt.setText(readTime);
                tempReadTime = null;
                sdf = null;
                date2 = null;
                current_date = null;


            } catch (Exception e) {

                tvReadAt.setText(readTime);
            }
        } else {
            tvReadAt.setText(getString(R.string.NotRead));

        }

    }

    private String findFileTypeFromExtension(String extension) {


        if (extension.equals("pdf")) {
            return FilePickerConst.PDF;
        } else if (extension.equals("doc") || extension.equals("docx") || extension.equals("dot") || extension.equals("dotx")) {
            return FilePickerConst.DOC;
        } else if (extension.equals("ppt") || extension.equals("pptx")) {
            return FilePickerConst.PPT;
        } else if (extension.equals("xls") || extension.equals("xlsx")) {
            return FilePickerConst.XLS;
        } else if (extension.equals("txt")) {
            return FilePickerConst.TXT;
        } else return "UNKNOWN";
    }


    private void setMessageTime(String ts) {

        ts = Utilities.formatDate(Utilities.tsFromGmt(ts));
        messageDate.setText(Utilities.findOverlayDate(ts.substring(9, 24)) + " ");


        messageTime.setText(convert24to12hourformat(ts.substring(0, 9)) + " ");

    }


    @SuppressWarnings("TryWithIdenticalCatches")
    private class getWallpaperDetails extends AsyncTask {


        @Override
        protected Object doInBackground(Object[] params) {


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    try {
                        Map<String, Object> map = AppController.getInstance().getDbController().getWallpaperDetails(documentId);


                        String wallpaperData = null;


                        if (map.get("wallpaperData") != null) {


                            wallpaperData = (String) map.get("wallpaperData");
                        }

                        updateWallpaper((int) map.get("wallpaperType"), wallpaperData);
                    } catch (Exception e) {
                        updateWallpaper(1, null);

                    }

                }
            });

            return null;
        }
    }

    private void updateWallpaper(int type, final String wallpaperDetails) {
        switch (type) {
            case 0: {
/*
 * Solid color
 */
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ivChatBackground.setImageDrawable(null);
                        ivChatBackground.setBackgroundColor(Color.parseColor(wallpaperDetails));
                    }
                });

                break;
            }

            case 1: {
                /*
                 *Default
                 */


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {


                                Glide.with(MessageInfoActivity.this)
                                        .load(R.drawable.chat_background)
                                        .transition(withCrossFade())

                                        .centerCrop()


                                        .into(ivChatBackground);
                            }
                        });
                    }
                });


                break;
            }

            case 2: {
/*
 *No wallpaper
 */

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ivChatBackground.setImageDrawable(null);
                        ivChatBackground.setBackgroundColor(ContextCompat.getColor(MessageInfoActivity.this, R.color.color_white));

                    }
                });


                break;
            }

            case 3: {
                /*
                 *Image from gallery or camera
                 */
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        Glide.with(MessageInfoActivity.this)
                                .load(wallpaperDetails)
                                .transition(withCrossFade())

                                .centerCrop()


                                .into(ivChatBackground);
                    }
                });


                break;
            }


            case 4: {
                /*
                 *Image from api call
                 */
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        Glide.with(MessageInfoActivity.this)
                                .load(wallpaperDetails)
                                .transition(withCrossFade())

                                .centerCrop()


                                .into(ivChatBackground);
                    }
                });


                break;
            }


            case 5: {
                /*
                 * Doodle drawn
                 */
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        Glide.with(MessageInfoActivity.this)
                                .load(wallpaperDetails)
                                .transition(withCrossFade())
                                .centerCrop()
                                .into(ivChatBackground);
                    }
                });


                break;
            }

        }
    }


}
