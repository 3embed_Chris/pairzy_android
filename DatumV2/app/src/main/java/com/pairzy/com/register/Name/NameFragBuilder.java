package com.pairzy.com.register.Name;
import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * <h2>NameFragBuilder</h2>
 * <P>
 *
 * </P>
 * @since  2/16/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface NameFragBuilder
{

    @FragmentScoped
    @Binds
    NameFragment getEmailFragment(NameFragment nameFragment);

    @FragmentScoped
    @Binds
    NameContract.Presenter taskPresenter(NameFrgPresenter presenter);
}
