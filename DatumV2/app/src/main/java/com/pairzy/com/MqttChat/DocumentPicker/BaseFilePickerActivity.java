package com.pairzy.com.MqttChat.DocumentPicker;

/**
 * Created by moda on 22/08/17.
 */

import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.pairzy.com.MqttChat.DocumentPicker.Utils.Orientation;


public abstract class BaseFilePickerActivity extends AppCompatActivity {


    public Typeface circularAirBold;
    public Typeface circularAirBook;

    protected void onCreate(@Nullable Bundle savedInstanceState, @LayoutRes int layout) {
        super.onCreate(savedInstanceState);
        circularAirBold= Typeface.createFromAsset(getAssets(),"fonts/CircularAir-Bold.otf");
        circularAirBook=Typeface.createFromAsset(getAssets(),"fonts/CircularAir-Book.otf");
        //setTheme(PickerManager.getInstance().getTheme());
        setContentView(layout);
        //set orientation
        Orientation orientation = PickerManager.getInstance().getOrientation();
        if(orientation==Orientation.PORTRAIT_ONLY)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else if(orientation==Orientation.LANDSCAPE_ONLY)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        initView();
    }

    protected abstract void initView();
}