package com.pairzy.com.home.Discover.Model;

import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigResponse;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PassportLocationDataSource;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Discover.DiscoveryFragPresenter;
import com.pairzy.com.home.Discover.DiscoveryFragUtil;
import com.pairzy.com.home.HomeModel.LoadMoreStatus;
import com.pairzy.com.home.HomeUtil;
import com.pairzy.com.locationScreen.model.LocationHolder;
import com.pairzy.com.passportLocation.model.PassportLocation;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.Utility;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * <h2>DiscoveryModel</h2>
 * @since  3/9/2018.
 * @author 3Embed.
 * @version 1.0.
 */

public class DiscoveryModel extends BaseModel
{
    @Named(HomeUtil.LOAD_MORE_STATUS)
    @Inject
    LoadMoreStatus loadMoreStatus;
    @Inject
    Utility utility;
    @Inject
    @Named(DiscoveryFragUtil.USER_LIST)
    ArrayList<UserItemPojo> userList;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    PassportLocationDataSource locationDataSource;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Geocoder geocoder;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    List<UnifiedNativeAd> mNativeAds;

    @Inject
    DiscoveryModel(){}
    /*
     * Parsing the response.*/
    public void parseUserData(String response)
    {
        try
        {
            SearchUserPojo searchUserPojo=utility.getGson().fromJson(response,SearchUserPojo.class);
            if(searchUserPojo.getData()!=null)
            {
                int page_position=0;
                boolean  no_more = searchUserPojo.getData().size()!=20;
                loadMoreStatus.setNo_more_data(no_more);
                for (UserItemPojo temp:searchUserPojo.getData())
                {
                    manageMediaList(temp);
                }
                if(DiscoveryFragPresenter.PAGE_COUNT==0)
                {
                    page_position=0;
                    userList.clear();

                }else
                {
                    page_position=userList.size()-1;
                    removeLoadMore();
                }
                userList.addAll(searchUserPojo.getData());
                prefetchImage(page_position);

                //save extra data
                dataSource.setRemainsLinksCount(searchUserPojo.getRemainsLikesCount());
                dataSource.setRemainsRewindsCount(searchUserPojo.getRemainsRewindCount());
                dataSource.setNextLikeTime(searchUserPojo.getNextLikeTime());
                dataSource.setBoostExpireTime(searchUserPojo.getBoostResponse().getExpire());
            }else
            {
                loadMoreStatus.setNo_more_data(true);
            }
        }catch (Exception e)
        {
            //Log.d("Error:",""+e.getMessage());
        }
    }

    /**
     *Storing the media file in a local list
     * to mange the media file in local list. */
    public void manageMediaList(UserItemPojo item)
    {
        ArrayList<UserMediaPojo> media_list = new ArrayList<>();
        if(!TextUtils.isEmpty(item.getProfileVideoThumbnail()))
        {
            UserMediaPojo video_item=new UserMediaPojo();
            video_item.setVideo_thumbnail(item.getProfileVideoThumbnail());
            video_item.setVideo_url(item.getProfileVideo());
            video_item.setVideo(true);
            media_list.add(video_item);
        }

        /*
         *Adding the user image in list */
        UserMediaPojo user_image=new UserMediaPojo();
        user_image.setImage_url(item.getProfilePic());
        user_image.setVideo(false);
        media_list.add(user_image);

        try
        {
            for(String other_img:item.getOtherImages())
            {
                UserMediaPojo other_media=new UserMediaPojo();
                other_media.setImage_url(other_img);
                other_media.setVideo(false);
                media_list.add(other_media);
            }
        }catch (Exception e){}
        item.setMedia_list(media_list);
    }

    /*
     *Checking user is there or not. */
    public boolean checkUserExist()
    {
        return userList.size()>0;
    }

    /**
     *Method is controller for the image pre fetch.
     * @param position contains the position from where we start prefatch.*/
    public void prefetchImage(int position)
    {
        if(position<0)
        {
            position=0;
        }

        int end_position=position+5;
        if(end_position>userList.size())
        {
            end_position=userList.size();
        }

        try
        {
            List<UserItemPojo> temp_list=userList.subList(position,end_position);
            for(UserItemPojo item:temp_list)
            {
                if(!item.isLoading())
                {
                    prefetch_Image(item.getProfilePic());
                    ArrayList<String> otherImage=item.getOtherImages();
                    if(otherImage!=null&&otherImage.size()>0)
                    {
                        for(String url:otherImage)
                        {
                            prefetch_Image(url);
                        }
                    }
                }
            }
        }catch (Exception e){}

        int start=position-5;
        if(start<0)
        {
            start=0;
        }

        try
        {
            if(start<position)
            {
                List<UserItemPojo> temp_list=userList.subList(start,position);
                for(UserItemPojo item:temp_list)
                {
                    if(!item.isLoading())
                    {
                        prefetch_Image(item.getProfilePic());
                        ArrayList<String> otherImage=item.getOtherImages();
                        if(otherImage!=null&&otherImage.size()>0)
                        {
                            for(String url:otherImage)
                            {
                                prefetch_Image(url);
                            }
                        }
                    }
                }
            }
        }catch (Exception e){}
    }

    /*
     * removing the load more data.*/
    private void removeLoadMore()
    {
        try
        {
            if(userList.size()>0)
            {
                UserItemPojo last_item=userList.get(userList.size()-1);
                if(last_item.isLoading())
                {
                    userList.remove(last_item);
                }
            }
        }catch (Exception e){}
    }

    /*
     * Failed on load more.*/
    public boolean handleFailedLoadMore()
    {
        try
        {
            if(userList.size()>0)
            {
                UserItemPojo last_item=userList.get(userList.size()-1);
                if(last_item.isLoading())
                {
                    last_item.setLoadingFailed(true);
                    return true;
                }
            }
        }catch (Exception e){}
        return false;
    }

    /**
     * It prefetch the images for loaded item
     * @see Fresco#getImagePipeline()
     */
    private void prefetch_Image(String pic_url)
    {
        ImagePipeline pipeline = Fresco.getImagePipeline();
        Uri mainUri = Uri.parse(pic_url);
        ImageRequest mainImageRequest = ImageRequestBuilder
                .newBuilderWithSource(mainUri)
                .build();
        pipeline.prefetchToDiskCache(mainImageRequest, null);
    }

    /*
     *Creating the form data for like service. */
    public Map<String, Object> setUpdateLocationDetails(double lat,double lng,boolean isPassported)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.UpdateLocationReqKey.LATITUDE,lat);
        map.put(ApiConfig.UpdateLocationReqKey.LONGITUDE,lng);
        map.put(ApiConfig.UpdateLocationReqKey.IP,utility.getLocalIpAddress());
        map.put(ApiConfig.UpdateLocationReqKey.TIMEZONE,utility.getLocalTimeZoneId());
        map.put(ApiConfig.UpdateLocationReqKey.IS_PASSPORT_LOCATION,isPassported);
        return map;
    }

    //location related methods.

    /**
     * Save Current Location to the SharedPreference.
     * @param currentLocation
     */
    public void saveCurrentLocation(PassportLocation currentLocation) {
        try {
            String currentLocationJson = utility.getGson().toJson(currentLocation);
            locationDataSource.setCurrentLocation(currentLocationJson);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //we need to use this after impl of automatic location patch to server
    /*
     * it will check for feature location first then saved current location.
     */
    public PassportLocation getSavedLocation()
    {
        try {
            String locationJson = locationDataSource.getFeatureLocation();
            boolean currentLoc = false;
            if(!TextUtils.isEmpty(locationJson)) {
                PassportLocation featureLocation = utility.getGson().fromJson(locationJson, PassportLocation.class);
                if(!TextUtils.isEmpty(featureLocation.getLocationName())){
                    return featureLocation;
                }
                else {
                    currentLoc = true;
                }
            }
            else{
                currentLoc = true;
            }

            if(currentLoc){ //return saved current location.
                PassportLocation currentLocation = parseCurrentLocation();
                return currentLocation;
            }
        }catch (Exception e){
        }
        return null;
    }


    /*
     * Fetch the saved currentLocation object.
     */
    public PassportLocation parseCurrentLocation() {
        try {
            String currentLocationJson = locationDataSource.getCurrentLocation();
            PassportLocation currentLocation = utility.getGson().fromJson(currentLocationJson, PassportLocation.class);
            return currentLocation;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Fetch the full address via lat and long.
     * @param locationHolder contain lat , long.
     * @return PassportLocation.
     */
    public PassportLocation getLocationName(LocationHolder locationHolder) {
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(locationHolder.getLatitude(),locationHolder.getLongitude(),1);
            if(addresses != null){
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String addressName = addresses.get(0).getFeatureName();
                boolean cityEmpty = TextUtils.isEmpty(city);
                boolean stateEmpty = TextUtils.isEmpty(state);
                boolean countryEmpty = TextUtils.isEmpty(country);
                String subAddress = ((cityEmpty)?"":city+((stateEmpty)?"":","))
                        +((stateEmpty)?"":state+((countryEmpty)?"":","))
                        +((countryEmpty)?"":country+".");
                PassportLocation currentLocation = new PassportLocation();
                currentLocation.setLocationName(addressName);
                currentLocation.setSubLocationName(subAddress);
                currentLocation.setLatitude(locationHolder.getLatitude());
                currentLocation.setLongitude(locationHolder.getLongitude());
                return currentLocation;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    public PassportLocation getFeatureLocation()
    {
        try
        {
            String locationJson = locationDataSource.getFeatureLocation();
            PassportLocation featureLocation = utility.getGson().fromJson(locationJson, PassportLocation.class);
            if(!TextUtils.isEmpty(featureLocation.getLocationName()))
                return featureLocation;
        }catch (Exception e){}
        return null;
    }



    public void parseCoinConfig(String response)
    {
        try
        {
            CoinConfigResponse coinConfigResponse =
                    utility.getGson().fromJson(response,CoinConfigResponse.class);
            if(!coinConfigResponse.getData().isEmpty())
                coinConfigWrapper.setCoinData(coinConfigResponse.getData().get(0));
        }catch (Exception e){}
    }

    public boolean isDialogDontNeedToShow() {
        return dataSource.getShowSuperlikeCoinDialog();
    }
    public boolean isDialogDontNeedToShowForChat() {
        return dataSource.getShowChatCoinDialog();
    }

    public void updateShowCoinDialogForChat(boolean dontShowAgain) {
        //update don't show pref
        dataSource.setShowChatCoinDialog(dontShowAgain);
    }
    public void updateSpendCoinShowPref(boolean dontShowAgain) {
        //update don't show pref
        dataSource.setShowSuperlikeCoinDialog(dontShowAgain);
    }


    public UserItemPojo parseRewindResponse(String response)
    {
        RewindResponsePojo searchUserPojo=utility.getGson().fromJson(response,RewindResponsePojo.class);
        return searchUserPojo.getData();
    }


    public boolean isPassportted()
    {
        return locationDataSource.isFeaturesLocActive();
    }


    public UserItemPojo getUserProfile(int position) throws EmptyData {
        return userList.get(position);
    }

    public boolean isProfileBoosted() {
        return dataSource.getBoostExpireTime() > System.currentTimeMillis();
    }

    public void insertAdsInMenuItems() {
        if (mNativeAds.size() <= 0) {
            return;
        }
        int offset = 5;
        int index = DiscoveryFragPresenter.PAGE_COUNT;
        index = (index==0)?offset-1:index-1;
        for (UnifiedNativeAd ad: mNativeAds) {
            UserItemPojo userItemPojo = new UserItemPojo();
            userItemPojo.setAdView(true);
            userItemPojo.setAd(ad);
            if(index <  userList.size())
                userList.add(index, userItemPojo);
            index = index + offset;
        }
    }

    public void injectBannerAds(int pageCount, int limit) {
        int start = 0;
        int listSize = userList.size();
        int end = listSize;
        if(!userList.isEmpty()){
            if(userList.size() == limit){
                end = limit;
                start = listSize-limit;
            }
            else{
                end = listSize;
                if(listSize < limit)
                    start = 0;
                else if(listSize> limit){
                    int extra  = listSize % limit;
                    start = userList.size() - extra;
                }
            }
            try {
                for (int i = start; i < end; i++) {
                    if(i % 4 == 0){
                        UserItemPojo userItemPojo = new UserItemPojo();
                        userItemPojo.setAdView(true);
                        userList.add(i,userItemPojo);
                    }
                }
            }catch (Exception e){}
            Log.d("DiscoveryModel", "injectBannerAds: start, end"+start+" ,"+end);

        }
    }

    public boolean isEnoughWalletBalanceToChat() {

        if(coinBalanceHolder.getCoinBalance() != null && coinConfigWrapper.getCoinData() != null){
            int coinRequired = coinConfigWrapper.getCoinData().getPerMsgWithoutMatch().getCoin();
            int walletBalance = Integer.parseInt(coinBalanceHolder.getCoinBalance());

            if(coinRequired <= walletBalance){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public String getCoinForUnmatchChat() {
        try{
            return ""+coinConfigWrapper.getCoinData().getPerMsgWithoutMatch().getCoin();
        }catch (Exception ignored){}
        return "";
    }

    public String getCoinBalance() {
        return coinBalanceHolder.getCoinBalance();
    }
}
