package com.pairzy.com.MyProfile.editEmail;

import android.text.TextUtils;

import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.register.Email.EmailModel;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.Utility;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by ankit on 27/4/18.
 */

public class EditEmailPresenter implements EditEmailContract.Presenter {

    @Inject
    EditEmailContract.View view;
    @Inject
    Utility utility;
    @Inject
    DatumProgressDialog progressDialog;
    @Inject
    EmailModel emailModel;
    @Inject
    NetworkService service;

    private CompositeDisposable compositeDisposable;
    @Inject
    EditEmailPresenter(){
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public boolean validateEmail(String mail)
    {
        return !TextUtils.isEmpty(mail) && emailModel.isValidEmail(mail);
    }

    @Override
    public void checkEmailIdExist(String email)
    {
        progressDialog.show();
        service.checkEmailAvailability(emailModel.getAuthorization(),
                emailModel.getLanguage(),
                emailModel.verifyParams(email))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> result)
                    {
                        progressDialog.cancel();
                        try {
                            if(result.code()==200)
                            {
                                if(view!=null)
                                    view.emailNotAvailable();
                            }else if(result.code()==412)
                            {
                                if(view!=null)
                                    view.finishActivity(email);
                            }else
                            {
                                if(view!=null)
                                    view.showError(emailModel.getError(result));
                            }
                        } catch (Exception e) {

                            if(view!=null)
                                view.showError(e.getMessage());
                        }

                    }
                    @Override
                    public void onError(Throwable errorMsg)
                    {
                        progressDialog.cancel();
                        if(view==null)
                            return;
                        view.showError(errorMsg.getMessage());

                    }
                    @Override
                    public void onComplete()
                    {}
                });
    }
}
