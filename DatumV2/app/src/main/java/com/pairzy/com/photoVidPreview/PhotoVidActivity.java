package com.pairzy.com.photoVidPreview;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.transition.Explode;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import com.androidinsta.com.ImageData;
import com.pairzy.com.R;
import com.pairzy.com.util.DraweeTransform;
import com.pairzy.com.util.TypeFaceManager;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h>PhotoVidActivity class</h>
 * @author 3Embed.
 * @since 12/5/18.
 * @version 1.0.
 */

public class PhotoVidActivity extends DaggerAppCompatActivity implements PhotoVidContract.View,
        ViewPager.OnPageChangeListener{

    @Inject
    PhotoVidContract.Presenter presenter;
    @Inject
    PhotoPagerAdapter photoPagerAdapter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    ArrayList<ImageData> photoList;


    @BindView(R.id.video_view)
    VideoView videoView;
    @BindView(R.id.my_image_view)
    SimpleDraweeView imageView;
    @BindView(R.id.view_pager_photo)
    ViewPager viewPagerPhoto;
    @BindView(R.id.tv_photo_counter)
    TextView tvPhotoCounter;

    private Unbinder unbinder;
    private MediaController mediaController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setSharedElementEnterTransition(DraweeTransform.createTransitionSet());
            }
        }
        setContentView(R.layout.actvity_photo_vid_preview);
        unbinder = ButterKnife.bind(this);
        initData(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }catch (Exception e){}
    }

    @OnClick(R.id.btn_back)
    public void onBack(){
        videoView.stopPlayback();
        supportFinishAfterTransition();
        onBackPressed();
    }

    private void initData(Intent intent) {
        ArrayList<ImageData> photoMediaList= intent.getParcelableArrayListExtra("photo_list");
        int position = intent.getIntExtra("position",0);

        if(photoMediaList != null){
            viewPagerPhoto.setVisibility(View.VISIBLE);
            photoList.clear();
            photoList.addAll(photoMediaList);
            if(!photoList.isEmpty()){
                if(tvPhotoCounter != null)
                    tvPhotoCounter.setText(String.format(Locale.getDefault(),"%d / %d",position+1,photoList.size()));
            }
            viewPagerPhoto.setAdapter(photoPagerAdapter);
            viewPagerPhoto.setCurrentItem(position);
        }else {
            viewPagerPhoto.setVisibility(View.GONE);
            String mediaUrl = intent.getStringExtra("media_url");
            if (mediaUrl.contains(".mp4")) {
                String videoThumb = intent.getStringExtra("media_thumb");
                imageView.setImageURI(Uri.parse(videoThumb));
                mediaController = new MediaController(this);
                videoView.setMediaController(mediaController);
                videoView.setVideoURI(Uri.parse(mediaUrl));
                videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        imageView.setImageURI(Uri.EMPTY);
                        imageView.setVisibility(View.GONE);
                    }
                });
                videoView.start();
            } else if (!mediaUrl.isEmpty()) {
                imageView.setImageURI(Uri.parse(mediaUrl));
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // set an exit transition
            getWindow().setExitTransition(new Explode());
        }
        ActivityCompat.finishAfterTransition(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(viewPagerPhoto != null)
            viewPagerPhoto.addOnPageChangeListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(viewPagerPhoto != null)
            viewPagerPhoto.removeOnPageChangeListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if(tvPhotoCounter != null)
            tvPhotoCounter.setText(String.format(Locale.getDefault(),"%d / %d",position+1,photoList.size()));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
