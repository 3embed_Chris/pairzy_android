package com.pairzy.com.home.Dates.Model;

/**
 * Created by ankit on 22/8/18.
 */

public enum DateType {
    VIDEO_DATE(1),
    IN_PERSON_DATE(2),
    AUDIO_DATE(3);
    int value;
    DateType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
