package com.pairzy.com.util.ProgressAleret;

/**
 * @since 2/15/2018.
 */
interface ProgressDialogCallback
{
    void onCanceled();
    void onShowing();
}
