package com.pairzy.com.userProfile.Model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
/**
 * <h2>OptionsItemHolder</h2>
 * <P>
 *  Holder class for the list type of item.
 * </P>
 *@since  3/26/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class OptionsItemHolder extends RecyclerView.ViewHolder
{
    TextView title;
    TextView selected_test;
    TextView count_data;

    public OptionsItemHolder(View itemView, TypeFaceManager typeFaceManager)
    {
        super(itemView);
        title=itemView.findViewById(R.id.title);
        title.setTypeface(typeFaceManager.getCircularAirLight());
        selected_test=itemView.findViewById(R.id.selected_text);
        selected_test.setTypeface(typeFaceManager.getCircularAirBook());
        count_data=itemView.findViewById(R.id.count_data);
        count_data.setTypeface(typeFaceManager.getCircularAirBook());
    }
}
