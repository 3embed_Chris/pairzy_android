package com.pairzy.com.home.AppSetting;

import android.app.Activity;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.CustomObserver.SettingsDataChangeObserver;
import com.pairzy.com.util.SuggestionAleret.SuggestionAlertCallback;
import com.pairzy.com.util.SuggestionAleret.SuggestionDialog;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.pairzy.com.util.ApiConfig.SuggestionAlert.DESCRIPTION;

/**
 * <h2>AppSettingPresenter</h2>
 * <P>
 *
 * </P>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class AppSettingPresenter implements AppSettingContract.Presenter,SuggestionAlertCallback
{
    private AppSettingContract.View view;
    private CompositeDisposable compositeDisposable;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    SuggestionDialog suggestionDialog;
    @Inject
    NetworkService service;
    @Inject
    Activity activity;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    SettingsDataChangeObserver dataChangeObserver;

    @Inject
    AppSettingPresenter(){
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void takeView(Object view)
    {
        this.view= (AppSettingContract.View) view;
    }

    @Override
    public void dropView()
    {
      view=null;
    }

    @Override
    public void initSettingsDataChangeObserver() {
        dataChangeObserver.getObservable().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        if(aBoolean){
                            if(view != null)
                                view.initDataChange();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void showSuggestionAlert()
    {
      suggestionDialog.showAlert(this);
    }

    @Override
    public void showBoostDialog()
    {
        if(view!=null)
            view.openBootDialog();
    }


    @Override
    public void onReport(String name, String email, String reason)
    {
        Map<String,Object> mapBody = new HashMap<>();
        mapBody.put(DESCRIPTION, reason);
        service.postReportIssue(dataSource.getToken(),"en",mapBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getSuggestionAlertObserver());
    }

    @Override
    public void onSuggestion(String name, String email, String reason)
    {
        Map<String,Object> mapBody = new HashMap<>();
        mapBody.put(DESCRIPTION, reason);
        service.postMakeSuggestion(dataSource.getToken(),"en",mapBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getSuggestionAlertObserver());
    }

    @Override
    public void onQuestion(String name, String email, String reason)
    {

        Map<String,Object> mapBody = new HashMap<>();
        mapBody.put(DESCRIPTION, reason);
        service.postAskQuestion(dataSource.getToken(),"en",mapBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getSuggestionAlertObserver());
    }

    private SingleObserver<Response<ResponseBody>> getSuggestionAlertObserver(){
        return new SingleObserver<Response<ResponseBody>>(){
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(Response<ResponseBody> value) {
                if(value.code() == 200){
                            /*Successful*/
                }
                else if(value.code() == 401){
                    AppController.getInstance().appLogout();
                }
                else{
                    try {
                        if (view != null)
                            view.showError(value.errorBody().string());
                    }catch (Exception e){}
                }
            }
            @Override
            public void onError(Throwable e) {
                if (view != null)
                    view.showError(activity.getString(R.string.no_internet_error));
            }
        };
    }
    @Override
    public void onSuggestionCanceled(){}

}
