package com.pairzy.com.home.Matches.PostFeed;

import android.app.Activity;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Matches.PostFeed.Model.PostListAdapter;
import com.pairzy.com.home.Matches.PostFeed.Model.PostListPojo;
import com.pairzy.com.home.Matches.PostFeed.deletePostDialog.DeletePostDialog;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 *<h2>PostFeedFragUtil</h2>
 * <P>
 *     User available util class to provide
 *     required data to the util page.
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class PostFeedFragUtil
{
    static final String NEWS_FEED_LAYOUT_MANAGER = "news_feed_layout_manager";
    public static final String NEWS_FEED_LIST="news_feed_list";


    @Named(NEWS_FEED_LIST)
    @Provides
    @FragmentScoped
    ArrayList<PostListPojo> provideNewsFeedList()
    {
        return  new ArrayList<>();
    }

    @FragmentScoped
    @Provides
    RequestManager provideRequestManager(Activity activity){
        return Glide.with(activity).setDefaultRequestOptions(new RequestOptions());
    }

    @Provides
    @FragmentScoped
    PostListAdapter getListAdapter(RequestManager requestManager,PreferenceTaskDataSource dataSource,Utility utility, @Named(NEWS_FEED_LIST)ArrayList<PostListPojo> list_data, TypeFaceManager typeFaceManager)
    {
        return new PostListAdapter(requestManager,dataSource, utility, list_data, typeFaceManager);
    }

    @Named(NEWS_FEED_LAYOUT_MANAGER)
    @Provides
    @FragmentScoped
    LinearLayoutManager provideLinearLayoutManager(Activity activity)
    {
        return  new LinearLayoutManager(activity);
    }

    @Provides
    @FragmentScoped
    DeletePostDialog deletePostDialog(Activity activity,TypeFaceManager typeFaceManager){
        return new DeletePostDialog(activity,typeFaceManager);
    }

}
