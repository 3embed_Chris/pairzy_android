package com.pairzy.com.moments;


import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.pairzy.com.AppController;
import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.pairzy.com.moments.model.MomentPostClickCallBack;
import com.pairzy.com.moments.model.MomentsVerticalAdapter;
import com.pairzy.com.moments.model.MomentsModel;
import com.pairzy.com.MqttChat.Utilities.Utilities;
import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Matches.PostFeed.deletePostDialog.DeletePostDialog;
import com.pairzy.com.home.Matches.PostFeed.deletePostDialog.PostDeleteCallback;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.progressbar.LoadingProgress;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h2>MomentsPresenter</h2>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */
public class MomentsPresenter implements MomentsContract.Presenter, MomentPostClickCallBack, PostDeleteCallback {

    private static final String TAG = MomentsPresenter.class.getSimpleName();

    @Inject
    MomentsContract.View view;
    @Inject
    MomentsModel model;
    @Inject
    MomentsVerticalAdapter momentsAdapter;
    @Inject
    NetworkService service;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Activity activity;
    @Inject
    LoadingProgress loadingProgress;
    @Inject
    DeletePostDialog deletePostDialog;
    private int position;
    private CompositeDisposable compositeDisposable;

    @Inject
    public MomentsPresenter() {
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void takeView(MomentsContract.View view) {
        //already injected
    }

    @Override
    public void dropView() {
        this.view = null;
        compositeDisposable.clear();
    }

    @Override
    public void setAdapterCallback() {
        momentsAdapter.setAdapterCallback(this);
    }

    @Override
    public void likeDislikeUserPost(double type, String postId)
    {
        service.postUserPostLike(dataSource.getToken(),model.getLanguage(),model.getLikeUnlikeData(type,postId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> value)
                    {
                        try {
                            if(value.code() == 200) {
                                if(type==2)
                                    Log.e(TAG, "onNext: successfully unliked" );
                                else
                                    Log.e(TAG, "onNext:successfully liked" );
                                //loadingProgress.cancel();
                            }

                        } catch (Exception e) {
                            //loadingProgress.cancel();
                            /*if(view!=null)
                                view.showError(activity.getString(R.string.failed_to_get_pref_error));*/
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {}
                });
    }

    @Override
    public void launchChat(MomentsData momentsData){
        Intent intent = new Intent(activity, ChatMessageActivity.class);
        intent.putExtra("chatId","");
        intent.putExtra("receiverUid",momentsData.getUserId());
        intent.putExtra("receiverName",     momentsData.getUserName());
        String docId = AppController.getInstance().findDocumentIdOfReceiver(momentsData.getUserId(), "");
        boolean initiated = false;
        boolean hasDefaultMessage = true;
        boolean isMatched = false;
        if (docId.isEmpty()) {
            docId = (String) AppController.findDocumentIdOfReceiver(momentsData.getUserId(), Utilities.tsInGmt(),momentsData.getUserName(),
                    momentsData.getProfilePic(), "", false,momentsData.getUserId(), String.valueOf(Utilities.getGmtEpoch()) , false);
            initiated = true;
        }
        else{
            intent.putExtra("chatId",AppController.getInstance().getDbController().getChatId(docId));
            Map<String, Object> chatInfo = AppController.getInstance().getDbController().getChatInfo(docId);
            if(chatInfo.containsKey("initiated"))
                initiated = (boolean) chatInfo.get("initiated");
            hasDefaultMessage = (boolean)chatInfo.get("hasDefaultMessage");
            isMatched = (boolean) chatInfo.get("isMatched");
        }
        intent.putExtra("documentId", docId);
        intent.putExtra("receiverIdentifier",momentsData.getUserId());
        intent.putExtra("receiverImage",  momentsData.getProfilePic());
        intent.putExtra("colorCode", AppController.getInstance().getColorCode(0 % 19));
        if (view != null) {
            AppController.getInstance().getDbController().updateChatUserData(
                    docId,
                    momentsData.getUserName(),
                    momentsData.getProfilePic(),
                    isMatched,
                    false,
                    initiated,
                    hasDefaultMessage
            );
            //AppController.getInstance().getDbController().updateChatUserOnline(docId, true);
            view.launchChatScreen(intent);
        }
    }
    @Override
    public void onLikePost(int position) {
        MomentsData momentsData = model.getMomentPostItem(position);
        if(momentsData != null){
            momentsData.setLiked(true);
            int count=momentsData.getLikeCount()+1;
            momentsData.setLikeCount(count);
            if(view != null)
                view.notifyDataAdapter(position);
            likeDislikeUserPost( 1,momentsData.getPostId());
        }
    }

    @Override
    public void onUnlikePost(int position) {
        MomentsData momentsData = model.getMomentPostItem(position);
        if(momentsData != null){
            momentsData.setLiked(false);
            int count=momentsData.getLikeCount();

            if(count > 0){
                count -= 1;
            }

            momentsData.setLikeCount(count);

            if(view != null)
                view.notifyDataAdapter(position);
            likeDislikeUserPost( 2,momentsData.getPostId());
        }
    }

    @Override
    public void onComment(int position) {
        MomentsData momentsData = model.getMomentPostItem(position);
        if(momentsData != null){
            if (view != null)
                view.launchWriteCommit(momentsData.getPostId());
        }
    }

    @Override
    public void onViewAllComment(int position) {
        MomentsData momentsData = model.getMomentPostItem(position);
        if(momentsData != null){
            if(view != null)
                view.launchCommentView(momentsData.getPostId());
        }
    }

    @Override
    public void onViewAllLikes(int position) {
        MomentsData momentsData = model.getMomentPostItem(position);
        if(momentsData != null){
            if(view != null)
                view.launchLikeView(momentsData.getPostId());
        }
    }

    @Override
    public void onChatMessage(int position) {
        MomentsData momentsData = model.getMomentPostItem(position);
        if(momentsData != null){
            launchChat(momentsData);
        }
    }

    @Override
    public void onDeletePost(int position) {
        MomentsData momentsData = model.getMomentPostItem(position);
        if(momentsData != null){
            deletePostDialog.showAlert(momentsData.getPostId(),position,this);
        }
    }

    private void deletePost(String postId) {
        loadingProgress.show();
        service.deletePost(dataSource.getToken(),model.getLanguage(),model.getDeletePostBody(postId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        loadingProgress.cancel();
                        if(responseBodyResponse.code() == 200){

                        }else if(responseBodyResponse.code() == 401){
                            AppController.getInstance().appLogout();
                        }else{
                            if(view != null)
                                view.showError(model.getError(responseBodyResponse));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        loadingProgress.cancel();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void onPostDelete(String postId, int position) {
        if(networkStateHolder.isConnected()) {
            if (model.deleteMomentPost(postId,position)) {
                if (view != null)
                    view.notifyDataAdapter();

                deletePost(postId);
            }
        }else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void parseDateData(int requestCode, int resultCode, Intent data) {
//        if(requestCode == ChatFrag.RESCHEDULE_REQ_CODE){
//            if(resultCode == -1){
//                DateListPojo dateData = (DateListPojo) data.getSerializableExtra("date_data");
//                if(dateData != null)
//                    callDateResponseApi(dateData, DateResponseType.RESCHEDULE);
//            }
//        }

        if(requestCode == AppConfig.COMMENT_SCREEN_REQ_CODE){
            if (resultCode == Activity.RESULT_OK) {
                String commentCount = data.getStringExtra("commentData");
                Log.e(TAG, "parseDateData: "+commentCount );
                MomentsData momentsData = model.getMomentPostItem(position);
                if(momentsData != null) {
                    momentsData.setCommentCount(Integer.valueOf(commentCount));
                    if (view != null)
                        view.notifyDataAdapter(position);
                }
            }
        }
    }
}
