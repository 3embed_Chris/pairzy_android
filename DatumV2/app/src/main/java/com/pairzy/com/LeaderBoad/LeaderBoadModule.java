package com.pairzy.com.LeaderBoad;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class LeaderBoadModule {

    @Binds
    @ActivityScoped
    abstract LeaderBoadContract.Presenter providePresenter(LeaderBoadPresenter leaderBoadPresenter);

    @Binds
    @ActivityScoped
    abstract LeaderBoadContract.View provideView(LeaderBoadActivity leaderBoadActivity);

    @Binds
    @ActivityScoped
    abstract Activity provideActivity(LeaderBoadActivity leaderBoadActivity);
}
