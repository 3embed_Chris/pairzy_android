package com.pairzy.com.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>Generic</h2>
 *
 * @author 3Embed
 * @version 1.0
 * @since 1/19/2018.
 */

public class Generic implements Serializable
{
    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
