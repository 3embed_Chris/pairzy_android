package com.pairzy.com.util.notificationHelper;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.TaskStackBuilder;

import com.pairzy.com.R;
import com.pairzy.com.campaignScreen.CampaignActivity;
import com.pairzy.com.home.HomeActivity;
import com.pairzy.com.userProfile.UserProfilePage;
import com.pairzy.com.util.DatumActivateLifeListener;

public class NotificationHelperImpl implements NotificationHelper{

    private static final String CHANNEL_ID = "datum_notification_channel";
    private static final String CHANNEL_NAME = "datum_notification";
    private static final int NOTIFICATION_ID = 707;

    private Context context;
    private Notification notification;
    private NotificationCompat.Builder notificationBuilder;
    private NotificationManagerCompat notificationManagerCompat;
    private DatumActivateLifeListener datumActivateLifeListener;

    @Nullable
    private NotificationChannel notificationChannel;

    public NotificationHelperImpl(Context context,DatumActivateLifeListener datumActivateLifeListener) {
        this.context = context;
        this.datumActivateLifeListener = datumActivateLifeListener;
        notificationManagerCompat = NotificationManagerCompat.from(context);
        createNotificationChannel();
    }

    private void createNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ID,CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setShowBadge(true);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100});
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }


    @Override
    public void showLikeNotification(String title, String message) {
        Intent intent = new Intent(context, UserProfilePage.class);
        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        taskStackBuilder.addNextIntentWithParentStack(intent);
        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder = new NotificationCompat.Builder(context,CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent);
    }

    @Override
    public void showNotification(String title, String message, Bundle bundle) {

        Intent intent = null;
        //if(datumActivateLifeListener.isHomeActivityActive() != null) {
            intent = new Intent(context, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("data",bundle);
//        }
//        else {
//            intent = new Intent(context, SplashActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.putExtra("data",bundle);
//        }
        buildAndShowNotification(title,message,intent);
    }

    private void buildAndShowNotification(String title,String message,Intent intent){
        try {
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setStyle(new NotificationCompat.BigTextStyle())
                    .setAutoCancel(true)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

            notification = notificationBuilder.build();

            notificationManagerCompat.notify(NOTIFICATION_ID, notification);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void showCampaignNotification(String title, String message, Bundle bundle) {
        Intent intent = new Intent(context, CampaignActivity.class);
        intent.putExtra("data",bundle);

        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        taskStackBuilder.addNextIntentWithParentStack(intent);
        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder = new NotificationCompat.Builder(context,CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent);
        buildAndShowNotification(title,message, intent);
    }


    @Override
    public void showChatNotification(String title, String message) {

    }

    @Override
    public void showMatchNotification(String title, String message) {

    }
}
