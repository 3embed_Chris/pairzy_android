package com.pairzy.com.messageInfo;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

public interface MessageInfoContract {
    interface View extends BaseView {

        void applyFonts();
        void showMessage(String message);
    }
    interface Presenter extends BasePresenter<MessageInfoContract.View> {

    }
}
