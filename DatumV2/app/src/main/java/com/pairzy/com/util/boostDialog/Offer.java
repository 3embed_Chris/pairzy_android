package com.pairzy.com.util.boostDialog;

/**
 * <h>Offer data class</h>
 * @author 3Embed.
 * @since 1/5/18.
 */

public class Offer {

    private String headerTag;
    private int monthCount;
    private String priceString;
    private boolean isSelected;

    public Offer(String headerTag,int monthCount, String priceString, boolean isSelected) {
        this.headerTag = headerTag;
        this.monthCount = monthCount;
        this.priceString = priceString;
        this.isSelected = isSelected;

    }

    public void setHeaderTag(String headerTag) {
        this.headerTag = headerTag;
    }

    public void setMonthCount(int monthCount) {
        this.monthCount = monthCount;
    }

    public void setPriceString(String priceString) {
        this.priceString = priceString;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    public String getHeaderTag() {
        return headerTag;
    }
    public int getMonthCount() {
        return monthCount;
    }

    public String getPriceString() {
        return priceString;
    }

    public boolean isSelected() {
        return isSelected;
    }
}
