package com.pairzy.com.MatchedView;

/**
 * @since  3/21/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface DateAlertCallback
{
    void onCallDate();
    void onPhysicalDate();
    void onVideoDate();
}
