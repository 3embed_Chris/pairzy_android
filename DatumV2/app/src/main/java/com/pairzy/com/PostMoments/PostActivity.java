package com.pairzy.com.PostMoments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.AppController;
import com.pairzy.com.Database.PostDb;
import com.pairzy.com.PostMoments.model.Post;
import com.pairzy.com.PostMoments.model.PostData;
import com.pairzy.com.R;
import com.pairzy.com.Utilities.Constants;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatActivity;


/**
 * <h2>PostActivity</h2>
 * <p>It uploads media to cloudinary server and creates a post</p>
 *
 * @author 3Embed
 * @since 2/26/2018
 */

public class PostActivity extends DaggerAppCompatActivity implements  PostContract.View {
    private static final String TAG = "PostActivity";


    @Inject
    PostPresenter presenter;


    @BindView(R.id.ivPreview)
    ImageView ivPreview;
    @BindView(R.id.video_icon)
    ImageView video_icon;
    @BindView(R.id.vidViewPreview)
    VideoView vidViewPreview;
    @BindView(R.id.sdPreview)
    SimpleDraweeView sdPreview;
    @BindView(R.id.etPostTitle)
    AutoCompleteTextView etPostTitle;
    @BindView(R.id.actionBarRl)
    androidx.appcompat.widget.Toolbar toolbar;
    @BindView(R.id.tvAddLocation)
    TextView tvAddLocation;
    @BindView(R.id.tvCategory)
    TextView tvCategory;
    @BindView(R.id.tvAddToMyChannel)
    TextView tvAddToMyChannel;
    @BindView(R.id.recyclerChannel)
    RecyclerView recyclerChannel;
    @BindView(R.id.tvShare)
    TextView tvShare;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvLocation)
    TextView tvLocation;
    @BindView(R.id.btnPublish)
    Button btnPublish;
    @BindView(R.id.switchAddToMyChannel)
    SwitchCompat switchAddToMyChannel;
    @BindView(R.id.rvRecentAddress)
    RecyclerView rvRecentAddress;
    @BindView(R.id.ibClose)
    ImageButton ibClose;
    @BindView(R.id.switchFacebook)
    SwitchCompat switchFacebook;
    @BindView(R.id.switchInsta)
    SwitchCompat switchInsta;
    @BindView(R.id.switchTwitter)
    SwitchCompat switchTwitter;
    @BindView(R.id.llChannel)
    RelativeLayout llChannel;

    @BindView(R.id.parentLayoutPost)
    RelativeLayout parent;


    @BindView(R.id.tvAddCategory)
    TextView tvAddCategory;
    @BindView(R.id.llShare)
    LinearLayout llShare;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ibBack)
    ImageButton ibBack;

    @BindView(R.id.PreviewProfilePic)
    SimpleDraweeView PreviewProfilePic;

    @Inject
    PreferenceTaskDataSource dataSource;

    private Unbinder unbinder;
    private String path;
    private String type;

    private boolean first;
    private String categoryId = "";
    private String channelId = "";
    private String musicId = "";



    ProgressDialog progressDialog;
    private boolean isGallery;
    LocationManager locationManager;
    private String city;
    private String place;
    private InputMethodManager imm;
    boolean isEdit = false;

    PostData postData = new PostData();
    Post post = new Post();
    PostDb db = new PostDb(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        unbinder = ButterKnife.bind(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        PreviewProfilePic.setImageURI(dataSource.getProfilePicture());
        isGallery = getIntent().getBooleanExtra("isGallery", false);
        //init the facebook callback manager

        progressDialog = new ProgressDialog(this);
        //Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this));
        imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        etPostTitle.requestFocus();
        etPostTitle.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                etPostTitle.setText(etPostTitle.getText().toString().replace("##", "#"));
                etPostTitle.setSelection(etPostTitle.getText().length());
            }
        });

        isEdit = getIntent().getStringExtra("call") != null;
        type = getIntent().getStringExtra(Constants.Post.TYPE);
        if (!isEdit) {
            path = getIntent().getStringExtra(Constants.Post.PATH).replace(Constants.Post.PATH_FILE, Constants.EMPTY);
            musicId = getIntent().getStringExtra("musicId");
            first = true;
            displayMedia();
            presenter.init(path, type);

        } else {
            ivPreview.setVisibility(View.GONE);
            vidViewPreview.setVisibility(View.GONE);
            sdPreview.setVisibility(View.VISIBLE);

            tvLocation.setVisibility(View.VISIBLE);
            tvAddress.setVisibility(View.VISIBLE);
            video_icon.setVisibility(type.equals(Constants.Post.IMAGE) ? View.GONE : View.VISIBLE);

            sdPreview.setImageURI(Uri.parse(getIntent().getStringExtra(Constants.Post.IMAGE)));

            etPostTitle.setSelection(etPostTitle.getText().length());
        }
        updateUi();
        showKeyboard();
    }

    @OnClick(R.id.ibBack)
    public void back() {
        hideKeyBoard();
        super.onBackPressed();
    }







    private void showKeyboard() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                etPostTitle.requestFocus();
                if (imm != null) {
                    imm.showSoftInput(etPostTitle, InputMethodManager.SHOW_FORCED);
                }
            }
        }, 200);
    }

    private void hideKeyBoard() {
        if (imm != null)
            imm.hideSoftInputFromWindow(etPostTitle.getWindowToken(), 0);
    }

    @OnClick(R.id.ibClose)
    public void close() {
        ibClose.setVisibility(View.GONE);
        tvLocation.setVisibility(View.GONE);
        tvAddress.setVisibility(View.GONE);
        rvRecentAddress.setVisibility(View.VISIBLE);
    }

    //@OnCheckedChanged(R.id.switchAddToMyChannel)
    public void clickSwitchAddToMyChannel(boolean isChecked) {
        if (switchAddToMyChannel.isChecked())
            recyclerChannel.setVisibility(View.VISIBLE);
        else
            recyclerChannel.setVisibility(View.GONE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }



    private void updateUi() {


    }




    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }











    @Override
    public void onBackPressed() {
        hideKeyBoard();
        super.onBackPressed();
    }



    @OnClick(R.id.btnPublish)
    void next()
    {
        hideKeyBoard();
        etPostTitle.getText().toString();
        postIt();
//        presenter.validate(etPostTitle);
    }




    public void displayMedia() {
        try {
            btnPublish.setEnabled(true);
            if (type.equals(Constants.Post.IMAGE)) {
                ivPreview.setVisibility(View.VISIBLE);
                vidViewPreview.setVisibility(View.GONE);
                video_icon.setVisibility(View.GONE);
                File imgFile = new File(path);
                Log.d(TAG, "displayMedia: " + path);
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                ivPreview.setImageBitmap(myBitmap);
                presenter.init(path, type);
            } else {
                presenter.init(path, type);
                ivPreview.setVisibility(View.GONE);
                vidViewPreview.setVisibility(View.VISIBLE);
                video_icon.setVisibility(View.VISIBLE);
                MediaController mediaController = new MediaController(PostActivity.this);
                mediaController.setVisibility(View.GONE);
                vidViewPreview.setMediaController(mediaController);
                Log.d(TAG, "displayMedia: " + path);
                vidViewPreview.setVideoPath(path);
                vidViewPreview.setOnPreparedListener(mp -> {
                    mp.setLooping(true);
                    mp.setVolume(0,0);
                    vidViewPreview.start();
                });
            }
        } catch (Exception ignored) {
        }
    }




    @Override
    protected void onResume() {
        super.onResume();
        super.onResumeFragments();
    }

    public static void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }

    @Override
    public void updatedSuccessfully() {
        setResult(Activity.RESULT_OK,getIntent());
        finish();
    }

    @Override
    public void showMessage(String upload_failed_from_cloudinary) {
        Snackbar snackbar = Snackbar.make(parent,upload_failed_from_cloudinary, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(this,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this,R.color.white));
        snackbar.show();
    }

    @Override
    public void showError(String error) {

    }
    private void postIt() {

        presenter.init(path, type);
        post.setPathForCloudinary(path);
        post.setTypeForCloudinary(type);
        post.setCaption(etPostTitle.getText().toString());

        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        post.setId(ts);
        post.setGallery(isGallery);
        postData.setId(ts);
        postData.setUserId(AppController.getInstance().getUserId());
        postData.setData(new Gson().toJson(post));
        postData.setStatus(0); //Notstarted

        db.addData(postData);

        try {
            AppController.getInstance().addNewPost(postData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        updatedSuccessfully();
    }
}
