package com.pairzy.com.home.Dates.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ankit on 14/5/18.
 */

public class Data {

    @SerializedName("pendingDates")
    @Expose
    private ArrayList<DateListPojo> pendingDates = null;
    @SerializedName("upcomingDates")
    @Expose
    private ArrayList<DateListPojo> upcomingDates = null;

    public ArrayList<DateListPojo> getPendingDates() {
        return pendingDates;
    }

    public void setPendingDates(ArrayList<DateListPojo> pendingDates) {
        this.pendingDates = pendingDates;
    }

    public ArrayList<DateListPojo> getUpcomingDates() {
        return upcomingDates;
    }

    public void setUpcomingDates(ArrayList<DateListPojo> upcomingDates) {
        this.upcomingDates = upcomingDates;
    }
}
