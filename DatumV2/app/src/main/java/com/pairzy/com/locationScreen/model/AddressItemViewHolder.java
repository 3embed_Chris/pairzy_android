package com.pairzy.com.locationScreen.model;

import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.util.TypeFaceManager;

/**
 * Created by ankit on 24/5/18.
 */

public class AddressItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView locationName;
    TextView addressTextview;
    RelativeLayout item;
    TextView tvRating;
    TextView tvDistance;
    AppCompatRatingBar ratingBar;
    private ItemActionCallBack callBack;
    private TypeFaceManager typeFaceManager;

    AddressItemViewHolder(View itemView,TypeFaceManager typeFaceManager, ItemActionCallBack callBack) {
        super(itemView);
        this.typeFaceManager = typeFaceManager;
        this.callBack = callBack;
        locationName = (TextView) itemView.findViewById(R.id.location_name);
        tvRating = (TextView) itemView.findViewById(R.id.rating_tv);
        tvDistance = (TextView) itemView.findViewById(R.id.distance_tv);
        ratingBar = (AppCompatRatingBar) itemView.findViewById(R.id.rating_bar);
        addressTextview = (TextView) itemView.findViewById(R.id.address_textview);
        item = (RelativeLayout) itemView.findViewById(R.id.item);
        locationName.setTypeface(typeFaceManager.getCircularAirBold());
        addressTextview.setTypeface(typeFaceManager.getCircularAirBook());
        tvRating.setTypeface(typeFaceManager.getCircularAirBook());
        tvDistance.setTypeface(typeFaceManager.getCircularAirBook());
        item.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(callBack != null)
            callBack.onClick(R.id.item,this.getAdapterPosition());
    }
}
