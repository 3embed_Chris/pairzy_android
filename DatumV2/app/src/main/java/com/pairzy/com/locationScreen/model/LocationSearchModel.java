package com.pairzy.com.locationScreen.model;

import com.pairzy.com.data.model.fourSq.FourResponse;
import com.pairzy.com.data.model.fourSq.Group;
import com.pairzy.com.data.model.fourSq.Item;
import com.pairzy.com.data.model.fourSq.Venue;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * <h>GoogleLocSearchModel class</h>
 * @author 3Embed.
 * @since 9/5/18.
 * @version 1.0.
 */

public class LocationSearchModel {

    @Named("ADDRESS_LIST")
    @Inject
    ArrayList<Venue> addressList;

    @Inject
    AddressAdapter addressAdapter;

    @Inject
    LocationSearchModel(){
    }

    public void filterResponse(FourResponse fourResponse) {
       ArrayList<Group> groupList = fourResponse.getResponse().getGroups();
       for(Group group: groupList){
           ArrayList<Item> itemList = group.getItems();
           for(Item item: itemList){
               Venue venue = item.getVenue();
               if(venue != null)
                   addressList.add(venue);
           }
       }

       addressAdapter.notifyDataSetChanged();
    }

    public int getResonseSize(FourResponse fourResponse) {
        int total = 0;
        ArrayList<Group> groupList = fourResponse.getResponse().getGroups();
        for(Group group: groupList){
            ArrayList<Item> itemList = group.getItems();
            total += itemList.size();
        }
        return total;
    }

    public void clearAddressList() {
        addressList.clear();
        addressAdapter.notifyDataSetChanged();
    }

    public int getListSize() {
        return addressList.size();
    }

    public void showLoadingItem() {
        try {
            if(getListSize() > 0){
                if(!addressList.get(getListSize()-1).isLoading()){
                    Venue loadingVenue = new Venue();
                    loadingVenue.setLoading(true);
                    addressList.add(loadingVenue);
                }
            }
        }catch (Exception e){
        }

    }
    public void notifyAdapter(){
        addressAdapter.notifyDataSetChanged();
    }

    public void removeLoadingItem() {
        try {
            if(getListSize() > 0){
                if(addressList.get(getListSize()-1).isLoading()){
                    addressList.remove(addressList.get(getListSize()-1));
                }
            }
        }catch (Exception e){
        }
    }

    public void showLoadingError() {
        try {
            if(getListSize() > 0){
                Venue loadingVenue = addressList.get(getListSize()-1);
                if(loadingVenue.isLoading()){
                    loadingVenue.setLoadingError(true);
                }
            }
        }catch (Exception e){
        }
    }

    public Venue getItem(int position) {
        if(position < getListSize())
            return addressList.get(position);
        return null;
    }
}
