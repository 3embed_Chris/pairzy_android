package com.pairzy.com.fbRegister;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;

import com.pairzy.com.MqttChat.Database.CouchDbController;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.networking.RxNetworkObserver;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.LocationProvider.LocationApiCallback;
import com.pairzy.com.util.LocationProvider.LocationApiManager;
import com.pairzy.com.util.LocationProvider.Location_service;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.Utility;
import com.facebookmanager.com.FacebookUserDetails;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h2>FbRegisterPagePresenter</h2>
 * <P>
 *
 * </P>
 * @since  2/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class FbRegisterPagePresenter implements FbRegisterContact.Presenter,App_permission.Permission_Callback,Location_service.GetLocationListener
{
    @Inject
    NetworkStateHolder holder;
    @Inject
    RxNetworkObserver rxNetworkObserver;
    @Inject
    Utility utility;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    DatumProgressDialog progressDialog;
    @Inject
    FbRegisterModel registerModel;
    @Inject
    NetworkService service;
    @Inject
    FbRegisterContact.View view;
    @Inject
    LocationApiManager locationApiManager;
    @Inject
    Location_service location_service;
    @Inject
    App_permission app_permission;
    @Inject
    Activity activity;
    @Inject
    CouchDbController couchDbController;
    private Bundle bundle;
    private CompositeDisposable compositeDisposable;

    @Inject
    FbRegisterPagePresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(Object view)
    {}

    @Override
    public void dropView()
    {
        location_service.stop_Location_Update();
        compositeDisposable.clear();
    }

    @Override
    public void updateProgress(int progress) {
        view.updateProgress(progress);
    }

    @Override
    public void showError(String message)
    {
        if(view==null)
            return;
        view.showError(message);
    }

    @Override
    public void showMessage(String message)
    {
        if(view==null)
            return;
        view.showMessage(message);
    }

    @Override
    public void launchFragment(DaggerFragment fragment, boolean keepBack)
    {
        if(view==null)
            return;
        view.openFragment(fragment,keepBack);
    }

    @Override
    public void updateProfile(Bundle data)
    {
        bundle=data;
        askForLocationPermission();
    }

    @Override
    public void initNetworkObserver()
    {
        view.updateInterNetStatus(holder.isConnected());
        Observer<NetworkStateHolder> observer = new Observer<NetworkStateHolder>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {
                compositeDisposable.add(d);
            }
            @Override
            public void onNext(NetworkStateHolder value)
            {
                view.updateInterNetStatus(value.isConnected());
            }
            @Override
            public void onError(Throwable e)
            {
                e.printStackTrace();
            }
            @Override
            public void onComplete()
            {}
        };
        rxNetworkObserver.getObservable().subscribeOn(Schedulers.newThread());
        rxNetworkObserver.getObservable().observeOn(AndroidSchedulers.mainThread());
        rxNetworkObserver.getObservable().subscribe(observer);
    }

    @Override
    public void launchNextValidFrag(int currentPos, boolean backtrack) {
        if(view != null)
            view.launchAppropriateFrag(currentPos, backtrack);
    }


    @Override
    public void facebookApiCall(FacebookUserDetails userDetails)
    {
        if(!holder.isConnected())
        {

        }else
        { }
        Observable<Response<ResponseBody>> body = service.facebookLogin(registerModel.getAuthorization(), registerModel.getLanguage(),
                registerModel.prepareFacebookRequest(userDetails));
        body.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> data)
                    {
                        try
                        {
                            if(data.code()==200) //this case not required here.
                            {
//                                String response_data = data.body().string();
//                                registerModel.parseLoginResponse(response_data);
//                                if(view!=null)
//                                    view.facebookLoginSuccess();
                            }else if(data.code()==201)
                            {
                                String response_data = data.body().string();
                                registerModel.parseSignUpResponse(response_data);
                                if (view != null)
                                    view.facebookSignUpSuccess();
                            }else
                            {
                                String error = registerModel.getError(data);
                                if(TextUtils.isEmpty(error)) {
                                    if (view != null)
                                        view.showMessage(registerModel.getError(data));
                                }
                                else{
                                    if (view != null)
                                        view.showMessage(activity.getString(R.string.api_server_error));
                                }
                            }

                        }catch (IOException e)
                        {
                            e.printStackTrace();
                            if(view!=null)
                                view.showMessage(e.getMessage());
                        } catch (DataParsingException e)
                        {
                            if(view!=null)
                                view.showMessage(e.getMessage());

                        } catch (EmptyData e)
                        {
                            if(view!=null)
                                view.showMessage(e.getMessage());
                        }
                        progressDialog.cancel();
                    }

                    @Override
                    public void onError(Throwable e)
                    {
                        progressDialog.cancel();
                        if(view!=null)
                            view.showMessage(e.getMessage());
                    }
                    @Override
                    public void onComplete() {}
                });

    }

    @Override
    public boolean onHandelActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode==Location_service.REQUEST_CHECK_SETTINGS)
        {
            if(resultCode== Activity.RESULT_OK)
            {
                location_service.checkLocationSettings();
            }else
            {
                getUserLocation();
            }
            return true;
        }else
        {
            return false;
        }
    }

    @Override
    public void askForLocationPermission()
    {
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.LOCATION);
        app_permission.getPermission("location",permissions,this);
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag)
    {
        getUserLocationApi();
    }


    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag)
    {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        app_permission.show_Alert_Permission(activity.getString(R.string.location_access_text),activity.getString(R.string.location_acess_subtitle),
                activity.getString(R.string.location_acess_message),stringArray);
    }


    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag)
    {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean parmanent)
    {
        getUserLocation();
    }

    @Override
    public void getUserLocation()
    {
        progressDialog.show();
        locationApiManager.getLocation(new LocationApiCallback() {
            @Override
            public void onSuccess(Double lat, Double lng, double accuracy)
            {
                bundle.putDouble(LAT,lat);
                bundle.putDouble(LNG,lng);
                if(view != null) {
                    FacebookUserDetails details = view.getFbDetails();
                    details.setLat(lat);
                    details.setLon(lng);
                    if(bundle.containsKey(FbRegisterContact.Presenter.VIDEO_THUMB_DATA)){
                        details.setProfileVideo(bundle.getString(FbRegisterContact.Presenter.VIDEO_DATA, ""));
                        details.setProfileVideoThumb(bundle.getString(FbRegisterContact.Presenter.VIDEO_THUMB_DATA, ""));
                    }
                    facebookApiCall(details);
                    //updateUserDetails(details);
                }
            }

            @Override
            public void onError(String error)
            {
                if(view!=null)
                    view.showError(error);
            }
        });
    }

    @Override
    public void getUserLocationApi()
    {
        progressDialog.show();
        location_service.setListener(this);
        location_service.checkLocationSettings();
    }

    @Override
    public void updateLocation(Location location)
    {
        if(location!=null)
        {
            location_service.stop_Location_Update();
            bundle.putDouble(LAT,location.getLatitude());
            bundle.putDouble(LNG,location.getLongitude());
            if(view != null) {
                FacebookUserDetails details = view.getFbDetails();
                details.setLat(location.getLatitude());
                details.setLon(location.getLatitude());

                if(bundle.containsKey(FbRegisterContact.Presenter.VIDEO_THUMB_DATA)){
                    details.setProfileVideo(bundle.getString(FbRegisterContact.Presenter.VIDEO_DATA, ""));
                    details.setProfileVideoThumb(bundle.getString(FbRegisterContact.Presenter.VIDEO_THUMB_DATA, ""));
                }
                facebookApiCall(details);
                //updateUserDetails(details);
            }
            //updateUserDetails(bundle);
        }
    }

    @Override
    public void location_Error(String error)
    {
        getUserLocation();
    }

}
