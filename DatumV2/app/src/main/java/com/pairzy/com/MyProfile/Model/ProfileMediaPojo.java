package com.pairzy.com.MyProfile.Model;

/**
 * <h2>ProfileMediaPojo</h2>
 * <P>
 *
 * </P>
 *@since  4/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ProfileMediaPojo
{
    private String image_url;
    private String video_url;
    private String video_thumbnail;
    private boolean isVideo=false;

    public boolean isVideo() {
        return isVideo;
    }

    public void setVideo(boolean video) {
        isVideo = video;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_thumbnail() {
        return video_thumbnail;
    }

    public void setVideo_thumbnail(String video_thumbnail) {
        this.video_thumbnail = video_thumbnail;
    }
}
