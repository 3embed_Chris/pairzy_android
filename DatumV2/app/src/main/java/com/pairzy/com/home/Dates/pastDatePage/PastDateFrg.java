package com.pairzy.com.home.Dates.pastDatePage;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.home.Dates.DatesFragContract;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;
import com.pairzy.com.home.Dates.pastDatePage.Model.PastDateAdapter;
import com.pairzy.com.util.TypeFaceManager;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * <h2>PastDateFrg</h2>
 * A simple {@link Fragment} subclass.
 * @author 3Embed.
 * @version 1.0.
 * @since 17-03-2017.
 */
@ActivityScoped
public class PastDateFrg extends DaggerFragment implements PastDateFrgContract.View ,SwipeRefreshLayout.OnRefreshListener
{
    @Inject
    PastDateFrgContract.Presenter presenter;
    @Named(PastDatePageUtil.PAST_DATE_LAYOUT_MANAGER)
    @Inject
    LinearLayoutManager linearLayoutManager;
    @Inject
    PastDateAdapter pastDateAdapter;
    @Inject
    DatesFragContract.Presenter datePresenter;
    @Inject
    Activity activity;
    @Inject
    TypeFaceManager typeFaceManager;

    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.past_date_list)
    RecyclerView pastDateRecycler;
    @BindView(R.id.loading_progress)
    ProgressBar pbLoading;
    @BindView(R.id.loading_text)
    TextView tvLoading;
    @BindView(R.id.error_icon)
    ImageView ivErrorIcon;
    @BindView(R.id.error_msg)
    TextView tvErrorMsg;
    @BindView(R.id.empty_data)
    RelativeLayout rlEmptyData;
    @BindView(R.id.loading_view)
    RelativeLayout rlLoadingView;
    @BindView(R.id.no_more_data)
    TextView tvNoMoreDataTitle;
    @BindView(R.id.empty_details)
    TextView tvEmptyDetail;
    @BindView(R.id.btn_retry)
    Button btnRetry;

    private Unbinder unbinder;
    public PastDateFrg() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_past_date, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder=ButterKnife.bind(this,view);
        presenter.takeView(this);
        initUi();
        applyFont();
        presenter.notifyPastdateAdapter();
    }

    private void applyFont() {
        tvNoMoreDataTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvEmptyDetail.setTypeface(typeFaceManager.getCircularAirBook());
        btnRetry.setTypeface(typeFaceManager.getCircularAirBold());
    }

    /*
    * initialization of the required data.*/
    private void initUi()
    {
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh2, R.color.refresh1,R.color.refresh);
        pastDateRecycler.setHasFixedSize(true);
        //pastDateRecycler.setNestedScrollingEnabled(false);
        pastDateRecycler.setLayoutManager(linearLayoutManager);
        pastDateRecycler.setAdapter(pastDateAdapter);
        presenter.setAdapterCallBack(pastDateAdapter);
        pastDateRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int positionView = linearLayoutManager.findLastVisibleItemPosition();
                presenter.doLoadMore(positionView);
                datePresenter.preFetchPastDateListImage(positionView);
            }
        });
        swipeRefreshLayout.setOnRefreshListener(this);
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
    }

    @Override
    public void notifyAdapter() {
        if(rlEmptyData != null) {
            if (presenter != null && !presenter.checkDateExist()) {
                emptyData();
            } else {
                pastDateAdapter.notifyDataSetChanged();
                rlLoadingView.setVisibility(View.GONE);
                rlEmptyData.setVisibility(View.GONE);
                pastDateRecycler.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void doLoadMore() {
        try {
            pastDateRecycler.post(() -> pastDateAdapter.notifyDataSetChanged());
        }catch (Exception e){
        }
        datePresenter.fetchPastDateList(false);
    }

    @Override
    public void refreshList() {
        datePresenter.fetchPastDateList(true);
    }

    @Override
    public void showMessage(String message) {
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    public void showError(int id)
    {
        String message=getString(id);
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showNetworkError(String error) {
        if(tvErrorMsg != null) {
            btnRetry.setVisibility(View.VISIBLE);
            tvErrorMsg.setVisibility(View.VISIBLE);
            tvErrorMsg.setText(error);
            ivErrorIcon.setVisibility(View.VISIBLE);
            tvLoading.setVisibility(View.GONE);
            pbLoading.setVisibility(View.GONE);
            pastDateRecycler.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void emptyData() {
        if(pastDateRecycler != null) {
            //pastDateRecycler.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showLoading() {
        if(btnRetry != null) {
            btnRetry.setVisibility(View.GONE);
            tvErrorMsg.setVisibility(View.GONE);
            ivErrorIcon.setVisibility(View.GONE);
            tvLoading.setVisibility(View.VISIBLE);
            pbLoading.setVisibility(View.VISIBLE);
            rlLoadingView.setVisibility(View.VISIBLE);
            //pastDateRecycler.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
        }
    }


    @Override
    public void launchUserProfile(PastDateListPojo pastDateListPojo) {
        datePresenter.launchUserProfile(pastDateListPojo);
    }


    @Override
    public void showError(String error)
    {
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @OnClick(R.id.btn_retry)
    public void onRetry(){
        onRefresh();
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        refreshList();
    }
}
