package com.pairzy.com.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 7/9/18.
 */

public class BoostCountData {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("start")
    @Expose
    private long start;
    @SerializedName("expire")
    @Expose
    private long expire;
    @SerializedName("likes")
    @Expose
    private Integer likes;
    @SerializedName("disLikes")
    @Expose
    private Integer disLikes;
    @SerializedName("supperLikes")
    @Expose
    private Integer supperLikes;
    @SerializedName("match")
    @Expose
    private Integer match;
    @SerializedName("unmatch")
    @Expose
    private Integer unmatch;
    @SerializedName("views")
    @Expose
    private Integer views;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getExpire() {
        return expire;
    }

    public void setExpire(long expire) {
        this.expire = expire;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getDisLikes() {
        return disLikes;
    }

    public void setDisLikes(Integer disLikes) {
        this.disLikes = disLikes;
    }

    public Integer getSupperLikes() {
        return supperLikes;
    }

    public void setSupperLikes(Integer supperLikes) {
        this.supperLikes = supperLikes;
    }

    public Integer getMatch() {
        return match;
    }

    public void setMatch(Integer match) {
        this.match = match;
    }

    public Integer getUnmatch() {
        return unmatch;
    }

    public void setUnmatch(Integer unmatch) {
        this.unmatch = unmatch;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

}
