package com.pairzy.com.home.Discover.GridFrg;

import android.content.Intent;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.home.Discover.GridFrg.Model.DeckCardItemClicked;
import com.pairzy.com.home.Discover.Model.UserItemPojo;

/**
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface GridDataViewContract
{
    interface View extends BaseView
    {
        void showError(String error);
        void showMessage(String message);
        void notifyDataChanged();
        void onLikeEventError(UserItemPojo temp);
        void showLoadingView();
        void openUerProfile(String data, android.view.View view);
        void adapterClickListener(DeckCardItemClicked itemClicked);
        void doRewind();
        void doLike();
        void doDislike();
        void doSuperLike();
        void doLoadMore();
        void openBoostDialog();
        void superLikeByButton();
        void superLikeBySwipe(UserItemPojo userItemPojo);
        void openChat(UserItemPojo userItemPojo);
        void setChatListNeedToUpdate(boolean yes);
        void showBoostViewCounter(boolean show, String viewsText);
        void startCoinAnimation();
        void launchCoinWallet();
        void callSuperLikeApi(UserItemPojo userItemPojo);
        void notifyFirstItem();

    }


    interface Presenter extends BasePresenter<View>
    {
        boolean onHandelActivityResult(int requestCode, int resultCode, Intent data);
        void initListener();
        void loadMore();
        void noData();
        void updateDataChanged();
        void showBoostDialog();
        boolean isNoMoreData();
        void revertAction(UserItemPojo item);
        void initiateSuperlike();
        void showBoostViewCounter(boolean show, int viewCount);
        void startCoinAnimation();
        void checkWalletForSuperlike(UserItemPojo userItemPojo);
        void checkWalletForSuperlikeBySwipe(UserItemPojo userItemPojo);
        void startVideoPlayFirstItem();
    }
}
