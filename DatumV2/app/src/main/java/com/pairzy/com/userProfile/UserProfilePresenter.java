package com.pairzy.com.userProfile;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.viewpager.widget.ViewPager;

import com.androidinsta.com.ImageData;
import com.androidinsta.com.InstaLoginHolder;
import com.androidinsta.com.InstaReponseHolder;
import com.androidinsta.com.InstagramManger;
import com.pairzy.com.AppController;

import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.pairzy.com.MqttChat.Utilities.Utilities;
import com.pairzy.com.R;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.userProfile.AleretBox.AlertBoxCallBack;
import com.pairzy.com.userProfile.AleretBox.BottomDialog;
import com.pairzy.com.userProfile.Model.DataModel;
import com.pairzy.com.userProfile.Model.MomentClickCallback;
import com.pairzy.com.userProfile.Model.MomentsGridAdapter;
import com.pairzy.com.userProfile.Model.UserDataDetails;
import com.pairzy.com.userProfile.Model.UserProfileData;
import com.pairzy.com.userProfile.Model.ViewModel;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.CustomObserver.CoinBalanceObserver;
import com.pairzy.com.util.CustomObserver.dataChangeObserver.DataChangeType;
import com.pairzy.com.util.CustomObserver.dataChangeObserver.DatumDataChangeObserver;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.ReportUser.ReportUserCallBack;
import com.pairzy.com.util.ReportUser.ReportUserDialog;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.SpendCoinDialog.CoinSpendDialogCallback;
import com.pairzy.com.util.SpendCoinDialog.WalletEmptyDialogCallback;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.boostDialog.BoostAlertCallback;
import com.pairzy.com.util.boostDialog.BoostDialog;
import com.pairzy.com.util.boostDialog.Slide;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.pairzy.com.util.timerDialog.TimerDialog;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.suresh.innapp_purches.InAppCallBack;
import com.suresh.innapp_purches.InnAppSdk;
import com.suresh.innapp_purches.SkuDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 *<h2>UserProfilePresenter</h2>
 * <P>
 *     Presenter class for the user to
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 12-03-2018.
 * */
public class UserProfilePresenter implements UserProfileContract.Presenter,ViewPager.OnPageChangeListener,AlertBoxCallBack,ReportUserCallBack
        ,BoostAlertCallback,ViewModel.OnRetryCallback,CoinSpendDialogCallback,WalletEmptyDialogCallback, MomentClickCallback
{
    private static final String TAG = UserProfilePresenter.class.getSimpleName();

    @Inject
    Activity activity;
    @Inject
    DataModel dataModel;
    @Inject
    UserProfileContract.View view;
    @Inject
    NetworkService service;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Utility utility;
    @Inject
    BottomDialog bottomDialog;
    @Inject
    ReportUserDialog reportUserDialog;
    @Inject
    LoadingProgress loadingProgress;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    BoostDialog boostDialog;
    @Inject
    ArrayList<Slide> slideArrayList;
    @Inject
    InstagramManger instagramManger;
    @Inject
    DatumProgressDialog datumProgressDialog;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    CoinDialog spendCoinDialog;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    CoinBalanceObserver coinBalanceObserver;
    @Inject
    TimerDialog timerDialog;
    @Inject
    DatumDataChangeObserver dataChangeObserver;


    private CompositeDisposable compositeDisposable;
    private boolean isForSuperLike = false;
    private boolean isMatchedUser = false;

    //private DecimalFormat decimalFormat;

    @Inject
    public UserProfilePresenter()
    {
        compositeDisposable=new CompositeDisposable();
        //decimalFormat = new DecimalFormat("0.0");
    }

    @Override
    public void initDataChangeObserver(){
        dataChangeObserver.getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<DataChangeType>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(DataChangeType dataChangeType) {
                        switch (dataChangeType){
                            case BLOCK_USER:
                            case UN_BLOCK_USER:
                                dataModel.refreshBlockedFromMqtt();
                                break;
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void checkIntent(Intent intent)
    {
        Bundle intent_data=intent.getExtras();
        if(intent_data!=null)
        {
            String data=intent_data.getString(UserProfilePage.USER_DATA);
            UserDataDetails details=dataModel.createMediaList(data);
            view.movePagerPosition(details.getCurrentImagePos());
            view.updateSegmentBar(dataModel.getMediaTotal(),details.getCurrentImagePos());
            String name_data;

            if(details.getAge()!=null && details.getAge().getIsHidden() == 0)
            {
                name_data=utility.formatString(details.getFirstName())+", "+details.getAge().getValue();

            }else if(details.getAge()==null&&details.getDateOfBirth()!=null)
            {
                name_data=utility.formatString(details.getFirstName())+", "+utility.getAgeFromDob(details.getDateOfBirth());
            }else
            {
                name_data=utility.formatString(details.getFirstName());
            }
            double dist;
            String away_text ="";
            if(details.getDistance()!=null)
            {
                dist= utility.round(details.getDistance().getValue(),2);

            }else
            {
                dist=0;
            }

            if(details.getDistance()!=null && details.getDistance().getIsHidden() ==0)
            {
                if(dataSource.getIsMile())
                {
                    dist = utility.round(utility.kmToMiles(dist),2);
                    if(dist < 1.0)
                        away_text=activity.getString(R.string.less_than_mile_away);
                    else
                        away_text=dist+" "+activity.getString(R.string.mile_text);
                }else
                {
                    if(dist < 1)
                        away_text=activity.getString(R.string.less_than_km_away);
                    else
                        away_text=dist+" "+activity.getString(R.string.km_text);
                }
            }else
            {
                away_text="";
            }

            String school_text = "";
            if(!TextUtils.isEmpty(details.getWork())){
                school_text = details.getWork();
                if(view != null)
                    view.showWorkIcon();
            }
            else{
                if(!TextUtils.isEmpty(details.getEducation())){
                    school_text = details.getEducation();
                }
            }
            if(view != null)
                view.updatePrimeryData (name_data,school_text,away_text,details.getOnlineStatus());
        }else
        {
            if(view!=null)
                view.closePage();
        }
    }
    @Override
    public void loadMomentScreen() {
        ArrayList<MomentsData> momentsDataList = dataModel.getMomentList();
        if(view != null)
            view.launchMomentScreen(momentsDataList,0 /*default post position*/);
    }

    @Override
    public void getUserDetails()
    {
        if(networkStateHolder.isConnected()) {
            view.addLoadingView(dataModel.getLoadingView());
            service.getUserProfile(dataModel.userProfileUrl(), dataSource.getToken(), dataModel.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            try {
                                if (value.code() == 200) {
                                    String data = value.body().string();
                                    dataModel.parseData(data);
                                    if (view != null) {
                                        if(!dataModel.isAboutTextEmpty())
                                            view.showAboutText(dataModel.getAboutText());
                                        view.setReportText(dataModel.getUserName());
                                        view.updateUserAge(dataModel.getNameAge());
                                        view.showOnlineStatus(dataModel.isUserOnline());
                                        view.addChildView(dataModel.getDetailsView());
                                        view.showButtonsState(dataModel.isUnliked(), dataModel.isSuperliked(), dataModel.isLiked(),dataModel.isMatched());
                                        if(view != null)
                                            view.showUserLocation(getUserLocationString(),dataModel.isLocationNeedToShow());
                                    }
                                    if(dataModel.getMomentList()!=null)
                                    {
                                        view.showMomentData(dataModel.getMomentList());
                                    }
                                    //added extra
                                    dataModel.createMediaList();
                                    if (dataModel.getMediaTotal() > 0)
                                    {
                                        view.movePagerPosition(0);
                                        view.updateSegmentBar(dataModel.getMediaTotal(), 0);
                                    }

                                    if (dataModel.isInstaDataExist()) {
                                        if (view != null)
                                            view.handleInstagramDetails();
                                    } else {
                                        if (view != null)
                                            view.hideInstadetails();
                                    }
                                } else if (value.code() == 401) {
                                    AppController.getInstance().appLogout();
                                } else {
                                    if (view != null)
                                        view.hideInstadetails();
                                    if (view != null)
                                        view.addLoadingView(dataModel.getErrorOnLoadingView());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                if (view != null)
                                    view.hideInstadetails();
                                if (view != null)
                                    view.addLoadingView(dataModel.getErrorOnLoadingView());
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null)
                                view.hideInstadetails();
                            if (view != null)
                                view.addLoadingView(dataModel.getErrorOnLoadingView());
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        } else{

            if(view != null)
                view.addLoadingView(dataModel.getInternetErrorView());
        }

    }

    private String getUserLocationString() {
        double distance = dataModel.getUserDistance();
        String distanceAwayText = "";
        if(dataSource.getIsMile())
        {
            distance = utility.round(utility.kmToMiles(distance),2);
            if(distance < 1.0)
                distanceAwayText=activity.getString(R.string.less_than_mile_away);
            else
                distanceAwayText=distance+" "+activity.getString(R.string.mile_text);
        }else
        {
            if(distance < 1)
                distanceAwayText=activity.getString(R.string.less_than_km_away);
            else
                distanceAwayText=distance+" "+activity.getString(R.string.km_text);
        }
        return distanceAwayText;
    }

    @Override
    public void getUserInstaPost()
    {
        instagramManger.getUserRecentPost()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<InstaReponseHolder>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(InstaReponseHolder value)
                    {
                        datumProgressDialog.cancel();
                        List<List<ImageData>> list_data=dataModel.prepareInstaResponse(value.getImageData());
                        if(view!=null)
                            view.showInsaPost(list_data);
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        datumProgressDialog.cancel();
                        if(view!=null)
                            view.showError(e.getMessage());
                    }
                    @Override
                    public void onComplete() {}
                });
    }


    @Override
    public void getUserDetails(String profileId) {
        dataModel.setUserId(profileId);
        getUserDetails();
    }


    @Override
    public void initListener()
    {
        view.intPageChangeListener(this);
    }

    @Override
    public void showOptionAlert()
    {
        String docId=  AppController.getInstance().findDocumentIdOfReceiver(dataModel.getUserId(),"");

        bottomDialog.showDialog(dataModel.getUserName(),
                dataModel.isUserBlockedByMe(),dataModel.isMatched(),!TextUtils.isEmpty(docId),this);
    }

    @Override
    public String collectUserId()
    {
        return dataModel.getUser_id();
    }


    @Override
    public void takeView(UserProfileContract.View view) {}
    @Override
    public void dropView() {
        this.view= null;
        compositeDisposable.clear();

    }
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
    {}

    @Override
    public void onPageSelected(int position)
    {
        view.updateSegmentBar(dataModel.getMediaTotal(),position);
    }

    @Override
    public void onPageScrollStateChanged(int state)
    {}

    @Override
    public void onRecommendFriend() {
        shareProfile();
    }

    @Override
    public void onReport() {
        reportProfile();
    }

    @Override
    public void reportProfile(){
        if(dataModel.isReportReasonsEmpty())
            getReportReasons();
        else{
            if (view != null)
                launchReportUserDialog(dataModel.getReasonsList());
        }
    }

    @Override
    public String getUserInstName() {
        return dataModel.getInstagramName();
    }

    @Override
    public String getUserInstaId()
    {
        return dataModel.getInstaGramProfileId();
    }

    @Override
    public String getUserInstaToken() {
        return dataModel.getOtherUserInstaToken();
    }

    @Override
    public void getOtherUserInstPost(String user_id,String userInstaToken)
    {
        instagramManger.getOtherPostDetails(user_id,userInstaToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<InstaReponseHolder>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(InstaReponseHolder value)
                    {
                        datumProgressDialog.cancel();
                        List<List<ImageData>> list_data=dataModel.prepareInstaResponse(value.getImageData());
                        if(view!=null)
                            view.showInsaPost(list_data);
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        datumProgressDialog.cancel();
                        if(view!=null)
                            view.showError(e.getMessage());
                    }
                    @Override
                    public void onComplete() {}
                });
    }

    @Override
    public void doInstagramLogin()
    {
        if(instagramManger.isUserLoggedIn())
        {
            try
            {
                InstaLoginHolder data=instagramManger.getUserDetails();
                updateInstagramDetails(data.getUser_id(),data.getUser_name(),data.getToken());
            } catch (Exception e)
            {
                doActualInstaLogin();
            }
        }else
        {
            doActualInstaLogin();
        }
    }

    @Override
    public String getUserName() {
        return dataModel.getUserName();
    }

    @Override
    public void setOnRetryCallback() {
        dataModel.setOnRetryCallback(this);
    }

    private void launchWalletEmptyDialogForSuperLike(){
        spendCoinDialog.showWalletEmptyDialog(activity.getString(R.string.wallet_empty_title),activity.getString(R.string.empty_wallet_superlike_msg),this);
    }

    private void launchWalletEmptyDialogForChat(){

        String msg = String.format(Locale.ENGLISH , "%s %s %s %s",activity.getString(R.string.you_need_atleast_text),
                dataModel.getCoinForUnmatchChat(),
                activity.getString(R.string.proactive_empty_wallet_sub_msg),
                utility.formatCoinBalance(dataModel.getCoinBalance())+".");
        spendCoinDialog.showWalletEmptyDialog(activity.getString(R.string.proactive_wallet_empty_title),msg,this);
    }

    @Override
    public void superLikeThisUser() {
        if(networkStateHolder.isConnected()) {
            if(dataModel.isEnoughWalletBalanceToSuperLike()){
                if (dataModel.isDialogDontNeedToShow()) {
                    if (view != null)
                        view.superLikeUser();
                }
                else {
                    launchSpendCoinDialogForSuperLike();
                }
            } else{
                //launch superLike empty dialog.
                launchWalletEmptyDialogForSuperLike();
            }
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    @Override
    public void checkForLikeAvail() {
        if(networkStateHolder.isConnected()) {
            if (dataModel.getRemainsLikeCount() > 0) {
                if (view != null)
                    view.initLikeUser();
            } else {
                launchLikesTimerDialog();
            }
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void checkForInternetThenDislike() {
        if(networkStateHolder.isConnected()){
            if(view != null)
                view.initDislikeUser();
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    private void launchLikesTimerDialog() {
        timerDialog.showDialog(false);
    }


    /*
     *Doing actual instagram login */
    private void doActualInstaLogin()
    {
        instagramManger.doLogin(activity,null)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<InstaLoginHolder>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(InstaLoginHolder value)
                    {
                        updateInstagramDetails(value.getUser_id(),value.getUser_name(),value.getToken());
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        if(view!=null)
                            view.showError(e.getMessage());
                    }
                    @Override
                    public void onComplete() {}
                });
    }

    @Override
    public void updateInstagramDetails(String userId,String name,String token)
    {
        datumProgressDialog.isCancelable(false);
        datumProgressDialog.show();
        service.updateInstagramId(dataSource.getToken(),dataModel.getLanguage(),dataModel.updateInstaId(userId,name,token))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> value)
                    {
                        if(value.code() == 200)
                        {
                            if(view!=null)
                                view.handleInstagramDetails();
                        }else
                        {
                            datumProgressDialog.cancel();
                            if(view!=null)
                                view.showError("Failed to update!");
                        }
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        datumProgressDialog.cancel();
                        if(view!=null)
                            view.showError(e.getMessage());
                    }
                    @Override
                    public void onComplete() {}
                });
    }


    @Override
    public void onBlock()
    {
        callUserBlockApi();
    }

    @Override
    public void onUnblock()
    {
        callUserUnBlockApi();
    }

    private void callUserBlockApi() {
        if(networkStateHolder.isConnected()){
            if(view != null)
                loadingProgress.show();
            Map<String,Object> body = new HashMap<>();
            body.put(ApiConfig.BlockOrUnblockUser.TARGET_USER_ID,dataModel.getUser_id());
            service.postBlockUser(dataSource.getToken(),dataModel.getLanguage(),body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if(value.code() == 200){
                                if(view != null)
                                    view.showMessage(String.format(Locale.getDefault(),"%s %s",dataModel.getUserName(),activity.getString(R.string.user_blocked_successful_msg)));
                                dataModel.setUserBlockedByMe(true);
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception ignored){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    //TODO: unblock user
    private void callUserUnBlockApi() {
        if(networkStateHolder.isConnected()){
            if(view != null)
                loadingProgress.show();
            Map<String,Object> body = new HashMap<>();
            body.put(ApiConfig.BlockOrUnblockUser.TARGET_USER_ID,dataModel.getUser_id());
            service.postUnBlockUser(dataSource.getToken(),dataModel.getLanguage(),body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if(value.code() == 200){
                                if(view != null)
                                    view.showMessage(String.format(Locale.getDefault(),"%s %s",dataModel.getUserName(),activity.getString(R.string.user_unblocked_successful_msg)));
                                dataModel.setUserBlockedByMe(false);
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception ignored){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void shareProfile() {
        String profileId = dataModel.getUser_id();
        if(!TextUtils.isEmpty(profileId)) {

            String link = "http://34.221.2.149:1008/documentation#!/searchResult/profileId/" + profileId;
            FirebaseDynamicLinks.getInstance().createDynamicLink()
                    .setLink(Uri.parse(link))
                    .setDomainUriPrefix("https://pairzy.page.link/")
                    .setAndroidParameters(
                            new DynamicLink.AndroidParameters.Builder(activity.getPackageName())
                                    .build())
                    .setIosParameters(
                            new DynamicLink.IosParameters.Builder("com.example.ios")
                                    .setAppStoreId("123456789")
                                    .setMinimumVersion("1.0.1")
                                    .build())
                    .buildShortDynamicLink()
                    .addOnSuccessListener(new OnSuccessListener<ShortDynamicLink>() {
                        @Override
                        public void onSuccess(ShortDynamicLink shortDynamicLink) {
                            Uri mInvitationUrl = shortDynamicLink.getShortLink();
                            Log.d(TAG, "onSuccess: shortUrl: "+mInvitationUrl);
                            if (view != null)
                                view.shareDeepLink(mInvitationUrl.toString());
                        }
                    });
        }
        else{
            if (view != null)
                view.showError("ProfileId can not be null!!");
        }
    }


    @Override
    public void onReportReasonSelect(String reason) {
        callReportApi(reason);
    }

    private void callReportApi(String reason) {
        if(networkStateHolder.isConnected()){
            if(view != null)
                loadingProgress.show();
            Map<String,Object> body = new HashMap<>();
            body.put(ApiConfig.ReportUser.TARGET_USER_ID,dataModel.getUser_id());
            body.put(ApiConfig.ReportUser.REPORT_REASON,reason);
            body.put(ApiConfig.ReportUser.REPORT_MESSAGE,"reporting user");
            service.postReportUser(dataSource.getToken(),dataModel.getLanguage(),body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if(value.code() == 200){
                                if(view != null)
                                    view.showMessage(dataModel.getUserName() + " has been reported successfully!!");
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception e){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if (view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    private void launchReportUserDialog(ArrayList<String> reasonsList) {
        reportUserDialog.showDialog(reasonsList,this);
    }

    public void getReportReasons() {
        if(networkStateHolder.isConnected()) {
            if(view != null)
                loadingProgress.show();
            service.getReportReasons(dataSource.getToken(), dataModel.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if (value.code() == 200) {
                                try {
                                    try {
                                        dataModel.parseReportReasonData(value.body().string());
                                    } catch (Exception ignored) {
                                    }
                                    if (dataModel.isReportReasonsEmpty()) {
                                        if (view != null)
                                            view.showError("report reason list can not be empty!!");
                                    } else {
                                        if (view != null)
                                            launchReportUserDialog(dataModel.getReasonsList());
                                    }
                                } catch (Exception e) {
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else {
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                } catch (Exception ignored) {
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if (view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    @Override
    public void launchBoostDialog() {
        getBoostDialog();
    }

    private void getBoostDialog()
    {
        if(!dataModel.isSubsPlanEmpty())
        {
            dataModel.selectMiddleItem();
            boostDialog.showAlert(this, dataModel.getSubsPlanList(), slideArrayList);
        }
        else {
            getSubsPlan();
        }
    }

    private void getSubsPlan()
    {
        if(networkStateHolder.isConnected()) {
            loadingProgress.show();
            service.getSubsPlans(dataSource.getToken(), dataModel.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onSuccess(Response<ResponseBody> value)
                        {
                            loadingProgress.cancel();
                            if(value.code() == 200)
                            {
                                try
                                {
                                    dataModel.parseSubsPlanList(value.body().string());
                                    if (dataModel.isSubsPlanEmpty())
                                    {
                                        loadingProgress.cancel();
                                        if(view != null)
                                            view.showError(activity.getString(R.string.subs_list_empty));
                                    }else
                                    {
                                        dataModel.selectMiddleItem();
                                        List<String> actual_ids=dataModel.collectsIds();
                                        InnAppSdk.getVendor().getProductDetails(activity,actual_ids, InnAppSdk.Type.SUB, new InAppCallBack.DetailsCallback()
                                        {
                                            @Override
                                            public void onSuccess(List<SkuDetails> list, List<String> errorList)
                                            {
                                                loadingProgress.cancel();
                                                if(list.size()>0)
                                                {
                                                    dataModel.updateDetailsData(list);
                                                    boostDialog.showAlert(UserProfilePresenter.this, dataModel.getSubsPlanList(), slideArrayList);
                                                }else
                                                {
                                                    dataModel.clearProductList();
                                                    if(view != null)
                                                        view.showError(activity.getString(R.string.subs_list_empty));
                                                }
                                            }
                                        });
                                    }
                                }catch (Exception e)
                                {
                                    loadingProgress.cancel();
                                    if(view != null)
                                        view.showError(activity.getString(R.string.subs_list_empty));
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else
                            {
                                loadingProgress.cancel();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if (view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    private void callSubscriptionApi(String purchaseId){

        if(networkStateHolder.isConnected()) {
            loadingProgress.show();
            Map<String, Object> body = new HashMap<>();
            body.put(ApiConfig.SubsPlan.PLAN_ID,purchaseId);
            body.put(ApiConfig.SubsPlan.PAYMENT_GETWAY_TAX_ID,String.valueOf(System.currentTimeMillis()));
            body.put(ApiConfig.SubsPlan.PAYMENT_GETWAY, AppConfig.DEFAULT_PAYMENT_GETWAY);
            body.put(ApiConfig.SubsPlan.USER_PURCHASE_TIME,String.valueOf(System.currentTimeMillis()));
            service.postSubscription(dataSource.getToken(), "en", body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value)
                        {
                            loadingProgress.cancel();
                            if(value.code() == 200)
                            {
                                try{
                                    dataModel.parseSubscription(value.body().string());
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                if(view != null)
                                    view.showMessage(activity.getString(R.string.plan_purchase_successful));
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception ignored){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }

    }

    @Override
    public void onInappSubscribe(int position)
    {
        String purchaseId=dataModel.getSubsPurchaseId(position);
        if(!TextUtils.isEmpty(purchaseId))
        {
            InnAppSdk.getVendor().purchasedItem(activity, purchaseId, InnAppSdk.Type.SUB, dataSource.getUserId(), new InAppCallBack.OnPurchased()
            {
                @Override
                public void onSuccess(String productId)
                {
                    String id=dataModel.extractIDFromKey(productId);
                    if(TextUtils.isEmpty(id))
                    {
                        if(view!=null)
                            view.showError("Error on updating !");
                    }else
                    {
                        callSubscriptionApi(id);
                    }
                }
                @Override
                public void onError(String id, String error)
                {
                    if(view != null)
                        view.showError(error);
                }
            });
        }else
        {
            if(view != null)
                view.showError("Error on purchasing "+purchaseId);
        }
    }

    @Override
    public void onNoThanks() {}

    @Override
    public void openChat()
    {
        UserProfileData userProfileData = dataModel.getUserProfileData();
        if(userProfileData != null){
            loadSpendCoinDialogForChat(userProfileData);
        }
    }

    private void loadSpendCoinDialogForChat(UserProfileData userProfileData) {
        if(userProfileData != null) {
            isMatchedUser = userProfileData.getIsMatch() > 0;
        }

        if(isMatchedUser){
            launchChat();
        }
        else{
            if(dataModel.isEnoughWalletBalanceToChat()){
                if(dataModel.isDialogDontNeedToShowForChat()){
                    launchChat();
                }
                else{
                    //launch spend on chat dialog.
                    launchSpendCoinDialogForChat();
                }
            }
            else{
                //launch wallet empty dialog for chat.
                launchWalletEmptyDialogForChat();
            }
        }
    }

    private void launchSpendCoinDialogForSuperLike() {
        isForSuperLike = true;
        Integer coinSpend = coinConfigWrapper.getCoinData().getSuperLike().getCoin();
        boolean isFemale = dataModel.isUserFemale();
        String title = String.format(Locale.ENGLISH,"Spend %s coins to let %s know you really like %s by super liking.",
                coinSpend,dataModel.getUserName(),isFemale?"her":"him");
        String btnText = String.format(Locale.ENGLISH, " I am OK to spend %s coins", coinSpend);
        spendCoinDialog.showCoinSpendDialog(title,activity.getString(R.string.superlike_spend_coin_title), btnText,this);
    }

    private void launchSpendCoinDialogForChat() {
        isForSuperLike = false;
        Integer coinSpend = coinConfigWrapper.getCoinData().getPerMsgWithoutMatch().getCoin();
        String btnText = String.format(Locale.ENGLISH, " I am OK to spend %s coins", coinSpend);
        spendCoinDialog.showCoinSpendDialog(activity.getString(R.string.chat_spend_coin_title), activity.getString(R.string.spend_coin_on_chat_dialog_msg),
                btnText, this);
    }

    private void launchChat(){
        UserProfileData userProfileData = dataModel.getUserProfileData();
        if(userProfileData != null) {
            Intent intent = new Intent(activity, ChatMessageActivity.class);
            intent.putExtra("receiverUid", userProfileData.getId());
            intent.putExtra("receiverName", userProfileData.getFirstName());
            String docId = AppController.getInstance().findDocumentIdOfReceiver(userProfileData.getId(), "");
            boolean initiated = false;
            boolean hasDefaultMessage = true;

            if (docId.isEmpty()) {
                docId = AppController.findDocumentIdOfReceiver(userProfileData.getId(), Utilities.tsInGmt(), userProfileData.getFirstName(),
                        userProfileData.getProfilePic(), "", false, userProfileData.getId(), "", false);
                initiated = true;
                intent.putExtra("chatId","");
            }
            else{
                intent.putExtra("chatId",AppController.getInstance().getDbController().getChatId(docId));
                Map<String, Object> chatInfo = AppController.getInstance().getDbController().getChatInfo(docId);
                if(chatInfo.containsKey("initiated"))
                    initiated = (boolean) chatInfo.get("initiated");
                hasDefaultMessage = (boolean)chatInfo.get("hasDefaultMessage");
            }
            intent.putExtra("documentId", docId);
            intent.putExtra("receiverIdentifier", userProfileData.getId());
            intent.putExtra("receiverImage", userProfileData.getProfilePic());
            intent.putExtra("colorCode", AppController.getInstance().getColorCode(0 % 19));

            if (view != null) {
                AppController.getInstance().getDbController().updateChatUserData(
                        docId,
                        userProfileData.getFirstName(),
                        userProfileData.getProfilePic(),
                        isMatchedUser,
                        userProfileData.getSuperliked() > 0,
                        initiated,
                        hasDefaultMessage
                );
                AppController.getInstance().getDbController().updateChatUserOnline(docId, userProfileData.getOnlineStatus() > 0);
                view.launchChatScreen(intent);
            }
        }
    }

    @Override
    public void onRetry() {
        getUserDetails();
    }

    private void loadWalletBalance()
    {
        if(networkStateHolder.isConnected()) {
            service.getCoinBalance(dataSource.getToken(), dataModel.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                try {
                                    dataModel.parseCoinBalance(value.body().string());
                                } catch (Exception e) {
                                }
                            } else if (value.code() == 401) {
                                AppController.getInstance().appLogout();
                            } else {
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                } catch (Exception e) {
                                }
                            }
                            coinBalanceObserver.publishData(true);
                        }
                        @Override
                        public void onError(Throwable e) {
                            if (view != null)
                                view.showError(activity.getString(R.string.failed_to_get_wallet_balance));
                        }
                    });
        }
        else{
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }

    }

    /*
     * coin dialog callback
     */
    @Override
    public void onOkToSpendCoin(boolean dontShowAgain) {
        if(isForSuperLike) {
            isForSuperLike = false;
            if (view != null)
                view.superLikeUser();
            dataModel.updateSpendCoinShowPref(dontShowAgain);
        }
        else{ //for chat.
            launchChat();
            dataModel.updateShowCoinDialogForChat(dontShowAgain);
        }
    }


    /*
     * coin dialog callback
     */
    @Override
    public void onByMoreCoin() {
        if(view != null)
            view.launchCoinWallet();
    }

    @Override
    public void onMomentClick(int position) {
        ArrayList<MomentsData> momentsDataList =  dataModel.getMomentList();
        if(view != null)
            view.launchMomentVerticalScreen(momentsDataList,position);
    }

    @Override
    public void setMomentClickCallback(MomentsGridAdapter momentsAdapter) {
        momentsAdapter.setClickCallback(this);
    }
}
