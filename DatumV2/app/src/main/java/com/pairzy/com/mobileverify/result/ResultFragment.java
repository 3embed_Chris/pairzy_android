package com.pairzy.com.mobileverify.result;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.mobileverify.AnimatorHandler;
import com.pairzy.com.mobileverify.MobileVerifyActivity;
import com.pairzy.com.mobileverify.MobileVerifyContract;
import com.pairzy.com.mobileverify.otp.Otp_Fragment;
import com.pairzy.com.register.RegisterPage;

import javax.inject.Inject;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * <h2>ResultFragment</h2>
 * <p>
 *
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 04/01/2018.
 **/
@ActivityScoped
public class ResultFragment extends DaggerFragment implements ResultContract.View
{
    private static final int REQUEST_CODE = 1;
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String COUNTRY_CODE= "countryCode";
    public static final String OTP="ENTERED_OTP";
    public static final String CODE="operationCode";
    /*
    * Operation code*/
    public static final String RESEND_OTP_OPERATION="resend_otp";
    public static final String VERIFY_OTP_OPERATION="verifyi_otp";
    public static final String REQUEST_OTP_OPERATION="request_otp";

    @BindString(R.string.ret_try_text)
    String ret_try;
    @Inject
    AnimatorHandler animatorHandler;
    @Inject
    MobileVerifyContract.Presenter mainVerifyPresenter;
    @Inject
    ResultPresenter resultPresenter;
    @BindView(R.id.ivResult)
    ImageView ivResult;
    @BindView(R.id.pbProgress)
    ProgressBar progressBar;
    @BindView(R.id.btnTryAgain)
    Button btnTryAgain;
    @BindView(R.id.tvMessage)
    TextView tvMessage;
    @Inject
    PreferenceTaskDataSource preferencesHelper;
    private String mobile_number;
    private String country_code;
    private Unbinder unbinder;
    private String CURRENT_OPERATION="";
    private String entered_otp="";


    @Inject
    Activity activity;

    @Inject
    public ResultFragment() {}

    @Override
    public void onDestroy()
    {
        unbinder.unbind();
        resultPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        resultPresenter.takeView(this);
        btnTryAgain.requestFocus();
        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        try {
            Bundle data=getArguments();
            assert data != null;
            mobile_number = data.getString(MOBILE_NUMBER);
            country_code=data.getString(COUNTRY_CODE);
            CURRENT_OPERATION=data.getString(CODE);
            assert CURRENT_OPERATION != null;
            if(CURRENT_OPERATION.equals(VERIFY_OTP_OPERATION))
            {
                entered_otp=data.getString(OTP);
            }
        } catch (Exception e)
        {
            resultPresenter.showMessage(e.getMessage());
        }
    }


    @OnClick(R.id.parent_view)
    public void parentLayout(){
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.result_check_fragment, viewGroup, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        resultPresenter.takeView(this);
        perFormRequiredOperation(CURRENT_OPERATION);
        view.requestFocus();
    }

    /**
     * Perform required operation.*/
    public void perFormRequiredOperation(String operation)
    {
        switch (operation)
        {
            case REQUEST_OTP_OPERATION:
                resultPresenter.RequestOtpMobile(country_code,mobile_number);
                break;
            case RESEND_OTP_OPERATION:
                resultPresenter.RequestOtpMobile(country_code,mobile_number);
                break;
            case VERIFY_OTP_OPERATION:
                resultPresenter.verifyOtp(country_code,mobile_number,entered_otp);
                break;
        }
    }


    @OnClick(R.id.btnTryAgain)
    public void tryAgain()
    {
        onReTry();
        //btnTryAgain.setEnabled(false);
        //perFormRequiredOperation(CURRENT_OPERATION);
        //btnTryAgain.setEnabled(true);
        activity.onBackPressed();
    }


    @Override
    public void updateResponseUI(String message)
    {
        showErrorView(message);
    }

    @Override
    public void showMessage(String s)
    {
        mainVerifyPresenter.showMessage(s);
    }

    @Override
    public void openOtpFrg()
    {
        /*
        * Ticking the timer start*/
        mainVerifyPresenter.startTimer();
        Bundle bundle=new Bundle();
        bundle.putString(Otp_Fragment.COUNTRY_CODE,country_code);
        bundle.putString(Otp_Fragment.MOBILE_NUMBER,mobile_number);
        Otp_Fragment otp_fragment=new Otp_Fragment();
        otp_fragment.setArguments(bundle);
        mainVerifyPresenter.moveFragment(otp_fragment,true);
    }

    @Override
    public void onLogin()
    {
        Bundle bundle=new Bundle();
        bundle.putString(MobileVerifyActivity.param1,country_code);
        bundle.putString(MobileVerifyActivity.param2,mobile_number);
        bundle.putString(MobileVerifyActivity.param3,entered_otp);
        mainVerifyPresenter.doLogin(bundle);
    }

    @Override
    public void openSingUp()
    {
        Bundle bundle=new Bundle();
        bundle.putString(RegisterPage.COUNTRY_CODE,country_code);
        bundle.putString(RegisterPage.MOBILE_NUMBER,mobile_number);
        bundle.putString(RegisterPage.OTP,entered_otp);
        mainVerifyPresenter.doSingUp(bundle);
    }

    @Override
    public void mobileNotAvailable()
    {
        preferencesHelper.setIsSignUp(true);
        resultPresenter.sendOtp(country_code+""+mobile_number);
    }

    @Override
    public void onlyRequestForOtp(){
        resultPresenter.sendOtp(country_code+""+mobile_number);
    }

    @Override
    public void goBackToMainPage() {
        tryAgain();
    }

    @Override
    public void updateMessageText(String message) {
        tvMessage.setText(message);
    }

    @Override
    public void mobileAvailable()
    {
        preferencesHelper.setIsSignUp(false);
        resultPresenter.sendOtp(country_code+""+mobile_number);
    }

    @Override
    public void otpSent()
    {
        sentOtp_UI();
    }

    @Override
    public void finishActivity(String phno) {
        mainVerifyPresenter.setResultData(phno);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CODE)
        {
            resultPresenter.sendOtp(country_code+""+mobile_number);
        }
    }

    /*
     *Do animation for the UI Change */
    private void sentOtp_UI()
    {
        progressBar.setVisibility(View.GONE);
        Animation animation=animatorHandler.getScaleDown();
        animation.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                scaleUpTick();
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        progressBar.setAnimation(animation);
    }
    /*
     *Scale up tick mark view */
    private void scaleUpTick()
    {
        tvMessage.setText(R.string.otp_sent);
        ivResult.setImageResource(R.drawable.sent_check);
        ivResult.setVisibility(View.VISIBLE);
        Animation in_anim=animatorHandler.getScaleUp();
        in_anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
              resultPresenter.openOtpPage();
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        ivResult.setAnimation(in_anim);
    }
    /*
    * Error on api*/
    private void showErrorView(String message)
    {
        progressBar.setVisibility(View.GONE);
        Animation animation=animatorHandler.getScaleDown();
        animation.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                scaleUpError(message);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        progressBar.setAnimation(animation);
    }
    /*
     *Scale up tick mark view */
    private void scaleUpError(String message)
    {
        tvMessage.setText(message);
        ivResult.setImageResource(R.drawable.ic_error);
        ivResult.setVisibility(View.VISIBLE);
        Animation in_anim=animatorHandler.getScaleUp();
        in_anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                btnTryAgain.setVisibility(View.VISIBLE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        ivResult.setAnimation(in_anim);
    }

    /*
     * handel retry.*/
    private void onReTry()
    {
        ivResult.clearAnimation();
        ivResult.setVisibility(View.GONE);
        btnTryAgain.setVisibility(View.GONE);
        tvMessage.setText(ret_try);
        progressBar.setVisibility(View.VISIBLE);
    }

}
