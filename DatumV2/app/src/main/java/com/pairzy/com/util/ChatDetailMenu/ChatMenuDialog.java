package com.pairzy.com.util.ChatDetailMenu;

import android.annotation.SuppressLint;
import android.app.Activity;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.Locale;

/**
 * <h2>ChatMenuDialog</h2>
 * <P>
 *
 * </P>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ChatMenuDialog
{
    private Activity activity;
    private BottomSheetDialog dialog;
    private TypeFaceManager typeFaceManager;
    private Utility utility;
    private ChatMenuCallback callBack;

    public ChatMenuDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility)
    {
        this.activity=activity;
        this.typeFaceManager=typeFaceManager;
        this.utility=utility;
        dialog = new BottomSheetDialog(activity,R.style.DatumBottomDialog);
    }

    public void showDialog(String user_name,boolean isMatched, boolean isNotificationMuted, boolean isBlocked, ChatMenuCallback callBack)
    {
        this.callBack = callBack;
        if(dialog.isShowing())
        {
            dialog.cancel();
        }
        user_name=utility.formatString( user_name);
        @SuppressLint("InflateParams")
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.chat_deatil_menu_dialog, null);
        Button btnDeleteChat=dialogView.findViewById(R.id.btn_delete_chat);
        btnDeleteChat.setTypeface(typeFaceManager.getCircularAirBook());
        btnDeleteChat.setOnClickListener(view -> {
            //pending
            if(callBack != null)
                callBack.onDeleteChat();
            if(dialog!=null)
                dialog.cancel();
        });

        Button btnMuteNotification=dialogView.findViewById(R.id.btn_mute_notification);
        if(isNotificationMuted) {
            btnMuteNotification.setText(String.format(Locale.ENGLISH, "Unmute Notification"));
        }
        else {
            btnMuteNotification.setText(String.format(Locale.ENGLISH, "Mute Notification"));
        }
        btnMuteNotification.setTypeface(typeFaceManager.getCircularAirBook());
        btnMuteNotification.setOnClickListener(view -> {
            //pending
            if(callBack != null)
                callBack.onMuteNotification(!isNotificationMuted);
            if(dialog!=null)
                dialog.cancel();
        });
        Button btnReport=dialogView.findViewById(R.id.btn_report);
        btnReport.setText(String.format(Locale.ENGLISH,"Report %s",user_name));
        btnReport.setTypeface(typeFaceManager.getCircularAirBook());
        btnReport.setOnClickListener(view -> {
            if(callBack != null)
                callBack.onReportUser();
            if(dialog!=null)
                dialog.cancel();
        });
        Button btnBlock =dialogView.findViewById(R.id.btn_block);
        if(isBlocked) {
            btnBlock.setText(String.format(Locale.ENGLISH, "Unblock %s", user_name));
        }
        else {
            btnBlock.setText(String.format(Locale.ENGLISH, "Block %s", user_name));
        }
        btnBlock.setTypeface(typeFaceManager.getCircularAirBook());
        btnBlock.setOnClickListener(view -> {
            if(isBlocked) {
                if (callBack != null)
                    callBack.onUnblockUser();
            }
            else {
                if (callBack != null)
                    callBack.onBlockUser();
            }
            if(dialog!=null)
                dialog.cancel();
        });

        TextView tvDivider = dialogView.findViewById(R.id.divider_unmatch_btn);
        Button btnUnmatch=dialogView.findViewById(R.id.btn_unmatch);
        btnUnmatch.setText(String.format(Locale.ENGLISH,"Unmatch %s",user_name));
        btnUnmatch.setTypeface(typeFaceManager.getCircularAirBook());
        btnUnmatch.setOnClickListener(view -> {
            //pending
            if(callBack != null)
                callBack.onUnmatch();
            if(dialog!=null)
                dialog.cancel();
        });
        if(!isMatched){
            btnUnmatch.setVisibility(View.GONE);
            btnBlock.setBackgroundResource(R.drawable.bottom_corner_white_bg);
            tvDivider.setVisibility(View.GONE);
        }

        Button onCanceled=dialogView.findViewById(R.id.onCanceled);
        onCanceled.setTypeface(typeFaceManager.getCircularAirBook());
        onCanceled.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();
        });
        dialog.setContentView(dialogView);
        dialog.show();
    }
}
