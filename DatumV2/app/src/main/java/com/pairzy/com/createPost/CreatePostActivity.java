package com.pairzy.com.createPost;


import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class CreatePostActivity extends DaggerAppCompatActivity implements CreatePostContract.View {


    @Inject
    TypeFaceManager typeFaceManager;
    @BindView(R.id.createPostViewPager)
    ViewPager viewPager;
    @BindView(R.id.tlCreatePost)
    TabLayout tabLayout;
    private FragmentManager fragmentManager;
    private CreatePostAdapter adapter;
    String title[];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);
        ButterKnife.bind(this);
        Init();
    }

    private void Init() {
        title = getResources().getStringArray(R.array.create_post_tab_titles);
        fragmentManager = getSupportFragmentManager();
        adapter = new CreatePostAdapter(fragmentManager,title);
        adapter.notifyDataSetChanged();
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(typeFaceManager.getCircularAirBold());
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }

    }
}
