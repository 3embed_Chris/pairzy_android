package com.pairzy.com.home.Matches.PostFeed.deletePostDialog;

public interface PostDeleteCallback {
    void onPostDelete(String postId,int position);
}
