package com.pairzy.com.home.Dates.Pending_page.Model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.util.TypeFaceManager;
import com.facebook.drawee.view.SimpleDraweeView;

/**
 * <h2>PastDateItemViewHolder</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class PendingRescheduleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    private ItemActionCallBack callBack;
    public SimpleDraweeView user_profile_pic,main_image_view;
    public ImageView user_status_dot;
    public TextView name,user_message;
    public TextView locationTv, dateTimeTv, tvTimeText;
    public RelativeLayout rlReschedule;

    public PendingRescheduleViewHolder(View itemView, ItemActionCallBack callBack, TypeFaceManager typeFaceManager)
    {
        super(itemView);
        this.callBack=callBack;
        user_profile_pic=itemView.findViewById(R.id.user_profile_pic);
        user_status_dot=itemView.findViewById(R.id.user_status_dot);
        name=itemView.findViewById(R.id.name);
        name.setTypeface(typeFaceManager.getCircularAirBold());
        main_image_view=itemView.findViewById(R.id.main_image_view);
        user_message=itemView.findViewById(R.id.user_message);
        user_message.setTypeface(typeFaceManager.getCircularAirBold());
        rlReschedule = itemView.findViewById(R.id.reschedule_button);
        rlReschedule.setSelected(true);
        rlReschedule.setOnClickListener(this);
        itemView.findViewById(R.id.cancel_button).setOnClickListener(this);
        locationTv = itemView.findViewById(R.id.location_tv);
        locationTv.setTypeface(typeFaceManager.getCircularAirBook());
        dateTimeTv = itemView.findViewById(R.id.date_time_tv);
        dateTimeTv.setTypeface(typeFaceManager.getCircularAirBook());
        tvTimeText = itemView.findViewById(R.id.time_text);
        tvTimeText.setTypeface(typeFaceManager.getCircularAirBook());
        RelativeLayout rlHeader = itemView.findViewById(R.id.rl_header_reschedule);
        rlHeader.setOnClickListener(this);
        main_image_view.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        if (callBack != null)
            callBack.onClick(view.getId(), this.getAdapterPosition());
    }
}
