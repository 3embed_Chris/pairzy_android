package com.pairzy.com.MySearchPreference.Model;
import com.pairzy.com.data.model.SearchPreference;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * <h2>PreferenceItem</h2>
 * @since  3/7/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class PreferenceItem
{
    @SerializedName("searchPreferences")
    @Expose
    private ArrayList<SearchPreference> searchPreferences = null;
    @SerializedName("myPreferences")
    @Expose
    private ArrayList<MyPreference> myPreferences = null;

    public ArrayList<SearchPreference> getSearchPreferences() {
        return searchPreferences;
    }

    public void setSearchPreferences(ArrayList<SearchPreference> searchPreferences)
    {
        this.searchPreferences = searchPreferences;
    }

    public ArrayList<MyPreference> getMyPreferences() {
        return myPreferences;
    }

    public void setMyPreferences(ArrayList<MyPreference> myPreferences) {
        this.myPreferences = myPreferences;
    }
}
