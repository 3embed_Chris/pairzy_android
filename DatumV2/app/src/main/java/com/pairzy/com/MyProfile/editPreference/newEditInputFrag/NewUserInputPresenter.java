package com.pairzy.com.MyProfile.editPreference.newEditInputFrag;

import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.Utility;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * <h2>NewUserInputPresenter</h2>
 * <P>
 *
 * </P>
 * @since  2/26/2018.
 * @author 3Embed.
 * @version 1.0.
 */
class NewUserInputPresenter implements NewUserInputContract.Presenter
{
    @Inject
    Utility utility;
    private NewUserInputContract.View view;
    private CompositeDisposable compositeDisposable;
    @Inject
    DatumProgressDialog progressDialog;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    NewUserInputModel model;
    @Inject
    NetworkService service;

    @Inject
    NewUserInputPresenter(){
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(Object view)
    {
        this.view= (NewUserInputContract.View) view;
    }

    @Override
    public void dropView()
    {
        view=null;
        compositeDisposable.clear();
    }

    @Override
    public void updatePreference(String pref_Id,String value)
    {


    }

    @Override
    public void showError() {
        if(view!=null)
        {
            view.showError("Please enter your "+view.getErrorTitle().toLowerCase()+"!");
        }
    }
}
