package com.pairzy.com.coinWallet.Model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.util.TypeFaceManager;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * <h>AllCoinViewHolder</h>
 * @author 3Embed.
 * @since 30/5/18.
 */
public class AllCoinViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.coin_image_iv)
    ImageView ivCoin;
    @BindView(R.id.coin_title_tv)
    TextView tvCoinTitle;
    @BindView(R.id.coin_time_tv)
    TextView tvCoinTime;
    @BindView(R.id.coin_tv)
    TextView tvCoin;
    private ItemActionCallBack callBack;
    private TypeFaceManager typeFaceManager;
    public AllCoinViewHolder(View itemView, ItemActionCallBack callBack, TypeFaceManager typeFaceManager) {
        super(itemView);
        this.callBack = callBack;
        this.typeFaceManager = typeFaceManager;
        ButterKnife.bind(this,itemView);
        itemView.setOnClickListener(this);
        tvCoinTitle.setTypeface(typeFaceManager.getCircularAirBook());
        tvCoinTime.setTypeface(typeFaceManager.getCircularAirLight());
        tvCoin.setTypeface(typeFaceManager.getCircularAirBold());
    }


    @Override
    public void onClick(View v) {
        if(callBack != null)
            callBack.onClick(R.id.coin_item,this.getAdapterPosition());
    }
}
