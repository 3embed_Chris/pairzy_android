package com.pairzy.com.appSettings;

import android.app.Activity;
import android.text.TextUtils;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.boostDialog.BoostAlertCallback;
import com.pairzy.com.util.boostDialog.BoostDialog;
import com.pairzy.com.util.boostDialog.Slide;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.suresh.innapp_purches.InAppCallBack;
import com.suresh.innapp_purches.InnAppSdk;
import com.suresh.innapp_purches.SkuDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class AppSettingsPresenter implements AppSettingsContract.Presenter, BoostAlertCallback {

    @Inject
    BoostDialog boostDialog;
    @Inject
    NetworkStateHolder holder;
    @Inject
    NetworkService service;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    ArrayList<Slide> slideArrayList;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    LoadingProgress loadingProgress;
    @Inject
    Activity  activity;
    @Inject
    AppSettingModel model;
    @Inject
    AppSettingsContract.View view;

    private CompositeDisposable compositeDisposable;

    @Inject
    public AppSettingsPresenter() {compositeDisposable=new CompositeDisposable();}

    @Override
    public void takeView(AppSettingsContract.View view) {

    }

    @Override
    public void dropView() {

    }

    @Override
    public void openBoostDialog()
    {
        launchBoostDialog();
    }


    private void launchBoostDialog() {
            getSubsPlan();
    }
    private void getSubsPlan()
    {
        if(networkStateHolder.isConnected())
        {
            loadingProgress.show();
            service.getSubsPlans(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value)
                        {
                            if(value.code() == 200)
                            {
                                try
                                {
                                    model.parseSubsPlanList(value.body().string());
                                    if (model.isSubsPlanEmpty())
                                    {
                                        loadingProgress.cancel();
                                        if(view != null)
                                            view.showError(activity.getString(R.string.subs_list_empty));
                                    }else
                                    {
                                        model.selectMiddleItem();
                                        List<String> actual_ids=model.collectsIds();
                                        InnAppSdk.getVendor().getProductDetails(activity,actual_ids, InnAppSdk.Type.SUB, new InAppCallBack.DetailsCallback()
                                        {
                                            @Override
                                            public void onSuccess(List<SkuDetails> list, List<String> errorList)
                                            {
                                                loadingProgress.cancel();
                                                if(list.size()>0)
                                                {
                                                    model.updateDetailsData(list);

                                                    if(!model.isSubsPlanEmpty())
                                                    {
                                                        model.selectMiddleItem();
                                                        boostDialog.showAlert(AppSettingsPresenter.this,model.getSubsPlanList(), slideArrayList);
                                                    }

                                                }else
                                                {
                                                    model.clearProductList();
                                                    if(view != null)
                                                        view.showError(activity.getString(R.string.subs_list_empty));
                                                }
                                            }
                                        });
                                    }
                                }catch (Exception e)
                                {
                                    loadingProgress.cancel();
                                    if(view != null)
                                        view.showError(activity.getString(R.string.subs_list_empty));
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else
                            {
                                loadingProgress.cancel();
                            }
                        }
                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if (view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void onInappSubscribe(int position) {
        String purchaseId=model.getSubsPurchaseId(position);
        if(!TextUtils.isEmpty(purchaseId))
        {
            InnAppSdk.getVendor().purchasedItem(activity, purchaseId, InnAppSdk.Type.SUB, dataSource.getUserId(), new InAppCallBack.OnPurchased()
            {
                @Override
                public void onSuccess(String productId)
                {
                    String id=model.extractIDFromKey(productId);
                    if(TextUtils.isEmpty(id))
                    {
                        if(view!=null)
                            view.showError("Error on updating !");
                    }else
                    {
                        callSubscriptionApi(id);
                    }
                }

                @Override
                public void onError(String id, String error)
                {
                    if(view != null)
                        view.showError(error);
                }
            });
        }else
        {
            if(view != null)
                view.showError("Error on purchasing "+purchaseId);
        }
    }

    @Override
    public void onNoThanks() { }

    private void callSubscriptionApi(String id)
    {
        if(networkStateHolder.isConnected())
        {
            loadingProgress.show();
            Map<String, Object> body = new HashMap<>();
            body.put(ApiConfig.SubsPlan.PLAN_ID,id);
            body.put(ApiConfig.SubsPlan.PAYMENT_GETWAY_TAX_ID,String.valueOf(System.currentTimeMillis()));
            body.put(ApiConfig.SubsPlan.PAYMENT_GETWAY, AppConfig.DEFAULT_PAYMENT_GETWAY);
            body.put(ApiConfig.SubsPlan.USER_PURCHASE_TIME,String.valueOf(System.currentTimeMillis()));
            service.postSubscription(dataSource.getToken(), model.getLanguage(), body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value)
                        {
                            loadingProgress.cancel();

                            if(value.code() == 200)
                            {
                                try{
                                    String  responnse=value.body().string();
                                    model.parseSubscription(responnse);
                                    if(view != null)
                                        view.showMessage(activity.getString(R.string.plan_purchase_successful));
                                }catch (Exception e)
                                {
                                    e.printStackTrace();
                                    if(view != null)
                                        view.showError(activity.getString(R.string.something_worng_text));
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception ignored){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view != null)
                                view.showError(activity.getString(R.string.api_server_error));
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }

    }
}
