package com.pairzy.com.home.Matches.PostFeed;

import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>PostFeedFragBuilder</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface PostFeedFragBuilder
{
    @FragmentScoped
    @Binds
    PostFeedFrag getProspectItemFragment(PostFeedFrag availableFrg);

    @FragmentScoped
    @Binds
    PostFeedFragContract.Presenter upcomingFragPresenter(PostFeedFragPresenter presenter);

}

