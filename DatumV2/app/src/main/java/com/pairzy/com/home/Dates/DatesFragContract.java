package com.pairzy.com.home.Dates;

import android.content.Intent;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;

/**
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface DatesFragContract
{
    interface View extends BaseView
    {
        void notifyAdapter();

        void showError(int errorId);
        void showError(String error);
        void showMessage(String message);
        void showNetworkError(String error, boolean pendingDate);
        void showNetworkError(String error);
        void showLoading(boolean pendingFrg);
        void showPastDateLoading();
        void notifyPastDateAdapter();
        void showPendingDateCount(int pendingDateCount);
        void launchUserProfile(Intent intent);
        void showCoinBalance(String coinBalance);
    }

    interface Presenter extends BasePresenter<View>
    {
        void fetchList(boolean isPending, boolean isRefresh);

        void loadCurrentDate(int offset, int limit);

        void preFetchImage(boolean fromPendingList, int positionView);

        void preFetchPastDateListImage(int positionView);

        void fetchPastDateList(boolean isRefresh);

        void addToTheUpcomingList(DateListPojo removedDate);
        void launchUserProfile(PastDateListPojo pastDateListPojo);
        void launchUserProfile(DateListPojo dateListPojo);
        void addToThePastDate(PastDateListPojo pastDateListPojo);
        void updatePendingDateCount(int pendingDateCount);
        void initDateRefreshObserver();
        void notifyPendingAdapter();

        void refreshDateList();

        boolean isDateRefreshReq();
    }
}
