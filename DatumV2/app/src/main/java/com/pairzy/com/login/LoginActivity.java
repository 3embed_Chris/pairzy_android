package com.pairzy.com.login;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.pairzy.com.mobileverify.MobileVerifyActivity;
import com.pairzy.com.register.RegisterPage;
import com.pairzy.com.util.accountKit.AccountKitManager;
import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatDelegate;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.UserPreference.MyPreferencePage;
import com.pairzy.com.home.HomeActivity;
import com.pairzy.com.util.CustomVideoView.NoInterNetView;
import com.pairzy.com.util.SpanFontStyle;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.webPage.WebActivity;
import com.facebookmanager.com.FacebookManager;

import java.util.Timer;

import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h2>LoginActivity</h2>
 * <p>
 * Login screen contains login/signup.
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 04/01/2018.
 **/
public class LoginActivity extends BaseDaggerActivity implements LoginContract.View
{
    @Inject
    AccountKitManager accountKitManager;
    @Inject
    LoginContract.Presenter presenter;
    @Inject
    FacebookManager facebookManager;
    @Inject
    Activity activity;
    @Inject
    SliderAdapter adapter;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.btnFacebook)
    Button btnFacebook;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.indicator)
    TabLayout indicator;
    @BindView(R.id.arrow_up)
    ImageView arrow_up;
    @BindView(R.id.tvFacebookDetail)
    TextView tvFacebookDetail;
    @BindView(R.id.tvTermsPrivacyText)
    TextView tvTermsPrivacyText;
    @BindView(R.id.privacy_title)
    TextView privacy_title;
    @BindView(R.id.privacy_sub_title)
    TextView privacy_sub_title;
    @BindView(R.id.privacy_details)
    TextView privacy_details;
    @BindView(R.id.bottom_hidden_view)
    RelativeLayout bottom_hidden_view;
    @BindView(R.id.no_internetView)
    NoInterNetView no_internetView;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    AnimatorHandler animatorHandler;
    private Unbinder unbinder;
    @BindView(R.id.bottom_view)
    RelativeLayout bottom_view;
    @BindView(R.id.top_view)
    RelativeLayout top_view;
    @BindView(R.id.login_parent)
    LinearLayout login_parent;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private int currentPage = 0;
    private int pageSize = 4;
    private Timer timer;
    private SlideTimerTask slideTimerTask;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (currentPage == pageSize) {
                currentPage = 0;
            }
            if(viewPager != null)
             viewPager.setCurrentItem(currentPage++, true);
        }
    };

    private final int duration = 550; //millisec
    private AlertDialog updateAlertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        unbinder = ButterKnife.bind(this);
        viewPager.setAdapter(adapter);
        indicator.setupWithViewPager(viewPager, true);
        textToLink();
        applyingFont();
        initButtonAnimation();
        //presenter.initNetworkObserver();
        setupSliderTimer();

    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.checkForForceUpdate();
    }

    private void setupSliderTimer() {
        if(timer == null)
            timer = new Timer();
        if(slideTimerTask == null)
            slideTimerTask = new SlideTimerTask(runnable);
        timer.schedule(slideTimerTask,2300,2500);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    /*
             * Initialization of the xml content.*/
    private void initButtonAnimation()
    {
        btnFacebook.startAnimation(animatorHandler.getScaleUp());
        btnFacebook.setSelected(true);
    }

    /*
     * Changing the fonts*/
    private void applyingFont()
    {
        btnLogin.setTypeface(typeFaceManager.getCircularAirBook());
        btnFacebook.setTypeface(typeFaceManager.getCircularAirBook());
        tvFacebookDetail.setTypeface(typeFaceManager.getCircularAirBook());
        tvTermsPrivacyText.setTypeface(typeFaceManager.getCircularAirBook());
        privacy_title.setTypeface(typeFaceManager.getCircularAirBold());
        privacy_sub_title.setTypeface(typeFaceManager.getCircularAirBook());
        privacy_details.setTypeface(typeFaceManager.getCircularAirBook());
    }

    /*
     *  As fun name suggest it will convert text of TextView(tvTermsPrivacyText)
     *  (Terms Of Service) and
     *  (Privacy policy) to Clickable span.
     */
    private void textToLink()
    {
        String termsStr = tvTermsPrivacyText.getText().toString();
        String termsAndService="Terms of Service";
        String privacyPolicy="Privacy Policy";
        SpannableString ss=new SpannableString(termsStr);
        ClickableSpan clickableSpanOne = new ClickableSpan() {
            @Override
            public void onClick(View view)
            {
                presenter.loadWebActivity(getString(R.string.terms_of_service),getString(R.string.urlTermsOfService));
            }
        };

        ClickableSpan clickableSpanTwo = new ClickableSpan() {
            @Override
            public void onClick(View view)
            {
                presenter.loadWebActivity(getString(R.string.privacy_policy_title),getString(R.string.urlPrivacyPolicy));
            }
        };
        int start1 = termsStr.indexOf(termsAndService);
        int start2 = termsStr.indexOf(privacyPolicy);
        ss.setSpan(clickableSpanOne,start1,start1+termsAndService.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new SpanFontStyle(typeFaceManager.getCircularAirBook(),R.color.dark_gray),start1,start1+termsAndService.length(),0);
        ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.darkGray)),start1,start1+termsAndService.length(),0);
        ss.setSpan(clickableSpanTwo,start2,start2+privacyPolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new SpanFontStyle(typeFaceManager.getCircularAirBook(),R.color.darkGray),start2,start2+privacyPolicy.length(),0);
        ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.darkGray)),start2,start2+privacyPolicy.length(),0);
        tvTermsPrivacyText.setText(ss);
        tvTermsPrivacyText.setMovementMethod(LinkMovementMethod.getInstance());
        tvTermsPrivacyText.setHighlightColor(Color.TRANSPARENT);
    }


    @OnClick(R.id.btnLogin)
    public void login()
    {
        if(btnFacebook.isSelected())
        {
            btnFacebook.startAnimation(animatorHandler.getScaleDown());
        }

        if(!btnLogin.isSelected())
        {
            Animation animation=animatorHandler.getScaleUp();
            btnLogin.startAnimation(animation);
            animation.setAnimationListener(new Animation.AnimationListener()
            {
                @Override
                public void onAnimationStart(Animation animation) {}
                @Override
                public void onAnimationEnd(Animation animation)
                {
                    if(activity!=null)
                    {
                      //  presenter.initPhoneLogin();
                        startActivity(new Intent(activity, MobileVerifyActivity.class));
                    }
                }
                @Override
                public void onAnimationRepeat(Animation animation) {}
            });
        }
        else
        {
            if(activity!=null)
//                presenter.initPhoneLogin();
                startActivity(new Intent(activity, MobileVerifyActivity.class));
        }
        btnLogin.setSelected(true);
        btnFacebook.setSelected(false);
    }


    @OnClick(R.id.arrow_up)
    public void closeBottomView()
    {
        presenter.hideBottomView();
    }


    @OnClick(R.id.tvFacebookDetail)
    public void callBottomView()
    {
        presenter.showBottomView();
    }


    @OnClick(R.id.btnFacebook)
    public void facebook()
    {
        if(!btnFacebook.isSelected())
        {
            Animation animation=animatorHandler.getScaleUp();
            btnFacebook.startAnimation(animation);
            animation.setAnimationListener(new Animation.AnimationListener()
            {
                @Override
                public void onAnimationStart(Animation animation) {}
                @Override
                public void onAnimationEnd(Animation animation)
                {
                    presenter.initFacebook();
                }
                @Override
                public void onAnimationRepeat(Animation animation) {}
            });
        } else{
            presenter.initFacebook();
        }
        if(btnLogin.isSelected())
        {
            btnLogin.startAnimation(animatorHandler.getScaleDown());
        }
        btnFacebook.setSelected(true);
        btnLogin.setSelected(false);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (!facebookManager.onActivityResult(requestCode, resultCode, data))
        {
            accountKitManager.onActivityResult(requestCode,resultCode,data);
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(login_parent,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void facebookError(String error)
    {
        Snackbar snackbar = Snackbar
                .make(login_parent,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void facebookCancel(String cancel)
    {
        Snackbar snackbar = Snackbar
                .make(login_parent,""+cancel, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }



    @Override
    public void facebookLoginSuccess()
    {
        Intent home_intent=new Intent(this,HomeActivity.class);
        home_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(home_intent);
        finish();
    }



    @Override
    public void facebookSignUpSuccess()
    {
        startActivity(new Intent(this,MyPreferencePage.class));
    }

    @Override
    public void showWebActivity(String title, @NonNull String url)
    {
        Intent intent = new Intent(LoginActivity.this, WebActivity.class);
        intent.putExtra("url",url);
        intent.putExtra("title",title);
        startActivity(intent);
    }


    @Override
    public void showBottomView()
    {
        handelBottomUpAnimation();
    }

    @Override
    public void hideBottomView()
    {
        hideOpenedBottomView();
    }

    @Override
    public void updateInterNetStatus(boolean status)
    {
       if(no_internetView!=null)
        {
            if(status)
            {
                no_internetView.internetFound();
            }else
            {
                no_internetView.showNoInternet();
            }
        }
    }

    @Override
    public void openRegisterPage(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void showUpdateDialog(boolean isMandatory) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.UpdateAlertDialog);
        builder.setTitle(R.string.update_app_dialog_title)
                .setMessage(R.string.update_app_dialog_message)
                .setPositiveButton(R.string.update_dialog_positive_button_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent viewIntent =
                                new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(getString(R.string.datum_play_store)));
                        startActivity(viewIntent);
                    }
                })
                .setNegativeButton(R.string.update_dialog_nagtive_button_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(isMandatory) {
                            dialog.dismiss();
                            finish();
                        }else{
                            dialog.dismiss();
                        }
                    }
                })
                .setCancelable(!isMandatory);

        updateAlertDialog = builder.create();
        updateAlertDialog.show();
    }

    @Override
    public void dismissUpdateDialog() {
        if(updateAlertDialog != null && updateAlertDialog.isShowing())
            updateAlertDialog.dismiss();
    }

    @Override
    public void openSingUp(Bundle data)
    {
        //mainVerifyPresenter.doSingUp(bundle);
        Intent intent=new Intent(this, RegisterPage.class);
        intent.putExtras(data);
        startActivity(intent);
    }

    @Override
    public void onLogin(Bundle data) {
        Intent intent=new Intent(this,HomeActivity.class);
        intent.putExtras(data);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    protected void onDestroy()
    {
        unbinder.unbind();
        presenter.dropView();
        if(slideTimerTask != null)
            slideTimerTask.cancel();
        if(timer != null)
            timer.cancel();
        super.onDestroy();
    }

    /*
     *Sow bottom view */
    private void handelBottomUpAnimation()
    {
        bottom_hidden_view.setVisibility(View.VISIBLE);
        Animation animation=animatorHandler.getBottomIn();
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {}
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        bottom_view.setAnimation(animation);
        top_view.setVisibility(View.GONE);
        Animation out_anim=animatorHandler.getTopOut();
        out_anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                tvFacebookDetail.setVisibility(View.INVISIBLE);
                tvTermsPrivacyText.setVisibility(View.INVISIBLE);
                arrow_up.setVisibility(View.VISIBLE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        top_view.setAnimation(out_anim);
        animation.start();
        out_anim.start();
    }

    /*
     *Hidding the bottom view */
    private void hideOpenedBottomView()
    {
        Animation animation=animatorHandler.getBottomOut();
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                bottom_hidden_view.setVisibility(View.GONE);
                arrow_up.setVisibility(View.GONE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        bottom_view.setAnimation(animation);

        top_view.setVisibility(View.VISIBLE);
        Animation in_anim=animatorHandler.getTopIn();
        in_anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                tvFacebookDetail.setVisibility(View.VISIBLE);
                tvTermsPrivacyText.setVisibility(View.VISIBLE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        top_view.setAnimation(in_anim);
        animation.start();
        in_anim.start();
    }

}