package com.pairzy.com.home.MakeMatch.swipeCardModel;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.pairzy.com.R;
import com.pairzy.com.home.MakeMatch.model.MakeMatchDataPOJO;
import com.pairzy.com.util.TypeFaceManager;

public class MatchMakerSwipeAdapter extends BaseAdapter {

    private MakeMatchDataPOJO makeMatchDataPOJO;
    private TypeFaceManager typeFaceManager;

    MatchMakerSwipeAdapter(MakeMatchDataPOJO makeMatchDataPOJO, TypeFaceManager typeFaceManager) {
        this.makeMatchDataPOJO=makeMatchDataPOJO;
        this.typeFaceManager=typeFaceManager;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View cardview, ViewGroup parent) {
        MatchMakerSwipeViewHolder holder=new MatchMakerSwipeViewHolder();
        if (cardview == null) {
            cardview = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_make_match_item, parent, false);

            holder.userTwoName=cardview.findViewById(R.id.tv_profile_two_name);
            holder.userTwoAge=cardview.findViewById(R.id.tv_profile_two_age);
            holder.userTwo=cardview.findViewById(R.id.sdv_profile_two);

            holder.userOneName=cardview.findViewById(R.id.tv_profile_one_name);
            holder.userOneAge=cardview.findViewById(R.id.tv_profile_one_age);
            holder.userOne=cardview.findViewById(R.id.sdv_profile_one);

            holder.item_swipe_left_indicator=cardview.findViewById(R.id.item_swipe_left_indicator);
            holder.item_swipe_right_indicator=cardview.findViewById(R.id.item_swipe_right_indicator);

            //todo init font family
            holder.userTwoName.setTypeface(typeFaceManager.getCircularAirBold());
            holder.userTwoAge.setTypeface(typeFaceManager.getCircularAirLight());
            holder.userOneName.setTypeface(typeFaceManager.getCircularAirBold());
            holder.userOneAge.setTypeface(typeFaceManager.getCircularAirLight());

            //todo binding data
            holder.userTwoName.setText(makeMatchDataPOJO.getUser2Name());
            holder.userTwoAge.setText(makeMatchDataPOJO.getUser2Age()+", "+makeMatchDataPOJO.getUser2HeightInFeet());
            holder.userTwo.setImageURI(makeMatchDataPOJO.getUser2ProfilePic());

            holder.userOneName.setText(makeMatchDataPOJO.getUser1Name());
            holder.userOneAge.setText(makeMatchDataPOJO.getUser1Age()+", "+makeMatchDataPOJO.getUser1HeightInFeet());
            holder.userOne.setImageURI(makeMatchDataPOJO.getUser1ProfilePic());

        }
        return cardview;
    }
}
