package com.pairzy.com.mobileverify.otp;

import android.text.TextUtils;
import javax.inject.Inject;
/**
 * <h2>OtpPresenter</h2>
 * <P>
 * </P>
 * @author 3Embed.
 * @version 2.0.
 * @since 04/01/2018.
 */
public class OtpPresenter implements OtpContract.Presenter
{
    private OtpContract.View view;

    @Inject
    OtpPresenter() {}
    @Override
    public void showError(String message)
    {
        if(view!=null)
            view.showMessage(message);
    }

    @Override
    public void takeView(Object view)
    {
        this.view= (OtpContract.View) view;
    }


    @Override
    public void notHaveOtpEvent()
    {
        if(view!=null)
            view.notHaveOtp();
    }

    @Override
    public void validateInput(String data)
    {
        if(TextUtils.isEmpty(data))
        {
            if(view!=null)
                view.disableNext();
        }else
        {
            if(view!=null)
                view.verifiedOtp(data);
        }
    }

    @Override
    public void onPrivacyEvent()
    {
        if(view!=null)
            view.openPrivacyPolice();
    }

    @Override
    public void onTermsEvent()
    {
        if(view!=null)
            view.TermAndServices();
    }

    @Override
    public void onContinueEvent()
    {}

    @Override
    public void dropView()
    {
        view=null;
    }
}
