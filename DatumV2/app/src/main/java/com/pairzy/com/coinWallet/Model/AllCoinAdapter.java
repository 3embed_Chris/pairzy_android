package com.pairzy.com.coinWallet.Model;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pairzy.com.R;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import java.util.Locale;

/**
 * <h>AllCoinAdapter</h>
 * <p>shows the all coin history list.</p>
 * @author 3Embed.
 * @since 30/5/18.
 */
public class AllCoinAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private final int LOADING = 0 , ITEM =1;
    //private final int CREDIT=1,DEBIT= 2,PURCHASE = 3;
    private final String CREDIT="CREDIT",DEBIT= "DEBIT",PURCHASE = "PURCHSE";
    private ArrayList<CoinPojo> arrayList;
    private ItemActionCallBack callBack;
    private TypeFaceManager typeFaceManager;
    private Utility utility;

    public void setCallBack(ItemActionCallBack callBack) {
        this.callBack = callBack;
    }

    public AllCoinAdapter(ArrayList<CoinPojo> arrayList, TypeFaceManager typeFaceManager, Utility utility) {
        this.arrayList = arrayList;
        this.typeFaceManager = typeFaceManager;
        this.utility = utility;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case LOADING:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.load_more_layout,parent,false);
                return new CoinLoadingViewHolder(view,callBack,typeFaceManager);
            case ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.coin_item_layout,parent,false);
                return new AllCoinViewHolder(view,callBack,typeFaceManager);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        CoinPojo coinPojo = arrayList.get(position);
        if(coinPojo.isLoading()){
            return LOADING;
        }
        return ITEM;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CoinPojo coinPojo = arrayList.get(position);
        if(coinPojo.isLoading()){
            try {
                bindLoadingView((CoinLoadingViewHolder) holder);
            }catch (Exception e){}
        }
        else{
            try {
                bindCoinItemView((AllCoinViewHolder) holder);
            }catch (Exception e){}
        }
    }

    private void bindCoinItemView(AllCoinViewHolder holder) {
        CoinPojo coinPojo = arrayList.get(holder.getAdapterPosition());
        if(coinPojo.getTxnType().equalsIgnoreCase(PURCHASE) || coinPojo.getTxnType().equalsIgnoreCase(CREDIT)){
            holder.ivCoin.setImageResource(R.drawable.ic_arrow_downward_white_24dp);
            holder.ivCoin.setBackgroundResource(R.drawable.coin_in_bg);
            //holder.tvCoinTitle.setText(String.format(Locale.ENGLISH,"Added %d Coins to wallet.",coinPojo.getCoinAmount()));
            if(!TextUtils.isEmpty(coinPojo.getTrigger())) {
                holder.tvCoinTitle.setText(coinPojo.getTrigger());
            }
            else{
                holder.tvCoinTitle.setText(String.format(Locale.ENGLISH,"Added %d Coins to wallet.",coinPojo.getCoinAmount()));
            }
            holder.tvCoin.setText(""+coinPojo.getCoinAmount());
            //holder.tvCoinTitle.setText(coinPojo.getTrigger());
            //holder.tvCoinTime.setText(utility.getFormatDateTime(Long.valueOf(coinPojo.getTimestamp())));
            holder.tvCoinTime.setText(utility.getFormatDateTime(coinPojo.getTimestamp()));
        }
        else{
            holder.ivCoin.setImageResource(R.drawable.ic_arrow_upward_white_24dp);
            holder.ivCoin.setBackgroundResource(R.drawable.coin_out_bg);

            if(!TextUtils.isEmpty(coinPojo.getTrigger())) {
                holder.tvCoinTitle.setText(coinPojo.getTrigger());
            }
            else{
                holder.tvCoinTitle.setText(String.format(Locale.ENGLISH,"Spend %d Coins on %s.",coinPojo.getCoinAmount(),coinPojo.getTrigger()));
            }

            holder.tvCoinTime.setText(utility.getFormatDateTime(coinPojo.getTimestamp()));

            holder.tvCoin.setText(""+coinPojo.getCoinAmount());
        }
        //holder.tvCoin.setText(coinPojo.getCoinClosingBalance());
    }

    private void bindLoadingView(CoinLoadingViewHolder holder) {
        CoinPojo coinPojo = arrayList.get(holder.getAdapterPosition());
        if(coinPojo.isLoadingError()){
            holder.setFailed();
        }
        else{
            holder.showLoadingLoadingAgain();
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
