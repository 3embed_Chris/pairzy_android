package com.pairzy.com.util.accountKit;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;

public class AccountKitManagerImpl implements AccountKitManager {

    public static int APP_REQUEST_CODE = 99;
    private static final String TAG = AccountKitManager.class.getSimpleName();
    private String currentCountryCode;
    private String currentPhone;

    private GetPhoneDetailCallback callback;
    public AccountKitManagerImpl() {
    }

    @Override
    public void initPhoneLogin(Activity activity,String countryCode,
                               String mobileNumber,GetPhoneDetailCallback callback) {
        this.callback = callback;
        final Intent intent = new Intent(activity, AccountKitActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        PhoneNumber phoneNumber = new PhoneNumber(countryCode,mobileNumber.replace(countryCode,""),"en");
        configurationBuilder.setInitialPhoneNumber(phoneNumber);
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        activity.startActivityForResult(intent, APP_REQUEST_CODE);
    }


    @Override
    public void initPhoneLogin(Activity activity,GetPhoneDetailCallback callback) {
        this.callback = callback;
        final Intent intent = new Intent(activity, AccountKitActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        activity.startActivityForResult(intent, APP_REQUEST_CODE);
    }

    @Override
    public void logout() {
        AccountKit.logOut();
    }

    @Override
    public void getPhoneDetail(GetPhoneDetailCallback callback) {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(Account account) {
                if(callback != null){
                    if(account != null) {
                        PhoneNumber phoneNumber = account.getPhoneNumber();
                        if(phoneNumber != null) {
                            currentCountryCode = '+' + phoneNumber.getCountryCode();
                            currentPhone = phoneNumber.getPhoneNumber();
                            callback.onSuccess(currentCountryCode, currentPhone);
                        }
                        else{
                            callback.onError("phoneNumber is null");
                        }
                    }
                    else{
                        callback.onError("account is null");
                    }
                }
            }

            @Override
            public void onError(AccountKitError accountKitError) {
                if(callback != null){
                    callback.onError(accountKitError.toString());
                }
            }

        });
    }

    @Override
    public boolean isLoggedIn() {
        return AccountKit.getCurrentAccessToken() != null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == APP_REQUEST_CODE) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();

                //showErrorActivity(loginResult.getError());

            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                if (loginResult.getAccessToken() != null) {
                    toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();
                    getPhoneDetail(callback);
                } else {
                    toastMessage = String.format(
                            "Success:%s...",
                            loginResult.getAuthorizationCode().substring(0,10));
                }
                //btnLogout.setVisibility(View.VISIBLE);

                // If you have an authorization code, retrieve it from
                // loginResult.getAuthorizationCode()
                // and pass it to your server and exchange it for an access token.

                // Success! Start your next activity...
                //goToMyLoggedInActivity();
            }

            // Surface the result to your user in an appropriate way.
            Log.d(TAG, "onActivityResult: toastMessage "+toastMessage);
//            Toast.makeText(
//                    this,
//                    toastMessage,
//                    Toast.LENGTH_LONG)
//                    .show();
        }
    }

    public interface GetPhoneDetailCallback{
        void onSuccess(String countryCode, String phone);
        void onError(String error);
    }

}
