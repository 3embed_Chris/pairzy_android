package com.pairzy.com.home.Prospects.RecentVisitors.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * @since 1/31/2018.
 * @author 3Embd.
 * @version 1.0.
 */
public class RecentResponseHolder
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<RecentUserItemPojo> data = null;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public ArrayList<RecentUserItemPojo> getData()
    {
        return data;
    }

    public void setData(ArrayList<RecentUserItemPojo> data)
    {
        this.data = data;
    }
}
