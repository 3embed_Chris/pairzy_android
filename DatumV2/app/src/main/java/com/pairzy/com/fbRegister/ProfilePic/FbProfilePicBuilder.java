package com.pairzy.com.fbRegister.ProfilePic;
import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * @since 1/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface FbProfilePicBuilder
{
    @FragmentScoped
    @Binds
    FbProfilePicFrg getEmailFragment(FbProfilePicFrg profilePicFrg);

    @FragmentScoped
    @Binds
    FbProfilePicContact.Presenter taskPresenter(FbProfilePicFrgPresenter presenter);
}
