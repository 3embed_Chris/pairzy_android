package com.pairzy.com.util.Exception;

/**
 * @since  4/13/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class SimilarDataException extends Exception
{
    public SimilarDataException() {
        super();
    }

    public SimilarDataException(String message)
    {
        super(message);
    }
}
