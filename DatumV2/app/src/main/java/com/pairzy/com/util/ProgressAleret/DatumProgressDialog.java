package com.pairzy.com.util.ProgressAleret;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
/**
 * @since  2/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class DatumProgressDialog
{
    private Activity mactivity;
    private Dialog progress_bar = null;
    private ProgressDialogCallback callback;
    private TypeFaceManager typeFaceManager;

    public DatumProgressDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        this.typeFaceManager=typeFaceManager;
        this.mactivity=activity;
        init(activity);
    }

    /*
     *inti dialog content*/
    private void init(Activity mActivity)
    {
        progress_bar = new Dialog(mActivity,android.R.style.Theme_Translucent);
        progress_bar.setCancelable(false);
        progress_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        @SuppressLint("InflateParams")
        View dialogView = LayoutInflater.from(mActivity).inflate(R.layout.datum_progress_dialog, null);
        TextView progress_title = dialogView.findViewById(R.id.progress_title);
        progress_title.setTypeface(typeFaceManager.getCircularAirBook());
        progress_bar.setContentView(dialogView);
        progress_bar.setCancelable(true);
        progress_bar.setOnCancelListener(dialog -> {
            if(callback!=null)
                callback.onCanceled();
        });
        RelativeLayout back_ground=dialogView.findViewById(R.id.progres_background_layout);
        back_ground.setOnClickListener(view -> {

        });
    }

    public void setCallback(ProgressDialogCallback callback)
    {
        this.callback=callback;
    }


    public void show()
    {
        if (progress_bar != null) {
            if (progress_bar.isShowing()) {
                progress_bar.dismiss();
            }
            mactivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progress_bar.show();
                }
            });
            if(callback!=null)
                callback.onShowing();
        }
    }

    public void cancel()
    {
        if (progress_bar.isShowing()) {
            progress_bar.dismiss();
        }
        if(callback!=null)
            callback.onCanceled();
    }

    public void isCancelable(boolean isCancel)
    {
        if (progress_bar != null) {
            progress_bar.setCancelable(isCancel);
        }
    }
}
