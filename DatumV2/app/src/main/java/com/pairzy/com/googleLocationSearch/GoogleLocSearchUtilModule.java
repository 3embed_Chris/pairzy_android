package com.pairzy.com.googleLocationSearch;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.googleLocationSearch.model.AddressListPojo;
import com.pairzy.com.googleLocationSearch.model.LocationAdapter;
import com.pairzy.com.locationScreen.model.LocationHolder;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.LocationProvider.Location_service;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * <h>GoogleLocSearchUtilModule class</h>
 * @author 3Embed.
 * @since 3/4/18.
 * @version 1.0.
 */

@Module
public class GoogleLocSearchUtilModule {

    @ActivityScoped
    @Provides
    App_permission provideAppPermission(Activity activity, TypeFaceManager typeFaceManager){
        return new App_permission(activity,typeFaceManager);
    }
    @Named("ADDRESS_LIST_POJO")
    @ActivityScoped
    @Provides
    ArrayList<AddressListPojo> provideListOfAddress(){
        return  new ArrayList<>();
    }

    @Named("PREF_LIST")
    @ActivityScoped
    @Provides
    ArrayList<String> providePreferanceList(){
        return  new ArrayList<>();
    }


    @ActivityScoped
    @Provides
    LocationAdapter provideLocationAdapter(@Named("ADDRESS_LIST_POJO") ArrayList<AddressListPojo> arrayList, TypeFaceManager typeFaceManager){
        return new LocationAdapter(arrayList,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    LinearLayoutManager provideLinearLayoutManager(Activity activity){
        return new LinearLayoutManager(activity);
    }

    @ActivityScoped
    @Provides
    Location_service getLocation_service(Activity activity)
    {
        return new Location_service(activity);
    }

    @ActivityScoped
    @Provides
    LocationHolder provideLocationHolder()
    {
        return new LocationHolder();
    }
}
