package com.pairzy.com.register.Userdob;

import android.app.Activity;
import android.app.DatePickerDialog;
import com.pairzy.com.R;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.inject.Inject;
/**
 * @since   2/16/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class DobFrgPresenter implements DobContract.Presenter
{
    private DobContract.View view;

    @Inject
    Activity activity;

    @Inject
    DobModel dobModel;

    @Inject
    DobFrgPresenter()
    {}


    @Override
    public void takeView(DobContract.View view) {
        this.view= view;
    }

    @Override
    public void dropView()
    {
        this.view=null;
    }

    @Override
    public void showMessage(String message)
    {}
    @Override
    public void onError(String message)
    {}

    @Override
    public void validateAge(String day, String month, String year)
    {
        if(dobModel.isAdult(day,month,year))
        {
            if(view!=null)
                view.moveNextFragment(dobModel.getDateInMilli(day,month,year));
        }else
        {
            if(view!=null)
                view.onError(view.getAdultErrorMessage());
        }
    }

    @Override
    public int[] getCurrentYear()
    {
        return dobModel.getCurrentYear();
    }


    @Override
    public void openDatePicker()
    {
        final Calendar c = Calendar.getInstance();
        final Calendar mincalender = Calendar.getInstance();
        mincalender.set(1900,1,1);
        mincalender.setTimeZone(TimeZone.getDefault());
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(activity,R.style.DatePicker,
                (view, year, monthOfYear, dayOfMonth) -> {
                    String days=dayOfMonth<10?"0"+dayOfMonth:""+dayOfMonth;
                    monthOfYear=monthOfYear+1;
                    String months=monthOfYear<10?"0"+monthOfYear:""+monthOfYear;
                    if(this.view!=null)
                    {
                        this.view.onDateSelected(days,months,""+year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.getDatePicker().setMinDate(mincalender.getTime().getTime());
        datePickerDialog.show();
    }

}
