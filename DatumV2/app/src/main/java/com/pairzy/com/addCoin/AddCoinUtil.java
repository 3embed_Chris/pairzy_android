package com.pairzy.com.addCoin;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.addCoin.AddCoinDialog.AddCoinDialog;
import com.pairzy.com.addCoin.model.CoinPlanListAdapter;
import com.pairzy.com.boostDetail.model.CoinPlan;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.progressbar.LoadingProgress;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;

/**
 * <h>AddCoinUtil</h>
 * <p>provides needed class instance.</p>
 * @author 3Embed
 * @since 4/6/18.
 */
@Module
public class AddCoinUtil {

    @ActivityScoped
    @Provides
    CoinPlanListAdapter providePlanListAdapter(ArrayList<CoinPlan> coinPlanList, TypeFaceManager typeFaceManager){
        return new CoinPlanListAdapter(coinPlanList,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    LinearLayoutManager provideLinearLayoutManager(Activity activity){
        return new LinearLayoutManager(activity);
    }

    @ActivityScoped
    @Provides
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }

    @ActivityScoped
    @Provides
    AddCoinDialog provideAddCoinDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility){
        return new AddCoinDialog(activity,typeFaceManager,utility);
    }

    @ActivityScoped
    @Provides
    CoinDialog provideCoinDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility){
        return new CoinDialog(activity,typeFaceManager,utility);
    }
}
