package com.pairzy.com.MqttManager;

import io.reactivex.Observable;
import io.reactivex.Observer;

/**
 * @since  3/9/2018.
 * @author 3Embed.
 * @version 1.0.
 */

public class MqttConnectionObserver extends Observable<MqttConStatus >
{
    private Observer<?super MqttConStatus > observer;

    @Override
    protected void subscribeActual(Observer<? super MqttConStatus > observer)
    {
        this.observer=observer;
    }

    public void publishData(MqttConStatus  data)
    {
        if(observer!=null)
        {
            observer.onNext(data);
            observer.onComplete();
        }
    }

}

