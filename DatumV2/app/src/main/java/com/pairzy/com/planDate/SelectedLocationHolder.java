package com.pairzy.com.planDate;

import java.io.Serializable;

/**
 * <h>SelectedLocationHolder class</h>
 * @author 3Embed.
 * @since 7/5/18.
 * @version 1.0.
 */

public class SelectedLocationHolder implements Serializable
{
    private Double latitude = 0.0;
    private Double longitude = 0.0;
    private String locationTitle;
    private String locationDetail;

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setLocationTitle(String locationTitle) {
        this.locationTitle = locationTitle;
    }

    public void setLocationDetail(String locationDetail) {
        this.locationDetail = locationDetail;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getLocationTitle() {
        return locationTitle;
    }

    public String getLocationDetail() {
        return locationDetail;
    }
}
