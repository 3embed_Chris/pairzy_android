package com.pairzy.com.MqttChat.DocumentPicker.Adapters;

/**
 * Created by moda on 22/08/17.
 */

public interface FileAdapterListener{
    void onItemSelected();
}