package com.pairzy.com.boostLikes;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.addCoin.AddCoinActivity;
import com.pairzy.com.boostLikes.Model.BoostLikeAdapter;
import com.pairzy.com.boostLikes.Model.BoostLikeAdapterCallback;
import com.pairzy.com.boostLikes.Model.BoostLikeData;
import com.pairzy.com.userProfile.UserProfilePage;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by ankit on 7/9/18.
 */

public class BoostLikeActivity extends BaseDaggerActivity implements BoostLikeContract.View ,SwipeRefreshLayout.OnRefreshListener{

    private Unbinder unbinder;
    @Inject
    BoostLikeContract.Presenter presenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    GridLayoutManager gridLayoutManager;
    @Inject
    Activity activity;
    @Inject
    BoostLikeAdapter adapter;
    @Inject
    ArrayList<BoostLikeData> boostLikeData;


    @BindView(R.id.likesme_list)
    RecyclerView item_list;
    @BindView(R.id.likesme_loading_view)
    RelativeLayout loading_view;
    @BindView(R.id.likesme_progress)
    ProgressBar online_progress;
    @BindView(R.id.likesme_error_icon)
    ImageView online_error_icon;
    @BindView(R.id.likesme_loading_text)
    TextView online_loading_text;
    @BindView(R.id.likesme_error_msg)
    TextView online_error_msg;
    @BindView(R.id.likesme_parent)
    SwipeRefreshLayout online_parent;
    @BindView(R.id.empty_data)
    RelativeLayout empty_data;
    @BindView(R.id.no_more_dara)
    TextView no_more_dara;
    @BindView(R.id.empty_details)
    TextView empty_details;
    @BindView(R.id.btn_retry)
    Button btnRetry;
    @BindView(R.id.tv_page_title)
    TextView tvPageTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boost_like);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        initView();
        presenter.initAdapterListener();
        presenter.getLikeList();
    }


    private void initView() {
        tvPageTitle.setTypeface(typeFaceManager.getCircularAirBold());
        btnRetry.setTypeface(typeFaceManager.getCircularAirBold());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            online_progress.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(activity,R.color.colorAccent)));
        }else
        {
            Drawable progressDrawable = online_progress.getProgressDrawable().mutate();
            progressDrawable.setColorFilter(ContextCompat.getColor(activity,R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            online_progress.setProgressDrawable(progressDrawable);
        }
        online_parent.setColorSchemeResources(R.color.refresh2, R.color.refresh1,R.color.refresh);
        online_parent.setOnRefreshListener(this);
        empty_details.setTypeface(typeFaceManager.getCircularAirBook());
        no_more_dara.setTypeface(typeFaceManager.getCircularAirBold());
        item_list.setHasFixedSize(true);
        //item_list.setNestedScrollingEnabled(false);
        item_list.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {
                return false;
            }
        });
        item_list.setLayoutManager(gridLayoutManager);
        item_list.setAdapter(adapter);
//        item_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
//            {
//                super.onScrolled(recyclerView, dx, dy);
//                int positionView=linearLayoutManager.findLastVisibleItemPosition();
//                if(presenter.checkLoadMore(positionView))
//                {
//                    presenter.getListData(true);
//                }
//                presenter.pee_fetchProfile(positionView);
//            }
//        });
    }

    @OnClick(R.id.btn_retry)
    public void onRetry(){
        onRefresh();
    }

    @OnClick(R.id.close_button)
    public void onClose(){
        finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    @Override
    public void onRefresh() {
        boostLikeData.clear();
        online_parent.setRefreshing(false);
        presenter.getLikeList();
    }

    @Override
    public void showError(String message)
    {
        Snackbar snackbar = Snackbar
                .make(online_parent,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(online_parent,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void launchCoinWallet() {
        Intent intent = new Intent(activity,AddCoinActivity.class);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @Override
    public void onDestroy()
    {
        presenter.dropView();
        unbinder.unbind();
        super.onDestroy();
    }

    @Override
    public void onDataUpdate()
    {
        adapter.notifyDataSetChanged();
        loading_view.setVisibility(View.GONE);
        empty_data.setVisibility(View.GONE);
        item_list.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress()
    {
        btnRetry.setVisibility(View.GONE);
        online_error_msg.setVisibility(View.GONE);
        online_error_icon.setVisibility(View.GONE);
        online_loading_text.setVisibility(View.VISIBLE);
        online_progress.setVisibility(View.VISIBLE);
        loading_view.setVisibility(View.VISIBLE);
        //item_list.setVisibility(View.GONE);
        empty_data.setVisibility(View.GONE);
    }

    @Override
    public void onApiError(String message)
    {
        btnRetry.setVisibility(View.VISIBLE);
        online_error_msg.setVisibility(View.VISIBLE);
        online_error_msg.setText(message);
        online_error_icon.setVisibility(View.VISIBLE);
        online_loading_text.setVisibility(View.GONE);
        online_progress.setVisibility(View.GONE);
        //item_list.setVisibility(View.GONE);
        empty_data.setVisibility(View.GONE);
        loading_view.setVisibility(View.VISIBLE);
    }

    @Override
    public void emptyData()
    {
        //item_list.setVisibility(View.GONE);
        loading_view.setVisibility(View.GONE);
        empty_data.setVisibility(View.VISIBLE);
    }

    @Override
    public void openUserProfile(String data)
    {
        Intent intent=new Intent(activity,UserProfilePage.class);
        Bundle intent_data=new Bundle();
        intent_data.putString(UserProfilePage.USER_DATA,data);
        intent.putExtras(intent_data);
        activity.startActivityForResult(intent, AppConfig.PROFILE_REQUEST);
    }


    @Override
    public void adapterListener(BoostLikeAdapterCallback adapterCallabck)
    {
        adapter.setAdapterCallabck(adapterCallabck);
    }

}
