package com.pairzy.com.PostMoments;

import android.app.Activity;
import android.util.Log;
import android.widget.AutoCompleteTextView;

import com.pairzy.com.AppController;
import com.pairzy.com.PostMoments.model.PostModel;
import com.pairzy.com.Utilities.Constants;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.util.CloudManager.UploadManager;
import com.pairzy.com.util.CloudManager.UploaderCallback;
import com.pairzy.com.util.progressbar.LoadingProgress;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by ankit on 23/2/18.
 */

public class PostPresenter implements PostContract.Presenter {

    private static final String TAG = PostPresenter.class.getSimpleName();

    private String path,caption;
    private String type,url;
    private CompositeDisposable compositeDisposable;

    @Inject
    PostContract.View view;
    @Inject
    UploadManager uploadManager;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    NetworkService networkService;
    @Inject
    PostModel model;
    @Inject
    Activity activity;
    @Inject
    LoadingProgress loadingProgress;


    @Inject
    PostPresenter() {
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void init(String path, String type) {
        this.path = path;
        this.type = type;

    }

    @Override
    public void sendPost()
    {
        networkService.sendPost(dataSource.getToken(),model.getLanguage(),model.sendBody(url,type,caption))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> value)
                    {
                        try
                        {
                            loadingProgress.cancel();
                            if(value.code() == 200)
                            {
                                String data=value.body().string();
                                if(view != null)
                                    view.updatedSuccessfully();
                            }
                        } catch (Exception e)
                        {
                            /*if(view!=null)
                                view.showError(activity.getString(R.string.failed_to_get_pref_error));*/
                        }
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        loadingProgress.cancel();
                        if(view != null)
                            view.showMessage("upload failed from cloudinary");

                    }
                    @Override
                    public void onComplete() {}
                });

    }

    @Override
    public void validate(AutoCompleteTextView etPostTitle) {
        caption=etPostTitle.getText().toString();
        Log.e(TAG, "sendPost: "+path+"   "+type+ "   "+caption );
        uploadToCloudinary(path,true);
    }

    @Override
    public void getCloudinaryDetails(String filePath) {
        networkService.getCloudinaryDetail()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(Response<ResponseBody> responseBodyResponse) {
                        loadingProgress.cancel();
                        if(responseBodyResponse.code() == 200){
                            try {
                                model.parseClodinaryDetail(responseBodyResponse.body().string());
                                uploadToCloudinary(filePath,false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else if(responseBodyResponse.code() == 401){
                            AppController.getInstance().appLogout();
                        }else{
                            if(view != null)
                                view.showError(model.getError(responseBodyResponse));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        loadingProgress.cancel();
                    }
                });
    }

    public void uploadToCloudinary(String filePath, boolean firstTimeUpload )
    {
        loadingProgress.show();
        if(type.equalsIgnoreCase(Constants.Post.IMAGE)) {
            uploadManager.uploadFile(filePath, new UploaderCallback() {

                @Override
                public void onSuccess(String main_url, String thumb_nail, int height, int width) {
                    Log.e(TAG, "onSuccess: " + main_url + "       " + thumb_nail);
                    url = main_url;
                    sendPost();
                }

                @Override
                public void onError(String error) {

                    if(error.equals(UploadManager.UN_CAUGHT_ERROR) && firstTimeUpload){
                        getCloudinaryDetails(filePath);
                    }else {
                        loadingProgress.cancel();
                        view.showMessage("upload failed from cloudinary");
                        Log.e(TAG, "onError: " + "cloudinary     image");
                    }
                }
            });
        }else {
            uploadManager.uploadVideoFile(filePath, new UploaderCallback() {
                @Override
                public void onSuccess(String main_url, String thumb_nail, int height, int width) {
                    Log.e(TAG, "onSuccess: " + main_url + "       " + thumb_nail);
                    url = main_url;
                    sendPost();
                }

                @Override
                public void onError(String error) {
                    if(error.equals(UploadManager.UN_CAUGHT_ERROR) && firstTimeUpload){
                        getCloudinaryDetails(filePath);
                    }else {
                        loadingProgress.cancel();
                        view.showMessage("upload failed from cloudinary");
                        Log.e(TAG, "onError: " + "cloudinary     Video");
                    }
                }
            });
        }

    }


    @Override
    public void takeView(Object view) {
        //already injected
    }

    @Override
    public void dropView() {
        this.view = null;
        compositeDisposable.clear();
    }

}
