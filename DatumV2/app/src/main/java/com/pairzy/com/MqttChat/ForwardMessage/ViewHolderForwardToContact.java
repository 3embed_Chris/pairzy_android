package com.pairzy.com.MqttChat.ForwardMessage;

import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pairzy.com.R;

import com.pairzy.com.util.TypeFaceManager;

/**
 * Created by moda on 30/08/17.
 */

public class ViewHolderForwardToContact extends RecyclerView.ViewHolder {


    public TextView contactName, contactStatus;

    public ImageView contactImage, contactSelected;


    public ViewHolderForwardToContact(View view, TypeFaceManager typeFaceManager) {
        super(view);

        contactStatus = (TextView) view.findViewById(R.id.contactStatus);

        contactName = (TextView) view.findViewById(R.id.contactName);

        contactImage = (ImageView) view.findViewById(R.id.storeImage2);
        contactSelected = (ImageView) view.findViewById(R.id.contactSelected);
        //Typeface tf = AppController.getInstance().getRobotoCondensedFont();
        contactName.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.NORMAL);
        contactStatus.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.NORMAL);
    }
}
