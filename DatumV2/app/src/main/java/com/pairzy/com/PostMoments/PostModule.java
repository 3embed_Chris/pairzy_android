package com.pairzy.com.PostMoments;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 21/2/18.
 */

//@ActivityScoped
@Module
public interface PostModule {

    @ActivityScoped
    @Binds
    PostContract.Presenter presenter(PostPresenter presenter);

    @ActivityScoped
    @Binds
    PostContract.View view(PostActivity activity);

    @ActivityScoped
    @Binds
    Activity provideActivity(PostActivity activity);

}
