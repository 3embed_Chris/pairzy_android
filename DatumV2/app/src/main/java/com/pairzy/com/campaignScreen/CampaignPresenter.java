package com.pairzy.com.campaignScreen;

import javax.inject.Inject;

/**
 * <h>CampaignPresenter</h>
 * <p> Contain non view related method for CampaignActivity.</p>
 * @author 3Embed.
 * @since 28/6/18.
 */
public class CampaignPresenter implements CampaignContract.Presenter {

    @Inject
    public CampaignPresenter() {
    }

}
