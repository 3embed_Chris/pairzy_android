package com.pairzy.com.editProfile;

import android.content.Intent;
import android.net.Uri;
import android.widget.ImageView;

/**
 * <h>EditPrefContract interface</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public interface EditProfileContract {

    interface View{
        void applyFont();
        void makeVideoThumbSquare();
        void recyclerViewSetup();
        void showAddVideoButton(boolean show);
        void openCamera(Uri uri, int mediaType);
        void openGallery(int mediaType);
        void showError(String errorMsg);
        void showMessage(String errorMsg);
        void showMessage(int errorId);
        void showVideoLoading(boolean show);
        void showVideoThumb(String videoUrl);
        void showTickMark(boolean show);
        void launchPreviewScreen(Intent intent, android.view.View view);
    }

    interface Presenter{
        void init();
        void loadUserVideo();
        void openChooser();
        void upDateToGallery();
        String getRecentTempVideo();
        void compressedVideo(String file_path, ImageView imageView);
        void compressedMedia(String file_path, ImageView imageView);
        void loadUserPicture();
        void launchVideoChooser();
        void compressImage(String filePath);
        void uploadVideo(String filePath,boolean firstTimeUpload);
        void uploadToAws(String filePath,boolean firstTimeUpload);
        boolean isProfileChanged();
        void removeVideo();
        void saveEditedData();
        void loadVideoPreview(android.view.View v);
        boolean isValidMediaSize();
        boolean isValidMediaSize(String filePath);
        void getCloudinaryDetails(String filePath,boolean isForVideoUpload);
    }
}