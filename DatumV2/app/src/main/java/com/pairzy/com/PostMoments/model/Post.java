package com.pairzy.com.PostMoments.model;

import java.util.ArrayList;

/**
 * Created by DELL on 3/26/2018.
 */

public class Post {
    private String id;
    private String title;
    private String pathForCloudinary;
    private String typeForCloudinary;
    private String imageUrl1;
    private String thumbnailUrl1;
    private Integer mediaType1;
    private String cloudinaryPublicId1;
    private String imageUrl1Width;
    private String imageUrl1Height;
    private ArrayList<String> files;
    private String audioFile;
    private String filterColor;
    private String duration;
    private String caption;
    private boolean gallery;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPathForCloudinary() {
        return pathForCloudinary;
    }

    public void setPathForCloudinary(String pathForCloudinary) {
        this.pathForCloudinary = pathForCloudinary;
    }

    public String getTypeForCloudinary() {
        return typeForCloudinary;
    }

    public void setTypeForCloudinary(String typeForCloudinary) {
        this.typeForCloudinary = typeForCloudinary;
    }

    public String getImageUrl1() {
        return imageUrl1;
    }

    public void setImageUrl1(String imageUrl1) {
        this.imageUrl1 = imageUrl1;
    }

    public String getThumbnailUrl1() {
        return thumbnailUrl1;
    }

    public void setThumbnailUrl1(String thumbnailUrl1) {
        this.thumbnailUrl1 = thumbnailUrl1;
    }


    public Integer getMediaType1() {
        return mediaType1;
    }

    public void setMediaType1(Integer mediaType1) {
        this.mediaType1 = mediaType1;
    }

    public String getCloudinaryPublicId1() {
        return cloudinaryPublicId1;
    }

    public void setCloudinaryPublicId1(String cloudinaryPublicId1) {
        this.cloudinaryPublicId1 = cloudinaryPublicId1;
    }

    public String getImageUrl1Width() {
        return imageUrl1Width;
    }

    public void setImageUrl1Width(String imageUrl1Width) {
        this.imageUrl1Width = imageUrl1Width;
    }

    public String getImageUrl1Height() {
        return imageUrl1Height;
    }

    public void setImageUrl1Height(String imageUrl1Height) {
        this.imageUrl1Height = imageUrl1Height;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public ArrayList<String> getFiles() {
        return files;
    }

    public void setFiles(ArrayList<String> files) {
        this.files = files;
    }

    public String getAudioFile() {
        return audioFile;
    }

    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }

    public String getFilterColor() {
        return filterColor;
    }

    public void setFilterColor(String filterColor) {
        this.filterColor = filterColor;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public boolean isGallery() {
        return gallery;
    }

    public void setGallery(boolean gallery) {
        this.gallery = gallery;
    }
}
