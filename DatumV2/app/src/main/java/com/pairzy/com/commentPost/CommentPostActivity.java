package com.pairzy.com.commentPost;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.facebook.drawee.view.SimpleDraweeView;
import com.pairzy.com.R;
import com.pairzy.com.commentPost.model.CommentPostAdapter;
import com.pairzy.com.commentPost.model.CommentPostDataPojo;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.ApiConfig;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatActivity;

public class CommentPostActivity extends DaggerAppCompatActivity implements CommentPostContract.View, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = CommentPostActivity.class.getSimpleName();
    @BindView(R.id.tvCommentTitle)
    AppCompatTextView title;

    @BindView(R.id.rvComment)
    RecyclerView recyclerView;

    @BindView(R.id.CommentProfile)
    SimpleDraweeView CommentProfile;

    @Inject
    PreferenceTaskDataSource dataSource;

    @BindView(R.id.tvComentPost)
    AppCompatTextView postComment;

    @BindView(R.id.etComentPost)
    AppCompatEditText writeComment;

    @BindView(R.id.commentRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Named(CommentPostUtil.COMMENT_LAYOUT_MANAGER)
    @Inject
    LinearLayoutManager layoutManager;

    @Inject
    CommentPostContract.Presenter presenter;

    @Inject
    CommentPostAdapter commentPostAdapter;

    @Named(CommentPostUtil.COMMENT_POST)
    @Inject
    ArrayList<CommentPostDataPojo> list;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_post);
        unbinder = ButterKnife.bind(this);
        initView();

    }

    private void initView() {
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh2, R.color.refresh1,R.color.refresh);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        presenter.getUserComment();
        recyclerView.setAdapter(commentPostAdapter);
        swipeRefreshLayout.setOnRefreshListener(this);
        CommentProfile.setImageURI(dataSource.getProfilePicture());
        presenter.setAdapterCallBack(commentPostAdapter);
    }
    @Override
    public void onRefresh() {
        presenter.getUserComment();
        swipeRefreshLayout.setRefreshing(false);
    }


    @OnClick(R.id.fl_back_button)
    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();

        Log.e(TAG, "onBackPressed: "+list.size() );

        String count= String.valueOf(list.size());
        returnIntent.putExtra("commentData",count);
        setResult(RESULT_OK,returnIntent);
        finish();
    }

    @OnClick(R.id.tvComentPost)
    void next()
    {
        String postComment= writeComment.getText().toString();
        String postId= getIntent().getStringExtra(ApiConfig.COMMENT.POST_ID);
        presenter.validateComment(postComment,postId);
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void CallGetUserComment() {
        presenter.getUserComment();
        writeComment.setText("");
        hideKeyboard(this);
    }

    @Override
    public void notifyDataAdapter(int position) {
        commentPostAdapter.notifyItemChanged(position);
    }

    @Override
    public void editComment(String comment) {
        writeComment.setText(comment);

    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
