package com.pairzy.com.fbRegister.Userdob;

import java.util.Calendar;

import javax.inject.Inject;

/**
 * @since  2/16/2018.
 * @version 1.0.
 */
public class FbDobModel
{
    @Inject
    FbDobModel(){}

    public boolean isAdult(String day, String month, String year)
    {
        double adult_age=568025136000.00;
        try
        {
            int day_data=Integer.parseInt(day);
            int month_data=Integer.parseInt(month);
            if(month_data >= 1 && month_data <= 12)
                month_data -= 1;
            int year_data=Integer.parseInt(year);
            Calendar given_Date = Calendar.getInstance();
            given_Date.set(year_data,month_data,day_data);
            long entered_time=given_Date.getTime().getTime();
            long current_time=System.currentTimeMillis();
            long remaining=current_time-entered_time;
            if(remaining>=adult_age)
            {
              return true;
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
       return false;
    }

    public double getDateInMilli(String day,String month,String year)
    {
        int day_data=Integer.parseInt(day);
        int month_data=Integer.parseInt(month);
        if(month_data >= 1 && month_data <= 12)
            month_data -= 1;
        int year_data=Integer.parseInt(year);
        Calendar given_Date = Calendar.getInstance();
        given_Date.set(year_data,month_data,day_data);
        return given_Date.getTime().getTime();
    }

    /*
     *Getting the current year */
    public int[] getCurrentYear()
    {
        int currentYear=Calendar.getInstance().get(Calendar.YEAR);
        String year=String.valueOf(currentYear);
        int arrayInt[]=new int[year.length()];
        for(int i=0;i<year.length();i++)
        {
            arrayInt[i]=Character.getNumericValue(year.charAt(i));
        }
        return arrayInt;
    }
}
