package com.pairzy.com.home.Matches.PostFeed;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.pairzy.com.AppController;
import com.pairzy.com.MqttChat.Utilities.Utilities;
import com.pairzy.com.MqttManager.MessageType;
import com.pairzy.com.MqttManager.Model.MqttMessage;
import com.pairzy.com.MqttManager.MqttRxObserver;
import com.pairzy.com.R;
import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.CustomAlertDialog.CustomAlertDialog;
import com.pairzy.com.home.Matches.PostFeed.Model.PostFeedIemCallBack;
import com.pairzy.com.home.Matches.PostFeed.Model.PostFeedModel;
import com.pairzy.com.home.Matches.PostFeed.Model.PostListAdapter;
import com.pairzy.com.home.Matches.PostFeed.Model.PostListPojo;
import com.pairzy.com.home.Matches.PostFeed.deletePostDialog.DeletePostDialog;
import com.pairzy.com.home.Matches.PostFeed.deletePostDialog.PostDeleteCallback;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.CalendarEventHelper;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.progressbar.LoadingProgress;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h2>PostFeedFragPresenter</h2>
 * <P>
 *
 * </P>
 * @since  3/17/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class PostFeedFragPresenter implements PostFeedFragContract.Presenter, App_permission.Permission_Callback, PostFeedIemCallBack, PostDeleteCallback
{

    private final String CALENDER_TAG = "calender_tag";
    private static final String TAG = PostFeedFragPresenter.class.getSimpleName();

    @Inject
    PostFeedModel model;

    @Inject
    NetworkService service;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    Utility utility;
    @Inject
    Activity activity;
    @Inject
    App_permission app_permission;
    @Inject
    CalendarEventHelper calendarEventHelper;
    @Inject
    CustomAlertDialog customAlertDialog;
    @Inject
    LoadingProgress loadingProgress;
    @Inject
    DeletePostDialog deletePostDialog;
    @Inject
    MqttRxObserver mqttRxObserver;

    private CompositeDisposable compositeDisposable;
    private PostFeedFragContract.View view;
    private PostFeedFrag newsFeedFrg;
    private int NEWS_FEED_POST = 0;
    private int LIMIT = 10;
    private int position;
    @Inject
    PostFeedFragPresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(Object view)
    {
        this.view= (PostFeedFragContract.View) view;
        this.newsFeedFrg = (PostFeedFrag)view;
    }

    @Override
    public void dropView()
    {
        view=null;
        compositeDisposable.clear();
    }


    public void getNewsFeedList(boolean isRefresh) {
        if(networkStateHolder.isConnected()) {
            //if (isRefresh) {
            //    NEWS_FEED_POST = 0;
            if(view != null){
                view.showLoading();
            }
//            } else {
//                NEWS_FEED_POST += LIMIT;
//            }
            service.requestPost(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                try {
                                    model.parseNewsFeedResponse(value.body().string());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else if(value.code() == 412){
                                //empty list
                            }
                            else {
                                if(view != null)
                                    view.showError(model.getError(value));
                            }
                            invalidateLayout();
                        }

                        @Override
                        public void onError(Throwable e) {

                            if(view != null)
                                view.showError(activity.getString(R.string.failed_to_get_post_list));

                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
        else{
            if(view != null)
                view.showNetworkError(activity.getString(R.string.no_internet_error));
        }
    }

    private void invalidateLayout() {
        if(model.checkPostExist()){
            if(view != null)
                view.showData();
        }else{
            if(view != null)
                view.emptyData();
        }
    }

    private void checkForCalenderPermission() {
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.READ_CALENDER);
        permissions.add(App_permission.Permission.WRITE_CALENDER);
        app_permission.getPermission_for_Sup_v4Fragment(CALENDER_TAG,permissions, newsFeedFrg,this);
    }


    @Override
    public void setAdapterCallBack(PostListAdapter postListAdapter) {
        postListAdapter.setItemCallback(this);
    }

    @Override
    public boolean checkDateExist() {
        return model.checkPostExist();
    }

    @Override
    public void parseDateData(int requestCode, int resultCode, Intent data) {
//        if(requestCode == ChatFrag.RESCHEDULE_REQ_CODE){
//            if(resultCode == -1){
//                DateListPojo dateData = (DateListPojo) data.getSerializableExtra("date_data");
//                if(dateData != null)
//                    callDateResponseApi(dateData, DateResponseType.RESCHEDULE);
//            }
//        }

        if(requestCode == AppConfig.COMMENT_SCREEN_REQ_CODE){

            if (resultCode == Activity.RESULT_OK) {
                String commentCount = data.getStringExtra("commentData");
                Log.e(TAG, "parseDateData: "+commentCount );
                PostListPojo postListPojo = model.getPostItem(position);
                if(postListPojo != null) {
                    postListPojo.setCommentCount(Integer.valueOf(commentCount));
                    if (view != null)
                        view.notifyDataAdapter(position);
                }
            }

        }
    }

    @Override
    public void checkAndLoadPost() {
        getNewsFeedList(true);
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
    }


    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        app_permission.show_Alert_Permission(activity.getString(R.string.calender_access_text), activity.getString(R.string.calender_access_subtitle),
                activity.getString(R.string.location_acess_message),stringArray);
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean parmanent) {
    }

    @Override
    public void likeDislikeUserPost(double type, String postId)
    {
//        if(loadingProgress!=null)
//            loadingProgress.show();
        service.postUserPostLike(dataSource.getToken(),model.getLanguage(),model.getLikeUnlikeData(type,postId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> value)
                    {
                        try {
                            if(value.code() == 200) {
                                if(type==2)
                                    Log.e(TAG, "onNext: successfully unliked" );
                                else
                                    Log.e(TAG, "onNext:successfully liked" );
                                //loadingProgress.cancel();
                            }


                        } catch (Exception e) {
                            //loadingProgress.cancel();
                            /*if(view!=null)
                                view.showError(activity.getString(R.string.failed_to_get_pref_error));*/
                        }
                    }
                    @Override
                    public void onError(Throwable e) {
                    }
                    @Override
                    public void onComplete() {}
                });
    }

    @Override
    public void launchChat(PostListPojo postListPojo) {
        Intent intent = new Intent(activity, ChatMessageActivity.class);
        intent.putExtra("chatId","");
        intent.putExtra("receiverUid",postListPojo.getUserId());
        intent.putExtra("receiverName",     postListPojo.getUserName());
        String docId = AppController.getInstance().findDocumentIdOfReceiver(postListPojo.getUserId(), "");
        boolean initiated = false;
        boolean hasDefaultMessage = true;
        boolean isMatched = false;
        if (docId.isEmpty()) {
            docId = (String) AppController.findDocumentIdOfReceiver(postListPojo.getUserId(), Utilities.tsInGmt(),postListPojo.getUserName(),
                    postListPojo.getProfilePic(), "", false,postListPojo.getUserId(), String.valueOf(Utilities.getGmtEpoch()) , false);
            initiated = true;
        }
        else{
            intent.putExtra("chatId",AppController.getInstance().getDbController().getChatId(docId));
            Map<String, Object> chatInfo = AppController.getInstance().getDbController().getChatInfo(docId);
            if(chatInfo.containsKey("initiated"))
                initiated = (boolean) chatInfo.get("initiated");
            hasDefaultMessage = (boolean)chatInfo.get("hasDefaultMessage");
            isMatched = (boolean) chatInfo.get("isMatched");
        }
        intent.putExtra("documentId", docId);
        intent.putExtra("receiverIdentifier",postListPojo.getUserId());
        intent.putExtra("receiverImage",  postListPojo.getProfilePic());
        intent.putExtra("colorCode", AppController.getInstance().getColorCode(0 % 19));
        if (view != null) {
            AppController.getInstance().getDbController().updateChatUserData(
                    docId,
                    postListPojo.getUserName(),
                    postListPojo.getProfilePic(),
                    isMatched,
                    false,
                    initiated,
                    hasDefaultMessage
            );
            //AppController.getInstance().getDbController().updateChatUserOnline(docId, true);
            view.launchChatScreen(intent);
        }
    }


    @Override
    public void onLikePost(int position) {
        PostListPojo postListPojo = model.getPostItem(position);
        if(postListPojo != null){
            postListPojo.setLiked(true);
            int count=Integer.valueOf(postListPojo.getLikeCount())+1;
            postListPojo.setLikeCount(count+"");
            if(view != null)
                view.notifyDataAdapter(position);
            likeDislikeUserPost( 1,postListPojo.getPostId());
        }
    }

    @Override
    public void onUnlikePost(int position) {
        PostListPojo postListPojo = model.getPostItem(position);
        if(postListPojo != null){
            postListPojo.setLiked(false);

            int count=Integer.valueOf(postListPojo.getLikeCount());
            if(count > 0){
                count -=1;
            }
            postListPojo.setLikeCount(count+"");
            if(view != null)
                view.notifyDataAdapter(position);
            likeDislikeUserPost( 2,postListPojo.getPostId());
        }
    }

    @Override
    public void onComment(int position) {
        Log.e(TAG, "onComment: " );
        PostListPojo postListPojo = model.getPostItem(position);
        if(postListPojo != null) {
            if (view != null)
                view.launchWriteCommit(postListPojo.getPostId());
        }
    }

    @Override
    public void onViewAllComment(int position) {
        Log.e(TAG, "onViewAllComment: " );
        PostListPojo postListPojo = model.getPostItem(position);
        if(postListPojo != null) {
            if(view != null)
                view.launchCommentView(postListPojo.getPostId());
        }
    }

    @Override
    public void onViewAllLikes(int position) {
        Log.e(TAG, "onViewAllLikes: " );
        PostListPojo postListPojo = model.getPostItem(position);
        if(postListPojo != null) {
            if(view != null)
                view.launchLikeView(postListPojo.getPostId());
        }
    }

    @Override
    public void onChatMessage(int position) {
        Log.e(TAG, "OnchatMessage: " );
        PostListPojo postListPojo = model.getPostItem(position);
        if(postListPojo != null ) {
            launchChat(postListPojo);
        }
    }

    @Override
    public void onPostDelete(int position) {
        Log.e(TAG, "onPostDelete: " );
        PostListPojo postListPojo = model.getPostItem(position);

        if(postListPojo != null ) {
            deletePostDialog.showAlert(postListPojo.getPostId(),position,this);
        }
    }

    @Override
    public void deletePost(String postId) {
        loadingProgress.show();
        service.deletePost(dataSource.getToken(),model.getLanguage(),model.getDeletePostBody(postId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        loadingProgress.cancel();
                        if(responseBodyResponse.code() == 200){

                        }else if(responseBodyResponse.code() == 401){
                            AppController.getInstance().appLogout();
                        }else{
                            if(view != null)
                                view.showError(model.getError(responseBodyResponse));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        loadingProgress.cancel();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void onPostDelete(String postId, int position) {
        if(networkStateHolder.isConnected()) {
            if (model.removePostList(postId,position)) {
                if (view != null)
                    view.notifyDataAdapter();
            }
            deletePost(postId);
        }else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void observeMatchAndUnmatchUser() {
        mqttRxObserver.getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MqttMessage>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(MqttMessage value) {
                        if(value != null && value.getData() != null){
                            try {
                                if (value.getData().has("messageType") && value.getData().getString("messageType").equals(MessageType.MATCH)) {
                                    checkAndLoadPost();
                                }
                                else if ( value.getData().has("messageType") && value.getData().getString("messageType").equals(MessageType.UN_MATCH)){
                                    checkAndLoadPost();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

}
