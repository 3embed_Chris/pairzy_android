package com.pairzy.com.boostDetail;

import android.content.Intent;

/**
 * <h>PassportContract interface</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public interface BoostDetailContract {

    interface View{
        void applyFont();

        void showError(String error);
        void showMessage(String error);
        void showMessage(int errorId);
        void showLoading();
        void showData();
        void showEmptyData();
        void showNetworkError(String errorMsg);
        void enableSubsButton(boolean enable);
        void launchAddCoinScreen(Intent intent);
        void launchCoinWallet();
    }

    interface Presenter{
        void init();
        void launchBoostDialog();
        void getCoinsPlan();
        void getSubsPlan();
        void loadCoinPlanIfRequired();
        void loadSubsPlanIfRequired();
    }
}