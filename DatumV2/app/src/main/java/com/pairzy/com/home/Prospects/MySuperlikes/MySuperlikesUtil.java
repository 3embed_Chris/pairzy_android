package com.pairzy.com.home.Prospects.MySuperlikes;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.home.Prospects.MySuperlikes.Model.MySuperlikesAdapter;
import com.pairzy.com.home.Prospects.MySuperlikes.Model.MySuperlikesItemPojo;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import dagger.Module;
import dagger.Provides;
/**
 * @since   3/23/2018.
 * @author 3Embed.
 * @version 1.0
 */
@Module
public class MySuperlikesUtil
{
    @Provides
    @FragmentScoped
    ArrayList<MySuperlikesItemPojo> getList()
    {
        return new ArrayList<>();
    }
    @Provides
    @FragmentScoped
    MySuperlikesAdapter getUserListAdapter(ArrayList<MySuperlikesItemPojo> list, TypeFaceManager typeFaceManager, Utility utility)
    {
        return new MySuperlikesAdapter(list,typeFaceManager,utility);
    }


    @Provides
    @FragmentScoped
    LinearLayoutManager getLinearLayoutManager(Activity activity)
    {
        return new LinearLayoutManager(activity);
    }
}
