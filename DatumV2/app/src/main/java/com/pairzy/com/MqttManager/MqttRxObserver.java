package com.pairzy.com.MqttManager;

import com.pairzy.com.MqttManager.Model.MqttMessage;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;

/**
 * <h2>MqttRxObserver</h2>
 * <P>
 *
 * </P>
 * @since  3/12/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MqttRxObserver
{
    private ConnectableObservable<MqttMessage> connectableObservable;
    private ObservableEmitter<MqttMessage> emitter;

    public MqttRxObserver() {
        Observable<MqttMessage> observable = Observable.create(e -> emitter = e);
        connectableObservable = observable.publish();
        connectableObservable.share();
        connectableObservable.replay();
        connectableObservable.connect();
    }

    public ConnectableObservable<MqttMessage> getObservable() {
        return connectableObservable;
    }

    /*
        * Publishing the data.*/
    public void publish(MqttMessage data)
    {
        if (emitter != null)
        {
            emitter.onNext(data);
        }
    }

}