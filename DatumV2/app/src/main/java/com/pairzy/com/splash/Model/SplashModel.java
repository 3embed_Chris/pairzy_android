package com.pairzy.com.splash.Model;

import android.text.TextUtils;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.coinBalance.CoinResponse;
import com.pairzy.com.data.model.coinBalance.Coins;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigResponse;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.util.Utility;

import javax.inject.Inject;

public class SplashModel extends BaseModel {

    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    Utility utility;
    @Inject
    CoinConfigWrapper coinConfigWrapper;

    @Inject
    public SplashModel() {
    }


    public boolean isCoinBalanceEmpty() {
        return TextUtils.isEmpty(coinBalanceHolder.getCoinBalance());
    }

    public void parseCoinBalance(String response)
    { try
        {
            CoinResponse coinResponse = utility.getGson().fromJson(response,CoinResponse.class);
            Coins coins = coinResponse.getData().getCoins();
            coinBalanceHolder.setCoinBalance(String.valueOf(coins.getCoin()));
        }catch (Exception e){}
    }

    public void parseCoinConfig(String response) {
        try
        {
            CoinConfigResponse coinConfigResponse =
                    utility.getGson().fromJson(response,CoinConfigResponse.class);
            if(!coinConfigResponse.getData().isEmpty())
                coinConfigWrapper.setCoinData(coinConfigResponse.getData().get(0));
        } catch (Exception e){}
    }
}
