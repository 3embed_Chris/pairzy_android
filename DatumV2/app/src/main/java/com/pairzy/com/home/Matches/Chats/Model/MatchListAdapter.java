package com.pairzy.com.home.Matches.Chats.Model;
import android.content.Context;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pairzy.com.R;
import com.pairzy.com.util.DatumCallbacks.ListItemClick;
import com.pairzy.com.util.TypeFaceManager;
import com.facebook.drawee.generic.RoundingParams;

import java.util.ArrayList;
/**
 * @since  4/28/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MatchListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private ArrayList<ChatListData> items;
    private TypeFaceManager typeFaceManager;
    private MatchAdapterCallback adapterCallBack;
    private ListItemClick callBack;
    private Context context;
    int whiteColor, superlikedBlueColor, boostItemColor;

    public MatchListAdapter(Context context, ArrayList<ChatListData> items, TypeFaceManager typeFaceManager)
    {
        this.context = context;
        this.items=items;
        this.typeFaceManager=typeFaceManager;
        intiCallback();
        whiteColor = context.getResources().getColor(R.color.white);
        superlikedBlueColor = context.getResources().getColor(R.color.chat_superliked_blue);
        boostItemColor = context.getResources().getColor(R.color.golden_color);
    }

    /*
     * Setting the adapter callback*/
    public void setAAdapterCallBack(MatchAdapterCallback callabck)
    {
        this.adapterCallBack=callabck;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.matclist_item,parent,false);
        return new MatchedItemHolder(view,typeFaceManager,callBack);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        try
        {
            initializedHolderData((MatchedItemHolder)holder);
        }catch (Exception e){}
    }

    @Override
    public int getItemCount()
    {
        return items.size();
    }

    /*
  * inti callback*/
    private void intiCallback()
    {
        callBack= (id, position) -> {
            if(adapterCallBack==null)
                return;

            switch (id)
            {
                case R.id.parent_view:
                    adapterCallBack.openChat(position);
                    break;
            }
        };
    }

    /*
     *Initializing the adapter data holder */
    private void initializedHolderData(MatchedItemHolder holder)
    {
        int position=holder.getAdapterPosition();
        ChatListData item_data=items.get(position);
        if(item_data.isForBoost() && position == 0){
            holder.user_name.setText(R.string.likes_text);
            RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
            roundingParams.setBorder(boostItemColor, 5.0f);
            roundingParams.setRoundAsCircle(true);
            holder.user_image.getHierarchy().setRoundingParams(roundingParams);
            holder.user_image.getHierarchy().setPlaceholderImage(R.drawable.bg);
            holder.user_image.getHierarchy().setFailureImage(R.drawable.bg);
            holder.user_image.setImageURI(Uri.EMPTY);
            holder.superLikedMeIcon.setImageResource(R.drawable.icon);
            holder.superLikedMeIcon.setVisibility(View.VISIBLE);
            holder.user_status_dot.setVisibility(View.GONE);
        }else {
            holder.superLikedMeIcon.setImageResource(R.drawable.super_likes_chat);
            holder.user_image.setImageURI(item_data.getProfilePic());
            holder.user_name.setText(item_data.getFirstName());
            if (item_data.getOnlineStatus() == 1) {
                holder.user_status_dot.setImageResource(R.drawable.online_dot);
            } else {
                holder.user_status_dot.setImageResource(0);
            }

            if (item_data.getSuperlikedMe() == 1) {
                RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
                roundingParams.setBorder(superlikedBlueColor, 5.0f);
                roundingParams.setRoundAsCircle(true);
                holder.user_image.getHierarchy().setRoundingParams(roundingParams);
                holder.superLikedMeIcon.setVisibility(View.VISIBLE);
            } else {
                RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
                roundingParams.setBorder(whiteColor, 5.0f);
                roundingParams.setRoundAsCircle(true);
                holder.user_image.getHierarchy().setRoundingParams(roundingParams);
                holder.superLikedMeIcon.setVisibility(View.GONE);
            }
        }
    }
}
