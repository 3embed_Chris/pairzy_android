package com.pairzy.com.LeaderBoad.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LeaderBoardPOJO {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("recordTotal")
    @Expose
    private Integer recordTotal;
    @SerializedName("data")
    @Expose
    private ArrayList<LeaderBoardDataPOJO> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getRecordTotal() {
        return recordTotal;
    }

    public void setRecordTotal(Integer recordTotal) {
        this.recordTotal = recordTotal;
    }

    public ArrayList<LeaderBoardDataPOJO> getData() {
        return data;
    }

    public void setData(ArrayList<LeaderBoardDataPOJO> data) {
        this.data = data;
    }
}
