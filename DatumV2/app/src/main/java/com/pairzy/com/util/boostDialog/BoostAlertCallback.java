package com.pairzy.com.util.boostDialog;

/**
 * <h>BoostAlertCallback interface</h>
 * @author 3Embed.
 * @since 1/6/2018.
 * @version 1.0.
 */
public interface BoostAlertCallback
{
    void onInappSubscribe(int position);

    void onNoThanks();
}
