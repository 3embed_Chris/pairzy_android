package com.pairzy.com.commentPost.model;

import android.view.View;

public interface ItemViewCallBack {

    void onViewItemCallBack(View view, int position);
}
