package com.pairzy.com;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.media.RingtoneManager;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PersistableBundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.multidex.MultiDex;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.androidinsta.com.InstagramManger;
import com.cloudinary.android.MediaManager;
import com.couchbase.lite.android.AndroidContext;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.pairzy.com.PostMoments.model.PostData;
import com.pairzy.com.Service.PublishPost;
import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.pairzy.com.MatchedView.Its_Match_alert;
import com.pairzy.com.MatchedView.MatchAlertObserver;
import com.pairzy.com.MatchedView.MatchedModel;
import com.pairzy.com.MatchedView.OpponentDetails;
import com.pairzy.com.MqttChat.AppStateChange.AppStateListener;
import com.pairzy.com.MqttChat.AppStateChange.AppStateMonitor;
import com.pairzy.com.MqttChat.AppStateChange.RxAppStateMonitor;
import com.pairzy.com.MqttChat.Calls.CallingApis;
import com.pairzy.com.MqttChat.CallsService.AudioCallService;
import com.pairzy.com.MqttChat.CallsService.VideoCallService;
import com.pairzy.com.MqttChat.Database.CouchDbController;
import com.pairzy.com.MqttChat.DownloadFile.FileDownloadService;
import com.pairzy.com.MqttChat.DownloadFile.FileUploadService;
import com.pairzy.com.MqttChat.DownloadFile.FileUtils;
import com.pairzy.com.MqttChat.DownloadFile.ServiceGenerator;
import com.pairzy.com.MqttChat.MQtt.CustomMQtt.IMqttActionListener;
import com.pairzy.com.MqttChat.MQtt.CustomMQtt.IMqttDeliveryToken;
import com.pairzy.com.MqttChat.MQtt.CustomMQtt.IMqttToken;
import com.pairzy.com.MqttChat.MQtt.CustomMQtt.MqttCallback;
import com.pairzy.com.MqttChat.MQtt.CustomMQtt.MqttConnectOptions;
import com.pairzy.com.MqttChat.MQtt.CustomMQtt.MqttMessage;
import com.pairzy.com.MqttChat.MQtt.MqttAndroidClient;
import com.pairzy.com.MqttChat.OreoJobService;
import com.pairzy.com.MqttChat.Service.AppKilled;
import com.pairzy.com.MqttChat.Utilities.ApiOnServer;
import com.pairzy.com.MqttChat.Utilities.MQTT_constants;
import com.pairzy.com.MqttChat.Utilities.MqttEvents;
import com.pairzy.com.MqttChat.Utilities.Utilities;
import com.pairzy.com.MqttManager.MessageType;
import com.pairzy.com.MqttManager.MqttRxObserver;
import com.pairzy.com.dagger.AppComponent;
import com.pairzy.com.dagger.AppUtilModule;
import com.pairzy.com.dagger.DaggerAppComponent;
import com.pairzy.com.data.model.DateEvent;
import com.pairzy.com.data.model.ProUserModel;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Matches.Chats.Model.ChatListData;
import com.pairzy.com.home.Matches.Chats.Model.ChatListResponse;
import com.pairzy.com.networking.NetworkModule;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.splash.SplashActivity;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.CalendarEventHelper;
import com.pairzy.com.util.CustomObserver.AdminCoinObserver;
import com.pairzy.com.util.CustomObserver.dataChangeObserver.DataChangeType;
import com.pairzy.com.util.CustomObserver.dataChangeObserver.DatumDataChangeObserver;
import com.pairzy.com.util.DatumActivateLifeListener;
import com.pairzy.com.util.FileUtil.Config;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.localization.LocalizationApplicationDelegate;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebookmanager.com.FacebookManager;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;
import com.suresh.innapp_purches.InnAppSdk;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.InitializationCallback;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.pairzy.com.util.AppConfig.NOTIFICATION_CHANNEL_ID_CHAT;
import static com.pairzy.com.util.AppConfig.NOTIFICATION_CHANNEL_NAME_CHAT_NOTIFICATION;


/**
 * Created by moda on 29/06/17.
 */
public class AppController extends DaggerApplication implements Application.ActivityLifecycleCallbacks, IMqttActionListener,
        AppStateListener
{
    LocalizationApplicationDelegate localizationDelegate = new LocalizationApplicationDelegate(this);
    private ArrayList<Integer> excludedIds;
    private boolean filtersUpdated;
    private String previousMatchChatId = "";
    private boolean showMatchDialog = false;
    private PublishPost publishPost;
    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        localizationDelegate.onConfigurationChanged(this);
    }

    @Override
    public Context getApplicationContext() {
        return localizationDelegate.getApplicationContext(super.getApplicationContext());
    }

    public static final String TAG = AppController.class
            .getSimpleName();
    public static Bus bus = new Bus(ThreadEnforcer.ANY);

    private static AppController mInstance;
    private AppComponent appComponent;
    private NotificationManager notificationManager;
    private boolean serviceAlreadyScheduled = false;
    public String currentActiveDateId;

    @Inject
    Utility utility;
    @Inject
    CalendarEventHelper calendarEventHelper;
    @Inject
    InstagramManger instagramManger;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    MatchedModel matchedModel;
    @Inject
    ProUserModel proUserModel;
    @Inject
    Its_Match_alert match_alert;
    @Inject
    DatumActivateLifeListener activateLifeListener;
    @Inject
    NetworkService service;
    @Inject
    MqttRxObserver mqttRxObserver;
    @Inject
    MatchAlertObserver matchAlertObserver;
    @Inject
    AdminCoinObserver adminCoinObserver;
    @Inject
    FacebookManager facebookManager;
    @Inject
    DatumDataChangeObserver datumDataChangeObserver;
    private String lastMessageFetchMessageId;
    private String currentAppLanguage = AppConfig.DEFAULT_LANGUAGE;

    public String getCurrentAppLanguage() {
        return currentAppLanguage;
    }

    public void setCurrentAppLanguage(String currentAppLanguage) {
        this.currentAppLanguage = currentAppLanguage;
    }

    public  AppComponent getAppComponent(){
        return appComponent;
    }

    /**
     * Arrays for the secret chats dTag message received
     */
    final String[] dTimeForDB = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "15", "30", "60", "3600", "86400", "604800"};//getResources().getStringArray(R.array.dTimeForDB);
    final String[] dTimeOptions = {"off", "1 second", "2 seconds", "3 seconds", "4 seconds", "5 seconds", "6 seconds", "7 seconds",
            "8 seconds", "9 seconds",
            "10 seconds", "15 seconds", "30 seconds", "1 minute", "1 hour", "1 day", "1 week"};//getResources().getStringArray(R.array.dTimeOptions);
    private SharedPreferences sharedPref;
    private boolean foreground;
    private String chatDocId, unsentMessageDocId, mqttTokenDocId, notificationDocId,mutedDocId, blockedDocId;
    private String userName, apiToken, userId, userImageUrl, userIdentifier;
    private String indexDocId;
    private CouchDbController db;
    private boolean signedIn = false;
    /*
     * Have put this value intentionally
     */
    private String activeReceiverId = "";
    private RequestQueue mRequestQueue;
    private ArrayList<String> colors;

    private int activeActivitiesCount;

    private MqttAndroidClient mqttAndroidClient;

    public MqttAndroidClient getMqttAndroidClient() {
        return mqttAndroidClient;
    }

    public void setMqttAndroidClient(MqttAndroidClient mqttAndroidClient) {
        this.mqttAndroidClient = mqttAndroidClient;
    }

    private MqttConnectOptions mqttConnectOptions;
    private boolean flag = true;

    private ArrayList<HashMap<String, Object>> tokenMapping = new ArrayList<>();
    private HashSet<IMqttDeliveryToken> set = new HashSet<>();

    private boolean applicationKilled;
    private long timeDelta = 0;
    private boolean newSignupOrLogin = false;
    private boolean activeOnACall = false;

    private String activeCallId, activeCallerId;
    private String activeSecretId = "";

    /**
     * Initialize for callback in the chatlist or chatmessage screen activity
     */
    private boolean callMinimized = false;
    private boolean firstTimeAfterCallMinimized = false;
    /*
     * Utilities for the resending of the unsent messages
     */
    public static Bus getBus() {
        return bus;
    }
    public static synchronized AppController getInstance() {
        return mInstance;
    }
    private boolean chatSynced;
    /*
     *
     * For clubbing of the notifications
     */
    private static final int NOTIFICATION_SIZE = 5;
    private ArrayList<Map<String, Object>> notifications = new ArrayList<>();
    private boolean activeOnVideoCall = false;
    /*
     *For auto download of the media messages
     *
     */
    // private boolean autoDownloadAllowed;
    private WifiManager wifiMgr;
    private WifiInfo wifiInfo;
    /*
     *For message auto download
     */
    private String removedMessageString = "The message has been removed";

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        appComponent  = DaggerAppComponent.builder()
                .application(this)
                .appUtil(new AppUtilModule())
                .netModule(new NetworkModule())
                .build();
        appComponent.inject(this);
        return appComponent;
    }

    //ImqttActionListener
    @Override
    public void onSuccess(IMqttToken asyncActionToken) {
        Log.d(TAG, "onSuccess called!!"+mqttAndroidClient.getClientId());
        //Log.d("log53", "connectionsuccess");
        if (newSignupOrLogin) {

            newSignupOrLogin = false;
            try {
                JSONObject tempObj = new JSONObject();
                tempObj.put("status", 1);

                /*
                 * Have to retain the message of currently being available for call
                 */
                AppController.getInstance().publish(MqttEvents.CallsAvailability.value + "/" + userId, tempObj, 0, true);
                AppController.getInstance().setActiveOnACall(false, true);

            } catch (JSONException e) {
            }
        } else {
            /*
             * Have put just for testing
             */
            /*TESTING*/
            if (signedIn) {
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("status", 1);
                    publish(MqttEvents.CallsAvailability.value + "/" + userId, obj, 0, true);
                    AppController.getInstance().setActiveOnACall(false, true);

                } catch (JSONException ignored) {
                }
            }
        }
        if (!applicationKilled) {
            updatePresence(1, false);
        } else {

            updatePresence(0, true);

        }

        /*
         *
         * Also have to make user available for call
         *
         */
        try {
            JSONObject obj = new JSONObject();
            obj.put("eventName", MqttEvents.Connect.value);
            bus.post(obj);
        } catch (Exception ignored) {
        }
        /*
         * To stop the internet checking service
         */
        if (flag) {
            flag = false;
            subscribeToTopic(userId,1);
            subscribeToTopic(MqttEvents.Message.value + "/" + userId, 1);
            subscribeToTopic(MqttEvents.Acknowledgement.value + "/" + userId, 2);
            subscribeToTopic(MqttEvents.Calls.value + "/" + userId, 0);
            subscribeToTopic(MqttEvents.UserUpdates.value + "/" + userId, 1);
            subscribeToTopic(MqttEvents.FetchMessages.value + "/" + userId, 1);

        }
        resendUnsentMessages();
    }

    @Override
    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
        Log.d("log54", "connectionfailed" + exception.toString());
    }


    @Override
    public void onAppDidEnterForeground() {
        updatePresence(1, false);
        foreground = true;
    }

    @Override
    public void onAppDidEnterBackground() {
        updatePresence(0, false);
        foreground = false;
        if (dataSource.isLoggedIn()) {
            updateTokenMapping();
        }
    }

    private static Thread.UncaughtExceptionHandler mDefaultUncaughtExceptionHandler;

    private static Thread.UncaughtExceptionHandler mCaughtExceptionHandler = new Thread.UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            Log.d(TAG, "uncaughtException: activeOnCall: "+AppController.getInstance().isActiveOnACall());
            if (AppController.getInstance().isActiveOnACall()) {
                AppController.getInstance().cutCallOnKillingApp(true);
            }
            mDefaultUncaughtExceptionHandler.uncaughtException(thread, ex);
        }
    };


    @Override
    public void onCreate()
    {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate();
        //Fabric.with(this, new Crashlytics());
        CrashlyticsCore core = new CrashlyticsCore.Builder()
                .disabled(com.crashlytics.android.BuildConfig.DEBUG)
                .build();
        Fabric.with(new Fabric.Builder(this).kits(new Crashlytics.Builder()
                .core(core)
                .build())
                .initializationCallback(new InitializationCallback<Fabric>() {
                    @Override
                    public void success(Fabric fabric) {

                        Log.d("log1", "1");
                        mDefaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
                        Thread.setDefaultUncaughtExceptionHandler(mCaughtExceptionHandler);
                    }

                    @Override
                    public void failure(Exception e) {
                        e.printStackTrace();
                        Log.d("log2", "2");
                    }
                })
                .build());
        Fresco.initialize(this);
        InnAppSdk.init(this,AppConfig.InappKey.Base64Key);
        //AudienceNetworkAds.initialize(this);
        //AdSettings.setIntegrationErrorMode(INTEGRATION_ERROR_CRASH_DEBUG_MODE);
        try {
            MediaManager.init(this);
        } catch (IllegalStateException e) {
            e.fillInStackTrace();
        }
        mInstance = this;
        sharedPref = this.getSharedPreferences("defaultPreferences", Context.MODE_PRIVATE);
        registerActivityLifecycleCallbacks(activateLifeListener);
        AppStateMonitor appStateMonitor = RxAppStateMonitor.create(this);
        appStateMonitor.addListener(mInstance);
        appStateMonitor.start();
        initChatSetup();
        setBackgroundColorArray();
        registerActivityLifecycleCallbacks(mInstance);
        activeActivitiesCount = 0;
    }

    public void logIndexDocIDTesting(){
        String indexDocId = sharedPref.getString("indexDoc", null);
        Log.d(TAG, "logIndexDocIDTesting: index_doc_id: "+indexDocId);
    }
    public void initChatSetup(){
        applicationKilled = sharedPref.getBoolean("applicationKilled", false);
        indexDocId = sharedPref.getString("indexDoc", null);
        Log.d(TAG, "initChatSetup: indexDoc: "+indexDocId);
        db = new CouchDbController(new AndroidContext(this));
        if (indexDocId == null) {
            indexDocId = db.createIndexDocument();
            sharedPref.edit().putString("indexDoc", indexDocId).apply();
        }
        chatSynced = sharedPref.getBoolean("chatSynced", false);
        signedIn = dataSource.isLoggedIn();
        if (signedIn) {
            userId = dataSource.getUserId();
            userIdentifier = userId;
            apiToken = dataSource.getToken();
            getUserDocIdsFromDb(userId);
            createMQttConnection(userId,true);
        }

        if (sharedPref.getBoolean("deltaRequired", true)) {
            getCurrentTime();
        } else {
            timeDelta = sharedPref.getLong("timeDelta", 0);
        }
    }


    private void unSubscribeAllTopics(){
        unsubscribeToTopic(userId);
        unsubscribeToTopic(MqttEvents.Message.value + "/" + userId);
        unsubscribeToTopic(MqttEvents.Acknowledgement.value + "/" + userId);
        unsubscribeToTopic(MqttEvents.Calls.value + "/" + userId);
        unsubscribeToTopic(MqttEvents.UserUpdates.value + "/" + userId);
        unsubscribeToTopic(MqttEvents.FetchMessages.value + "/" + userId);
    }

    //need to finish activity after calling this
    public void appLogout() {
        Log.d(TAG, "appLogoutCalled!!");
        updatePresence(0,false);
        try {
            if(notificationManager != null)
                notificationManager.cancelAll();
            unSubscribeAllTopics();
        }catch (Exception ignored){}
        deleteCalenderEvent();
        dataSource.clear();
        instagramManger.loggedOut();
        facebookManager.logout();
        updatePresence(0,false);
        disconnect();
        if( android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP
                && (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) && serviceAlreadyScheduled ) {
            ComponentName serviceName = new ComponentName(mInstance, OreoJobService.class.getName());
            PersistableBundle extras = null;
            extras = new PersistableBundle();
            extras.putString("command", "stop");
            JobInfo jobInfo = (new JobInfo.Builder(MQTT_constants.MQTT_JOB_ID, serviceName)).setExtras(extras).setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).setMinimumLatency(1L).setOverrideDeadline(1L).build();
            JobScheduler jobScheduler = (JobScheduler) mInstance.getSystemService(Context.JOB_SCHEDULER_SERVICE);
            try {
                jobScheduler.schedule(jobInfo);
            } catch (IllegalArgumentException errorMessage) {
                errorMessage.printStackTrace();
            }
        }
        //enabled show that it can create again oreoJobService.
        mqttAndroidClient = null;
        mqttConnectOptions = null;
        //so that able to subscribe again if new user login just after logout
        flag = true;

        Intent audioServiceIntent = new Intent(mInstance, AudioCallService.class);
        stopService(audioServiceIntent);

        Intent videoServiceIntent = new Intent(mInstance, VideoCallService.class);
        stopService(videoServiceIntent);

        unsubscribeToFirebaseTopic();
        //setSignedIn(false,false,null,null,null,-1);
        Intent intent=new Intent(this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void deleteCalenderEvent() {
        ArrayList<DateEvent> dateEvents = getEventId();
        for(DateEvent dateEvent: dateEvents) {
            calendarEventHelper.deleteEvent(dateEvent.getEventId());
        }
    }

    private ArrayList<DateEvent> getEventId(){
        ArrayList<String> jsonEventList = dataSource.getEvents();
        ArrayList<DateEvent> dateEvents = new ArrayList<>();
        try {
            for (String json : jsonEventList) {
                DateEvent dateEvent = utility.getGson().fromJson(json, DateEvent.class);
                if (dateEvent != null)
                    dateEvents.add(dateEvent);
            }
        }catch (Exception ignored){}
        return dateEvents;
    }
    public void subscribeToFirebaseTopic() {
        try {
            FirebaseMessaging.getInstance().subscribeToTopic(String.valueOf("/" + "topics" + "/" + userId));
        }catch (Exception e){
            e.printStackTrace();
            Log.e("HomePresenter",e.getMessage());
        }
    }
    public void unsubscribeToFirebaseTopic() {
        try {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(String.valueOf("/" + "topics" + "/" + userId));
        }catch (Exception e){
            e.printStackTrace();
            Log.e("HomePresenter",e.getMessage());
        }
    }

    public void getUserDocIdsFromDb(String userId) {
        String userDocId = db.getUserInformationDocumentId(indexDocId, userId);
        ArrayList<String> docIds = db.getUserDocIds(userDocId);
        chatDocId = docIds.get(0);
        unsentMessageDocId = docIds.get(1);
        mqttTokenDocId = docIds.get(2);
        notificationDocId = docIds.get(3);
        mutedDocId = docIds.get(4);
        blockedDocId = docIds.get(5);
        getUserInfoFromDb(userDocId);
        tokenMapping = db.fetchMqttTokenMapping(mqttTokenDocId);
    }

    public void getUserInfoFromDb(String docId) {
        Map<String, Object> userInfo = db.getUserInfo(docId);
        userName = (String) userInfo.get("userName");
        userIdentifier = (String) userInfo.get("userIdentifier");
        apiToken = dataSource.getToken();
        userId = (String) userInfo.get("userId");
        userImageUrl = (String) userInfo.get("userImageUrl");
        // Log.d("log71", apiToken + " " + userName + " " + userIdentifier + " " + userId + " " + userImageUrl);
        notifications = db.fetchAllNotifications(notificationDocId);
    }

    /**
     * Prepare image or audio or video file for upload
     */
    @SuppressWarnings("all")
    public File convertByteArrayToFileToUpload(byte[] data, String name, String extension) {
        File file = null;
        try {
            File folder = new File(Environment.getExternalStorageDirectory().getPath() + Config.CHAT_UPLOAD_THUMBNAILS_FOLDER);
            if (!folder.exists() && !folder.isDirectory()) {
                folder.mkdirs();
            }

            file = new File(Environment.getExternalStorageDirectory().getPath() + Config.CHAT_UPLOAD_THUMBNAILS_FOLDER, name + extension);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(file);

            fos.write(data);
            fos.flush();
            fos.close();
        } catch (IOException e) {

        }


        return file;

    }

    /**
     * To prepare image file for upload
     */
    private int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {


        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;


            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private Bitmap decodeSampledBitmapFromResource(String pathName,
                                                   int reqWidth, int reqHeight) {


        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathName, options);


        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);


        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathName, options);

    }

    /**
     * To upload image or video or audio to server using multipart upload to avoid OOM exception
     */
    @SuppressWarnings("TryWithIdenticalCatches,all")

    private void uploadFile(final Uri fileUri, final String name, final int messageType,
                            final JSONObject obj, final String receiverUid, final String id,
                            final HashMap<String, Object> mapTemp, final String secretId, final String extension,
                            final boolean toDelete, final boolean isGroupMessage, final Object groupMembersDocId) {

        FileUploadService service =
                ServiceGenerator.createService(FileUploadService.class);

        final File file = FileUtils.getFile(this, fileUri);

        String url = null;
        if (messageType == 1) {

            url = name + ".jpg";


        } else if (messageType == 2) {

            url = name + ".mp4";


        } else if (messageType == 5) {

            url = name + ".mp3";


        } else if (messageType == 7) {

            url = name + ".jpg";


        } else if (messageType == 9) {

            url = name + extension;


        }


        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);


        MultipartBody.Part body =
                MultipartBody.Part.createFormData("photo", url, requestFile);


        String descriptionString = "Datum File Uploading";
        RequestBody description =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), descriptionString);

        // finally, execute the request
        Call<ResponseBody> call = service.upload(description, body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {

/**
 *
 *
 * has to get url from the server in response
 *
 *
 * */


                try {


                    if (response.code() == 200) {


                        String url = null;
                        if (messageType == 1) {

                            url = name + ".jpg";


                        } else if (messageType == 2) {

                            url = name + ".mp4";


                        } else if (messageType == 5) {

                            url = name + ".mp3";


                        } else if (messageType == 7) {

                            url = name + ".jpg";


                        } else if (messageType == 9) {

                            url = name + extension;


                        }
                        Log.d(TAG, "onResponse: fetchUrl "+ ApiOnServer.CHAT_FETCH_PATH + url);
                        obj.put("payload", Base64.encodeToString((ApiOnServer.CHAT_FETCH_PATH + url).getBytes("UTF-8"), Base64.DEFAULT));
                        obj.put("dataSize", file.length());
                        obj.put("timestamp", new Utilities().gmtToEpoch(Utilities.tsInGmt()));


                        if (toDelete) {
                            File fdelete = new File(fileUri.getPath());
                            if (fdelete.exists()) fdelete.delete();
                        }

                    }


                } catch (JSONException e) {

                } catch (IOException e) {

                }


                /**
                 *
                 *
                 * emitting to the server the values after the file has been uploaded
                 *
                 **/


                String tsInGmt = Utilities.tsInGmt();
                String docId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid, secretId);
                db.updateMessageTs(docId, id, tsInGmt);

                AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + receiverUid, obj, 1, false, mapTemp);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    /**
     * Update change in signin status
     * If signed in then have to connect socket,start listening on various socket events and disconnect socket in case of signout, stop listening on various socket events
     */
    public void setSignedIn(boolean fromSplash,   final boolean signedIn, final String userId, final String userName, final String userIdentifier) {
        this.signedIn = signedIn;
        apiToken = dataSource.getToken();
        mInstance.userId = userId;
        mInstance.userIdentifier = userIdentifier;
        mInstance.userName = userName;
        newSignupOrLogin = true;

        if(!fromSplash) {
            mqttAndroidClient = null;
            mqttConnectOptions = null;
        }
        flag = true;
        getUserDocIdsFromDb(userId);

        if(!fromSplash) {
            AppController.getInstance().setServiceAlredyScheduled(false);
            createMQttConnection(userId, true);
        }

        if (sharedPref.getString("chatNotificationArray", null) == null) {
            SharedPreferences.Editor prefsEditor = sharedPref.edit();
            prefsEditor.putString("chatNotificationArray", new Gson().toJson(new ArrayList<Map<String, String>>()));
            prefsEditor.apply();
        }

        getChatHistory();
    }

    private void setServiceAlredyScheduled(boolean isScheduled) {
        this.serviceAlreadyScheduled = isScheduled;
    }

    private void getChatHistory() {

        System.out.println("1:logged in getChats: ");

        CompositeDisposable compositeDisposable=new CompositeDisposable();

        service.getChats(dataSource.getToken(),AppConfig.DEFAULT_LANGUAGE, "0")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> value)
                    {
                        System.out.println("2:logged in getChats: ");
                        try
                        {
                            if(value.code() == 200)
                            {
                                String data = value.body().string();
                                getChatsHandler(data);
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        System.out.println("3:logged in getChats: ");


                    }
                    @Override
                    public void onComplete() {}
                });
    }

    private void getChatsHandler(String response) {
        System.out.println("4:logged in getChats: ");

        ChatListResponse result_data = utility.getGson().fromJson(response, ChatListResponse.class);
        ArrayList<ChatListData> temp = result_data.getData();

        for (int i = 0; i < temp.size(); i++) {
            System.out.println("5:logged in getChats: ");

            ChatListData chatListData = temp.get(i);
            //if (!chatListData.getPayload().equals(AppConfig.DEFAULT_MESSAGE)){
            String docId = AppController.getInstance().findDocumentIdOfReceiver(chatListData.getRecipientId(), "");
            System.out.println("6:logged in getChats: " + docId);
            if (TextUtils.isEmpty(docId)) {
                docId = AppController.findDocumentIdOfReceiver(chatListData.getRecipientId(), Utilities.tsInGmt(), chatListData.getFirstName(),
                        chatListData.getProfilePic(), "", false, chatListData.getRecipientId(), chatListData.getChatId(), false);
                System.out.println("7:logged in getChats: " + docId);
            }
            AppController.getInstance().getDbController()
                    .updateChatUserData(docId,
                            chatListData.getFirstName(),
                            chatListData.getProfilePic(),
                            chatListData.getIsMatchedUser() >0,
                            chatListData.getSuperlikedMe() > 0,
                            chatListData.isInitiated(),
                            chatListData.getPayload().equals(AppConfig.DEFAULT_MESSAGE)
                    );

            if(chatListData.getIsBlocked() > 0 || chatListData.getIsBlockedByMe() > 0) {
                AppController.getInstance().getDbController().addBlockedUser(AppController.getInstance().getBlockedDocId(), chatListData.getRecipientId(), chatListData.getRecipientId(), chatListData.getIsBlockedByMe() > 0, chatListData.getIsBlocked() > 0);
            }else{
                AppController.getInstance().getDbController().removeBlockedUser(AppController.getInstance().getBlockedDocId(),chatListData.getRecipientId());
            }

            Log.d(TAG, String.format(Locale.ENGLISH, "getChatsHandler: \n" +
                            "firstName: %s \n" +
                            "profileImage: %s \n" +
                            "isMatch: %d \n" +
                            "isSuperlikedMe : %d\n" +
                            "isInitiated: %d\n" +
                            "hasDefaultMessage : %d" +
                            "isBlocked : %d " +
                            "isBlockedByMe : %d",
                    chatListData.getFirstName(),
                    chatListData.getProfilePic(),
                    chatListData.getIsMatchedUser(),
                    chatListData.getSuperlikedMe(),
                    chatListData.isInitiated()?1:0,
                    chatListData.getPayload().equals(AppConfig.DEFAULT_MESSAGE)?1:0,
                    chatListData.getIsBlocked(),
                    chatListData.getIsBlockedByMe()
            ));

            //}
        }
    }

    public long getTimeDelta() {
        return timeDelta;
    }

    public boolean getSignedIn() {
        return this.signedIn;
    }

    public String getChatDocId() {

        return chatDocId;
    }

    public String getUserIdentifier() {


        return userIdentifier;
    }

    public String getApiToken() {


        return apiToken;
    }

    public boolean isForeground() {
        return foreground;
    }

    public SharedPreferences getSharedPreferences() {

        return sharedPref;
    }

    public boolean isActiveOnVideoCall() {
        return activeOnVideoCall;
    }

    public String getActiveReceiverId() {

        return activeReceiverId;
    }

    public void setActiveReceiverId(String receiverId) {
        this.activeReceiverId = receiverId;
    }

    public void setActiveSecretId(String secretId) {
        this.activeSecretId = secretId;
    }

    public String getActiveSecretId() {
        return activeSecretId;
    }

    public boolean isCallMinimized() {
        return callMinimized;
    }

    public void setCallMinimized(boolean callMinimized) {
        this.callMinimized = callMinimized;

    }
    public boolean getChatSynced() {
        return chatSynced;
    }

    public void setChatSynced(boolean synced) {
        chatSynced = synced;
        sharedPref.edit().putBoolean("chatSynced", synced).apply();
    }
    public boolean isActiveOnACall() {
        return activeOnACall;
    }

    public void setActiveOnACall(boolean activeOnACall, boolean notCallCut) {
        this.activeOnACall = activeOnACall;
        if (!activeOnACall && notCallCut) {

            this.callMinimized = false;

        }
    }
    public boolean isFirstTimeAfterCallMinimized() {
        return firstTimeAfterCallMinimized;
    }

    public void setFirstTimeAfterCallMinimized(boolean firstTimeAfterCallMinimized) {
        this.firstTimeAfterCallMinimized = firstTimeAfterCallMinimized;
    }
    public CouchDbController getDbController() {
        return db;
    }

    public void setApplicationKilled(boolean applicationKilled) {
        this.applicationKilled = applicationKilled;
        sharedPref.edit().putBoolean("applicationKilled", applicationKilled).apply();
        if (applicationKilled) {
            sharedPref.edit().putString("lastSeenTime", Utilities.tsInGmt()).apply();
        }
    }

    public String getUserName() {
        return userName;
    }

    public String getUserId() {
        return userId;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public int getActiveActivitiesCount() {
        return activeActivitiesCount;
    }

    public void setActiveCallId(String activeCallId) {
        this.activeCallId = activeCallId;
    }

    public void setActiveCallerId(String activeCallerId) {
        this.activeCallerId = activeCallerId;
    }

    public String getunsentMessageDocId() {
        return unsentMessageDocId;
    }

    public String getMutedDocId() {

        return mutedDocId;
    }

    public String getBlockedDocId() {

        return blockedDocId;
    }

    public String getIndexDocId() {

        return indexDocId;
    }
    /**
     * To search if there exists any document containing chat for a particular receiver and if founde returns its document
     * id else return empty string
     */
    @SuppressWarnings("unchecked")
    public String findDocumentIdOfReceiver(String ReceiverUid, String secretId)
    {
        String docId = "";

        Map<String, Object> chatDetails = db.getAllChatDetails(AppController.getInstance().getChatDocId());

        if (chatDetails != null) {
            ArrayList<String> receiverUidArray = (ArrayList<String>) chatDetails.get("receiverUidArray");

            ArrayList<String> receiverDocIdArray = (ArrayList<String>) chatDetails.get("receiverDocIdArray");

            ArrayList<String> secretIdArray = (ArrayList<String>) chatDetails.get("secretIdArray");

            for (int i = 0; i < receiverUidArray.size(); i++) {

                if (receiverUidArray.get(i).equals(ReceiverUid)) {

                    if (secretId.isEmpty()) {
                        if (secretIdArray.get(i).isEmpty()) {
                            return receiverDocIdArray.get(i);
                        }
                    } else {
                        if (secretIdArray.get(i).equals(secretId)) {
                            return receiverDocIdArray.get(i);
                        }
                    }

                }

            }
        }

        return docId;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    private void setBackgroundColorArray() {

        colors = new ArrayList<>();

        colors.add("#FFCDD2");
        colors.add("#D1C4E9");
        colors.add("#B3E5FC");
        colors.add("#C8E6C9");
        colors.add("#FFF9C4");
        colors.add("#FFCCBC");
        colors.add("#CFD8DC");
        colors.add("#F8BBD0");
        colors.add("#C5CAE9");
        colors.add("#B2EBF2");
        colors.add("#DCEDC8");
        colors.add("#FFECB3");
        colors.add("#D7CCC8");
        colors.add("#F5F5F5");
        colors.add("#FFE0B2");
        colors.add("#F0F4C3");
        colors.add("#B2DFDB");
        colors.add("#BBDEFB");
        colors.add("#E1BEE7");

    }

    public String getColorCode(int position) {

        return colors.get(position);
    }


    /**
     * @param clientId is same as the userId
     */
    @SuppressWarnings("unchecked")
    public MqttAndroidClient createMQttConnection(String clientId, boolean notFromJobScheduler) {

        Log.d("log52","creating mqtt connection "+clientId+", notFromJobScheduler: "+notFromJobScheduler);
        if (!dataSource.isLoggedIn())
        {
            clientId = "123";
            return null;
        }

        if(mqttAndroidClient == null || mqttConnectOptions ==null) {

            Log.d(TAG, "createMQttConnection: called!!");
            String serverUri = "tcp://" + ApiOnServer.BROKER_URL /*+ ":" + ApiOnServer.PORT*/;

            mqttAndroidClient = new MqttAndroidClient(mInstance, serverUri, clientId);

            mqttAndroidClient.setCallback(new MqttCallback() {

                @Override
                public void connectionLost(Throwable cause) {

                    Log.d("log55", "connectionlost: "+ cause);
                    try {
                        JSONObject obj = new JSONObject();
                        obj.put("eventName", MqttEvents.Disconnect.value);

                        bus.post(obj);
                    } catch (Exception e) {

                    }

                }

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {

                    JSONObject obj = convertMessageToJsonObject(message);

                    Log.d("log56", topic + " " + obj.toString());
                    if (topic.equals(userId)) {
                        String messageType = obj.getString("messageType");
                        switch (messageType) {
                            case MessageType.UN_BLOCK:
                                proUserModel.parseUserUnblocked(obj.toString());
                                datumDataChangeObserver.publishData(DataChangeType.BLOCK_USER);
                                break;
                            case MessageType.BLOCK:
                                proUserModel.parseUserBlocked(obj.toString());
                                datumDataChangeObserver.publishData(DataChangeType.UN_BLOCK_USER);
                                break;
                            case MessageType.BOOST:
                                proUserModel.parseLikeCount(obj.toString());
                                Log.d("AppController", "New Like Count received!!");
                                break;
                            case MessageType.NEW_LIKES:
                                proUserModel.parseLikeRefresh(obj.toString());
                                Log.d("AppController", "Next Like time refreshed");
                                break;
                            case MessageType.PRO_USER:
                                proUserModel.parseResponse(obj.toString());
                                break;
                            case MessageType.DELETE_USER:
                                Log.d("AppController", "This User has been Deleted by Admin!");
                                appLogout();
                                break;
                            case MessageType.BANDED_USER:
                                Log.d("AppController", "This User has been Blocked!");
                                appLogout();
                                break;
                            case MessageType.ADMIN_COIN:
                                Log.d("AppController", "Admin added coin to user id: " + userId);
                                proUserModel.parseAddCoinResponse(obj);
                                adminCoinObserver.publishData(true);
                                //appLogout();
                                break;
                            case MessageType.MATCH:
                                OpponentDetails details = null;
                                showMatchDialog = false;
                                try {
                                    details = matchedModel.parseMatch(obj.toString());
                                    String currentDateChatId = details.getChatId();
                                    if(TextUtils.isEmpty(previousMatchChatId) || !previousMatchChatId.equals(currentDateChatId)) {
                                        showMatchDialog =true;
                                    }

                                    if(showMatchDialog) {
                                        previousMatchChatId = details.getChatId();
                                        String docId = findDocumentIdOfReceiver(details.getUser_id(), "");
                                        boolean hasDefaultMessage = true;
                                        boolean isInitiated = false;
                                        if (TextUtils.isEmpty(docId)) {
                                            docId = AppController.findDocumentIdOfReceiver(details.getUser_id(), Utilities.tsInGmt(),
                                                    details.getName(), details.getProfile_pic(), "", false, details.getUser_id(), details.getChatId(), false);

                                        } else {
                                            Map<String, Object> chatInfo = AppController.getInstance().getDbController().getChatInfo(docId);
                                            hasDefaultMessage = (boolean) chatInfo.get("hasDefaultMessage");
                                            isInitiated = (boolean) chatInfo.get("initiated");
                                        }
                                        AppController.getInstance().getDbController()
                                                .updateChatUserData(docId,
                                                        details.getName(),
                                                        details.getProfile_pic(),
                                                        true,
                                                        details.getIsSuperlikedMe() > 0,
                                                        isInitiated,
                                                        hasDefaultMessage
                                                );
                                        Log.d(TAG, String.format(Locale.ENGLISH,
                                                "messageArrived Match: \n" +
                                                        "docId: %s \n" +
                                                        "userId: %s \n" +
                                                        "userName: %s \n" +
                                                        "profilePic: %s \n" +
                                                        "isSuperlikedMe: %d \n" +
                                                        "chatId: %s ", details.getUser_id(),
                                                details.getUser_id(),
                                                details.getName(),
                                                details.getProfile_pic(),
                                                details.getIsSuperlikedMe(),
                                                details.getChatId()));
                                        match_alert.show_info_alert(activateLifeListener.isHomeActivityActive(), details.getProfile_pic(), details.getUser_id(), details.getName(), details.getChatId(), details.getIsSuperlikedMe() > 0);
                                        //custom message
                                        com.pairzy.com.MqttManager.Model.MqttMessage mqttMessage2 = new com.pairzy.com.MqttManager.Model.MqttMessage();
                                        mqttMessage2.setData(obj);
                                        mqttMessage2.setTopic(topic);
                                        matchAlertObserver.publishData(details);
                                        mqttRxObserver.publish(mqttMessage2);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    if (details != null && showMatchDialog)
                                        match_alert.showNotification(details.getProfile_pic(), details.getUser_id(), details.getName());
                                }
                                break;
                            case MessageType.UN_MATCH:
                                String userId = matchedModel.parseUnmatchResponse(obj.toString());
                                String docId = AppController.getInstance().findDocumentIdOfReceiver(userId,"");
                                if(!TextUtils.isEmpty(docId)){
                                    AppController.getInstance().getDbController().deleteParticularChatDetail(AppController.getInstance().getChatDocId(),docId,false,"");
                                }
                                Log.d(TAG, String.format(Locale.ENGLISH,"messageArrived: unMatch \n" +
                                        "docId: %s \n" +
                                        "userId: %s",docId,userId));
                                com.pairzy.com.MqttManager.Model.MqttMessage mqttMessage3 = new com.pairzy.com.MqttManager.Model.MqttMessage();
                                mqttMessage3.setData(obj);
                                mqttMessage3.setTopic(topic);
                                mqttRxObserver.publish(mqttMessage3);
                                break;
                        }
                    } else if (topic.equals(MqttEvents.Acknowledgement.value + "/" + userId)) {

                        /*
                         * For an acknowledgement message received
                         */

                        String sender = obj.getString("from");
                        String document_id_DoubleTick = obj.getString("doc_id");
                        String status = obj.getString("status");


                        JSONArray arr_temp = obj.getJSONArray("msgIds");
                        String id = arr_temp.getString(0);

                        /*
                         * For callback in to activity to update UI
                         */
                        try {

                            obj.put("msgId", id);
                            obj.put("eventName", topic);
                            bus.post(obj);

                        } catch (JSONException e) {

                        }


                        String secretId = "";
                        if (obj.has("secretId")) {
                            secretId = obj.getString("secretId");

                        }
                        if (status.equals("2")) {

                            /*
                             * message delivered
                             */


                            if (!(db.updateMessageStatus(document_id_DoubleTick, id, 1, obj.getString("deliveryTime")))) {
                                db.updateMessageStatus(
                                        db.getDocumentIdOfReceiverChatlistScreen(
                                                AppController.getInstance().getChatDocId(), sender, secretId), id, 1, obj.getString("deliveryTime"));

                            }
                        } else {
                            /*
                             * message read
                             */
                            if (!secretId.isEmpty()) {

                                if (obj.getLong("dTime") != -1) {
                                    String docId = findDocumentIdOfReceiver(sender, secretId);
                                    if (!docId.isEmpty()) {
                                        setTimer(docId, id, obj.getLong("dTime") * 1000);
                                    }
                                }
                            }

                            if (!(db.drawBlueTickUptoThisMessage(document_id_DoubleTick, id, obj.getString("readTime")))) {


                                db.drawBlueTickUptoThisMessage(
                                        db.getDocumentIdOfReceiverChatlistScreen(
                                                AppController.getInstance().getChatDocId(), sender, secretId), id, obj.getString("readTime"));

                            }
                        }


                    } else if (topic.equals(MqttEvents.Message.value + "/" + userId)) {

                        /*
                         * For an actual message(Like text,image,video etc.) received
                         */

                        Log.d("log56", topic + " " + obj.toString());

                        String receiverUid = obj.getString("from");
                        String receiverIdentifier = obj.getString("receiverIdentifier");
                        int isMatchedUser = 0; //not a match assuming by default

                        if(obj.has("isMatchedUser"))
                            isMatchedUser = obj.getInt("isMatchedUser");
                        String chatId = "";  //
                        if (obj.has("chatId"))
                            chatId = obj.getString("chatId");

                        String messageType = obj.getString("type");
                        String actualMessage = obj.getString("payload").trim();
                        if (actualMessage.equals(AppConfig.DEFAULT_MESSAGE))
                            return;
                        String timestamp = String.valueOf(obj.getString("timestamp"));

                        String id = obj.getString("id");

                        String mimeType = "", fileName = "", extension = "";
                        String docIdForDoubleTickAck = obj.getString("toDocId");
                        int dataSize = -1;

                        if (messageType.equals("1") || messageType.equals("2") || messageType.equals("5") || messageType.equals("7")
                                || messageType.equals("9")) {
                            dataSize = obj.getInt("dataSize");


                            if (messageType.equals("9")) {

                                mimeType = obj.getString("mimeType");
                                fileName = obj.getString("fileName");
                                extension = obj.getString("extension");

                            }

                        } else if (messageType.equals("10")) {

                            String replyType = obj.getString("replyType");

                            if (replyType.equals("1") || replyType.equals("2") || replyType.equals("5") || replyType.equals("7")
                                    || replyType.equals("9")) {
                                dataSize = obj.getInt("dataSize");


                                if (replyType.equals("9")) {


                                    mimeType = obj.getString("mimeType");
                                    fileName = obj.getString("fileName");
                                    extension = obj.getString("extension");


                                }

                            }
                        }

                        String secretId = "";
                        long dTime = -1;
                        if (obj.has("secretId")) {
                            secretId = obj.getString("secretId");
                            dTime = obj.getLong("dTime");
                        }

                        String receiverName = "";
                        String userImage;

                        receiverName = obj.getString("name");
                        userImage = obj.getString("userImage");

                        String documentId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid, secretId);

                        if (documentId.isEmpty()) {
                            /*
                             * Here, chatId is assumed to be empty
                             */
                            documentId = findDocumentIdOfReceiver(receiverUid, Utilities.tsInGmt(),
                                    receiverName, userImage, secretId, true, receiverIdentifier, chatId, false);
                        }
                        //now it has message
                        db.updateChatUserHasDefaultMessage(documentId,false);

                        /*
                         * For message removal
                         */
                        db.setDocumentIdOfReceiver(documentId, docIdForDoubleTickAck, receiverUid);
                        db.updateDestructTime(documentId, dTime);
                        /*
                         * For callback in to activity to update UI
                         */
                        if (!db.checkAlreadyExists(documentId, id)) {
                            String replyType = "";

                            if (messageType.equals("1") || messageType.equals("2") || messageType.equals("7")) {


                                AppController.getInstance().putMessageInDb(receiverUid, messageType,
                                        actualMessage, timestamp, id, documentId, obj.getString("thumbnail").trim(),
                                        dataSize, dTime, receiverName, false, -1, false, -1, null, obj.has("wasEdited"));
                            } else if (messageType.equals("9")) {
                                /*
                                 * For document received
                                 */

                                AppController.getInstance().putMessageInDb(receiverUid,
                                        actualMessage, timestamp, id, documentId, dataSize,
                                        dTime, false, -1, false, -1, mimeType, fileName, extension);


                            } else if (messageType.equals("10")) {

                                /*
                                 * For the reply message received
                                 */

                                replyType = obj.getString("replyType");

                                String previousMessageType = obj.getString("previousType");

                                String previousFileType = "", thumbnail = "";


                                if (previousMessageType.equals("9")) {

                                    previousFileType = obj.getString("previousFileType");

                                }
                                if (replyType.equals("1") || replyType.equals("2") || replyType.equals("7")) {


                                    thumbnail = obj.getString("thumbnail").trim();

                                }


                                AppController.getInstance().putReplyMessageInDb(receiverUid,
                                        actualMessage, timestamp, id, documentId,
                                        dataSize, dTime, false, -1, false, -1, mimeType, fileName, extension, thumbnail, receiverName,
                                        replyType, obj.getString("previousReceiverIdentifier"), obj.getString("previousFrom"),
                                        obj.getString("previousPayload"), obj.getString("previousType"), obj.getString("previousId"), previousFileType, obj.has("wasEdited"));


                            } else if (messageType.equals("11")) {


                                db.updateChatListForRemovedOrEditedMessage(documentId, actualMessage, true, Utilities.epochtoGmt(obj.getString("removedAt")));
                                db.markMessageAsRemoved(documentId, id, removedMessageString, Utilities.epochtoGmt(obj.getString("removedAt")));
                                try {
                                    obj.put("eventName", topic);
                                    bus.post(obj);
                                } catch (Exception e) {
                                }
                                return;
                            } else if (messageType.equals("12")) {


                                db.updateChatListForRemovedOrEditedMessage(documentId, actualMessage, true, Utilities.epochtoGmt(obj.getString("editedAt")));
                                db.markMessageAsEdited(documentId, id, actualMessage);
                                try {

                                    obj.put("eventName", topic);
                                    bus.post(obj);


                                } catch (Exception e) {

                                }
                                return;
                            } else {

                                AppController.getInstance().putMessageInDb(receiverUid,
                                        messageType, actualMessage, timestamp, id, documentId, null, dataSize, dTime, receiverName, false, -1, false, -1, null, obj.has("wasEdited"));

                            }


                            //     if (!messageType.equals("11") && !messageType.equals("12") &&(!messageType.equals("0") || !actualMessage.isEmpty())) {

                            if (!messageType.equals("0") || !actualMessage.isEmpty()) {

                                JSONObject obj2 = new JSONObject();
                                obj2.put("from", AppController.getInstance().userId);
                                obj2.put("msgIds", new JSONArray(Arrays.asList(new String[]{id})));
                                obj2.put("doc_id", docIdForDoubleTickAck);
                                obj2.put("to", receiverUid);
                                obj2.put("status", "2");

                                if (!secretId.isEmpty()) {
                                    obj2.put("secretId", secretId);
                                    obj2.put("dTime", dTime);
                                }
                                obj2.put("deliveryTime", Utilities.getGmtEpoch());
                                AppController.getInstance().publish(MqttEvents.Acknowledgement.value + "/" + receiverUid, obj2, 2, false);
                            }
                            /*
                             * For callback in to activity to update UI
                             */

                            try {
                                obj.put("eventName", topic);
                                bus.post(obj);

                            } catch (Exception e) {

                            }

                            //TODO: commented contactSync to see notification
                            //if (contactSynced) {
                            if (!db.checkIfReceiverChatMuted(mutedDocId, receiverUid, secretId)) {
                                Intent intent;

//                        if (signInType != 0) {

//                            if (secretId.isEmpty()) {
//                                intent = new Intent(mInstance, com.example.moda.mqttchat.ContactSync.Activities.ChatMessagesScreen.class);
//                            } else {
//
//                                intent = new Intent(mInstance, com.example.moda.mqttchat.ContactSync.SecretChat.SecretChatMessageScreen.class);
//                                intent.putExtra("secretId", secretId);
//                            }


                                /*
                                 * To allow retrieval
                                 */
                                intent = new Intent(mInstance, ChatMessageActivity.class);

                                intent.putExtra("isMatched", (isMatchedUser == 1));
                                intent.putExtra("receiverUid", receiverUid);
                                intent.putExtra("receiverName", receiverName);

                                intent.putExtra("receiverIdentifier", receiverIdentifier);

                                intent.putExtra("documentId", documentId);


                                intent.putExtra("colorCode", AppController.getInstance().getColorCode(5));

                                intent.putExtra("receiverImage", userImage);

                                intent.putExtra("fromNotification", true);

                                /*
                                 *To generate the push notification locally
                                 */
                                generatePushNotificationLocal(documentId, messageType, receiverName,
                                        actualMessage, intent, dTime, secretId, receiverUid, replyType);
                            }
                            //}

                            int type = Integer.parseInt(messageType);
                            int replyTypeInt = -1;
                            if (!replyType.isEmpty()) {
                                replyTypeInt = Integer.parseInt(replyType);
                            }

                            if (type == 1 || type == 2 || type == 5 || type == 7 || type == 9 ||
                                    (type == 10 && (replyTypeInt == 1 || replyTypeInt == 2 || replyTypeInt == 5
                                            || replyTypeInt == 7 || replyTypeInt == 9))) {
                                if (ActivityCompat.checkSelfPermission(mInstance, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                        == PackageManager.PERMISSION_GRANTED) {

                                    if (checkWifiConnected()) {
                                        Object[] params = new Object[8];
                                        try {
                                            params[0] = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");
                                        } catch (UnsupportedEncodingException e) {

                                        }

                                        params[1] = messageType;
                                        params[5] = id;
                                        params[6] = documentId;
                                        params[7] = receiverUid;
                                        switch (type) {
                                            case 1:

                                            {
                                                if (sharedPref.getBoolean("wifiPhoto", false)) {

                                                    params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;

                                            }
                                            case 2: {
                                                if (sharedPref.getBoolean("wifiVideo", false)) {
                                                    params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;
                                            }
                                            case 5: {
                                                if (sharedPref.getBoolean("wifiAudio", false)) {
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";


                                                    new DownloadMessage().execute(params);
                                                }
                                                break;

                                            }
                                            case 7: {
                                                if (sharedPref.getBoolean("wifiPhoto", false)) {
                                                    params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;
                                            }
                                            case 9: {
                                                if (sharedPref.getBoolean("wifiDocument", false)) {
                                                    params[4] = Environment.getExternalStorageDirectory().getPath()
                                                            + Config.CHAT_DOWNLOADS_FOLDER + fileName;
                                                    new DownloadMessage().execute(params);
                                                }
                                                break;
                                            }
                                            case 10: {
                                                params[2] = obj.getString("replyType");
                                                switch (Integer.parseInt((String) params[2])) {
                                                    case 1:
                                                    {
                                                        if (sharedPref.getBoolean("wifiPhoto", false)) {

                                                            params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;

                                                    }
                                                    case 2:

                                                    {
                                                        if (sharedPref.getBoolean("wifiVideo", false)) {
                                                            params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;
                                                    }
                                                    case 5: {
                                                        if (sharedPref.getBoolean("wifiAudio", false)) {
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";

                                                            new DownloadMessage().execute(params);

                                                        }
                                                        break;
                                                    }

                                                    case 7: {
                                                        if (sharedPref.getBoolean("wifiPhoto", false)) {

                                                            params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";


                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;
                                                    }
                                                    case 9: {
                                                        if (sharedPref.getBoolean("wifiDocument", false)) {
                                                            params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                    + Config.CHAT_DOWNLOADS_FOLDER + fileName;

                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;
                                                    }
                                                }
                                                break;
                                            }

                                        }


                                    } else if (checkMobileDataOn()) {


                                        Object[] params = new Object[8];
                                        try {


                                            params[0] = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");

                                        } catch (UnsupportedEncodingException e) {

                                        }

                                        params[1] = messageType;
                                        params[5] = id;
                                        params[6] = documentId;
                                        params[7] = receiverUid;
                                        switch (type) {
                                            case 1: {
                                                if (sharedPref.getBoolean("mobilePhoto", false)) {
                                                    params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                    new DownloadMessage().execute(params);
                                                }
                                                break;
                                            }
                                            case 2: {
                                                if (sharedPref.getBoolean("mobileVideo", false)) {
                                                    params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;
                                            }
                                            case 5: {
                                                if (sharedPref.getBoolean("mobileAudio", false)) {
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";


                                                    new DownloadMessage().execute(params);
                                                }
                                                break;

                                            }
                                            case 7: {
                                                if (sharedPref.getBoolean("mobilePhoto", false)) {
                                                    params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                    params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                            Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                    new DownloadMessage().execute(params);

                                                }
                                                break;
                                            }
                                            case 9: {
                                                if (sharedPref.getBoolean("mobileDocument", false)) {
                                                    params[4] = Environment.getExternalStorageDirectory().getPath()
                                                            + Config.CHAT_DOWNLOADS_FOLDER + fileName;


                                                    new DownloadMessage().execute(params);
                                                }
                                                break;
                                            }
                                            case 10: {


                                                params[2] = obj.getString("replyType");

                                                switch (Integer.parseInt((String) params[2])) {

                                                    case 1: {
                                                        if (sharedPref.getBoolean("mobilePhoto", false)) {
                                                            params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;
                                                    }
                                                    case 2: {
                                                        if (sharedPref.getBoolean("mobileVideo", false)) {
                                                            params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;
                                                    }
                                                    case 5: {
                                                        if (sharedPref.getBoolean("mobileAudio", false)) {
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";

                                                            new DownloadMessage().execute(params);

                                                        }
                                                        break;
                                                    }
                                                    case 7: {
                                                        if (sharedPref.getBoolean("mobilePhoto", false)) {
                                                            params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                            params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                    Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;
                                                    }
                                                    case 9: {
                                                        if (sharedPref.getBoolean("mobileDocument", false)) {
                                                            params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                    + Config.CHAT_DOWNLOADS_FOLDER + fileName;

                                                            new DownloadMessage().execute(params);
                                                        }
                                                        break;
                                                    }
                                                }
                                                break;
                                            }

                                        }


                                    }


                                }
                            }
                        } else {
                            if (messageType.equals("11")) {
                                db.updateChatListForRemovedOrEditedMessage(documentId, actualMessage, true, Utilities.epochtoGmt(obj.getString("removedAt")));
                                db.markMessageAsRemoved(documentId, id, removedMessageString, Utilities.epochtoGmt(obj.getString("removedAt")));
                                try {
                                    obj.put("eventName", topic);
                                    bus.post(obj);
                                } catch (Exception e) {
                                }
                                return;
                            } else if (messageType.equals("12")) {

                                db.updateChatListForRemovedOrEditedMessage(documentId, actualMessage, true, Utilities.epochtoGmt(obj.getString("editedAt")));
                                db.markMessageAsEdited(documentId, id, actualMessage);
                                try {

                                    obj.put("eventName", topic);
                                    bus.post(obj);


                                } catch (Exception e) {

                                }
                                return;
                            }

                        }

                        if (!secretId.isEmpty()) {
                            if (!actualMessage.trim().isEmpty() || dTime != -1) {
                                db.updateSecretInviteImageVisibility(documentId, false);
                            }
                        }

                    } else if (topic.substring(0, 3).equals("Onl"))

                    {

                        /*
                         * For a message received on the online status topic
                         */


                        /*
                         * To check for the online status
                         */

                        try {
                            obj.put("eventName", topic);
                            bus.post(obj);

                        } catch (Exception e) {

                        }

                    } else if (topic.substring(0, 4).equals(MqttEvents.Typing.value)) {



                        /*
                         * For a message received on the Typing status topic
                         */


                        if (!activeReceiverId.isEmpty()) {


                            try {

                                obj.put("eventName", topic);
                                bus.post(obj);

                            } catch (Exception e) {

                            }


                        }


                    } else if (topic.equals(MqttEvents.Calls.value + "/" + userId)) {
                        /*
                         * For message received on the actual call event
                         */
                        if (obj.getInt("type") == 0) {
                            /*
                             * For receiving of the call request,will set mine status to be busy as well and open the incoming call screen
                             */
                            JSONObject tempObj = new JSONObject();
                            tempObj.put("status", 0);
                            /*
                             * Have to retain the message of currently being busy
                             */
                            AppController.getInstance().publish(MqttEvents.CallsAvailability.value + "/" + userId, tempObj, 0, true);
                            AppController.getInstance().setActiveOnACall(true, true);

                            CallingApis.OpenIncomingCallScreen(obj, mInstance);
                        }

                        obj.put("eventName", topic);
                        bus.post(obj);

                    } else if (topic.substring(0, 6).equals(MqttEvents.Calls.value + "A"))
                    {
                        obj.put("eventName", topic);
                        /*
                         * Will have to eventually unsubscribe from this topic,as only use  of this is to check if opponent is available to receive the call
                         */
                        bus.post(obj);
                        /*
                         * For message received on the call event to check for the availability
                         */
                        unsubscribeToTopic(topic);
                    } else if (topic.equals(MqttEvents.UserUpdates.value + "/" + userId))
                    {

                    } else if (topic.equals(MqttEvents.FetchChats.value + "/" + userId)) {
                        /*
                         * To fetch the list of the chats
                         */
                    } else if (topic.equals(MqttEvents.FetchMessages.value + "/" + userId)) {
                        /*
                         * To receive the result of the fetch messages api
                         */
                        /*
                         * First add the message received in the local db and then post on the bus
                         */
                        boolean messagesFetched = false;
                        boolean isNeedToNotifyUser = false;
                        try {
                            JSONArray messages = obj.getJSONArray("messages");
                            if (messages.length() > 0) {
                                /*
                                 * Messages in the normal or the secret chat
                                 */

                                JSONObject messageObject;

                                String receiverUid = obj.getString("opponentUid");
                                String secretId = "";
                                long dTime, expectedDTime = -1;

                                if (obj.has("secretId") && !obj.getString("secretId").isEmpty()) {
                                    secretId = obj.getString("secretId");
                                }

                                String receiverIdentifier = "";
                                Map<String, Object> chatDetails = AppController.getInstance().getDbController().
                                        getAllChatDetails(chatDocId);

                                if (chatDetails != null) {

                                    ArrayList<String> receiverUidArray = (ArrayList<String>) chatDetails.get("receiverUidArray");

                                    ArrayList<String> receiverDocIdArray = (ArrayList<String>) chatDetails.get("receiverDocIdArray");

                                    ArrayList<String> secretIdArray = (ArrayList<String>) chatDetails.get("secretIdArray");

                                    for (int i = 0; i < receiverUidArray.size(); i++) {


                                        if (receiverUidArray.get(i).equals(receiverUid) && secretIdArray.get(i).equals(secretId)) {

                                            receiverIdentifier = db.fetchReceiverIdentifierFromChatId
                                                    (receiverDocIdArray.get(i), obj.getString("chatId"));
                                            break;
                                        }

                                    }
                                }


                                if (receiverIdentifier.isEmpty()) {
                                    return;
                                }

                                boolean isSelf;
                                for (int i = 0; i < messages.length(); i++) {

                                    messageObject = messages.getJSONObject(i);

                                    String id = messageObject.getString("messageId");

                                    //solution for getting multiple mqtt response.

                                    if(i==0) {
                                        //Log.d(TAG, "messageArrived: id "+id);
                                        //Log.d(TAG, "messageArrived: lastMessageId "+lastMessageFetchMessageId);
                                        if (!TextUtils.isEmpty(lastMessageFetchMessageId)) {
                                            if (id.equals(lastMessageFetchMessageId))
                                                messagesFetched = true;
                                        }
                                        lastMessageFetchMessageId = id;
                                    }

                                    if(messagesFetched){
                                        messagesFetched = false;
                                        break;
                                    }else{
                                        isNeedToNotifyUser = true;
                                    }

                                    if (!secretId.isEmpty()) {
                                        dTime = messageObject.getLong("dTime");
                                    } else {
                                        dTime = -1;
                                    }


                                    isSelf = messageObject.getString("senderId").equals(userId);


                                    /*
                                     * For an actual message(Like text,image,video etc.) received
                                     */


                                    String messageType = messageObject.getString("messageType");
                                    String actualMessage = messageObject.getString("payload").trim();
                                    if (actualMessage.equals(AppConfig.DEFAULT_MESSAGE))
                                        continue;
                                    String timestamp = String.valueOf(messageObject.getLong("timestamp"));


                                    String mimeType = "", fileName = "", extension = "";
                                    String docIdForDoubleTickAck = messageObject.getString("toDocId");
                                    int dataSize = -1;

                                    if (messageType.equals("1") || messageType.equals("2") || messageType.equals("5") ||
                                            messageType.equals("7") || messageType.equals("9")) {
                                        dataSize = messageObject.getInt("dataSize");


                                        if (messageType.equals("9")) {


                                            mimeType = messageObject.getString("mimeType");
                                            fileName = messageObject.getString("fileName");
                                            extension = messageObject.getString("extension");


                                        }
                                    } else if (messageType.equals("10")) {

                                        String replyType = messageObject.getString("replyType");

                                        if (replyType.equals("1") || replyType.equals("2") || replyType.equals("5") || replyType.equals("7")
                                                || replyType.equals("9")) {
                                            dataSize = messageObject.getInt("dataSize");


                                            if (replyType.equals("9")) {


                                                mimeType = messageObject.getString("mimeType");
                                                fileName = messageObject.getString("fileName");
                                                extension = messageObject.getString("extension");


                                            }


                                        }
                                    }

                                    String receiverName;
                                    String userImage;

                                    receiverName = messageObject.getString("name");
                                    userImage = messageObject.getString("userImage");

                                    String documentId = AppController.getInstance().findDocumentIdOfReceiver(receiverUid, secretId);
                                    if (documentId.isEmpty()) {
                                        /*
                                         * Here, chatId is assumed to be empty
                                         */
                                        documentId = findDocumentIdOfReceiver(receiverUid, Utilities.tsInGmt(),
                                                receiverName, userImage, secretId, true, receiverIdentifier, obj.getString("chatId"), false);
                                    }

                                    db.setDocumentIdOfReceiver(documentId, docIdForDoubleTickAck, receiverUid);

                                    if (!db.checkAlreadyExists(documentId, id)) {
                                        if (messageObject.has("expectedDTime")) {
                                            expectedDTime = messageObject.getLong("expectedDTime");
                                            setTimer(documentId, id, expectedDTime);
                                        }

                                        if (messageType.equals("11")) {
                                            AppController.getInstance().putMessageInDb(receiverUid,
                                                    messageType, actualMessage, timestamp, id, documentId,

                                                    null, dataSize, dTime, receiverName, isSelf, messageObject.getInt("status"), messageObject.has("expectedDTime"), expectedDTime, Utilities.epochtoGmt(messageObject.getString("removedAt")), false);

                                        } else if (messageType.equals("1") || messageType.equals("2") || messageType.equals("7")) {

                                            AppController.getInstance().putMessageInDb(receiverUid, messageType,
                                                    actualMessage, timestamp, id, documentId, messageObject.getString("thumbnail").trim(),

                                                    dataSize, dTime, receiverName, isSelf, messageObject.getInt("status"),
                                                    messageObject.has("expectedDTime"), expectedDTime, null, false);
                                        } else if (messageType.equals("9")) {

                                            AppController.getInstance().putMessageInDb(receiverUid,
                                                    actualMessage, timestamp, id, documentId, dataSize,
                                                    dTime, isSelf, messageObject.getInt("status"), messageObject.has("expectedDTime"),
                                                    expectedDTime, mimeType, fileName, extension);


                                        } else if (messageType.equals("10")) {

                                            /*
                                             * For the reply message received
                                             */


                                            String replyType = messageObject.getString("replyType");

                                            String previousMessageType = messageObject.getString("previousType");

                                            String previousFileType = "", thumbnail = "";


                                            if (previousMessageType.equals("9")) {

                                                previousFileType = messageObject.getString("previousFileType");

                                            }
                                            if (replyType.equals("1") || replyType.equals("2") || replyType.equals("7")) {


                                                thumbnail = messageObject.getString("thumbnail").trim();

                                            }


                                            AppController.getInstance().putReplyMessageInDb(receiverUid,
                                                    actualMessage, timestamp, id, documentId,
                                                    dataSize, dTime, isSelf, messageObject.getInt("status"), messageObject.has("expectedDTime"),
                                                    expectedDTime, mimeType, fileName, extension, thumbnail, receiverName,
                                                    replyType, messageObject.getString("previousReceiverIdentifier"),
                                                    messageObject.getString("previousFrom"),
                                                    messageObject.getString("previousPayload"), messageObject.getString("previousType"),
                                                    messageObject.getString("previousId"),
                                                    previousFileType, messageObject.has("wasEdited"));


                                        } else {

                                            AppController.getInstance().putMessageInDb(receiverUid,
                                                    messageType, actualMessage, timestamp, id, documentId,


                                                    null, dataSize, dTime, receiverName, isSelf, messageObject.getInt("status"), messageObject.has("expectedDTime"), expectedDTime, null, messageObject.has("wasEdited"));


                                        }


                                        if ((messageObject.getInt("status") == 1) && isSelf && !messageType.equals("11") && (!messageType.equals("0") || !actualMessage.isEmpty())) {
                                            JSONObject obj2 = new JSONObject();
                                            obj2.put("from", AppController.getInstance().userId);
                                            obj2.put("msgIds", new JSONArray(Arrays.asList(new String[]{id})));
                                            obj2.put("doc_id", docIdForDoubleTickAck);
                                            obj2.put("to", receiverUid);

                                            obj2.put("status", "2");
                                            obj2.put("deliveryTime", Utilities.getGmtEpoch());

                                            if (!secretId.isEmpty()) {
                                                obj2.put("secretId", secretId);
                                                obj2.put("dTime", dTime);

                                            }


                                            AppController.getInstance().publish(MqttEvents.Acknowledgement.value + "/" + receiverUid, obj2, 2, false);
                                        }
                                        int type = Integer.parseInt(messageType);
                                        int replyTypeInt = -1;

                                        if (obj.has("replyType")) {
                                            replyTypeInt = Integer.parseInt(obj.getString("replyType"));
                                        }
                                        if (type == 1 || type == 2 || type == 5 || type == 7 || type == 9 ||
                                                (type == 10 && (replyTypeInt == 1 || replyTypeInt == 2 || replyTypeInt == 5
                                                        || replyTypeInt == 7 || replyTypeInt == 9))) {
                                            if (ActivityCompat.checkSelfPermission(mInstance, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                                    == PackageManager.PERMISSION_GRANTED) {


                                                if (checkWifiConnected()) {

                                                    Object[] params = new Object[8];
                                                    try {


                                                        params[0] = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");

                                                    } catch (UnsupportedEncodingException e) {

                                                    }

                                                    params[1] = messageType;
                                                    params[5] = id;
                                                    params[6] = documentId;
                                                    params[7] = receiverUid;
                                                    switch (type) {
                                                        case 1: {
                                                            if (sharedPref.getBoolean("wifiPhoto", false)) {
                                                                params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                        Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";


                                                                new DownloadMessage().execute(params);

                                                            }
                                                            break;
                                                        }

                                                        case 2: {
                                                            if (sharedPref.getBoolean("wifiVideo", false)) {
                                                                params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                        Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";


                                                                new DownloadMessage().execute(params);

                                                            }
                                                            break;
                                                        }
                                                        case 5: {
                                                            if (sharedPref.getBoolean("wifiAudio", false)) {
                                                                params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                        Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";


                                                                new DownloadMessage().execute(params);

                                                            }
                                                            break;
                                                        }

                                                        case 7: {
                                                            if (sharedPref.getBoolean("wifiPhoto", false)) {
                                                                params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                        Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                                new DownloadMessage().execute(params);

                                                            }
                                                            break;
                                                        }
                                                        case 9: {
                                                            if (sharedPref.getBoolean("wifiDocument", false)) {
                                                                params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                        + Config.CHAT_DOWNLOADS_FOLDER + fileName;


                                                                new DownloadMessage().execute(params);
                                                            }
                                                            break;


                                                        }
                                                        case 10: {


                                                            params[2] = obj.getString("replyType");

                                                            switch (Integer.parseInt((String) params[2])) {

                                                                case 1: {
                                                                    if (sharedPref.getBoolean("wifiPhoto", false)) {

                                                                        params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                        params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";
                                                                        new DownloadMessage().execute(params);
                                                                    }
                                                                    break;

                                                                }
                                                                case 2: {
                                                                    if (sharedPref.getBoolean("wifiVideo", false)) {

                                                                        params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                        params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                                                        new DownloadMessage().execute(params);
                                                                    }
                                                                    break;
                                                                }
                                                                case 5: {
                                                                    if (sharedPref.getBoolean("wifiAudio", false)) {

                                                                        params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";
                                                                        new DownloadMessage().execute(params);
                                                                    }
                                                                    break;

                                                                }
                                                                case 7: {
                                                                    if (sharedPref.getBoolean("wifiPhoto", false)) {

                                                                        params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                        params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";
                                                                        new DownloadMessage().execute(params);
                                                                    }
                                                                    break;
                                                                }
                                                                case 9: {
                                                                    if (sharedPref.getBoolean("wifiDocument", false)) {
                                                                        params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                                + Config.CHAT_DOWNLOADS_FOLDER + fileName;


                                                                        new DownloadMessage().execute(params);

                                                                    }
                                                                    break;
                                                                }
                                                            }
                                                            break;
                                                        }

                                                    }


                                                } else if (checkMobileDataOn()) {


                                                    Object[] params = new Object[8];
                                                    try {


                                                        params[0] = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");

                                                    } catch (UnsupportedEncodingException e) {

                                                    }

                                                    params[1] = messageType;
                                                    params[5] = id;
                                                    params[6] = documentId;
                                                    params[7] = receiverUid;
                                                    switch (type) {
                                                        case 1: {
                                                            if (sharedPref.getBoolean("mobilePhoto", false)) {
                                                                params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                        Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";


                                                                new DownloadMessage().execute(params);

                                                            }
                                                            break;
                                                        }
                                                        case 2: {
                                                            if (sharedPref.getBoolean("mobileVideo", false)) {
                                                                params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                        Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";


                                                                new DownloadMessage().execute(params);

                                                            }
                                                            break;
                                                        }
                                                        case 5: {
                                                            if (sharedPref.getBoolean("mobileAudio", false)) {
                                                                params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                        Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";


                                                                new DownloadMessage().execute(params);

                                                            }
                                                            break;
                                                        }

                                                        case 7: {
                                                            if (sharedPref.getBoolean("mobilePhoto", false)) {
                                                                params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                        Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";

                                                                new DownloadMessage().execute(params);

                                                            }
                                                            break;
                                                        }
                                                        case 9: {
                                                            if (sharedPref.getBoolean("mobileDocument", false)) {
                                                                params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                        + Config.CHAT_DOWNLOADS_FOLDER + fileName;


                                                                new DownloadMessage().execute(params);
                                                            }
                                                            break;


                                                        }
                                                        case 10: {


                                                            params[2] = obj.getString("replyType");

                                                            switch (Integer.parseInt((String) params[2])) {

                                                                case 1: {
                                                                    if (sharedPref.getBoolean("mobilePhoto", false)) {

                                                                        params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                        params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";
                                                                        new DownloadMessage().execute(params);
                                                                    }
                                                                    break;

                                                                }
                                                                case 2: {
                                                                    if (sharedPref.getBoolean("mobileVideo", false)) {

                                                                        params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                        params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp4";

                                                                        new DownloadMessage().execute(params);
                                                                    }
                                                                    break;
                                                                }
                                                                case 5: {
                                                                    if (sharedPref.getBoolean("mobileAudio", false)) {

                                                                        params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".mp3";
                                                                        new DownloadMessage().execute(params);
                                                                    }
                                                                    break;

                                                                }
                                                                case 7: {
                                                                    if (sharedPref.getBoolean("mobilePhoto", false)) {

                                                                        params[3] = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER + "/" + timestamp + ".jpg";
                                                                        params[4] = Environment.getExternalStorageDirectory().getPath() +
                                                                                Config.CHAT_DOWNLOADS_FOLDER + receiverUid + id + ".jpg";
                                                                        new DownloadMessage().execute(params);
                                                                    }
                                                                    break;
                                                                }
                                                                case 9: {
                                                                    if (sharedPref.getBoolean("mobileDocument", false)) {
                                                                        params[4] = Environment.getExternalStorageDirectory().getPath()
                                                                                + Config.CHAT_DOWNLOADS_FOLDER + fileName;


                                                                        new DownloadMessage().execute(params);

                                                                    }
                                                                    break;
                                                                }
                                                            }
                                                            break;
                                                        }

                                                    }


                                                }


//                                            }
                                            }
                                        }
                                    } else {


                                        if (messageType.equals("11")) {


                                            AppController.getInstance().putMessageInDb(receiverUid,
                                                    messageType, actualMessage, timestamp, id, documentId,


                                                    null, dataSize, dTime, receiverName, isSelf, messageObject.getInt("status"), messageObject.has("expectedDTime"), expectedDTime, Utilities.epochtoGmt(messageObject.getString("removedAt")), false);

                                        }


                                    }


                                    if (!secretId.isEmpty()) {


                                        if (!actualMessage.trim().isEmpty() || dTime != -1) {


                                            db.updateSecretInviteImageVisibility(documentId, false);
                                        }
                                    }
                                }
                            }
                        } catch (JSONException e) {
                        }
                        /*
                         * For callback in to activity to update UI
                         */
                        try {
                            obj.put("eventName", topic);
                            if(isNeedToNotifyUser) {
                                isNeedToNotifyUser = false;
                                bus.post(obj);
                            }
                        } catch (Exception e) {
                        }
                    }
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {

                    if (set.contains(token)) {
                        String id = null, docId = null;
                        boolean removedMessage = false;

                        int size = tokenMapping.size();
                        HashMap<String, Object> map;

                        for (int i = 0; i < size; i++) {


                            map = tokenMapping.get(i);

                            if (map.get("MQttToken").equals(token)) {

                                id = (String) map.get("messageId");
                                docId = (String) map.get("docId");
                                removedMessage = map.containsKey("messageType");

                                tokenMapping.remove(i);

                                set.remove(token);
                                break;
                            }


                        }

                        if (!removedMessage) {
                            if (docId != null && id != null) {
                                try {
                                    JSONObject obj = new JSONObject();
                                    obj.put("messageId", id);
                                    obj.put("docId", docId);
                                    obj.put("eventName", MqttEvents.MessageResponse.value);
                                    bus.post(obj);
                                } catch (JSONException e) {

                                }
                                db.updateChatUserHasDefaultMessage(docId,false);

                                db.updateMessageStatus(docId, id, 0, null);
                                db.removeUnsentMessage(AppController.getInstance().unsentMessageDocId, id);

                            }
                        } else {

                            /*
                             *Have to update only incase the message was removed before sending due to no internet
                             */
                            db.updateMessageStatus(docId, id, 0, null);
                            db.removeUnsentMessage(AppController.getInstance().unsentMessageDocId, id);
                        }
                    }
                }
            });
            mqttConnectOptions = new
                    MqttConnectOptions();
            mqttConnectOptions.setCleanSession(false);
            mqttConnectOptions.setMaxInflight(1000);
            mqttConnectOptions.setAutomaticReconnect(dataSource.isLoggedIn());
            mqttConnectOptions.setUserName(AppConfig.MqttCredendials.USERNAME);
            mqttConnectOptions.setPassword(AppConfig.MqttCredendials.PASSWORD.toCharArray());
            JSONObject obj = new JSONObject();
            try
            {
                obj.put("lastSeenEnabled", sharedPref.getBoolean("enableLastSeen", true));
                obj.put("status", 2);
                obj.put("onlineStatus", 1);
                obj.put("userId", userId);
            } catch (
                    JSONException e)
            {
            }
            Log.d(TAG, "createMQttConnection: setWill topic:  "+MqttEvents.OnlineStatus.value + "/" + userId);
            mqttConnectOptions.setWill(MqttEvents.OnlineStatus.value + "/" + userId, obj.toString().
                    getBytes(), 0, true);
            mqttConnectOptions.setKeepAliveInterval(60);
        }

        if (!serviceAlreadyScheduled) {
            serviceAlreadyScheduled = true;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                ComponentName serviceName = new ComponentName(mInstance, OreoJobService.class.getName());
                PersistableBundle extras = new PersistableBundle();
                extras.putString("command", "start");
                JobInfo jobInfo = (new JobInfo.Builder(MQTT_constants.MQTT_JOB_ID, serviceName)).setExtras(extras).setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY).setMinimumLatency(1L).setOverrideDeadline(1L).build();
                JobScheduler jobScheduler = (JobScheduler) mInstance.getSystemService(Context.JOB_SCHEDULER_SERVICE);
                try {
                    jobScheduler.schedule(jobInfo);
                } catch (IllegalArgumentException errorMessage) {
                    errorMessage.printStackTrace();
                }
            } else {
                Intent changeStatus = new Intent(mInstance, AppKilled.class);
                startService(changeStatus);
            }
        }

        if (notFromJobScheduler /*&& (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)*/) {
            connectMqttClient();
        }
        return mqttAndroidClient;
    }

    @SuppressWarnings("TryWithIdenticalCatches")
    public void publish(String topicName, JSONObject obj, int qos, boolean retained) {

        Log.d(TAG, "publish: called");
        try{
            if(topicName.contains(MqttEvents.Calls.value) && currentActiveDateId != null){
                obj.put("dateId",currentActiveDateId);
            }
        }catch (Exception e){}
        try {
            mqttAndroidClient.publish(topicName, obj.toString().getBytes(), qos, retained);

        } catch (MqttException e) {

        } catch (NullPointerException e) {

        }catch (IllegalArgumentException e){

        }

    }

    private JSONObject convertMessageToJsonObject(MqttMessage message) {

        JSONObject obj = new JSONObject();
        try {

            obj = new JSONObject(new String(message.getPayload()));
        } catch (JSONException e) {

        }
        return obj;
    }

    /*
     * To save the message received to the local couchdb for the normal and the group chat
     */
    private void putMessageInDb(String receiverUid, String messageType, String actualMessage,
                                String timestamp, String id, String receiverdocId, String thumbnailMessage,
                                int dataSize, long dTime, String senderName, boolean isSelf, int status, boolean timerStarted, long expectedDTime, String removedAt, boolean wasEdited) {

        byte[] data = Base64.decode(actualMessage, Base64.DEFAULT);

        byte[] thumbnailData = null;


        if ((messageType.equals("1")) || (messageType.equals("2")) || (messageType.equals("7"))) {

            thumbnailData = Base64.decode(thumbnailMessage, Base64.DEFAULT);


        }


        String name = timestamp;
        /*
         *
         *
         * initially in db we only store path of the thumbnail and nce downloaded we will replace that field withthe actual path of the downloaded file
         *
         *
         * */

        String tsInGmt = Utilities.epochtoGmt(timestamp);

        /*
         * Text message
         */
        if (messageType.equals("0")) {

            String text = "";
            try {

                text = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }


            boolean isDTag = false;

            if (text.trim().isEmpty()) {


                isDTag = true;

                if (dTime != -1) {

                    String message_dTime = String.valueOf(dTime);

                    for (int i = 0; i < dTimeForDB.length; i++) {
                        if (message_dTime.equals(dTimeForDB[i])) {

                            if (i == 0) {


                                text = getString(R.string.Timer_set_off);
                            } else {


                                text = getString(R.string.Timer_set_to) + " " + dTimeOptions[i];

                            }
                            break;
                        }
                    }
                } else {

                    if (isSelf) {


                        text = getResources().getString(R.string.YouInvited) + " " + senderName + " " +
                                getResources().getString(R.string.JoinSecretChat);

                    } else {
                        text = getResources().getString(R.string.youAreInvited) + " " + senderName + " " +
                                getResources().getString(R.string.JoinSecretChat);
                    }
                }

            }


            Map<String, Object> map = new HashMap<>();
            map.put("message", text);
            map.put("messageType", "0");

            map.put("isDTag", isDTag);
            if (wasEdited)
                map.put("wasEdited", true);


            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }

            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);


                db.updateChatListForNewMessage(receiverdocId, text, true, tsInGmt, tsInGmt);


            } else {


                db.addNewChatMessageAndSort(receiverdocId, map, null);

            }

        } else if (messageType.equals("1")) {
            /*
             * Image message
             */

            String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");

            Map<String, Object> map = new HashMap<>();


            try {

                map.put("message", new String(data, "UTF-8"));


            } catch (UnsupportedEncodingException e) {

            }

            map.put("messageType", "1");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            map.put("downloadStatus", 0);

            map.put("dataSize", dataSize);


            map.put("thumbnailPath", thumbnailPath);
            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }



            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);

            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }
            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewImage), true, tsInGmt, tsInGmt);

            } else {


                db.addNewChatMessageAndSort(receiverdocId, map, null);

            }

        } else if (messageType.equals("2")) {

            /*
             * Video message
             */
            String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");


            Map<String, Object> map = new HashMap<>();


            /*
             *
             *
             * message key will contail the url on server until downloaded and once downloaded
             * it will contain the local path of the video or image
             *
             * */
            try {

                map.put("message", new String(data, "UTF-8"));


            } catch (UnsupportedEncodingException e) {

            }


            map.put("messageType", "2");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            map.put("downloadStatus", 0);
            map.put("dataSize", dataSize);

            map.put("thumbnailPath", thumbnailPath);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }
            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewVideo), true, tsInGmt, tsInGmt);

            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);

            }
        } else if (messageType.equals("3")) {
            /*
             * Location message
             */

            String placeString = "";
            try {

                placeString = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }


            Map<String, Object> map = new HashMap<>();
            map.put("message", placeString);
            map.put("messageType", "3");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }
            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewLocation), true, tsInGmt, tsInGmt);
            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("4")) {
            /*
             * Contact message
             */

            String contactString = "";
            try {

                contactString = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }


            Map<String, Object> map = new HashMap<>();
            map.put("message", contactString);
            map.put("messageType", "4");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }
            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);

            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }
            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewContact), true, tsInGmt, tsInGmt);

            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("5")) {

            /*
             * Audio message
             */
            Map<String, Object> map = new HashMap<>();


            try {

                map.put("message", new String(data, "UTF-8"));


            } catch (UnsupportedEncodingException e) {

            }


            map.put("messageType", "5");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);
            map.put("downloadStatus", 0);
            map.put("dataSize", dataSize);


            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }
            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewAudio), true, tsInGmt, tsInGmt);
            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("6")) {


            /*
             * Sticker
             */

            String text = "";
            try {

                text = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }
            Map<String, Object> map = new HashMap<>();
            map.put("message", text);
            map.put("messageType", "6");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);


            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }

            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }
            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);

                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewSticker), true, tsInGmt, tsInGmt);

            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("7")) {
            /*
             * Doodle
             */
            String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");


            Map<String, Object> map = new HashMap<>();

            try {

                map.put("message", new String(data, "UTF-8"));


            } catch (UnsupportedEncodingException e) {

            }

            map.put("messageType", "7");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            map.put("downloadStatus", 0);

            map.put("dataSize", dataSize);
            map.put("thumbnailPath", thumbnailPath);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }
            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }
            thumbnailPath = null;

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewDoodle), true, tsInGmt, tsInGmt);
            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }
        } else if (messageType.equals("8")) {

            /*
             * Gif
             */


            String url = "";
            try {

                url = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }
            Map<String, Object> map = new HashMap<>();
            map.put("message", url);
            map.put("messageType", "8");
            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }
            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewGiphy), true, tsInGmt, tsInGmt);
            } else {
                db.addNewChatMessageAndSort(receiverdocId, map, null);


            }

        } else if (messageType.equals("11")) {

            String text = "";
            try {

                text = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {

            }

            Map<String, Object> map = new HashMap<>();
            map.put("message", text);
            map.put("messageType", "11");
            map.put("removedAt", removedAt);
            map.put("isDTag", false);

            map.put("isSelf", isSelf);
            map.put("from", receiverUid);
            map.put("Ts", tsInGmt);
            map.put("id", id);

            if (isSelf) {


                map.put("deliveryStatus", String.valueOf(status));


            }

            /*
             * For secret chat exclusively
             */

            map.put("dTime", dTime);
            map.put("timerStarted", timerStarted);
            if (timerStarted) {
                map.put("expectedDTime", expectedDTime);

            }

            if (status == -1) {
                db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);


                db.updateChatListForNewMessage(receiverdocId, text, true, tsInGmt, tsInGmt);


            } else {


                db.addNewChatMessageAndSort(receiverdocId, map, null);

            }

        }

    }


    private void putMessageInDb(String receiverUid, String actualMessage,
                                String timestamp, String id, String receiverdocId,
                                int dataSize, long dTime, boolean isSelf, int status, boolean timerStarted, long expectedDTime, String mimeType, String fileName, String extension) {


        /*
         * For saving the document received
         */


        byte[] data = Base64.decode(actualMessage, Base64.DEFAULT);


        String tsInGmt = Utilities.epochtoGmt(timestamp);


        /*
         * document message
         */


        Map<String, Object> map = new HashMap<>();


        try {

            map.put("message", new String(data, "UTF-8"));


        } catch (UnsupportedEncodingException e) {

        }

        map.put("messageType", "9");
        map.put("isSelf", isSelf);
        map.put("from", receiverUid);
        map.put("Ts", tsInGmt);
        map.put("id", id);


        map.put("fileName", fileName);
        map.put("mimeType", mimeType);
        map.put("extension", extension);

        map.put("downloadStatus", 0);

        map.put("dataSize", dataSize);


        if (isSelf) {


            map.put("deliveryStatus", String.valueOf(status));


        }



        /*
         * For secret chat exclusively
         */

        map.put("dTime", dTime);
        map.put("timerStarted", timerStarted);

        if (timerStarted) {
            map.put("expectedDTime", expectedDTime);

        }
        if (status == -1) {
            db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
            db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewDocument), true, tsInGmt, tsInGmt);

        } else {


            db.addNewChatMessageAndSort(receiverdocId, map, null);

        }


    }


    private void putReplyMessageInDb(String receiverUid, String actualMessage,
                                     String timestamp, String id, String receiverdocId,
                                     int dataSize, long dTime, boolean isSelf, int status, boolean timerStarted, long expectedDTime,
                                     String mimeType, String fileName, String extension,
                                     String thumbnail, String senderName, String replyType, String previousReceiverIdentifier, String previousFrom,

                                     String previousPayload, String previousType, String

                                             previousId, String previousFileType, boolean wasEdited)

    {


        String name = timestamp;
        Map<String, Object> map = new HashMap<>();
        map.put("messageType", "10");
        map.put("previousReceiverIdentifier", previousReceiverIdentifier);

        map.put("previousFrom", previousFrom);


        map.put("previousType", previousType);

        map.put("previousId", previousId);


        if (previousType.equals("9")) {

            map.put("previousFileType", previousFileType);

            map.put("previousPayload", previousPayload);
        } else if (previousType.equals("1") || previousType.equals("2") || previousType.equals("7"))

        {


            map.put("previousPayload", convertByteArrayToFile(Base64.decode(previousPayload, Base64.DEFAULT), previousId, "jpg"));
        } else {

            map.put("previousPayload", previousPayload);

        }



        /*
         * For saving the reply message received
         */


        byte[] data = Base64.decode(actualMessage, Base64.DEFAULT);

        byte[] thumbnailData = null;


        if ((replyType.equals("1")) || (replyType.equals("2")) || (replyType.equals("7"))) {


            thumbnailData = Base64.decode(thumbnail, Base64.DEFAULT);


        }



        /*
         *
         *
         * Initially in db we only store path of the thumbnail and nce downloaded we will replace that field withthe actual path of the downloaded file
         *
         *
         * */

        String tsInGmt = Utilities.epochtoGmt(timestamp);
        switch (Integer.parseInt(replyType)) {


            case 0: {
                /*
                 * Text message
                 */
                String text = "";
                try {

                    text = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }


                boolean isDTag = false;

                if (text.trim().isEmpty()) {


                    isDTag = true;

                    if (dTime != -1) {

                        String message_dTime = String.valueOf(dTime);

                        for (int i = 0; i < dTimeForDB.length; i++) {
                            if (message_dTime.equals(dTimeForDB[i])) {

                                if (i == 0) {


                                    text = getString(R.string.Timer_set_off);
                                } else {


                                    text = getString(R.string.Timer_set_to) + " " + dTimeOptions[i];

                                }
                                break;
                            }
                        }
                    } else {

                        if (isSelf) {


                            text = getResources().getString(R.string.YouInvited) + " " + senderName + " " +
                                    getResources().getString(R.string.JoinSecretChat);

                        } else {
                            text = getResources().getString(R.string.youAreInvited) + " " + senderName + " " +
                                    getResources().getString(R.string.JoinSecretChat);
                        }
                    }

                }

//                Map<String, Object> map = new HashMap<>();

                map.put("message", text);
                map.put("replyType", "0");

                map.put("isDTag", isDTag);
                if (wasEdited)
                    map.put("wasEdited", true);
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }

                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, text, true, tsInGmt, tsInGmt);


                } else {


                    db.addNewChatMessageAndSort(receiverdocId, map, null);

                }
                break;
            }

            case 1: {
                /*
                 * Image
                 */

                String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");


                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }

                map.put("replyType", "1");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);

                map.put("downloadStatus", 0);

                map.put("dataSize", dataSize);


                map.put("thumbnailPath", thumbnailPath);
                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }



                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);

                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }
                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewImage), true, tsInGmt, tsInGmt);

                } else {


                    db.addNewChatMessageAndSort(receiverdocId, map, null);

                }
                break;

            }
            case 2: {
                /*
                 * Video
                 */

                String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");





                /*
                 *
                 *
                 * message key will contail the url on server until downloaded and once downloaded
                 * it will contain the local path of the video or image
                 *
                 * */
                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }


                map.put("replyType", "2");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);

                map.put("downloadStatus", 0);
                map.put("dataSize", dataSize);

                map.put("thumbnailPath", thumbnailPath);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }
                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewVideo), true, tsInGmt, tsInGmt);

                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }
            case 3: {
                /*
                 * Location
                 */

                String placeString = "";
                try {

                    placeString = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }


                map.put("message", placeString);
                map.put("replyType", "3");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }
                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewLocation), true, tsInGmt, tsInGmt);
                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }
            case 4: {
                /*
                 * Contact
                 */
                String contactString = "";
                try {

                    contactString = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }


                map.put("message", contactString);
                map.put("replyType", "4");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }
                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);

                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }
                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewContact), true, tsInGmt, tsInGmt);

                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }

                break;

            }
            case 5: {
                /*
                 * Audio
                 */


                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }


                map.put("replyType", "5");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);
                map.put("downloadStatus", 0);
                map.put("dataSize", dataSize);


                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }
                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewAudio), true, tsInGmt, tsInGmt);
                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }
            case 6: {
                /*
                 * Sticker
                 */

                String text = "";
                try {

                    text = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }

                map.put("message", text);
                map.put("replyType", "6");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);


                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }

                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }
                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);

                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewSticker), true, tsInGmt, tsInGmt);

                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }

            case 7: {
                /*
                 * Doodle
                 */


                String thumbnailPath = convertByteArrayToFile(thumbnailData, name, "jpg");


                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }

                map.put("replyType", "7");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);

                map.put("downloadStatus", 0);

                map.put("dataSize", dataSize);
                map.put("thumbnailPath", thumbnailPath);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }
                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }
                thumbnailPath = null;

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewDoodle), true, tsInGmt, tsInGmt);
                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }


                break;

            }


            case 8: {
                /*
                 * Gif
                 */

                String url = "";
                try {

                    url = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {

                }

                map.put("message", url);
                map.put("replyType", "8");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);

                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }
                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);
                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }

                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewGiphy), true, tsInGmt, tsInGmt);
                } else {
                    db.addNewChatMessageAndSort(receiverdocId, map, null);


                }
                break;

            }
            case 9: {



                /*
                 * document message
                 */


                try {

                    map.put("message", new String(data, "UTF-8"));


                } catch (UnsupportedEncodingException e) {

                }

                map.put("replyType", "9");
                map.put("isSelf", isSelf);
                map.put("from", receiverUid);
                map.put("Ts", tsInGmt);
                map.put("id", id);


                map.put("fileName", fileName);
                map.put("mimeType", mimeType);
                map.put("extension", extension);

                map.put("downloadStatus", 0);

                map.put("dataSize", dataSize);


                if (isSelf) {


                    map.put("deliveryStatus", String.valueOf(status));


                }



                /*
                 * For secret chat exclusively
                 */

                map.put("dTime", dTime);
                map.put("timerStarted", timerStarted);

                if (timerStarted) {
                    map.put("expectedDTime", expectedDTime);

                }
                if (status == -1) {
                    db.addNewChatMessageAndSort(receiverdocId, map, tsInGmt);
                    db.updateChatListForNewMessage(receiverdocId, getString(R.string.NewDocument), true, tsInGmt, tsInGmt);

                } else {


                    db.addNewChatMessageAndSort(receiverdocId, map, null);

                }

                break;
            }
        }


    }


    public String convertByteArrayToFile(byte[] data, String name, String extension) {

        File file;

        String path = getFilesDir() + Config.CHAT_RECEIVED_THUMBNAILS_FOLDER;

        try {

            File folder = new File(path);


            if (!folder.exists() && !folder.isDirectory()) {
                folder.mkdirs();
            }


            file = new File(path, name + "." + extension);


            if (!file.exists()) {

                file.createNewFile();

            }


            FileOutputStream fos = new FileOutputStream(file);


            fos.write(data);
            fos.flush();
            fos.close();
        } catch (IOException e) {

        }

        return path + "/" + name + "." + extension;
    }

    public void connectMqttClient() {
        Log.d(TAG,"connect mqtt client called");
        try
        {
            if(!mqttAndroidClient.isConnected())
                mqttAndroidClient.connect(mqttConnectOptions, mInstance, mInstance);

        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void subscribeToTopic(String topic, int qos) {
        Log.d(TAG,"trying to subscribe"+topic+" "+flag);
        try {

            if (mqttAndroidClient != null) {
                Log.d(TAG,"actual subscribe");

                mqttAndroidClient.subscribe(topic, qos);
            }
        } catch (MqttException e) {

        }
    }

    @SuppressWarnings("TryWithIdenticalCatches")
    public void unsubscribeToTopic(String topic) {

        Log.d(TAG,"unsubscribing"+topic);
        try {

            if (mqttAndroidClient != null) {
                mqttAndroidClient.unsubscribe(topic);
            }
        } catch (MqttException e) {

        } catch (NullPointerException e) {

        }
    }

    public void updatePresence(int status, boolean applicationKilled) {
        //Log.d(TAG, "updatePresence: status: "+status+", appKilled :"+applicationKilled);
        if (signedIn) {

            if (status == 0) {
                /*
                 * Background
                 */
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("status", 0);
                    obj.put("onlineStatus", 0);


                    if (applicationKilled) {
                        if (sharedPref.getString("lastSeenTime", null) != null) {
                            obj.put("timestamp", sharedPref.getString("lastSeenTime", null));
                        } else {

                            obj.put("timestamp", Utilities.tsInGmt());
                        }

                    } else {

                        obj.put("timestamp", Utilities.tsInGmt());
                    }
                    obj.put("userId", userId);
                    obj.put("_id", userId);

                    obj.put("lastSeenEnabled", sharedPref.getBoolean("enableLastSeen", true));
                    obj.put("lastOnline", System.currentTimeMillis());

                    publish(MqttEvents.OnlineStatus.value + "/" + userId, obj, 0, true);

                } catch (JSONException w) {
                }
            } else {

                /*
                 *Foreground
                 */

//                if (!applicationKilled) {
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("status", 1);
                    obj.put("onlineStatus", 1);
                    obj.put("userId", userId);
                    obj.put("_id", userId);
                    obj.put("lastSeenEnabled", sharedPref.getBoolean("enableLastSeen", true));
                    obj.put("lastOnline", System.currentTimeMillis());

                    publish(MqttEvents.OnlineStatus.value + "/" + userId, obj, 0, true);
                } catch (JSONException w) {

                }
                //    }
            }

        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(localizationDelegate.attachBaseContext(base));
        MultiDex.install(this);
    }

    public void publishChatMessage(String topicName, JSONObject obj, int qos, boolean retained, HashMap<String, Object> map) {
        Log.d(TAG, "publishChatMessage: called");
        IMqttDeliveryToken token = null;
        try {
            obj.put("userImage", userImageUrl);

        } catch (JSONException e) {

        }


        if (mqttAndroidClient != null && mqttAndroidClient.isConnected()) {
            try {


                token = mqttAndroidClient.publish(topicName, obj.toString().getBytes(), qos, retained);


            } catch (MqttException e) {


            }


            map.put("MQttToken", token);

            tokenMapping.add(map);
            set.add(token);
        }
    }

    public boolean canPublish() {

        return mqttAndroidClient != null && mqttAndroidClient.isConnected();


    }

    public void updateTokenMapping() {


        if (signedIn) {
            db.addMqttTokenMapping(mqttTokenDocId, tokenMapping);
        }

    }

    @SuppressWarnings("TryWithIdenticalCatches")
    private void resendUnsentMessages() {

        String documentId = AppController.getInstance().unsentMessageDocId;
        if (documentId != null) {

            ArrayList<Map<String, Object>> arr = db.getUnsentMessages(documentId);
            if (arr.size() > 0) {
                String to;
                for (int i = 0; i < arr.size(); i++) {
                    Map<String, Object> map = arr.get(i);
                    JSONObject obj = new JSONObject();
                    try {
                        to = (String) map.get("to");
                        obj.put("from", userId);
                        obj.put("to", map.get("to"));
                        obj.put("receiverIdentifier", userIdentifier);

                        String type = (String) map.get("type");

                        String message = (String) map.get("message");

                        String id = (String) map.get("id");

                        String secretId = "";

                        if (map.containsKey("secretId")) {
                            secretId = (String) map.get("secretId");
                        }

                        obj.put("name", map.get("name"));
                        obj.put("userImage", map.get("userImage"));
                        obj.put("toDocId", map.get("toDocId"));
                        obj.put("id", id);
                        obj.put("type", type);

                        if (!secretId.isEmpty()) {
                            obj.put("secretId", secretId);
                            obj.put("dTime", map.get("dTime"));

                        }

                        obj.put("timestamp", map.get("timestamp"));
                        HashMap<String, Object> mapTemp = new HashMap<>();
                        mapTemp.put("messageId", id);
                        mapTemp.put("docId", map.get("toDocId"));


                        if (type.equals("0")) {
                            /*
                             * Text message
                             */
                            try {
                                obj.put("payload", Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT).trim());

                            } catch (UnsupportedEncodingException e) {
                            }

                            AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                            String tsInGmt = Utilities.tsInGmt();
                            /*
                             * Have been intentionally made to query again to avoid the case of chat being deleted b4 message is send
                             */
                            String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                            db.updateMessageTs(docId, id, tsInGmt);
                        } else if (type.equals("1")) {
                            /*
                             * Image message
                             */
                            Uri uri = null;
                            Bitmap bm = null;
                            try {
                                final BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inJustDecodeBounds = true;
                                BitmapFactory.decodeFile(message, options);

                                int height = options.outHeight;
                                int width = options.outWidth;

                                float density = AppController.getInstance().getResources().getDisplayMetrics().density;
                                int reqHeight;

                                reqHeight = (int) ((150 * density) * (height / width));
                                bm = AppController.getInstance().decodeSampledBitmapFromResource(message, (int) (150 * density), reqHeight);

                                if (bm != null) {

                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                    bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);
                                    byte[] b = baos.toByteArray();
                                    try {
                                        baos.close();
                                    } catch (IOException e) {

                                    }
                                    baos = null;

                                    File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".jpg");
                                    b = null;
                                    uri = Uri.fromFile(f);
                                    f = null;

                                }

                            } catch (OutOfMemoryError e) {

                            } catch (Exception e) {

                                /*
                                 *
                                 * to handle the file not found exception
                                 *
                                 *
                                 * */


                            }


                            if (uri != null) {

                                /*
                                 *
                                 *
                                 * make thumbnail
                                 *
                                 * */

                                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                bm.compress(Bitmap.CompressFormat.JPEG, 1, baos);

                                bm = null;
                                byte[] b = baos.toByteArray();

                                try {
                                    baos.close();
                                } catch (IOException e) {

                                }
                                baos = null;

                                obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT));

                                AppController.getInstance().uploadFile(uri,
                                        AppController.getInstance().userId + id, 1, obj, (String) map.get("to"),
                                        id, mapTemp, secretId, null, true, map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));

                                uri = null;
                                b = null;
                                bm = null;
                            }


                        } else if (type.equals("2")) {
                            /*
                             * Video message
                             */


                            Uri uri = null;

                            try {
                                File video = new File(message);


                                if (video.exists())
                                {

                                    byte[] b = convertFileToByteArray(video);
                                    video = null;

                                    File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".mp4");
                                    b = null;

                                    uri = Uri.fromFile(f);
                                    f = null;

                                    b = null;

                                    if (uri != null) {

                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                        Bitmap bm = ThumbnailUtils.createVideoThumbnail(message,
                                                MediaStore.Images.Thumbnails.MINI_KIND);

                                        if (bm != null) {

                                            bm.compress(Bitmap.CompressFormat.JPEG, 1, baos);
                                            bm = null;
                                            b = baos.toByteArray();
                                            try {
                                                baos.close();
                                            } catch (IOException e) {

                                            }
                                            baos = null;


                                            obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT));


                                            AppController.getInstance().uploadFile(uri,
                                                    AppController.getInstance().userId + id, 2, obj,
                                                    (String) map.get("to"), id, mapTemp, secretId, null, true, map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));
                                            uri = null;
                                            b = null;

                                        }
                                    }

                                }


                            } catch (OutOfMemoryError e) {

                            } catch (Exception e) {

                                /*
                                 *
                                 * to handle the file not found exception incase file has not been found
                                 *
                                 * */


                            }


                        } else if (type.equals("3")) {

                            /*
                             * Location message
                             */

                            try {
                                obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                            } catch (UnsupportedEncodingException e) {

                            }
                            AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                            String tsInGmt = Utilities.tsInGmt();
                            String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"),
                                    secretId);
                            db.updateMessageTs(docId, id, tsInGmt);
                        } else if (type.equals("4")) {

                            /*
                             * Contact message
                             */

                            try {

                                obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                            } catch (UnsupportedEncodingException e) {

                            }

                            AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                            String tsInGmt = Utilities.tsInGmt();
                            String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                            db.updateMessageTs(docId, id, tsInGmt);
                        } else if (type.equals("5")) {

                            /*
                             * Audio message
                             */

                            Uri uri;
                            try {
                                File audio = new File(message);

                                if (audio.exists()) {

                                    byte[] b = convertFileToByteArray(audio);
                                    audio = null;

                                    File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".mp3");
                                    b = null;

                                    uri = Uri.fromFile(f);
                                    f = null;

                                    b = null;


                                    if (uri != null) {

                                        AppController.getInstance().uploadFile(uri, AppController.getInstance().userId + id,
                                                5, obj, (String) map.get("to"), id, mapTemp, secretId, null,
                                                map.containsKey("toDelete"), map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));
                                    }

                                }
                            } catch (Exception e) {

                                /*
                                 *
                                 * to handle the file not found exception incase file has not been found
                                 *
                                 * */
                            }
                        } else if (type.equals("6")) {
                            /*
                             * Sticker
                             */
                            try {
                                obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                            } catch (UnsupportedEncodingException e) {

                            }

                            AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);
                            String tsInGmt = Utilities.tsInGmt();
                            String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                            db.updateMessageTs(docId, id, tsInGmt);

                        } else if (type.equals("7")) {
                            /*
                             *Doodle
                             */

                            Uri uri = null;
                            Bitmap bm = null;


                            try {

                                final BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inJustDecodeBounds = true;
                                BitmapFactory.decodeFile(message, options);


                                int height = options.outHeight;
                                int width = options.outWidth;

                                float density = AppController.getInstance().getResources().getDisplayMetrics().density;
                                int reqHeight;


                                reqHeight = (int) ((150 * density) * (height / width));

                                bm = AppController.getInstance().decodeSampledBitmapFromResource(message, (int) (150 * density), reqHeight);


                                if (bm != null) {


                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                    bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                    byte[] b = baos.toByteArray();

                                    try {
                                        baos.close();
                                    } catch (IOException e) {

                                    }
                                    baos = null;


                                    File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".jpg");
                                    b = null;

                                    uri = Uri.fromFile(f);
                                    f = null;


                                }


                            } catch (OutOfMemoryError e) {


                            } catch (Exception e) {

                                /*
                                 *
                                 * to handle the file not found exception
                                 *
                                 *
                                 * */


                            }


                            if (uri != null) {


                                /*
                                 *
                                 *
                                 * make thumbnail
                                 *
                                 * */

                                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                bm.compress(Bitmap.CompressFormat.JPEG, 1, baos);


                                bm = null;
                                byte[] b = baos.toByteArray();

                                try {
                                    baos.close();
                                } catch (IOException e) {

                                }
                                baos = null;


                                obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT));


                                AppController.getInstance().uploadFile(uri, AppController.getInstance().userId + id, 7, obj,
                                        (String) map.get("to"), id, mapTemp, secretId, null, true, map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));

                                uri = null;
                                b = null;
                                bm = null;
                            }

                        } else if (type.equals("8")) {
                            /*
                             *Gif
                             */

                            try {
                                obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                            } catch (UnsupportedEncodingException e) {

                            }

                            AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);

                            String tsInGmt = Utilities.tsInGmt();
                            String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                            db.updateMessageTs(docId, id, tsInGmt);

                        } else if (type.equals("9")) {

                            /*
                             * Document
                             */


                            Uri uri;
                            try {

                                File document = new File(message);


                                if (document.exists()) {


                                    uri = Uri.fromFile(document);


                                    document = null;


                                    if (uri != null) {


                                        AppController.getInstance().uploadFile(uri, AppController.getInstance().userId + id,
                                                9, obj, (String) map.get("to"), id, mapTemp, secretId,

                                                (String) map.get("extension"), false, map.containsKey("isGroupMessage"),
                                                map.get("groupMembersDocId"));
                                    }


                                }
                            } catch (Exception e) {


                                /*
                                 *
                                 * to handle the file not found exception incase file has not been found
                                 *
                                 * */


                            }
                        } else if (type.equals("10")) {
                            /*
                             * Reply message
                             */
                            String replyType = (String) map.get("replyType");


                            String previousType = (String) map.get("previousType");


                            obj.put("replyType", replyType);


                            obj.put("previousFrom", map.get("previousFrom"));

                            obj.put("previousType", previousType);

                            obj.put("previousId", map.get("previousId"));

                            obj.put("previousReceiverIdentifier", map.get("previousReceiverIdentifier"));
                            if (previousType.equals("9")) {

                                obj.put("previousFileType", map.get("previousFileType"));
                            }

                            if (previousType.equals("1") || previousType.equals("2") || previousType.equals("7"))

                            {
                                switch (Integer.parseInt(previousType)) {

                                    case 1: {


                                        /*
                                         *Image
                                         */
                                        Bitmap bm = decodeSampledBitmapFromResource((String) map.get("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {

                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());


                                        break;

                                    }
                                    case 2: {

                                        /*
                                         * Video
                                         */
                                        Bitmap bm = ThumbnailUtils.createVideoThumbnail((String) map.get("previousPayload"),
                                                MediaStore.Images.Thumbnails.MINI_KIND);


                                        bm = Bitmap.createScaledBitmap(bm, 180, 180, false);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {

                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;

                                    }
                                    case 7: {
                                        /*
                                         * Doodle
                                         */


                                        Bitmap bm = decodeSampledBitmapFromResource((String) map.get("previousPayload"), 180, 180);


                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {

                                        }
                                        baos = null;


                                        obj.put("previousPayload", Base64.encodeToString(b, Base64.DEFAULT).trim());
                                        break;
                                    }


                                }

                            } else {


                                obj.put("previousPayload", map.get("previousPayload"));

                            }


                            switch (Integer.parseInt(replyType)) {

                                case 0: {
                                    /*
                                     * Text
                                     */

                                    try {


                                        obj.put("payload", Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT).trim());

                                    } catch (UnsupportedEncodingException e) {

                                    }

                                    AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);

                                    String tsInGmt = Utilities.tsInGmt();
                                    String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                                    db.updateMessageTs(docId, id, tsInGmt);
                                    break;
                                }
                                case 1: {
                                    /*
                                     * Image
                                     */

                                    Uri uri = null;
                                    Bitmap bm = null;


                                    try {

                                        final BitmapFactory.Options options = new BitmapFactory.Options();
                                        options.inJustDecodeBounds = true;
                                        BitmapFactory.decodeFile(message, options);


                                        int height = options.outHeight;
                                        int width = options.outWidth;

                                        float density = AppController.getInstance().getResources().getDisplayMetrics().density;
                                        int reqHeight;


                                        reqHeight = (int) ((150 * density) * (height / width));

                                        bm = AppController.getInstance().decodeSampledBitmapFromResource(message, (int) (150 * density), reqHeight);


                                        if (bm != null) {


                                            ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                            bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                            byte[] b = baos.toByteArray();

                                            try {
                                                baos.close();
                                            } catch (IOException e) {

                                            }
                                            baos = null;


                                            File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".jpg");
                                            b = null;

                                            uri = Uri.fromFile(f);
                                            f = null;


                                        }


                                    } catch (OutOfMemoryError e) {


                                    } catch (Exception e) {

                                        /*
                                         *
                                         * to handle the file not found exception
                                         *
                                         *
                                         * */


                                    }


                                    if (uri != null) {


                                        /*
                                         *
                                         *
                                         * make thumbnail
                                         *
                                         * */

                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 1, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {

                                        }
                                        baos = null;


                                        obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT));


                                        AppController.getInstance().uploadFile(uri,
                                                AppController.getInstance().userId + id, 1, obj, (String) map.get("to"),
                                                id, mapTemp, secretId, null, true, map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));

                                        uri = null;
                                        b = null;
                                        bm = null;
                                    }

                                    break;
                                }
                                case 2: {
                                    /*
                                     * Video
                                     */
                                    Uri uri = null;

                                    try {
                                        File video = new File(message);


                                        if (video.exists())


                                        {

                                            byte[] b = convertFileToByteArray(video);
                                            video = null;


                                            File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".mp4");
                                            b = null;

                                            uri = Uri.fromFile(f);
                                            f = null;

                                            b = null;

                                            if (uri != null) {


                                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                                Bitmap bm = ThumbnailUtils.createVideoThumbnail(message,
                                                        MediaStore.Images.Thumbnails.MINI_KIND);

                                                if (bm != null) {

                                                    bm.compress(Bitmap.CompressFormat.JPEG, 1, baos);
                                                    bm = null;
                                                    b = baos.toByteArray();
                                                    try {
                                                        baos.close();
                                                    } catch (IOException e) {

                                                    }
                                                    baos = null;


                                                    obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT));


                                                    AppController.getInstance().uploadFile(uri,
                                                            AppController.getInstance().userId + id, 2, obj,
                                                            (String) map.get("to"), id, mapTemp, secretId, null, true,
                                                            map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));
                                                    uri = null;
                                                    b = null;

                                                }
                                            }

                                        }


                                    } catch (OutOfMemoryError e) {

                                    } catch (Exception e) {


                                        /*
                                         *
                                         * to handle the file not found exception incase file has not been found
                                         *
                                         * */


                                    }


                                    break;
                                }
                                case 3: {
                                    /*
                                     * Location
                                     */
                                    try {
                                        obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                                    } catch (UnsupportedEncodingException e) {

                                    }

                                    AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);

                                    String tsInGmt = Utilities.tsInGmt();
                                    String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"),
                                            secretId);
                                    db.updateMessageTs(docId, id, tsInGmt);

                                    break;
                                }
                                case 4: {
                                    /*
                                     * Contact
                                     */

                                    try {


                                        obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                                    } catch (UnsupportedEncodingException e) {

                                    }

                                    AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);

                                    String tsInGmt = Utilities.tsInGmt();
                                    String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                                    db.updateMessageTs(docId, id, tsInGmt);
                                    break;
                                }
                                case 5: {
                                    /*
                                     * Audio
                                     */

                                    Uri uri;
                                    try {

                                        File audio = new File(message);


                                        if (audio.exists()) {


                                            byte[] b = convertFileToByteArray(audio);
                                            audio = null;


                                            File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".mp3");
                                            b = null;

                                            uri = Uri.fromFile(f);
                                            f = null;

                                            b = null;


                                            if (uri != null) {


                                                AppController.getInstance().uploadFile(uri, AppController.getInstance().userId + id,
                                                        5, obj, (String) map.get("to"), id, mapTemp, secretId,
                                                        null, map.containsKey("toDelete"), map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));
                                            }


                                        }
                                    } catch (Exception e) {


                                        /*
                                         *
                                         * to handle the file not found exception incase file has not been found
                                         *
                                         * */


                                    }
                                    break;
                                }
                                case 6: {
                                    /*
                                     * Sticker
                                     */
                                    try {
                                        obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                                    } catch (UnsupportedEncodingException e) {

                                    }

                                    AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);

                                    String tsInGmt = Utilities.tsInGmt();
                                    String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                                    db.updateMessageTs(docId, id, tsInGmt);


                                    break;
                                }
                                case 7: {
                                    /*
                                     * Doodle
                                     */
                                    Uri uri = null;
                                    Bitmap bm = null;


                                    try {

                                        final BitmapFactory.Options options = new BitmapFactory.Options();
                                        options.inJustDecodeBounds = true;
                                        BitmapFactory.decodeFile(message, options);


                                        int height = options.outHeight;
                                        int width = options.outWidth;

                                        float density = AppController.getInstance().getResources().getDisplayMetrics().density;
                                        int reqHeight;


                                        reqHeight = (int) ((150 * density) * (height / width));

                                        bm = AppController.getInstance().decodeSampledBitmapFromResource(message, (int) (150 * density), reqHeight);


                                        if (bm != null) {


                                            ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                            bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);


                                            byte[] b = baos.toByteArray();

                                            try {
                                                baos.close();
                                            } catch (IOException e) {

                                            }
                                            baos = null;


                                            File f = AppController.getInstance().convertByteArrayToFileToUpload(b, id, ".jpg");
                                            b = null;

                                            uri = Uri.fromFile(f);
                                            f = null;


                                        }


                                    } catch (OutOfMemoryError e) {


                                    } catch (Exception e) {

                                        /*
                                         *
                                         * to handle the file not found exception
                                         *
                                         *
                                         * */


                                    }


                                    if (uri != null) {


                                        /*
                                         *
                                         *
                                         * Make thumbnail
                                         *
                                         * */

                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 1, baos);


                                        bm = null;
                                        byte[] b = baos.toByteArray();

                                        try {
                                            baos.close();
                                        } catch (IOException e) {

                                        }
                                        baos = null;


                                        obj.put("thumbnail", Base64.encodeToString(b, Base64.DEFAULT));


                                        AppController.getInstance().uploadFile(uri, AppController.getInstance().userId + id, 7, obj,
                                                (String) map.get("to"), id, mapTemp, secretId, null, true, map.containsKey("isGroupMessage"), map.get("groupMembersDocId"));

                                        uri = null;
                                        b = null;
                                        bm = null;
                                    }

                                    break;
                                }
                                case 8: {
                                    /*
                                     * Gif
                                     */

                                    try {
                                        obj.put("payload", (Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT)).trim());

                                    } catch (UnsupportedEncodingException e) {
                                    }

                                    AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);

                                    String tsInGmt = Utilities.tsInGmt();
                                    String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                                    db.updateMessageTs(docId, id, tsInGmt);
                                    break;
                                }
                                case 9: {

                                    /*
                                     * Document
                                     */
                                    Uri uri;
                                    try {

                                        File document = new File(message);


                                        if (document.exists()) {


                                            uri = Uri.fromFile(document);


                                            document = null;


                                            if (uri != null) {


                                                AppController.getInstance().uploadFile(uri, AppController.getInstance().userId + id,
                                                        9, obj, (String) map.get("to"), id, mapTemp, secretId,
                                                        (String) map.get("extension"), false, map.containsKey("isGroupMessage"),
                                                        map.get("groupMembersDocId"));
                                            }


                                        }
                                    } catch (Exception e) {


                                        /*
                                         *
                                         * to handle the file not found exception incase file has not been found
                                         *
                                         * */


                                    }

                                    break;
                                }

                            }


                        } else if (type.equals("11")) {


                            /*
                             * Remove message
                             */
                            try {


                                obj.put("payload", Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT).trim());


                                obj.put("removedAt", map.get("timestamp"));


                            } catch (UnsupportedEncodingException e) {

                            }

                            AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);

                        } else if (type.equals("12")) {

                            /*
                             * Edit message
                             */
                            try {

                                obj.put("editedAt", map.get("timestamp"));
                                obj.put("payload", Base64.encodeToString(message.getBytes("UTF-8"), Base64.DEFAULT).trim());

                            } catch (UnsupportedEncodingException e) {

                            }

                            AppController.getInstance().publishChatMessage(MqttEvents.Message.value + "/" + to, obj, 1, false, mapTemp);

                            String tsInGmt = Utilities.tsInGmt();

                            /*
                             * Have been intentionally made to query again to avoid the case of chat being deleted b4 message is send
                             */
                            String docId = AppController.getInstance().findDocumentIdOfReceiver((String) map.get("to"), secretId);
                            db.updateMessageTs(docId, id, tsInGmt);
                        }

                    } catch (JSONException e) {

                    } catch (OutOfMemoryError e) {


                    }

                }


            }


        }

    }

    public void disconnect() {
        try {
            if (mqttAndroidClient != null)
                mqttAndroidClient.disconnect();
        } catch (Exception e) {

        }
    }

    /**
     * To generate the push notifications locally
     */

    @SuppressWarnings("unchecked")
    private void generatePushNotificationLocal(String notificationId, String messageType, String senderName, String actualMessage, Intent intent, long dTime, String secretId, String receiverUid, String replyType) {

//        if(true)
//            return;

        if ((!foreground) || (activeReceiverId.isEmpty()) || (!(activeReceiverId.equals(receiverUid))) || (!(activeReceiverId.equals(receiverUid) && activeSecretId.equals(secretId)))) {
            try {

                String pushMessage = "";
                switch (Integer.parseInt(messageType)) {


                    case 0:

                    {
                        try {
                            pushMessage = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");


                            if (pushMessage.trim().isEmpty()) {


                                /*
                                 *Intentionally written the code twice,as this is not the common case
                                 */

                                if (dTime != -1) {
                                    String message_dTime = String.valueOf(dTime);

                                    for (int i = 0; i < dTimeForDB.length; i++) {
                                        if (message_dTime.equals(dTimeForDB[i])) {

                                            if (i == 0) {


                                                pushMessage = getString(R.string.Timer_set_off);
                                            } else {


                                                pushMessage = getString(R.string.Timer_set_to) + " " + dTimeOptions[i];

                                            }
                                            break;
                                        }
                                    }
                                } else {

                                    pushMessage = getResources().getString(R.string.youAreInvited) + " " + senderName + " " +
                                            getResources().getString(R.string.JoinSecretChat);
                                }

                            }


                        } catch (UnsupportedEncodingException e) {

                        }
                        break;
                    }
                    case 1: {

                        pushMessage = "Image";
                        break;
                    }

                    case 2: {
                        pushMessage = "Video";
                        break;
                    }
                    case 3: {
                        pushMessage = "Location";

                        break;
                    }
                    case 4: {
                        pushMessage = "Contact";

                        break;
                    }
                    case 5: {
                        pushMessage = "Audio";

                        break;
                    }
                    case 6: {

                        pushMessage = "Sticker";

                        break;
                    }
                    case 7: {

                        pushMessage = "Doodle";

                        break;

                    }
                    case 8: {

                        pushMessage = "Gif";

                        break;
                    }


                    case 10: {

                        switch (Integer.parseInt(replyType)) {


                            case 0:

                            {
                                try {
                                    pushMessage = new String(Base64.decode(actualMessage, Base64.DEFAULT), "UTF-8");


                                } catch (UnsupportedEncodingException e) {

                                }
                                break;
                            }
                            case 1: {

                                pushMessage = "Image";
                                break;
                            }

                            case 2: {
                                pushMessage = "Video";
                                break;
                            }
                            case 3: {
                                pushMessage = "Location";

                                break;
                            }
                            case 4: {
                                pushMessage = "Contact";

                                break;
                            }
                            case 5: {
                                pushMessage = "Audio";

                                break;
                            }
                            case 6: {

                                pushMessage = "Sticker";

                                break;
                            }
                            case 7: {

                                pushMessage = "Doodle";

                                break;

                            }
                            case 8: {

                                pushMessage = "Gif";

                                break;
                            }
                            default: {
                                pushMessage = "Document";
                            }
                        }
                        break;
                    }
                    default: {
                        pushMessage = "Document";
                    }


                }


                /*
                 * For clubbing of the notifications
                 */

                NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();


                Map<String, Object> notificationInfo = fetchNotificationInfo(notificationId);


                int unreadMessageCount;
                int systemNotificationId;

                ArrayList<String> messages;
                if (notificationInfo == null) {

                    /*
                     * No previous notifications for the chat
                     *
                     */
                    notificationInfo = new HashMap<>();
                    messages = new ArrayList<>();

                    messages.add(pushMessage);
                    notificationInfo.put("notificationMessages", messages);

                    notificationInfo.put("notificationId", notificationId);

                    systemNotificationId = Integer.parseInt(String.valueOf(System.currentTimeMillis()).substring(9));
                    notificationInfo.put("systemNotificationId", systemNotificationId);


                    unreadMessageCount = 0;
                } else {
                    messages = (ArrayList<String>) notificationInfo.get("notificationMessages");
                    messages.add(0, pushMessage);

                    if (messages.size() > NOTIFICATION_SIZE) {
                        messages.remove(messages.size() - 1);
                    }
                    systemNotificationId = (int) notificationInfo.get("systemNotificationId");
                    notificationInfo.put("notificationMessages", messages);
                    unreadMessageCount = (int) notificationInfo.get("messagesCount");

                }
                notificationInfo.put("messagesCount", unreadMessageCount + 1);
                addOrUpdateNotification(notificationInfo, notificationId);


                for (int i = 0; i < messages.size(); i++) {


                    inboxStyle.addLine(messages.get(i));


                }

                if (unreadMessageCount > (NOTIFICATION_SIZE - 1)) {
                    inboxStyle.setSummaryText("+" + (unreadMessageCount - (NOTIFICATION_SIZE - 1)) + " " + getString(R.string.more_message));
                }

                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                stackBuilder.addParentStack(ChatMessageActivity.class);

                Bundle bundle = new Bundle();
                //to navigate to match/chat tab.
                bundle.putInt("type", 1);
                intent.putExtra("data",bundle);
                stackBuilder.addNextIntent(intent);
                PendingIntent pendingIntent = stackBuilder.getPendingIntent(systemNotificationId,PendingIntent.FLAG_ONE_SHOT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                String title, tickerText;
                if (messages.size() == 1) {

                    pushMessage = senderName + ": " + pushMessage;
                    tickerText = pushMessage;
                    title = getString(R.string.app_name);
                } else {
                    tickerText = senderName + ": " + pushMessage;
                    title = senderName;
                }


                inboxStyle.setBigContentTitle(title);
                NotificationCompat.Builder
                        notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.mipmap.ic_launcher))
                        .setContentTitle(title)

                        .setContentText(pushMessage)
                        .setTicker(tickerText)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setStyle(inboxStyle)
                        .setDefaults(Notification.DEFAULT_VIBRATE)
                        .setPriority(Notification.PRIORITY_HIGH);

                if (Build.VERSION.SDK_INT >= 21) notificationBuilder.setVibrate(new long[0]);

                if(notificationManager == null)
                    notificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                /*
                 *
                 * Notification id is used to notify the same notification
                 *
                 * */
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
                {
                    int importance = NotificationManager.IMPORTANCE_HIGH;
                    NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_CHAT, NOTIFICATION_CHANNEL_NAME_CHAT_NOTIFICATION, importance);
                    notificationChannel.enableLights(true);
                    notificationChannel.setLightColor(Color.RED);
                    notificationChannel.enableVibration(true);
                    notificationChannel.setVibrationPattern(new long[]{100});
                    assert notificationManager != null;
                    notificationBuilder.setChannelId(NOTIFICATION_CHANNEL_ID_CHAT);
                    notificationManager.createNotificationChannel(notificationChannel);
                }
                notificationManager.notify(notificationId, systemNotificationId, notificationBuilder.build());

            } catch (Exception e) {

            }
        }

    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

        activeActivitiesCount++;
        sAnalytics = GoogleAnalytics.getInstance(this);
        /*
         *
         * As app can also be started when clicked on the chat notification
         *
         *
         * */
        if (AppController.getInstance().getSignedIn() && activity.getClass().getSimpleName().equals("HomeActivity")) {

//            sendAcknowledgements();
            foreground = true;

            setApplicationKilled(false);
            updatePresence(1, false);

            /*
             *
             * When the app started,update the status(JUST have put it for checking)
             *
             */

            //  if (signedIn) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("status", 1);
                publish(MqttEvents.CallsAvailability.value + "/" + userId, obj, 0, true);
                AppController.getInstance().setActiveOnACall(false, true);


            } catch (JSONException e) {

            }
            //      }
        }


    }

    @Override
    public void onActivityDestroyed(Activity activity) {

        activeActivitiesCount--;
    }

    @Override
    public void onActivityPaused(Activity activity) {


    }

    @Override
    public void onActivityResumed(Activity activity) {


    }

    @Override
    public void onActivitySaveInstanceState(Activity activity,
                                            Bundle outState) {

    }
    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {


    }

    public void updateReconnected() {
        Log.d("log55", "reconnection");

        if (!applicationKilled) {
            updatePresence(1, false);
        } else {

            updatePresence(0, true);
        }
        try {
            JSONObject obj = new JSONObject();
            obj.put("eventName", MqttEvents.Connect.value);

            bus.post(obj);
        } catch (Exception e) {

        }

        if (flag) {
            flag = false;
            subscribeToTopic(userId,1);
            subscribeToTopic(MqttEvents.Message.value + "/" + userId, 1);
            subscribeToTopic(MqttEvents.Acknowledgement.value + "/" + userId, 2);
            subscribeToTopic(MqttEvents.Calls.value + "/" + userId, 0);
            subscribeToTopic(MqttEvents.UserUpdates.value + "/" + userId, 1);
            subscribeToTopic(MqttEvents.FetchMessages.value + "/" + userId, 1);
        }

        if (signedIn) {
            try {
                JSONObject obj = new JSONObject();
                obj.put("status", 1);
                publish(MqttEvents.CallsAvailability.value + "/" + userId, obj, 0, true);
                AppController.getInstance().setActiveOnACall(false, true);

            } catch (JSONException e) { }
        }

        resendUnsentMessages();
    }

    public void updateLastSeenSettings(boolean lastSeenSetting) {


        sharedPref.edit().putBoolean("enableLastSeen", lastSeenSetting).apply();
    }

    @SuppressWarnings("unchecked")
    public void getCurrentTime() {

        new FetchTime().execute();
    }

    public void cutCallOnKillingApp(boolean appCrashed) {
        /*
         * Have to make myself available and inform  other user of the call getting cut
         */
        try {
            if (activeCallId != null && activeCallerId != null) {
                JSONObject obj = new JSONObject();
                obj.put("callId", activeCallId);
                obj.put("userId", activeCallerId);
                obj.put("type", 2);
                AppController.getInstance().publish(MqttEvents.Calls.value + "/" + activeCallerId, obj, 0, false);

                if (appCrashed) {
                    JSONObject obj2 = new JSONObject();
                    obj2.put("eventName", "appCrashed");
                    bus.post(obj2);
                }
            }
            JSONObject obj = new JSONObject();
            obj.put("status", 1);

            AppController.getInstance().publish(MqttEvents.CallsAvailability.value + "/" + AppController.getInstance().getUserId(), obj, 0, true);
            AppController.getInstance().setActiveOnACall(false, true);
        } catch (JSONException e) {
        }
    }


    public String randomString() {
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        sb.append("PnPLabs3Embed");
        return sb.toString();
    }

    public void setTimer(String documentId, String messageId, long expectedDTime) {
        if (expectedDTime > 0) {
            long temp = Utilities.getGmtEpoch();
            db.setTimerStarted(documentId, messageId, temp + expectedDTime);
        }
    }


    /**
     * Convert image or video or audio to byte[] so that it can be send on socket(Unsetn messages)
     */
    @SuppressWarnings("TryWithIdenticalCatches")
    private static byte[] convertFileToByteArray(File f) {
        byte[] byteArray = null;
        byte[] b;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {

            InputStream inputStream = new FileInputStream(f);
            b = new byte[2663];

            int bytesRead;

            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }

            byteArray = bos.toByteArray();
        } catch (IOException e) {

        } catch (OutOfMemoryError e) {

        } finally {
            b = null;

            try {
                bos.close();

            } catch (IOException e) {

            }

        }

        return byteArray;
    }


    /**
     *
     * To find the document id of the receiver on receipt of new message,if exists or create a new document for chat with that receiver and return its document id
     */
    @SuppressWarnings("unchecked")
    public static String findDocumentIdOfReceiver(String receiverUid, String timestamp, String receiverName,
                                                  String receiverImage, String secretId, boolean invited,
                                                  String receiverIdentifier, String chatId, boolean groupChat)
    {
        CouchDbController db = AppController.getInstance().getDbController();
        Map<String, Object> chatDetails = db.getAllChatDetails(AppController.getInstance().getChatDocId());

        if (chatDetails != null) {

            ArrayList<String> receiverUidArray = (ArrayList<String>) chatDetails.get("receiverUidArray");

            ArrayList<String> receiverDocIdArray = (ArrayList<String>) chatDetails.get("receiverDocIdArray");

            ArrayList<String> secretIdArray = (ArrayList<String>) chatDetails.get("secretIdArray");

            for (int i = 0; i < receiverUidArray.size(); i++) {
                if (receiverUidArray.get(i).equals(receiverUid) && secretIdArray.get(i).equals(secretId)) {

                    return receiverDocIdArray.get(i);
                }
            }
        }

        /*  here we also need to enter receiver name*/

        String docId = db.createDocumentForChat(timestamp, receiverUid, receiverName, receiverImage, secretId, invited,
                receiverIdentifier, chatId, groupChat);

        db.addChatDocumentDetails(receiverUid, docId, AppController.getInstance().getChatDocId(), secretId);

        return docId;
    }

    public boolean isApplicationKilled() {
        return applicationKilled;
    }

    //-----------------------------
    public void refreshMediaGallery(String path) {

        for (int i = 0; i < 17; i++) {


            try {

                MediaScannerConnection.scanFile(this, new String[]{path + (i) + ".jpg"}, null, new MediaScannerConnection.OnScanCompletedListener() {
                    /*
                     *   (non-Javadoc)
                     * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                     */
                    public void onScanCompleted(String path, Uri uri) {


                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void notifyOnImageDeleteRequired(String tempPath) {
        try {

            MediaScannerConnection.scanFile(this, new String[]{tempPath}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Integer> getExcludedFilterIds() {
        return excludedIds;
    }

    public void setExcludedFilterIds(ArrayList<Integer> excludedIds) {
        this.excludedIds = excludedIds;
        //db.updateExcludedFilters(userDocId, excludedIds);
    }

    public boolean isFiltersUpdated() {
        return filtersUpdated;
    }

    public void setFiltersUpdated(boolean filtersUpdated) {
        this.filtersUpdated = filtersUpdated;
    }


    @SuppressWarnings("TryWithIdenticalCatches")
    private class FetchTime extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {

            sharedPref.edit().putBoolean("deltaRequired", true).apply();
            String url_ping = "https://google.com/";
            URL url = null;
            try {
                url = new URL(url_ping);

            } catch (MalformedURLException e) {

            }
            try {
                /*
                 * Maybe inaccurate due to network inaccuracy
                 */
                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                if (urlc.getResponseCode() == 200 || urlc.getResponseCode() == 503) {
                    long dateStr = urlc.getDate();
                    //Here I do something with the Date String

                    timeDelta = System.currentTimeMillis() - dateStr;

                    sharedPref.edit().putBoolean("deltaRequired", false).apply();
                    sharedPref.edit().putLong("timeDelta", timeDelta).apply();
                    urlc.disconnect();
                }
            } catch (IOException e) {
                /*
                 * Should disable user from using the app
                 */
            } catch (NullPointerException e) {

            }
            return null;
        }
    }

    /**
     * To fetch a particular notification info
     */
    public Map<String, Object> fetchNotificationInfo(String notificationId) {
        for (int i = 0; i < notifications.size(); i++) {
            if (notifications.get(i).get("notificationId").equals(notificationId)) {
                return notifications.get(i);
            }
        }
        return null;
    }

    /**
     * To add or update the content of the notifications
     */
    public void addOrUpdateNotification(Map<String, Object> notification, String notificationId) {

        db.addOrUpdateNotificationContent(notificationDocId, notificationId, notification);

        for (int i = 0; i < notifications.size(); i++) {

            if (notifications.get(i).get("notificationId").equals(notificationId)) {
                notifications.set(i, notification);
                return;
            }
        }
        notifications.add(0, notification);
    }

    /*
     * To remove a particular notification
     */
    public void removeNotification(String notificationId) {

        boolean found = false;
        for (int i = 0; i < notifications.size(); i++) {

            if (notifications.get(i).get("notificationId").equals(notificationId)) {
                notifications.remove(i);
                found = true;
                break;
            }
        }
        if (found) {
            //  db.getParticularNotificationId(notificationDocId, notificationId);
            int systemNotificationId = db.removeNotification(notificationDocId, notificationId);
            if (systemNotificationId != -1) {

                NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


                nMgr.cancel(notificationId, systemNotificationId);
            }
        }
    }

    /*
     *
     *To allow the option of auto downloading of the media,when on wifi
     *
     */
    private boolean checkWifiConnected() {
        /*
         *Since connectionManager.isConnected() is deprecated in api23
         */
        if (wifiMgr == null) {
            wifiMgr = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        }
        if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

            if (wifiInfo == null) {

                wifiInfo = wifiMgr.getConnectionInfo();

            }
            if (wifiInfo == null) {


                return false;
            } else {
                if (wifiInfo.getNetworkId() == -1) {


                    return false; // Not connected to an access point
                }
            }


            return true; // Connected to an access point
        } else {


            return false; // Wi-Fi adapter is OFF
        }


    }


    private boolean checkMobileDataOn() {


        try {

            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = cm.getActiveNetworkInfo();

            return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);


        } catch (Exception e) {

            return false;
        }
    }


    /*
     * Will try to download the media message asynchronously in the background
     *
     */
    @SuppressWarnings("TryWithIdenticalCatches")
    private class DownloadMessage extends AsyncTask {

        /*
         *         String messageType = (String) params[1];
         *
         *           String replyTypeString = (String) params[2];
         *
         *            String thumbnailPath = (String) params[3];
         *            String filePath = (String) params[4];
         *           String messageId = (String) params[5];
         *
         *            String docId = (String) params[6];
         *            String senderId = (String) params[7];
         */

        @Override
        protected Object doInBackground(final Object[] params) {

            String url = (String) params[0];

            final FileDownloadService downloadService =
                    ServiceGenerator.createService(FileDownloadService.class);

            Call<ResponseBody> call = downloadService.downloadFileWithDynamicUrlAsync(url);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {

                    if (response.isSuccessful()) {
                        new AsyncTask<Void, Long, Void>() {
                            @Override
                            protected Void doInBackground(Void... voids) {

                                int replyType = -1;

                                if ((params[1]).equals("10")) {

                                    replyType = Integer.parseInt((String) params[2]);
                                }

                                boolean writtenToDisk = writeResponseBodyToDisk(response.body(), (String) params[4],
                                        (String) params[1], (String) params[5], replyType);


                                if (writtenToDisk) {

                                    //  deleteFileFromServer(url);

                                    if (params[3] != null) {
                                        /*
                                         *
                                         * incase of image or video delete the thumbnail
                                         *
                                         * */


                                        File fDelete = new File((String) params[3]);
                                        if (fDelete.exists()) fDelete.delete();


                                    }


                                    /*
                                     *To send the callback for automatically media downloaded
                                     */
                                    JSONObject obj = new JSONObject();
                                    try {
                                        obj.put("eventName", "MessageDownloaded");

                                        obj.put("messageType", params[1]);
                                        obj.put("replyType", params[2]);
                                        obj.put("filePath", params[4]);
                                        obj.put("messageId", params[5]);
                                        obj.put("docId", params[6]);
                                        obj.put("senderId", params[7]);

                                        bus.post(obj);
                                    } catch (JSONException e) {

                                    }


                                    try {

                                        db.updateDownloadStatusAndPath((String) params[6],
                                                (String) params[4], (String) params[5]);
                                    } catch (Exception e) {

                                    }

                                }


                                return null;


                            }
                        }.execute();


                    }


                }

                @Override
                public void onFailure(final Call<ResponseBody> call, Throwable t) {
                }

            });


            return null;
        }

    }

    @SuppressWarnings("all")
    private boolean writeResponseBodyToDisk(ResponseBody body, String filePath,
                                            String messageType, final String messageId, int replyType) {
        try {
            // todo change the file location/name according to your needs

            File folder = new File(Environment.getExternalStorageDirectory().getPath() + "/hola");

            if (!folder.exists() && !folder.isDirectory()) {
                folder.mkdirs();
            }

            File file = new File(filePath);


            if (!file.exists()) {
                file.createNewFile();
            }


            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {

                byte[] fileReader = new byte[4096];

                final long fileSize = body.contentLength();


                inputStream = body.byteStream();
                outputStream = new FileOutputStream(file);


                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1) {
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                }

                outputStream.flush();

                return true;

            } catch (ArrayIndexOutOfBoundsException e) {
                return false;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }
    public void createNewPostListener() {

        if (publishPost == null) {
            publishPost = new PublishPost();
        }
        publishPost.retryPublishingPosts(service);
    }

    public void addNewPost(PostData postData) {
        Log.e(TAG, "addNewPost: " );
        if (publishPost != null) {
            publishPost.addNewPost(postData);
        }


    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker(R.xml.global_tracker);
        }

        return sTracker;
    }
}

