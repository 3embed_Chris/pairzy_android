package com.pairzy.com.campaignScreen;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h>CampaignModule</h>
 * <p>provide needed class object instance.</p>
 * @author 3Embed.
 * @since 28/6/18.
 * @version 1.0.
 */
@Module
public abstract class CampaignModule {

    @ActivityScoped
    @Binds
    abstract CampaignContract.View bindView(CampaignActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity bindActvity(CampaignActivity activity);

    @ActivityScoped
    @Binds
    abstract CampaignContract.Presenter bindPresenter(CampaignPresenter presenter);

}
