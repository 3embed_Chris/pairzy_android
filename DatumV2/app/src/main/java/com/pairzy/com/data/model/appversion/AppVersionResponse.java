package com.pairzy.com.data.model.appversion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppVersionResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private AppVersionData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AppVersionData getData() {
        return data;
    }

    public void setData(AppVersionData data) {
        this.data = data;
    }

}