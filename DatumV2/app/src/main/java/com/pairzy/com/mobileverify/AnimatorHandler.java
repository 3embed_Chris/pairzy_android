package com.pairzy.com.mobileverify;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.pairzy.com.R;
/**
 * @since  2/6/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class AnimatorHandler
{
    private Context context;

    AnimatorHandler(Context context)
    {
        this.context=context;
    }

    public Animation getScaleUp()
    {
        return AnimationUtils.loadAnimation(context, R.anim.scal_center_up);
    }

    public Animation getScaleDown()
    {
        return AnimationUtils.loadAnimation(context,R.anim.scal_center_down);
    }

    public Animation getShakingAnimation()
    {
        return AnimationUtils.loadAnimation(context,R.anim.shaking_animation);
    }
}
