package com.pairzy.com.home.Dates;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.home.Dates.Pending_page.PendingFrg;
import com.pairzy.com.home.Dates.pastDatePage.PastDateFrg;
import com.pairzy.com.home.Dates.upcomingPage.UpcomingFrg;
import com.pairzy.com.home.HomeContract;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * <h2>DatesFrag</h2>
 * A simple {@link Fragment} subclass.
 * @author 3Embed.
 * @version 1.0.
 * @since 05-03-2018.
 */
@ActivityScoped
public class DatesFrag extends DaggerFragment implements DatesFragContract.View
{
    @BindView(R.id.title_text)
    TextView title_text;
    @BindView(R.id.dates_tab_layout)
    TabLayout dates_tab_layout;
    @BindView(R.id.datesViewpager)
    ViewPager datesViewpager;
    @BindView(R.id.parent_layout)
    CoordinatorLayout parent_layout;
    TextView tvCount;
    @BindView(R.id.tv_coin_balance)
    TextView tvCoinBalance;

    @Named(DatesFragUtil.DATE_FRAG_LIST)
    @Inject
    ArrayList<Fragment> fragmentList;

    @Inject
    Activity activity;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    HomeContract.Presenter main_presenter;
    @Inject
    DatesFragContract.Presenter presenter;

    private DatesAdapter datesAdapter;
    private FragmentManager fragmentManager;
    private Unbinder unbinder;
    private String title[];

    public DatesFrag() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_dates_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder=ButterKnife.bind(this,view);
        presenter.takeView(this);
        initUI();
        presenter.initDateRefreshObserver();
    }

    /*
    * initialization of the xml content.*/
    private void initUI()
    {
        tvCoinBalance.setTypeface(typeFaceManager.getCircularAirLight());
        title_text.setTypeface(typeFaceManager.getCircularAirBold());

        title = getResources().getStringArray(R.array.date_tab_titles);
        fragmentManager = getChildFragmentManager();
        datesAdapter = new DatesAdapter(fragmentManager,fragmentList);
        datesAdapter.notifyDataSetChanged();

        datesViewpager.setOffscreenPageLimit(2);
        datesViewpager.setAdapter(datesAdapter);
        dates_tab_layout.setupWithViewPager(datesViewpager);

        ViewGroup vg = (ViewGroup) dates_tab_layout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(typeFaceManager.getCircularAirBold());
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
        if(main_presenter.checkToUpcomingFrom())
        {
            datesViewpager.setCurrentItem(1);
        }
        try{
            setupTabCustomView();
        }catch (Exception e){}
    }

    @Override
    public void onResume() {
        super.onResume();
        main_presenter.updateCoinBalance();
        if(presenter.isDateRefreshReq()){
            presenter.refreshDateList();
        }
    }

    private void setupTabCustomView() {
        datesAdapter.notifyDataSetChanged();
        for (int i = 0; i < datesAdapter.getCount(); i++) {
            TabLayout.Tab tab = dates_tab_layout.getTabAt(i);
            tab.setCustomView(getTabView(i));
        }
    }

    private View getTabView(int i) {
        View tabView = LayoutInflater.from(dates_tab_layout.getContext()).inflate(R.layout.custom_date_tab_item,null);
        TextView tvTitle = tabView.findViewById(R.id.tv_tab_title);
        tvTitle.setText(title[i]);
        tvTitle.setTypeface(typeFaceManager.getCircularAirBold());
        if(i == 0) {
            tvCount = tabView.findViewById(R.id.tv_tab_count);
            tvCount.setVisibility(View.GONE);
            tvCount.setTypeface(typeFaceManager.getCircularAirBook());
        }
        else {
            TextView textViewCount = tabView.findViewById(R.id.tv_tab_count);
            textViewCount.setTypeface(typeFaceManager.getCircularAirBook());
            textViewCount.setVisibility(View.GONE);
        }
        return tabView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.dropView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }


    @OnClick(R.id.option_view)
    void onOptionClick()
    {

    }


    @OnClick(R.id.parent_layout)
    void onParentCLicked()
    {}


    @Override
    public void notifyAdapter() {
        for(Fragment fragment : fragmentList){
            if(fragment instanceof PendingFrg){
                ((PendingFrg)fragment).notifyAdapter();
            }
            if(fragment instanceof UpcomingFrg){
                ((UpcomingFrg)fragment).notifyAdapter();
            }
        }
    }
    @Override
    public void notifyPastDateAdapter() {
        for(Fragment fragment : fragmentList){
            if(fragment instanceof PastDateFrg){
                ((PastDateFrg)fragment).notifyAdapter();
            }
        }
    }

    @Override
    public void showPendingDateCount(int pendingDateCount) {
        if(tvCount != null) {
            if(pendingDateCount == 0)
                main_presenter.updatePendingDateBadgeCount("");
            else
                main_presenter.updatePendingDateBadgeCount(String.valueOf(pendingDateCount));

            tvCount.setText(String.valueOf(pendingDateCount));
            if (pendingDateCount == 0)
                tvCount.setVisibility(View.GONE);
            else
                tvCount.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void launchUserProfile(Intent intent) {
        startActivityForResult(intent, AppConfig.PROFILE_REQUEST);
    }

    @Override
    public void showCoinBalance(String coinBalance) {
        if(tvCoinBalance != null)
            tvCoinBalance.setText(coinBalance);
    }

    @Override
    public void showError(int id)
    {
        String message=getString(id);
        Snackbar snackbar = Snackbar
                .make(parent_layout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showError(String error)
    {
        Snackbar snackbar = Snackbar
                .make(parent_layout,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(parent_layout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showNetworkError(String error,boolean pendingDateFrg) {
        if(pendingDateFrg){
            for(Fragment fragment: fragmentList){
                if(fragment instanceof PendingFrg )
                    ((PendingFrg)fragment).showNetworkError(error);
            }
        }
        else{
            //upcomingFrg
            for(Fragment fragment: fragmentList){
                if(fragment instanceof UpcomingFrg)
                    ((UpcomingFrg)fragment).showNetworkError(error);
            }
        }
    }

    @Override
    public void showNetworkError(String error) {
        for(Fragment fragment: fragmentList){
            if(fragment instanceof PastDateFrg)
                ((PastDateFrg)fragment).showNetworkError(error);
        }
    }

    @Override
    public void showLoading(boolean pendingDateFrg) {
        if(pendingDateFrg){
            for(Fragment fragment: fragmentList){
                if(fragment instanceof PendingFrg )
                    ((PendingFrg)fragment).showLoading();
            }
        }
        else{
            //upcomingFrg
            for(Fragment fragment: fragmentList){
                if(fragment instanceof UpcomingFrg)
                    ((UpcomingFrg)fragment).showLoading();
            }
        }
    }

    @Override
    public void showPastDateLoading() {
        for(Fragment fragment: fragmentList){
            if(fragment instanceof PastDateFrg)
                ((PastDateFrg)fragment).showLoading();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.coin_view)
    void onCoinView()
    {
        main_presenter.launchCoinWallet();
    }
}
