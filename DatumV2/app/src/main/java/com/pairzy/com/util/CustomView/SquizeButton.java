package com.pairzy.com.util.CustomView;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
/**
 * <h2>SquizeButton</h2>
 * <P>
 *
 * </P>
 * @since  3/20/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class SquizeButton extends RelativeLayout implements View.OnTouchListener
{
    private ObjectAnimator scaleDownX,scaleDownY;
    private ObjectAnimator scaleDownX2,scaleDownY2;
    private static final int CLICK_ACTION_THRESHOLD = 100;
    private float startX;
    private float startY;
    private SquizeClicked listener;

    public SquizeButton(Context context) {
        super(context);
        initUi();
    }

    public SquizeButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUi();
    }

    public SquizeButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUi();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public SquizeButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initUi();
    }

    /*
    * initialization of the content*/
    private void initUi()
    {
        DatumBounceInterpolator interpolator=new DatumBounceInterpolator(0.2, 20);
        scaleDownX = ObjectAnimator.ofFloat(this,"scaleX", 0.8f);
        scaleDownY = ObjectAnimator.ofFloat(this,"scaleY", 0.8f);
        scaleDownX.setDuration(50);
        scaleDownX.setInterpolator(interpolator);
        scaleDownY.setDuration(50);
        scaleDownY.setInterpolator(interpolator);
        scaleDownX2 = ObjectAnimator.ofFloat(this, "scaleX", 1f);
        scaleDownY2 = ObjectAnimator.ofFloat(this, "scaleY", 1f);
        scaleDownX2.setDuration(50);
        scaleDownX2.setInterpolator(interpolator);
        scaleDownY2.setDuration(50);
        scaleDownY2.setInterpolator(interpolator);
        setOnTouchListener(this);
    }

    /*
    * Setting the listener.*/
    public void setClickListener(SquizeClicked listener)
    {
        this.listener=listener;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent)
    {
        switch (motionEvent.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                startX = motionEvent.getX();
                startY = motionEvent.getY();
                AnimatorSet scaleDown = new AnimatorSet();
                scaleDown.play(scaleDownX).with(scaleDownY);
                scaleDown.start();
                break;
            case MotionEvent.ACTION_UP:
                AnimatorSet scaleDown2 = new AnimatorSet();
                scaleDown2.play(scaleDownX2).with(scaleDownY2);
                scaleDown2.start();
                float endX = motionEvent.getX();
                float endY = motionEvent.getY();
                if (isAClick(startX, endX, startY, endY))
                {
                    if(listener!=null)
                        listener.onClicked();
                }
                break;
        }
        return true;
    }

    private boolean isAClick(float startX, float endX, float startY, float endY)
    {
        float differenceX = Math.abs(startX - endX);
        float differenceY = Math.abs(startY - endY);
        return !(differenceX > CLICK_ACTION_THRESHOLD/* =5 */ || differenceY > CLICK_ACTION_THRESHOLD);
    }
}
