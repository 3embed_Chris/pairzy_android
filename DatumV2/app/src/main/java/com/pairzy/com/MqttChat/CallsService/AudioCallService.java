package com.pairzy.com.MqttChat.CallsService;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.pairzy.com.AppController;
import com.pairzy.com.BuildConfig;
import com.pairzy.com.MqttChat.Calls.InCallFinishActivity;
import com.pairzy.com.MqttChat.Calls.WebRTC.AppRTCAudioManager;
import com.pairzy.com.MqttChat.Calls.WebRTC.AppRTCClient;
import com.pairzy.com.MqttChat.Calls.WebRTC.DirectRTCClient;
import com.pairzy.com.MqttChat.Calls.WebRTC.PeerConnectionClient;
import com.pairzy.com.MqttChat.Calls.WebRTC.WebSocketRTCClient;
import com.pairzy.com.MqttChat.Utilities.MqttEvents;
import com.pairzy.com.MqttChat.Utilities.TextDrawable;
import com.pairzy.com.MqttChat.Utilities.Utilities;
import com.pairzy.com.R;
import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.pairzy.com.splash.SplashActivity;
import com.pairzy.com.util.AppConfig;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SessionDescription;
import org.webrtc.StatsReport;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_AECDUMP_ENABLED;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_AUDIOCODEC;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_AUDIO_BITRATE;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_DATA_CHANNEL_ENABLED;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_DISABLE_BUILT_IN_AEC;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_DISABLE_BUILT_IN_AGC;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_DISABLE_BUILT_IN_NS;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_DISABLE_WEBRTC_AGC_AND_HPF;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_ENABLE_RTCEVENTLOG;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_FLEXFEC_ENABLED;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_HWCODEC_ENABLED;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_ID;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_LOOPBACK;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_MAX_RETRANSMITS;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_MAX_RETRANSMITS_MS;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_NEGOTIATED;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_NOAUDIOPROCESSING_ENABLED;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_OPENSLES_ENABLED;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_ORDERED;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_PROTOCOL;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_ROOMID;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_SAVE_INPUT_AUDIO_TO_FILE_ENABLED;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_TRACING;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_URLPARAMETERS;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_VIDEOCODEC;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_VIDEO_BITRATE;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_VIDEO_CALL;
import static com.pairzy.com.MqttChat.Calls.WebRTC.Constants.EXTRA_VIDEO_FPS;
import static com.pairzy.com.util.AppConfig.NOTIFICATION_CHANNEL_ID_FOURGROUND_SERVICE;
import static com.pairzy.com.util.AppConfig.NOTIFICATION_CHANNEL_NAME_CHAT_SERVICE;

/**
 * Created by moda on 16/08/17.
 */

public class AudioCallService extends Service implements AppRTCClient.SignalingEvents, PeerConnectionClient.PeerConnectionEvents,
        AudioCallEvents {

    private static final String TAG = "AudioCallService";

    // List of mandatory application permissions.

    private static final String[] MANDATORY_PERMISSIONS = {
            "android.permission.MODIFY_AUDIO_SETTINGS", "android.permission.RECORD_AUDIO",
            "android.permission.INTERNET"
    };

    // Peer connection statistics callback period in ms.
    private static final int STAT_CALLBACK_PERIOD = 1000;

    @Nullable
    private PeerConnectionClient peerConnectionClient;
    @Nullable
    private AppRTCClient appRtcClient;
    @Nullable
    private AppRTCClient.SignalingParameters signalingParameters;
    @Nullable
    private AppRTCAudioManager audioManager;
    @Nullable
    private EglBase rootEglBase;
    @Nullable
    private NotificationManager mNotificationManager;
    @Nullable
    private AppRTCClient.RoomConnectionParameters roomConnectionParameters;
    @Nullable
    private PeerConnectionClient.PeerConnectionParameters peerConnectionParameters;


    private long callStartedTimeMs = 0;


    private static Bus bus = AppController.getBus();


    private boolean disconnect = false;

    /* Media Player Class to enable ring sound */ MediaPlayer mp;

    private CountDownTimer timer;
    private  CountDownTimer cTimer;

    private FrameLayout root;


    private View audioCallView, callHeaderView;
    private Context mContext;
    private Intent intent;
    private Handler handler;


    /**
     * Previously the fragment content to be accessible in service now
     */
    private long countUp, countUpHeader;

    private Chronometer stopWatchHeader;


    private ImageView mute, speaker;
    private TextView tvStopWatch, tvCallerName;


    private boolean isMute = false, isSpeaker = false;

    private ImageView callerImage;


    /**
     * Call control interface for container activity.
     */

    private String imageUrl = "";
    private ImageView initiateChat;


    private String callDuration, callDurationHeader;


    /**
     * Legacy code although not used as of now
     */
    private WindowManager windowManager;
    private TextView callHeaderTv;
    private ImageView callHeaderIv;
    private WindowManager.LayoutParams params;
    private boolean callConnectedOnce = false;
    private LinearLayout llLoading;
    private TextView tvLoading;
    private boolean connected;
    private TextView tvReconnecting
            ;
    private MediaPlayer reconnectTonePlayer;
    private CountDownTimer reconnectTimer;
    private long MAX_RECONNECT_TIME = 30000;
    private boolean timerAlreadyStarted;

    @Override
    public void onCreate() {
        super.onCreate();
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        windowManager = (WindowManager)getSystemService(WINDOW_SERVICE);
        mContext = this;
        audioCallView = LayoutInflater.from(this).inflate(R.layout.audio_call_service, null);
        bus.register(this);
        // Set window styles for fullscreen-window size. Needs to be done before
        // adding content.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            audioCallView
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        } else {

            audioCallView
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                    );
        }

        tvStopWatch = (TextView) audioCallView.findViewById(R.id.tvStopWatch);
        //Add the view to the window.
        addMainLayout();
        handler = new Handler(Looper.getMainLooper());
        root = (FrameLayout) audioCallView.findViewById(R.id.root);
        signalingParameters = null;

        rootEglBase = EglBase.create();

        // Check for mandatory permissions.
        for (String permission : MANDATORY_PERMISSIONS) {
            if (checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                logAndToast("Permission " + permission + " is not granted");
                stopSelf();
                return;
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        this.intent = intent;
        if (intent != null) {
            if((intent.getAction() != null && intent.getAction().equals(AppConfig.ACTION_START_FOURGROUND)) || Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ){
                attachNotification();
            }
            if (!intent.getExtras().getBoolean("isIncomingCall", true)) {
                AppController.getInstance().subscribeToTopic(MqttEvents.CallsAvailability.value + "/" + intent.getExtras().getString("callerId"), 0);

                mp = MediaPlayer.create(this, R.raw.calling);
                mp.setLooping(true);
                mp.start();
                startCallProcedure(false);
            } else {
                tvStopWatch.setText(getString(R.string.connecting));
                startCallProcedure(true);
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void attachNotification() {
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent  = PendingIntent.getActivity(getApplicationContext(),112,intent,0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_launcher);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.foreground_service_title))
                .setTicker(getString(R.string.call_running_notif_title))
                .setContentText(getString(R.string.app_name))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(
                        Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true);
        //.build();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
        {
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_FOURGROUND_SERVICE, NOTIFICATION_CHANNEL_NAME_CHAT_SERVICE, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID_FOURGROUND_SERVICE);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        startForeground(AppConfig.NOTIFICATION_ID.FOREGROUND_SERVICE,
                mBuilder.build());
    }

    @Override
    public IBinder onBind(Intent intent) {


        this.intent = intent;
        return null;
    }

    private void destroyOnAppCrashed(boolean appCrashed) {
        disconnect();
        bus.unregister(this);

        if (!appCrashed) {

            if (AppController.getInstance().isApplicationKilled()) {

                startActivity(new Intent(getApplicationContext(), InCallFinishActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }
            //System.exit(1);
        }
    }

    @Override
    public void onDestroy() {
        destroyOnAppCrashed(false);
        super.onDestroy();
    }

    // OnCallEvents interface implementation.
    @Override
    public void onCallHangUp(int val, boolean received) {

        if (!disconnect) {
            disconnect = true;
            /* Send Reject through call event and call callEnd API */

            /*
             * MQtt
             */

            /*
             * Timeout from the sender side or call canceled from sender side
             */

            if (intent != null) {
                try {
                    if (!received) {

                        JSONObject obj = new JSONObject();


                        obj.put("callId", intent.getExtras().getString(EXTRA_ROOMID));


                        obj.put("userId", AppController.getInstance().getUserId());
                        obj.put("type", val);
                        String dateId=  intent.getExtras().getString("dateId");
                        obj.put("dateId",dateId);

                        /*
                         * 7--timeout
                         * 2--reject
                         */

                        Log.w("AudioDate","dateId: "+dateId);
                        AppController.getInstance().publish(MqttEvents.Calls.value + "/" + intent.getExtras().getString("callerId"), obj, 0, false);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
        disconnect();
    }

    @Override
    public void onMute() {
        isMute = !isMute;
        audioManager.setMicrophoneMute(isMute);
    }

    @Override
    public void onSpeaker() {

        isSpeaker = !isSpeaker;
        audioManager.setSpeakerphoneOn(isSpeaker);
    }


    private void startCall() {
        if (appRtcClient == null) {

            return;
        }
        callStartedTimeMs = System.currentTimeMillis();

        // Start room connection.
        // logAndToast(getString(R.string.connecting_to, roomConnectionParameters.roomUrl));
        appRtcClient.connectToRoom(roomConnectionParameters);

        // Create and audio manager that will take care of audio routing,
        // audio modes, audio device enumeration etc.
//        audioManager = AppRTCAudioManager.create(false, this, new Runnable() {
//            // This method will be called each time the audio state (number and
//            // type of devices) has been changed.
//            @Override
//            public void run() {
//                onAudioManagerChangedState();
//            }
//        });
        // Store existing audio settings and change audio mode to
        // MODE_IN_COMMUNICATION for best possible VoIP performance.

        //audioManager.init();
        audioManager = AppRTCAudioManager.create(mContext);

        /*
         * To set speaker as default for the audio call
         */
        //audioManager.setSpeakerphoneOn(false);
    }

    // Should be called from UI thread
    private void callConnected() {
        //   final long delta = System.currentTimeMillis() - callStartedTimeMs;
        callConnectedOnce = true;
        if (peerConnectionClient == null) {
            return;
        }
        // Update video view.
        // Enable statistics callback.
        peerConnectionClient.enableStatsEvents(false, STAT_CALLBACK_PERIOD);

        try {
            /* Stop the calling sound */
            if (mp != null) {
                try {
                    if (mp.isPlaying()) {

                        mp.stop();
                        mp.release();
                    }
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(timer != null)
            timer.cancel();

        /* Start the stop watch */
//        AudioCallFragment.startStopWatch();
        if (!timerAlreadyStarted) {
            timerAlreadyStarted = true;
            startStopWatch();
        }

    }

    private void onAudioManagerChangedState() {
        // TODO(henrika): disable video if AppRTCAudioManager.AudioDevice.EARPIECE
        // is active.
    }

    private void removeViews(){
        if (windowManager == null) {
            windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        }
        try {
            if (AppController.getInstance().isCallMinimized()) {
                try {
                    if (callHeaderView != null) {
                        windowManager.removeView(callHeaderView);
                    }
                }catch (IllegalArgumentException e){
                    if(audioCallView != null){
                        /*
                         * for clearing the full screen UI mode
                         * */
                        audioCallView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                        try{
                            windowManager.removeView(audioCallView);
                        }catch (IllegalArgumentException ef){}
                    }
                }
            } else {
                try {
                    if (audioCallView != null) {
                        /*
                         * For clearing of the full screen UI mode
                         */
                        audioCallView
                                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                        windowManager.removeView(audioCallView);
                    }
                }catch (IllegalStateException e){
                    if (callHeaderView != null) {
                        try {
                            windowManager.removeView(callHeaderView);
                        } catch (IllegalArgumentException ef) {
                        }
                    }
                }
            }

            if(mNotificationManager != null)
                mNotificationManager.cancel(AppConfig.NOTIFICATION_ID.FOREGROUND_SERVICE);

            try {
                if (rootEglBase != null)
                    rootEglBase.release();
            }catch (Exception e){
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Disconnect from remote resources, dispose of local resources, and exit.
    private void disconnect() {
        try {

            /*
             * To make myself available for receiving the new call
             */

            if(cTimer != null)
                cTimer.cancel();
            AppController.getInstance().currentActiveDateId = "";

            JSONObject obj = new JSONObject();
            obj.put("status", 1);

            AppController.getInstance().publish(MqttEvents.CallsAvailability.value + "/" + AppController.getInstance().getUserId(), obj, 0, true);
            AppController.getInstance().setActiveOnACall(false, false);


            timer.cancel();

            if (appRtcClient != null) {
                appRtcClient.disconnectFromRoom();
                appRtcClient = null;
            }
            if (peerConnectionClient != null) {
                peerConnectionClient.close();
                peerConnectionClient = null;
            }
            if (audioManager != null) {
                audioManager.stop();
                audioManager = null;
            }

            try {
                if (mp != null)
                    mp.stop();
            } catch (Exception e) {

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        removeViews();
        stopSelf();
    }

    private void logAndToast(String msg) {


    }


    // -----Implementation of AppRTCClient.AppRTCSignalingEvents ---------------
    // All callbacks are invoked from websocket signaling looper thread and
    // are routed to UI thread.
    private void onConnectedToRoomInternal(final AppRTCClient.SignalingParameters params) {
        // final long delta = System.currentTimeMillis() - callStartedTimeMs;

        signalingParameters = params;
        if (peerConnectionClient != null) {
            // logAndToast("Creating peer connection, delay=" + delta + "ms");
            peerConnectionClient.createPeerConnection(null, null, null,
                    signalingParameters);

            if (signalingParameters != null && signalingParameters.initiator) {
                //    logAndToast("Creating OFFER...");
                // Create offer. Offer SDP will be send to answering client in
                // PeerConnectionEvents.onLocalDescription event.
                peerConnectionClient.createOffer();
            } else {
                if (params.offerSdp != null) {
                    peerConnectionClient.setRemoteDescription(params.offerSdp);
                    // logAndToast("Creating ANSWER...");
                    // Create answer. Answer SDP will be send to offering client in
                    // PeerConnectionEvents.onLocalDescription event.
                    peerConnectionClient.createAnswer();
                }
                if (params.iceCandidates != null) {
                    // Add remote ICE candidates from room.
                    for (IceCandidate iceCandidate : params.iceCandidates) {
                        peerConnectionClient.addRemoteIceCandidate(iceCandidate);
                    }
                }
            }
        }
    }

    @Override
    public void onConnectedToRoom(final AppRTCClient.SignalingParameters params) {

        handler.post(new Runnable() {
            @Override
            public void run() {
                onConnectedToRoomInternal(params);
            }
        });
    }
    @Override
    public void onRemoteDescription(final SessionDescription sdp) {
        //     final long delta = System.currentTimeMillis() - callStartedTimeMs;


        handler.post(new Runnable() {
            @Override
            public void run() {
                if (peerConnectionClient != null) {
                    //   logAndToast("Received remote " + sdp.type + ", delay=" + delta + "ms");
                    peerConnectionClient.setRemoteDescription(sdp);
                    if (signalingParameters != null && !signalingParameters.initiator) {
                        //     logAndToast("Creating ANSWER...");
                        // Create answer. Answer SDP will be send to offering client in
                        // PeerConnectionEvents.onLocalDescription event.
                        peerConnectionClient.createAnswer();
                    }
                } else {
                    Log.e(TAG, "Received remote SDP for non-initilized peer connection.");

                }
            }
        });
    }

    @Override
    public void onRemoteIceCandidate(final IceCandidate candidate) {


        handler.post(new Runnable() {
            @Override
            public void run() {
                if (peerConnectionClient != null) {

                    peerConnectionClient.addRemoteIceCandidate(candidate);
                } else {
                    Log.e(TAG, "Received ICE candidate for a non-initialized peer connection.");

                }
            }
        });
    }

    @Override
    public void onRemoteIceCandidatesRemoved(IceCandidate[] candidates) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(peerConnectionClient != null){
                    peerConnectionClient.removeRemoteIceCandidates(candidates);
                } else{

                    Log.e(TAG, "Received ICE candidate removals for a non-initialized peer connection.");
                }
            }
        });
    }

    @Override
    public void onChannelClose() {

    }

    private void reportError(final String description) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mContext, description, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onChannelError(final String description) {
          reportError(description);
    }

    // -----Implementation of PeerConnectionClient.PeerConnectionEvents.---------
    // Send local peer connection SDP and ICE candidates to remote party.
    // All callbacks are invoked from peer connection client looper thread and
    // are routed to UI thread.
    @Override
    public void onLocalDescription(final SessionDescription sdp) {
        //    final long delta = System.currentTimeMillis() - callStartedTimeMs;


        handler.post(new Runnable() {
            @Override
            public void run() {
                if (appRtcClient != null) {
                    //     logAndToast("Sending " + sdp.type + ", delay=" + delta + "ms");
                    if (signalingParameters != null && signalingParameters.initiator) {
                        appRtcClient.sendOfferSdp(sdp);
                    } else {
                        appRtcClient.sendAnswerSdp(sdp);
                    }
                    if (peerConnectionParameters != null && peerConnectionParameters.videoMaxBitrate > 0) {
                        if (peerConnectionClient != null) {
                            peerConnectionClient.setVideoMaxBitrate(peerConnectionParameters.videoMaxBitrate);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onIceCandidate(final IceCandidate candidate) {


        handler.post(new Runnable() {
            @Override
            public void run() {
                if (appRtcClient != null) {
                    appRtcClient.sendLocalIceCandidate(candidate);
                }
            }
        });
    }

    @Override
    public void onIceCandidatesRemoved(IceCandidate[] candidates) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (appRtcClient != null) {
                    appRtcClient.sendLocalIceCandidateRemovals(candidates);
                }
            }
        });
    }

    @Override
    public void onIceConnected() {
        //  final long delta = System.currentTimeMillis() - callStartedTimeMs;
    }

    @Override
    public void onIceDisconnected() {
    }

    @Override
    public void onConnected() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                tvReconnecting.setVisibility(View.GONE);
                connected = true;

                if (reconnectTonePlayer != null) {

                    try {
                        if (reconnectTonePlayer.isPlaying()) {
                            try {
                                reconnectTonePlayer.stop();

                                reconnectTonePlayer.release();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                }


                callConnected();
                stopReconnectTimer();
            }
        });
    }

    private void stopReconnectTimer() {
        if (reconnectTimer != null) {
            try {
                reconnectTimer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onDisconnected() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                tvReconnecting.setVisibility(View.VISIBLE);
                connected = false;

                if (reconnectTonePlayer != null) {
                    try {
                        if (reconnectTonePlayer.isPlaying()) {
                            try {
                                reconnectTonePlayer.stop();

                                reconnectTonePlayer.release();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                }


                reconnectTonePlayer = MediaPlayer.create(mContext, R.raw.end_call);
                reconnectTonePlayer.setLooping(true);

                if (!connected) {

                    try {
                        reconnectTonePlayer.start();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    initializeReconnectTimer();
                }

            }
        });
    }

    private void initializeReconnectTimer() {


        if (reconnectTimer != null) {
            try {
                reconnectTimer.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        reconnectTimer = new CountDownTimer(MAX_RECONNECT_TIME, 1000) {
            public void onTick(long millisUntilFinished) {

                if (connected) {
                    reconnectTimer.cancel();

                }
            }

            public void onFinish() {
                reconnectTimer.cancel();

                Toast.makeText(mContext, getResources().getString(R.string.reconnect_failed), Toast.LENGTH_LONG).show();
                onCallHangUp(2, false);
            }
        };
        reconnectTimer.start();

    }


    @Override
    public void onPeerConnectionClosed() {
    }

    @Override
    public void onPeerConnectionStatsReady(final StatsReport[] reports) {
    }

    @Override
    public void onPeerConnectionError(final String description) {
        //reportError(description);
    }

    @Subscribe
    public void getMessage(JSONObject object) {
        try {


            if (object.getString("eventName").substring(0, 6).equals(MqttEvents.Calls.value + "A")) {

                if (intent != null) {


                    Bundle extras = intent.getExtras();

                    if (object.getInt("status") == 0) {
                        /*
                         * Receiver is busy
                         */


                        if (root != null) {

                            Snackbar snackbar = Snackbar.make(root, extras.getString("callerName") + " " + getString(R.string.busy), Snackbar.LENGTH_SHORT);


                            snackbar.show();
                            View view = snackbar.getView();
                            TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                        }
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {


                                stopSelf();
                            }
                        }, 2000);


                    } else {
                        /*
                         * I put myself as busy and make the call request to the receiver
                         */


                        try {

                            JSONObject obj = new JSONObject();
                            obj.put("status", 0);


                            AppController.getInstance().publish(MqttEvents.CallsAvailability.value + "/" + AppController.getInstance().getUserId(), obj, 0, true);
                            AppController.getInstance().setActiveOnACall(true, true);

                            obj = new JSONObject();
                            obj.put("callerId", AppController.getInstance().getUserId());
                            obj.put("callId", extras.getString(EXTRA_ROOMID));
                            obj.put("dateId",extras.getString("dateId"));
                            obj.put("callType", "0");
                            obj.put("callerName", AppController.getInstance().getUserName());
                            obj.put("callerImage", AppController.getInstance().getUserImageUrl());
                            obj.put("callerIdentifier", AppController.getInstance().getUserIdentifier());

                            obj.put("type", 0);
                            /*
                             * CalleeId althought not required but can be used in future on server so using it
                             * CalleeId contains the receiverUid,to whom the call has been made
                             *
                             * */



                        /*
                         * Type-0---call initiate request,on receiving the call initiate request,receiver will set his status as busy,so nobody else can call him
                         *
                         * /


                        obj.put("type", 0);
/*
 * Not making any message of call signalling as being persistent intentionally
 */

                            AppController.getInstance().publish(MqttEvents.Calls.value + "/" + extras.getString("callerId"), obj, 0, false);


                            AppController.getInstance().setActiveCallId(extras.getString(EXTRA_ROOMID));
                            AppController.getInstance().setActiveCallerId(extras.getString("callerId"));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                }


            } else if (object.getString("eventName").equals(MqttEvents.Calls.value + "/" + AppController.getInstance().getUserId())) {

                if (intent != null) {
                    switch (object.getInt("type")) {

                        case 1:
                            tvStopWatch.setText(getString(R.string.connecting));
                            break;
                        case 2:
                            onCallHangUp(2, true);
                            break;
                        case 3:


                            if (root != null) {

                                Snackbar snackbar = Snackbar.make(root, getString(R.string.NoAnswer) + " " + intent.getExtras().getString("callerName"), Snackbar.LENGTH_SHORT);


                                snackbar.show();
                                View view = snackbar.getView();
                                TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                            }

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {


                                    stopSelf();
                                }
                            }, 2000);
                            break;

                        case 7:


                            onCallHangUp(7, true);


                    }
                }
            } else if (object.getString("eventName").equals("turnOnScreen")) {

                turnOnScreen();


            } else if (object.getString("eventName").equals("turnOffScreen")) {

                turnOffScreen();

            } else if (object.getString("eventName").equals("appCrashed")) {
                destroyOnAppCrashed(true);
                System.exit(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void startCallProcedure(boolean incomingCall) {

        // Get Intent parameters.

        if (intent != null) {

            final Intent intent = this.intent;

            Uri roomUri = intent.getData();
            if (roomUri == null) {
                //  logAndToast(getString(R.string.missing_url));

                stopSelf();
                return;
            }

            String roomId = intent.getStringExtra(EXTRA_ROOMID);
            if (roomId == null || roomId.length() == 0) {
                //  logAndToast(getString(R.string.missing_url));
                stopSelf();
                return;
            }

            AppController.getInstance().currentActiveDateId = intent.getStringExtra("dateId");

            boolean loopback = intent.getBooleanExtra(EXTRA_LOOPBACK, false);
            boolean tracing = intent.getBooleanExtra(EXTRA_TRACING, false);

            PeerConnectionClient.DataChannelParameters dataChannelParameters = null;
            if (intent.getBooleanExtra(EXTRA_DATA_CHANNEL_ENABLED, false)) {
                dataChannelParameters = new PeerConnectionClient.DataChannelParameters(intent.getBooleanExtra(EXTRA_ORDERED, true),
                        intent.getIntExtra(EXTRA_MAX_RETRANSMITS_MS, -1),
                        intent.getIntExtra(EXTRA_MAX_RETRANSMITS, -1), intent.getStringExtra(EXTRA_PROTOCOL),
                        intent.getBooleanExtra(EXTRA_NEGOTIATED, false), intent.getIntExtra(EXTRA_ID, -1));
            }

            peerConnectionParameters =
                    new PeerConnectionClient.PeerConnectionParameters(intent.getBooleanExtra(EXTRA_VIDEO_CALL, false), loopback,
                            tracing, 0, 0, intent.getIntExtra(EXTRA_VIDEO_FPS, 0),
                            intent.getIntExtra(EXTRA_VIDEO_BITRATE, 0), intent.getStringExtra(EXTRA_VIDEOCODEC),
                            intent.getBooleanExtra(EXTRA_HWCODEC_ENABLED, true),
                            intent.getBooleanExtra(EXTRA_FLEXFEC_ENABLED, false),
                            intent.getIntExtra(EXTRA_AUDIO_BITRATE, 0), intent.getStringExtra(EXTRA_AUDIOCODEC),
                            intent.getBooleanExtra(EXTRA_NOAUDIOPROCESSING_ENABLED, false),
                            intent.getBooleanExtra(EXTRA_AECDUMP_ENABLED, false),
                            intent.getBooleanExtra(EXTRA_SAVE_INPUT_AUDIO_TO_FILE_ENABLED, false),
                            intent.getBooleanExtra(EXTRA_OPENSLES_ENABLED, false),
                            intent.getBooleanExtra(EXTRA_DISABLE_BUILT_IN_AEC, false),
                            intent.getBooleanExtra(EXTRA_DISABLE_BUILT_IN_AGC, false),
                            intent.getBooleanExtra(EXTRA_DISABLE_BUILT_IN_NS, false),
                            intent.getBooleanExtra(EXTRA_DISABLE_WEBRTC_AGC_AND_HPF, false),
                            intent.getBooleanExtra(EXTRA_ENABLE_RTCEVENTLOG, false), dataChannelParameters);


            // Create connection client. Use DirectRTCClient if room name is an IP otherwise use the
            // standard WebSocketRTCClient.
            if (loopback || !DirectRTCClient.IP_PATTERN.matcher(roomId).matches()) {
                appRtcClient = new WebSocketRTCClient(this);
            } else {
                Log.i(TAG, "Using DirectRTCClient because room name looks like an IP.");
                appRtcClient = new DirectRTCClient(this);
            }
            String urlParameters = intent.getStringExtra(EXTRA_URLPARAMETERS);
            roomConnectionParameters =
                    new AppRTCClient.RoomConnectionParameters(roomUri.toString(),
                            roomId, loopback,urlParameters);

            // Send intent arguments to fragments.



            // Create peer connection client.
            peerConnectionClient = new PeerConnectionClient(
                    getApplicationContext(), rootEglBase, peerConnectionParameters, this);
            PeerConnectionFactory.Options options = new PeerConnectionFactory.Options();
            if (loopback) {
                options.networkIgnoreMask = 0;
            }

            peerConnectionClient.createPeerConnectionFactory(options);

            showCallControlsUi();
            startCall();


            AudioManager audioManager1 = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            audioManager1.setStreamVolume(AudioManager.STREAM_MUSIC, 10, 0);


            /* Implement the count down timer */
            timer = new CountDownTimer(60000, 1000) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    /* Perform the click of cancel button here */
                    Toast.makeText(mContext, getResources().getString(R.string.Timeout), Toast.LENGTH_LONG).show();
                    onCallHangUp(7, false);
                }
            };
            timer.start();

        }


    }

    private void setCallDuration() {

        cTimer = new CountDownTimer(3600000, 1000) {
            public void onTick(long millisUntilFinished) {
                long milliSec =(3600000 - millisUntilFinished)/1000;
                long sec = milliSec % 60;
                long min = milliSec/60;
                if (min<10) {
                    if (sec<10) {
                        tvStopWatch.setText("00:0" + min + ":0" + sec);
                        callHeaderTv.setText("00:0" + min + ":0" + sec);
                    }
                    else {
                        tvStopWatch.setText("00:0" + min + ":" + sec);
                        callHeaderTv.setText("00:0" + min + ":" + sec);

                    }

                } else {
                    if (sec<10) {
                        tvStopWatch.setText("00:" + min + ":0" + sec);
                        callHeaderTv.setText("00:" + min + ":0" + sec);
                    }
                    else {
                        tvStopWatch.setText("00:" + min + ":" + sec);
                        callHeaderTv.setText("00:" + min + ":" + sec);
                    }

                }


            }
            @Override
            public void onFinish() {

            }
        };
        cTimer.start();
        setupCallHeaderDuration();

        /*
         * UNCOMMENT THIS LINE IF HAVE TO START AUDIO CALL WITH SPEAKER BEING SET TO LOUDSPEAKER
         */
        //speaker.performClick();

        initiateChat.setVisibility(View.VISIBLE);

    }

    public void startStopWatch() {
        setCallDuration();
        if(initiateChat != null)
            initiateChat.setVisibility(View.VISIBLE);
    }


    public void callCompleteDateApi(){

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("date_id", intent.getStringExtra("dateId"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(llLoading != null)
            llLoading.setVisibility(View.VISIBLE);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                BuildConfig.BASEURL + "completeDate", jsonObject,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d(TAG, "completeDate onResponse: "+response);
                            if (response.getInt("code") == 200) {
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if(llLoading != null)
                            llLoading.setVisibility(View.GONE);
                        onCallHangUp(2, false);
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if(llLoading != null)
                    llLoading.setVisibility(View.GONE);
                onCallHangUp(2, false);
            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("authorization", AppController.getInstance().getApiToken());
                headers.put("lang", AppConfig.DEFAULT_LANGUAGE);
                return headers;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        /* Add the request to the RequestQueue.*/
        AppController.getInstance().addToRequestQueue(jsonObjReq, "completeDateApiRequest");
        if(llLoading != null)
            llLoading.setVisibility(View.VISIBLE);
    }

    @SuppressWarnings("TryWithIdenticalCatches")

    private void showCallControlsUi() {
        /*
         * Instead of adding the fragment in the container,we now just update the visibility of the view
         *
         */


        callHeaderView = LayoutInflater.from(this).inflate(R.layout.layout_floating_widget, null);
        callHeaderIv = (ImageView) callHeaderView.findViewById(R.id.image_iv);
        callHeaderTv = (TextView) callHeaderView.findViewById(R.id.duration);
        stopWatchHeader = (Chronometer) callHeaderView.findViewById(R.id.chrono);


        callerImage = (ImageView) audioCallView.findViewById(R.id.thumbnail);

        llLoading = audioCallView.findViewById(R.id.ll_loading);
        tvLoading = audioCallView.findViewById(R.id.tv_loading);

        /*
         * For initiating of the chat
         */

        initiateChat = (ImageView) audioCallView.findViewById(R.id.initiateChat);


        // Create UI controls.
        ImageView cancelButton = (ImageView) audioCallView.findViewById(R.id.diconnect_btn);
        mute = (ImageView) audioCallView.findViewById(R.id.mute);
        //mute.setSelected(isMute);
        speaker = (ImageView) audioCallView.findViewById(R.id.speaker);
        //speaker.setSelected(isSpeaker);

//        mute.getDrawable().clearColorFilter();
//        speaker.getDrawable().clearColorFilter();
//        speaker.getDrawable().setColorFilter(0xFF000000, PorterDuff.Mode.SRC_ATOP);
//        mute.getDrawable().setColorFilter(0xFF000000, PorterDuff.Mode.SRC_ATOP);

        tvCallerName = (TextView) audioCallView.findViewById(R.id.tvCallerName);
        tvReconnecting = (TextView) audioCallView.findViewById(R.id.tvReconnecting);


        final String caller_id = intent.getStringExtra("callerId");


        /*
         * If userId doesn't exists in contact
         */
        if (intent != null) {
            Bundle extras = intent.getExtras();
            tvCallerName.setText(extras.getString("callerName"));
            //tvCallerName.setText(extras.getString("callerIdentifier"));

            imageUrl = extras.getString("callerImage");

            if (imageUrl == null || imageUrl.isEmpty()) {


                callerImage.setImageDrawable(TextDrawable.builder()
                        .beginConfig()
                        .textColor(Color.WHITE)
                        .useFont(Typeface.DEFAULT)
                        .fontSize(124 * (int) getResources().getDisplayMetrics().density) /* size in px */
                        .bold()
                        .toUpperCase()
                        .endConfig()
                        .buildRect((extras.getString("callerIdentifier").trim()).charAt(0) + "", Color.parseColor(AppController.getInstance().getColorCode(5))));

                /*
                 * For header when call is minimized
                 */
                callHeaderIv.setImageDrawable(TextDrawable.builder()


                        .beginConfig()
                        .textColor(Color.WHITE)
                        .useFont(Typeface.DEFAULT)
                        .fontSize(36 * (int) getResources().getDisplayMetrics().density) /* size in px */
                        .bold()
                        .toUpperCase()
                        .endConfig()


                        .buildRect((extras.getString("callerIdentifier").trim()).charAt(0) + "", Color.parseColor(AppController.getInstance().getColorCode(5))));


            } else {

                try {
                    Glide.with(mContext).asBitmap().load(imageUrl)
                            .centerCrop().placeholder(R.drawable.chat_attachment_profile_default_image_frame)
                            .transform(new CircleCrop())
                            .into(new BitmapImageViewTarget(callerImage));

                    /*
                     * For header when call is minimized
                     */

                    Glide.with(mContext).asBitmap()
                            .load(imageUrl)
                            .centerCrop().placeholder(R.drawable.chat_attachment_profile_default_image_frame)
                            .transform(new CircleCrop())
                            .into(callHeaderIv);

                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }
        }

        // Add buttons click events.
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(callConnectedOnce) {
                    callConnectedOnce = false;
                    callCompleteDateApi();
                }
                else {
                    onCallHangUp(2, false);
                }
            }
        });

        mute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onMute();
                mute.setSelected(isMute);

            }
        });

        speaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSpeaker();
                speaker.setSelected(isSpeaker);

            }
        });


        WindowManager.LayoutParams params;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
        }
        else{
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
        }
        // final WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        //Specify the view position
        params.gravity = Gravity.TOP | Gravity.START;        //Initially view will be added to top-left corner
        params.x = 0;
        params.y = 100;

        callHeaderView.setOnTouchListener(new View.OnTouchListener()

        {
            private int lastAction;
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {


                    case MotionEvent.ACTION_DOWN:


                        //remember the initial position.
                        initialX = params.x;
                        initialY = params.y;

                        //get the touch location
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();

                        lastAction = event.getAction();
                        return true;
                    case MotionEvent.ACTION_UP:
                        //As we implemented on touch listener with ACTION_MOVE,
                        //we have to check if the previous action was ACTION_DOWN
                        //to identify if the user clicked the view or not.


                        if (lastAction == MotionEvent.ACTION_DOWN) {
                            //Open the chat conversation click.

                            //close the service and remove the chat heads


                            windowManager.removeView(callHeaderView);
                            addMainLayout();
                            AppController.getInstance().setCallMinimized(false);
                        }
                        lastAction = event.getAction();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        //Calculate the X and Y coordinates of the view.


                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);


                        if ((int) (event.getRawX() - initialTouchX) == 0 && (int) (event.getRawY() - initialTouchY) == 0) {
                            lastAction = 0;
                        } else {


                            lastAction = event.getAction();
                        }

                        try {
                            //Update the layout with new X & Y coordinate
                            windowManager.updateViewLayout(callHeaderView, params);
                        } catch (NullPointerException e) {

                            windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
                            windowManager.updateViewLayout(callHeaderView, params);
                        }
                        return true;
                }
                return false;
            }
        });
        /*
         *To initiate chat while on the call
         *
         */
        initiateChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                windowManager.removeView(audioCallView);
                windowManager.addView(callHeaderView, params);

                /*
                 * To open the chats fragment,we add to the container
                 */
                String docId = AppController.findDocumentIdOfReceiver(caller_id, Utilities.tsInGmt(),
                        tvCallerName.getText().toString(), imageUrl, "", false,
                        intent.getExtras().getString("callerIdentifier"), "", false);


                AppController.getInstance().setCallMinimized(true);
                AppController.getInstance().setFirstTimeAfterCallMinimized(true);

//                if (AppController.getInstance().getActiveActivitiesCount() == 0) {


                    try {
                        Intent intent = new Intent(mContext, ChatMessageActivity.class);

                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("receiverUid", caller_id);
                        intent.putExtra("receiverName", tvCallerName.getText().toString());
                        intent.putExtra("documentId", docId);

                        intent.putExtra("receiverImage", imageUrl);
                        intent.putExtra("colorCode", AppController.getInstance().getColorCode(5));

                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


//                } else {
//                    JSONObject obj = new JSONObject();
//                    try {
//                        obj.put("eventName", "callMinimized");
//
//                        obj.put("receiverUid", caller_id);
//                        obj.put("receiverName", tvCallerName.getText().toString());
//                        obj.put("documentId", docId);
//                        obj.put("receiverImage", imageUrl);
//                        obj.put("colorCode", AppController.getInstance().getColorCode(5));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                    bus.post(obj);
//                }

            }
        });
    }

    @SuppressWarnings("TryWithIdenticalCatches")
    public void turnOnScreen() {

        try {
            if (!AppController.getInstance().isCallMinimized()) {

                WindowManager.LayoutParams params;
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    params = new WindowManager.LayoutParams();
                    params.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                    params.screenBrightness = 1;
                    params.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
                }
                else{
                    params = new WindowManager.LayoutParams();
                    params.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                    params.screenBrightness = 1;
                    params.type = WindowManager.LayoutParams.TYPE_PHONE;
                }
                if(windowManager == null)
                    windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
                windowManager.updateViewLayout(audioCallView, params);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("TryWithIdenticalCatches")
    public void turnOffScreen() {


        try {

            if (!AppController.getInstance().isCallMinimized()) {
                final WindowManager.LayoutParams params;
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    params = new WindowManager.LayoutParams();
                    params.screenBrightness = 0;
                    params.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
                }
                else{
                    params = new WindowManager.LayoutParams();
                    params.screenBrightness = 0;
                    params.type = WindowManager.LayoutParams.TYPE_PHONE;
                }
                if(windowManager != null)
                    windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
                windowManager.updateViewLayout(audioCallView, params);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void addMainLayout() {

//        if (params == null) {
//                params = new WindowManager.LayoutParams(
//                    WindowManager.LayoutParams.MATCH_PARENT,
//                    WindowManager.LayoutParams.MATCH_PARENT,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
//                    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD,
//                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED,
//                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    params.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
//                }
//                else {
//                    params.type = WindowManager.LayoutParams.TYPE_PHONE;
//                }
//        }
//        //Add the view to the window
//
//        if (windowManager == null) {
//            windowManager = (WindowManager)getSystemService(WINDOW_SERVICE);
//        }
//        windowManager.addView(audioCallView, params);
        //windowManager.addView(audioCallView,params);

        WindowManager.LayoutParams topLeftParams;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            topLeftParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, PixelFormat.RGB_565);
        }else {
            topLeftParams = new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.TYPE_PHONE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, PixelFormat.RGB_565);
        }
        topLeftParams.gravity = Gravity.CENTER;
        topLeftParams.x = 0;
        topLeftParams.y = 0;
        topLeftParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        topLeftParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        windowManager.addView(audioCallView, topLeftParams);
    }

    private void setupCallHeaderDuration() {

        stopWatchHeader.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer arg0) {
                countUpHeader = (SystemClock.elapsedRealtime() - arg0.getBase()) / 1000;

                callDurationHeader = String.format(Locale.US, "%02d:%02d:%02d", countUpHeader / 3600, countUpHeader / 60, countUpHeader % 60);


                try {
                    callHeaderTv.setText(callDurationHeader);


                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });

        stopWatchHeader.setBase(SystemClock.elapsedRealtime());
        stopWatchHeader.start();

    }
}
