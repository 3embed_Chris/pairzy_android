package com.pairzy.com.home.Prospects;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
/**
 * <h2>ProspectsContract</h2>
 * <P>
 *
 * </P>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface ProspectsContract
{
    interface View extends BaseView
    {
        void showCoinBalance(String coinBalance);
    }

    interface Presenter extends BasePresenter<View>
    {

    }
}
