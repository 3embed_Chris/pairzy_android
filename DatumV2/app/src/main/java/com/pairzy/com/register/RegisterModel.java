package com.pairzy.com.register;

import android.os.Bundle;
import android.text.TextUtils;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigResponse;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.DeviceUuidFactory;
import com.pairzy.com.util.Utility;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
/**
 * @since  2/21/2018.
 */
class RegisterModel  extends BaseModel
{
    @Inject
    Utility utility;
    @Inject
    DeviceUuidFactory deviceUuidFactory;
    @Inject
    CoinConfigWrapper coinConfigWrapper;

    @Inject
    RegisterModel(){}

    /**
     * @param phno : country code.
     * @param country_code : mobile_number
     * @param otp : entered otp data.
     * @return parameters
     */
    Map<String, Object> signUpData(Bundle data,String country_code,String phno,String otp)
    {
        String email=data.getString(RegisterContact.Presenter.EMAIL_DATA);
        String name=data.getString(RegisterContact.Presenter.NAME_DATA);
        Double dob_data=data.getDouble(RegisterContact.Presenter.DOB_DATA);
        String pic_data=data.getString(RegisterContact.Presenter.PIC_DATA);
        String video_data=data.getString(RegisterContact.Presenter.VIDEO_DATA);
        String video_thumb=data.getString(RegisterContact.Presenter.VIDEO_THUMB_DATA);
        double latitude=data.getDouble(RegisterContact.Presenter.LAT);
        double longitude=data.getDouble(RegisterContact.Presenter.LNG);
        int gender=data.getInt(RegisterContact.Presenter.GENDER_DATA);
        Long dob=dob_data.longValue();
        String required_phno=country_code+phno;
        Integer otp_data=Integer.parseInt(otp);
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.signUpKey.COUNTRY_CODE,country_code);
        map.put(ApiConfig.signUpKey.PHONE_NUMBER,required_phno);
        map.put(ApiConfig.signUpKey.OTP,otp_data);
        map.put(ApiConfig.signUpKey.EMAIL,email);
        map.put(ApiConfig.signUpKey.FIRST_NAME,name);
        map.put(ApiConfig.signUpKey.DOB,dob);
        map.put(ApiConfig.signUpKey.GENDER,gender);
        map.put(ApiConfig.signUpKey.PROFILE_PIC,pic_data);
        map.put(ApiConfig.signUpKey.LATITUDE,latitude);
        map.put(ApiConfig.signUpKey.LONGITUDE,longitude);
        if(!TextUtils.isEmpty(video_data) && !TextUtils.isEmpty(video_thumb)) {
            map.put(ApiConfig.signUpKey.PROFILE_VIDEO, video_data);
            map.put(ApiConfig.signUpKey.VIDEO_THUMBNAIL, video_thumb);
        }
        map.put(ApiConfig.signUpKey.DEVICE_ID,deviceUuidFactory.getDeviceUuid());
        map.put(ApiConfig.signUpKey.DEVICE_MAKER,utility.getDeviceMaker());
        map.put(ApiConfig.signUpKey.DEVICE_MODEL,utility.getModel());
        map.put(ApiConfig.signUpKey.DEVICE_TYPE, ApiConfig.DeviceType.ANDROID);
        map.put(ApiConfig.signUpKey.DEVICE_OS, ""+android.os.Build.VERSION.RELEASE);
        map.put(ApiConfig.signUpKey.APP_VERSION, utility.getAppCurrentVersion());
        return map;
    }

    public void parseCoinConfig(String response) {
        try
        {
            CoinConfigResponse coinConfigResponse =
                    utility.getGson().fromJson(response,CoinConfigResponse.class);
            if(!coinConfigResponse.getData().isEmpty())
                coinConfigWrapper.setCoinData(coinConfigResponse.getData().get(0));
        } catch (Exception e){}
    }
}
