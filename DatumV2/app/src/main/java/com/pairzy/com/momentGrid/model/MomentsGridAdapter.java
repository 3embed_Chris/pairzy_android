package com.pairzy.com.momentGrid.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.R;
import com.pairzy.com.data.local.PreferencesHelper;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MomentsGridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<MomentsData> list;
    private Context mContext;
    private MomentClickCallback clickCallback;
    private ItemActionCallBack vhCallback;

    @Inject
    PreferencesHelper data;

    public MomentsGridAdapter(Context context, List<MomentsData> list)
    {
        this.list= (ArrayList<MomentsData>) list;
        this.mContext =context;
        initCallback();
    }
    public void setClickCallback(MomentClickCallback clickCallback){
        this.clickCallback = clickCallback;
    }

    private void initCallback() {
        vhCallback = new ItemActionCallBack() {
            @Override
            public void onClick(int id, int position) {
                switch (id){
                    case R.id.card_view:
                        if(clickCallback != null)
                            clickCallback.onMomentClick(position);
                        break;
                }
            }
        };
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.moments_list_item,parent, false);
        return new MomentsHolder(view,vhCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        MomentsHolder momentsHolder= (MomentsHolder) holder;
        ArrayList<String> postUrls = list.get(position).getUrl();
        if(postUrls != null && !postUrls.isEmpty()){
            String previewUrl = postUrls.get(0);
            if(previewUrl.contains(".mp4") || previewUrl.contains(".mov")) {
                momentsHolder.videoIcon.setVisibility(View.VISIBLE);
                try {
                    String uri = previewUrl.replace("upload/", "upload/vs_20,dl_200,h_200,e_loop/").replace(".mp4", ".gif").replace(".mov",".gif");
                    //Log.d(ListAdapter.class.getName(),"gif url "+uri);
                    DraweeController controller = Fresco.newDraweeControllerBuilder()
                            .setUri(uri)
                            .setAutoPlayAnimations(true)
                            .build();
                    momentsHolder.sdvPostPreview.setController(controller);
                }catch (Exception e){
                    previewUrl = previewUrl.replace(".mp4",".jpg").replace(".mov",".jpg");
                    momentsHolder.sdvPostPreview.setImageURI(previewUrl);
                }
            }else{
                momentsHolder.sdvPostPreview.setImageURI(previewUrl);
                momentsHolder.videoIcon.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public int getItemCount() {
        return  list.size();
    }

}
