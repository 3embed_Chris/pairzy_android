package com.pairzy.com.MqttChat.Giphy;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import com.pairzy.com.AppController;
import com.pairzy.com.R;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;


public class GifPlayer extends AppCompatActivity {

    private ImageView giphyIv;
    private Bus bus = AppController.getBus();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gif_player);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        giphyIv = (ImageView) findViewById(R.id.giphyIV);
        setUpActivity(getIntent());


        ImageView close = (ImageView) findViewById(R.id.close);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        bus.register(this);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);


        setUpActivity(intent);
    }

@SuppressWarnings("TryWithIdenticalCatches")
    private void setUpActivity(Intent intent) {


        String gifUrl = intent.getStringExtra("gifUrl");

        try {

            Glide.with(this)
                    .asGif()
                    .load(gifUrl)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)

                    .transition(withCrossFade())
                    .into(giphyIv);

        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onBackPressed() {

try{
        if (AppController.getInstance().isActiveOnACall()) {
            if (AppController.getInstance().isCallMinimized()) {
                super.onBackPressed();
                supportFinishAfterTransition();
            }
        } else {
            super.onBackPressed();
            supportFinishAfterTransition();
        }
}catch(Exception e){e.printStackTrace();}
    }

    private void minimizeCallScreen(JSONObject obj) {
        try {
            Intent intent = new Intent(GifPlayer.this, ChatMessageActivity.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra("receiverUid", obj.getString("receiverUid"));
            intent.putExtra("receiverName", obj.getString("receiverName"));
            intent.putExtra("documentId", obj.getString("documentId"));

            intent.putExtra("receiverImage", obj.getString("receiverImage"));
            intent.putExtra("colorCode", obj.getString("colorCode"));

            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void getMessage(JSONObject object) {
        try {
            if (object.getString("eventName").equals("callMinimized")) {

                minimizeCallScreen(object);
            }

        } catch (
                JSONException e)
        {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


        bus.unregister(this);
    }
}
