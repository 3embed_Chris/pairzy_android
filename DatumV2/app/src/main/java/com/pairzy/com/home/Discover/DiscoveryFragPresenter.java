package com.pairzy.com.home.Discover;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.util.Log;
import com.pairzy.com.AppController;
import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.pairzy.com.MqttChat.Utilities.Utilities;
import com.pairzy.com.R;
import com.pairzy.com.data.model.BoostCountData;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.UserActionHolder;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Discover.GridFrg.GridDataViewContract;
import com.pairzy.com.home.Discover.ListFrg.ListDataViewFrgContract;
import com.pairzy.com.home.Discover.Model.DiscoveryModel;
import com.pairzy.com.home.Discover.Model.UserAssetData;
import com.pairzy.com.home.Discover.Model.UserItemPojo;
import com.pairzy.com.home.HomeModel.LoadMoreStatus;
import com.pairzy.com.home.HomeUtil;
import com.pairzy.com.locationScreen.model.LocationHolder;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.passportLocation.model.PassportLocation;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.CustomObserver.BoostEndObserver;
import com.pairzy.com.util.CustomObserver.BoostViewCountObserver;
import com.pairzy.com.util.CustomObserver.CoinBalanceObserver;
import com.pairzy.com.util.LocationProvider.LocationApiCallback;
import com.pairzy.com.util.LocationProvider.LocationApiManager;
import com.pairzy.com.util.LocationProvider.Location_service;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.SpendCoinDialog.CoinSpendDialogCallback;
import com.pairzy.com.util.SpendCoinDialog.WalletEmptyDialogCallback;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h2>DiscoveryFragPresenter</h2>
 * <P>
 *  collecting the preference of all the user.
 * </P>
 * @since  2/27/2018.
 * @author 3Emmbed.
 * @version 1.0.
 */
public class DiscoveryFragPresenter implements DiscoveryFragContract.Presenter,App_permission.Permission_Callback,Location_service.GetLocationListener,UserActionEventError
        ,CoinSpendDialogCallback,WalletEmptyDialogCallback
{
    @Named(HomeUtil.LOAD_MORE_STATUS)
    @Inject
    LoadMoreStatus loadMoreStatus;
    public static final int LIMIT=20;
    public static int PAGE_COUNT=0;
    @Inject
    NetworkStateHolder holder;
    private boolean isRefresh=false;
    @Inject
    ListDataViewFrgContract.Presenter list_presenter;
    @Inject
    GridDataViewContract.Presenter gridDataViewPresenter;
    @Inject
    Utility utility;
    @Inject
    DiscoveryModel model;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    NetworkService service;
    @Inject
    LocationApiManager locationApiManager;
    @Inject
    Location_service location_service;
    @Inject
    App_permission app_permission;
    @Inject
    Activity activity;
    @Inject
    CoinBalanceObserver coinBalanceObserver;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    CoinDialog spendCoinDialog;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    LoadingProgress loadingProgress;
    @Inject
    BoostViewCountObserver boostViewCountObserver;
    @Inject
    BoostEndObserver boostEndObserver;
    @Inject
    UserActionHolder userActionHolder;


    private DiscoveryFragContract.View view;
    private CompositeDisposable compositeDisposable;
    private DiscoveryFrag discoveryFrg;
    private Boolean updateCoinBalance = false;
    private UserItemPojo last_performed;
    private boolean isFromCard = false;
    private boolean isForSuperlike = true;
    private UserItemPojo userItemPojo;

    /*native add related var*/

    // The number of native ads to load and display.
    public static final int NUMBER_OF_ADS = 5;

    // The AdLoader used to load ads.
    private AdLoader adLoader;

    // List of native ads that have been successfully loaded.
    @Inject
    List<UnifiedNativeAd> mNativeAds;


    @Inject
    DiscoveryFragPresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }


    @Override
    public void takeView(Object view)
    {
        this.discoveryFrg= (DiscoveryFrag) view;
        this.view= (DiscoveryFragContract.View) view;
        initAssetObserver();
    }


    @Override
    public void dropView()
    {
        view=null;
        compositeDisposable.clear();
        last_performed=null;
    }

    private void loadNativeAds() {
        if(dataSource.getSubscription() != null)
            return;
        mNativeAds.clear();
        AdLoader.Builder builder = new AdLoader.Builder(activity, activity.getString(R.string.NATIVE_ADD_UNIT_ID));
        adLoader = builder.forUnifiedNativeAd(
                new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                    @Override
                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                        // A native ad loaded successfully, check if the ad loader has finished loading
                        // and if so, insert the ads into the list.
                        mNativeAds.add(unifiedNativeAd);
                        if (!adLoader.isLoading()) {
                            model.insertAdsInMenuItems();
                            list_presenter.updateDataChanged();
                            gridDataViewPresenter.updateDataChanged();
                        }
                    }
                }).withAdListener(
                new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        // A native ad failed to load, check if the ad loader has finished loading
                        // and if so, insert the ads into the list.
                        Log.e("MainActivity", "The previous native ad failed to load. Attempting to"
                                + " load another.");
                        if (!adLoader.isLoading()) {
                            model.insertAdsInMenuItems();
                        }
                    }
                }).build();
        // Load the Native Express ad.
        adLoader.loadAds(new AdRequest.Builder().build(), NUMBER_OF_ADS);
    }

    @Override
    public void initBoostEndObserver() {
        boostEndObserver.getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Boolean value) {
                        if(value){
                            gridDataViewPresenter.showBoostViewCounter(false,0);
                            showBoostViewCounter(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void startVideoPlayFirstItem() {
        gridDataViewPresenter.startVideoPlayFirstItem();
    }

    @Override
    public void initBoostViewCountObserver() {
        boostViewCountObserver.getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BoostCountData>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(BoostCountData value) {
                        if(value != null){
                            gridDataViewPresenter.showBoostViewCounter(true,value.getViews());
                            list_presenter.showBoostViewCounter(true,value.getViews());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }

    @Override
    public void showSearchView()
    {
        updateCurrentLocation(true);
        if(view != null)
            view.showUserSearchMessage("");
    }


    @Override
    public void updateCurrentLocation(boolean isRefresh)
    {
        this.isRefresh=isRefresh;
        if(isRefresh)
        {
            if(view != null)
                view.showUserSearchMessage("");
        }
        if(model.isPassportted())
        {
            PassportLocation currentLocation = model.getFeatureLocation();
            if(currentLocation != null)
            {
                updateUserCurrentLocation(currentLocation.getLatitude(),currentLocation.getLongitude(),true);
            }
            else
                askForLocationPermission();
        }else
        {
            askForLocationPermission();
        }
    }


    /*
     *Updating the user current location */
    private void updateUserCurrentLocation(double lat,double lng,boolean isPassported)
    {
        if(holder.isConnected())
        {
            service.updateLocation(dataSource.getToken(),model.getLanguage(),model.setUpdateLocationDetails(lat,lng,isPassported))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value)
                        {
                            try
                            {
                                if(value.code() == 200)
                                {
                                    getUsers();
                                }
                                else if(value.code() == 401){
                                    AppController.getInstance().appLogout();
                                }
                                else
                                {
                                    if(view!=null)
                                        view.showError(model.getError(value));
                                    //extra added
                                    if(view != null)
                                        view.hideUserSearchMessage();
                                }
                            } catch (Exception e)
                            {
                                e.printStackTrace();
                                if(view!=null)
                                    view.showError(R.string.failed_update_location);
                                //extra added
                                if(view != null)
                                    view.hideUserSearchMessage();
                            }
                        }

                        @Override
                        public void onError(Throwable e)
                        {
                            if(view!=null)
                                view.showError(R.string.failed_update_location);
                            //extra added
                            if(view != null)
                                view.hideUserSearchMessage();
                        }
                        @Override
                        public void onComplete(){}
                    });
        }else
        {
            if(!model.checkUserExist() || this.isRefresh)
            {
                if(view!=null)
                    view.showNetworkError();
            }else
            {
                if(view!=null)
                    view.showError(R.string.internet_error_Text);
            }
        }
    }

    /*
     * Getting search user from the given user location.
     * */
    @Override
    public void getUsers()
    {
        if(holder.isConnected())
        {
            if(this.isRefresh)
            {
                loadMoreStatus.setNo_more_data(false);
                PAGE_COUNT=0;
                if(view != null)
                    view.showUserSearchMessage("");
            }else
            {
                PAGE_COUNT+=LIMIT;
            }

            this.isRefresh=false;
            onUsersReceived();
            Log.w("AUTH_TOKEN: ",dataSource.getToken());
            Log.w("USER_ID: ",dataSource.getUserId());
            service.getsearchResult(PAGE_COUNT,PAGE_COUNT+LIMIT, dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value)
                        {
                            try
                            {
                                if(value.code()==200)
                                {
                                    String responseData=value.body().string();
                                    model.parseUserData(responseData);


                                    list_presenter.updateDataChanged();
                                    gridDataViewPresenter.updateDataChanged();
                                    if(view!=null&&model.checkUserExist()) {
                                        view.user_found();
                                        //loadNativeAds();
                                    }
                                    else{
                                        if(view != null)
                                            view.noUserFound();
                                    }

                                    //show boost counter
//                                    gridDataViewPresenter.showBoostViewCounter(model.isProfileBoosted(),getBoostViewCount());
//                                    list_presenter.showBoostViewCounter(model.isProfileBoosted(),getBoostViewCount());
                                }
                                else if(value.code() == 401){
                                    AppController.getInstance().appLogout();
                                }
                                else
                                {
                                    if( model.handleFailedLoadMore())
                                        list_presenter.updateDataChanged();

                                    if(view!=null)
                                        view.showError(model.getError(value));

                                    //added extra
                                    if(view != null)
                                        view.hideUserSearchMessage();
                                }
                            } catch (Exception e)
                            {
                                if( model.handleFailedLoadMore())
                                    list_presenter.updateDataChanged();
                                if(view!=null)
                                    view.showError(R.string.failed_get_members);

                                //added extra
                                if(view != null)
                                    view.hideUserSearchMessage();
                            }
                        }

                        @Override
                        public void onError(Throwable e)
                        {
                            e.printStackTrace();
                            if(view!=null&&model.checkUserExist())
                                view.user_found();
                            else{
                                if(view != null)
                                    view.noUserFound();
                            }

                            if( model.handleFailedLoadMore())
                                list_presenter.updateDataChanged();

                            if(view!=null)
                                view.showError(R.string.failed_get_members);

                            if(view != null)
                                view.hideUserSearchMessage();
                        }
                        @Override
                        public void onComplete() {}
                    });
        }else
        {
            if(!model.checkUserExist() || this.isRefresh)
            {
                if(view!=null)
                    view.showNetworkError();
            }else
            {
                if(view!=null)
                    view.showError(R.string.internet_error_Text);
            }
        }
    }

    @Override
    public void onUsersReceived()
    {}

    @Override
    public void preFetchImage(int position)
    {
        model.prefetchImage(position);
    }


    @Override
    public void doLike(UserItemPojo item)
    {
        if(!item.isAdView()) {
            last_performed = item;
            if (view != null)
                view.onLike(item.getOpponentId());
        }
    }


    @Override
    public void doDisLike(UserItemPojo item)
    {
        if(!item.isAdView()){
            last_performed = item;
            if (view != null)
                view.onDislike(item.getOpponentId());
        }
    }


    @Override
    public void doSuperLike(UserItemPojo item)
    {
        if(!item.isAdView()) {
            last_performed = item;
            if (view != null)
                view.onSuperLike(item.getOpponentId());
        }
    }


    @Override
    public void askForLocationPermission()
    {
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.LOCATION);
        app_permission.getPermission_for_Sup_v4Fragment("location",permissions,discoveryFrg,this);
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag)
    {
        getUserLocationFromService();
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag)
    {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        app_permission.show_Alert_Permission(activity.getString(R.string.location_access_text),activity.getString(R.string.location_acess_subtitle),
                activity.getString(R.string.location_acess_message),stringArray);
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag)
    {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag,boolean parmanent)
    {
        getUserLocationFromApi();
    }

    /*
     * getting user location from GPS*/
    private void getUserLocationFromService()
    {
        location_service.setListener(this);
        location_service.checkLocationSettings();
    }

    /*
     * getting user location from link*/
    @Override
    public void getUserLocationFromApi()
    {
        locationApiManager.getLocation(new LocationApiCallback()
        {
            @Override
            public void onSuccess(Double lat, Double lng, double accuracy)
            {
                LocationHolder locationHolder = new LocationHolder();
                locationHolder.setLatitude(lat);
                locationHolder.setLongitude(lng);
                PassportLocation currentLocation = model.getLocationName(locationHolder);
                model.saveCurrentLocation(currentLocation);
                updateUserCurrentLocation(lat,lng,false);

            }

            @Override
            public void onError(String error)
            {
                if(view!=null)
                    view.showError(error);
                if(view != null)
                    view.hideUserSearchMessage();
            }
        });
    }


    @Override
    public void updateLocation(Location location)
    {
        if(location!=null)
        {
            location_service.stop_Location_Update();
            LocationHolder holder = new LocationHolder();
            holder.setLatitude(location.getLatitude());
            holder.setLongitude(location.getLongitude());
            updateUserCurrentLocation(location.getLatitude(), location.getLongitude(),false);
            PassportLocation currentLocation = model.getLocationName(holder);
            model.saveCurrentLocation(currentLocation);

        }
    }

    @Override
    public void location_Error(String error)
    {
        getUserLocationFromApi();
    }

    /**
     * initialization of the like,Dislike and super like event
     */
    private void initAssetObserver()
    {
        if(view!=null)
            view.updateListener(this);
    }

    @Override
    public void onRewind()
    {
        handleAdShow();
//        if(dataSource.getSubscription() != null)
//        {
            doRewindLastAction(true);
//        }
//        else{
//            if(view != null)
//                view.openBootsDailoag();
//        }

    }


    private void launchSpendCoinDialogForChat() {
        Integer coinSpend = coinConfigWrapper.getCoinData().getPerMsgWithoutMatch().getCoin();
        String btnText = String.format(Locale.ENGLISH, " I am OK to spend %s coins", coinSpend);
        spendCoinDialog.showCoinSpendDialog(activity.getString(R.string.chat_spend_coin_title), activity.getString(R.string.spend_coin_on_chat_dialog_msg),
                btnText, this);
    }


    private void initiateSuperlike() {
        if(view!=null) {
            if(this.isFromCard) {
                gridDataViewPresenter.initiateSuperlike();
            }
            else{
                list_presenter.initiateSuperlike();
            }
        }
    }


    /*
     *Doing api call*/
    private void doRewindLastAction(boolean isShowProgress)
    {

        if(networkStateHolder.isConnected())
        {
            if(isShowProgress)
                loadingProgress.show();

            service.reWind(dataSource.getToken(),model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> value)
                        {
                            loadingProgress.cancel();
                            if (value.code()==200)
                            {
                                try
                                {
                                    String response=value.body().string();
                                    Log.d("45t6", ":"+response);
                                    UserItemPojo rewind_data=model.parseRewindResponse(response);
                                    model.manageMediaList(rewind_data);
                                    activity.runOnUiThread(() -> {
                                        if(view!=null)
                                        {
                                            if(view.getPagePosition()==0)
                                            {
                                                gridDataViewPresenter.revertAction(rewind_data);
                                            }else
                                            {
                                                list_presenter.revertAction(rewind_data);
                                            }
                                        }
                                    });

                                }catch (Exception e)
                                {}
                            }else if(value.code() == 204)
                            {
                                if (view != null)
                                    view.showError("Swipe to like or dislike!");
                            }else if(value.code() == 402)
                            {
                                activity.runOnUiThread(() -> {
                                    if(view!=null)
                                        view.openBootsDailoag();
                                });
                            }else if(value.code() == 401)
                            {
                                AppController.getInstance().appLogout();
                            } else
                            {
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                } catch (Exception ignored){}
                            }
                        }
                        @Override
                        public void onError(Throwable e)
                        {
                            loadingProgress.cancel();

                        }
                        @Override
                        public void onComplete(){}
                    });
        } else
        {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void openChat(UserItemPojo userItemPojo)
    {
        this.userItemPojo = userItemPojo;
        isForSuperlike = false;
        initChat();
    }

    @Override
    public void showBoostViewCounter(boolean show) {
        gridDataViewPresenter.showBoostViewCounter(show,getBoostViewCount());
        list_presenter.showBoostViewCounter(show,getBoostViewCount());
    }

    private int getBoostViewCount() {
        return dataSource.getDuringBoostViewCount();
    }

    @Override
    public void startCoinAnimation() {
        if(discoveryFrg.getPagePosition() == 0) { //grid
            gridDataViewPresenter.startCoinAnimation();
        }
        else {
            list_presenter.startCoinAnimation();
        }
    }

    @Override
    public void saveCurrentBoostViewCount(int currentViewCount) {
//        if (dataSource.getBoostExpireTime() > 0)
//            dataSource.setDuringBoostViewCount(currentViewCount);
//        else{
//            dataSource.setDuringBoostViewCount(0);
//        }
    }


    private void initChat() {
        //assuming matched user not appear in the list.
        if(model.isEnoughWalletBalanceToChat()){
            if(model.isDialogDontNeedToShowForChat()){
                launchChat();
            }
            else{
                //launch spend coin dialog for chat.
                launchSpendCoinDialogForChat();
            }
        }
        else{
            //launch empty coin dialog for chat.
            launchWalletEmptyDialogForChat();
        }
    }

    private void launchWalletEmptyDialogForChat(){
        String msg = String.format(Locale.ENGLISH , "%s %s %s %s",activity.getString(R.string.you_need_atleast_text),
                model.getCoinForUnmatchChat(),
                activity.getString(R.string.proactive_empty_wallet_sub_msg),
                utility.formatCoinBalance(model.getCoinBalance())+".");
        spendCoinDialog.showWalletEmptyDialog(activity.getString(R.string.proactive_wallet_empty_title),msg,this);
    }

    private void launchChat(){
        if(userItemPojo != null)
        {
            Intent intent = new Intent(activity, ChatMessageActivity.class);
            intent.putExtra("chatId","");
            intent.putExtra("receiverUid", userItemPojo.getOpponentId());
            intent.putExtra("receiverName", userItemPojo.getFirstName());
            String docId = AppController.getInstance().findDocumentIdOfReceiver(userItemPojo.getOpponentId(), "");
            boolean initiated = false;
            boolean hasDefaultMessage = true;
            if (docId.isEmpty()) {
                docId = AppController.findDocumentIdOfReceiver(userItemPojo.getOpponentId(), Utilities.tsInGmt(), userItemPojo.getFirstName(),
                        userItemPojo.getProfilePic(), "", false, userItemPojo.getOpponentId(), "", false);
                initiated = true;
            }else{
                intent.putExtra("chatId",AppController.getInstance().getDbController().getChatId(docId));
                Map<String, Object> chatInfo = AppController.getInstance().getDbController().getChatInfo(docId);
                if(chatInfo.containsKey("initiated"))
                    initiated = (boolean) chatInfo.get("initiated");
                hasDefaultMessage = (boolean)chatInfo.get("hasDefaultMessage");
            }
            intent.putExtra("documentId", docId);
            intent.putExtra("receiverIdentifier", userItemPojo.getOpponentId());
            intent.putExtra("receiverImage", userItemPojo.getProfilePic());
            intent.putExtra("colorCode", AppController.getInstance().getColorCode(0 % 19));
            if (view != null) {

                AppController.getInstance().getDbController().updateChatUserData(
                        docId,
                        userItemPojo.getFirstName(),
                        userItemPojo.getProfilePic(),
                        false,
                        userItemPojo.isSuperlikedMe() > 0,
                        initiated,
                        hasDefaultMessage
                );
                AppController.getInstance().getDbController().updateChatUserOnline(docId, userItemPojo.getOnlineStatus() > 0);
                view.launchChatScreen(intent);
                view.setNeedToUpdateChat(true);
            }
        }
    }

    @Override
    public void onRevertAction(UserAssetData value)
    {
        if(last_performed!=null&&last_performed.getOpponentId().equals(value.getId()))
        {
            if(value.isError())
            {
                if(view!=null)
                {
                    if(view.getPagePosition() == 0)
                    {
                        gridDataViewPresenter.revertAction(last_performed);
                    }else
                    {
                        list_presenter.revertAction(last_performed);
                    }
                }
            }
            last_performed=null;
        }
    }

    @Override
    public void onError(UserAssetData userAssetData)
    {}

    /*
     * coin dialog callback
     */
    @Override
    public void onOkToSpendCoin(boolean dontShowAgain) {
        launchChat();
        model.updateShowCoinDialogForChat(dontShowAgain);
    }


    /*
     * coin dialog callback
     */
    @Override
    public void onByMoreCoin() {
        if(view != null)
            view.launchCoinWallet();
    }

    @Override
    public void handleAdShow() {
        userActionHolder.incrementUserActionCount();
        if( userActionHolder.canShowAds()){
            if(view != null)
                view.showLoadedProfileAds();
        }
    }
}
