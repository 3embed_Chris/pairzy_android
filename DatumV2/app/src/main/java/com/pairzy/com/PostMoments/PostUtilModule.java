package com.pairzy.com.PostMoments;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.util.progressbar.LoadingProgress;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ankit on 27/2/18.
 */

@Module
public class PostUtilModule {

    @ActivityScoped
    @Provides
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }
}
