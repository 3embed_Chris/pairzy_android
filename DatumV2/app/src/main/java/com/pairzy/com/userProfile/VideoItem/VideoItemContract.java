package com.pairzy.com.userProfile.VideoItem;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
/**
 * @since  4/4/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface VideoItemContract
{
    interface View extends BaseView
    {

    }

    interface Presenter extends BasePresenter<View>
    {

    }
}
