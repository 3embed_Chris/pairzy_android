package com.pairzy.com.util.progressbar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import com.pairzy.com.R;
/**
 * <h2>LoadingProgress</h2>
 * <p>
 * Progress dialog for the loading thing.
 * </P>
 *
 * @author 3Embed.
 * @version 1.0.
 * @since 12/13/2017.
 */
public class LoadingProgress implements CircleDialog
{
    private boolean isTitle = false;
    private Dialog progress_bar = null;
    private TextView progress_title = null;
    private Activity activity;

    public  LoadingProgress(Activity activity)
    {
        this.activity = activity;
        init(activity);
    }

    /*
     *inti dialog content*/
    private void init(Activity mActivity)
    {
        progress_bar = new Dialog(mActivity, android.R.style.Theme_Translucent);
        progress_bar.setCancelable(false);
        progress_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        @SuppressLint("InflateParams")
        View dialogView = LayoutInflater.from(mActivity).inflate(R.layout.app_circular_progrss_layout, null);
        progress_title = dialogView.findViewById(R.id.progress_title);
        progress_bar.setContentView(dialogView);
        progress_bar.setCancelable(true);
        progress_bar.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                //Canceled..
            }
        });
    }


    @Override
    public void show() {
        if (progress_bar != null) {
            if (progress_bar.isShowing()) {
                progress_bar.dismiss();
            }
            progress_bar.show();
        }
    }

    @Override
    public void cancel() {
        if (progress_bar.isShowing()) {
            progress_bar.dismiss();
        }
    }

    @Override
    public void setTitle(final String title) {
        if (progress_title != null && title != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progress_title.setText(title);
                }
            });

        }
    }

    @Override
    public void setTitleColor(final int id) {
        if (progress_title != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progress_title.setTextColor(id);
                }
            });
        }
    }

    @Override
    public void setTypeface(final Typeface title_text_style) {
        if (progress_title != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progress_title.setTypeface(title_text_style);
                }
            });
        }
    }

    @Override
    public void showTitle(boolean isVisible) {
        isTitle = isVisible;
    }

    @Override
    public void isCancelable(boolean isCancel) {
        if (progress_bar != null) {
            progress_bar.setCancelable(isCancel);
        }
    }
}
