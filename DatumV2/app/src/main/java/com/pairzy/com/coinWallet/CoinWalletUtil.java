package com.pairzy.com.coinWallet;

import android.app.Activity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;

import com.pairzy.com.coinWallet.Model.CoinHistoryPagerAdapter;
import com.pairzy.com.coinWallet.Model.CoinPojo;
import com.pairzy.com.dagger.ActivityScoped;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * <h>CoinWalletUtil</h>
 * <p>provide the instance of helper class.</p>
 * @author 3Embed.
 * @since 30/5/18.
 */
@Module
public class CoinWalletUtil {

    public static final String COIN_ALL_LIST ="all_coin_list",
            COIN_IN_LIST = "coin_in_list", COIN_OUT_LIST = "coin_out_list";

    @ActivityScoped
    @Provides
    ArrayList<Fragment>  provideFragmentList(){
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    FragmentManager provideFragmentManager(Activity activity){
        return ((AppCompatActivity)activity).getSupportFragmentManager();
    }
    @ActivityScoped
    @Provides
    CoinHistoryPagerAdapter provideViewPagerAdapter(ArrayList<Fragment> fragmentList, FragmentManager fragmentManager){
        return new CoinHistoryPagerAdapter(fragmentList,fragmentManager);
    }

    @Named(COIN_ALL_LIST)
    @ActivityScoped
    @Provides
    ArrayList<CoinPojo>  provideAllCoinList(){
        return new ArrayList<>();
    }

    @Named(COIN_IN_LIST)
    @ActivityScoped
    @Provides
    ArrayList<CoinPojo>  provideInCoinList(){
        return new ArrayList<>();
    }

    @Named(COIN_OUT_LIST)
    @ActivityScoped
    @Provides
    ArrayList<CoinPojo>  provideOutCoinList(){
        return new ArrayList<>();
    }


}
