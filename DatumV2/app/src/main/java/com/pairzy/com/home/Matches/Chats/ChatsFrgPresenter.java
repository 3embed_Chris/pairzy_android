package com.pairzy.com.home.Matches.Chats;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;

import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.pairzy.com.AppController;
import com.pairzy.com.MqttChat.Utilities.MqttEvents;
import com.pairzy.com.MqttChat.Utilities.Utilities;
import com.pairzy.com.MqttManager.MessageType;
import com.pairzy.com.MqttManager.Model.MqttMessage;
import com.pairzy.com.MqttManager.MqttRxObserver;
import com.pairzy.com.R;
import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Matches.Chats.Model.ChatAdapterItemCallback;
import com.pairzy.com.home.Matches.Chats.Model.ChatListData;
import com.pairzy.com.home.Matches.Chats.Model.ChatListItem;
import com.pairzy.com.home.Matches.Chats.Model.ChatListModel;
import com.pairzy.com.home.Matches.Chats.Model.MatchAdapterCallback;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.ChatOptionDialog.ChatOptionCallBack;
import com.pairzy.com.util.ChatOptionDialog.ChatOptionDialog;
import com.pairzy.com.util.CustomObserver.CoinBalanceObserver;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.Exception.NoMoreDataException;
import com.pairzy.com.util.ReportUser.ReportUserCallBack;
import com.pairzy.com.util.ReportUser.ReportUserDialog;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.SpendCoinDialog.CoinSpendDialogCallback;
import com.pairzy.com.util.SpendCoinDialog.WalletEmptyDialogCallback;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.progressbar.LoadingProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ChatsFrgPresenter implements ChatsFrgContract.Presenter,MatchAdapterCallback,ChatAdapterItemCallback,ReportUserCallBack,ChatOptionCallBack, CoinSpendDialogCallback
    , WalletEmptyDialogCallback
{

    private ChatsFrgContract.View view;
    private static String TAG = ChatsFrgPresenter.class.getSimpleName();

    @Inject
    ChatListModel model;
    @Inject
    NetworkService service;
    @Inject
    NetworkStateHolder holder;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Activity activity;
    @Inject
    ReportUserDialog reportUserDialog;
    @Inject
    LoadingProgress loadingProgress;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    ChatOptionDialog chatOptionDialog;
    @Inject
    CoinBalanceObserver coinBalanceObserver;
    @Inject
    Utility utility;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    MqttRxObserver mqttRxObserver;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    CoinDialog spendCoinDialog;


    private CompositeDisposable compositeDisposable;
    private ChatListItem chatListItem;
    private int currentPosition = -1;
    private Boolean isFromMatchList = true;

    @Inject
    ChatsFrgPresenter()
    {
        compositeDisposable=new CompositeDisposable();

    }

    @Override
    public void observeMatchAndUnmatchUser() {
        mqttRxObserver.getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<MqttMessage>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(MqttMessage value) {
                        if(value != null && value.getData() != null){
                            try {
                                if (value.getData().has("messageType") && value.getData().getString("messageType").equals(MessageType.MATCH)) {
                                    try {
                                        ChatListData chatListData = model.parseMatchedMessage(value.getData());
                                        boolean isAddedToMatchList = model.addNewMatchedUser(chatListData);
                                        if(isAddedToMatchList){
                                            if(view != null)
                                                view.notifyMatchList(); //1 is first actual matchList position.
                                        }
                                        else{
                                            if(view != null)
                                                view.notifyChatList();
                                        }
                                        showValidLayout();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                else if ( value.getData().has("messageType") && value.getData().getString("messageType").equals(MessageType.UN_MATCH)){
                                    Log.w("UN_MATCH RECEIVED:", value.getData().toString());
                                    String userId = model.parseUserId(value.getData().toString());
                                    if(!TextUtils.isEmpty(userId)){
                                        int pos = -1;
                                        pos = model.removeFromMatchList(userId);
                                        if(pos != -1){
                                            if(view != null)
                                                view.notifyMatchList(pos);
                                        }
                                        pos = -1;
                                        pos = model.removeFromChatList(userId);
                                        if(pos != -1){
                                            if(view != null)
                                                view.notifyChatList(pos);
                                        }
                                        showValidLayout();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void showValidLayout() {

        if(view!=null)
            view.onNoMatchFound();

        if(view != null)
            view.onMatchedFound();

        if (model.isMatchListEmpty() && model.isChatListEmpty()) {
            if (view != null)
                view.onNoMatchFound();
        } else if (model.isMatchListEmpty() && !model.isChatListEmpty()) {
            if (view != null)
                view.showMatchList(false);
        } else if (!model.isMatchListEmpty()) {
            if (view != null)
                view.showMatchList(true);
        }

        if (view != null)
            view.showEmptyActiveChatLayout(model.isChatListEmpty());

        if(view != null)
            view.updateChatBadgeCount(""/*model.getUnreadChatCount()*/);
    }

    @Override
    public void takeView(ChatsFrgContract.View view)
    {
        this.view= view;
    }

    @Override
    public void dropView()
    {
        compositeDisposable.clear();
        view=null;
    }

    @Override
    public void getMatchedUser()
    {
        if(!holder.isConnected())
        {
            if(view!=null)
                view.onInternetError();
        }else
        {
            if(view != null)
                view.showLoadingView();

            service.getChats(dataSource.getToken(),model.getLanguage(), "0")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> value)
                        {
                            try
                            {
                                if(value.code() == 200)
                                {

                                    String data = value.body().string();
                                    model.parseResponse(data);

                                }
                                else if(value.code() == 401){
                                    AppController.getInstance().appLogout();
                                }
                                else if(value.code() == 412 || value.code() == 404 )
                                {
                                    if(view!=null)
                                        view.onNoMatchFound();
                                }else
                                {
                                    if(view!=null)
                                        view.onMatchedApiFailed(model.getError(value));
                                }

                            }catch (EmptyData ignored)
                            {
                            }catch (Exception e)
                            {
                                if(view!=null)
                                    view.onMatchedApiFailed(e.getMessage());
                            }

                            // need to organise notify
                            if(view != null)
                                view.notifyMatchList();
                            showValidLayout();
                        }
                        @Override
                        public void onError(Throwable e)
                        {
                            if(view!=null)
                                view.onMatchedApiFailed(activity.getString(R.string.failed_to_get_chat_user));
                        }

                        @Override
                        public void onComplete() {}
                    });
        }
    }


    private void launchChat(int position){
        this.currentPosition = position;
        ChatListData chatListItem = model.getMatchItem(position);
        if(chatListItem != null) {
            Intent intent = new Intent(activity, ChatMessageActivity.class);

            intent.putExtra("chatId", chatListItem.getChatId());
            intent.putExtra("receiverUid", chatListItem.getRecipientId());
            intent.putExtra("receiverName", chatListItem.getFirstName());

            String docId = AppController.getInstance().findDocumentIdOfReceiver(chatListItem.getRecipientId(), "");
            boolean initiated = false;
            if (docId.isEmpty()) {
                docId = AppController.findDocumentIdOfReceiver(chatListItem.getRecipientId(), Utilities.tsInGmt(),chatListItem.getFirstName(),
                        chatListItem.getProfilePic(), "", false, chatListItem.getRecipientId(), chatListItem.getChatId(), false);
                initiated = true;
                AppController.getInstance().getDbController().updateChatUserData(
                        docId,
                        chatListItem.getFirstName(),
                        chatListItem.getProfilePic(),
                        chatListItem.getIsMatchedUser() > 0,
                        chatListItem.getSuperlikedMe()>0,
                        initiated,
                        chatListItem.isHasDefaultMessage()
                );
            }
            intent.putExtra("documentId", docId);
            intent.putExtra("receiverIdentifier", chatListItem.getRecipientId());
            intent.putExtra("receiverImage", chatListItem.getProfilePic());
            intent.putExtra("colorCode", AppController.getInstance().getColorCode(position % 19));

            if (view != null) {
                AppController.getInstance().getDbController().updateChatUserOnline(docId, chatListItem.getOnlineStatus() > 0);
                view.launchChatScreen(intent);
                view.setNeedToUpdateChat(true);
            }
        }
    }

    @Override
    public void openChat(int position)
    {
        ChatListData matchListItemPojo = model.getMatchItem(position);
        if(matchListItemPojo != null) {
            //launch chat option
            if(position == 0 && matchListItemPojo.isForBoost()){
                loadLikesScreen();
            }
            else {
                launchChatOptionDialog(matchListItemPojo.getFirstName(), position);
            }
        }
    }

    private void loadLikesScreen() {
        if(dataSource.getSubscription() == null){
            if(view != null)
                view.openBoostDialog();
        }
        else{
            //launch prospect or boosted then launch boostLike screen
            if(dataSource.getBoostExpireTime() > 0 && dataSource.getBoostExpireTime() > System.currentTimeMillis()){
                //open likes screen
                if(view != null)
                    view.openBoostLikeList();
            }
            else {
//                if (view != null)
//                    view.openProspectScreen();
                //insted go for check and boost sub dialog
                //it their in home presenter
                if(view != null)
                    view.checkAndLaunchProfileBoostDialog();
            }
        }
    }

    private void launchChatOptionDialog(String userName , int position){
        chatOptionDialog.showDialog(userName,position, this);
    }

    @Override
    public void initiateChat(int position) {
        isFromMatchList = false;
        currentPosition = position;
        chatListItem = model.getChatItem(position);
        if(chatListItem != null){
            loadSpendCoinDialog();
        }
    }

    private void loadSpendCoinDialog() {
        boolean isMatchedUser = false;
        boolean isInitiated = false;
        if(chatListItem != null){
            isMatchedUser = chatListItem.isMatchedUser();
            isInitiated = chatListItem.isInitiated();
        }

        if(/*model.isDialogDontNeedToShow() ||*/ isMatchedUser || !isInitiated){  //coin not required

           // if(coinConfigWrapper.getCoinData() != null) {
                launchChat();
           // }
            /* else{
                callCoinConfigApi(false);
            }*/
        }
        else{  //coin requires

            if(model.isEnoughWalletBalanceToChat()){
                if(model.isDialogDontNeedToShowForChat()){
                    launchChat();
                }
                else{
                    //launch spend on chat dialog.
                    launchSpendCoinDialogForChat();
                }
            }
            else{
                //launch wallet empty dialog for chat.
                launchWalletEmptyDialogForChat();
            }
        }
    }

    private void launchSpendCoinDialogForChat() {
        Integer coinSpend = coinConfigWrapper.getCoinData().getPerMsgWithoutMatch().getCoin();
        String btnText = String.format(Locale.ENGLISH, " I am OK to spend %s coins", coinSpend);
        spendCoinDialog.showCoinSpendDialog(activity.getString(R.string.chat_spend_coin_title), activity.getString(R.string.spend_coin_on_chat_dialog_msg),
                btnText, this);
    }

    private void launchWalletEmptyDialogForChat(){

        String msg = String.format(Locale.ENGLISH , "%s %s %s %s",activity.getString(R.string.you_need_atleast_text),
                model.getCoinForUnmatchChat(),
                activity.getString(R.string.proactive_empty_wallet_sub_msg),
                utility.formatCoinBalance(model.getCoinBalance())+".");
        spendCoinDialog.showWalletEmptyDialog(activity.getString(R.string.proactive_wallet_empty_title),msg,this);
    }

    private void launchChat(){
        try {
            Intent intent;
            intent = new Intent(activity, ChatMessageActivity.class);
            intent.putExtra("receiverUid", chatListItem.getReceiverUid());
            intent.putExtra("chatId", chatListItem.getChatId());
            intent.putExtra("receiverName", chatListItem.getReceiverName());
            intent.putExtra("documentId", chatListItem.getDocumentId());
            intent.putExtra("receiverIdentifier", chatListItem.getReceiverUid());
            intent.putExtra("receiverImage", chatListItem.getReceiverImage());
            intent.putExtra("colorCode", AppController.getInstance().getColorCode(currentPosition % 19));
            intent.putExtra("fromSearchMessage", false);
            /*
             * To clear the unread messages count, when clicked on that chat
             */
            if (chatListItem.hasNewMessage()) {
                chatListItem.sethasNewMessage(false);
                notifyAdapter(currentPosition);
            }
            if(view != null) {
                view.launchChatScreen(intent);
                view.setNeedToUpdateChat(true);
                view.setFromChatFragment(true);
            }

        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
    private void notifyAdapter(int position) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                model.notifyAdapter(position);
            }
        });
    }

    @Override
    public void reportUser(int position)
    {
        try {
            //need user id in order to report this user
            chatListItem = model.getChatUser(position);
            if(model.isReportReasonsEmpty())
                getReportReasons();
            else{
                if (view != null)
                    launchReportUserDialog(model.getReasonsList());
            }
        } catch (NoMoreDataException e)
        {
            e.printStackTrace();
        }
    }

    private void launchReportUserDialog(ArrayList<String> reasonsList) {
        reportUserDialog.showDialog(reasonsList,this);
    }

    public void getReportReasons() {
        if(networkStateHolder.isConnected()) {
            if(view != null)
                loadingProgress.show();
            service.getReportReasons(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if (value.code() == 200) {
                                try {
                                    try {
                                        model.parseReportReasonData(value.body().string());
                                    } catch (Exception ignored) {
                                    }
                                    if (model.isReportReasonsEmpty()) {
                                        if (view != null)
                                            view.showError(activity.getString(R.string.failed_toget_report_reason_list_msg));
                                    } else {
                                        if (view != null)
                                            launchReportUserDialog(model.getReasonsList());
                                    }
                                } catch (Exception e) {
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else {
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                } catch (Exception ignored) {
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if (view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    @Override
    public void blockUser(int position)
    {
        try {
            chatListItem = model.getChatUser(position);
            callUserBlockApi();
        } catch (NoMoreDataException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void unBlockUser(int position) {
        try {
            chatListItem = model.getChatUser(position);
            callUserUnBlockApi();
        } catch (NoMoreDataException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void unMatchActiveUser(int position)
    {
        try {
            chatListItem = model.getChatUser(position);
            callUnMatchApi(chatListItem);
        } catch (NoMoreDataException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteActiveUserChat(int position) {
        try {
            deleteChatItem(position);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callDeleteChatApi(String chatId) {

        if(networkStateHolder.isConnected()) {
            loadingProgress.show();
            Map<String, Object> mapBody = new HashMap<>();
            mapBody.put("chatId", chatId);
            service.deleteChat(dataSource.getToken(), model.getLanguage(), mapBody)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                AppController.getInstance().getDbController().deleteParticularChatDetail(AppController.getInstance().getChatDocId(), chatListItem.getDocumentId(), false, "");

                                if (model.removeFromChatList(chatListItem)) {
                                    if (view != null)
                                        view.showMessage(chatListItem.getReceiverName() + activity.getString(R.string.chat_deleted_successful_msg));
                                    model.notifyAdapter();

                                    //check for empty
                                    if (model.isMatchListEmpty() && model.isChatListEmpty()) {
                                        if (view != null)
                                            view.onNoMatchFound();
                                    } else if (model.isMatchListEmpty() && !model.isChatListEmpty()) {
                                        if (view != null)
                                            view.showMatchList(false);
                                    } else if (!model.isMatchListEmpty()) {
                                        if (view != null)
                                            view.showMatchList(true);
                                    }
                                    if (view != null)
                                        view.showEmptyActiveChatLayout(model.isChatListEmpty());
                                } else {
                                    if (view != null)
                                        view.showError(String.format(Locale.getDefault(),"%s %s",chatListItem.getReceiverName(), activity.getString(R.string.chat_delete_failed_msg)));
                                }

                            } else if (value.code() == 401) {
                                AppController.getInstance().appLogout();
                            }

                            loadingProgress.cancel();
                        }

                        @Override
                        public void onError(Throwable e) {
                            if(view != null)
                                view.showError(activity.getString(R.string.server_error));
                            loadingProgress.cancel();
                        }
                    });
        } else {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    private void unMatchMatchedUser(int position)
    {
        try {
            ChatListData matchListItemPojo = model.getMatchItem(position);
            callUnMatchApi(matchListItemPojo);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private void notifyAdapter(){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                model.notifyAdapter();
            }
        });
    }

    public void handleGetMessage(JSONObject object) {

        try {
            if ( object.getString("eventName").equals(MqttEvents.UserUpdates.value + "/" + AppController.getInstance().getUserId())) {
                switch (object.getInt("type")) {
                    case 2:
                        try {
                            model.updateProfilePic(object);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;
                    case 4:
                        switch (object.getInt("subtype")) {
                            case 0:
                                /*
                                 * Contact name or number changed but number still valid
                                 */
                                model.updateContactName(object);
                                break;
                            case 1:
                                /*
                                 * Number of active contact changed and new number not in contact
                                 */
                                model.updateActiveContactNumber(object);
                                break;
                            case 2:
                                /*
                                 * New contact added
                                 */
                                model.updateNewContact(object);
                                break;
                        }
                        break;
                    case 5:
                        /*
                         * Number was in active contact
                         */
                        model.addActiveContactNumber(object);
                        break;
                    case 3:
                        model.updateInactiveNameAndNumber(object);
                        break;
                }
                notifyAdapter();
            } else if (object.getString("eventName").equals("ContactNameUpdated")) {

                model.updateContactName(object);
                notifyAdapter();
            } else if (object.getString("eventName").equals("contactRefreshed")) {
                //get chat list;
                //getMatchedUser();
                //showData();
            } else if (object.getString("eventName").equals(MqttEvents.FetchChats.value + "/" + AppController.getInstance().getUserId())) {
                if(view != null)
                    view.showData();
                model.addChatsFetchedFromServer(object.getJSONArray("chats"));
                if(view != null)
                    view.updateChatBadgeCount(model.getUnreadChatCount());
                notifyAdapter();
                AppController.getInstance().setChatSynced(true);
            }
//            else if (object.getString("eventName").equals("SyncChats")) {
//                /*
//                 * If logging in for the first time,then have to sync the chats
//                 */
//                if (AppController.getInstance().canPublish()) {
//                    AppController.getInstance().subscribeToTopic(MqttEvents.FetchChats.value + "/" + AppController.getInstance().getUserId(), 1);
//                    if(view != null)
//                        view.showLoadingView();
//                    fetchChatHistory();
//                } else {
//                    if (view != null) {
//                        view.showError(activity.getString(R.string.no_internet_error));
//                    }
//                }
//            }
            else if (object.getString("eventName").equals(MqttEvents.Message.value + "/" + AppController.getInstance().getUserId())) {

                model.parseMessageReceived(object);
                if(view != null)
                    view.notifyChatList(0);
                int pos = model.removeUserFromMatchList(object);
                if(pos != -1){
                    if(view != null)
                        view.notifyMatchList(pos);
                }
                showValidLayout();
                if(view != null)
                    view.updateChatBadgeCount(model.getUnreadChatCount());
            } else if (object.getString("eventName").equals(MqttEvents.Acknowledgement.value + "/" + AppController.getInstance().getUserId())) {
                /*
                 * Will update status only if not active on any of the chats
                 */
                model.parseStatusUpdate(object);
                //already notifed data change
            } else if (object.getString("eventName").equals(MqttEvents.MessageResponse.value)) {
                /*
                 * Will update status only if not active on any of the chats
                 */
                if (AppController.getInstance().getActiveReceiverId().isEmpty()) {
                    model.updateLastMessageDeliveryStatus(object.getString("docId"), object.getString("messageId"));
                }
                //already notified data change
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /*
     * To fetch the list of the chats in the background
     */
    private void fetchChatHistory() {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                ""/*ApiOnServer.FETCH_CHATS*/ + "/0", null, new com.android.volley.Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {
                    if (response.getInt("code") != 200) {
                        if (view != null)
                            view.showError(response.getString("errorBody"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                if(view != null)
//                    view.showData();
                try {
//                    if (view != null)
//                        view.showError(error.networkResponse.toString());
                }catch (Exception ignored){}
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj");
                headers.put("token", AppController.getInstance().getApiToken());
                return headers;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                20 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        /* Add the request to the RequestQueue.*/
        AppController.getInstance().addToRequestQueue(jsonObjReq, "getChatsApi");
    }

    @Override
    public void loadChatList() {

        if(view != null) {
            if (view.isNeedToUpdateList()) {

                if(view != null)
                    view.setNeedToUpdateChat(false);

                if(view != null)
                    view.showLoadingView();

                model.collectChatListDetails();

                if(view != null)
                    view.updateChatBadgeCount(model.getUnreadChatCount());

                if(view != null)
                    view.notifyChatList();
                if(view != null)
                    view.notifyMatchList();

                showValidLayout();
            }
        }
    }

    @Override
    public void onReportReasonSelect(String reason) {
        callReportApi(reason);
    }

    private void callReportApi(String reason) {

        if(chatListItem == null){
            if(view != null)
                Log.e(TAG, "callReportApi: "+"chatListItem can not be null!!");
            return;
        }

        if(networkStateHolder.isConnected()){
            if(view != null)
                loadingProgress.show();
            Map<String,Object> body = new HashMap<>();
            body.put(ApiConfig.ReportUser.TARGET_USER_ID,chatListItem.getReceiverUid());
            body.put(ApiConfig.ReportUser.REPORT_REASON,reason);
            body.put(ApiConfig.ReportUser.REPORT_MESSAGE,"reporting user");
            service.postReportUser(dataSource.getToken(),model.getLanguage(),body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if(value.code() == 200){
                                if(view != null)
                                    view.showMessage(String.format(Locale.getDefault(),chatListItem.getReceiverName() + activity.getString(R.string.user_reported_successfully_text)));
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception e){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if (view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    private void callUserBlockApi() {
        if(chatListItem == null){
            Log.e(TAG, "callUserBlockApi: "+"user chatListItem can not be null.");
            return;
        }
        if(networkStateHolder.isConnected()){
            if(view != null)
                loadingProgress.show();
            Map<String,Object> body = new HashMap<>();
            body.put(ApiConfig.BlockOrUnblockUser.TARGET_USER_ID,chatListItem.getReceiverUid());
            service.postBlockUser(dataSource.getToken(),model.getLanguage(),body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if(value.code() == 200){
                                model.setChatUserAsBlocked(chatListItem);
                                if(view != null)
                                    view.showMessage(String.format(Locale.getDefault(),chatListItem.getReceiverName() + activity.getString(R.string.user_block_successful_msg)));
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception ignored){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view != null)
                                view.showError(e.getMessage());
                        }

                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    private void callUserUnBlockApi() {
        if(chatListItem == null){
            Log.e(TAG, "callUserUnBlockApi: "+"user chatListItem can not be null.");
            return;
        }
        if(networkStateHolder.isConnected()){
            if(view != null)
                loadingProgress.show();
            Map<String,Object> body = new HashMap<>();
            body.put(ApiConfig.BlockOrUnblockUser.TARGET_USER_ID,chatListItem.getReceiverUid());
            service.postUnBlockUser(dataSource.getToken(),model.getLanguage(),body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if(value.code() == 200){
                                model.setChatUserAsUnblocked(chatListItem);
                                if(view != null)
                                    view.showMessage(String.format(Locale.getDefault(),"%s %s",chatListItem.getReceiverName() ,activity.getString(R.string.user_unblocked_successful_msg)));
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception ignored){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    private void callUnMatchApi(ChatListItem chatListItem){
        if(chatListItem == null){
            Log.e(TAG, "callUnMatchApi: "+"user chatListItem can not be null.");
            return;
        }
        if(networkStateHolder.isConnected()){
            if(view != null)
                loadingProgress.show();
            Map<String,Object> body = new HashMap<>();
            body.put(ApiConfig.UnMatchUser.TARGET_USER_ID,chatListItem.getReceiverUid());
            service.postUnMatchUser(dataSource.getToken(),model.getLanguage(),body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if(value.code() == 200){

                                AppController.getInstance().getDbController().deleteParticularChatDetail(AppController.getInstance().getChatDocId(),chatListItem.getDocumentId(),false,"");
                                if(view != null)
                                    view.showMessage(String.format(Locale.getDefault(), "%s %s",chatListItem.getReceiverName(), activity.getString(R.string.user_unmatched_successful_msg)));
                                //chat remove item from chat
                                if(model.removeFromChatList(chatListItem)){
                                    model.notifyAdapter();

                                    //check for empty
                                    if(model.isMatchListEmpty() && model.isChatListEmpty()){
                                        if(view != null)
                                            view.onNoMatchFound();
                                    }else if(model.isMatchListEmpty() && !model.isChatListEmpty()){
                                        if(view != null)
                                            view.showMatchList(false);
                                    }
                                    else if(!model.isMatchListEmpty()){
                                        if(view != null)
                                            view.showMatchList(true);
                                    }
                                    if(view != null)
                                        view.showEmptyActiveChatLayout(model.isChatListEmpty());
                                }
                                else{
//                                    if(view != null)
//                                        view.showError(chatListItem.getReceiverName() + " user unMatched failed!!");
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception ignored){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    //called form match list without chat.
    private void callUnMatchApi(ChatListData matchListItemPojo){
        if(matchListItemPojo == null){
            Log.e(TAG, "callUnMatchApi: " + "user matchListItemPojo can not be null!!");
            return;
        }
        if(networkStateHolder.isConnected()){
            if(view != null)
                loadingProgress.show();
            Map<String,Object> body = new HashMap<>();
            body.put(ApiConfig.UnMatchUser.TARGET_USER_ID,matchListItemPojo.getRecipientId());
            service.postUnMatchUser(dataSource.getToken(),model.getLanguage(),body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if(value.code() == 200){

                                //can not take chance just delete from local
                                String docId = AppController.getInstance().findDocumentIdOfReceiver(matchListItemPojo.getRecipientId(), "");
                                if(!TextUtils.isEmpty(docId))
                                    AppController.getInstance().getDbController().deleteParticularChatDetail(AppController.getInstance().getChatDocId(),docId,false,"");

                                if(view != null)
                                    view.showMessage(String.format(Locale.getDefault(),"%s %s",matchListItemPojo.getFirstName(), activity.getString(R.string.user_unmatched_successful_msg)));

                                //chat remove item from chat
                                if(model.removeFromMatchList(matchListItemPojo)){
                                    if(view != null)
                                        view.notifyMatchList();
                                    //check for empty
                                    if(model.isMatchListEmpty() && model.isChatListEmpty()){
                                        if(view != null)
                                            view.onNoMatchFound();
                                    }else if(model.isMatchListEmpty() && !model.isChatListEmpty()){
                                        if(view != null)
                                            view.showMatchList(false);
                                    }
                                    else if(!model.isMatchListEmpty()){
                                        if(view != null)
                                            view.showMatchList(true);
                                    }
                                    if(view != null)
                                        view.showEmptyActiveChatLayout(model.isChatListEmpty());
                                }
                                else if(value.code() == 401){
                                    AppController.getInstance().appLogout();
                                }
                                else{
//                                    if(view != null)
//                                        view.showError(matchListItemPojo.getFirstName() + " user unMatched failed!!");
                                }
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception ignored){}
                            }
                            loadingProgress.cancel();
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }
    //chat option callback
    @Override
    public void onChatClick(int position) {
        isFromMatchList = true;
        currentPosition = position;
        launchChat(position);
    }

    @Override
    public void onUnMatchClick(int position) {
        unMatchMatchedUser(position);
    }


    public void deleteChatItem(int position) throws NoMoreDataException {
        ChatListItem chatListItem = model.getChatUser(position);
        if (!chatListItem.isGroupChat()) {
            deleteChat(0, position, null);
        }
    }

    private void deleteChat(final int type, final int position, final String groupMembersDocId) throws NoMoreDataException {
        /*
         * Allowing only to delete a non-group chat or secret chat by swiping
         */
        ChatListItem mChatData = model.getChatUser(position);
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(activity, 0);
        builder.setTitle(activity.getResources().getString(R.string.DeleteConfirmation));
        builder.setMessage(activity.getResources().getString(R.string.DeleteChat));
        builder.setPositiveButton(activity.getResources().getString(R.string.Continue), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                try {
                    ChatListItem item = mChatData;
                    if (type == 0) {
                        /*
                         * To delete the normal or the secret chat from the server
                         */
                        //deleteChatFromServer(item);
                        chatListItem = model.getChatItem(position);
                        String chatId = chatListItem.getChatId();
                        if(!TextUtils.isEmpty(chatId))
                            callDeleteChatApi(chatId);
                        else{
                            if(view != null)
                                view.showError(activity.getString(R.string.chat_id_is_missing_text));
                        }
                    }
//                    else {
//                        /*
//                         * To delete the group chat from the server
//                         */
//                        deleteGroup(item, groupMembersDocId);
//                    }

//                    mChatData.remove(position);
//                    if (position == 0) {
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                mAdapter.notifyDataSetChanged();
//                            }
//                        });
//                    } else {
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                mAdapter.notifyItemRemoved(position);
//                            }
//                        });
//                    }
                } catch (IndexOutOfBoundsException e)

                {
                    e.printStackTrace();
                }

                dialog.dismiss();
            }
        });

        builder.setNegativeButton(activity.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // mAdapter.notifyItemChanged(position);

                        //mAdapter.notifyDataSetChanged();
                    }
                });
                dialog.cancel();

            }
        });
        androidx.appcompat.app.AlertDialog alertDialog = builder.create();


        alertDialog.setOnCancelListener(
                new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //mAdapter.notifyItemChanged(position);
                            }
                        });


                    }
                });
        alertDialog.show();

        Button b_pos;
        b_pos = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        if (b_pos != null) {
            b_pos.setTextColor(
                    ContextCompat.getColor(activity, R.color.datum)

            );
        }
        Button n_pos;
        n_pos = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        if (n_pos != null) {
            n_pos.setTextColor(ContextCompat.getColor(activity, R.color.datum));
        }


    }


    @Override
    public void parseActivityResult(int requestCode, int resultCode, Intent data) {
        if( requestCode == AppConfig.CHAT_SCREEN_REQ_CODE ){
            if(resultCode == -1){ //result ok
                if(isFromMatchList)
                    model.removeFromMatchList(currentPosition);
                if(view != null)
                    view.notifyMatchList();
                //For refresh the active chat list.
//                if(view != null)
//                    view.setNeedToUpdateChat(true);
//                loadChatList();
            }
        }
    }

    /*
     * coin dialog callback
     */
    @Override
    public void onOkToSpendCoin(boolean dontShowAgain) {
        launchChat();
        model.updateShowCoinDialogForChat(dontShowAgain);  //true means dont show
    }

    /*
     * coin dialog callback
     */
    @Override
    public void onByMoreCoin() {
        if(view != null)
            view.launchCoinWallet();
    }
}
