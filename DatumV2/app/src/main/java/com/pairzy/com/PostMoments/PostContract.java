package com.pairzy.com.PostMoments;

import android.widget.AutoCompleteTextView;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

/**
 * Created by ankit on 21/2/18.
 */

public interface PostContract {

    interface View extends BaseView {

        void updatedSuccessfully();
        void showMessage(String upload_failed_from_cloudinary);
        void showError(String error);
    }

    interface Presenter extends BasePresenter {
        void init(String path, String type);
        void sendPost();
        void validate(AutoCompleteTextView etPostTitle);
        void getCloudinaryDetails(String filePath);
    }
}
