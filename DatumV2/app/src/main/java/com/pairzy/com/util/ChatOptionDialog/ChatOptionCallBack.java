package com.pairzy.com.util.ChatOptionDialog;
/**
 * <h2>ChatMenuCallback</h2>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface ChatOptionCallBack
{
    void onChatClick(int position);
    void onUnMatchClick(int position);
}
