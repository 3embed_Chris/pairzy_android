package com.pairzy.com.userProfile.Model;

import com.pairzy.com.data.model.PrefData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * <h2>UserPreference</h2>
 * <P>
 *
 * </P>
 *@since  4/9/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class UserPreference
{

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("data")
    @Expose
    private ArrayList<PrefData> data = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<PrefData> getData() {
        return data;
    }

    public void setData(ArrayList<PrefData> data) {
        this.data = data;
    }
}
