package com.pairzy.com.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.pairzy.com.R;


public class VideoPlayer extends FrameLayout implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener,
        MediaPlayer.OnPreparedListener, MediaPlayer.OnVideoSizeChangedListener,MediaPlayer.OnInfoListener,TextureView.SurfaceTextureListener
{
    private MediaPlayer mMediaPlayer;
    private String path;
    private static final int LOCAL_VIDEO = 4;
    private static final int STREAM_VIDEO = 5;
    private boolean mIsVideoReadyToBePlayed = false;
    private boolean isSurfaceAvailable=false;
    private ImageView image_preview;
    private int media=LOCAL_VIDEO;
    private Listener listener;
    private Surface player_surface=null;
    private Handler handler;
    private RelativeLayout details_holder;
    private Bitmap prev_bitmap;
    private ImageView play_button;


    public VideoPlayer(@NonNull Context context)
    {
        super(context);
        intiView(context);
    }


    public VideoPlayer(@NonNull Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
        intiView(context);
    }


    public VideoPlayer(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        intiView(context);
    }


    @SuppressLint("InflateParams")
    private void intiView(Context context)
    {
        View view=LayoutInflater.from(context).inflate(R.layout.video_preview,null,false);
        addView(view);
        TextureView playbackView = findViewById(R.id.PlaybackView);
        playbackView.setOnClickListener(v -> pauseVideo());
        playbackView.setSurfaceTextureListener(this);
        play_button = findViewById(R.id.play_button);
        image_preview=findViewById(R.id.image_preview);
        details_holder=findViewById(R.id.details_holder);
        play_button.setOnClickListener(v -> onPlayButton());
    }


    public void setPath(String path)
    {
        releaseBitmap();
        this.path=path;
        if(TextUtils.isEmpty(path))
        {
            throw new IllegalArgumentException("Given video path is null!");
        }
        prev_bitmap = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.FULL_SCREEN_KIND);
        this.image_preview.setImageBitmap(prev_bitmap);
        details_holder.setEnabled(true);
        play_button.setVisibility(VISIBLE);
        details_holder.setAlpha(1f);
        releaseMediaPlayer();
        doCleanUp();
    }


    public void setPath(String video_path,String thumb_nail)
    {
        releaseBitmap();
        this.path=video_path;
        if(TextUtils.isEmpty(path))
        {
            throw new IllegalArgumentException("Given video path is null!");
        }
        if(TextUtils.isEmpty(thumb_nail))
        {
            throw new IllegalArgumentException("Given video thumbnail image path is null!");
        }
        prev_bitmap = BitmapFactory.decodeFile(thumb_nail);
        this.image_preview.setImageBitmap(prev_bitmap);
        releaseMediaPlayer();
        doCleanUp();
    }


    public void setListener(Listener listener)
    {
        this.listener=listener;
    }


    private void onPlayButton()
    {
        try
        {
            if(isSurfaceAvailable)
            {
                releaseMediaPlayer();
                doCleanUp();
                playVideo();
            }else
            {
                handler = new Handler();
                handler.postDelayed(() -> {
                    releaseMediaPlayer();
                    doCleanUp();
                    playVideo();
                }, 1000);
            }

        }catch (Exception e)
        {
            e.printStackTrace();
            if(listener!=null)
                listener.onError(e.getMessage());
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent)
    {}


    @Override
    public void onCompletion(MediaPlayer mp)
    {
        details_holder.setAlpha(1f);
        details_holder.setEnabled(true);
        play_button.setVisibility(VISIBLE);
    }

    @Override
    public void onPrepared(MediaPlayer mp)
    {
        mIsVideoReadyToBePlayed = true;
        if (mIsVideoReadyToBePlayed)
        {
            startVideoPlayback();
        }
    }

    private void startVideoPlayback()
    {
        mMediaPlayer.start();
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mp, int width, int height)
    {
        if (mIsVideoReadyToBePlayed)
        {
            startVideoPlayback();
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height)
    {
        player_surface= new Surface(surface);
        isSurfaceAvailable=true;
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height){}
    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface)
    {
        player_surface=null;
        isSurfaceAvailable=false;
        return false;
    }
    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {}


    private void playVideo()
    {
        try
        {
            switch (media)
            {
                case LOCAL_VIDEO:
                    //path = "";
                    break;
                case STREAM_VIDEO:
                    break;
            }
            // Create a new media player and set the listeners
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(path);
            mMediaPlayer.setSurface(player_surface);
            mMediaPlayer.prepare();
            mMediaPlayer.setOnBufferingUpdateListener(this);
            mMediaPlayer.setOnCompletionListener(this);
            mMediaPlayer.setOnPreparedListener(this);
            mMediaPlayer.setOnVideoSizeChangedListener(this);
            mMediaPlayer.setOnInfoListener(this);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        } catch (Exception e)
        {
            if(listener!=null)
                listener.onError(e.getMessage());
        }
    }

    private void releaseMediaPlayer()
    {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    private void doCleanUp()
    {
        mIsVideoReadyToBePlayed = false;
    }


    /**
     * Releasing the bitmap object.
     */
    private void releaseBitmap()
    {
       if(prev_bitmap!=null)
           prev_bitmap.recycle();
        prev_bitmap=null;
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra)
    {
        if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START)
        {
            details_holder.setAlpha(0f);
            details_holder.setEnabled(false);
            play_button.setVisibility(GONE);
            return true;
        }else if(what == MediaPlayer.MEDIA_INFO_BUFFERING_START)
        {
            details_holder.setAlpha(1f);
            details_holder.setEnabled(true);
            play_button.setVisibility(VISIBLE);
            return true;
        }else if(what == MediaPlayer.MEDIA_INFO_BUFFERING_END)
        {
            details_holder.setAlpha(0f);
            details_holder.setEnabled(false);
            play_button.setVisibility(GONE);
            return true;
        }
        return false;
    }

    public interface Listener
    {
        void  onError(String error);
    }

    public void pauseVideo()
    {
        if(mMediaPlayer!=null&&mMediaPlayer.isPlaying())
        {
            mMediaPlayer.pause();
            details_holder.setAlpha(1f);
            details_holder.setEnabled(true);
            play_button.setVisibility(VISIBLE);
        }
    }

    public void pausePlay()
    {
        if(mMediaPlayer!=null&&!mMediaPlayer.isPlaying())
        {
            mMediaPlayer.start();
            details_holder.setAlpha(0f);
            details_holder.setEnabled(false);
            play_button.setVisibility(GONE);
        }

    }


    public void releasePlayer()
    {
        releaseMediaPlayer();
        doCleanUp();
        releaseBitmap();
    }
}
