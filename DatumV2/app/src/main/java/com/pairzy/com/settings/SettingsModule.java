package com.pairzy.com.settings;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h>PassportModule class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

@Module
public abstract class SettingsModule {

    @ActivityScoped
    @Binds
    abstract SettingsContract.Presenter settingsPresenter(SettingsPresenter presenter);

    @ActivityScoped
    @Binds
    abstract SettingsContract.View settingsView(SettingsActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity settingsActivity(SettingsActivity activity);
}
