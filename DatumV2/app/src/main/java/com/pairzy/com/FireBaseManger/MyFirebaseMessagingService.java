package com.pairzy.com.FireBaseManger;

import android.os.Bundle;
import android.util.Log;

import com.pairzy.com.AppController;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.CustomObserver.BoostEndObserver;
import com.pairzy.com.util.CustomObserver.DateRefreshObserver;
import com.pairzy.com.util.notificationHelper.NotificationHelper;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

/**
 * Created by ankit on 20/6/18.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Inject
    DateRefreshObserver dateRefreshObserver;
    @Inject
    BoostEndObserver boostEndObserver;
    @Inject
    NotificationHelper notificationHelper;
    @Inject
    PreferenceTaskDataSource dataSource;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        dataSource.setPushToken(s);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AppController.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG, "From: " + remoteMessage);

        try {
            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0) {

                Log.w(TAG, "Message data payload: " + remoteMessage.getData());

                Map<String, String> data = remoteMessage.getData();
                if (data.containsKey("type")) {
                    if(Integer.parseInt(Objects.requireNonNull(data.get("type"))) == 32){
                       //post like, comment and create.
                        Bundle bundle = new Bundle();
                        bundle.putInt("type", 32);
                        bundle.putString("title", data.get("title"));
                        bundle.putString("message", data.get("message"));
                        notificationHelper.showNotification(data.get("title"),data.get("message"), bundle);
                    }else if (Integer.parseInt(Objects.requireNonNull(data.get("type"))) == 30) {
                        //campaign
                        Bundle bundle = new Bundle();
                        bundle.putInt("type", 30);
                        bundle.putString("image", data.get("image"));
                        bundle.putString("campaignUrl", data.get("url"));
                        bundle.putString("title", data.get("title"));
                        bundle.putString("message", data.get("message"));
                        notificationHelper.showNotification(data.get("title"),data.get("message"), bundle);
                    } else if (Integer.parseInt(Objects.requireNonNull(data.get("type"))) == 40) { //push
                        Bundle bundle = new Bundle();
                        bundle.putInt("type", 40);
                        notificationHelper.showNotification(data.get("title"),data.get("message"), bundle);
                    } else if (Integer.parseInt(Objects.requireNonNull(data.get("type"))) == 1) { // match
                        Bundle bundle = new Bundle();
                        bundle.putInt("type", 1);
                        bundle.putString("target_id", data.get("target_id"));
                        /*
                         * already receiving match data on Mqtt with causing multiple notification.
                         */
                        notificationHelper.showNotification(data.get("title"),data.get("message"),bundle);
                    } else if (Integer.parseInt(Objects.requireNonNull(data.get("type"))) == 2) { //likes
                        Bundle bundle = new Bundle();
                        bundle.putInt("type", 2);
                        bundle.putString("target_id", data.get("target_id"));
                        notificationHelper.showNotification(data.get("title"),data.get("message"),bundle);
                    } else if (Integer.parseInt(Objects.requireNonNull(data.get("type"))) == 7 ||
                            Integer.parseInt(Objects.requireNonNull(data.get("type"))) == 8 ||
                            Integer.parseInt(Objects.requireNonNull(data.get("type"))) == 9 ||
                            Integer.parseInt(Objects.requireNonNull(data.get("type"))) == 10) {
                        //date created reschedule ,date confirmed and date cancelled
                        dateRefreshObserver.publishData(true);
                        Bundle bundle = new Bundle();
                        bundle.putInt("type", 7);
                        bundle.putString("target_id", data.get("target_id"));
                        notificationHelper.showNotification(data.get("title"),data.get("message"),bundle);

                    } else if(Integer.parseInt(Objects.requireNonNull(data.get("type"))) == 31){ //boost
                        boostEndObserver.publishData(true);
                        notificationHelper.showNotification(data.get("title"),data.get("message"),null);
                        dataSource.setDuringBoostViewCount(0);
                    }
                    /*else if(Integer.parseInt(data.get("type")) == 41){ //chat notification
                    }*/
                    else{
                        notificationHelper.showNotification(data.get("title"),data.get("message"),null);
                    }
                }
            }
            // Check if message contains a notification payload.
            else if (remoteMessage.getNotification() != null) {

                notificationHelper.showNotification("NotificationBody"/*getString(R.string.app_name)*/, remoteMessage.getNotification().getBody(),null);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
