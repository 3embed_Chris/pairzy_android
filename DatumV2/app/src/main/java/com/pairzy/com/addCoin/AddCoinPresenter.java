package com.pairzy.com.addCoin;

import android.app.Activity;
import android.text.TextUtils;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.addCoin.AddCoinDialog.AddCoinDialog;
import com.pairzy.com.addCoin.AddCoinDialog.AddCoinDialogCallBack;
import com.pairzy.com.addCoin.model.AddCoinModel;
import com.pairzy.com.boostDetail.model.CoinPlan;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.CustomObserver.AdminCoinObserver;
import com.pairzy.com.util.CustomObserver.CoinBalanceObserver;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.suresh.innapp_purches.InAppCallBack;
import com.suresh.innapp_purches.InnAppSdk;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h>PassportPresenter class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public class AddCoinPresenter implements AddCoinContract.Presenter,ItemActionCallBack, AddCoinDialogCallBack{

    @Inject
    AddCoinContract.View view;
    @Inject
    NetworkService service;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    AddCoinModel model;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    Activity activity;
    @Inject
    LoadingProgress loadingProgress;
    @Inject
    CoinBalanceObserver coinBalanceObserver;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    AddCoinDialog addCoinDialog;
    @Inject
    AdminCoinObserver adminCoinObserver;
    @Inject
    CoinDialog coinDialog;

    private CompositeDisposable compositeDisposable;
    private CoinPlan currentCoinPlan;


    @Inject
    public AddCoinPresenter(){
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void init() {
        if(view != null)
            view.applyFont();
    }

    @Override
    public void observeAdminCoinAdd() {
        adminCoinObserver.getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Boolean value) {
                        if(value){
                            getCoinBalance();
                           // getCoinPlans();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void callAddCoinApi(int coinValue) {
        if(networkStateHolder.isConnected()) {
            loadingProgress.show();
            service.addCoinToWallet(dataSource.getToken(), model.getLanguage(), model.getAddCoinToWalletBody(coinValue))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if (value.code() == 200) {
                                coinDialog.showDialog(String.valueOf(coinValue));
                                if(view != null)
                                    view.showMessage(String.format(Locale.ENGLISH,"Free %d Coin added to wallet successfully!!",coinValue));
                                model.updateCoinWalletBalanceBy(coinValue);
                                adminCoinObserver.publishData(true);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view != null)
                                view.showError(activity.getString(R.string.api_server_error));
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    @Override
    public void getCoinBalance()
    {
        if(networkStateHolder.isConnected())
        {
            if(view != null)
                view.showBalanceLoading(true);
            service.getCoinBalance(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value)
                        {
                            if(view != null)
                                view.showBalanceLoading(false);
                            if(value.code() == 200){
                                try {
                                    model.parseCoinBalance(value.body().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception e){}
                            }
                            coinBalanceObserver.publishData(true);
                        }

                        @Override
                        public void onError(Throwable e) {
                            if(view != null)
                                view.showError(e.getMessage());
                            if(view != null)
                                view.showBalanceLoading(false);
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
            if(view != null)
                view.showNetworkError(activity.getString(R.string.no_internet_error));
            if(view != null)
                view.showBalanceLoading(false);
        }
    }

    @Override
    public void getCoinPlans()
    {
        if(networkStateHolder.isConnected()) {

            if (view != null)
                view.showLoading();
            service.getCoinPlans(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value)
                        {
                            if(value.code() == 200)
                            {
                                try
                                {
                                    model.parseCoinPlanList(value.body().string());
                                    if (model.isSubsPlanEmpty())
                                    {
                                        if (view != null)
                                            view.showEmptyData();
                                    }else
                                    {
                                        List<String> actual_ids=model.collectsIds();
                                        InnAppSdk.getVendor().getProductDetails(activity,actual_ids, InnAppSdk.Type.INNAPP, (list, errorList) -> {
                                            if(list.size()>0)
                                            {
                                                model.updateDetailsData(list);
                                                if (view != null)
                                                    view.showData();
                                            }else
                                            {
                                                model.clearProductList();
                                                if (view != null)
                                                    view.showEmptyData();
                                            }
                                        });
                                        model.notifyPlanListAdapter();
                                    }
                                }catch (Exception e)
                                {
                                    e.printStackTrace();
                                    if (view != null)
                                        view.showEmptyData();
                                    if(view != null)
                                        view.showError(activity.getString(R.string.subs_list_empty));
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else
                            {
                                if (view != null)
                                    view.showEmptyData();
                                try {
                                    String error = value.errorBody().string();
                                    if (view != null)
                                        view.showError(error);
                                }catch (Exception ignored){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null)
                                view.showError(activity.getString(R.string.failed_to_get_coin_plan));
                            if (view != null)
                                view.showNetworkError(activity.getString(R.string.failed_to_get_coin_plan));
                        }
                    });

        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
            if(view != null)
                view.showNetworkError(activity.getString(R.string.no_internet_error));
        }

    }

    @Override
    public void onClick(int id, int position) {
        switch (id){
            case R.id.coin_plan_rl:
                currentCoinPlan = model.getCoinPlan(position);
                if(currentCoinPlan != null)
                    launchAddCoinAlert(currentCoinPlan.getPlanName());
                else{
                    if(view != null)
                        view.showError("coinPlan object is null");
                }
                break;
        }
    }

    private void launchAddCoinAlert(String coinPlanName) {
        addCoinDialog.showDialog(coinPlanName,this);
    }

    private void callCoinPurchaseApi(CoinPlan coinPlan)
    {
        if(coinPlan == null){
            if(view != null)
                view.showError(activity.getString(R.string.failed_to_get_plan));
            return;
        }
        currentCoinPlan = coinPlan;
        if(view != null)
            loadingProgress.show();

        //Log.d("345rt", ":"+coinPlan.get_id());
        Map<String, Object> body = new HashMap<>();
        body.put(ApiConfig.CoinPlan.PLAN_ID,coinPlan.get_id());
        body.put(ApiConfig.CoinPlan.PAYMENT_GETWAY_TAX_ID,String.valueOf(System.currentTimeMillis()));
        body.put(ApiConfig.CoinPlan.TRIGGER,"");
        body.put(ApiConfig.CoinPlan.PAYMENT_TYPE,"Card");
        body.put(ApiConfig.CoinPlan.PAYMENT_TAXN_ID,"0");
        body.put(ApiConfig.CoinPlan.USER_PURCHASE_TIME,String.valueOf(System.currentTimeMillis()));
        service.postCoinPlans(dataSource.getToken(),model.getLanguage(),body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onSuccess(Response<ResponseBody> value)
                    {
                        if(view != null)
                            loadingProgress.cancel();

                        if(value.code() == 200)
                        {
                            if(view != null)
                                view.showMessage(activity.getString(R.string.coin_plan_successful));
                            try {
                                model.addToCoinBalance(currentCoinPlan.getNoOfCoinUnlock().getCoin());
                                if(view != null)
                                    view.showCoinBalance(model.getCoinBalance() +" Coins");
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            coinBalanceObserver.publishData(true);
                        }
                        else if(value.code() == 401){
                            AppController.getInstance().appLogout();
                        }
                        else{
                            try{
                                if(view != null)
                                    view.showError(value.errorBody().string());
                            }catch (Exception e){}
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null)
                            loadingProgress.cancel();
                        if(view != null)
                            view.showError(activity.getString(R.string.failed_to_purchase_coin_plan));
                    }
                });
    }

    @Override
    public void observeCoinBalanceChange() {
        coinBalanceObserver.getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Boolean value) {
                        if(view != null)
                            view.showCoinBalance(model.getCoinBalance()+" Coins");
                    }

                    @Override
                    public void onError(Throwable e) {}

                    @Override
                    public void onComplete() {}
                });
    }

    @Override
    public void updateCoinBalance() {
        if(view != null)
            view.showBalanceLoading(false);
        if(!TextUtils.isEmpty(coinBalanceHolder.getCoinBalance())){
            if(view != null)
                view.showCoinBalance(model.getCoinBalance()+" Coins");
        } else{
            getCoinBalance();
        }
    }

    @Override
    public void loadCoinPlanIfRequired() {
        if(model.isCoinPlanEmpty())
            getCoinPlans();
        else{
            if(view != null)
                view.showData();
            model.notifyPlanListAdapter();
        }
    }


    @Override
    public void onOk()
    {
        if(currentCoinPlan != null)
            purchaseCoin(currentCoinPlan);
        else
        {
            if(view != null)
                view.showError("coinPlan can not be null");
        }
    }


    private void purchaseCoin(final CoinPlan coinPlan)
    {
        String purchaseId=coinPlan.getActualIdForAndroid();
        if(!TextUtils.isEmpty(purchaseId))
        {
            InnAppSdk.getVendor().purchasedItem(activity, purchaseId, InnAppSdk.Type.INNAPP, dataSource.getUserId(), new InAppCallBack.OnPurchased()
            {
                @Override
                public void onSuccess(String productId)
                {
                    String id=coinPlan.get_id();
                    if(TextUtils.isEmpty(id))
                    {
                        if(view!=null)
                            view.showError("Error on updating !");
                    }else
                    {
                        callCoinPurchaseApi(coinPlan);
                    }
                }
                @Override
                public void onError(String id, String error)
                {
                    if(view != null)
                        view.showError(error);
                }

            });
        }else
        {
            if(view != null)
                view.showError("Error on purchasing "+purchaseId);
        }
    }

    @Override
    public void dispose() {
        compositeDisposable.clear();
    }
}
