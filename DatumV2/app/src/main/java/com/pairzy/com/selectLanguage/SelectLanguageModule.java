package com.pairzy.com.selectLanguage;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h>PassportModule class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

@Module
public abstract class SelectLanguageModule {

    @ActivityScoped
    @Binds
    abstract SelectLanguageContract.Presenter selectedLaguagePresenter(SelectLanguagePresenter presenter);

    @ActivityScoped
    @Binds
    abstract SelectLanguageContract.View selectLanguageView(SelectLanguageActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity activity(SelectLanguageActivity activity);
}
