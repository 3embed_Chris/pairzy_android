package com.pairzy.com.home.Dates.Pending_page;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.addCoin.AddCoinActivity;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.home.Dates.DatesFragContract;
import com.pairzy.com.home.Dates.DatesFragPresenter;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;
import com.pairzy.com.home.Dates.Pending_page.Model.PendingListAdapter;
import com.pairzy.com.planCallDate.CallDateActivity;
import com.pairzy.com.planDate.DateActivity;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.TypeFaceManager;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * <h2>PastDateFrg</h2>
 * A simple {@link Fragment} subclass.
 * @author 3Embed.
 * @version 1.0.
 * @since 17-03-2017.
 */
@ActivityScoped
public class PendingFrg extends DaggerFragment implements PendingFrgContract.View,SwipeRefreshLayout.OnRefreshListener
{
    public static final int RESCHEDULE_REQ_CODE = 110;
    private Unbinder unbinder;
    @Inject
    PendingFrgContract.Presenter presenter;
    @Inject
    DatesFragContract.Presenter datePresenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Named(PendingPageUtil.PENDING_LAYOUT_MANAGER)
    @Inject
    LinearLayoutManager linearLayoutManager;
    @Inject
    PendingListAdapter pendingListAdapter;
    @Inject
    Activity activity;
    @Inject
    App_permission app_permission;

    @BindView(R.id.pending_list)
    RecyclerView pendingRecycler;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.loading_progress)
    ProgressBar pbLoading;
    @BindView(R.id.loading_text)
    TextView tvLoading;
    @BindView(R.id.error_icon)
    ImageView ivErrorIcon;
    @BindView(R.id.error_msg)
    TextView tvErrorMsg;
    @BindView(R.id.empty_data)
    RelativeLayout rlEmptyData;
    @BindView(R.id.loading_view)
    RelativeLayout rlLoadingView;
    @BindView(R.id.no_more_data)
    TextView tvNoMoreDataTitle;
    @BindView(R.id.empty_details)
    TextView tvEmptyDetail;
    @BindView(R.id.btn_retry)
    Button btnRetry;

    public PendingFrg() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_pending_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder=ButterKnife.bind(this,view);
        presenter.takeView(this);
        initView();
        applyFont();
        presenter.notifyPendingAdapter();
        presenter.observeDateListChange();
    }

    private void applyFont() {
        tvNoMoreDataTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvEmptyDetail.setTypeface(typeFaceManager.getCircularAirBook());
        btnRetry.setTypeface(typeFaceManager.getCircularAirBold());
    }

    private void initView() {
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh2, R.color.refresh1,R.color.refresh);
        pendingRecycler.setLayoutManager(linearLayoutManager);
        pendingRecycler.setHasFixedSize(true);
        //pendingRecycler.setNestedScrollingEnabled(false);
        pendingRecycler.setAdapter(pendingListAdapter);
        presenter.setAdapterCallBack(pendingListAdapter);
        pendingRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int positionView = linearLayoutManager.findLastVisibleItemPosition();
                presenter.doLoadMore(positionView);
                datePresenter.preFetchImage(true,positionView);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(DatesFragPresenter.pendingDateListUpdated) {
            pendingListAdapter.notifyDataSetChanged();
            DatesFragPresenter.pendingDateListUpdated = false;
        }
    }


    @Override
    public void showLoading() {
        if(tvErrorMsg != null) {
            btnRetry.setVisibility(View.GONE);
            tvErrorMsg.setVisibility(View.GONE);
            ivErrorIcon.setVisibility(View.GONE);
            tvLoading.setVisibility(View.VISIBLE);
            pbLoading.setVisibility(View.VISIBLE);
            rlLoadingView.setVisibility(View.VISIBLE);
            //pendingRecycler.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
        }
    }

    @Override
    public void notifyAdapter() {
        if(rlEmptyData != null) {
            if (presenter != null && !presenter.checkDateExist()) {
                emptyData();
            } else {
                pendingListAdapter.notifyDataSetChanged();
                rlLoadingView.setVisibility(View.GONE);
                rlEmptyData.setVisibility(View.GONE);
                pendingRecycler.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void doLoadMore() {
        try
        {
            pendingRecycler.post(() -> pendingListAdapter.notifyDataSetChanged());
        }catch (Exception e){}
        datePresenter.fetchList(true,false);
    }

    @Override
    public void showMessage(String message) {
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void refreshList() {
        datePresenter.fetchList(true,true);
    }

    @Override
    public void showError(int id)
    {
        String message=getString(id);
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showNetworkError(String errorMsg) {
        if(tvErrorMsg != null) {
            btnRetry.setVisibility(View.VISIBLE);
            tvErrorMsg.setVisibility(View.VISIBLE);
            tvErrorMsg.setText(errorMsg);
            ivErrorIcon.setVisibility(View.VISIBLE);
            tvLoading.setVisibility(View.GONE);
            pbLoading.setVisibility(View.GONE);
            pendingRecycler.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void emptyData()
    {
        if(pendingRecycler != null) {
            //pendingRecycler.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.VISIBLE);
        }
    }


    /*
     * helper method
     */
    private void launchScreen(Intent intent,DateListPojo dateListPojo){
        try {
            intent.putExtra("reschedule", PendingFrgPresenter.RESCHEDULE_TAG);
            intent.putExtra("date_data", dateListPojo);
            intent.putExtra("date_type", dateListPojo.getDateType());
            startActivityForResult(intent, RESCHEDULE_REQ_CODE);
            activity.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }catch (Exception e){
        }
    }

    @Override
    public void addToTheUpcomingList(DateListPojo removedDate) {
        datePresenter.addToTheUpcomingList(removedDate);
    }

    @Override
    public void launchCallDateScreen(DateListPojo dateListPojo) {
        Intent intent = new Intent(getContext(), CallDateActivity.class);
        launchScreen(intent,dateListPojo);
    }

    @Override
    public void launchPhyDateScreen(DateListPojo dateListPojo) {
        Intent intent = new Intent(getContext(), DateActivity.class);
        launchScreen(intent,dateListPojo);
    }

    @Override
    public void launchUserProfile(DateListPojo dateListPojo) {
        datePresenter.launchUserProfile(dateListPojo);
    }

    @Override
    public void addTothePastDate(PastDateListPojo pastDateListPojo) {
        datePresenter.addToThePastDate(pastDateListPojo);
    }

    @Override
    public void launchCoinWallet() {
        Intent intent = new Intent(activity,AddCoinActivity.class);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @Override
    public void updatePendingDateCount(int pendingDateCount) {
        datePresenter.updatePendingDateCount(pendingDateCount);
    }

    @Override
    public void showError(String error)
    {
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.parseDateData(requestCode,resultCode,data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if(!app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults))
        {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        unbinder.unbind();
        presenter.dropView();
    }

    @OnClick(R.id.btn_retry)
    public void onRetry(){
        onRefresh();
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        refreshList();
    }
}
