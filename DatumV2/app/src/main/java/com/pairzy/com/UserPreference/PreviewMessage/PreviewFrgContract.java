package com.pairzy.com.UserPreference.PreviewMessage;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
/**
 * <h2>{@link PreviewFrgContract}</h2>
 * <P>
 *     Preview frg contract fro the user.
 * </P>
 * @since 27/26/2018.
 * @author 3Embed.
 * @version 1.0. */

public interface PreviewFrgContract
{
    interface Presenter extends BasePresenter
    {

    }

    interface View extends BaseView
    {

    }
}
