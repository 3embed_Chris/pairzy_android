package com.pairzy.com.MyProfile.ImageItem;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pairzy.com.R;
import com.facebook.drawee.view.SimpleDraweeView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileImageItemFrg extends DaggerFragment implements ImageItemContract.View
{
    private String IMAGE_PATH;
    private Unbinder unbinder;

    @BindView(R.id.image)
    SimpleDraweeView image;

    @Inject
    public ProfileImageItemFrg() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_image_item_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder=ButterKnife.bind(this,view);
        initData();
    }


    private void initData()
    {
        image.setImageURI(IMAGE_PATH);

    }

    /*
     *Setting the video url. */
    public void setMediaFile(String image_path)
    {
        this.IMAGE_PATH=image_path;
    }

    @Override
    public void onDestroy()
    {
        unbinder.unbind();
        super.onDestroy();
    }
}
