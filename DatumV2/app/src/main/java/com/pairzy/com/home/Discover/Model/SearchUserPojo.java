package com.pairzy.com.home.Discover.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
/**
 * @since  3/13/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class SearchUserPojo
{
    @SerializedName("data")
    @Expose
    private ArrayList<UserItemPojo> data = null;

    @SerializedName("remainsLikesInString")
    @Expose
    private String remainsLikesCount = "0";

    @SerializedName("remainsRewindsInString")
    @Expose
    private String remainsRewindCount = "0";

    @SerializedName("nextLikeTime")
    @Expose
    private long nextLikeTime = 0;

    @SerializedName("boost")
    @Expose
    private BoostResponse boostResponse;

    public ArrayList<UserItemPojo> getData() {
        return data;
    }
    public void setData(ArrayList<UserItemPojo> data) {
        this.data = data;
    }

    public String getRemainsLikesCount() {
        return remainsLikesCount;
    }

    public void setRemainsLikesCount(String remainsLikesCount) {
        this.remainsLikesCount = remainsLikesCount;
    }

    public String getRemainsRewindCount() {
        return remainsRewindCount;
    }

    public void setRemainsRewindCount(String remainsRewindCount) {
        this.remainsRewindCount = remainsRewindCount;
    }

    public long getNextLikeTime() {
        return nextLikeTime;
    }

    public void setNextLikeTime(long nextLikeTime) {
        this.nextLikeTime = nextLikeTime;
    }

    public void setBoostResponse(BoostResponse boostResponse) {
        this.boostResponse = boostResponse;
    }

    public BoostResponse getBoostResponse() {
        return boostResponse;
    }
}
