package com.pairzy.com.UserPreference.EditInputFrg;
import com.pairzy.com.BaseModel;
import com.pairzy.com.util.ApiConfig;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
/**
 * @since 2/26/2018.
 * @author 3Embed.
 * @version 1.0.
 */
class UserInputModel extends BaseModel
{
    @Inject
    UserInputModel(){}

    String formatValues(String data)
    {
        ArrayList<String> temp_data=new ArrayList<>();
        temp_data.add(data);
        JSONArray arr_strJson = new JSONArray(temp_data);
        return arr_strJson.toString();
    }
    /*
        *provide the params. */
    Map<String, Object> getParams(String pref_id,String values)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.updatePreferenceKey.PREF_ID,pref_id);
        map.put(ApiConfig.updatePreferenceKey.PREF_VALUES,values);
        return map;
    }
}
