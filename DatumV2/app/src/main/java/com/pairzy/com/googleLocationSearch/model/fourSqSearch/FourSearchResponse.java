package com.pairzy.com.googleLocationSearch.model.fourSqSearch;

import com.pairzy.com.data.model.fourSq.Meta;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h>FourSquare pojo class</h>
 * @author 3Embed.
 * @since 9/5/18.
 * @version 1.0.
 */

public class FourSearchResponse {

    @SerializedName("meta")
    @Expose
    private Meta meta;
    @SerializedName("response")
    @Expose
    private Response response;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}
