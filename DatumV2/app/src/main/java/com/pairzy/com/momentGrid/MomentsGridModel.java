package com.pairzy.com.momentGrid;

import com.pairzy.com.BaseModel;
import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.util.ApiConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

/**
 * <h2>MomentsGridModel</h2>
 * <P> this is a model class</P>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */
public class MomentsGridModel extends BaseModel {

    String TAG= MomentsGridModel.class.getSimpleName();

    @Inject
    MomentsGridModel(){}

    @Inject
    ArrayList<MomentsData> postList;


    public Map<String, Object> getLikeUnlikeData(double type, String postId) {
        Map<String, Object> map=new HashMap<>();
        map.put(ApiConfig.LIKE_UNLIKE.POST_ID,postId);
        map.put(ApiConfig.LIKE_UNLIKE.TYPE,type);
        return map;
    }

    public MomentsData getPostItem(int position) {
        try {
            return postList.get(position);
        }catch (Exception e){
        }
        return null;

    }

    public void updateMomentData(ArrayList<MomentsData> momentsData) {
        postList.clear();
        postList.addAll(momentsData);
    }
}



