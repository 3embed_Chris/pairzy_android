package com.pairzy.com.settings;

import androidx.annotation.NonNull;

/**
 * <h>PassportContract interface</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public interface SettingsContract {

    interface View{
        void applyFont();
        void showSplashScreen();
        void showAppVersion(String appVersion);
        void launchWebActivity(String title, @NonNull String url);
        void finishActivity();
    }

    interface Presenter{
        void loadLogoutDialog();
        void loadAppVersion();
        void init();
        void launchWebActivity(String title, @NonNull String url);
    }
}