package com.pairzy.com.MqttChat.ModelClasses;

/**
 * Created by moda on 28/07/17.
 */

public class ContactsItem {//implements GroupListWrapper.Selector {

    private String contactUid, contactName, contactImage, contactIdentifier, contactStatus;

    public String getContactUid() {
        return contactUid;
    }

    public void setContactUid(String contactUid) {
        this.contactUid = contactUid;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactImage() {
        return contactImage;
    }

    public void setContactImage(String contactImage) {
        this.contactImage = contactImage;
    }

    public String getContactIdentifier() {
        return contactIdentifier;
    }

    public void setContactIdentifier(String contactIdentifier) {
        this.contactIdentifier = contactIdentifier;
    }

    public String getContactStatus() {
        return contactStatus;
    }


    public void setContactStatus(String contactStatus) {
        this.contactStatus = contactStatus;
    }


//    private int itemType;
//
//    public int getItemType() {
//        return itemType;
//    }
//
//
//    public void setItemType(int itemType) {
//        this.itemType = itemType;
//    }
//    @Override
//    public String select() {
//        return contactName;
//    }
}
