package com.pairzy.com.home.Dates.upcomingPage;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import androidx.core.app.ActivityCompat;

import com.pairzy.com.AppController;
import com.pairzy.com.MqttChat.Calls.CallingApis;
import com.pairzy.com.MqttChat.Calls.Common;
import com.pairzy.com.MqttChat.Utilities.Utilities;
import com.pairzy.com.R;
import com.pairzy.com.data.model.DateEvent;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.CustomAlertDialog.CustomAlertDialog;
import com.pairzy.com.home.Dates.CustomAlertDialog.CustomAlertDialogCallBack;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Dates.Model.DateResponseType;
import com.pairzy.com.home.Dates.Model.DateType;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;
import com.pairzy.com.home.Dates.Pending_page.PendingFrg;
import com.pairzy.com.home.Dates.upcomingPage.Model.UpcomingAdapter;
import com.pairzy.com.home.Dates.upcomingPage.Model.UpcomingModel;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.CalendarEventHelper;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h2>PostFeedFragPresenter</h2>
 * <P>
 *
 * </P>
 * @since  3/17/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class UpcomingFrgPresenter implements UpcomingFrgContract.Presenter, ItemActionCallBack,App_permission.Permission_Callback,CustomAlertDialogCallBack
{

    private final String CALENDER_TAG = "calender_tag";

    @Inject
    UpcomingModel model;

    @Inject
    NetworkService service;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    Utility utility;
    @Inject
    Activity activity;
    @Inject
    App_permission app_permission;
    @Inject
    CalendarEventHelper calendarEventHelper;
    @Inject
    CustomAlertDialog customAlertDialog;

    private CompositeDisposable compositeDisposable;
    private UpcomingFrgContract.View view;
    private int currentPosition = -1;
    private UpcomingFrg upcomingFrg;
    private DateResponseType dateResponseType;
    private DateListPojo dateListPojo;
    private DateResponseType currentDateResponse;

    @Inject
    UpcomingFrgPresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(Object view)
    {
        this.view= (UpcomingFrgContract.View) view;
        this.upcomingFrg = (UpcomingFrg)view;
    }

    @Override
    public void dropView()
    {
        view=null;
        compositeDisposable.clear();
    }

    private void checkForCalenderPermission() {
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.READ_CALENDER);
        permissions.add(App_permission.Permission.WRITE_CALENDER);
        app_permission.getPermission_for_Sup_v4Fragment(CALENDER_TAG,permissions,upcomingFrg,this);
    }

    @Override
    public void notifyUpcomingAdapter() {
        if(model.getListSize() == 0){
            if(view != null)
                view.refreshList();
        }
        else{
            if(view != null)
                view.notifyAdapter();
        }
    }

    @Override
    public void doLoadMore(int currentPos) {
        if(model.loadMoreRequired(currentPos))
        {
            if(view!=null)
                view.doLoadMore();
        }
    }

    @Override
    public void setAdapterCallBack(UpcomingAdapter upcomingAdapter) {
        upcomingAdapter.setCallBack(this);
    }

    @Override
    public boolean checkDateExist() {
        return model.checkDateExist();
    }

    @Override
    public void parseDateData(int requestCode, int resultCode, Intent data) {
        if(requestCode == PendingFrg.RESCHEDULE_REQ_CODE){
            if(resultCode == -1){
                DateListPojo dateData = (DateListPojo) data.getSerializableExtra("date_data");
                if(dateData != null)
                    callDateResponseApi(dateData, DateResponseType.RESCHEDULE);
            }
        }
    }

    /*
     * help to decide date screen to launch.
     */
    private void launchRescheduleScreen(DateListPojo dateListPojo){
        if(dateListPojo != null) {
            if (dateListPojo.getDateType() == DateType.IN_PERSON_DATE.getValue()) {
                if(view != null)
                    view.launchPhyDateScreen(dateListPojo);
            }
            else if (dateListPojo.getDateType() == DateType.VIDEO_DATE.getValue()) {
                if(view != null)
                    view.launchCallDateScreen(dateListPojo);
            }
            else if (dateListPojo.getDateType() == DateType.AUDIO_DATE.getValue()) {
                if(view != null)
                    view.launchCallDateScreen(dateListPojo);
            }
        }
    }

    @Override
    public void onClick(int id, int position) {
        DateListPojo upcomingItemPojo = model.getUpcomingDateItem(position);
        currentPosition = position;
        switch (id){
            case R.id.reschedule_button:
                if(upcomingItemPojo != null) {
                    currentDateResponse = DateResponseType.RESCHEDULE;
                    loadAlertDialog(upcomingItemPojo);
                    //launchRescheduleScreen(upcomingItemPojo);
                }
                break;
            case R.id.cancel_button:
                if(upcomingItemPojo != null) {
                    currentDateResponse = DateResponseType.DENIED;
                    loadAlertDialog(upcomingItemPojo);
                    //callDateResponseApi(upcomingItemPojo, DateResponseType.DENIED);
                }
                break;
            case R.id.rl_header_upcoming:
            case R.id.main_image_view:
                if(upcomingItemPojo != null){
                    if(view != null)
                        view.launchUserProfile(upcomingItemPojo);
                }
                break;
            case R.id.call_button:
                if(upcomingItemPojo != null) {
                    initiateCall(upcomingItemPojo);
                }
                break;
            case  R.id.call_button_overlay:
                if(upcomingItemPojo != null){
                    showDateInfoDialog(upcomingItemPojo);
                }
        }
    }

    private void showDateInfoDialog(DateListPojo upcomingItemPojo) {
        if(upcomingItemPojo.getDateType() == DateType.VIDEO_DATE.getValue()){
            if(view != null)
                customAlertDialog.showDialog(DateType.VIDEO_DATE);
        }
        else if(upcomingItemPojo.getDateType() == DateType.AUDIO_DATE.getValue()){
            if(view != null)
                customAlertDialog.showDialog(DateType.AUDIO_DATE);
        }
        else{
            if(view != null)
                customAlertDialog.showDialog(DateType.IN_PERSON_DATE);
        }
    }

    private void loadAlertDialog(DateListPojo dateListPojo) {
        DateType dateType = DateType.IN_PERSON_DATE;
        if(dateListPojo.getDateType() == DateType.AUDIO_DATE.getValue()){
            dateType = DateType.AUDIO_DATE;
        }
        else if(dateListPojo.getDateType() == DateType.VIDEO_DATE.getValue()){
            dateType = DateType.VIDEO_DATE;
        }
        customAlertDialog.showDialog(dateListPojo.getOpponentName(),dateType,currentDateResponse,this);
    }

    private void requestAudioCall(DateListPojo upcomingDatePojo) {
        Map<String, Object> callItem = new HashMap<>();
        String callId = AppController.getInstance().randomString();
        callItem.put("receiverName", upcomingDatePojo.getOpponentName());
        callItem.put("receiverImage", upcomingDatePojo.getOpponentProfilePic());
        callItem.put("receiverUid", upcomingDatePojo.getOpponentId());
        callItem.put("callTime", Utilities.tsInGmt());
        callItem.put("callInitiated", true);
        callItem.put("callId", callId);
        callItem.put("callType", activity.getResources().getString(R.string.AudioCall));
        callItem.put("receiverIdentifier", upcomingDatePojo.getOpponentId());
        //callItem.put("dateId",upcomingDatePojo.getDataId());
        //AppController.getInstance().getDbController().addNewCall(AppController.getInstance().getCallsDocId(), callItem);
        Common.callerName = upcomingDatePojo.getOpponentName();
        CallingApis.initiateCall(activity, upcomingDatePojo.getDataId(),upcomingDatePojo.getOpponentId(), upcomingDatePojo.getOpponentName(),upcomingDatePojo.getOpponentProfilePic(),
                "0", upcomingDatePojo.getOpponentId(), callId);
    }

    private void intiateAudioCall(DateListPojo upcomingDatePojo){
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.RECORD_AUDIO},
                    71);
        } else {

            requestAudioCall(upcomingDatePojo);
        }
    }

    private void doAudioCallDate(DateListPojo upcomingItemPojo) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(activity) || !Settings.canDrawOverlays(activity)) {
                if (!Settings.System.canWrite(activity)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                    intent.setData(Uri.parse("package:" + activity.getPackageName()));
                    if(view != null)
                        activity.startActivity(intent);
                }

                //If the draw over permission is not available open the settings screen
                //to grant the permission.

                if (!Settings.canDrawOverlays(activity)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + activity.getPackageName()));
                    if(view != null)
                        activity.startActivity(intent);
                }
            } else {
                //showCallTypeChooserPopup(initiateDate);
                intiateAudioCall(upcomingItemPojo);
            }
        } else {
            //showCallTypeChooserPopup(initiateDate);
            intiateAudioCall(upcomingItemPojo);
        }
    }

    private void initiateCall(DateListPojo upcomingItemPojo) {
        if(upcomingItemPojo.getDateType() == 3){ //audio call
            doAudioCallDate(upcomingItemPojo);
        }
        else if(upcomingItemPojo.getDateType() == 1){ //video call
            doVideoCallDate(upcomingItemPojo);
        }else{//in-Person date
            if(view != null)
                view.launchDirectionScreen(upcomingItemPojo.getLatitude(),upcomingItemPojo.getLongitude(),upcomingItemPojo.getPlaceName());
        }
    }


    private void requestVideoCall(DateListPojo upcomingDatePojo) {

//        if(view != null)
//            view.sh
        Map<String, Object> callItem = new HashMap<>();
        String callId = AppController.getInstance().randomString();

        callItem.put("receiverName", upcomingDatePojo.getOpponentName());
        callItem.put("receiverImage", upcomingDatePojo.getOpponentProfilePic());
        callItem.put("receiverUid", upcomingDatePojo.getOpponentId());
        callItem.put("callTime", Utilities.tsInGmt());
        callItem.put("callInitiated", true);
        callItem.put("callId", callId);
        callItem.put("callType", activity.getResources().getString(R.string.VideoCall));
        callItem.put("receiverIdentifier", upcomingDatePojo.getOpponentId());
        //AppController.getInstance().getDbController().addNewCall(AppController.getInstance().getCallsDocId(), callItem);
        Common.callerName = upcomingDatePojo.getOpponentName();

        CallingApis.initiateCall(activity, upcomingDatePojo.getDataId(),upcomingDatePojo.getOpponentId(), upcomingDatePojo.getOpponentName(), upcomingDatePojo.getOpponentProfilePic(),
                "1", upcomingDatePojo.getOpponentId(), callId);

    }

    private void initiateVideoCall(DateListPojo upcomingItemPojo){

        ArrayList<String> arr1 = new ArrayList<>();
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            arr1.add(Manifest.permission.CAMERA);
        }

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            arr1.add(Manifest.permission.RECORD_AUDIO);
        }

        if (arr1.size() > 0) {

            ActivityCompat.requestPermissions(activity, arr1.toArray(new String[arr1.size()]),
                    72);
        } else {
            requestVideoCall(upcomingItemPojo);
        }
    }

    private void doVideoCallDate(DateListPojo upcomingItemPojo) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.System.canWrite(activity) || !Settings.canDrawOverlays(activity)) {
                if (!Settings.System.canWrite(activity)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                    intent.setData(Uri.parse("package:" + activity.getPackageName()));
                    activity.startActivity(intent);
                }
                //If the draw over permission is not available open the settings screen
                //to grant the permission.

                if (!Settings.canDrawOverlays(activity)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + activity.getPackageName()));
                    activity.startActivity(intent);
                }
            } else {
                //showCallTypeChooserPopup(initiateDate);
                initiateVideoCall(upcomingItemPojo);
            }
        } else {
            //showCallTypeChooserPopup(initiateDate);

            // Permission is not granted
            initiateVideoCall(upcomingItemPojo);
        }
    }

    private void callDateResponseApi(DateListPojo upcomingItemPojo, DateResponseType dateResponseType) {

        if(networkStateHolder.isConnected()) {
            this.dateListPojo = upcomingItemPojo;
            this.dateResponseType = dateResponseType;
            if(dateResponseType == DateResponseType.RESCHEDULE){
                model.replaceDateItem(dateListPojo,currentPosition);
            }
            else {
                DateListPojo removedDate = model.removeDateFromList(currentPosition);
                if (removedDate != null) {
                    if (view != null) {
                        if (dateResponseType == DateResponseType.DENIED) {
                            //canceled from upcoming
                            removedDate.setProposedOn(System.currentTimeMillis());
                            try {
                                PastDateListPojo pastDateListPojo = new PastDateListPojo(removedDate, utility.formatString(removedDate.getOpponentName()));
                                view.addTothePastDate(pastDateListPojo);
                            } catch (Exception e) {
                            }
                        }
                    }
                }
            }
//            if(view != null)
//                view.showData();
            if(view != null)
                view.notifyPendingAdapter();
            Map<String, Object> body = new HashMap<>();
            body.put(ApiConfig.PostDateResponse.RESPONSE, dateResponseType.getValue());
            body.put(ApiConfig.PostDateResponse.DATE_ID, upcomingItemPojo.getDataId());
            body.put(ApiConfig.PostDateResponse.PROPOSED_ON, upcomingItemPojo.getProposedOn());
            body.put(ApiConfig.PostDateResponse.DATE_TYPE, upcomingItemPojo.getDateType());
            body.put(ApiConfig.PostDateResponse.LATITUDE, upcomingItemPojo.getLatitude());
            body.put(ApiConfig.PostDateResponse.LONGITUDE, upcomingItemPojo.getLongitude());
            body.put(ApiConfig.PostDateResponse.PLACE_NAME, upcomingItemPojo.getPlaceName());

            service.postDateResponse(dataSource.getToken(), model.getLanguage(), body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            if (value.code() == 200) {
//                                if (view != null)
//                                    view.refreshList();
                                checkForCalenderPermission();
                            } else if (value.code() == 401) {
                                AppController.getInstance().appLogout();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            if (view != null) {
                                view.showError(activity.getString(R.string.failed_to_update_date_response));
                            }
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }else{
            if(view != null)
                view.showError(R.string.no_internet_error);
        }
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        if(this.dateResponseType == DateResponseType.DENIED) {
            if (dateListPojo != null)
                deletePreviousReminder(dateListPojo.getOpponentId());
        }
        else if(this.dateResponseType == DateResponseType.RESCHEDULE) {
            if (dateListPojo != null)
                setDateReminder();
        }
    }

    private void setDateReminder() {
        if(dateListPojo != null) {
            String personName = dateListPojo.getOpponentName();
            String location = dateListPojo.getPlaceName();
            String _userId = dateListPojo.getOpponentId();
            String dateType = "In person date";
            if(dateListPojo.getDateType() == DateType.AUDIO_DATE.getValue()){
                dateType = "Audio Date";
            }else if(dateListPojo.getDateType() == DateType.VIDEO_DATE.getValue()){
                dateType = "Video Date";
            }
            deletePreviousReminder(_userId);
            int eventId = calendarEventHelper.addDateReminder(dateListPojo.getProposedOn(), personName, location,dateType);
            DateEvent dateEvent = new DateEvent();
            dateEvent.setUserId(_userId);
            dateEvent.setEventId(String.valueOf(eventId));
            model.saveEventId(dateEvent);
        }
    }

    private void deletePreviousReminder(String userId) {
        String eventId = model.checkIfReminderExist(userId);
        if(!TextUtils.isEmpty(eventId)){
            calendarEventHelper.deleteEvent(eventId);
        }
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        app_permission.show_Alert_Permission(activity.getString(R.string.calender_access_text), activity.getString(R.string.calender_access_subtitle),
                activity.getString(R.string.location_acess_message),stringArray);
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean parmanent) {
    }


    @Override
    public void onOk() {
        DateListPojo dateListPojo = model.getUpcomingDateItem(currentPosition);
        if(dateListPojo != null) {
            if(currentDateResponse == DateResponseType.RESCHEDULE)
                launchRescheduleScreen(dateListPojo);
            else
                callDateResponseApi(dateListPojo,currentDateResponse);
        }
    }
}
