package com.pairzy.com.home.Prospects.Passed;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.home.HomeContract;
import com.pairzy.com.home.Prospects.OnAdapterItemClicked;
import com.pairzy.com.home.Prospects.Passed.Model.PassedUserAdapter;
import com.pairzy.com.userProfile.UserProfilePage;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.TypeFaceManager;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * <h2>PassedFrg</h2>
 * A simple {@link Fragment} subclass.
 * @author 3Embed.
 * @since 17-03-2018.
 * @version 1.0.
 */
public class PassedFrg extends DaggerFragment implements PassedContract.View,SwipeRefreshLayout.OnRefreshListener
{
    @Inject
    LinearLayoutManager linearLayoutManager;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    PassedUserAdapter passedUserAdapter;
    private Unbinder unbinder;
    @Inject
    PassedContract.Presenter presenter;
    @Inject
    HomeContract.Presenter main_presenter;
    @Inject
    Activity activity;
    @BindView(R.id.passed_list)
    RecyclerView item_list;
    @BindView(R.id.passed_loading_view)
    RelativeLayout loading_view;
    @BindView(R.id.passed_progress)
    ProgressBar online_progress;
    @BindView(R.id.passed_error_icon)
    ImageView online_error_icon;
    @BindView(R.id.passed_loading_text)
    TextView online_loading_text;
    @BindView(R.id.passed_error_msg)
    TextView online_error_msg;
    @BindView(R.id.passed_parent)
    SwipeRefreshLayout online_parent;
    @BindView(R.id.empty_data)
    RelativeLayout empty_data;
    @BindView(R.id.no_more_dara)
    TextView no_more_dara;
    @BindView(R.id.empty_details)
    TextView empty_details;
    @BindView(R.id.btn_retry)
    Button btnRetry;

    @Inject
    public PassedFrg() {}
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_passed_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder= ButterKnife.bind(this,view);
        presenter.takeView(this);
        presenter.initAdapterListener();
        presenter.initMatchListener();
        initUi();
        presenter.getListData(false);
    }

    /*
     * inti data view.*/
    private void initUi()
    {
        btnRetry.setTypeface(typeFaceManager.getCircularAirBold());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            online_progress.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(activity,R.color.colorAccent)));
        }else
        {
            Drawable progressDrawable = online_progress.getProgressDrawable().mutate();
            progressDrawable.setColorFilter(ContextCompat.getColor(activity,R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            online_progress.setProgressDrawable(progressDrawable);
        }
        online_parent.setColorSchemeResources(R.color.refresh2, R.color.refresh1,R.color.refresh);
        online_parent.setOnRefreshListener(this);
        no_more_dara.setTypeface(typeFaceManager.getCircularAirBold());
        empty_details.setTypeface(typeFaceManager.getCircularAirBook());
        item_list.setHasFixedSize(true);
        //item_list.setNestedScrollingEnabled(false);
        item_list.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {
                return false;
            }
        });
        item_list.setLayoutManager(linearLayoutManager);
        item_list.setAdapter(passedUserAdapter);
        item_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                int positionView=linearLayoutManager.findLastVisibleItemPosition();
                if(presenter.checkLoadMore(positionView))
                {
                    presenter.getListData(true);
                }
                presenter.pee_fetchProfile(positionView);
            }
        });
    }

    @Override
    public void onDestroy()
    {
        unbinder.unbind();
        presenter.dropView();
        super.onDestroy();
    }


    @Override
    public void onDataUpdate()
    {
        passedUserAdapter.notifyDataSetChanged();
        loading_view.setVisibility(View.GONE);
        empty_data.setVisibility(View.GONE);
        item_list.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        btnRetry.setVisibility(View.GONE);
        online_error_msg.setVisibility(View.GONE);
        online_error_icon.setVisibility(View.GONE);
        online_loading_text.setVisibility(View.VISIBLE);
        online_progress.setVisibility(View.VISIBLE);
        loading_view.setVisibility(View.VISIBLE);
        //item_list.setVisibility(View.GONE);
        empty_data.setVisibility(View.GONE);
    }

    @Override
    public void onApiError(String message)
    {
        btnRetry.setVisibility(View.VISIBLE);
        online_error_msg.setVisibility(View.VISIBLE);
        online_error_msg.setText(message);
        online_error_icon.setVisibility(View.VISIBLE);
        online_loading_text.setVisibility(View.GONE);
        online_progress.setVisibility(View.GONE);
        //item_list.setVisibility(View.GONE);
        empty_data.setVisibility(View.GONE);
        loading_view.setVisibility(View.VISIBLE);
    }

    @Override
    public void emptyData()
    {
        //item_list.setVisibility(View.GONE);
        loading_view.setVisibility(View.GONE);
        empty_data.setVisibility(View.VISIBLE);
    }

    @Override
    public void openUserProfile(String details)
    {
        Intent intent=new Intent(activity,UserProfilePage.class);
        Bundle intent_data=new Bundle();
        intent_data.putString(UserProfilePage.USER_DATA,details);
        intent.putExtras(intent_data);
        activity.startActivityForResult(intent, AppConfig.PROFILE_REQUEST);
    }

    @Override
    public void setAdapterListener(OnAdapterItemClicked callback) {
        passedUserAdapter.setPassedAdapterCallback(callback);
    }


    @OnClick(R.id.btn_retry)
    public void onRetry(){
        onRefresh();
    }

    @Override
    public void onRefresh() {
        online_parent.setRefreshing(false);
        presenter.getListData(false);
    }
}
