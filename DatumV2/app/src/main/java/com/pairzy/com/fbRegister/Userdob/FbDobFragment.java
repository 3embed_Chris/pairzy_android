package com.pairzy.com.fbRegister.Userdob;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.fbRegister.FbRegisterContact;
import com.pairzy.com.fbRegister.FbRegisterPage;
import com.pairzy.com.fbRegister.Gender.FbGenderFragment;
import com.pairzy.com.util.InputFilterMinMax;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * <h2>FbDobFragment</h2>
 * <P>
 * A simple {@link Fragment} subclass.
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 15.02.2017
 */
@ActivityScoped
public class FbDobFragment extends DaggerFragment implements FbDobContract.View
{
    @BindString(R.string.userNotAdult)
    String userNotAdult;
    @Override
    public String getAdultErrorMessage() {
        return userNotAdult;
    }
    @Inject
    Activity activity;
    @Inject
    Utility utility;
    @Inject
    FbRegisterContact.Presenter mainpresenter;
    @Inject
    FbDobContract.Presenter presenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @BindView(R.id.close_button)
    RelativeLayout close_button;
    @BindView(R.id.skip_page)
    TextView skip_page;
    @BindView(R.id.first_title)
    TextView first_title;
    @BindView(R.id.second_title)
    TextView second_title;
    @BindView(R.id.day1)
    EditText day1;
    @BindView(R.id.day2)
    EditText day2;
    @BindView(R.id.mon1)
    EditText mon1;
    @BindView(R.id.mon2)
    EditText mon2;
    @BindView(R.id.year1)
    EditText year1;
    @BindView(R.id.year2)
    EditText year2;
    @BindView(R.id.year3)
    EditText year3;
    @BindView(R.id.year4)
    EditText year4;
    @BindView(R.id.hint_details)
    TextView hint_details;
    @BindView(R.id.btnNext)
    RelativeLayout next_button;
    private Unbinder unbinder;
    private Handler handler;

    @Inject
    public FbDobFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_dob, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder= ButterKnife.bind(this,view);
        handler = new Handler(Looper.getMainLooper());
        initUIDetails();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        presenter.takeView(this);
        day1.requestFocus();
        utility.openSpotInputKey(activity,day1);
        mainpresenter.updateProgress(3);
        handelFocous();
    }


    @Override
    public void onDetach()
    {
        super.onDetach();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
    }

    @OnClick(R.id.calender_view)
    void openDateDialog()
    {
        utility.closeSpotInputKey(activity,day1);
        presenter.openDatePicker();
    }

    @OnClick(R.id.close_button)
    void onClose()
    {
        activity.onBackPressed();
    }

    @OnClick(R.id.btnNext)
    void onNext()
    {
        String day=day1.getText().toString()+""+day2.getText().toString();
        String month=mon1.getText().toString()+""+mon2.getText().toString();
        String year=year1.getText().toString()+""+year2.getText().toString()+year3.getText().toString()+""+year4.getText().toString();
        presenter.validateAge(day,month,year);
    }

    @OnClick(R.id.parentLayout)
    void onParentClick(){}

    @Override
    public void moveNextFragment(Double date_imliSec)
    {
        FbGenderFragment genderFragment=new FbGenderFragment();
        Bundle data=getArguments();
        if(data==null)
        {
            data=new Bundle();
        }
        data.putDouble(FbRegisterContact.Presenter.DOB_DATA,date_imliSec);
        genderFragment.setArguments(data);
        FbRegisterPage.details.setBirthDate(String.valueOf(date_imliSec));
        mainpresenter.launchNextValidFrag(2,true);
    }

    @OnClick(R.id.skip_page)
    void onSkipCLicked()
    {
        moveNextFragment(0.0);
    }

    /*
   * intialization of the xml content.*/
    private void initUIDetails()
    {
        next_button.setEnabled(false);
        skip_page.setTypeface(typeFaceManager.getCircularAirBook());
        first_title.setTypeface(typeFaceManager.getCircularAirBold());
        second_title.setTypeface(typeFaceManager.getCircularAirBold());
        day1.setTypeface(typeFaceManager.getCircularAirBook());
        day1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.toString().equals("0"))
                {
                    day2.setFilters(new InputFilter[]{new InputFilterMinMax(1,9)});
                }else if(editable.toString().equals("3"))
                {
                    day2.setFilters(new InputFilter[]{new InputFilterMinMax(0,1)});
                }else
                {
                    day2.setFilters(new InputFilter[]{new InputFilterMinMax(0,9)});
                }
                if(editable.length()>0)
                {
                    day2.requestFocus();
                }
                validateInput();
            }
        });
        day1.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                day1.setText("");
                day1.requestFocus();
            }
            return false;
        });
        day1.setFilters(new InputFilter[]{new InputFilterMinMax(0,3)});

        day2.setTypeface(typeFaceManager.getCircularAirBook());
        day2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.length()>0)
                {
                    mon1.requestFocus();
                }
                validateInput();
            }});
        day2.setFilters(new InputFilter[]{new InputFilterMinMax(0,9)});
        day2.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                day1.setText("");
                day1.requestFocus();
            }
            return false;
        });

        mon1.setTypeface(typeFaceManager.getCircularAirBook());
        mon1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.toString().equals("0"))
                {
                    mon2.setFilters(new InputFilter[]{new InputFilterMinMax(1,9)});
                }else
                {
                    mon2.setFilters(new InputFilter[]{new InputFilterMinMax(0,2)});
                }
                if(editable.length()>0)
                {
                    mon2.requestFocus();
                }
                validateInput();
            }
        });
        mon1.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                day2.setText("");
                day2.requestFocus();
            }
            return false;
        });
        mon1.setFilters(new InputFilter[]{new InputFilterMinMax(0,1)});

        mon2.setTypeface(typeFaceManager.getCircularAirBook());
        mon2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.length()>0)
                {
                    year1.requestFocus();
                }
                validateInput();
            }
        });
        mon2.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                mon1.setText("");
                mon1.requestFocus();
            }
            return false;
        });
        mon2.setFilters(new InputFilter[]{new InputFilterMinMax(0,9)});

        year1.setTypeface(typeFaceManager.getCircularAirBook());
        year1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.toString().equals("1"))
                {
                    year2.setFilters(new InputFilter[]{new InputFilterMinMax(9,9)});
                }else
                {
                    year2.setFilters(new InputFilter[]{new InputFilterMinMax(0,0)});
                }
                if(editable.length()>0)
                {
                    year2.requestFocus();
                }
                validateInput();
            }
        });

        year1.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                mon2.setText("");
                mon2.requestFocus();
            }
            return false;
        });

        year1.setFilters(new InputFilter[]{new InputFilterMinMax(1,2)});
        year2.setTypeface(typeFaceManager.getCircularAirBook());
        year2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.toString().equals("9"))
                {
                    year3.setFilters(new InputFilter[]{new InputFilterMinMax(0,9)});

                }else
                {
                    year3.setFilters(new InputFilter[]{new InputFilterMinMax(0,presenter.getCurrentYear()[2])});
                }
                if(editable.length()>0)
                {
                    year3.requestFocus();
                }
                validateInput();
            }
        });
        year2.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                year1.setText("");
                year1.requestFocus();
            }
            return false;
        });

        year3.setTypeface(typeFaceManager.getCircularAirBook());
        year3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(year2.getText().toString().equals("9"))
                {
                    year4.setFilters(new InputFilter[]{new InputFilterMinMax(0,9)});
                }else
                {
                    year4.setFilters(new InputFilter[]{new InputFilterMinMax(0,presenter.getCurrentYear()[3])});
                }

                if(editable.length()>0)
                {
                    year4.requestFocus();
                }
                validateInput();
            }
        });
        year3.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                year2.setText("");
                year2.requestFocus();
            }
            return false;
        });

        year3.setFilters(new InputFilter[]{new InputFilterMinMax(0,presenter.getCurrentYear()[2])});
        year4.setTypeface(typeFaceManager.getCircularAirBook());
        year4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.length()>0)
                {
                    year4.requestFocus();
                }
                validateInput();
            }
        });
        year4.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                if(year4.getText().toString().length()<1)
                {
                    year3.setText("");
                    year3.requestFocus();
                }else
                {
                    year4.setText("");
                    year4.requestFocus();
                }
            }
            return false;
        });
        year4.setFilters(new InputFilter[]{new InputFilterMinMax(0,presenter.getCurrentYear()[3])});
    }

    @Override
    public void showMessage(String message)
    {
        mainpresenter.showMessage(message);
    }

    @Override
    public void onError(String error) {
        mainpresenter.showError(error);
    }

    /*
     *Handling the focous change.*/
    private void handelFocous()
    {
        if(day1.getText().length()<1)
        {
            day1.requestFocus();
        }else if(day2.getText().length()<1)
        {
            day2.requestFocus();
        }else if(mon1.getText().length()<1)
        {
            mon1.requestFocus();
        }else if(mon2.getText().length()<1)
        {
            mon2.requestFocus();
        }else if(year1.getText().length()<1)
        {
            year1.requestFocus();
        }else if(year2.getText().length()<1)
        {
            year2.requestFocus();
        }else if(year3.getText().length()<1)
        {
            year3.requestFocus();
        }else
        {
            year4.requestFocus();
        }
    }

    @Override
    public void onDateSelected(String day, String month, String year)
    {
        day1.setText(String.format("%s", day.charAt(0)));
        day2.setText(String.format("%s", day.charAt(1)));
        mon1.setText(String.format("%s", month.charAt(0)));
        mon2.setText(String.format("%s", month.charAt(1)));
        year1.setText(String.format("%s", year.charAt(0)));
        year2.setText(String.format("%s", year.charAt(1)));
        year3.setText(String.format("%s", year.charAt(2)));
        year4.setText(String.format("%s", year.charAt(3)));
        validateInput();
    }

    /*
     * Validating the input*/
    private void validateInput()
    {
        boolean isCompleted;
        if(day1.getText().length()<1)
        {
            isCompleted=false;
            day1.requestFocus();
        }else if(day2.getText().length()<1)
        {
            isCompleted=false;
            day2.requestFocus();
        }else if(mon1.getText().length()<1)
        {
            isCompleted=false;
            mon1.requestFocus();
        }else if(mon2.getText().length()<1)
        {
            isCompleted=false;
            mon2.requestFocus();
        }else if(year1.getText().length()<1)
        {
            isCompleted=false;
            year1.requestFocus();
        }else if(year2.getText().length()<1)
        {
            isCompleted=false;
            year2.requestFocus();
        }else if(year3.getText().length()<1)
        {
            isCompleted=false;
            year3.requestFocus();
        }else if(year4.getText().length()<1)
        {
            isCompleted=false;
            year4.requestFocus();
        }else
        {
            isCompleted=true;
            year4.setSelection(year4.getText().length());
        }
        handelNextButton(isCompleted);
    }

    /*
    *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            next_button.setEnabled(true);
        }
        else {
            next_button.setEnabled(false);
        }
    }
}
