package com.pairzy.com.home.Prospects.MySuperlikes.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * @since  3/28/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MySuperLikeResponseHolder
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<MySuperlikesItemPojo> data = null;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public ArrayList<MySuperlikesItemPojo> getData()
    {
        return data;
    }

    public void setData(ArrayList<MySuperlikesItemPojo> data)
    {
        this.data = data;
    }
}
