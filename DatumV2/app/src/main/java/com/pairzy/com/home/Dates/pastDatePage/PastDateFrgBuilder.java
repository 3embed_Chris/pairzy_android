package com.pairzy.com.home.Dates.pastDatePage;

import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>PastDateFrgBuilder</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface PastDateFrgBuilder
{
    @FragmentScoped
    @Binds
    PastDateFrg getPendingFragment(PastDateFrg availableFrg);

    @FragmentScoped
    @Binds
    PastDateFrgContract.Presenter pastDateFragPresenter(PastDateFrgPresenter presenter);

}
