package com.pairzy.com.PostMoments.model;

import com.pairzy.com.BaseModel;
import com.pairzy.com.Utilities.Constants;
import com.pairzy.com.data.model.cloudinaryDetail.CloudinaryData;
import com.pairzy.com.data.model.cloudinaryDetail.CloudinaryDetailResponse;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.CloudManager.UploadManager;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Utility;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class PostModel extends BaseModel {

    @Inject
    Utility utility;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    UploadManager uploadManager;

    @Inject
    PostModel(){}

    public Map<String, Object> sendBody(String path, String type,String caption) {
        String flag;
        if(type.equals(Constants.Post.IMAGE))
          flag="3";
        else
            flag="4";

        Map<String,Object> map=new HashMap<>();
        map.put(ApiConfig.POST.TYPE_FLAG,flag);
        map.put(ApiConfig.POST.DESC,caption);
        map.put(ApiConfig.POST.URL,path);
        return map;
    }

    public void parseClodinaryDetail(String response) throws DataParsingException {
        try{
            CloudinaryDetailResponse cloudinaryDetailResponse =
                    utility.getGson().fromJson(response,CloudinaryDetailResponse.class);
            CloudinaryData cloudinaryData = cloudinaryDetailResponse.getCloudinaryData();

            dataSource.setCloudName(cloudinaryData.getCloudName());
            dataSource.setCloudinaryApiKey(cloudinaryData.getApiKey());
            dataSource.setClodinaryApiSecret(cloudinaryData.getApiSecret());

            reinitCloudnaryUpload();
        }catch (Exception e){
            throw new DataParsingException(e.getMessage());
        }
    }

    private void reinitCloudnaryUpload() {
        uploadManager.setNewCreds(true,
                dataSource.getCloudName(),
                dataSource.getCloudinaryApiKey(),
                dataSource.getCloudinaryApiSecret());
    }
}