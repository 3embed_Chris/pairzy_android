package com.pairzy.com.home.Discover.ListFrg;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.R;
import com.pairzy.com.home.Discover.Model.AdapterCallback;
import com.pairzy.com.home.Discover.Model.AgeResponse;
import com.pairzy.com.home.Discover.Model.UserItemPojo;
import com.pairzy.com.home.Discover.Model.UserMediaPojo;
import com.pairzy.com.home.HomeModel.HomeAnimation;
import com.pairzy.com.util.DatumCallbacks.ListItemClick;
import com.pairzy.com.util.LoadingViews.LoadMoreViewHolder;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

import java.util.List;
import java.util.Locale;
/**
 * <h2>ListDataAdapter</h2>
 * <P>
 *
 * </P>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ListDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private final int LOADING=0,IMAGE_ITEM=1,AD_VIEW = 2;
    private List<UserItemPojo> albumList;
    private TypeFaceManager typeFaceManager;
    private Utility utility;
    private ListItemClick callBack;
    private AdapterCallback adpCallback;
    private GridLayoutManager.SpanSizeLookup onSpanSizeLookup=null;
    private HomeAnimation homeAnimation;

    public ListDataAdapter(TypeFaceManager typeFaceManager, Utility utility, List<UserItemPojo> albumList,HomeAnimation homeAnimation)
    {
        init_Span_look_Up();
        this.utility=utility;
        this.albumList = albumList;
        this.typeFaceManager=typeFaceManager;
        this.homeAnimation = homeAnimation;
        initCallBack();
    }

    /*
     * Setting the callback*/
    public void setEventBack(AdapterCallback adpCallback)
    {
        this.adpCallback=adpCallback;
    }

    @Override
    public int getItemViewType(int position)
    {
        UserItemPojo temp=albumList.get(position);
        if(temp.isLoading())
        {
            return LOADING;
        }
        else if(temp.isAdView()){

            return AD_VIEW;
        }
        else
        {
            return IMAGE_ITEM;
        }
    }
    /**
     * <h2>init_Span_look_Up</h2>
     * Helper class to set span size for grid items based on orientation and device type
     */
    private void init_Span_look_Up()
    {
        if(onSpanSizeLookup==null)
        {
            onSpanSizeLookup = new GridLayoutManager.SpanSizeLookup()
            {
                @Override
                public int getSpanSize(int position)
                {
                    if(albumList.get(position).isLoading())
                    {
                        return 2;
                    }else
                    {
                        return 1;
                    }
                }
            };
        }}

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        RecyclerView.ViewHolder holder;
        switch (viewType)
        {
            case LOADING:
                View loading_item = LayoutInflater.from(parent.getContext()).inflate(R.layout.load_more_layout,parent, false);
                return new LoadMoreViewHolder(loading_item,typeFaceManager,callBack);
            case IMAGE_ITEM:
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item_view, parent, false);
                return new ListDataViewHolder(itemView,callBack);
            case AD_VIEW:
                View adView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_ad_view, parent, false);
                return new AdViewListViewHolder(adView,callBack);
            default:
                View image_item = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item_view,parent, false);
                return new ListDataViewHolder(image_item,callBack);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        switch (holder.getItemViewType())
        {
            case LOADING:
                loadingView((LoadMoreViewHolder) holder);
                break;
            case IMAGE_ITEM:
                try {
                    initData((ListDataViewHolder) holder);
                }catch (Exception e){}
                break;
            case AD_VIEW:
                try {
                    initAdData((AdViewListViewHolder) holder);
                }catch (Exception e){
                    e.getMessage();
                }
                break;
            default:
                try
                {
                    initData((ListDataViewHolder) holder);
                }catch (Exception e){}
        }
    }

    private void populateNativeAdView(UnifiedNativeAd nativeAd,
                                      UnifiedNativeAdView adView) {
        // Some assets are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        NativeAd.Image icon = nativeAd.getIcon();

        if (icon == null) {
            adView.getIconView().setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(icon.getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAd);
    }

    private void initAdData(AdViewListViewHolder holder) {
        int position=holder.getAdapterPosition();
        UserItemPojo data=albumList.get(position);
        holder.getAdView().setVisibility(View.VISIBLE);
        UnifiedNativeAd nativeAd = data.getAd();
        if(nativeAd != null)
            populateNativeAdView(nativeAd,holder.getAdView());
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        GridLayoutManager layoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
        layoutManager.setSpanSizeLookup(onSpanSizeLookup);
        super.onAttachedToRecyclerView(recyclerView);
    }


    /*
     *Handling the loading view */
    private void loadingView(LoadMoreViewHolder holder)
    {
        int position=holder.getAdapterPosition();
        UserItemPojo data=albumList.get(position);
        if(data.isLoadingFailed())
        {
            holder.setFailed();
        }else
        {
            holder.showLoadingLoadingAgain();
        }
    }

    /*
     * init data for the user details.*/
    private void initData(ListDataViewHolder holder)
    {
        int position=holder.getAdapterPosition();
        UserItemPojo data=albumList.get(position);
        if(data.getMedia_list()!=null)
        {
            UserMediaPojo media_temp=data.getMedia_list().get(data.getCurrentImagePos());
            if(media_temp.isVideo()) {
                String uri =media_temp.getVideo_url();
                //Log.d(ListAdapter.class.getName(),"video url "+uri);
                try {
                    uri = uri.replace("upload/", "upload/vs_20,dl_200,h_200,e_loop/").replace(".mp4", ".gif").replace(".mov",".gif");
                    //Log.d(ListAdapter.class.getName(),"gif url "+uri);
                }catch (Exception e){}
                DraweeController controller  = Fresco.newDraweeControllerBuilder()
                        .setUri(uri)
                        .setAutoPlayAnimations(true)
                        .build();
                //holder.thumbnail.setImageURI(media_temp.getVideo_thumbnail());
                holder.thumbnail.setController(controller);
            }
            else{
                holder.thumbnail.setImageURI(data.getProfilePic());
            }
        }
        else {
            holder.thumbnail.setImageURI(data.getProfilePic());
        }

        if(data.isSuperlikedMe() > 0)
        {
            holder.ivSuperlikeMe.setVisibility(View.VISIBLE);
            holder.ivSuperlikeMe.startAnimation(homeAnimation.getSlowBlinkingAnimation());
        }
        else
        {
            holder.ivSuperlikeMe.setVisibility(View.GONE);
        }
        holder.user_name.setTypeface(typeFaceManager.getCircularAirBold());
        AgeResponse ageResponse =  data.getAge();
        if(ageResponse != null ) {
            if (ageResponse.getIsHidden() == 0) {
                holder.user_name.setText(String.format(Locale.ENGLISH, "%s, %d", utility.formatString(data.getFirstName()), ageResponse.getValue()));
            } else{
                holder.user_name.setText(String.format(Locale.ENGLISH, "%s ", utility.formatString(data.getFirstName())));
            }
        }

        if(data.getOnlineStatus() == 1){
            holder.user_status.setImageResource(R.drawable.online_dot);
        }
        else{
            holder.user_status.setImageResource(0);
        }

        if(!TextUtils.isEmpty(data.getProfileVideoThumbnail())){
            holder.videoView.setVisibility(View.VISIBLE);
        }
        else{
            holder.videoView.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    public int getItemCount()
    {
        return albumList.size();
    }

    /*
     *inti xml content */
    private void initCallBack()
    {
        callBack= (id, position) -> {
            try
            {
                switch (id)
                {
                    case R.id.dislike_view:
                        adpCallback.onDislike(position);
                        break;
                    case R.id.boots_view:
                        adpCallback.onBoots(position);
                        break;
                    case R.id.like_view:
                        adpCallback.onLike(position);
                        break;
                    case R.id.superlike_view:
                        adpCallback.onSuperLike(position);
                        break;
                    case R.id.video_view:
                    case R.id.card_view:
                        adpCallback.openUserDetails(position,null);
                        break;
                    case R.id.chat_view:
                        UserItemPojo userItemPojo = null;
                        try {
                            userItemPojo = albumList.get(position);
                        }catch (Exception e){}
                        if(userItemPojo != null)
                            adpCallback.openChatScreen(userItemPojo);
                }
            }catch (Exception e){}
        };
    }

    /*
     *Remove item from list . */
    public UserItemPojo removeItem(int index)
    {
        UserItemPojo temp_data=albumList.remove(index);
        temp_data.item_actual_pos=index;
        notifyDataSetChanged();
        return temp_data;
    }
}
