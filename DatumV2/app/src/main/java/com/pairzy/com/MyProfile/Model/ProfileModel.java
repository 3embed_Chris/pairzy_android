package com.pairzy.com.MyProfile.Model;

import android.app.Activity;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;

import com.androidinsta.com.ImageData;
import com.pairzy.com.BaseModel;
import com.pairzy.com.MyProfile.MyProfilePagePresenter;
import com.pairzy.com.R;
import com.pairzy.com.boostDetail.model.SubsPlan;
import com.pairzy.com.boostDetail.model.SubsPlanResponse;
import com.pairzy.com.data.model.MyPrefrance;
import com.pairzy.com.data.model.PrefData;
import com.pairzy.com.data.model.Subscription;
import com.pairzy.com.data.model.SubscriptionResponse;
import com.pairzy.com.data.source.PassportLocationDataSource;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.passportLocation.model.PassportLocation;
import com.pairzy.com.userProfile.Model.AgeResponse;
import com.pairzy.com.userProfile.Model.DistanceResponse;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.DeviceUuidFactory;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.Utility;
import com.suresh.innapp_purches.SkuDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
/**
 * <h2>ProfileModel</h2>
 * <P>
 *
 * </P>
 * @since  4/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ProfileModel extends BaseModel
{

    private String editedWork = "";
    private String editedJob = "";
    private String editedEducation = "";
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    ArrayList<ProfileMediaPojo> media_list;
    @Inject
    ProfileMediaAdapter adapter;
    @Inject
    ProfileViewModel viewModel;
    @Inject
    PassportLocationDataSource locationDataSource;

    @Inject
    Utility utility;
    @Inject
    Activity activity;

    @Inject
    ArrayList<SubsPlan> subsPlanList;
    @Inject
    DeviceUuidFactory deviceUuidFactory;
    @Inject
    ArrayList<MomentsData> momentsDataList;
    @Inject
    ArrayList<ImageData> instaMedia;

    @Inject
    ProfileModel(){}

    /*
     *Managing for my requirement */
    private ArrayList<View> views=new ArrayList<>();
    private ArrayList<MyPrefrance> myPreference;
    private ProfileData data = new ProfileData();

    /*
     * Parsing the data from the server*/
    public void parseData(String response) throws EmptyData,DataParsingException
    {
        try
        {
            //Log.d("ert43", ":"+response);
            ProfileResponseHolder result_data=utility.getGson().fromJson(response,ProfileResponseHolder.class);
            data=result_data.getData();
            if(data!=null)
            {
                dataSource.setMyDetails(response);
                dataSource.setName(data.getFirstName());
                dataSource.setBirthDate(data.getDateOfBirth());
                dataSource.setCountryCode(data.getCountryCode());
                dataSource.setMobileNumber(data.getMobileNumber());
                dataSource.setGender(data.getGender());
                dataSource.setEmail(data.getEmailId());
                try{
                    data.getOtherImages().remove(data.getProfilePic());
                }catch (Exception ignored){}
                dataSource.setProfilePicture(data.getProfilePic());
                dataSource.setUserVideoThumbnail(data.getProfileVideoThumbnail());
                dataSource.setUserVideo(data.getProfileVideo());
                dataSource.setUserOtherImages(data.getOtherImages());
                dataSource.setMyAbout(data.getAbout());
//                ArrayList<String> selectedJob = data.getMyPreference().get(0).getData().get(1).getSelectedValues();

                /*
                * Storing the preference*/
                myPreference = data.getMyPreference();
                String mySearch_details=utility.getGson().toJson(myPreference);
                dataSource.setMyPreference(mySearch_details);

                momentsDataList.clear();
                momentsDataList.addAll(data.getMoments());

                /*
                 *Updating the user details. */
                //createMediaList();
            }else
            {
                throw new EmptyData(activity.getString(R.string.details_is_empty));
            }
        }catch (Exception e)
        {
            throw new DataParsingException(e.getMessage());
        }
    }

    /*
     *Creating the user media list */
    public void createMediaList()
    {
        media_list.clear();
        adapter.notifyDataSetChanged();
        if(!TextUtils.isEmpty(dataSource.getUserVideoThumbnail()))
        {
            ProfileMediaPojo video_item=new ProfileMediaPojo();
            video_item.setVideo_thumbnail(dataSource.getUserVideoThumbnail());
            video_item.setVideo_url(dataSource.getUserVideoUrl());
            video_item.setVideo(true);
            media_list.add(video_item);
        }

        /*
         *Adding the user image in list */
        ProfileMediaPojo user_image=new ProfileMediaPojo();
        user_image.setImage_url(dataSource.getProfilePicture());
        user_image.setVideo(false);
        media_list.add(user_image);
        try
        {
            ArrayList<String> otherImage=dataSource.getUserOtherImages();
            if(otherImage!=null)
            {
                for(String other_img:otherImage)
                {
                    ProfileMediaPojo other_media=new ProfileMediaPojo();
                    other_media.setImage_url(other_img);
                    other_media.setVideo(false);
                    media_list.add(other_media);
                }
            }
        }catch (Exception e){}
        adapter.notifyDataSetChanged();
    }

    /*
     * Media total*/
    public int getMediaTotal()
    {
        return media_list.size();
    }


    /*
     * Getting the details view*/
    public ArrayList<View> getDetailsView(MyProfilePagePresenter presenter)
    {
        views.clear();
        View view=viewModel.getUserDetailsView(presenter,utility.formatString(dataSource.getName()),dataSource.getGender(),dataSource.getBirthDate().equals("")?21:(dataSource.getBirthDate().equals("0")?0:utility.getAgeFromDob(Double.parseDouble(dataSource.getBirthDate()))),dataSource.getMobileNumber(),dataSource.getEmail(),getSavedLocation());
        views.add(view);
        View about_view=viewModel.addEditTextView(dataSource.getMyAbout(),presenter);
        views.add(about_view);
        for(MyPrefrance temp:myPreference)
        {
            View view1=viewModel.addDetailsView(temp.getData(),temp.getTitle(),presenter);
            views.add(view1);
        }
        return views;
    }


    public View getLoadingView()
    {
        return viewModel.loadingView();
    }

    public View getErrorOnLoadingView(String error,MyProfilePagePresenter presenter)
    {
        return viewModel.errorOnLoadingView(error,presenter);
    }


    /**
     * @param id : instagram id
     * @return parameters
     */
    public Map<String, Object> updateInstaId(String id,String name,String token)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.UpdateInstaId.ID, id);
        map.put(ApiConfig.UpdateInstaId.TOKEN, token);
        map.put(ApiConfig.UpdateInstaId.USER_NAME, name);
        return map;
    }

    /*
     *it will save the edited photo and video data to EditProfile object.
     */
    public void keepEditPhotoVideos() {
        data.setProfileVideo(dataSource.getUserVideoUrl());
        data.setProfileVideoThumbnail(dataSource.getUserVideoThumbnail());
        data.setProfilePic(dataSource.getProfilePicture());
        data.setOtherImages(dataSource.getUserOtherImages());
    }

    /**
     * set the edited phone to EditProfile object.
     * @param editedPhone
     */
    public void keepEditedPhone(String editedPhone) {
        if(data != null){
            data.setMobileNumber(editedPhone);
        }
    }

    /**
     * set the edited dob to EditProfile object.
     * @param editedDobMiliSec
     */
    public void keepEditedDob(Double editedDobMiliSec) {
        if(data != null){
            data.setDateOfBirth(String.valueOf(editedDobMiliSec));
        }
    }

    /**
     * set the edited email to EditProfile object.
     * @param editedEmail
     */
    public void keepEditedEmail(String editedEmail) {
        if(data != null){
            data.setEmailId(editedEmail);
        }
    }

    /**
     * set the edited Name to EditProfile object.
     * @param editedName
     */
    public void keepEditedName(String editedName) {
        if(data != null){
            data.setFirstName(editedName);
        }
    }

    /**
     * set the edited Gender to EditProfile object.
     * @param editedGender
     */
    public void keepEditedGender(String editedGender) {
        if(data != null){
            data.setGender(editedGender);
        }
    }

    /**
     * set the edited PrefData object to MyPreferenceList at appropriate position.
     * @param prefData
     */
    public void keepEditedPrefData(PrefData prefData) {
        if(data != null){
            for(MyPrefrance myPrefrance :data.getMyPreference()){
                if(myPrefrance.getTitle().equals(prefData.getTitle())){
                    for(int i =0; i < myPrefrance.getData().size();i++){
                        PrefData prefData1 = myPrefrance.getData().get(i);
                        if(prefData.getId().equals(prefData1.getId())){
                            myPrefrance.getData().set(i,prefData);
                            if(prefData.getLabel().equalsIgnoreCase("work")){
                                editedWork = getSelectedString(prefData);
                            }
                            else if(prefData.getLabel().equalsIgnoreCase("job")){
                                editedJob = getSelectedString(prefData);
                            }
                            if(prefData.getLabel().equalsIgnoreCase("education")){
                                editedEducation = getSelectedString(prefData);
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    /*helper method*/
    private String getSelectedString(PrefData prefData){
        try {
            List<String> selectedList = prefData.getSelectedValues();
            return selectedList.size() > 0 ? selectedList.get(0) : "";
        }catch (Exception e){}
        return "";
    }

    /*
     * It will return the Edit Profile Api BODY Object.
     */
    public EditProfileData getMapBody() {
        try {
            EditProfileData editProfileData = new EditProfileData(data);
            JSONArray jsonArray = new JSONArray();
            for(MyPrefrance myPrefrance :data.getMyPreference()){
                for(int i =0; i < myPrefrance.getData().size();i++){
                    PrefData prefData1 = myPrefrance.getData().get(i);
                    JSONObject jsonObject = new JSONObject();
                    try {
                        if(prefData1.getSelectedValues()!= null && !prefData1.getSelectedValues().isEmpty()) {
                            jsonObject.put("pref_id", prefData1.getId());
                            jsonObject.put("selectedValues", new JSONArray(prefData1.getSelectedValues()));
                            jsonArray.put(jsonObject);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if(jsonArray.length() > 0)
                editProfileData.setMyPreferences(jsonArray.toString());
            editProfileData.setType(dataSource.getProfileUpdateType());
            return editProfileData;
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * set the edited showDist boolean to EditProfile object.
     * @param dontShow
     * @return "isReallyChange" will be used to toggle visibility of tickMark button.
     */
    public boolean saveDistancePref(boolean dontShow) {
        boolean isReallyChanged = false;
        try {
            if (data != null) {
                DistanceResponse distResponse = data.getDistResponse();
                isReallyChanged = dontShow != distResponse.getIsHidden()>0;
                distResponse.setIsHidden((dontShow)?1:0);
                data.setDistResponse(distResponse);
            }
        }catch (Exception e){
        }
        return isReallyChanged;
    }

    /**
     * set the edited showAge boolean to EditProfile object.
     * @param dontShow
     * @return "isReallyChange" will be used to toggle visibility of tickMark button.
     */
    public boolean saveAgePref(boolean dontShow) {
        boolean isReallyChanged = false;
        try {
            if (data != null) {
                AgeResponse ageResponse = data.getAgeResponse();
                isReallyChanged = dontShow != ageResponse.getIsHidden()>0;
                ageResponse.setIsHidden((dontShow)?1:0);
                data.setAgeResponse(ageResponse);
            }
        }catch (Exception e){}
        return isReallyChanged;
    }

    /**
     * set the edited About text to EditProfile object.
     * @param newText
     */
    public void keepNewAboutText(String newText){
        if(data != null)
            data.setAbout(newText);
    }

    /*
     * Return the isAgeHidden boolean value.
     */
    public boolean isAgeHidden() {
        if(data != null)
         return data.getAgeResponse().getIsHidden() > 0;
        return true;
    }

    /*
     * Return the isAgeHidden boolean value.
     */
    public boolean isDistHidden() {
        if(data != null)
            return data.getDistResponse().getIsHidden() > 0;
        return true;
    }

    public void saveEditedProfile() {
        try {
            if(!TextUtils.isEmpty(editedWork))
                dataSource.setMyWorkPlace(editedWork);
            if(!TextUtils.isEmpty(editedEducation))
                dataSource.setMyEducation(editedEducation);
            if(!TextUtils.isEmpty(editedJob))
                dataSource.setMyJob(editedJob);
            if(!TextUtils.isEmpty(data.getGender())){
                dataSource.setGender(data.getGender());
            }
            if(data != null && !TextUtils.isEmpty(data.getFirstName())){
                dataSource.setName(data.getFirstName());
            }
        }catch (Exception e){}
    }


    //BoostResponse dialog methods
    public boolean isSubsPlanEmpty() {
        return  subsPlanList.isEmpty();
    }

    public void selectMiddleItem() {
        if(!isSubsPlanEmpty()){
            for(SubsPlan subsPlan : subsPlanList)
                if(subsPlan.isSelected())
                    return;
            int pos = getSubsPlanListSize() / 2;
            SubsPlan subsPlan = subsPlanList.get(pos);
            subsPlan.setSelected(true);
            subsPlan.setHeaderTag("MOST POPULAR");
        }
    }

    public int getSubsPlanListSize() {
        return subsPlanList.size();
    }

    public ArrayList<SubsPlan> getSubsPlanList() {
        return subsPlanList;
    }

    public void parseSubsPlanList(String response) {
        try{
            subsPlanList.clear();
            SubsPlanResponse subsPlanResponse = utility.getGson().fromJson(response,SubsPlanResponse.class);
            ArrayList<SubsPlan> subsPlans = subsPlanResponse.getData();
            if(subsPlans != null){
                subsPlanList.addAll(subsPlans);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getSubsPurchaseId(int position) {
        try {
            if (position < getSubsPlanListSize()) {
                return subsPlanList.get(position).getActualIdForAndroid();
            }
        }catch (Exception ignored){}
        return "";
    }

    public String getSavedLocation()
    {
        try {
            String locationJson = locationDataSource.getFeatureLocation();
            boolean currentLoc = false;
            if(!TextUtils.isEmpty(locationJson)) {
                PassportLocation featureLocation = utility.getGson().fromJson(locationJson, PassportLocation.class);
                if(!TextUtils.isEmpty(featureLocation.getSubLocationName())){
                    return featureLocation.getSubLocationName();
                }
                else {
                    currentLoc = true;
                }
            }
            else{
                currentLoc = true;
            }

            if(currentLoc){
                PassportLocation currentLocation = parseCurrentLocation();
                if(currentLocation.getSubLocationName().isEmpty()){
                    return "Add Your Location...";
                }
                return currentLocation.getSubLocationName();
            }
        }catch (Exception e){
        }
        return "Add Your Location...";
    }

    private PassportLocation parseCurrentLocation() {
        try {
            String currentLocationJson = locationDataSource.getCurrentLocation();
            PassportLocation currentLocation = utility.getGson().fromJson(currentLocationJson, PassportLocation.class);
            return currentLocation;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public List<String> collectsIds()
    {
        List<String> items=new ArrayList<>();
        for(SubsPlan item :subsPlanList)
        {
            items.add(item.getActualIdForAndroid());
        }
        return items;
    }

    public void updateDetailsData(List<SkuDetails> details)
    {
        for(SkuDetails item:details)
        {
            String id=item.getProductId();
            for(SubsPlan product :subsPlanList)
            {
                if(id.equals(product.getActualIdForAndroid()))
                {
                    product.setPrice_text(item.getPriceText());
                    product.setCurrencySymbol(item.getCurrency());
                }
            }
        }
    }

    public void clearProductList()
    {
        subsPlanList.clear();
    }

    public List<List<ImageData>> prepareInstaResponse(ArrayList<ImageData> response)
    {
        instaMedia.clear();
        instaMedia.addAll(response);
        response.add(new ImageData());
        return chopIntoParts(response,6);
    }


    private  <T>List<List<T>> chopIntoParts( final List<T> ls,int part_size)
    {
        final List<List<T>> lsParts = new ArrayList<List<T>>();
        int lat_position=part_size;
        int start_pos=0;
        for( int i=0;i<ls.size();i++ )
        {
            boolean isLast=false;
            if(lat_position>ls.size()||lat_position==ls.size())
            {
                isLast=true;
                lat_position=ls.size();
            }
            lsParts.add( new ArrayList<T>(ls.subList(start_pos,lat_position)));
            if(isLast)
            {
                break;
            }else
            {
                start_pos=lat_position;
                lat_position=lat_position+part_size;
            }
        }
        return lsParts;
    }

    public String extractIDFromKey(String productID)
    {
        for(SubsPlan plan:subsPlanList)
        {
            if(plan.getActualId().equals(productID))
            {
                return plan.getId();
            }
        }
        return "";
    }

    public void parseSubscription(String response) {
        try{
            SubscriptionResponse subsResponse = utility.getGson().fromJson(response,SubscriptionResponse.class);
            if(subsResponse != null){
                Subscription subscription = subsResponse.getData().getSubscription().get(0);
                if(subscription != null){
                    String jsonSubs = utility.getGson().toJson(subscription,Subscription.class);
                    dataSource.setSubscription(jsonSubs);
                }
            }
        }catch (Exception e){}
    }

    /**
     * @param mobile_number : mobile number
     * @return parameters
     */
    public Map<String, Object> verifyParams(String mobile_number)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.VerifyMobileKey.PHONE_NUMBER, mobile_number);
        return map;
    }

    /**
     * @param mobile_number : mobile_number
     * @return parameters
     */
    public Map<String, Object> requestOtpParams(String mobile_number, Boolean isPhoneUpdate)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.RequestOtpKey.PHONE_NUMBER, mobile_number);
        if(isPhoneUpdate)
            map.put(ApiConfig.RequestOtpKey.TYPE, ApiConfig.VerificationType.PHONE_UPDATE);
        else
            map.put(ApiConfig.RequestOtpKey.TYPE, ApiConfig.VerificationType.NEW_REGISTRATION_OR_LOGIN);
        map.put(ApiConfig.RequestOtpKey.DEVICE_ID, Build.ID);
        return map;
    }

    /**
     * <p>
     *     Locally store the updated mobile number.
     * </p>
     * @param currentCountryCode countryCode ex: +91
     * @param currentPhone   currentPhone ex: 9999999999
     */
    public void updatePhoneNumber(String currentCountryCode, String currentPhone) {
        dataSource.setCountryCode(currentCountryCode);
        dataSource.setMobileNumber(currentPhone);
    }

    /**
     * <p>
     *     Return the Map body object for mobile update api.
     * </p>
     * @param countryCode
     * @param mobile_number
     * @param otp
     * @return  return Map object.
     */
    public Map<String, Object> getVerifyOptForChangeNumData(String countryCode, String mobile_number, String otp) {
        Integer otp_data=Integer.parseInt(otp);
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.VerifyOtpKey.COUNTRY_CODE, countryCode);
        map.put(ApiConfig.VerifyOtpKey.PHONE_NUMBER, mobile_number);
        map.put(ApiConfig.VerifyOtpKey.OTP,otp_data);
        map.put(ApiConfig.VerifyOtpKey.DEVICE_ID,deviceUuidFactory.getDeviceUuid());
        map.put(ApiConfig.VerifyOtpKey.TYPE,ApiConfig.VerificationType.PHONE_UPDATE);
        return map;
    }

    public ArrayList<MomentsData> getMomentList() {
        return momentsDataList;
    }


    public List<List<MomentsData>> getCustomMomentList() {
        return chopIntoParts(getMomentList(),6);
    }

    public void updateMomentData(ArrayList<MomentsData> momentsData) {
        momentsDataList.clear();
        momentsDataList.addAll(momentsData);
    }

    public ArrayList<ImageData> getInstaMedia() {
        return instaMedia;
    }

    public int getExactPosition(int page, int position) {
        int finalIndex = (page * 6) + position;
        if(finalIndex < 0 || finalIndex > getInstaMedia().size()){
            finalIndex = 0;
        }
        return finalIndex;
    }
}
