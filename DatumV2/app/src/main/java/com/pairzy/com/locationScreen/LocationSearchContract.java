package com.pairzy.com.locationScreen;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;

/**
 * <h>GoogleLocSearchContract interface.</h>
 * @author 3Embed.
 * @since 9/5/18.
 * @version 1.0.
 */

public interface LocationSearchContract {

    interface View{

        void showMessage(String msg);
        void showMessage(int msgId);
        void showError(String errorMsg);
        void applyFont();
        void setData(Intent data);
        void showLoading();
        void showNetworkError(String errorMsg);
        void emptyData();
        void showData();
    }

    interface Presenter{
        void init();
        void loadPlaces();
        void handleOnScroll(RecyclerView recyclerView, int dx, int dy);
        void searchPlace(String newText);
        void dispose();
        void parseLocation(int requestCode, int resultCode, Intent intent);
    }
}
