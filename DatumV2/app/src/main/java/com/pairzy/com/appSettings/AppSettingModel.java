package com.pairzy.com.appSettings;


import android.util.Log;

import com.pairzy.com.BaseModel;
import com.pairzy.com.boostDetail.model.SubsPlan;
import com.pairzy.com.boostDetail.model.SubsPlanResponse;
import com.pairzy.com.data.model.Subscription;
import com.pairzy.com.data.model.SubscriptionResponse;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.Utility;
import com.suresh.innapp_purches.SkuDetails;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

class AppSettingModel extends BaseModel {

    @Inject
    Utility utility;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    ArrayList<SubsPlan> subsPlanList;

    @Inject
    public AppSettingModel() {
    }

    public void parseSubsPlanList(String response)
    {
        subsPlanList.clear();
        SubsPlanResponse subsPlanResponse = utility.getGson().fromJson(response,SubsPlanResponse.class);
        ArrayList<SubsPlan> subsPlans = subsPlanResponse.getData();
        subsPlanList.addAll(subsPlans);
    }

    public boolean isSubsPlanEmpty()
    {
        return  subsPlanList.isEmpty();
    }
    public void selectMiddleItem()
    {
        int mid_pos = getSubsPlanListSize() / 2;
        for(int count=0;count<subsPlanList.size();count++)
        {
            SubsPlan subsPlan = subsPlanList.get(count);
            if(mid_pos==count)
            {
                subsPlan.setSelected(true);
                subsPlan.setHeaderTag("MOST POPULAR");
            }else
            {
                subsPlan.setSelected(false);
            }
        }
    }


    public List<String> collectsIds()
    {
        List<String> items=new ArrayList<>();
        for(SubsPlan item :subsPlanList)
        {
            items.add(item.getActualIdForAndroid());
        }
        return items;
    }


    public void updateDetailsData(List<SkuDetails> details)
    {
        for(SkuDetails item:details)
        {
            Log.d("8ry", ":"+item.toString());
            String id=item.getProductId();
            for(SubsPlan product :subsPlanList)
            {
                Log.d("8ry", ":"+product.toString());
                if(id.equals(product.getActualIdForAndroid()))
                {
                    product.setPrice_text(item.getPriceText());
                    product.setCurrencySymbol(item.getCurrency());
                }
            }
        }
    }

    public int getSubsPlanListSize()
    {
        return subsPlanList.size();
    }

    public ArrayList<SubsPlan> getSubsPlanList()
    {
        return subsPlanList;
    }

    public void clearProductList()
    {
        subsPlanList.clear();
    }
    public String getSubsPurchaseId(int position)
    {
        try {
            if (position<getSubsPlanListSize())
            {
                return subsPlanList.get(position).getActualIdForAndroid();
            }
        }catch (Exception ignored){}
        return "";

    }

    public String extractIDFromKey(String productID)
    {
        for(SubsPlan plan:subsPlanList)
        {
            if(plan.getActualId().equals(productID))
            {
                return plan.getId();
            }
        }
        return "";
    }
    public void parseSubscription(String response)
    {
        try{
            SubscriptionResponse subsResponse = utility.getGson().fromJson(response,SubscriptionResponse.class);
            if(subsResponse != null)
            {
                Subscription subscription = subsResponse.getData().getSubscription().get(0);
                if(subscription != null){
                    String jsonSubs = utility.getGson().toJson(subscription,Subscription.class);
                    Log.d("23ert", ":"+jsonSubs);
                    dataSource.setSubscription(jsonSubs);
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
