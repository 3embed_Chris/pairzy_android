package com.pairzy.com.selectLanguage.model;

public class LanguageItem {
    private String languageName;
    private String languageCode;
    private boolean isSelected;

    public LanguageItem(String languageName, String languageCode, boolean isSelected) {
        this.languageName = languageName;
        this.languageCode = languageCode;
        this.isSelected = isSelected;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
