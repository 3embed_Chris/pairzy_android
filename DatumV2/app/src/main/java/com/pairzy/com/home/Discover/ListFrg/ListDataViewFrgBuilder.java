package com.pairzy.com.home.Discover.ListFrg;

import android.app.Activity;
import androidx.recyclerview.widget.GridLayoutManager;
import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.home.Discover.DiscoveryFragUtil;
import com.pairzy.com.home.Discover.Model.UserItemPojo;
import com.pairzy.com.home.HomeModel.HomeAnimation;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import java.util.ArrayList;
import javax.inject.Named;
import dagger.Module;
import dagger.Provides;
/**
 * <h2>ListDataViewFrgBuilder</h2>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class ListDataViewFrgBuilder
{
    @Provides
    @FragmentScoped
    ListDataAdapter getListAdapter(Activity activity,TypeFaceManager typeFaceManager,Utility utility,
                                   @Named(DiscoveryFragUtil.USER_LIST) ArrayList<UserItemPojo> data,
                                   HomeAnimation homeAnimation
                                   )
    {
        return new ListDataAdapter(typeFaceManager,utility,data,homeAnimation);
    }

    @Provides
    @FragmentScoped
    GridLayoutManager getGridLayoutManager(Activity activity)
    {
        return new GridLayoutManager(activity,2);
    }

}
