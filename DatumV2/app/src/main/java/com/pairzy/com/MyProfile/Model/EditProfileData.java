package com.pairzy.com.MyProfile.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by ankit on 3/5/18.
 */

public class EditProfileData {
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("email")
    @Expose
    private String emailId;
    @SerializedName("dob")
    @Expose
    private double dob;
    //instaGramProfileId
    //instaGramToken
    //education
    //work
    //bioData
    @SerializedName("profilePic")
    @Expose
    private String profilePic;

    @SerializedName("profileVideo")
    @Expose
    private String profileVideo;

    @SerializedName("profileVideoThumbnail")
    @Expose
    private String profileVideoThumbnail;

    @SerializedName("otherImages")
    @Expose
    private String otherImages;

    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    //need to add
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("dontShowMyAge")
    @Expose
    private Integer  dontShowMyAge;

    @SerializedName("dontShowMyDist")
    @Expose
    private Integer dontShowMyDist;

    @SerializedName("myPreferences")
    @Expose
    private String myPreferences;

    @SerializedName("type")
    @Expose
    private int type;

    public EditProfileData(){
    }
    public EditProfileData(ProfileData data) {
        if(data.getFirstName() != null)
            firstName = data.getFirstName();
        if(data.getGender() != null)
            gender = data.getGender();
        height = data.getHeight();
        if(data.getEmailId() != null)
            emailId = data.getEmailId();
        dob = data.getDateOfBirth() != null?Double.parseDouble(data.getDateOfBirth()):0;
        if(data.getProfilePic() != null)
            profilePic = data.getProfilePic();
        if(data.getProfileVideo() != null)
            profileVideo = data.getProfileVideo();
        if(data.getProfileVideoThumbnail() != null)
            profileVideoThumbnail = data.getProfileVideoThumbnail();
        if(data.getOtherImages() !=  null && !data.getOtherImages().isEmpty() )
            otherImages = new JSONArray(data.getOtherImages()).toString();
        else
            otherImages = new JSONArray(new ArrayList()).toString();
        longitude = 0.0;
        latitude = 0.0;
        if(data.getAbout() != null)
            about = data.getAbout();
        if(data.getAgeResponse() != null)
            dontShowMyAge = data.getAgeResponse().getIsHidden();
        if(data.getDistResponse() != null)
            dontShowMyDist = data.getDistResponse().getIsHidden();

    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public void setDob(double dob) {
        this.dob = dob;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public void setProfileVideo(String profileVideo) {
        this.profileVideo = profileVideo;
    }

    public void setProfileVideoThumbnail(String profileVideoThumbnail) {
        this.profileVideoThumbnail = profileVideoThumbnail;
    }

    public void setOtherImages(String otherImages) {
        this.otherImages = otherImages;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public void setMyPreferences(String myPreferences) {
        this.myPreferences = myPreferences;
    }

    public void setDontShowMyAge(Integer dontShowMyAge) {
        this.dontShowMyAge = dontShowMyAge;
    }

    public void setDontShowMyDist(Integer dontShowMyDist) {
        this.dontShowMyDist = dontShowMyDist;
    }

    public void setType(int type) {
        this.type = type;
    }
}
