package com.pairzy.com.home.Discover.GridFrg;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.pairzy.com.R;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.home.Discover.GridFrg.Model.DeckCardItemClicked;
import com.pairzy.com.home.Discover.GridFrg.Model.GridModel;
import com.pairzy.com.home.Discover.Model.UserItemPojo;
import com.pairzy.com.home.HomeModel.LoadMoreStatus;
import com.pairzy.com.home.HomeUtil;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.SpendCoinDialog.CoinSpendDialogCallback;
import com.pairzy.com.util.SpendCoinDialog.WalletEmptyDialogCallback;

import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * <h2>GridDataViewPresenter</h2>
 * <P>
 *
 * </P>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class GridDataViewPresenter implements GridDataViewContract.Presenter,DeckCardItemClicked,WalletEmptyDialogCallback,CoinSpendDialogCallback
{
    private GridDataViewContract.View view;

    @Named(HomeUtil.LOAD_MORE_STATUS)
    @Inject
    LoadMoreStatus loadMoreStatus;
    @Inject
    Activity activity;
    @Inject
    GridModel model;
    @Inject
    CoinDialog coinDialog;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    private boolean superLikeBySwipe;
    private UserItemPojo userItemPojo;

    @Inject
    GridDataViewPresenter()
    {}

    @Override
    public void takeView(GridDataViewContract.View view) {
        this.view=view;
    }

    @Override
    public void dropView()
    {
        view=null;
    }

    @Override
    public void initListener()
    {
        if(view!=null)
            view.adapterClickListener(this);
    }


    @Override
    public void loadMore()
    {
        if(view!=null&&!loadMoreStatus.isNo_more_data())
            view.doLoadMore();
    }

    @Override
    public void noData()
    {
        if(view!=null)
            view.showLoadingView();
    }


    @Override
    public void updateDataChanged()
    {
        if(view!=null)
            view.notifyDataChanged();
    }

    @Override
    public void showBoostDialog()
    {
        if(view!=null)
            view.openBoostDialog();
    }

    @Override
    public void openUserProfile(String data,View data_view)
    {
        if(view!=null)
            view.openUerProfile(data,data_view);
    }

    @Override
    public void openChatScreen(UserItemPojo userItemPojo, View view) {
        if(this.view != null)
            this.view.openChat(userItemPojo);
    }



    @Override
    public boolean isNoMoreData() {
        return loadMoreStatus.isNo_more_data();
    }

    @Override
    public void revertAction(UserItemPojo item)
    {
        if(view!=null)
            view.onLikeEventError(item);
    }

    /*
     *On Handel result */
    @Override
    public boolean onHandelActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode== AppConfig.PROFILE_REQUEST&&resultCode== Activity.RESULT_OK)
        {
            Bundle result_data=data.getExtras();
            assert result_data != null;
            int result_action=result_data.getInt(AppConfig.RESULT_DATA);
            if(result_action==AppConfig.ON_SUPER_LIKE)
            {
                if(view != null)
                    view.doSuperLike();
            }else if(result_action==AppConfig.ON_LIKE)
            {
                if(view!=null)
                    view.doLike();
            }else if(result_action==AppConfig.ON_DISLIKE)
            {
                if(view!=null)
                    view.doDislike();
            }
            else if(result_action==AppConfig.ON_CHAT)
            {
                if(view!=null)
                    view.setChatListNeedToUpdate(true);
            }
            return true;
        }else
        {
            return false;
        }
    }

    @Override
    public void initiateSuperlike() {
        if(view!=null)
            view.doSuperLike();
    }

    @Override
    public void showBoostViewCounter(boolean show, int viewCount) {
        if(view != null) {
            String boostText = String.format(Locale.ENGLISH,"%d %s",viewCount,activity.getString(R.string.views));
            view.showBoostViewCounter(show, boostText);
        }
    }

    @Override
    public void startCoinAnimation() {
        if(view != null)
            view.startCoinAnimation();
    }

    private void launchWalletEmptyDialogForSuperlike(){
        coinDialog.showWalletEmptyDialog(activity.getString(R.string.wallet_empty_title),activity.getString(R.string.empty_wallet_superlike_msg),this);
    }

    private void launchSpendCoinDialogForSuperlike(UserItemPojo userItemPojo) {
        Integer coinSpend = coinConfigWrapper.getCoinData().getSuperLike().getCoin();
        boolean isFemale = userItemPojo.getGender().equalsIgnoreCase("female");
        String title = String.format(Locale.ENGLISH,"Spend %s coins to let %s know you really like %s by super liking.",
                coinSpend,userItemPojo.getFirstName(),isFemale?"her":"him");
        String btnText = String.format(Locale.ENGLISH, " I am OK to spend %s coins", coinSpend);
        coinDialog.showCoinSpendDialog(title,activity.getString(R.string.superlike_spend_coin_title), btnText,this);
    }

    @Override
    public void checkWalletForSuperlike(UserItemPojo userItemPojo) {
        if(model.isEnoughWalletBalanceToSuperLike()) {
            if(model.isSuperlikeSpendDialogNeedToShow()){
                launchSpendCoinDialogForSuperlike(userItemPojo);
            }
            else {
                initiateSuperlike();
            }
        }
        else{
            //show wallet empty dialog for superlike.
            launchWalletEmptyDialogForSuperlike();
        }
    }

    @Override
    public void checkWalletForSuperlikeBySwipe(UserItemPojo userItemPojo) {
        superLikeBySwipe = true;
        userItemPojo = userItemPojo;
        if(model.isEnoughWalletBalanceToSuperLike()) {
            if(model.isSuperlikeSpendDialogNeedToShow()){
                launchSpendCoinDialogForSuperlike(userItemPojo);
                revertAction(userItemPojo);
            }
            else {
                superLikeBySwipe = false;
                if(view != null)
                    view.callSuperLikeApi(userItemPojo);
            }
        }
        else{
            superLikeBySwipe = false;
            //show wallet empty dialog for superlike.
            launchWalletEmptyDialogForSuperlike();
            revertAction(userItemPojo);
        }
    }

    @Override
    public void startVideoPlayFirstItem() {
        if(view != null)
            view.notifyFirstItem();
    }


    /*
     * coin Dialog callback
     */
    @Override
    public void onByMoreCoin() {
        if(view != null)
            view.launchCoinWallet();
    }

    /*
     * coin Dialog callback
     */
    @Override
    public void onOkToSpendCoin(boolean dontShowAgain) {
        if(superLikeBySwipe){
            if(view != null)
                view.callSuperLikeApi(userItemPojo);
        }
        else {
            initiateSuperlike();
        }
    }
}
