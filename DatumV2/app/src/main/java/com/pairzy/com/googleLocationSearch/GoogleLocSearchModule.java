package com.pairzy.com.googleLocationSearch;
import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h>LocationSearchModule interface</h>
 * @author 3Embed.
 * @since 22/05/2018.
 * @version 1.0.
 */

@Module
public interface GoogleLocSearchModule {

    @ActivityScoped
    @Binds
    GoogleLocSearchContract.Presenter bindsSearchPresenter(GoogleLocSearchPresenter presenter);

    @ActivityScoped
    @Binds
    GoogleLocSearchContract.View bindsSearchView(GoogleLocSearchActivity activity);

    @ActivityScoped
    @Binds
    Activity provideActivity(GoogleLocSearchActivity activity);
}
