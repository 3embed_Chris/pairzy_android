package com.pairzy.com.selectLanguage.model;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.model.DateEvent;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.CalendarEventHelper;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by ankit on 21/5/18.
 */

public class SelectLanguageModel extends BaseModel {

    @Inject
    Context context;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Utility utility;
    @Inject
    CalendarEventHelper calendarEventHelper;
    @Inject
    ArrayList<LanguageItem> languageItems;

    @Inject
    SelectLanguageModel(){
    }

    public String getCurrentVersion() {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void deleteCalenderEvent() {
        ArrayList<DateEvent> dateEvents = getEventId();
        for(DateEvent dateEvent: dateEvents) {
            calendarEventHelper.deleteEvent(dateEvent.getEventId());
        }
    }

    private ArrayList<DateEvent> getEventId(){
        ArrayList<String> jsonEventList = dataSource.getEvents();
        ArrayList<DateEvent> dateEvents = new ArrayList<>();
        try {
            for (String json : jsonEventList) {
                DateEvent dateEvent = utility.getGson().fromJson(json, DateEvent.class);
                if (dateEvent != null)
                    dateEvents.add(dateEvent);
            }
        }catch (Exception e){}
        return dateEvents;
    }

    public LanguageItem parseLanguageItem(int position) {
        try {
            return languageItems.get(position);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public void selectCurrentAppLanguage() {
        for(LanguageItem languageItem:languageItems){
            if(languageItem.getLanguageCode().equals(dataSource.getSelectedLanguageIso())){
                languageItem.setSelected(true);
            }
            else{
                languageItem.setSelected(false);
            }
        }
    }

    public String selectAppLanguage(int position) {
        for(LanguageItem languageItem: languageItems){
            languageItem.setSelected(false);
        }
        try {
            languageItems.get(position).setSelected(true);
            return languageItems.get(position).getLanguageCode();
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }
}
