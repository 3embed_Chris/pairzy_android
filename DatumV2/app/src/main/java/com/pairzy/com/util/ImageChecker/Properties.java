package com.pairzy.com.util.ImageChecker;

/**
 * <h2>Properties</h2>
 * <P>
 *
 * </P>
 * @since  4/3/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public enum Properties
{
    VIOLENT("wad"),
    CELEBRITIES("celebrities"),
    NUDITY("nudity"),
    FACE_DETECTION("faces");
    String value;
    Properties(String data)
    {
        this.value=data;
    }
}
