package com.pairzy.com.register.Gender;

import javax.inject.Inject;

/**
 * <h2>GenderPresenter</h2>
 * <P>
 *
 * </P>
 * @since  2/15/2018.
 * @version 1.0.
 * @author 3Embed.
 */
public class GenderPresenter implements GenderContract.Presenter
{
    @Inject
    GenderPresenter(){}
    private GenderContract.View view;
    @Override
    public void handleSelection(int position, boolean isSelected)
    {

    }

    @Override
    public void takeView(GenderContract.View view) {
        this.view= view;
    }

    @Override
    public void dropView() {
      view=null;
    }
}
