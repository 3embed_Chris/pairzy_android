package com.pairzy.com.util.CardDeckView;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.PointF;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.OvershootInterpolator;
/**
 * and project Swipe cards.
 * Use with caution dinausaurs might appear!
 */
public class FlingCardListener implements View.OnTouchListener
{
    private static final int INVALID_POINTER_ID = -1;
    private final float objectX;
    private final float objectY;
    private final int objectH;
    private final int objectW;
    private final int parentWidth;
    private final FlingListener mFlingListener;
    private final Object dataObject;
    private final float halfWidth;
    private final int TOUCH_ABOVE = 0;
    private final int TOUCH_BELOW = 1;
    private float BASE_ROTATION_DEGREES;
    private float aPosX;
    private float aPosY;
    private float aDownTouchX;
    private float aDownTouchY;
    private int mActivePointerId = INVALID_POINTER_ID;
    private View frame = null;
    private int touchPosition;
    private boolean isAnimationRunning = false;
    private float MAX_COS = (float) Math.cos(Math.toRadians(45));
    private ProgressParameter progressParameter=new ProgressParameter();


    public FlingCardListener(View frame, Object itemAtPosition, FlingListener flingListener)
    {
        this(frame, itemAtPosition, 15f, flingListener);
    }

    public FlingCardListener(View frame, Object itemAtPosition, float rotation_degrees, FlingListener flingListener)
    {
        super();

        this.frame = frame;
        this.objectX = frame.getX();
        this.objectY = frame.getY();
        this.objectH = frame.getHeight();
        this.objectW = frame.getWidth();
        this.halfWidth = objectW / 2f;
        this.dataObject = itemAtPosition;
        this.parentWidth = ((ViewGroup) frame.getParent()).getWidth();
        this.BASE_ROTATION_DEGREES = rotation_degrees;
        this.mFlingListener = flingListener;

    }


    public boolean onTouch(View view, MotionEvent event)
    {
        switch (event.getAction() & MotionEvent.ACTION_MASK)
        {
            case MotionEvent.ACTION_DOWN:
                mActivePointerId = event.getPointerId(0);
                float x = 0;
                float y = 0;
                boolean success = false;
                try
                {
                    x = event.getX(mActivePointerId);
                    y = event.getY(mActivePointerId);
                    success = true;
                } catch (Exception e)
                {}

                if (success)
                {
                    // Remember where we started
                    aDownTouchX = x;
                    aDownTouchY = y;
                    //to prevent an initial jump of the magnifier, aposX and aPosY must
                    //have the values from the magnifier frame
                    if (aPosX == 0) {
                        aPosX = frame.getX();
                    }
                    if (aPosY == 0) {
                        aPosY = frame.getY();
                    }

                    if (y < objectH / 2) {
                        touchPosition = TOUCH_ABOVE;
                    } else {
                        touchPosition = TOUCH_BELOW;
                    }
                }

                view.getParent().requestDisallowInterceptTouchEvent(true);
                break;

            case MotionEvent.ACTION_UP:
                mActivePointerId = INVALID_POINTER_ID;
                resetCardViewOnStack(view);
                view.getParent().requestDisallowInterceptTouchEvent(false);
                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                break;

            case MotionEvent.ACTION_POINTER_UP:
                // Extract the index of the pointer that left the touch sensor
                final int pointerIndex = (event.getAction() &
                        MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                final int pointerId = event.getPointerId(pointerIndex);
                if (pointerId == mActivePointerId) {
                    // This was our active pointer going up. Choose a new
                    // active pointer and adjust accordingly.
                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    mActivePointerId = event.getPointerId(newPointerIndex);
                }
                break;
            case MotionEvent.ACTION_MOVE:

                // Find the index of the active pointer and fetch its position
                final int pointerIndexMove = event.findPointerIndex(mActivePointerId);
                final float xMove = event.getX(pointerIndexMove);
                final float yMove = event.getY(pointerIndexMove);

                //from http://android-developers.blogspot.com/2010/06/making-sense-of-multitouch.html
                // Calculate the distance moved
                final float dx = xMove - aDownTouchX;
                final float dy = yMove - aDownTouchY;

                // Move the frame
                aPosX += dx;
                aPosY += dy;

                // calculate the rotation degrees
                float distobjectX = aPosX - objectX;
                float rotation = BASE_ROTATION_DEGREES * 2.f * distobjectX / parentWidth;
                if (touchPosition == TOUCH_BELOW) {
                    rotation = -rotation;
                }

                //in this area would be code for doing something with the view as the frame moves.
                frame.setX(aPosX);
                frame.setY(aPosY);
                frame.setRotation(rotation);
                ProgressParameter progressParameter=getScrollProgressPercent();
                mFlingListener.onScroll(progressParameter.scrollProgressPercent,progressParameter.isHorizontal,progressParameter.scrollHorizontalProgressPercent);
                break;

            case MotionEvent.ACTION_CANCEL: {
                mActivePointerId = INVALID_POINTER_ID;
                view.getParent().requestDisallowInterceptTouchEvent(false);
                break;
            }
        }

        return true;
    }


    private ProgressParameter getScrollProgressPercent()
    {
        if (movedBeyondLeftBorder())
        {
            float zeroToOneValue = -(aPosX + halfWidth - rightBorder()) / (leftBorder() - rightBorder());
            progressParameter.setScrollProgressPercent(zeroToOneValue);
            progressParameter.setIsHorizontal(0);
            return progressParameter;
        } else if (movedBeyondRightBorder())
        {
            float zeroToOneValue = (aPosX + halfWidth - leftBorder()) / (rightBorder() - leftBorder());
            progressParameter.setScrollProgressPercent(zeroToOneValue);
            progressParameter.setIsHorizontal(0);
            return progressParameter;
        } else if (movedUpperBorder())
        {
            float zeroToOneValue = (aPosY + halfWidth - leftBorder()) / (rightBorder() - leftBorder());
            progressParameter.setScrollHorizontalProgressPercent(zeroToOneValue * 2f - 1f);
            progressParameter.setIsHorizontal(1);
            return progressParameter;
        } else
        {
            float zeroToOneValue = (aPosX + halfWidth - leftBorder()) / (rightBorder() - leftBorder());
            progressParameter.setScrollProgressPercent(zeroToOneValue * 2f - 1f);
            progressParameter.setIsHorizontal(0);
            return progressParameter;
        }
    }


    private boolean resetCardViewOnStack(View view)
    {
        if (movedBeyondLeftBorder()) {
            // Left Swipe
            onSelected(1, getExitPoint(-objectW), 100);
            mFlingListener.onScroll(-1.0f,0,0);
        } else if (movedBeyondRightBorder()) {
            // Right Swipe
            onSelected(2, getExitPoint(parentWidth), 100);
            mFlingListener.onScroll(1.0f,0,0);
        }
        else if (movedUpperBorder())
        {
            if (aPosY < -400)
            {
                onSelected(3, 0, 100);
                mFlingListener.onScroll(0,1,3f);
            }
            else
            {
                float abslMoveDistance = Math.abs(aPosX - objectX);
                aPosX = 0;
                aPosY = 0;
                aDownTouchX = 0;
                aDownTouchY = 0;
                frame.animate()
                        .setDuration(100)
                        .setInterpolator(new OvershootInterpolator(1.5f))
                        .x(objectX)
                        .y(objectY)
                        .rotation(0);
                mFlingListener.onScroll(0,1,0.0f);
                if (abslMoveDistance < 4.0)
                {
                    mFlingListener.onClick(dataObject);
                }

            }
        }
        else {
            float abslMoveDistance = Math.abs(aPosX - objectX);
            aPosX = 0;
            aPosY = 0;
            aDownTouchX = 0;
            aDownTouchY = 0;
            frame.animate()
                    .setDuration(100)
                    .setInterpolator(new OvershootInterpolator(1.5f))
                    .x(objectX)
                    .y(objectY)
                    .rotation(0);
            mFlingListener.onScroll(0.0f,0,0);
            if (abslMoveDistance < 4.0)
            {
                mFlingListener.onClick(dataObject);
            }
        }
        return false;
    }


    private boolean movedBeyondLeftBorder() {
        return aPosX + halfWidth < leftBorder();
    }

    private boolean movedBeyondRightBorder()
    {
        return aPosX + halfWidth > rightBorder();
    }

    private boolean movedUpperBorder()
    {
        float posX=0;
        float returnvalue= upperBorder();
        return posX + halfWidth > returnvalue;
    }



    public float leftBorder() {
        return parentWidth / 4.f;
    }

    public float rightBorder() {
        return 3 * parentWidth / 4.f;
    }

    public float upperBorder()
    {
        return -25000;
    }

    public void onSelected(final int isLeft, float exitY, long duration)
    {

        isAnimationRunning = true;
        float exitX;
        if (isLeft == 1) {
            exitX = -objectW - getRotationWidthOffset();
        } else if (isLeft == 2){
            exitX = parentWidth + getRotationWidthOffset();
        }
        else
        {
            exitX = 0;
            exitY = -30000;
        }
        this.frame.animate()
                .setDuration(duration)
                .setInterpolator(new AccelerateInterpolator())
                .x(exitX)
                .y(exitY)
                .setListener(new AnimatorListenerAdapter()
                {
                    @Override
                    public void onAnimationEnd(Animator animation)
                    {
                        if (isLeft == 1)
                        {
                            mFlingListener.onCardExited();
                            mFlingListener.leftExit(dataObject);
                        } else if (isLeft == 2)
                        {
                            mFlingListener.onCardExited();
                            mFlingListener.rightExit(dataObject);
                        } else if (isLeft == 3)
                        {
                            mFlingListener.onCardExited();
                            mFlingListener.upperExit(dataObject);
                        }
                        else
                        {
                            mFlingListener.onScroll(0f,0,0);
                        }
                        isAnimationRunning = false;
                    }
                })
                .rotation(getExitRotation(false));
    }

    /**
     * Starts a default left exit animation.
     */
    public void selectLeft() {
        if (!isAnimationRunning)
            onSelected(1, objectY, 400);
    }

    /**
     * Starts a default right exit animation.
     */
    public void selectRight() {
        if (!isAnimationRunning) {
            onSelected(2, objectY, 400);
        }
    }

    /**
     * Starts a default Upper exit animation.
     */
    public void selectUpper() {
        if (!isAnimationRunning) {
            onSelected(3, objectY, 1500);
        }
    }

    private float getExitPoint(int exitXPoint) {
        float[] x = new float[2];
        x[0] = objectX;
        x[1] = aPosX;

        float[] y = new float[2];
        y[0] = objectY;
        y[1] = aPosY;

        LinearRegression regression = new LinearRegression(x, y);

        //Your typical y = ax+b linear regression
        return (float) regression.slope() * exitXPoint + (float) regression.intercept();
    }

    private float getExitRotation(boolean isLeft) {
        float rotation = BASE_ROTATION_DEGREES * 2.f * (parentWidth - objectX) / parentWidth;
        if (touchPosition == TOUCH_BELOW)
        {
            rotation = -rotation;
        }
        if (isLeft)
        {
            rotation = -rotation;
        }
        return rotation;
    }



    /**
     * When the object rotates it's width becomes bigger.
     * The maximum width is at 45 degrees.
     * <p/>
     * The below method calculates the width offset of the rotation.
     */
    private float getRotationWidthOffset() {
        return objectW / MAX_COS - objectW;
    }


    public void setRotationDegrees(float degrees) {
        this.BASE_ROTATION_DEGREES = degrees;
    }

    public boolean isTouching() {
        return this.mActivePointerId != INVALID_POINTER_ID;
    }

    public PointF getLastPoint() {
        return new PointF(this.aPosX, this.aPosY);
    }


    protected interface FlingListener
    {
        void onCardExited();

        void leftExit(Object dataObject);

        void rightExit(Object dataObject);

        void upperExit(Object dataObject);

        void onClick(Object dataObject);

        void onScroll(float scrollProgressPercent, int isHorizontal, float scrollHorizontalProgressPercent);

    }
}





