package com.pairzy.com.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 7/9/18.
 */

public class BoostCountMqttResponse {
    @SerializedName("boost")
    @Expose
    private BoostCountData boost;

    public BoostCountData getBoost() {
        return boost;
    }

    public void setBoost(BoostCountData boost) {
        this.boost = boost;
    }
}
