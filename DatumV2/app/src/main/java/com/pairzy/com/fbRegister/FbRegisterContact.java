package com.pairzy.com.fbRegister;


import android.content.Intent;
import android.os.Bundle;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.facebookmanager.com.FacebookUserDetails;

import dagger.android.support.DaggerFragment;

public interface FbRegisterContact
{
    interface View extends BaseView
    {
        void openPreferencePage();
        void updateProgress(int progress);
        void  showError(String message);
        void showMessage(String message);
        void openFragment(DaggerFragment fragment, boolean keepBack);
        void updateInterNetStatus(boolean status);
        void launchAppropriateFrag(int currentPos, boolean keepBacktrack);
        FacebookUserDetails getFbDetails();
        void facebookLoginSuccess();
        void facebookSignUpSuccess();
    }


    interface Presenter extends BasePresenter
    {
        String EMAIL_DATA="email_data";
        String NAME_DATA="name_data";
        String DOB_DATA="dob_data";
        String GENDER_DATA="gender_data";
        String PIC_DATA="pic_data";
        String VIDEO_DATA="video_data";
        String VIDEO_THUMB_DATA="video_thumb_data";
        String LAT="latitude";
        String LNG="longitude";

        void updateProgress(int progress);

        void showError(String message);

        void showMessage(String message);

        void launchFragment(DaggerFragment daggerFragment, boolean keepBack);

        void updateProfile(Bundle data);

        void getUserLocation();

        void getUserLocationApi();

        void askForLocationPermission();

        boolean onHandelActivityResult(int requestCode, int resultCode, Intent data);

        void initNetworkObserver();

        void launchNextValidFrag(int currentPos, boolean backtrack);

        void facebookApiCall(FacebookUserDetails userDetails);
    }
}
