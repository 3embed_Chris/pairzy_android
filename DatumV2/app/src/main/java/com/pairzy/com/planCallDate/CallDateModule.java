package com.pairzy.com.planCallDate;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h>CallDateModule class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

@Module
public abstract class CallDateModule {

    @ActivityScoped
    @Binds
    abstract CallDateContract.Presenter callDatePresenter(CallDatePresenter presenter);

    @ActivityScoped
    @Binds
    abstract CallDateContract.View callDateView(CallDateActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity callDateActivity(CallDateActivity activity);

}
