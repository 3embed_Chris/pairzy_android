package com.pairzy.com.coinWallet.coinIn;

import com.pairzy.com.coinWallet.coinIn.model.InCoinModel;

import javax.inject.Inject;

/**
 *<h>InCoinPresenter</h>
 * <p></p>
 *@author 3Embed.
 *@since 30/5/18.
 */

public class InCoinPresenter implements InCoinContract.Presenter {

    private InCoinContract.View view;

    @Inject
    InCoinModel model;

    @Inject
    public InCoinPresenter(){
    }

    @Override
    public void takeView(InCoinContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        this.view = null;
    }

    @Override
    public boolean isCoinHistoryEmpty() {
        return model.isCoinHistoryEmpty();
    }
}
