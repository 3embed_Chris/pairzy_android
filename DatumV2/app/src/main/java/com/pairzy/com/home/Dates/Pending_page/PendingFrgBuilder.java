package com.pairzy.com.home.Dates.Pending_page;

import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * <h2>ChatFragModule</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface PendingFrgBuilder
{
    @FragmentScoped
    @Binds
    PendingFrg getPendingFragment(PendingFrg availableFrg);

    @FragmentScoped
    @Binds
    PendingFrgContract.Presenter pendingFragPresenter(PendingFrgPresenter presenter);

}
