package com.pairzy.com.MqttChat.Utilities;

import com.pairzy.com.BuildConfig;

/**
 * Created by moda on 20/06/17.
 */

public class ApiOnServer {
  //datum broker
  public static final String BROKER_URL = "34.209.115.119:1883";

  public static final String CHAT_MULTER_UPLOAD_URL = "http://34.209.115.119:8008/";

  private static final String CHAT_FETCH_SERVER_URL = "http://34.209.115.119:8083/chatDoc/";

  public static final String CHAT_FETCH_PATH = CHAT_FETCH_SERVER_URL;

  public static final String DELETE_DOWNLOAD = CHAT_MULTER_UPLOAD_URL + "deleteImage";

  public static final String TRENDING_STICKERS = "http://api.giphy.com/v1/stickers/trending?api_key=8WLfsTD45ylMmDAkJMxffPN3pCsVLZKa";

  public static final String TRENDING_GIFS = "http://api.giphy.com/v1/gifs/trending?api_key=8WLfsTD45ylMmDAkJMxffPN3pCsVLZKa";

  public static final String GIPHY_APIKEY = "&api_key=8WLfsTD45ylMmDAkJMxffPN3pCsVLZKa";

  public static final String SEARCH_STICKERS = "http://api.giphy.com/v1/stickers/search?q=";

  public static final String SEARCH_GIFS = "http://api.giphy.com/v1/gifs/search?q=";

  /**
   * GET api to fetch the messages into the list
   */
  public static final String FETCH_MESSAGES = BuildConfig.BASEURL + "Messages";

}
