package com.pairzy.com.commentPost.model;

import com.pairzy.com.BaseModel;
import com.pairzy.com.commentPost.CommentPostUtil;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

public class CommentModel extends BaseModel {

    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Utility utility;

    @Named(CommentPostUtil.COMMENT_POST)
    @Inject
    ArrayList<CommentPostDataPojo> commentPostList;

    @Inject
    CommentPostAdapter adapter;
    @Inject
    CommentModel(){}

    public void parseResponse(String data) {
        try
        {
            CommentPostPojo result_data=utility.getGson().fromJson(data,CommentPostPojo.class);

            commentPostList.clear();
            ArrayList<CommentPostDataPojo> dataPojo=result_data.getData();

            commentPostList.addAll(dataPojo);
            adapter.notifyDataSetChanged();


        }catch (Exception e)
        {
            try {
                throw new DataParsingException(e.getMessage());
            } catch (DataParsingException e1) {
                e1.printStackTrace();
            }
        }

    }

    public Map<String, Object> Credentials(String comment, String postId) {
        Map<String, Object> map=new HashMap<>();
        map.put(ApiConfig.COMMENT.COMMENT,comment);
        map.put(ApiConfig.COMMENT.POST_ID,postId);
        return map;
    }

    public CommentPostDataPojo getCommentItem(int position) {
        return commentPostList.get(position);
    }

    public void deletedComment(int position) {
        if(commentPostList!=null)
        {
            commentPostList.remove(position);
        }

    }
}
