package com.pairzy.com.createPost;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class CreatePostModule {

    @ActivityScoped
    @Binds
    abstract CreatePostContract.Presenter providePresenter(CreatePostPresenter createPostPresenter);

    @ActivityScoped
    @Binds
    abstract CreatePostContract.View provideView(CreatePostActivity createPostActivity);

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(CreatePostActivity createPostActivity);

}
