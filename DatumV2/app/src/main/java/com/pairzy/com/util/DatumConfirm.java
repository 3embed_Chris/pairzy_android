package com.pairzy.com.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.login.LoginActivity;

/**
 * Created by DELL on 1/10/2018.
 */

public class DatumConfirm extends Dialog implements View.OnClickListener
{

    private Activity activity;
    private String title;
    private String message;
    private TextView tvTitle;
    private TextView tvMessage;
    private Button btnYes;
    private Button btnNo;

    public DatumConfirm(@NonNull Activity activity, String title, String message) {
        super(activity);
        this.activity = activity;
        this.title = title;
        this.message = message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_confirm);

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvMessage = (TextView) findViewById(R.id.tvMessage);
        btnYes = (Button) findViewById(R.id.btnYes);
        btnNo = (Button) findViewById(R.id.btnNo);

        btnYes.setOnClickListener(this);
        btnNo.setOnClickListener(this);

        tvTitle.setText(title);
        tvMessage.setText(message);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnYes:
                activity.startActivity(new Intent(activity, LoginActivity.class));
                activity.finish();
                break;
            case R.id.btnNo:
                dismiss();
                break;
        }
    }
}
