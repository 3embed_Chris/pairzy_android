package com.pairzy.com.selectLanguage;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.selectLanguage.model.AppLanguageAdapter;
import com.pairzy.com.splash.SplashActivity;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.appbar.AppBarLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h>Settings activity</h>
 * <p> This Settings activity user settings and Logout and Delete account function.</p>
 *
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public class SelectLanguageActivity extends BaseDaggerActivity implements SelectLanguageContract.View{

    @Inject
    SelectLanguageContract.Presenter presenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Activity activity;
    @Inject
    AppLanguageAdapter appLanguageAdapter;

    @BindView(R.id.page_title)
    TextView tvPageTitle;
    @BindView(R.id.tv_appbar_title)
    TextView tvAppbarTitle;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.rv_language)
    RecyclerView rvLanguage;
    private Unbinder unbinder;
    private LinearLayoutManager linearLayoutManager;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        presenter.init();
        initView();
        presenter.loadAppVersion();
    }

    private void initView() {

        linearLayoutManager = new LinearLayoutManager(activity);
        rvLanguage.setLayoutManager(linearLayoutManager);
        rvLanguage.setHasFixedSize(true);
        rvLanguage.setAdapter(appLanguageAdapter);
        presenter.setAdapterCallback(appLanguageAdapter);
        presenter.selectTheCurrentLanguage();

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if(scrollRange == -1){
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if(scrollRange + verticalOffset == 0){
                    isShow = true;
                    showToolbarTitle(true);
                } else if(isShow){
                    isShow = false;
                    showToolbarTitle(false);
                }
            }
        });
    }

    private void showToolbarTitle(boolean show) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(show){
                    if(tvAppbarTitle != null)
                        tvAppbarTitle.setVisibility(View.VISIBLE);
                }
                else {
                    if(tvAppbarTitle != null)
                        tvAppbarTitle.setVisibility(View.GONE);
                }
            }
        });
    }
    @Override
    public void applyFont() {
        tvPageTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvAppbarTitle.setTypeface(typeFaceManager.getCircularAirBold());
    }

    @Override
    public void showSplashScreen() {
        Intent intent=new Intent(SelectLanguageActivity.this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void notifyAdapter() {
        appLanguageAdapter.notifyDataSetChanged();
    }

    @Override
    public void setNewLanguage(String selectedLanguage) {
        setLanguage(selectedLanguage);
        AppController.getInstance().setCurrentAppLanguage(selectedLanguage);
    }

    @Override
    public void finishActivity() {
        this.finish();
    }


    @OnClick(R.id.close_button)
    public void close(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        /*if(AppController.getInstance().isLanguageChange()) {
            setResult(RESULT_OK);
            AppController.getInstance().setLanguageChange(false);
        }
        else{
            setResult(RESULT_CANCELED);
        }*/
        this.finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(null);
        unbinder.unbind();
        presenter.dispose();
    }
}
