package com.pairzy.com.home.Dates.CustomAlertDialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import androidx.appcompat.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.home.Dates.Model.DateResponseType;
import com.pairzy.com.home.Dates.Model.DateType;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.Locale;

/**
 * <h2>ChatMenuDialog</h2>
 * <P>
 *
 * </P>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class CustomAlertDialog
{
    private Activity activity;
    private Dialog dialog;
    private TypeFaceManager typeFaceManager;
    private Utility utility;
    private CustomAlertDialogCallBack callBack;


    public CustomAlertDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility)
    {
        this.activity=activity;
        this.typeFaceManager=typeFaceManager;
        this.utility=utility;
    }


    public void showDialog(String userName, DateType dateType,DateResponseType dateResponseType, CustomAlertDialogCallBack callBack)
    {

        this.callBack = callBack;
        if(dialog != null && dialog.isShowing())
        {
            dialog.cancel();
        }
        @SuppressLint("InflateParams")
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.custom_alert_dialog, null);
        builder.setView(dialogView);
        TextView tvTitle =dialogView.findViewById(R.id.tv_title);
        tvTitle.setTypeface(typeFaceManager.getCircularAirBold());

        TextView tvMsg =dialogView.findViewById(R.id.tv_msg);
        tvMsg.setTypeface(typeFaceManager.getCircularAirBook());
        //Are u sure want to Confirm DATE_TYPE date with NAME ?
        //Are u sure want to Reschedule DATE_TYPE date with NAME?
        //Are u sure want to Cancel DATE_TYPE date with NAME?
        String responseText = "";

        if(dateResponseType == DateResponseType.ACCEPTED){
            responseText = activity.getString(R.string.confirm_Text);
        }
        else if(dateResponseType == DateResponseType.RESCHEDULE){
            responseText = activity.getString(R.string.reschedule_text);
        }
        else{
            responseText = activity.getString(R.string.cancel_text);
        }

        String dateTypeText = "";
        if(dateType == DateType.IN_PERSON_DATE){
            dateTypeText = activity.getString(R.string.in_person_text);
        }
        else if(dateType == DateType.AUDIO_DATE){
            dateTypeText = "Voice call";
        }
        else{
            dateTypeText = "Video call";
        }

        tvMsg.setText(String.format(Locale.ENGLISH,"Are you sure want to %s %s date with %s ?",responseText,dateTypeText,utility.formatString(userName)));

        Button onCanceled=dialogView.findViewById(R.id.onCanceled);
        onCanceled.setTypeface(typeFaceManager.getCircularAirBook());
        onCanceled.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();
        });
        Button onOk=dialogView.findViewById(R.id.onOk);
        onOk.setTypeface(typeFaceManager.getCircularAirBook());
        onOk.setOnClickListener(view -> {
            if(callBack != null)
                callBack.onOk();
            if(dialog!=null)
                dialog.cancel();
        });
        dialog = builder.create();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
    }

    public void showDialog(DateType dateType)
    {

        //this.callBack = callBack;
        if(dialog != null && dialog.isShowing())
        {
            dialog.cancel();
        }
        @SuppressLint("InflateParams")
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.custom_alert_dialog, null);
        builder.setView(dialogView);
        TextView tvTitle =dialogView.findViewById(R.id.tv_title);
        tvTitle.setTypeface(typeFaceManager.getCircularAirBold());

        TextView tvMsg =dialogView.findViewById(R.id.tv_msg);
        tvMsg.setTypeface(typeFaceManager.getCircularAirBook());

        String dateInfoText = "";
        if(dateType == DateType.IN_PERSON_DATE){
            dateInfoText = activity.getString(R.string.inperson_date_info_text);
        }
        else if(dateType == DateType.AUDIO_DATE){
            dateInfoText = activity.getString(R.string.audio_date_info_text);
        }
        else{
            dateInfoText = activity.getString(R.string.video_date_info_text);
        }

        tvMsg.setText(dateInfoText);
        Button onCanceled=dialogView.findViewById(R.id.onCanceled);
        onCanceled.setTypeface(typeFaceManager.getCircularAirBook());
        onCanceled.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();
        });
        Button onOk=dialogView.findViewById(R.id.onOk);
        onOk.setTypeface(typeFaceManager.getCircularAirBook());
        onOk.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();
        });
        onCanceled.setVisibility(View.GONE);
        onOk.setBackgroundResource(R.drawable.round_corner_with_whitebg);

        dialog = builder.create();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
    }
}
