package com.pairzy.com.fbRegister.Email;

import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>FbEmailFrgBuilder</h2>
 * <P>
 *
 * </P>
 * @since  2/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface FbEmailFrgBuilder
{
    @FragmentScoped
    @Binds
    FbEmailFragment getEmailFragment(FbEmailFragment emailFragment);

    @FragmentScoped
    @Binds
    FbEmailFrgContract.Presenter taskPresenter(FbEmailFrgPresenter presenter);
}
