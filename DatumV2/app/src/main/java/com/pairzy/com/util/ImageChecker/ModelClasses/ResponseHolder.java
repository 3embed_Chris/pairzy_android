package com.pairzy.com.util.ImageChecker.ModelClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * @since /17/2018.
 */
public class ResponseHolder
{
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("weapon")
    @Expose
    private Double weapon;
    @SerializedName("alcohol")
    @Expose
    private Double alcohol;
    @SerializedName("drugs")
    @Expose
    private Double drugs;
    @SerializedName("faces")
    @Expose
    private ArrayList<Face> faces = null;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
    public Double getWeapon()
    {
        return weapon;
    }

    public void setWeapon(Double weapon)
    {
        this.weapon = weapon;
    }

    public Double getAlcohol()
    {
        return alcohol;
    }

    public void setAlcohol(Double alcohol)
    {
        this.alcohol = alcohol;
    }

    public Double getDrugs() {
        return drugs;
    }

    public void setDrugs(Double drugs) {
        this.drugs = drugs;
    }

    public ArrayList<Face> getFaces() {
        return faces;
    }

    public void setFaces(ArrayList<Face> faces) {
        this.faces = faces;
    }
}
