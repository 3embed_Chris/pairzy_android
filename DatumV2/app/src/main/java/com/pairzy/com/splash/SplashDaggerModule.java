package com.pairzy.com.splash;

import android.app.Activity;
import com.pairzy.com.dagger.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h1>SplashDaggerModule</h1>
 *
 * @author Shaktisinh Jadeja
 * @version 2.0
 * @since 04/01/2018
 */
@Module
public abstract class SplashDaggerModule
{
    @ActivityScoped
    @Binds
    abstract Activity provideActivity(SplashActivity splashActivity);

    @ActivityScoped
    @Binds
    abstract SplashContract.View provideView(SplashActivity splashActivity);

    @ActivityScoped
    @Binds
    abstract SplashContract.Presenter taskPresenter(SplashPresenter presenter);

}