package com.pairzy.com.util.boostDialog;

/**
 * Created by ankit on 29/5/18.
 */

public interface SlideItemClickCallback
{
    void onSlideItemClick(int position);
}
