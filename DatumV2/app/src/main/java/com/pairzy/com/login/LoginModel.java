package com.pairzy.com.login;

import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.pairzy.com.AppController;
import com.pairzy.com.BaseModel;
import com.pairzy.com.MqttChat.Database.CouchDbController;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.LocationResponse;
import com.pairzy.com.data.model.LoginData;
import com.pairzy.com.data.model.LoginResponse;
import com.pairzy.com.data.model.MyPrefrance;
import com.pairzy.com.data.model.SearchPreference;
import com.pairzy.com.data.model.Subscription;
import com.pairzy.com.data.model.appversion.AppVersionData;
import com.pairzy.com.data.model.appversion.AppVersionResponse;
import com.pairzy.com.data.model.coinBalance.Coins;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.model.coinCoinfig.CoinData;
import com.pairzy.com.data.source.PassportLocationDataSource;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.locationScreen.model.LocationHolder;
import com.pairzy.com.passportLocation.model.PassportLocation;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.DeviceUuidFactory;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.Utility;
import com.facebookmanager.com.FacebookUserDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
/**
 * <h2>OTPModel</h2>
 * <p>
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 06/01/2018.
 **/
class LoginModel extends BaseModel
{
    private final String TAG = "LoginModel";
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    DeviceUuidFactory deviceUuidFactory;
    @Inject
    Utility utility;
    @Inject
    CouchDbController couchDbController;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    PassportLocationDataSource locationDataSource;
    @Inject
    Geocoder geocoder;

    @Inject
    LoginModel() {}

    /*
     * Parsing data.*/
    public  void parseLoginResponse(String response) throws DataParsingException,EmptyData
    {
        try
        {
            LoginResponse value=utility.getGson().fromJson(response,LoginResponse.class);
            LoginData data=value.getData();
            if(data!=null)
            {
                    if (data.isPassportLocation())
                    {
                        LocationResponse locationResponse = data.getLocationResponse();
                        if (locationResponse != null) {
                            PassportLocation passportLocation = null;
                            LocationHolder locationHolder = new LocationHolder();
                            locationHolder.setLongitude(locationResponse.getLongitude());
                            locationHolder.setLatitude(locationResponse.getLatitude());
                            passportLocation = getLocationName(locationHolder);
                            if (passportLocation != null) {
                                String jsonPassport = utility.getGson().toJson(passportLocation, PassportLocation.class);
                                locationDataSource.setFeatureLocation(jsonPassport);
                                locationDataSource.setFeaturedLcoationActive(true);
                            }else
                            {
                                locationDataSource.setFeaturedLcoationActive(false);
                            }
                        }
                    }else
                    {
                        locationDataSource.setFeaturedLcoationActive(false);
                    }

                dataSource.setMyDetails(response);
                dataSource.setToken(data.getToken());
                dataSource.setName(data.getFirstName());
                dataSource.setUserId(data.getId());
                dataSource.setBirthDate(data.getDob());
                dataSource.setCountryCode(data.getCountryCode());
                dataSource.setMobileNumber(data.getContactNumber());
                dataSource.setGender(data.getGender());
                dataSource.setEmail(data.getEmail());
                dataSource.setProfilePicture(data.getProfilePic());
                dataSource.setUserVideoThumbnail(data.getProfileVideoThumbnail());
                dataSource.setUserVideo(data.getProfileVideo());
                ArrayList<String> other_images=data.getOtherImages();
                if(other_images==null)
                {
                    other_images=new ArrayList<>();
                }
                dataSource.setUserOtherImages(other_images);

                /*
                 *Storing the search preference*/
                ArrayList<SearchPreference> search_data=data.getSearchPreferences();
                String search_details=utility.getGson().toJson(search_data);
                dataSource.setSearchPreference(search_details);
                /*
                 *Storing the My preference*/
                ArrayList<MyPrefrance> mySearch_data = data.getmyPreferences();
                String mySearch_details=utility.getGson().toJson(mySearch_data);
                dataSource.setMyPreference(mySearch_details);

                /*
                 * save subscription
                 */
                ArrayList<Subscription> subscriptionList = data.getSubscription();
                if(subscriptionList != null && !subscriptionList.isEmpty()){
                    Subscription subscription = subscriptionList.get(0);
                    String subsJson = utility.getGson().toJson(subscription,Subscription.class);
                    dataSource.setSubscription(subsJson);
                }

                /*
                 *coin config data
                 */
                CoinData coinData = value.getCoinConfigData();
                coinConfigWrapper.setCoinData(coinData);

                /*
                 *wallet
                 */
                Coins coins = value.getWalletCoins();
                coinBalanceHolder.setCoinBalance(coins.getCoin().toString());

                dataSource.setLoggedIn(true);
                initialChatSetup(data);
            }else
            {
                throw new EmptyData("CoinData is empty!");
            }
        }catch (Exception e)
        {
            dataSource.setLoggedIn(false);
            throw new DataParsingException(e.getMessage());
        }
    }


    /*
     * Code from hola live on success of mob verification.
     */
    private void initialChatSetup(LoginData loginData){

        String profilePic = "", userName = "";

        CouchDbController db = AppController.getInstance().getDbController();

        Map<String, Object> map = new HashMap<>();
        if (!TextUtils.isEmpty(loginData.getProfilePic())) {
            profilePic = loginData.getProfilePic();
            map.put("userImageUrl", profilePic);

        } else {
            map.put("userImageUrl", "");
        }
        if (!TextUtils.isEmpty(loginData.getFirstName())) {
            userName = loginData.getFirstName();
            map.put("userName", userName);
        }
        map.put("userId", loginData.getId());
        map.put("userIdentifier", loginData.getId());
        map.put("apiToken", loginData.getToken());
                            /*
                             * By phone number verification
                             */
        map.put("userLoginType", 1);

        if (!db.checkUserDocExists(AppController.getInstance().getIndexDocId(), loginData.getId())) {

            String userDocId = db.createUserInformationDocument(map);

            db.addToIndexDocument(AppController.getInstance().getIndexDocId(), loginData.getId(), userDocId);

        } else {
            db.updateUserDetails(db.getUserDocId(loginData.getId(), AppController.getInstance().getIndexDocId()), map);
        }
        if (!userName.isEmpty()) {
            db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(), loginData.getId(), 1, true);
        } else {
            db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(), loginData.getId(), 1, false);
        }
        AppController.getInstance().setSignedIn(false,true,loginData.getId() , userName, loginData.getId());
    }

//    /*
//     * Parsing data.*/
//    public void parseSignUpResponse(String response) throws DataParsingException,EmptyData
//    {
//        try
//        {
//            LoginResponse value=utility.getGson().fromJson(response,LoginResponse.class);
//            LoginData data=value.getData();
//            if(data!=null)
//            {
//                dataSource.setName(data.getFirstName());
//                if(dataSource.getGender().equals("1")){
//                    dataSource.setGender("male");
//                }else{
//                    dataSource.setGender("female");
//                }
//
//                dataSource.setProfilePicture(data.getProfilePic());
//                dataSource.setToken(data.getToken());
//                dataSource.setIsFacebookLogin(true);
//                initialChatSetup(data);
//            }else
//            {
//                throw new EmptyData("CoinData is empty!");
//            }
//        }catch (Exception e)
//        {
//            throw new DataParsingException(e.getMessage());
//        }
//    }



    Map<String, Object> prepareFacebookRequest(FacebookUserDetails userDetails)
    {
        Map<String, Object> map = new HashMap<>();
        if(!TextUtils.isEmpty(userDetails.getFirstName()))
            map.put(ApiConfig.FBRequestKey.FIRST_NAME, userDetails.getFirstName());
        map.put(ApiConfig.FBRequestKey.FB_ID, userDetails.getId());
        map.put(ApiConfig.FBRequestKey.PUSH_TOKEN,dataSource.getPushToken());
        map.put(ApiConfig.FBRequestKey.DEVICE_ID,deviceUuidFactory.getDeviceUuid());
        map.put(ApiConfig.FBRequestKey.DEVICE_MAKER,utility.getDeviceMaker());
        map.put(ApiConfig.FBRequestKey.DEVICE_MODEL,utility.getModel());
        map.put(ApiConfig.FBRequestKey.DEVICE_TYPE, ApiConfig.DeviceType.ANDROID);
        if(!TextUtils.isEmpty(userDetails.getBio()))
            map.put(ApiConfig.FBRequestKey.BIO_DATA,userDetails.getBio());
        if(!TextUtils.isEmpty(userDetails.getWork()))
            map.put(ApiConfig.FBRequestKey.WORK,userDetails.getWork());
        if(!TextUtils.isEmpty(userDetails.getEducation()))
            map.put(ApiConfig.FBRequestKey.EDUCATION,userDetails.getEducation());
        if(!TextUtils.isEmpty(userDetails.getEmail()))
            map.put(ApiConfig.FBRequestKey.EMAIL,userDetails.getEmail());
        map.put(ApiConfig.FBRequestKey.DEVICE_OS, ""+android.os.Build.VERSION.RELEASE);
        map.put(ApiConfig.FBRequestKey.APP_VERSION, utility.getAppCurrentVersion());
        return map;
    }

    boolean isNeedToCallUpdateVersionApi() {
//        if( (System.currentTimeMillis() - dataSource.getNewVersionUpdatedTime() >= AppConfig.UPDATE_CHECK_INTERVAL) || dataSource.getNewVersionUpdatedTime() == 0L){
//            //time is expired or first time
//            return true;
//        }else if(dataSource.getNewVersion().isEmpty()){
//            return true;
//        }
        return true;
    }

    AppVersionData parseAppVersionData(String response) {
        try{
            AppVersionResponse appVersionResponse = utility.getGson().fromJson(response,AppVersionResponse.class);
            AppVersionData appVersionData =  appVersionResponse.getData();

            //save the app version data
            if(!dataSource.getNewVersion().equals(appVersionData.getAppversion())){
                dataSource.setUpdateDialogShowed(false);
            }
            dataSource.setNewVersionData(appVersionData.getAppversion());
            dataSource.setIsMandatory(appVersionData.getIsMandatory());
            dataSource.setNewVersionUpdatedTime(System.currentTimeMillis());
            dataSource.setUpdateDialogShowed(true);

            return appVersionData;
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isMandatory(AppVersionData appVersionData){
        try{
            return appVersionData.getIsMandatory();
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    private int getVersionString(String appVersion) {
        try {
            String version = appVersion.replaceAll("[.]","");
            return Integer.parseInt(version);
        }catch (Exception e){
            e.printStackTrace();
        }
        return 1;
    }

    boolean getSavedIsMandatory(){
        return dataSource.isMandatory();
    }

    boolean shouldShowUpdateDialog() {
        try {
            String appVersion = dataSource.getNewVersion();
            int lestestVersionSum = getVersionString(appVersion.trim());
            int currentVersionSum = getVersionString(utility.getAppCurrentVersion());
            boolean isMendatory = getSavedIsMandatory();
            Log.d(TAG, "shouldShowUpdateDialog saved_data: lestestVersionSum, currentVersionSum"+lestestVersionSum+" , "+currentVersionSum);
            if(lestestVersionSum > currentVersionSum){
                if(isMendatory){
                    return true;
                }
                else{
                    return !dataSource.isUpdatedDialogShowed();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    boolean shouldShowUpdateDialog(AppVersionData appVersionData) {
        try {
            if (appVersionData == null) {
                Log.e(TAG, "shouldShowUpdateDialog:  appVersionData can not be null!");
                return false;
            }
            String appVersion = appVersionData.getAppversion();
            int lestestVersionSum = getVersionString(appVersion.trim());
            int currentVersionSum = getVersionString(utility.getAppCurrentVersion());
            boolean isMendatory = appVersionData.getIsMandatory();
            Log.d(TAG, "shouldShowUpdateDialog: lestestVersionSum, currentVersionSum"+lestestVersionSum+" , "+currentVersionSum);
            if(lestestVersionSum > currentVersionSum){
                if(isMendatory){
                    return true;
                }
                else{
                    return !dataSource.isUpdatedDialogShowed();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    /**
     * @param mobile_number : mobile_number
     * @return parameters
     */
    Map<String, Object> requestOtpParams(String mobile_number,Boolean isPhoneUpdate)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.RequestOtpKey.PHONE_NUMBER, mobile_number);
        if(isPhoneUpdate)
            map.put(ApiConfig.RequestOtpKey.TYPE, 3);
        else
            map.put(ApiConfig.RequestOtpKey.TYPE, 1);
        map.put(ApiConfig.RequestOtpKey.DEVICE_ID, Build.ID);
        return map;
    }

    /*
     * Parsing the otp verification data*/
    public boolean parseVerificationRes(String response) throws EmptyData,DataParsingException
    {
        try
        {
            LoginResponse response_data=utility.getGson().fromJson(response,LoginResponse.class);
            LoginData data=response_data.getData();
            if(data!=null)
            {
                if(data.getIsNewUser())
                {
                    return true;
                }else
                {
                    try {
                        if (data.isPassportLocation())
                        {
                            LocationResponse locationResponse = data.getLocationResponse();
                            if (locationResponse != null) {
                                PassportLocation passportLocation = null;
                                LocationHolder locationHolder = new LocationHolder();
                                locationHolder.setLongitude(locationResponse.getLongitude());
                                locationHolder.setLatitude(locationResponse.getLatitude());
                                passportLocation = getLocationName(locationHolder);
                                if (passportLocation != null) {
                                    String jsonPassport = utility.getGson().toJson(passportLocation, PassportLocation.class);
                                    locationDataSource.setFeatureLocation(jsonPassport);
                                    locationDataSource.setFeaturedLcoationActive(true);
                                }else
                                {
                                    locationDataSource.setFeaturedLcoationActive(false);
                                }
                            }
                        }else
                        {
                            locationDataSource.setFeaturedLcoationActive(false);
                        }

                        ArrayList<Subscription> subscriptionList = data.getSubscription();
                        if(subscriptionList != null && !subscriptionList.isEmpty()){
                            Subscription subscription = subscriptionList.get(0);
                            //if it is free plan...no need to save it.
                            if(!subscription.getSubscriptionId().equals(AppConfig.FREE_PLAN_SUBS_ID)) {
                                String subsJson = utility.getGson().toJson(subscription, Subscription.class);
                                dataSource.setSubscription(subsJson);
                            }
                        }
                    }catch (Exception ignored){}
                    dataSource.setMyDetails(response);
                    dataSource.setToken(data.getToken());
                    dataSource.setName(data.getFirstName());
                    dataSource.setUserId(data.getId());
                    dataSource.setBirthDate(data.getDob());
                    dataSource.setCountryCode(data.getCountryCode());
                    dataSource.setMobileNumber(data.getContactNumber());
                    dataSource.setGender(data.getGender());
                    dataSource.setEmail(data.getEmail());
                    dataSource.setProfilePicture(data.getProfilePic());
                    dataSource.setUserVideoThumbnail(data.getProfileVideoThumbnail());
                    dataSource.setUserVideo(data.getProfileVideo());
                    dataSource.setMyEducation(data.getEducation());
                    dataSource.setMyWorkPlace(data.getWork());
                    dataSource.setMyJob(data.getJob());
                    ArrayList<String> other_images=data.getOtherImages();
                    if(other_images==null)
                    {
                        other_images=new ArrayList<>();
                    }
                    dataSource.setUserOtherImages(other_images);

                    /*
                     *Storing preference */
                    ArrayList<SearchPreference> search_data=data.getSearchPreferences();
                    String search_details=utility.getGson().toJson(search_data);
                    dataSource.setSearchPreference(search_details);
                    /*
                     *Storing My preference */
                    ArrayList<MyPrefrance> mySearch_data = data.getmyPreferences();
                    String mySearch_details=utility.getGson().toJson(mySearch_data);
                    dataSource.setMyPreference(mySearch_details);

                    /*
                     *coin config data
                     */
                    CoinData coinData = response_data.getCoinConfigData();
                    coinConfigWrapper.setCoinData(coinData);

                    /*
                     *wallet
                     */
                    Coins coins = response_data.getWalletCoins();
                    coinBalanceHolder.setCoinBalance(coins.getCoin().toString());

                    dataSource.setLoggedIn(true);
                    initialChatSetup(data);
                    return false;
                }
            }else
            {
                throw new EmptyData("CoinData is empty!");
            }
        }catch (Exception e)
        {
            dataSource.setLoggedIn(false);
            e.printStackTrace();
            throw new DataParsingException(e.getMessage());
        }
    }

    /**
     * Fetch the full address via lat and long.
     * @param locationHolder contain lat , long.
     * @return PassportLocation.
     */
    public PassportLocation getLocationName(LocationHolder locationHolder) {
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(locationHolder.getLatitude(),locationHolder.getLongitude(),1);
            if(addresses != null){
                String  subLocation = addresses.get(0).getSubLocality();
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String addressName = addresses.get(0).getFeatureName();

                boolean cityEmpty = TextUtils.isEmpty(city);
                boolean stateEmpty = TextUtils.isEmpty(state);
                boolean countryEmpty = TextUtils.isEmpty(country);
                String subAddress = ((cityEmpty)?"":city+((stateEmpty)?"":","))
                        +((stateEmpty)?"":state+((countryEmpty)?"":","))
                        +((countryEmpty)?"":country+".");
                PassportLocation currentLocation = new PassportLocation();
                currentLocation.setLocationName(addressName);
                currentLocation.setSubLocationName(subAddress);
                currentLocation.setLatitude(locationHolder.getLatitude());
                currentLocation.setLongitude(locationHolder.getLongitude());
                return currentLocation;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param mobile_number : mobile_number
     * @return parameters
     */
    Map<String, Object> verifyOtpData(String mobile_number,String otp)
    {
        Integer otp_data=Integer.parseInt(otp);
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.VerifyOtpKey.PHONE_NUMBER, mobile_number);
        map.put(ApiConfig.VerifyOtpKey.TYPE, ApiConfig.VerificationType.NEW_REGISTRATION_OR_LOGIN);
        map.put(ApiConfig.VerifyOtpKey.OTP,otp_data);
        map.put(ApiConfig.VerifyOtpKey.PUSH_TOKEN,dataSource.getPushToken());
        map.put(ApiConfig.VerifyOtpKey.DEVICE_ID,deviceUuidFactory.getDeviceUuid());
        map.put(ApiConfig.VerifyOtpKey.DEVICE_MAKER,utility.getDeviceMaker());
        map.put(ApiConfig.VerifyOtpKey.DEVICE_MODEL,utility.getModel());
        map.put(ApiConfig.VerifyOtpKey.DEVICE_TYPE, ApiConfig.DeviceType.ANDROID);
        map.put(ApiConfig.VerifyOtpKey.DEVICE_OS, ""+android.os.Build.VERSION.RELEASE);
        map.put(ApiConfig.VerifyOtpKey.APP_VERSION, utility.getAppCurrentVersion());
        return map;
    }
}
