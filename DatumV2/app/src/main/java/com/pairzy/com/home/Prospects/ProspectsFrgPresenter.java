package com.pairzy.com.home.Prospects;

import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;
/**
 * @since  3/5/2018.
 * @author 3Embed.
 * @since 1.0.
 */
public class ProspectsFrgPresenter implements ProspectsContract.Presenter
{
    private ProspectsContract.View view;
    private CompositeDisposable compositeDisposable;
    @Inject
    ProspectsFrgPresenter(){
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(ProspectsContract.View view)
    {
        this.view= view;
    }

    @Override
    public void dropView()
    {
        compositeDisposable.clear();
        view=null;
    }
}
