package com.pairzy.com.momentGrid.model;

import android.view.View;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.R;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MomentsHolder extends  RecyclerView.ViewHolder  implements View.OnClickListener {

    private ItemActionCallBack callBack;

    @BindView(R.id.ivPostlistPost)
    SimpleDraweeView sdvPostPreview;
    @BindView(R.id.card_view)
    CardView cardView;
    @BindView(R.id.ivPostListVideoIcon)
    SimpleDraweeView videoIcon;

    public MomentsHolder(View itemView,ItemActionCallBack callBack)
    {
        super(itemView);
        this.callBack = callBack;
        ButterKnife.bind(this,itemView);
        cardView.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if(callBack != null)
            callBack.onClick(v.getId(),getAdapterPosition());
    }
}
