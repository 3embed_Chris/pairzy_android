package com.pairzy.com.home.Prospects.LikesMe;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.home.Prospects.LikesMe.Model.LikesMeAdapter;
import com.pairzy.com.home.Prospects.LikesMe.Model.LikesMeItemPojo;
import com.pairzy.com.home.Prospects.LikesMe.Model.LikeLoadMoreStatus;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import dagger.Module;
import dagger.Provides;
/**
 * @since   3/23/2018.
 * @author 3Embed.
 * @version 1.0
 */
@Module
public class LikeMeUtil
{
    @Provides
    @FragmentScoped
    ArrayList<LikesMeItemPojo> getList()
    {
        return new ArrayList<>();
    }
    @Provides
    @FragmentScoped
    LikesMeAdapter getUserListAdapter(ArrayList<LikesMeItemPojo> list, TypeFaceManager typeFaceManager, Utility utility)
    {
        return new LikesMeAdapter(list,typeFaceManager,utility);
    }

    @Provides
    @FragmentScoped
    LinearLayoutManager getLinearLayoutManager(Activity activity)
    {
        return new LinearLayoutManager(activity);
    }

    @Provides
    @FragmentScoped
    LikeLoadMoreStatus getLodeMoreStatus()
    {
        return new LikeLoadMoreStatus();
    }
}
