package com.pairzy.com.MyProfile.VideoItem;

import javax.inject.Inject;

/**
 *@since  4/4/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class VideoItemPresenter implements VideoItemContract.Presenter
{
    private VideoItemContract.View view;
    @Inject
    VideoItemPresenter(){}

    @Override
    public void takeView(VideoItemContract.View view)
    {
     this.view=view;
    }

    @Override
    public void dropView()
    {
        view=null;
    }
}
