package com.pairzy.com.MySearchPreference;

import android.app.Activity;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.boostDialog.BoostDialog;
import com.pairzy.com.util.progressbar.LoadingProgress;
import dagger.Module;
import dagger.Provides;
/**
 * @since  3/8/2018.
 */
@Module
public class MySearchPrefUtil
{
    @Provides
    @ActivityScoped
    AnimatorHandler animatorHandler(Activity activity)
    {
        return new AnimatorHandler(activity);
    }

    @ActivityScoped
    @Provides
    LoadingProgress getLoadingProgress(Activity activity)
    {
        return new LoadingProgress(activity);
    }

    @Provides
    @ActivityScoped
    BoostDialog getBootsDialog(Activity activity, TypeFaceManager typeFaceManager, PreferenceTaskDataSource dataSource, Utility utility)
    {
        return new BoostDialog(activity,typeFaceManager,dataSource,utility);
    }

}
