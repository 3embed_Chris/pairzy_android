package com.pairzy.com.MyProfile.editName;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.MyProfile.MyProfilePagePresenter;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by ankit on 27/4/18.
 */

public class EditNameActivity extends BaseDaggerActivity implements EditNameContract.View,
                            TextWatcher{

    @Inject
    EditNamePresenter presenter;
    @Inject
    Activity activity;
    @Inject
    Utility utility;

    @Inject
    TypeFaceManager typeFaceManager;

    @BindView(R.id.close_button)
    RelativeLayout close_button;
    @BindView(R.id.skip_page)
    TextView skip_page;
    @BindView(R.id.first_title)
    TextView first_title;
    @BindView(R.id.second_title)
    TextView second_title;

    @BindView(R.id.input_name)
    EditText input_name;
    @BindView(R.id.hint_details)
    TextView hint_details;
    private Unbinder unbinder;

    @BindView(R.id.btnNext)
    RelativeLayout btnNext;
    @BindView(R.id.parentLayout)
    RelativeLayout parent_view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_name);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        initUIDetails();
        initData(getIntent());
    }

    private void initData(Intent intent) {
        String name = intent.getStringExtra(MyProfilePagePresenter.NAME_DATA);
        input_name.setText(name==null?"":name);
        input_name.setSelection(input_name.getText().length());
    }

    /*
    * intialization of the xml content.*/
    private void initUIDetails()
    {
        btnNext.setEnabled(false);
        skip_page.setTypeface(typeFaceManager.getCircularAirBook());
        first_title.setTypeface(typeFaceManager.getCircularAirBold());
        second_title.setTypeface(typeFaceManager.getCircularAirBold());
        input_name.setTypeface(typeFaceManager.getCircularAirBook());
        hint_details.setTypeface(typeFaceManager.getCircularAirLight());
        input_name.addTextChangedListener(this);
        input_name.setOnEditorActionListener((v, actionId, event) -> {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE))
            {
                presenter.validateName(input_name.getText().toString());
            }
            return false;
        });
        utility.openSpotInputKey(activity,input_name);
    }


    @OnClick(R.id.skip_page)
    void onSkipClick()
    {
        onBackPressed();
    }

    @OnClick(R.id.close_button)
    void onClose()
    {
        onBackPressed();
    }

    @OnClick(R.id.btnNext)
    void onNext()
    {
        presenter.validateName(input_name.getText().toString());
    }

    @OnClick(R.id.parentLayout)
    void onParentClick(){}
    @Override
    public void invalidateName(String message)
    {
        onError(message);
    }

    @Override
    public void onValidName(String name)
    {
        Intent intent = new Intent();
        intent.putExtra(MyProfilePagePresenter.NAME_DATA,name);
        setResult(RESULT_OK,intent);
        onBackPressed();
    }

    @Override
    public void onError(String message)
    {
        Snackbar snackbar = Snackbar
                .make(parent_view,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }


    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(parent_view,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }
    /*
    *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            btnNext.setEnabled(true);
        }
        else {
            btnNext.setEnabled(false);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {
        if(charSequence.length()>0)
        {
            handelNextButton(true);
        }else
        {
            handelNextButton(false);
        }
    }
    @Override
    public void afterTextChanged(Editable editable) {}
    @Override
    public void onBackPressed() {
        utility.closeSpotInputKey(activity,input_name);
        this.finish();
        activity.overridePendingTransition(R.anim.slide_from_left,R.anim.slide_to_right);
        super.onBackPressed();
    }
}
