package com.pairzy.com.MyProfile.Model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.R;

import java.util.List;

public class MomentMediaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private List<List<MomentsData>> media_list;
    private Context mcontext;
    private OpenInstagram instagram;
    private MomentClickCallback clickCallback;

    public MomentMediaAdapter(Context context,List<List<MomentsData>> lists)
    {
        this.media_list=lists;
        this.mcontext=context;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.insta_media_view,parent, false);
        return new InstaMediaHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        try
        {
            handeData((InstaMediaHolder) holder);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return media_list.size();
    }

    private void handeData(InstaMediaHolder holder)
    {
        int position=holder.getAdapterPosition();
        MomentsGridAdapter gridAdapter=new MomentsGridAdapter(mcontext,media_list.get(position));
        gridAdapter.setClickCallback(clickCallback);
        holder.item_grid.setAdapter(gridAdapter);
        holder.item_grid.setLayoutManager(new GridLayoutManager(mcontext,3));
    }

    public void setClickCallback(MomentClickCallback clickCallback) {
        this.clickCallback = clickCallback;
    }
}
