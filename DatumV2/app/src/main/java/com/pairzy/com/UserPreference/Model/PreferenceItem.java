package com.pairzy.com.UserPreference.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * @since  2/22/2018.
 */
public class PreferenceItem
{
    private int position;
    private int list_no;
    private int max_count;
    @SerializedName("pref_id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("options")
    @Expose
    private ArrayList<String> options=null;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("iconNonSelected")
    @Expose
    private String iconNonSelected;
    @SerializedName("iconSelected")
    @Expose
    private String iconSelected;
    @SerializedName("isDone")
    @Expose
    private Boolean isDone;
    @SerializedName("selectedValues")
    @Expose
    private ArrayList<String> selectedValues=null;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getList_no() {
        return list_no;
    }

    public void setList_no(int list_no) {
        this.list_no = list_no;
    }

    public int getMax_count() {
        return max_count;
    }

    public void setMax_count(int max_count) {
        this.max_count = max_count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public ArrayList<String> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<String> options) {
        this.options = options;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getIconNonSelected() {
        return iconNonSelected;
    }

    public void setIconNonSelected(String iconNonSelected) {
        this.iconNonSelected = iconNonSelected;
    }

    public String getIconSelected() {
        return iconSelected;
    }

    public void setIconSelected(String iconSelected) {
        this.iconSelected = iconSelected;
    }

    public Boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(Boolean isDone)
    {
        this.isDone = isDone;
    }

    public ArrayList<String> getSelectedValues()
    {
        return selectedValues;
    }

    public void setSelectedValues(ArrayList<String> selectedValues)
    {
        this.selectedValues = selectedValues;
    }
}
