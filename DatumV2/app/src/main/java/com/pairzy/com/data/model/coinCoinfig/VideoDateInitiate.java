package com.pairzy.com.data.model.coinCoinfig;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 4/7/18.
 */

public class VideoDateInitiate {
    @Expose
    @SerializedName("Coin")
    Integer coin;

    public Integer getCoin() {
        return coin;
    }

    public void setCoin(Integer coin) {
        this.coin = coin;
    }
}
