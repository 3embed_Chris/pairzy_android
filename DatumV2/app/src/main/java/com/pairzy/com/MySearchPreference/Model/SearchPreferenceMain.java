package com.pairzy.com.MySearchPreference.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * @since  3/7/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class SearchPreferenceMain
{

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<PreferenceItem> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<PreferenceItem> getData() {
        return data;
    }

    public void setData(ArrayList<PreferenceItem> data)
    {
        this.data = data;
    }

}