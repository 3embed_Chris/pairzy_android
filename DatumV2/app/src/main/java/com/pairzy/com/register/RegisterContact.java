package com.pairzy.com.register;


import android.content.Intent;
import android.os.Bundle;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

import dagger.android.support.DaggerFragment;

public interface RegisterContact
{
    interface View extends BaseView
    {
        void openPreferencePage();
        void updateProgress(int progress);
        void  showError(String message);
        void showMessage(String message);
        void openFragment(DaggerFragment fragment, boolean keepBack);
        String getCountryCode();
        String getMobileNumber();
        String getEnteredOtp();
        void updateInterNetStatus(boolean status);
    }


    interface Presenter extends BasePresenter
    {
        String EMAIL_DATA="email_data";
        String NAME_DATA="name_data";
        String DOB_DATA="dob_data";
        String GENDER_DATA="gender_data";
        String PIC_DATA="pic_data";
        String VIDEO_DATA="video_data";
        String VIDEO_THUMB_DATA="video_thumb_data";
        String LAT="latitude";
        String LNG="longitude";

        void updateProgress(int progress);

        void showError(String message);

        void showMessage(String message);

        void launchFragment(DaggerFragment fragment, boolean keepBack);

        void updateProfile(Bundle data);

        void getUserLocation();

        void getUserLocationApi();

        void askForLocationPermission();

        void updateUserDetails(Bundle data);

        boolean onHandelActivityResult(int requestCode, int resultCode, Intent data);

        void initNetworkObserver();
    }
}
