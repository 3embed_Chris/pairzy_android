package com.pairzy.com.MyProfile.editEmail;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 27/4/18.
 */

@Module
public abstract class EditEmailModule {

    @ActivityScoped
    @Binds
    abstract Activity editEmailActivity(EditEmailActivity activity);

    @ActivityScoped
    @Binds
    abstract EditEmailContract.View editEmailView(EditEmailActivity activity);

    @ActivityScoped
    @Binds
    abstract EditEmailContract.Presenter editEmailPresenter(EditEmailPresenter presenter);
}
