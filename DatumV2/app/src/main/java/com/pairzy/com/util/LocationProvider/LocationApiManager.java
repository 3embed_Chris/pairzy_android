package com.pairzy.com.util.LocationProvider;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
/**
 * <h2></h2>
 * @since 3/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class LocationApiManager
{
    private String GoogleApiKey="";
    private  HttpURLConnection conn;

    public LocationApiManager(String key)
    {
        this.GoogleApiKey=key;
    }

    public void getLocation(LocationApiCallback callback)
    {

        if(GoogleApiKey==null)
            throw new IllegalArgumentException("Initialized the Manager first!");
        /*
        * Getting the location */
        new CollectLocation().execute(callback);
    }

    @SuppressLint("StaticFieldLeak")
    private class CollectLocation extends AsyncTask<LocationApiCallback,Void,String>
    {
       private LocationApiCallback callback_data;
       private boolean isError=false;


        @Override
        protected String doInBackground(LocationApiCallback... locationApiCallbacks)
        {
            callback_data=locationApiCallbacks[0];
            isError=false;
            try {
                return getLocationApi();
            } catch (Exception e)
            {
                e.printStackTrace();
                isError=true;
                return e.getMessage();

            }
            finally {
                if(conn!=null)
                    conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            if(isError)
            {
                if(callback_data!=null)
                    callback_data.onError(s);
            }else
            {
                try
                {
                    JSONObject jsonObject=new JSONObject(s);
                    Double accuracy=jsonObject.getDouble( "accuracy");
                    JSONObject location_Data=jsonObject.getJSONObject("location");
                    Double lat=location_Data.getDouble("lat");
                    Double lng=location_Data.getDouble("lng");
                    if(callback_data!=null)
                        callback_data.onSuccess(lat,lng,accuracy);

                } catch (JSONException e)
                {
                    e.printStackTrace();
                    if(callback_data!=null)
                        callback_data.onError(e.getMessage());
                }
            }
        }
    }

    private String getLocationApi() throws Exception
    {
        URL url = new URL(getLocationUrl());
        conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(15000 );
        conn.setConnectTimeout(15000);
        conn.setRequestMethod("POST");
        int responseCode=conn.getResponseCode();
        if (responseCode == HttpsURLConnection.HTTP_OK)
        {

            InputStream in = conn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null) {
                result.append(line);
            }
            in.close();
            return result.toString();
        }
        else {
            return "false : " + responseCode;
        }

    }


    private String getLocationUrl()
    {
        return "https://www.googleapis.com/geolocation/v1/geolocate?key="+GoogleApiKey;
    }
}
