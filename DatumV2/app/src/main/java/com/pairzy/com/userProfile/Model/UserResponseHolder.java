package com.pairzy.com.userProfile.Model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * @since 1/4/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class UserResponseHolder
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private UserProfileData data;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public UserProfileData getData()
    {
        return data;
    }

    public void setData(UserProfileData data)
    {
        this.data = data;
    }

}
