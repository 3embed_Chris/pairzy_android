package com.pairzy.com.MySearchPreference.Model;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.pairzy.com.MySearchPreference.MySearchPrefPresenter;
import com.pairzy.com.R;
import com.pairzy.com.data.model.SearchPreference;
import com.pairzy.com.util.DatumRangeBar.DatumRangeSeekBar;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.datum_spinner.DatumSpinner;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;

import javax.inject.Inject;
/**
 * <h2>ProfileViewModel</h2>
 * <P>
 *
 * </P>
 * @since  3/7/2018.
 * @author 3Embed.
 * @version 1.0.
 */
class ViewModel
{
    private String TAG = ViewModel.class.getSimpleName();

    private LocationClickCallback clickCallback;
    private DecimalFormat decimalFormat, decimalFormatTwo;
    private final String CM ="cm";
    private final String FT ="ft";
    private final String KM ="km";
    private final String MI ="mi";

    @Inject
    Activity activity;

    @Inject
    TypeFaceManager typeFaceManager;

    @Inject
    ViewModel(){
        decimalFormat = new DecimalFormat("0.0");
        decimalFormat.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        decimalFormatTwo = new DecimalFormat("0.00");
        decimalFormatTwo.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.ENGLISH));
    }

    public void setClickCallback(LocationClickCallback clickCallback) {
        this.clickCallback = clickCallback;
    }

    public View getSelectionView(SearchPreference preferences)
    {
        @SuppressLint("InflateParams")
        View view=activity.getLayoutInflater().inflate(R.layout.preference_drop_list_view,null);
        TextView heading_text=view.findViewById(R.id.heading_text);
        heading_text.setTypeface(typeFaceManager.getCircularAirLight());
        heading_text.setText(preferences.getPreferenceTitle());
        TextView selected_text=view.findViewById(R.id.selected_text);
        selected_text.setTypeface(typeFaceManager.getCircularAirBook());
        DatumSpinner datumSpinner=view.findViewById(R.id.pref_spinner);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(
                activity, R.layout.pref_spinner_item,preferences.getOptionsValue());
        spinnerArrayAdapter.setDropDownViewResource(R.layout.pref_spinner_item);
        datumSpinner.setAdapter(spinnerArrayAdapter);
        datumSpinner.setSpinnerEventsListener(new DatumSpinner.OnSpinnerEventsListener() {
            @Override
            public void onSpinnerOpened(Spinner spinner) {}
            @Override
            public void onSpinnerClosed(Spinner spinner) {}
            @Override
            public void onItemSelected(String item)
            {
                preferences.getSelectedValue().clear();
                preferences.getSelectedValue().add(item);
                selected_text.setText(preferences.getSelectedValue().get(0));
            }
        });

        try
        {
            datumSpinner.setSelectedInitial(preferences.getOptionsValue().indexOf(preferences.getSelectedValue().get(0)));
        }catch (Exception e){}
        selected_text.setText(preferences.getSelectedValue().get(0));
        RelativeLayout rlRoot = view.findViewById(R.id.root);
        rlRoot.setOnClickListener(view2 ->datumSpinner.performClick());
        return view;
    }

    public View getMultiSelectionView(SearchPreference preferences, MySearchPrefPresenter presenter)
    {
        @SuppressLint("InflateParams")
        View view=activity.getLayoutInflater().inflate(R.layout.preference_drop_multi_list,null);
        TextView heading_text=view.findViewById(R.id.heading_text);
        heading_text.setTypeface(typeFaceManager.getCircularAirLight());
        heading_text.setText(preferences.getPreferenceTitle());
        TextView selected_text=view.findViewById(R.id.selected_text);
        selected_text.setTypeface(typeFaceManager.getCircularAirBook());
        com.pairzy.com.MySearchPreference.Model.DatumSpinner spinner=view.findViewById(R.id.pref_spinner);

        ArrayList<StateVO> spinnerArrayAdapter = new ArrayList<>();
        for (int i = 0; i < preferences.getOptionsValue().size(); i++) {
            StateVO stateVO = new StateVO();
            stateVO.setTitle(preferences.getOptionsValue().get(i));
            stateVO.setSelected(false);
            spinnerArrayAdapter.add(stateVO);
        }
        MultiListSelectAdapter multiListSelectAdapter = new MultiListSelectAdapter(activity, 0, spinnerArrayAdapter,preferences);
        spinner.setAdapter(multiListSelectAdapter);

        spinner.setSpinnerEventsListener(new com.pairzy.com.MySearchPreference.Model.DatumSpinner.OnSpinnerEventsListener() {
            @Override
            public void onSpinnerOpened(Spinner spinner) {}
            @Override
            public void onSpinnerClosed(Spinner spinner) {
                if(multiListSelectAdapter.getSelectedValues()!=null) {
                    if(multiListSelectAdapter.getSelectedValues().size() > 0) {
                        preferences.setSelectedValue(multiListSelectAdapter.getSelectedValues());
                        if (multiListSelectAdapter.getSelectedValues().size() > 1) {
                            selected_text.setText(multiListSelectAdapter.getSelectedValues().get(0) + "+" + (multiListSelectAdapter.getSelectedValues().size() - 1));
                        } else {
                            selected_text.setText(multiListSelectAdapter.getSelectedValues().get(0));
                        }
                    }
                }
            }
            @Override
            public void onItemSelected(String item)
            {
                preferences.getSelectedValue().clear();
                preferences.getSelectedValue().add(item);
                selected_text.setText(preferences.getSelectedValue().get(0));
            }
        });


        RelativeLayout rlRoot = view.findViewById(R.id.root);
        rlRoot.setOnClickListener(view2 -> {
            spinner.performClick();
        });
        return view;
    }
    /*
     *updating the user details.
     **/
    public View getRangeBarView(SearchPreference preferences,boolean isSingleThumb)
    {
        @SuppressLint("InflateParams")
        View view=activity.getLayoutInflater().inflate(R.layout.preference_range_bar,null);
        TextView heading_text=view.findViewById(R.id.heading_text);
        heading_text.setTypeface(typeFaceManager.getCircularAirBook());
        switch (preferences.getPriority()){
            case 2: //age
                heading_text.setText(activity.getString(R.string.age_text));
                break;
            case 4: //height
                heading_text.setText(activity.getString(R.string.height_text));
                break;
            case 3: //distances
                heading_text.setText(activity.getString(R.string.distance_text));
                break;
            default:
                heading_text.setText(preferences.getPreferenceTitle());
        }
        TextView range_min=view.findViewById(R.id.range_min);
        range_min.setTypeface(typeFaceManager.getCircularAirBook());
        TextView range_mid=view.findViewById(R.id.range_mid);
        range_mid.setTypeface(typeFaceManager.getCircularAirBook());
        TextView range_max=view.findViewById(R.id.range_max);
        range_max.setTypeface(typeFaceManager.getCircularAirBook());
        TextView single_unit=view.findViewById(R.id.single_unit);
        single_unit.setTypeface(typeFaceManager.getCircularAirBook());
        TabLayout rang_tab=view.findViewById(R.id.tabLayout);
        ArrayList<String> options_unit=preferences.getOptionsUnits();

        if(options_unit.size()==1)
        {
            single_unit.setVisibility(View.VISIBLE);
            rang_tab.setVisibility(View.GONE);
            if(preferences.getSelectedUnit().equalsIgnoreCase("years"))
                single_unit.setText("");
        }else {
            single_unit.setVisibility(View.GONE);
            rang_tab.setVisibility(View.VISIBLE);
            for (int i = 0;i<options_unit.size(); i++)
            {
                TabLayout.Tab tab = rang_tab.newTab();
                tab.setCustomView(getTabView(options_unit.get(i)));
                rang_tab.addTab(tab);
            }
            String selected_unit=preferences.getSelectedUnit();
            int index=options_unit.indexOf(selected_unit);
            if(index<0)
                index=0;
            TabLayout.Tab tab = rang_tab.getTabAt(index);
            assert tab != null;
            tab.select();
        }

        DatumRangeSeekBar datumRangeSeekBar =view.findViewById(R.id.rangeSeekbar);
        ArrayList<String> option_value=preferences.getOptionsValue();
        Double min_value,max_value;
        try
        {
            min_value=Double.parseDouble(option_value.get(0));
            max_value=Double.parseDouble(option_value.get(1));
        }catch (Exception e)
        {
            /*
             * Default value.*/
            min_value=0.0;
            max_value=100.0;
        }
        datumRangeSeekBar.setSingleThumb(isSingleThumb);
        datumRangeSeekBar.setRangeValues(min_value,max_value);
        ArrayList<String> selected_value=preferences.getSelectedValue();
        Double selected_min,selected_max;
        try
        {
            if(preferences.getPreferenceTitle().equalsIgnoreCase("age")) {
                int minSelectedVal = (int) Double.parseDouble(selected_value.get(0));
                range_min.setText(String.valueOf(minSelectedVal));
                double maxSelectedVal;
                if((Double.parseDouble(selected_value.get(1))) == max_value) {
                    maxSelectedVal = max_value;
                    range_max.setText(String.valueOf((int)maxSelectedVal).concat("+"));
                }
                else {
                    maxSelectedVal = Double.parseDouble(selected_value.get(1));
                    range_max.setText(String.valueOf((int)maxSelectedVal));
                }
            }
//            else{
//                if(preferences.getSelectedUnit().equalsIgnoreCase(FT)){
//                    range_min.setText(ftStringFormat(Double.parseDouble(decimalFormat.format(Double.parseDouble(selected_value.get(0))))));
//                    range_max.setText(ftStringFormat(Double.parseDouble(decimalFormat.format(Double.parseDouble(selected_value.get(1))))));
//                }
//                else if(preferences.getSelectedUnit().equalsIgnoreCase(M)){
//                    range_min.setText(decimalFormat.format(Double.parseDouble(selected_value.get(0))));
//                    range_max.setText(decimalFormat.format(Double.parseDouble(selected_value.get(1))));
//                }
//                else if(preferences.getSelectedUnit().equalsIgnoreCase(KM)){
//                    //range_min.setText(decimalFormat.format(selected_value.get(0)));
//                    range_max.setText(selected_value.get(1));
//                }
//                else if(preferences.getSelectedUnit().equalsIgnoreCase(MI)){
//                    //range_min.setText(decimalFormat.format(selected_value.get(0)));
//                    range_max.setText(selected_value.get(1));
//                }
//            }
            selected_min=Double.parseDouble(selected_value.get(0));
            selected_max=Double.parseDouble(selected_value.get(1));
        }catch (Exception e)
        {
            selected_min=min_value;
            selected_max=max_value;
        }

        datumRangeSeekBar.setSelectedMaxValue(selected_max);
        datumRangeSeekBar.setSelectedMinValue(selected_min);
        Double final_max = max_value;
        datumRangeSeekBar.setOnRangeSeekBarChangeListener((bar, minValuedata, maxValuedata) -> {
            try{
                if(preferences.getSelectedUnit().equalsIgnoreCase(FT)){
                    range_min.setText(ftStringFormat(Double.parseDouble(String.valueOf(minValuedata))));
                    range_max.setText(ftStringFormat(Double.parseDouble(String.valueOf(maxValuedata))));
                    preferences.getSelectedValue().set(0,""+ftToCm(Double.parseDouble(String.valueOf(minValuedata))));
                    preferences.getSelectedValue().set(1,""+ftToCm(Double.parseDouble(String.valueOf(maxValuedata))));
                }else if(preferences.getSelectedUnit().equalsIgnoreCase(CM)){
                    range_min.setText(String.valueOf((int)Double.parseDouble(String.valueOf(minValuedata))));
                    range_max.setText(String.valueOf((int)Double.parseDouble(String.valueOf(maxValuedata))));
                    preferences.getSelectedValue().set(0,""+minValuedata);
                    preferences.getSelectedValue().set(1,""+maxValuedata);
                }else if(preferences.getSelectedUnit().equalsIgnoreCase(MI)){
                    range_min.setText(String.valueOf((int)Double.parseDouble(String.valueOf(minValuedata))));
                    range_max.setText(String.valueOf((int)Double.parseDouble(String.valueOf(maxValuedata))));
                    preferences.getSelectedValue().set(0,""+mileToKm(Double.parseDouble(String.valueOf(minValuedata))));
                    preferences.getSelectedValue().set(1,""+mileToKm(Double.parseDouble(String.valueOf(maxValuedata))));
                } else if(preferences.getSelectedUnit().equalsIgnoreCase(KM)){
                    range_min.setText(String.valueOf((int)Double.parseDouble(String.valueOf(minValuedata))));
                    range_max.setText(String.valueOf((int)Double.parseDouble(String.valueOf(maxValuedata))));
                    preferences.getSelectedValue().set(0,""+minValuedata);
                    preferences.getSelectedValue().set(1,""+maxValuedata);
                }
                else if(preferences.getSelectedUnit().equalsIgnoreCase("years")){
                    preferences.getSelectedValue().set(0,""+minValuedata);
                    preferences.getSelectedValue().set(1,""+maxValuedata);
                    range_min.setText(String.valueOf((int)Double.parseDouble(String.valueOf(minValuedata))));
                    if(Double.parseDouble(String.valueOf(maxValuedata)) == final_max) {
                        double maxValue = final_max;
                        range_max.setText(String.valueOf((int)maxValue).concat("+"));
                    }
                    else
                        range_max.setText(String.valueOf((int)Double.parseDouble(String.valueOf(maxValuedata))));
                }
            }
            catch (Exception e){
                //range_max.setText(getIntFromString(max_data));
            }
        });

        rang_tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                try {
                    preferences.setSelectedUnit(options_unit.get(tab.getPosition()));
                    Log.d(TAG, "onTabSelected: "+preferences.getSelectedUnit());
                    handleUnitChange(preferences,tab,datumRangeSeekBar,range_min,range_max);
                }catch (Exception e){
                    e.getMessage();
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                try {
                    preferences.setSelectedUnit(options_unit.get(tab.getPosition()));
                    Log.d(TAG, "onTabReselected: "+preferences.getSelectedUnit());
                    handleUnitReselected(preferences,tab,datumRangeSeekBar,range_min,range_max);
                }catch (Exception e){
                    e.getMessage();
                }
            }
        });
        if(preferences.getSelectedUnit().equalsIgnoreCase(CM)){
            rang_tab.getTabAt(0).select();
        }
        else if(preferences.getSelectedUnit().equalsIgnoreCase(FT)){
            rang_tab.getTabAt(1).select();
        }
        if(preferences.getSelectedUnit().equalsIgnoreCase(MI)){
            rang_tab.getTabAt(1).select();
        }
        else if(preferences.getSelectedUnit().equalsIgnoreCase(KM)){
            rang_tab.getTabAt(0).select();
        }
        return view;
    }


    private void handleUnitReselected(SearchPreference preferences,TabLayout.Tab tab,DatumRangeSeekBar datumRangeSeekBar,TextView range_min,TextView range_max ) {
        double minVal = Double.parseDouble(preferences.getOptionsValue().get(0));
        double maxVal = Double.parseDouble(preferences.getOptionsValue().get(1));
        if (preferences.getOptionsUnits().get(tab.getPosition()).equalsIgnoreCase(FT) ) {
            ArrayList<String> optionValues = preferences.getOptionsValue(); //not being change
            Double minOptionValue = cmToft(Double.parseDouble(optionValues.get(0)));
            Double maxOptionValue = cmToft(Double.parseDouble(optionValues.get(1)));
            datumRangeSeekBar.setRangeValues(minOptionValue, maxOptionValue);
            ArrayList<String> selectedValue = preferences.getSelectedValue();
            double minSelected = cmToft(Double.parseDouble(selectedValue.get(0)));
            double maxSelected = cmToft(Double.parseDouble(selectedValue.get(1)));
            range_min.setText(ftStringFormat(minSelected));
            range_max.setText(ftStringFormat(maxSelected));
            datumRangeSeekBar.setSelectedMinValue(minSelected);
            datumRangeSeekBar.setSelectedMaxValue(maxSelected);

        }else if(preferences.getOptionsUnits().get(tab.getPosition()).equalsIgnoreCase(CM)){   //default option unit

            ArrayList<String> optionValues = preferences.getOptionsValue(); //not being change
            Double minOptionValue = Double.parseDouble(optionValues.get(0));
            Double maxOptionValue = Double.parseDouble(optionValues.get(1));
            datumRangeSeekBar.setRangeValues(minOptionValue, maxOptionValue);
            ArrayList<String> selectedValue = preferences.getSelectedValue();
            double minSelected = Double.parseDouble(selectedValue.get(0));
            double maxSelected = Double.parseDouble(selectedValue.get(1));
            range_min.setText(String.valueOf((int)minSelected));
            range_max.setText(String.valueOf((int)maxSelected));
            datumRangeSeekBar.setSelectedMinValue(minSelected);
            datumRangeSeekBar.setSelectedMaxValue(maxSelected);

        } else if (preferences.getOptionsUnits().get(tab.getPosition()).equalsIgnoreCase(MI)) {
            ArrayList<String> optionValues = preferences.getOptionsValue(); //not being change
            double minOptionValue = kmToMile(Double.parseDouble(optionValues.get(0)));
            double maxOptionValue = kmToMile(Double.parseDouble(optionValues.get(1)));
            Log.d(TAG, "handleUnitReselected to MI: minOptionVal and maxOptionVal "+(int)minOptionValue+" , "+(int)maxOptionValue);
            datumRangeSeekBar.setRangeValues(minOptionValue, maxOptionValue);
            ArrayList<String> selectedValue = preferences.getSelectedValue();
            double minSelected = kmToMile(Double.parseDouble(selectedValue.get(0)));
            double maxSelected = kmToMile(Double.parseDouble(selectedValue.get(1)));
            range_min.setText(String.valueOf((int)minSelected));
            range_max.setText(String.valueOf((int)maxSelected));
            datumRangeSeekBar.setSelectedMinValue(minSelected);
            datumRangeSeekBar.setSelectedMaxValue(maxSelected);
        }
        else if(preferences.getOptionsUnits().get(tab.getPosition()).equalsIgnoreCase(KM)){  //default option unit
            ArrayList<String> optionValues = preferences.getOptionsValue(); //not being change
            double minOptionValue = Double.parseDouble(optionValues.get(0));
            double maxOptionValue = Double.parseDouble(optionValues.get(1));
            Log.d(TAG, "handleUnitReselected to KM: minOptionVal and maxOptionVal "+(int)minOptionValue+" , "+(int)maxOptionValue);
            datumRangeSeekBar.setRangeValues(minOptionValue, maxOptionValue);
            ArrayList<String> selectedValue = preferences.getSelectedValue();
            double minSelected = Double.parseDouble(selectedValue.get(0));
            double maxSelected = Double.parseDouble(selectedValue.get(1));
            range_min.setText(String.valueOf((int)minSelected));
            range_max.setText(String.valueOf((int)maxSelected));
            datumRangeSeekBar.setSelectedMinValue(minSelected);
            datumRangeSeekBar.setSelectedMaxValue(maxSelected);
        }
    }

    private double feetToInch(double val) {
        try {
            return val * 12;
        }catch (Exception e){
            return val;
        }
    }

    private String ftStringFormat(double ftValue) {
        //format 5'1"
        try {
            int firstNum = (int) ftValue;
            int secondNum= (int) feetToInch(ftValue - firstNum);
            return firstNum + "'" + secondNum + '"';
        }catch (Exception e){
            e.getMessage();
            return String.valueOf(ftValue);
        }
    }

    private void handleUnitChange(SearchPreference preferences,TabLayout.Tab tab,DatumRangeSeekBar datumRangeSeekBar,TextView range_min,TextView range_max ) {
        double minVal = Double.parseDouble(preferences.getOptionsValue().get(0));
        double maxVal = Double.parseDouble(preferences.getOptionsValue().get(1));
        if (preferences.getOptionsUnits().get(tab.getPosition()).equalsIgnoreCase(FT)) {
            Double minDoubleVal = cmToft(Double.valueOf(minVal));
            Double maxDoubleVal = cmToft(Double.valueOf(maxVal));
            datumRangeSeekBar.setRangeValues(minDoubleVal,maxDoubleVal);
            ArrayList<String> selectedValue = preferences.getSelectedValue();
            double minSelected = cmToft(Double.parseDouble(selectedValue.get(0)));
            double maxSelected = cmToft(Double.parseDouble(selectedValue.get(1)));
            range_min.setText(ftStringFormat(minSelected));
            range_max.setText(ftStringFormat(maxSelected));
        } else if (preferences.getOptionsUnits().get(tab.getPosition()).equalsIgnoreCase(CM)) {
            double minDoubleVal = Double.valueOf(minVal);  //option value by default in M so no need to convert
            double maxDoubleVal = Double.valueOf(maxVal);
            datumRangeSeekBar.setRangeValues(minDoubleVal, maxDoubleVal);
            ArrayList<String> selectedValue = preferences.getSelectedValue();
            double minSelected = Double.parseDouble(selectedValue.get(0));
            double maxSelected = Double.parseDouble(selectedValue.get(1));
            range_min.setText(String.valueOf((int)minSelected));
            range_max.setText(String.valueOf((int)maxSelected));
        } else if (preferences.getOptionsUnits().get(tab.getPosition()).equalsIgnoreCase(MI)) {
            double minDoubleVal = kmToMile(Double.valueOf(minVal));
            double maxDoubleVal = kmToMile(Double.valueOf(maxVal));
            Log.d(TAG, "handleUnitChange to MI: minOptionVal and maxOptionVal "+(int)minDoubleVal+" , "+(int)maxDoubleVal);
            datumRangeSeekBar.setRangeValues((int)minDoubleVal,(int)maxDoubleVal);
            ArrayList<String> selectedValue = preferences.getSelectedValue();
            double minSelected = kmToMile(Double.parseDouble(selectedValue.get(0)));
            double maxSelected = kmToMile(Double.parseDouble(selectedValue.get(1)));
            range_min.setText(String.valueOf((int)minSelected));
            range_max.setText(String.valueOf((int)maxSelected));
        } else if (preferences.getOptionsUnits().get(tab.getPosition()).equalsIgnoreCase(KM)) {
            double minDoubleVal = Double.valueOf(minVal);  //option value by default in KM
            double maxDoubleVal = Double.valueOf(maxVal);
            Log.d(TAG, "handleUnitChange to KM: minOptionVal and maxOptionVal "+(int)minDoubleVal+" , "+(int)maxDoubleVal);
            datumRangeSeekBar.setRangeValues((int)minDoubleVal,(int)maxDoubleVal);
            ArrayList<String> selectedValue = preferences.getSelectedValue();
            double minSelected = Double.parseDouble(selectedValue.get(0));
            double maxSelected = Double.parseDouble(selectedValue.get(1));
            range_min.setText(String.valueOf((int)minSelected));
            range_max.setText(String.valueOf((int)maxSelected));
        }
    }

    private double cmToft(double val) {
        try {
            return val / 30;
        }catch (Exception e){
            return val;
        }
    }

    private double ftToCm(double val) {
        try {
            return val * 30;
        }catch (Exception e){
            return val;
        }
    }

    private double kmToMile(double val) {
        try {
            return  val * 0.62;
        }catch (Exception e){
            return val;
        }
    }

    private double mileToKm(double val) {
        try {
            return  val / 0.62;
        }catch (Exception e){
            return val;
        }
    }

    /*
     * Convert int data*/
    private String getIntFromString(Object number_Text)
    {
        String number_data;
        try
        {
            number_data= String.valueOf(number_Text);
        }catch (Exception e)
        {
            e.printStackTrace();
            number_data="0";
        }
        return number_data;
    }

    /*
     * Getting the custom adapter.*/
    private View getTabView(String  text)
    {
        @SuppressLint("InflateParams")
        View tab = LayoutInflater.from(activity).inflate(R.layout.preference_range_tab, null);
        TextView tab_text=tab.findViewById(R.id.tab_txt);
        tab_text.setText(text);
        return tab;
    }

    public View getLocationView(String locationName, MySearchPrefPresenter presenter) {
        setClickCallback(presenter);
        @SuppressWarnings("InflateParams")
        View locationView = LayoutInflater.from(activity).inflate(R.layout.preference_location_view,null);
        TextView locationTitle = locationView.findViewById(R.id.location_text);
        TextView myLocationTitle = locationView.findViewById(R.id.my_current_location_text);
        TextView selectedLocation = locationView.findViewById(R.id.selected_location_text);
        selectedLocation.setText(locationName);
        locationTitle.setTypeface(typeFaceManager.getCircularAirLight());
        myLocationTitle.setTypeface(typeFaceManager.getCircularAirBook());
        selectedLocation.setTypeface(typeFaceManager.getCircularAirLight());
        LinearLayout llLocation = locationView.findViewById(R.id.ll_location);
//        llLocation.setOnClickListener(v -> {
//            if(clickCallback != null)
//                clickCallback.onLocationClick();
//        });
        RelativeLayout rlRoot = locationView.findViewById(R.id.root);
        rlRoot.setOnClickListener(v -> {
            if(clickCallback != null)
                clickCallback.onLocationClick();
        });
        return locationView;
    }
}
