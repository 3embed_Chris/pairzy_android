package com.pairzy.com.userProfile.ImageItem;

import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * @since  4/4/2018.
 * @version 1.0.
 * @author 3Embed.
 */
@Module
public interface ImageItemBuilder
{
    @FragmentScoped
    @Binds
    ImageItemFrg getProspectItemFragment(ImageItemFrg likesMeFrg);

    @FragmentScoped
    @Binds
    ImageItemContract.Presenter taskPresenter(ImageItemPresenter presenter);
}
