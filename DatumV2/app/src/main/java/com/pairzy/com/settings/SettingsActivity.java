package com.pairzy.com.settings;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;

import com.pairzy.com.selectLanguage.SelectLanguageActivity;
import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.appbar.AppBarLayout;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.splash.SplashActivity;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.webPage.WebActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h>Settings activity</h>
 * <p> This Settings activity user settings and Logout and Delete account function.</p>
 *
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public class SettingsActivity extends BaseDaggerActivity implements SettingsContract.View{

    @Inject
    SettingsPresenter presenter;
    @Inject
    TypeFaceManager typeFaceManager;

    @BindView(R.id.tv_sort)
    TextView tvSort;
    @BindView(R.id.page_title)
    TextView tvPageTitle;
    @BindView(R.id.tv_appbar_title)
    TextView tvAppbarTitle;
    @BindView(R.id.tv_community_title)
    TextView tvCommunityTitle;
    @BindView(R.id.tv_community_title_one)
    TextView tvCommunityTitleOne;
    @BindView(R.id.tv_community_title_two)
    TextView tvCommunityTitleTwo;
    @BindView(R.id.tv_legal_title)
    TextView tvLegal;
    @BindView(R.id.tv_privacy_policy_title)
    TextView tvPrivacyPolicy;
    @BindView(R.id.tv_terms_of_service_title)
    TextView tvTermsOfService;
    @BindView(R.id.tv_license_title)
    TextView tvLicense;
    @BindView(R.id.btn_logout)
    Button btnLogout;
    @BindView(R.id.tv_app_version)
    TextView tvAppVersion;
    @BindView(R.id.btn_delete_account)
    Button btnDeleteAccount;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.tv_select_language_title)
    TextView tvSelectLanguageTitle;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        presenter.init();
        initView();
        presenter.loadAppVersion();
    }

    private void initView() {
        tvSelectLanguageTitle.setTypeface(typeFaceManager.getCircularAirBook());

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if(scrollRange == -1){
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if(scrollRange + verticalOffset == 0){
                    isShow = true;
                    showToolbarTitle(true);
                } else if(isShow){
                    isShow = false;
                    showToolbarTitle(false);
                }
            }
        });
    }

    @OnClick(R.id.tv_select_language_title)
    public void selectLanguage(){
        Intent intent = new Intent(SettingsActivity.this, SelectLanguageActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    private void showToolbarTitle(boolean show) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if(show){
                    tvAppbarTitle.setVisibility(View.VISIBLE);
                }
                else {
                    tvAppbarTitle.setVisibility(View.GONE);
                }
            }
        });
    }
    @Override
    public void applyFont() {
        tvSort.setTypeface(typeFaceManager.getCircularAirBook());
        tvPageTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvAppbarTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvCommunityTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvCommunityTitleOne.setTypeface(typeFaceManager.getCircularAirBook());
        tvCommunityTitleTwo.setTypeface(typeFaceManager.getCircularAirBook());
        tvLegal.setTypeface(typeFaceManager.getCircularAirBold());
        tvPrivacyPolicy.setTypeface(typeFaceManager.getCircularAirBook());
        tvTermsOfService.setTypeface(typeFaceManager.getCircularAirBook());
        tvLicense.setTypeface(typeFaceManager.getCircularAirBook());
        btnLogout.setTypeface(typeFaceManager.getCircularAirBook());
        tvAppVersion.setTypeface(typeFaceManager.getCircularAirBook());
        btnDeleteAccount.setTypeface(typeFaceManager.getCircularAirBook());
    }

    @Override
    public void showSplashScreen() {
        Intent intent=new Intent(SettingsActivity.this, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void showAppVersion(String appVersion) {
        String versionStr = getResources().getString(R.string.version_text).concat(" "+appVersion);
        tvAppVersion.setText(versionStr);
    }


    @Override
    public void launchWebActivity(String title, @NonNull String url) {
        Intent intent = new Intent(SettingsActivity.this, WebActivity.class);
        intent.putExtra("url",url);
        intent.putExtra("title",title);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);

    }

    @Override
    public void finishActivity() {
        this.finish();
    }

    @OnClick(R.id.tv_community_title_one)
    public void communityGuideline(){
        presenter.launchWebActivity(getString(R.string.community_guideline),getString(R.string.url_community_guideline));
    }
    @OnClick(R.id.tv_community_title_two)
    public void safetyTips(){
        presenter.launchWebActivity(getString(R.string.safety_tips),getString(R.string.url_safety));
    }
    @OnClick(R.id.tv_privacy_policy_title)
    public void privacyPolicy(){
        presenter.launchWebActivity(getString(R.string.privacy_policy_title),getString(R.string.urlPrivacyPolicy));
    }
    @OnClick(R.id.tv_terms_of_service_title)
    public void termsOfService(){
        presenter.launchWebActivity(getString(R.string.terms_of_service),getString(R.string.urlTermsOfService));
    }
    @OnClick(R.id.tv_license_title)
    public void license(){
        presenter.launchWebActivity(getString(R.string.license_title),getString(R.string.url_license));
    }
    @OnClick(R.id.btn_logout)
    public void logout(){
        presenter.loadLogoutDialog();
    }
    @OnClick(R.id.btn_delete_account)
    public void deleteAccount(){
        presenter.loadDeleteAccountDialog();
    }
    @OnClick(R.id.close_button)
    public void close(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        this.finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        presenter.dispose();
    }
}
