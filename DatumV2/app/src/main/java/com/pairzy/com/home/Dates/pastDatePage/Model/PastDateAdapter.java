package com.pairzy.com.home.Dates.pastDatePage.Model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pairzy.com.R;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.home.Dates.Model.LoadMoreDateViewHolder;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import java.util.ArrayList;
/**
 * <h2>PastDateAdapter</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class PastDateAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private static final int LOADING_ITEM = 0, PAST_DATE_ITEM = 1;
    private static final String ACCEPTED = "accepted", DENIED = "denied", EXPIRE = "";
    private ArrayList<PastDateListPojo> list;
    private TypeFaceManager typeFaceManager;
    private ItemActionCallBack callBack;
    private Utility utility;

    public void setCallBack(ItemActionCallBack callBack) {
        this.callBack = callBack;
    }

    public PastDateAdapter(Utility utility, ArrayList<PastDateListPojo> list, TypeFaceManager typeFaceManager)
    {
        this.utility = utility;
        this.list=list;
        this.typeFaceManager=typeFaceManager;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view;
        switch (viewType){
            case LOADING_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.load_more_layout,parent,false);
                return new LoadMoreDateViewHolder(view,callBack,typeFaceManager);
            case PAST_DATE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.past_date_item,parent,false);
                return new PastDateItemViewHolder(view,callBack,typeFaceManager);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.past_date_item,parent,false);
                return new PastDateItemViewHolder(view,callBack,typeFaceManager);
        }
    }

    @Override
    public int getItemViewType(int position) {
        PastDateListPojo pastDateListPojo= list.get(position);
        if(!pastDateListPojo.isLoading()){
            return PAST_DATE_ITEM;
        }
        else{
            return LOADING_ITEM;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        switch (holder.getItemViewType())
        {
            case LOADING_ITEM:
                try
                {
                    loadingView((LoadMoreDateViewHolder) holder);
                }catch (Exception e){}
                break;
            case PAST_DATE_ITEM:
                try
                {
                    handelDataView((PastDateItemViewHolder) holder);
                }catch (Exception e){}
                break;
            default:
                try
                {
                    handelDataView((PastDateItemViewHolder) holder);
                }catch (Exception e){}
        }
    }

    /*
     *Handling the loading view */
    private void loadingView(LoadMoreDateViewHolder holder)
    {
        int position=holder.getAdapterPosition();
        PastDateListPojo data=  list.get(position);
        if(data.isLoadingFailed())
        {
            holder.setFailed();
        }else
        {
            holder.showLoadingLoadingAgain();
        }
    }


    private void handelDataView(PastDateItemViewHolder holder2)
    {
        int position=holder2.getAdapterPosition();
        PastDateListPojo pastDateListPojo=  list.get(position);
        if(pastDateListPojo==null)
            return;
        holder2.simpleDraweeView.setImageURI(pastDateListPojo.getOpponentProfilePic());
        holder2.user_name.setText(utility.formatString(pastDateListPojo.getOpponentName()));
        holder2.date_name.setText(utility.formatString(pastDateListPojo.getStatus()));
        holder2.time_view.setText(utility.time_converter(pastDateListPojo.getProposedOn()>0?pastDateListPojo.getProposedOn():0));

        if(pastDateListPojo.getOpponentResponse().equals(ACCEPTED)) {
            holder2.flRating.setVisibility(View.VISIBLE);
            if (pastDateListPojo.getRating() == 0) {
                holder2.rlRateTheDate.setVisibility(View.VISIBLE);
            } else {
                holder2.ratingBar.setRating(pastDateListPojo.getRating());
                holder2.rlRateTheDate.setVisibility(View.GONE);
            }
        }
        else{
            holder2.flRating.setVisibility(View.GONE);
        }
    }
    @Override
    public int getItemCount() {
        return list.size();
    }
}
