package com.pairzy.com.boostLikes;

import android.app.Activity;
import android.view.View;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.boostLikes.Model.BoostLikeAdapter;
import com.pairzy.com.boostLikes.Model.BoostLikeAdapterCallback;
import com.pairzy.com.boostLikes.Model.BoostLikeData;
import com.pairzy.com.boostLikes.Model.BoostLikeModel;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.CustomObserver.CoinBalanceObserver;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.SpendCoinDialog.CoinDialogCallBack;
import com.pairzy.com.util.progressbar.LoadingProgress;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by ankit on 7/9/18.
 */


public class BoostLikePresenter implements BoostLikeContract.Presenter ,BoostLikeAdapterCallback,CoinDialogCallBack{

    @Inject
    BoostLikeContract.View view;
    @Inject
    NetworkService service;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    BoostLikeModel model;
    @Inject
    Activity activity;
    @Inject
    NetworkStateHolder holder;
    @Inject
    BoostLikeAdapter adapter;
    @Inject
    CoinDialog spendCoinDialog;
    @Inject
    CoinBalanceObserver coinBalanceObserver;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    LoadingProgress loadingProgress;

    private CompositeDisposable compositeDisposable;
    private BoostLikeData lastPerformed;
    private String userId = null;
    private int currentPosition;

    @Inject
    public BoostLikePresenter() {
        compositeDisposable = new CompositeDisposable();
    }


    @Override
    public void initAdapterListener()
    {
        if(view!=null)
            view.adapterListener(this);
    }

    @Override
    public void getLikeList() {

        if(holder.isConnected()) {
            service.getBoostLikeList(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                try {
                                    model.parseLikeList(value.body().string());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            else if(value.code() == 204){
                                try {
                                    if (view != null)
                                        view.emptyData();
                                } catch (Exception e) {
                                }
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(model.getError(value));
                                } catch (Exception e) {
                                }
                            }
                            isEmpty();
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null)
                                view.showError(activity.getString(R.string.no_internet_error));
                            isEmpty();
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
            isEmpty();
        }
    }

    @Override
    public void dropView() {
        compositeDisposable.dispose();
    }


    private void launchLikesTimerDialog() {
        //launchBoostDialog(false,true);
        //TODO : saprate dialog
        if(view != null)
            view.showError("Like over try after some time!!");
    }

    @Override
    public void onLike(int position) {
        if(holder.isConnected())
        {
            BoostLikeData item=adapter.removeItem(position);
            if(item!=null) {
                this.lastPerformed = item;
                doLiked(item.getId());
            }
            isEmpty();
        }else
        {
            //Show toast
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    private void doLiked(final String user_id)
    {

        if (holder.isConnected()) {
            service.doLikeService(dataSource.getToken(), model.getLanguage(), model.setUserDetails(user_id))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            try {
                                if (value.code() == 405) {
                                    if (view != null)
                                        view.showMessage("Already liked!!");
                                } else if (value.code() == 401) {
                                    AppController.getInstance().appLogout();
                                } else if (value.code() != 200 && value.code() != 201) {
                                    if (value.code() == 409) {
                                        /**
                                         * Open boost dialog
                                         */
                                        launchLikesTimerDialog();
                                    }
                                    onRevertAction(user_id, true);
                                } else {
                                    if (value.code() == 200) {
                                        model.parseLikeResponse(value.body().string());
                                    }
                                    onRevertAction(user_id, false);
                                }
                            } catch (Exception e) {
                                if (view != null)
                                    view.showError(e.getMessage());
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            onRevertAction(user_id, true);
                            if (view != null)
                                view.showError(activity.getString(R.string.server_error));
                        }

                        @Override
                        public void onComplete() {
                        }

                    });
        } else {
            if (view != null)
                view.showError(activity.getString(R.string.internet_error_Text));
        }
    }


    private void onRevertAction(String user_id, boolean error) {
        if(lastPerformed!=null&&lastPerformed.getId().equals(user_id))
        {
            if(error)
            {
                if(view!=null)
                {
                    revertAction(lastPerformed);
                }
            }
            lastPerformed=null;
        }
    }

    private void revertAction(BoostLikeData item){
        if(item!=null)
        {
            model.addDataInPosition(item);
            isEmpty();
        }
    }

    private void isEmpty()
    {

        if(model.isListEmpty())
        {
            if(view!=null)
                view.emptyData();
        }
        else{
            if(view != null)
                view.onDataUpdate();
        }
    }


    private void doDislike(String user_id)
    {

        service.doUnLikeService(dataSource.getToken(),model.getLanguage(),model.setUserDetails(user_id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> value)
                    {
                        try
                        {

                            if(value.code() == 405) {
                                if (view != null) {
                                    view.showMessage("Already disliked!!");
                                }
                            }
                            else if(value.code() == 200)
                            {
                                onRevertAction(user_id,false);
                            } else if(value.code() == 401)
                            {
                                AppController.getInstance().appLogout();
                            } else
                            {
                                onRevertAction(user_id,true);
                            }
                        } catch (Exception e)
                        {
                            if(view!=null)
                                view.showError(e.getMessage());
                        }
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        onRevertAction(user_id,true);
                        if(view!=null)
                            view.showError(activity.getString(R.string.server_error));
                    }
                    @Override
                    public void onComplete() {}
                });

    }

    @Override
    public void onDislike(int position) {
        if(holder.isConnected())
        {
            BoostLikeData item=adapter.removeItem(position);
            if(item!=null) {
                this.lastPerformed = item;
                doDislike(item.getId());
            }
            isEmpty();
        }else
        {
            //Show toast
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    private void launchWalletEmptyDialog(){
        spendCoinDialog.showDialog("",true,"",activity.getString(R.string.empty_wallet_superlike_msg),"",this,true,false);
    }

    public void doSuperLike(String user_id)
    {
        service.doSupperLike(dataSource.getToken(),model.getLanguage(),model.setUserDetails(user_id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> value)
                    {
                        try
                        {
                            if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else if(value.code() == 405) {
                                if (view != null) {
                                    view.showMessage("Already superliked!!");
                                }
                            }
                            else if(value.code() != 200&&value.code() != 201)
                            {
                                if(value.code() == 402)
                                {
                                    try {
                                        if (view != null)
                                            view.showError(activity.getString(R.string.insufficient_balance_msg));
                                        /**
                                         * Opening wallet
                                         */
                                        launchWalletEmptyDialog();
                                    }catch (Exception ignored){}
                                }
                                onRevertAction(user_id,true);
                            }else
                            {
                                try {
                                    model.parseSuperLike(value.body().string());
//                                        if(view != null)
//                                            view.showCoinBalance(utility.formatCoinBalance(model.getCoinBalance()));
                                    coinBalanceObserver.publishData(true);
                                }catch (Exception e){}
                                onRevertAction(user_id,false);
//                                    if(view != null)
//                                        view.startCoinAnimation();
                            }
                        } catch (Exception e)
                        {
                            onRevertAction(user_id,true);
                        }
                    }

                    @Override
                    public void onError(Throwable e)
                    {
                        onRevertAction(user_id,true);
                        if(view!=null)
                            view.showError(activity.getString(R.string.server_error));
                    }

                    @Override
                    public void onComplete() {}
                });
    }


    private void callCoinConfigApi()
    {
        if(holder.isConnected())
        {
            loadingProgress.show();
            service.getCoinConfig(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if (value.code() == 200) {
                                try {
                                    model.parseCoinConfig(value.body().string());
                                    if (coinConfigWrapper.getCoinData() == null) {
                                        if (view != null)
                                            view.showError("failed to fetch coinConfig!!");
                                    } else {
                                        launchSuperlikeCoinDialog();
                                    }
                                } catch (Exception e) {
                                }
                            } else if(value.code() == 401)
                            {
                                AppController.getInstance().appLogout();
                            } else {
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                } catch (Exception e) {
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if (view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    private void loadSpendCoinDialog() {
        if(model.isDialogDontNeedToShow()){
            if(view != null)
                //doSuperLike(userId);
            initSuperlike();
        }
        else {
            if(coinConfigWrapper.getCoinData() != null) {
                launchSuperlikeCoinDialog();
            }
            else{
                callCoinConfigApi();
            }
        }
    }

    private void launchSuperlikeCoinDialog() {
        BoostLikeData boostLikeData = model.getBoostLikeItem(currentPosition);
        if(boostLikeData != null){
            int coinSpend = coinConfigWrapper.getCoinData().getSuperLike().getCoin();
            boolean isFemale = true;
            try {
               // isFemale = lastPerformed.getGender().equalsIgnoreCase("female");
            }catch (Exception e){}
            spendCoinDialog.showDialog(boostLikeData.getFirstName(),isFemale, activity.getString(R.string.superlike_spend_coin_title), "", String.valueOf(coinSpend), this, false, true);
        }
    }

    @Override
    public void onSuperLike(int position) {
        this.currentPosition = position;
        loadSpendCoinDialog();
    }

    private void initSuperlike(){
        if(holder.isConnected())
        {
            BoostLikeData item=adapter.removeItem(currentPosition);
            if(item!=null) {
                this.lastPerformed = item;
                doSuperLike(item.getId());
            }
            isEmpty();
        }else
        {
            //Show toast
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    @Override
    public void openUserDetails(int position, View view) {
    }

    /*
   * empty wallet dialog callback
   */
    @Override
    public void onCoinButton(boolean dontShowAgain) {
        initSuperlike();
    }

    /*
     * empty wallet dialog callback
     */
    @Override
    public void onBuyCoin() {
        if(view != null)
            view.launchCoinWallet();
    }
}
