package com.pairzy.com.home.Prospects.Online.Model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.util.DatumCallbacks.ListItemClick;
import com.pairzy.com.util.TypeFaceManager;
import com.facebook.drawee.view.SimpleDraweeView;
/**
 * <h2>OnlineListItemHolder</h2>
 * @since  3/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class OnlineListItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    public SimpleDraweeView simpleDraweeView;
    public TextView user_name;
    public TextView school_name;
    public TextView time_view;
    public LinearLayout end_view;
    private ListItemClick callBack;

    public OnlineListItemHolder(View itemView, TypeFaceManager typeFaceManager,ListItemClick callBack)
    {
        super(itemView);
        this.callBack=callBack;
        simpleDraweeView=itemView.findViewById(R.id.user_profile_pic);
        user_name=itemView.findViewById(R.id.user_name);
        user_name.setTypeface(typeFaceManager.getCircularAirBold());
        school_name=itemView.findViewById(R.id.school_name);
        school_name.setTypeface(typeFaceManager.getCircularAirBook());
        time_view=itemView.findViewById(R.id.time_view);
        time_view.setTypeface(typeFaceManager.getCircularAirBook());
        end_view=itemView.findViewById(R.id.end_view);
        itemView.findViewById(R.id.parent_view).setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        if(callBack!=null)
            callBack.onClicked(view.getId(),this.getAdapterPosition());
    }
}
