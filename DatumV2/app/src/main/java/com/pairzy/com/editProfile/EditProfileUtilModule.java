package com.pairzy.com.editProfile;

import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.GridLayoutManager;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.editProfile.Model.ProfilePhotoAdapter;
import com.pairzy.com.editProfile.Model.ProfilePicture;
import com.pairzy.com.editProfile.ProfileAlert.ProfileAlertDialog;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.GridSpacingItemDecoration;
import com.pairzy.com.util.ImageChecker.ImproperImageAlert;
import com.pairzy.com.util.MediaBottomSelector;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.videocompressor.com.CompressImage;
import com.videocompressor.com.VideoCompressor;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * <h>EditPrefUtilModule class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

@Module
public class EditProfileUtilModule {

    @ActivityScoped
    @Provides
    LoadingProgress getLoadingProgress(Activity activity)
    {
        return new LoadingProgress(activity);
    }

    @Named("PIC_LIST")
    @ActivityScoped
    @Provides
    ArrayList<ProfilePicture> providePictureList()
    {
        return new ArrayList<ProfilePicture>();
    }

    @ActivityScoped
    @Provides
    ProfilePhotoAdapter provideProfilePhotoAdapter(Activity activity,@Named("PIC_LIST") ArrayList<ProfilePicture> profilePictures)
    {
        return new ProfilePhotoAdapter(activity,profilePictures);
    }

    @Provides
    @ActivityScoped
    GridLayoutManager provideGridLayoutManager(Activity activity)
    {
        return new GridLayoutManager(activity,3);
    }

    @Provides
    @ActivityScoped
    GridSpacingItemDecoration getGridSpacingItemDecoration(Utility utility)
    {
        return  new GridSpacingItemDecoration(3,utility.dpToPx(10), false);
    }

    @Provides
    @ActivityScoped
    MediaBottomSelector getMediaBottomSelector(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new MediaBottomSelector(activity,typeFaceManager);
    }

    @Provides
    @ActivityScoped
    App_permission getApp_permission(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new App_permission(activity,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    CompressImage compressImage()
    {
        return new CompressImage();
    }

    @ActivityScoped
    @Provides
    VideoCompressor getVideoCompressor(Context context)
    {
        return new VideoCompressor(context);
    }

    @ActivityScoped
    @Provides
    ImproperImageAlert getImproperImageAlert(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new ImproperImageAlert(activity,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    ProfileAlertDialog provideProfileAlertDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new ProfileAlertDialog(activity,typeFaceManager);
    }

}
