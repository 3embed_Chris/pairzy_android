package com.pairzy.com.home;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;

import com.pairzy.com.AppController;
import com.pairzy.com.MatchedView.Its_Match_alert;
import com.pairzy.com.R;
import com.pairzy.com.campaignScreen.CampaignActivity;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.UserActionHolder;
import com.pairzy.com.data.model.appversion.AppVersionData;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Discover.Model.UserAssetData;
import com.pairzy.com.home.Discover.UserActionEventError;
import com.pairzy.com.home.HomeModel.HomeDataModel;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.networking.RxNetworkObserver;
import com.pairzy.com.userProfile.UserProfilePage;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.CustomObserver.AdminCoinObserver;
import com.pairzy.com.util.CustomObserver.CoinBalanceObserver;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.SpendCoinDialog.CoinDialogCallBack;
import com.pairzy.com.util.SpendCoinDialog.CoinSpendDialogCallback;
import com.pairzy.com.util.SpendCoinDialog.WalletEmptyDialogCallback;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.boostDialog.BoostAlertCallback;
import com.pairzy.com.util.boostDialog.BoostDialog;
import com.pairzy.com.util.boostDialog.Slide;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.pairzy.com.util.timerDialog.TimerDialog;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.suresh.innapp_purches.InAppCallBack;
import com.suresh.innapp_purches.InnAppSdk;
import com.suresh.innapp_purches.SkuDetails;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * <h2>HomePresenter</h2>
 *
 * @since  2/27/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class HomePresenter implements HomeContract.Presenter,BoostAlertCallback,CoinDialogCallBack,
        CoinSpendDialogCallback, WalletEmptyDialogCallback
{
    private static final String TAG = HomePresenter.class.getSimpleName();

    private boolean toUpcomingFrom=false;
    @Inject
    BoostDialog boostDialog;
    @Inject
    RxNetworkObserver rxNetworkObserver;
    @Inject
    Activity activity;
    @Inject
    HomeContract.View view;
    @Inject
    HomeDataModel model;
    @Inject
    NetworkStateHolder holder;
    @Inject
    NetworkService service;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    ArrayList<Slide> slideArrayList;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    LoadingProgress loadingProgress;
    @Inject
    CoinBalanceObserver coinBalanceObserver;
    @Inject
    Utility utility;
    @Inject
    CoinDialog spendCoinDialog;
    @Inject
    Its_Match_alert match_alert;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    TimerDialog timerDialog;
    @Inject
    AdminCoinObserver adminCoinObserver;
    @Inject
    UserActionHolder userActionHolder;


    private UserActionEventError accetEventObserver;
    private CompositeDisposable compositeDisposable;
    private boolean needToUpdateChatList = false;
    private String userId;
    private boolean openPostFeedtab;

    @Inject
    HomePresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(Object view)
    {}

    @Override
    public void dropView() {
        compositeDisposable.clear();
    }


    @Override
    public void observeAdminCoinAdd() {
        adminCoinObserver.getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Boolean value) {
                        if(value){
                            loadCoinBalance();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void stopPlayer() {
        if(view != null)
            view.stopPlayer();
    }

    @Override
    public void observeCoinBalanceChange()
    {
        coinBalanceObserver.getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean value) {
                        if(value){
                            if (view != null)
                                view.showCoinBalance(utility.formatCoinBalance(model.getCoinBalance()));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {}

                    @Override
                    public void onComplete() {}
                });
    }



    @Override
    public void loadCoinBalance()
    {
        if(networkStateHolder.isConnected()) {
            service.getCoinBalance(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                try {
                                    model.parseCoinBalance(value.body().string());
                                } catch (Exception e) {
                                }
                            } else if (value.code() == 401) {
                                AppController.getInstance().appLogout();
                            } else {
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                } catch (Exception e) {
                                }
                            }
                            coinBalanceObserver.publishData(true);
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null)
                                view.showError(activity.getString(R.string.failed_to_get_wallet_balance));
                        }
                    });
        }
        else{
            if (view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }

    }

    @Override
    public void updateCoinBalance() {
        if(!TextUtils.isEmpty(coinBalanceHolder.getCoinBalance())){
            if (view != null)
                view.showCoinBalance(utility.formatCoinBalance(model.getCoinBalance()));
        }
        else{
            loadCoinBalance();
        }
    }


    @Override
    public void launchCoinWallet() {
        if(view != null)
            view.launchCoinWallet();
    }

    @Override
    public void setNeedToUpdateChat(boolean yes) {
        needToUpdateChatList = yes;
    }

    @Override
    public boolean isNeedToUpdateChatList() {
        return needToUpdateChatList;
    }


    @Override
    public void checkForProfileBoost() {
        handleAdShow();
        if(model.getBoostExpireTime() == -1) { //update boost data from server first time
            callGetBoostData(true);
        }
        else if(model.getBoostExpireTime() > System.currentTimeMillis()){  //boosted
            //show timer in dialog.
            showProfileBoostTimer();
        }
        else{
            //patch api.
            if(model.isEnoughCoinForBoostProfile()) {
                launchSpendCoinForBoost();
                //callBoostApi()
            }
            else{
                launchWalletEmptyDialogForBoost();
            }
        }
    }

    @Override
    public void checkOnlyForProfileBoost() {
        if(model.getBoostExpireTime() == -1) { //update boost data from server first time
            callGetBoostData(false);
        }
        else if(model.getBoostExpireTime() > 0 && model.getBoostExpireTime() > System.currentTimeMillis()){
            if(view != null)
                view.showBoostViewCounter(true);
        }
        else{
            if(view != null)
                view.showBoostViewCounter(false);
        }
    }

    @Override
    public void openProspectScreen() {
        if(view != null)
            view.openProspectScreen();
    }

    @Override
    public void updateUnreadChatBadgeCount(String unreadChatCount) {
        if(view != null)
            view.showUnreadChatCount(unreadChatCount, TextUtils.isEmpty(unreadChatCount));
    }

    @Override
    public void updatePendingDateBadgeCount(String unreadPendingDateCount) {
        if(view != null)
            view.showPendingDateCount(unreadPendingDateCount, TextUtils.isEmpty(unreadPendingDateCount));
    }

    @Override
    public boolean isFeedtabNeedToOpen() {
        return openPostFeedtab;
    }

    @Override
    public void setFeedTabOpen(boolean feedTabOpen) {
        this.openPostFeedtab = feedTabOpen;
    }


    private void launchSpendCoinForBoost() {
        int coinSpend = coinConfigWrapper.getCoinData().getBoostProfileForADay().getCoin();
        String title = String.format(Locale.ENGLISH,"Spend %s coins to boost your profile.",coinSpend);
        String btnText = String.format(Locale.ENGLISH, " I am OK to spend %s coins", coinSpend);
        spendCoinDialog.showCoinSpendDialog(title,"",btnText,this);
    }

    private void showProfileBoostTimer() {
        if(model.getBoostExpireTime() > System.currentTimeMillis()){  //boosted
            //show timer in dialog
            launchProfileBoostDialog();
        }
        else{
            //patch api
            callBoostApi();
        }
    }

    private void launchProfileBoostDialog() {
        //launchBoostDialog(true,false);
        timerDialog.showDialog(true);
        if(view != null)
            view.showBoostViewCounter(true);
    }


    private void callGetBoostData(boolean showLoading) {
        if(holder.isConnected()) {
            if(showLoading)
                loadingProgress.show();
            service.getBoostData(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                try {
                                    String response = value.body().string();
                                    Log.d(TAG, "onSuccess: boostDataResponse: "+response);
                                    model.parseGetBoostApiData(response);
                                    if(showLoading)
                                        showProfileBoostTimer();
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                            else if(value.code() == 204){
                                //this profile not boosted
                                if(showLoading)
                                    callBoostApi();
                            }
                            loadingProgress.cancel();
                        }

                        @Override
                        public void onError(Throwable e) {
                            if(view != null)
                                view.showError(activity.getString(R.string.server_error));
                            loadingProgress.cancel();
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    private void callBoostApi() {
        if(holder.isConnected()) {
            loadingProgress.show();
            service.boostProfile(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if(value.code() == 200){
                                try{
                                    String response = value.body().string();
                                    Log.d(TAG, "onSuccess: boostProfile: "+response);
                                    model.parseBoostResponse(response);

                                    coinBalanceObserver.publishData(true);
                                    if(view != null)
                                        view.startCoinAnimation();
                                    if(view != null)
                                        view.showBoostViewCounter(true);

                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                            else if(value.code() == 402){
                                try {
                                    launchWalletEmptyDialogForBoost();
                                }catch (Exception ignored){}
                            }
                            loadingProgress.cancel();
                        }

                        @Override
                        public void onError(Throwable e) {
                            if(view != null)
                                view.showError(activity.getString(R.string.server_error));
                            loadingProgress.cancel();
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void openFragmentPage(FragmentTransaction ft,DaggerFragment fragment)
    {
        if(view!=null)
            view.moveFragment(ft,fragment);
    }

    @Override
    public void showMessage(String message)
    {
        if(view!=null)
            view.showMessage(message);
    }

    @Override
    public void showError(String error)
    {
        if(view!=null)
            view.showError(error);
    }

    @Override
    public void initNetworkObserver()
    {
        if(view!=null)
            view.updateInterNetStatus(holder.isConnected());

        Observer<NetworkStateHolder> observer = new Observer<NetworkStateHolder>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {
                compositeDisposable.add(d);
            }
            @Override
            public void onNext(NetworkStateHolder value)
            {
                if(view!=null)
                    view.updateInterNetStatus(value.isConnected());
            }
            @Override
            public void onError(Throwable e)
            {
                e.printStackTrace();
            }
            @Override
            public void onComplete()
            {}
        };
        rxNetworkObserver.getObservable().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }


    @Override
    public void doLiked(boolean fromUserProfile, final String user_id)
    {
        handleAdShow();
        if(model.getRemainsLikeCount() > 0 || fromUserProfile) {

            if (holder.isConnected()) {
                service.doLikeService(dataSource.getToken(), model.getLanguage(), model.setUserDetails(user_id))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Response<ResponseBody>>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                compositeDisposable.add(d);
                            }

                            @Override
                            public void onNext(Response<ResponseBody> value) {
                                try {
                                    if (value.code() == 405) {
                                        if (view != null)
                                            view.showMessage("Already liked!!");
                                    } else if (value.code() == 401) {
                                        AppController.getInstance().appLogout();
                                    } else if (value.code() != 200 && value.code() != 201) {
                                        if (value.code() == 409) {
                                            /**
                                             * Open timer dialog
                                             */
                                            //launchLikesTimerDialog();
                                        }
                                        if(!fromUserProfile) {
                                            if (accetEventObserver != null)
                                                accetEventObserver.onRevertAction(new UserAssetData(user_id, true));
                                        }
                                    } else {
                                        if (value.code() == 200) {
                                            model.parseLikeResponse(value.body().string());

                                        }
                                        if(!fromUserProfile) {
                                            if (accetEventObserver != null)
                                                accetEventObserver.onRevertAction(new UserAssetData(user_id, false));

                                        }
                                    }
                                } catch (Exception e) {
                                    if (view != null)
                                        view.showError(e.getMessage());
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                if(!fromUserProfile) {
                                    if (accetEventObserver != null)
                                        accetEventObserver.onRevertAction(new UserAssetData(user_id, true));

                                }
                                if (view != null)
                                    view.showError(activity.getString(R.string.server_error));
                            }

                            @Override
                            public void onComplete() {
                            }
                        });
            } else {
                if (view != null)
                    view.showError(activity.getString(R.string.internet_error_Text));
            }
        }
        else{
            launchLikesTimerDialog();
            if (accetEventObserver != null)
                accetEventObserver.onRevertAction(new UserAssetData(user_id, true));
        }
    }

    private void launchLikesTimerDialog() {
        timerDialog.showDialog(false);
    }

    @Override
    public void doDislike(String user_id)
    {
        handleAdShow();
        if(holder.isConnected())
        {
            service.doUnLikeService(dataSource.getToken(),model.getLanguage(),model.setUserDetails(user_id))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> value)
                        {
                            try
                            {

                                if(value.code() == 405) {
                                    if (view != null) {
                                        view.showMessage("Already disliked!!");
                                    }
                                }
                                else if(value.code() == 200)
                                {
                                    if(accetEventObserver!=null)
                                        accetEventObserver.onRevertAction(new UserAssetData(user_id,false));
                                } else if(value.code() == 401)
                                {
                                    AppController.getInstance().appLogout();
                                } else
                                {
                                    if(accetEventObserver!=null)
                                        accetEventObserver.onRevertAction(new UserAssetData(user_id,true));
                                }
                            } catch (Exception e)
                            {
                                if(view!=null)
                                    view.showError(e.getMessage());
                            }
                        }
                        @Override
                        public void onError(Throwable e)
                        {
                            if(accetEventObserver!=null)
                                accetEventObserver.onRevertAction(new UserAssetData(user_id,true));
                            if(view!=null)
                                view.showError(activity.getString(R.string.server_error));
                        }
                        @Override
                        public void onComplete() {}
                    });
        }else
        {
            if(view!=null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    private void launchWalletEmptyDialogForBoost(){
        spendCoinDialog.showWalletEmptyDialog(activity.getString(R.string.wallet_empty_title),activity.getString(R.string.empty_wallet_profile_boost_msg),this);
    }

    private void launchWalletEmptyDialogForSuperLike(){
        spendCoinDialog.showWalletEmptyDialog(activity.getString(R.string.wallet_empty_title),activity.getString(R.string.empty_wallet_superlike_msg),this);
    }

    @Override
    public void doSuperLike(String user_id)
    {
        handleAdShow();
        if(holder.isConnected())
        {
            service.doSupperLike(dataSource.getToken(), model.getLanguage(), model.setUserDetails(user_id))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            try {
                                if (value.code() == 401) {
                                    AppController.getInstance().appLogout();

                                } else if (value.code() != 200 && value.code() != 201) {
                                    if (value.code() == 402) {
                                        try {
                                            launchWalletEmptyDialogForSuperLike();
                                        } catch (Exception ignored) {
                                        }
                                    }
                                    else if (value.code() == 405) {
                                        if (view != null) {
                                            view.showMessage(activity.getString(R.string.already_superliked));
                                        }
                                    }
                                    if (accetEventObserver != null)
                                        accetEventObserver.onRevertAction(new UserAssetData(user_id, true));

                                } else {  //success
                                    try {
//                                        if(model.removeUserProfile(user_id)){
//
//                                        }
                                        userId = null;
                                        model.parseSuperLike(value.body().string());
                                        coinBalanceObserver.publishData(true);
                                        if (view != null)
                                            view.startCoinAnimation();
                                    } catch (Exception e) {
                                    }

                                    if (accetEventObserver != null)
                                        accetEventObserver.onRevertAction(new UserAssetData(user_id, false));
                                }
                            } catch (Exception e) {
                                if (accetEventObserver != null)
                                    accetEventObserver.onRevertAction(new UserAssetData(user_id, true));
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (accetEventObserver != null)
                                accetEventObserver.onRevertAction(new UserAssetData(user_id, true));
                            if (view != null)
                                view.showError(activity.getString(R.string.server_error));
                        }

                        @Override
                        public void onComplete() {
                        }
                    });

        }else
        {
            if(view!=null)
                view.showError(activity.getString(R.string.internet_error_Text));
        }
    }


    /*
     *On Handel result */
    @Override
    public boolean onHandelActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode== AppConfig.PROFILE_REQUEST&&resultCode== Activity.RESULT_OK)
        {
            Bundle result_data=data.getExtras();
            if(result_data!=null&&result_data.containsKey(AppConfig.USER_ID))
            {
                String user_id=result_data.getString(AppConfig.USER_ID);
                int result_action=result_data.getInt(AppConfig.RESULT_DATA);
                if(result_action==AppConfig.ON_SUPER_LIKE)
                {
                    doSuperLike(user_id);
                }else if(result_action==AppConfig.ON_LIKE)
                {
                    doLiked(true,user_id);
                }else if(result_action==AppConfig.ON_DISLIKE)
                {
                    doDislike(user_id);
                }
                else if(result_action==AppConfig.ON_CHAT)
                {
                    setNeedToUpdateChat(true);
                }
                return true;
            }
            return false;
        }else
        {
            return false;
        }
    }

    @Override
    public void openBoostDialog()
    {
        launchBoostDialog();
    }


    private void launchBoostDialog()
    {
        if(!model.isSubsPlanEmpty())
        {
            model.selectMiddleItem();
            boostDialog.showAlert(this,model.getSubsPlanList(), slideArrayList);
        }
        else {
            getSubsPlan();
        }
    }


    private void getSubsPlan()
    {
        if(networkStateHolder.isConnected())
        {
            loadingProgress.show();
            service.getSubsPlans(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value)
                        {
                            if(value.code() == 200)
                            {
                                try
                                {
                                    model.parseSubsPlanList(value.body().string());
                                    if (model.isSubsPlanEmpty())
                                    {
                                        loadingProgress.cancel();
                                        if(view != null)
                                            view.showError(activity.getString(R.string.subs_list_empty));
                                    }else
                                    {
                                        model.selectMiddleItem();
                                        List<String> actual_ids=model.collectsIds();
                                        InnAppSdk.getVendor().getProductDetails(activity,actual_ids, InnAppSdk.Type.SUB, new InAppCallBack.DetailsCallback()
                                        {
                                            @Override
                                            public void onSuccess(List<SkuDetails> list,List<String> errorList)
                                            {
                                                loadingProgress.cancel();
                                                if(list.size()>0)
                                                {
                                                    model.updateDetailsData(list);

                                                    if(!model.isSubsPlanEmpty())
                                                    {
                                                        model.selectMiddleItem();
                                                        boostDialog.showAlert(HomePresenter.this,model.getSubsPlanList(), slideArrayList);
                                                    }

                                                }else
                                                {
                                                    model.clearProductList();
                                                    if(view != null)
                                                        view.showError(activity.getString(R.string.subs_list_empty));
                                                }
                                            }
                                        });
                                    }
                                }catch (Exception e)
                                {
                                    loadingProgress.cancel();
                                    if(view != null)
                                        view.showError(activity.getString(R.string.subs_list_empty));
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else
                            {
                                loadingProgress.cancel();
                            }
                        }
                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if (view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    public void checkForDynamicLink(Intent intent)
    {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(intent)
                .addOnSuccessListener(activity, pendingDynamicLinkData -> {
                    // Get deep link from result (may be null if no link is found)
                    Uri deepLink = null;
                    try{
                        if(pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            //Log.d("exe","deepLink"+deepLink);
                            int start = deepLink.toString().lastIndexOf("/");
                            int end = deepLink.toString().length();
                            String profileId = deepLink.toString().substring(start + 1, end);
                            //Log.d("exe","deepLinkpostId"+profileId);
                            openProfile(profileId);
                        }
                    }catch (Exception e){e.printStackTrace();}
                })
                .addOnFailureListener(activity,new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //Log.d("exe","getDynamicLink"+e.getMessage());

                    }
                });
    }


    private void openProfile(String profileId)
    {
        if(dataSource.getSubscription() != null) {
            if (!TextUtils.isEmpty(profileId)) {
                Intent intent = new Intent(activity, UserProfilePage.class);
                intent.putExtra("profile_id", profileId);
                if (view != null)
                    view.openProfile(intent);
            }
        }
        else{
            openBoostDialog();
        }
    }


    private void callSubscriptionApi(String id)
    {
        if(networkStateHolder.isConnected())
        {
            loadingProgress.show();
            Map<String, Object> body = new HashMap<>();
            body.put(ApiConfig.SubsPlan.PLAN_ID,id);
            body.put(ApiConfig.SubsPlan.PAYMENT_GETWAY_TAX_ID,String.valueOf(System.currentTimeMillis()));
            body.put(ApiConfig.SubsPlan.PAYMENT_GETWAY,AppConfig.DEFAULT_PAYMENT_GETWAY);
            body.put(ApiConfig.SubsPlan.USER_PURCHASE_TIME,String.valueOf(System.currentTimeMillis()));
            service.postSubscription(dataSource.getToken(), model.getLanguage(), body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value)
                        {
                            loadingProgress.cancel();

                            if(value.code() == 200)
                            {
                                try{
                                    String  responnse=value.body().string();
                                    model.parseSubscription(responnse);
                                    if(view != null)
                                        view.showMessage(activity.getString(R.string.plan_purchase_successful));
                                }catch (Exception e)
                                {
                                    e.printStackTrace();
                                    if(view != null)
                                        view.showError(activity.getString(R.string.something_worng_text));
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception ignored){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view != null)
                                view.showError(activity.getString(R.string.api_server_error));
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }

    }

    @Override
    public void onInappSubscribe(int position)
    {
        String purchaseId=model.getSubsPurchaseId(position);
        if(!TextUtils.isEmpty(purchaseId))
        {
            InnAppSdk.getVendor().purchasedItem(activity, purchaseId, InnAppSdk.Type.SUB, dataSource.getUserId(), new InAppCallBack.OnPurchased()
            {
                @Override
                public void onSuccess(String productId)
                {
                    String id=model.extractIDFromKey(productId);
                    if(TextUtils.isEmpty(id))
                    {
                        if(view!=null)
                            view.showError("Error on updating !");
                    }else
                    {
                        callSubscriptionApi(id);
                    }
                }

                @Override
                public void onError(String id, String error)
                {
                    if(view != null)
                        view.showError(error);
                }
            });
        }else
        {
            if(view != null)
                view.showError("Error on purchasing "+purchaseId);
        }
    }


    @Override
    public void onNoThanks()
    {}

    public void subscribeToFirebaseTopic()
    {
        AppController.getInstance().subscribeToFirebaseTopic();
    }

    @Override
    public void parseIntent(Intent intent)
    {
        Bundle data = intent.getBundleExtra("data");
        if( data != null)
        {
            int type = data.getInt("type", -1);
            Log.w("Type", ""+type);
            //swipe more Its a Match dialog click;
            if(type == 72){
                if(view != null)
                    view.selectTab(0);
            }else if(type == 71){
                if(view != null){
                    String userId = data.getString("user_id");
                    if(!TextUtils.isEmpty(userId))
                        openProfile(userId);
                }
            }else if(type == 61){  //audio call


            }else if(type == 32){ //post like, comment and create
                setFeedTabOpen(true);
                if(view != null)
                    view.selectTab(3);
            } else if(type == 51){
                match_alert.show_info_alert(activity, data.getString("user_image")
                        ,data.getString("user_id")
                        ,data.getString("user_name"),data.getString("chat_id"),data.getBoolean("is_superliked"));

                if(view != null){ //move to match
                    view.selectTab(3);
                }
            }
            else if(type == 30) { //campaign
                if (view != null) {
                    Intent intent1 = new Intent(activity, CampaignActivity.class);
                    intent1.putExtra("data", data);
                    view.launchCampaignScreen(intent1);
                }
            }
            else if(type == 1){ //match
                if(view != null){
                    view.selectTab(3);
                }
            }
            else if(type == 2){  //mutual like
                if(view != null){
                    String targetId = data.getString("target_id");
                    if(!TextUtils.isEmpty(targetId))
                        openProfile(targetId);
                }
            }
            else if(type == 7 || type == 8 || type == 9){  //mutual like
                if(type==7)
                    toUpcomingFrom=true;
                if(view != null){
                    view.selectTab(2);
                }
            }
            else if(type == 41){  //chat notif
                if(view != null){
                        view.selectTab(3);
                }
            }
        }
        else
        {
            try {
                checkForDynamicLink(intent);
            }catch (Exception e){}
            Log.w("111","fcm intent");
            Log.w("222", String.valueOf(data));
            //Log.w("333",data.getString("key"));
        }
    }

    @Override
    public void setRevertCallback(UserActionEventError callback)
    {
        accetEventObserver=callback;
    }


    /*
     * empty wallet dialog callback
     */
    @Override
    public void onCoinButton(boolean dontShowAgain) {

    }

    /*
     * empty wallet dialog callback
     */
    @Override
    public void onBuyCoin() {
        if(view != null)
            view.launchCoinWallet();
    }

    @Override
    public void onOkToSpendCoin(boolean dontShowAgain) {
        callBoostApi();
    }

    /*
     * empty wallet dialog callback
     */
    @Override
    public void onByMoreCoin() {
        if(view != null)
            view.launchCoinWallet();
    }

    @Override
    public void handleAdShow() {
        userActionHolder.incrementUserActionCount();
        if( userActionHolder.canShowAds()){
            if(view != null)
                view.showLoadedProfileAds();
        }
    }

    @Override
    public void showLoadedProfileAds() {
        if(view != null)
            view.showLoadedProfileAds();
    }

    @Override
    public void checkForForceUpdate() {
        if(model.isNeedToCallUpdateVersionApi()) {
            service.getAppVersion()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> responseBodyResponse) {
                            if (responseBodyResponse.code() == 200) {
                                try {
                                    AppVersionData appVersionData = model.parseAppVersionData(responseBodyResponse.body().string());
                                    if (model.shouldShowUpdateDialog(appVersionData)) {
                                        dataSource.setUpdateDialogShowed(true);
                                        if (view != null)
                                            view.showUpdateDialog(model.isMandatory(appVersionData));
                                    }else{
                                        if(view != null)
                                            view.dismissUpdateDialog();
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                        }
                    });
        }else{
            if (model.shouldShowUpdateDialog()) {
                if (view != null)
                    view.showUpdateDialog(model.getSavedIsMandatory());
            }
        }
    }

    @Override
    public void updateDiscoveryProfilePicture() {
        if(view != null)
            view.updateDiscoveryProfilePicture();
    }
    @Override
    public boolean checkToUpcomingFrom()
    {
        return toUpcomingFrom;
    }

    @Override
    public void openAppSettingPage() {
        if(view != null)
            view.launchAppSettingsPage();
    }
}
