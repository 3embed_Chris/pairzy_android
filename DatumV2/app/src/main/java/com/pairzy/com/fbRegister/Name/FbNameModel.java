package com.pairzy.com.fbRegister.Name;

import android.text.TextUtils;

import com.pairzy.com.BaseModel;
import com.pairzy.com.util.AppConfig;

import java.util.regex.Pattern;

import javax.inject.Inject;

/**
 * <h2>FbNameModel</h2>
 * <p>
 * ListModel of @{@link FbNameFragment}
 * </P>
 *
 * @author 3Embed.
 * @version 1.0.
 * @since 06/01/2018.
 **/
class FbNameModel extends BaseModel
{
    @Inject
    FbNameModel() {}

    /**
     * <p>Checks given name is valid or not<p/>
     * @param charSequence user text input.
     */
    public boolean isValidName(CharSequence charSequence)
    {
        return checkForEmpty(charSequence) && Pattern.matches(AppConfig.USERNAME_REGEX,charSequence);
    }

    private boolean checkForEmpty(CharSequence charSequence)
    {
        return !TextUtils.isEmpty(charSequence.toString());
    }

}
