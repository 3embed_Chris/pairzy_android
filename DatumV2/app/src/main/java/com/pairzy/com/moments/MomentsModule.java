package com.pairzy.com.moments;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>MomentsModule</h2>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */
@Module
public abstract class MomentsModule {

     public static final String MOMENTS = "moments";

    @Binds
    @ActivityScoped
    abstract MomentsContract.Presenter providepresenter(MomentsPresenter momentsPresenter);

    @Binds
    @ActivityScoped
    abstract MomentsContract.View provideView(MomentsActivity momentsActivity);

    @Binds
    @ActivityScoped
    abstract Activity provideActivity(MomentsActivity momentsActivity);


}
