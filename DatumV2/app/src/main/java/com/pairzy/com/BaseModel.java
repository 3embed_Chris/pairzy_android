package com.pairzy.com;

import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.AppConfig;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * <h2>BaseModel</h2>
 * <p>
 *</P>
 * @author 3Embed.
 * @version 1.0.
 * @since 06/01/2018.
 **/
public class BaseModel
{
    /**
     * @param result : API response
     * @return String message
     */
    public String getMessage(ResponseBody result) {
        try {
            JSONObject object = new JSONObject(result.string());
            return object.getString(ApiConfig.MESSAGE);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getMessage(Response<ResponseBody> result)
    {
        try {
            JSONObject object = new JSONObject(result.errorBody().string());
            return object.getString(ApiConfig.MESSAGE);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }


    public String getError(Response<ResponseBody> result)
    {
        try {
            JSONObject object = new JSONObject(result.errorBody().string());
            return object.getString(ApiConfig.MESSAGE);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * @return String authorization key
     */
    public String getAuthorization()
    {
        return AppConfig.AUTH_KEY;
    }

    /**
     * @return String language code
     */
    public String getLanguage() {
        return AppConfig.DEFAULT_LANGUAGE;
    }


}
