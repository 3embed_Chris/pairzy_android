package com.pairzy.com.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

@SerializedName("planId")
@Expose
private String planId;
@SerializedName("cost")
@Expose
private Integer cost;
@SerializedName("durationInMonths")
@Expose
private Integer durationInMonths;
@SerializedName("planName")
@Expose
private String planName;
@SerializedName("purchaseDate")
@Expose
private Integer purchaseDate;
@SerializedName("purchaseTime")
@Expose
private Integer purchaseTime;
@SerializedName("userPurchaseTime")
@Expose
private Integer userPurchaseTime;
@SerializedName("expiryTime")
@Expose
private Integer expiryTime;

public String getPlanId() {
return planId;
}

public void setPlanId(String planId) {
this.planId = planId;
}

public Integer getCost() {
return cost;
}

public void setCost(Integer cost) {
this.cost = cost;
}

public Integer getDurationInMonths() {
return durationInMonths;
}

public void setDurationInMonths(Integer durationInMonths) {
this.durationInMonths = durationInMonths;
}

public String getPlanName() {
return planName;
}

public void setPlanName(String planName) {
this.planName = planName;
}

public Integer getPurchaseDate() {
return purchaseDate;
}

public void setPurchaseDate(Integer purchaseDate) {
this.purchaseDate = purchaseDate;
}

public Integer getPurchaseTime() {
return purchaseTime;
}

public void setPurchaseTime(Integer purchaseTime) {
this.purchaseTime = purchaseTime;
}

public Integer getUserPurchaseTime() {
return userPurchaseTime;
}

public void setUserPurchaseTime(Integer userPurchaseTime) {
this.userPurchaseTime = userPurchaseTime;
}

public Integer getExpiryTime() {
return expiryTime;
}

public void setExpiryTime(Integer expiryTime) {
this.expiryTime = expiryTime;
}

}