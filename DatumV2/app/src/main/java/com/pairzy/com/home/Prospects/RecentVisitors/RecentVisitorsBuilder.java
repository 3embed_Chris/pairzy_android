package com.pairzy.com.home.Prospects.RecentVisitors;
import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * @since  3/23/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface RecentVisitorsBuilder
{
    @FragmentScoped
    @Binds
    RecentVisitorsFrg getProspectItemFragment(RecentVisitorsFrg recentVisitorsFrg);

    @FragmentScoped
    @Binds
    RecentVisitorsContract.Presenter taskPresenter(RecentVisitorsPresenter presenter);
}
