package com.pairzy.com.planCallDate;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.data.model.DateEvent;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Dates.Model.DateType;
import com.pairzy.com.home.Dates.Pending_page.PendingFrgPresenter;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.planCallDate.model.CallDateModel;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.CalendarEventHelper;
import com.pairzy.com.util.CustomObserver.CoinBalanceObserver;
import com.pairzy.com.util.CustomObserver.DateObserver;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.SpendCoinDialog.CoinSpendDialogCallback;
import com.pairzy.com.util.SpendCoinDialog.WalletEmptyDialogCallback;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h>CallDatePresenter class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public class CallDatePresenter implements CallDateContract.Presenter,SingleDateAndTimePickerDialog.Listener,App_permission.Permission_Callback,
        WalletEmptyDialogCallback, CoinSpendDialogCallback {

    private final String CALENDER_TAG = "calender_tag";

    @Inject
    CallDateContract.View view;
    @Inject
    SingleDateAndTimePickerDialog.Builder dateTimeDialog;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    NetworkService service;
    @Inject
    CallDateModel model;
    @Inject
    LoadingProgress loadingProgress;
    @Inject
    Activity activity;
    @Inject
    DateObserver dateObserver;
    @Inject
    CalendarEventHelper calendarEventHelper;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    CoinDialog spendCoinDialog;
    @Inject
    App_permission app_permission;
    @Inject
    CoinBalanceObserver coinBalanceObserver;

    private String userId;
    private String userImage;
    private String userName ="";
    private int dateTypeInt = 3; //videoDate.
    private Long selectedTime = 0L;
    private DateListPojo date_data;
    private String rescheduleTag;
    private boolean isAudioDate = false;
    private CompositeDisposable compositeDisposable;


    @Inject
    public CallDatePresenter(){
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void init() {
        if(view != null) {
            view.applyFont();
        }
    }

    @Override
    public void launchDateTimePicker() {
        dateTimeDialog.listener(this).display();
    }

    @Override
    public void initData(Intent intent) {
        rescheduleTag = intent.getStringExtra("reschedule");
        if(rescheduleTag != null && rescheduleTag.equals(PendingFrgPresenter.RESCHEDULE_TAG)){
            date_data = (DateListPojo) intent.getSerializableExtra("date_data");
            if(date_data != null) {
                if(date_data.getDateType() == DateType.AUDIO_DATE.getValue()){ //audio
                    isAudioDate =  true;
                }
                else{  //video
                    isAudioDate = false;
                }
                if (view != null) {
                    view.initUserData(date_data,isAudioDate);
                }
            }

        }
        else {
            userId = intent.getStringExtra("user_id");
            userImage = intent.getStringExtra("user_image");
            userName = intent.getStringExtra("user_name");
            dateTypeInt = intent.getIntExtra("date_type",3);
            if(dateTypeInt == DateType.AUDIO_DATE.getValue()) {//audio
                isAudioDate = true;
            }
            else{    //video
                isAudioDate = false;
            }
            if (userImage != null && userName != null & userId != null) {
                if(view != null)
                    view.initUserData(userName, userImage,isAudioDate);
            }
        }
        String yourName = dataSource.getName();
        String profilePic = dataSource.getProfilePicture();
        if( yourName!= null && profilePic != null ){
            view.initOwnData(yourName,profilePic);
        }
    }


    @Override
    public void onDateSelected(Date date) {
        if(view != null) {
            try {
                SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd @ hh:mm a");
                Calendar calendar = Calendar.getInstance();
                selectedTime = date.getTime();
                calendar.setTimeInMillis(selectedTime);
                view.showSelectedDate(formatter.format(calendar.getTime()));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    private void launchWalletEmptyDialog(){
        if(isAudioDate) {
            spendCoinDialog.showWalletEmptyDialog(activity.getString(R.string.wallet_empty_title),activity.getString(R.string.audio_date_empty_wallet_msg),this);
        }
        else{
            spendCoinDialog.showWalletEmptyDialog(activity.getString(R.string.wallet_empty_title), activity.getString(R.string.video_date_empty_wallet_msg),this);
        }
    }

    private void callDateApi() {
        if (rescheduleTag != null && rescheduleTag.equals(PendingFrgPresenter.RESCHEDULE_TAG) && date_data != null) {
            if (view != null) {
                if (selectedTime != 0) {
                    if(!isValidDate(selectedTime))
                        return;
                    date_data.setProposedOn(selectedTime);
                }
                view.returnFinalData(date_data);
            }
            return;
        }

        if (model.isMissingData(userId, selectedTime))
            return;

//        if(!isValidDate(selectedTime)){
//            return;
//        }

        Map<String, Object> mapBody = model.getBodyMap(userId, selectedTime,isAudioDate);

        if(networkStateHolder.isConnected()) {
            loadingProgress.show();
            service.planDate(dataSource.getToken(), model.getLanguage(), mapBody)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if (value.code() == 200) {
                                try {
                                    model.parseCallDateResponse(value.body().string());
                                    coinBalanceObserver.publishData(true);
                                } catch (Exception e) {}
                                if (view != null)
                                    view.showMessage(R.string.date_plan_successful);
                                dateObserver.publishData(true);
                                checkForCalenderPermission();
                            } else if (value.code() == 401) {
                                AppController.getInstance().appLogout();
                            }
                            else if(value.code() == 402){
                                launchWalletEmptyDialog();
                            }
                            else {
                                try {
                                    if (view != null)
                                        view.showError(model.getErrorMessage(value.errorBody().string()));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            e.printStackTrace();
                            if (view != null)
                                view.showError(activity.getString(R.string.failed_to_schedule_date));
                        }

                        @Override
                        public void onComplete() {
                        }

                    });
        }else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    private boolean isValidDate(Long _selecteTime) {
        boolean isValid = true;
        if(System.currentTimeMillis() > _selecteTime){ //less than 2 hour
            if (view != null)
                view.showError(activity.getString(R.string.invalid_date_time_msg));
//            else if((System.currentTimeMillis()+120*60*1000) > _selecteTime){
//                if (view != null)
//                    view.showError(activity.getString(R.string.invalid_date_time_specific_msg));
//            }
//            else{
//                if (view != null)
//                    view.showError(activity.getString(R.string.invalid_date_time_genric_msg));
//            }
            isValid = false;
        }
        return isValid;
    }

    public void checkForCalenderPermission() {

        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.READ_CALENDER);
        permissions.add(App_permission.Permission.WRITE_CALENDER);
        app_permission.getPermission(CALENDER_TAG,permissions,this);
    }

    private void setDateReminder(Long dateReminder) {
        String personName = "";
        String location = "";
        String _userId = "";
        String dateType = "Video Date";
        if(dateTypeInt == DateType.AUDIO_DATE.getValue()) {
            dateType = "Audio Date";
        }

        if(date_data != null) {
            personName = date_data.getOpponentName();
            userId = date_data.getOpponentId();
        }
        else{
            personName = userName;
            _userId = userId;
        }

        deletePreviousReminder(userId);
        int eventId = calendarEventHelper.addDateReminder(dateReminder,personName,location,dateType);
        DateEvent dateEvent = new DateEvent();
        dateEvent.setUserId(_userId);
        dateEvent.setEventId(String.valueOf(eventId));
        model.saveEventId(dateEvent);
        finishActivity();
    }

    private void finishActivity(){
        activity.finish();
    }

    private void deletePreviousReminder(String userId) {
        String eventId = model.checkIfReminderExist(userId);
        if(!TextUtils.isEmpty(eventId)){
            calendarEventHelper.deleteEvent(eventId);
        }
    }


    public void loadCoinDialog() {
        if(model.isEnoughWalletBalance(isAudioDate)) {
            if (model.isDialogNeedToShow(isAudioDate, rescheduleTag)) {
                launchSpendCoinDialog();
            } else {
                callDateApi();
            }
        }
        else{
            //show not enough coin
            launchWalletEmptyDialog();
        }
    }

    private void launchSpendCoinDialog() {
        try {
            if(rescheduleTag != null && rescheduleTag.equals(PendingFrgPresenter.RESCHEDULE_TAG)){
                Integer coinSpend = coinConfigWrapper.getCoinData().getResheduleDate().getCoin();
                String btnText = String.format(Locale.ENGLISH, " I am OK to spend %s coins", coinSpend);
                spendCoinDialog.showCoinSpendDialog(activity.getString(R.string.spend_coin_on_reschedule_date_dialog_title),
                        activity.getString(R.string.spend_coin_on_reschedule_date_dialog_msg),
                        btnText,this);
            }
            else {
                if (isAudioDate) {
                    Integer coinSpend = coinConfigWrapper.getCoinData().getAudioDateInitiate().getCoin();
                    String title = String.format(Locale.ENGLISH,"Spend %d coins to setup a %s with %s.",coinSpend,activity.getString(R.string.Audio_date),userName);
                    String btnText = String.format(Locale.ENGLISH, " I am OK to spend %s coins", coinSpend);
                    spendCoinDialog.showCoinSpendDialog(title,"", btnText,this);
                } else {
                    Integer coinSpend = coinConfigWrapper.getCoinData().getVideoDateInitiate().getCoin();
                    String title = String.format(Locale.ENGLISH,"Spend %d coins to setup a %s with %s.",coinSpend,activity.getString(R.string.Video_date),userName);
                    String btnText = String.format(Locale.ENGLISH, " I am OK to spend %s coins", coinSpend);
                    spendCoinDialog.showCoinSpendDialog(title,"",btnText,this);
                }
            }
        }catch (Exception e){}
    }


    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        setDateReminder(selectedTime);
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        app_permission.show_Alert_Permission(activity.getString(R.string.calender_access_text),activity.getString(R.string.calender_access_subtitle),
                activity.getString(R.string.location_acess_message),stringArray);
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean parmanent) {
        finishActivity();
    }

    public void dispose() {
        compositeDisposable.clear();
    }

    @Override
    public void checkForValidInput() {
        if(selectedTime == 0L){
            if(view != null)
                view.showMessage(activity.getString(R.string.empty_date_time_msg));
        }else{
            loadCoinDialog();
        }
    }

    /*
     * empty wallet dialog callback
     */
    @Override
    public void onByMoreCoin() {
        if(view != null)
            view.launchCoinWallet();
    }

    /*
     * on ok to spend coin dialog callback
     */
    @Override
    public void onOkToSpendCoin(boolean dontShowAgain) {
        callDateApi();
        if(rescheduleTag != null && rescheduleTag.equals(PendingFrgPresenter.RESCHEDULE_TAG)){
            model.updateRescheduleSpendCoinShowPref(!dontShowAgain); //false means dont show
        }
        else {
            model.updateSpendCoinShowPref(!dontShowAgain, isAudioDate);  //false means dont showC
        }
    }
}
