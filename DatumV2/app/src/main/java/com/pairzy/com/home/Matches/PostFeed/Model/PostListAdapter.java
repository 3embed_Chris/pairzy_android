package com.pairzy.com.home.Matches.PostFeed.Model;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

/**
 * <h2>PostGridListAdapter</h2>
 * <P>
 *
 * </P>
 * @since  3/17/2018.
 * @author 3Embed.
 * @version 1.0.
 */

public class PostListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private static final  String TAG = PostListAdapter.class.getName();
    private TypeFaceManager typeFaceManager;
    private PostFeedIemCallBack adapterItemCallback;
    private ItemViewCallBack callBack;
    private ArrayList<PostListPojo> list;
    private PreferenceTaskDataSource dataSource;
    private Utility utility;

    private Context context;
    private RequestManager requestManager;

    public void setItemCallback(PostFeedIemCallBack callback) {
        this.adapterItemCallback=callback;
    }

    public PostListAdapter(RequestManager requestManager,PreferenceTaskDataSource dataSource,Utility utility, ArrayList<PostListPojo> list, TypeFaceManager typeFaceManager) {
        this.dataSource = dataSource;
        this.typeFaceManager=typeFaceManager;
        this.requestManager = requestManager;
        this.list=list;
        this.utility = utility;
        intiCallback();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_list_item,parent,false);
        context=parent.getContext();
        return new PostListViewHolder(view,typeFaceManager,callBack);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        PostListViewHolder  postListViewHolder= (PostListViewHolder) holder;
        postListViewHolder.onBind(list.get(position),requestManager);

        if(list.get(position).getLiked())
            postListViewHolder.postLike.setImageResource(R.drawable.islike);
        else
            postListViewHolder.postLike.setImageResource(R.drawable.ic_like);

        postListViewHolder.sdvPostPreview.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    postListViewHolder.postLiked.setVisibility(View.VISIBLE);
                    if(!list.get(position).getLiked())
                        if (adapterItemCallback != null)
                            adapterItemCallback.onLikePost(position);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            postListViewHolder.postLiked.setVisibility(View.GONE);
                        }
                    }, 2000);

                    return super.onDoubleTap(e);
                }
            });



            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TEST", "Raw event: " + event.getAction() + ", (" + event.getRawX() + ", " + event.getRawY() + ")");
                gestureDetector.onTouchEvent(event);
                return true;
            }

        });
        handleViewData(postListViewHolder,position);
    }


    private void handleViewData(PostListViewHolder postListViewHolder, int i) {
        postListViewHolder.profileName.setTypeface(typeFaceManager.getCircularAirBold());
        postListViewHolder.tvPostTime.setTypeface(typeFaceManager.getCircularAirLight());
        postListViewHolder.tvPostLikeCount.setTypeface(typeFaceManager.getCircularAirBold());
        postListViewHolder.tvPostCommentViewAll.setTypeface(typeFaceManager.getCircularAirBold());
        postListViewHolder.tvUpdatedBio.setTypeface(typeFaceManager.getCircularAirBook());

        postListViewHolder.profileName.setText(list.get(i).getUserName());
        postListViewHolder.tvPostLikeCount.setText(String.format("%s %s", list.get(i).getLikeCount(), context.getString(R.string.likes_text_lower)));
        postListViewHolder.tvPostCommentViewAll.setText(String.format("%s %s", list.get(i).getCommentCount(), context.getString(R.string.comments_text)));
        postListViewHolder.profilePic.setImageURI(list.get(i).getProfilePic());

        if(list.get(i).getType().equalsIgnoreCase("match"))
        { postListViewHolder.PostMatch.setVisibility(View.VISIBLE);
            postListViewHolder.postLike.setVisibility(View.GONE);
            postListViewHolder.postLiked.setVisibility(View.GONE);
            postListViewHolder.tvPostLikeCount.setVisibility(View.GONE);
            postListViewHolder.postComment.setVisibility(View.GONE);
            postListViewHolder.tvPostCommentViewAll.setVisibility(View.GONE);

        }
        else { postListViewHolder.PostMatch.setVisibility(View.GONE);
            postListViewHolder.postLike.setVisibility(View.VISIBLE);
            postListViewHolder.postLiked.setVisibility(View.GONE);
            postListViewHolder.tvPostLikeCount.setVisibility(View.VISIBLE);
            postListViewHolder.postComment.setVisibility(View.VISIBLE);
            postListViewHolder.tvPostCommentViewAll.setVisibility(View.VISIBLE);
        }
        postListViewHolder.tvUpdatedBio.setText(list.get(i).getDescription());
        postListViewHolder.tvPostTime.setText(utility.getdate(list.get(i).getPostedOn()));

        if(dataSource.getUserId().equals(list.get(i).getUserId()))
            postListViewHolder.profileMessage.setVisibility(View.GONE);
        else
            postListViewHolder.profileMessage.setVisibility(View.VISIBLE);
        if(list.get(i).isPairSuccess()) {
            postListViewHolder.sdvPostPreviewPair.setVisibility(View.VISIBLE);
        } else {
            postListViewHolder.sdvPostPreviewPair.setVisibility(View.GONE);
        }
        ArrayList<String> postUrls = list.get(i).getUrl();
        if(list.get(i).getTypeFlag().equalsIgnoreCase("4")) {
            postListViewHolder.Bio.setVisibility(View.VISIBLE);
            postListViewHolder.BioText.setVisibility(View.VISIBLE);
            postListViewHolder.Bio.setText(list.get(i).getDescription());
            postListViewHolder.tvUpdatedBio.setText("");
            postListViewHolder.bioBlurBackground.setVisibility(View.VISIBLE);
        }
        else {
            postListViewHolder.Bio.setVisibility(View.GONE);
            postListViewHolder.BioText.setVisibility(View.GONE);
            postListViewHolder.bioBlurBackground.setVisibility(View.GONE);
        }

        if(postUrls != null && !postUrls.isEmpty()){
            String postUrlPreview = postUrls.get(0);
            postUrlPreview = postUrlPreview.replace(".mp4",".jpg").replace(".mov","");
            postListViewHolder.sdvPostPreview.setImageURI(postUrlPreview);

            if(postUrls.size()>1&&list.get(i).isPairSuccess()){
                String postUrlPreviewPair = postUrls.get(1);
                postUrlPreviewPair = postUrlPreviewPair.replace(".mp4",".jpg").replace(".mov","");
                postListViewHolder.sdvPostPreviewPair.setImageURI(postUrlPreviewPair);
            }

        }

        if(dataSource.getUserId().equalsIgnoreCase(list.get(i).getUserId()))
            postListViewHolder.flDeletePost.setVisibility(View.VISIBLE);
        else
            postListViewHolder.flDeletePost.setVisibility(View.GONE);


        //        //TODO setting adapter
//        PostSubAdapter subAdapter=new PostSubAdapter(list.get(i).getUrl(),null);
//        postListViewHolder.rvPostlistPost.setAdapter(subAdapter);
//        postListViewHolder.rvPostlistPost.setLayoutManager(new LinearLayoutManager(context,RecyclerView.HORIZONTAL,false));
//        SnapHelper startSnapHelper = new StartSnapHelper();
//        postListViewHolder.rvPostlistPost.setOnFlingListener(null);
//        startSnapHelper.attachToRecyclerView(postListViewHolder.rvPostlistPost);
//        subAdapter.notifyDataSetChanged();

    }

    private void intiCallback() {
        Log.e(TAG, "intiCallback: " );
        callBack = (view, position) -> {
            {
                switch (view.getId()) {
                    case R.id.tvPostlistViewAll:
                        if (adapterItemCallback != null)
                            adapterItemCallback.onViewAllComment(position);
                        break;

                    case R.id.ivPostlistPostComment:
                        if (adapterItemCallback != null)
                            adapterItemCallback.onComment(position);
                        break;

                    case R.id.rvPostlistPost:
                        /*if(list.get(position).getLiked())
                            if (adapterItemCallback != null)
                                adapterItemCallback.onLikePost(position);
*/
                        break;
                    case R.id.ivPostlistPostMessage:
                        adapterItemCallback.onChatMessage(position);
                        break;

                    case R.id.ivPostlistPostLike:
                        if(list.get(position).getLiked())
                        {
                            if (adapterItemCallback != null)
                                adapterItemCallback.onUnlikePost(position);

                        }
                        else{
                            if (adapterItemCallback != null)
                                adapterItemCallback.onLikePost(position);
                        }
                        break;

                    case R.id.tvPostlistLikedCount:
                        if (adapterItemCallback != null)
                            adapterItemCallback.onViewAllLikes(position);
                        break;
                    case R.id.fl_delete_button:
                        if(adapterItemCallback != null)
                            adapterItemCallback.onPostDelete(position);
                }
            }

        };
    }

    @Override
    public int getItemCount() { return list.size(); }

}
