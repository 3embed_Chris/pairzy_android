package com.pairzy.com.MySearchPreference.Model;

/**
 * Created by ankit on 26/5/18.
 */

public interface LocationClickCallback {
    void onLocationClick();
}
