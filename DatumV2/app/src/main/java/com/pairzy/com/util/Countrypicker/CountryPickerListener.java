package com.pairzy.com.util.Countrypicker;

public interface CountryPickerListener {
  void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID, int max);
}