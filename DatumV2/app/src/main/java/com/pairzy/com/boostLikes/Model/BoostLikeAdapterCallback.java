package com.pairzy.com.boostLikes.Model;

public interface BoostLikeAdapterCallback
{
    void onLike(int position);
    void onDislike(int position);
    void onSuperLike(int position);
    void openUserDetails(int position, android.view.View view);
//    void openChatScreen(UserItemPojo userItemPojo);
}