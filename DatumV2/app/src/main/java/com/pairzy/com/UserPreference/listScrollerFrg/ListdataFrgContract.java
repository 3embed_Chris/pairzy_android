package com.pairzy.com.UserPreference.listScrollerFrg;
import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

import java.util.ArrayList;

/**
 * @since  2/22/2018.
 */
public interface ListdataFrgContract
{
    interface View extends BaseView
    {
        void updateTheSelection(ArrayList<String> selected);
        void showMessage(String message);
        void showError(String message);
    }

    interface Presenter extends BasePresenter
    {
        boolean addSelection(String data);
        ArrayList<String> getMultiSelectedData();
        String getSingleSelectedData();
        boolean isDataExist();
        void removeSelection(String data);
        void addSingleSelection(String data);
        void  updatePreference(String pref_Id, int selectionType);

        void updateSelection(ArrayList<ListData> options_list_data, int seletionType);
    }

}
