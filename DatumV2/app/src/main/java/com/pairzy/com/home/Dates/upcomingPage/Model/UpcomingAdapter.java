package com.pairzy.com.home.Dates.upcomingPage.Model;

import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.R;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.home.Dates.Model.LoadMoreDateViewHolder;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

/**
 * <h2>PostGridListAdapter</h2>
 * <P>
 *
 * </P>
 * @since  3/17/2018.
 * @author 3Embed.
 * @version 1.0.
 */

public class UpcomingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private final int UPCOMING_ITEM = 1, LOADING_ITEM = 0;
    private ArrayList<DateListPojo> list_data;
    private TypeFaceManager typeFaceManager;
    private ItemActionCallBack callBack;
    private Utility utility;

    public void setCallBack(ItemActionCallBack callBack) {
        this.callBack = callBack;
    }

    public UpcomingAdapter(Utility utility, ArrayList<DateListPojo> list_data, TypeFaceManager typeFaceManager)
    {
        this.utility = utility;
        this.list_data = list_data;
        this.typeFaceManager = typeFaceManager;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view;
        switch (viewType) {
            case LOADING_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.load_more_layout, parent, false);
                return new LoadMoreDateViewHolder(view, callBack, typeFaceManager);
            case UPCOMING_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.upcoming_list_item, parent, false);
                return new UpcomingItemViewHolder(view, callBack, typeFaceManager);
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.upcoming_list_item, parent, false);
                return new UpcomingItemViewHolder(view, callBack, typeFaceManager);
        }
    }

    @Override
    public int getItemViewType(int position) {
        DateListPojo upcomingItemPojo = list_data.get(position);
        if(!upcomingItemPojo.isLoading()){
            return UPCOMING_ITEM;
        }
        else{
            return LOADING_ITEM;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        switch (holder.getItemViewType())
        {
            case LOADING_ITEM:
                try
                {
                    loadingView((LoadMoreDateViewHolder) holder);
                }catch (Exception e){}
                break;
            case UPCOMING_ITEM:
                try
                {
                    handelDataView((UpcomingItemViewHolder)holder);
                }catch (Exception e){}
                break;
            default:
                try
                {
                    handelDataView((UpcomingItemViewHolder)holder);
                }catch (Exception e){}
        }
    }


    @Override
    public int getItemCount()
    {
        return list_data.size();
    }

    /*
     *Handling the loading view */
    private void loadingView(LoadMoreDateViewHolder holder)
    {
        int position=holder.getAdapterPosition();
        DateListPojo data=  list_data.get(position);
        if(data.isLoadingFailed())
        {
            holder.setFailed();
        }else
        {
            holder.showLoadingLoadingAgain();
        }
    }


    private void handelDataView(UpcomingItemViewHolder holder2)
    {
        int position=holder2.getAdapterPosition();
        DateListPojo upcomingItemPojo=  list_data.get(position);
        if(upcomingItemPojo==null)
            return;
        holder2.user_profile_pic.setImageURI(Uri.parse(upcomingItemPojo.getOpponentProfilePic()));
        holder2.main_image_view.setImageURI(Uri.parse(upcomingItemPojo.getOpponentProfilePic()));
        holder2.name.setText(utility.formatString(upcomingItemPojo.getOpponentName()));

        if(upcomingItemPojo.getDateType() == 1){//video date
            upcomingItemPojo.setRequestedFor("Video Date");
        }
        else if(upcomingItemPojo.getDateType() == 2){ //phy date
            upcomingItemPojo.setRequestedFor("In Person Date");
        }
        else{ // audio date
            upcomingItemPojo.setRequestedFor("Audio Date");
        }

        holder2.user_message.setText(upcomingItemPojo.getRequestedFor());
        if(!TextUtils.isEmpty(upcomingItemPojo.getPlaceName())) {
            holder2.locationTv.setVisibility(View.VISIBLE);
            holder2.locationTv.setText(upcomingItemPojo.getPlaceName());
        }
        else{
            holder2.locationTv.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(upcomingItemPojo.getProposedOnInMyTimeZone())){
            holder2.dateTimeTv.setVisibility(View.VISIBLE);
            holder2.dateTimeTv.setText(upcomingItemPojo.getProposedOnInMyTimeZone());
        }
        else{
            holder2.dateTimeTv.setVisibility(View.GONE);
        }


        if(upcomingItemPojo.isLiveDate()) {
            holder2.call_button.setEnabled(true);
            holder2.callButtonOverlay.setVisibility(View.GONE);
            holder2.callButtonOverlay.setClickable(false);
        }
        else {
            holder2.call_button.setEnabled(false);
            holder2.callButtonOverlay.setVisibility(View.VISIBLE);
            holder2.callButtonOverlay.setClickable(false);
        }

        if (upcomingItemPojo.getDateType() == 3) { //audio
            holder2.call_tv.setText(R.string.audio_call_date);
            holder2.call_iv.setImageResource(R.mipmap.ic_call_notif);
        } else if (upcomingItemPojo.getDateType() == 1) { //video
            holder2.call_tv.setText(R.string.video_call_date);
            holder2.call_iv.setImageResource(R.drawable.ic_video);
        }
        else{
            holder2.call_tv.setText(R.string.get_direction);
            holder2.call_iv.setImageResource(R.drawable.ic_navigation);
        }

        //holder2.tvTimeText.setText(utility.time_Converter_sort_fm(upcomingItemPojo.getCreatedTimestamp()>0?upcomingItemPojo.getCreatedTimestamp():0));
        holder2.tvTimeText.setText(utility.time_Converter_upcoming(upcomingItemPojo.getProposedOn()>0?upcomingItemPojo.getProposedOn():0));
    }

}
