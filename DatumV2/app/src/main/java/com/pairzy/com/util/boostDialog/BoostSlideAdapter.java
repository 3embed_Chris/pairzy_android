package com.pairzy.com.util.boostDialog;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

/**
 * <h>BoostSlideAdapter class</h>
 * @author 3Embed.
 * @since 1/5/18.
 * @version 1.0.
 */

public class BoostSlideAdapter extends PagerAdapter {

    private ArrayList<Slide> sliderList = new ArrayList<>();
    private Context context;
    private TypeFaceManager typeFaceManager;
    private SlideItemClickCallback clickCallback;
    private Utility utility;

    BoostSlideAdapter(SlideItemClickCallback clickCallback,
                      TypeFaceManager typeFaceManager,
                      Context context,
                      ArrayList<Slide> slideList,
                      Utility utility
    ){
        this.clickCallback = clickCallback;
        this.sliderList = slideList;
        this.context = context;
        this.typeFaceManager = typeFaceManager;
        this.utility = utility;
    }
    @Override
    public int getCount() {
        return sliderList.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        Slide slide = sliderList.get(position);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.boost_slider_item, container, false);
        //TextView tvSlideTitle = layout.findViewById(R.id.slide_title_tv);
        TextView tvSlideCenter = layout.findViewById(R.id.slide_center_tv);
        ImageView ivSlide = layout.findViewById(R.id.slide_iv);
        //set data
        TextView tvSlideSubTitle = layout.findViewById(R.id.slide_sub_title_tv);
        TextView tvSlideMsg = layout.findViewById(R.id.slide_msg_tv);
        tvSlideSubTitle.setText(slide.getSubTitle());
        tvSlideMsg.setText(slide.getMsg());

        if(slide.getImageRes() == 0){
            ivSlide.setImageResource(slide.getImageRes());
            tvSlideCenter.setVisibility(View.VISIBLE);
            tvSlideCenter.setText("99+");
            ivSlide.setVisibility(View.INVISIBLE);
        }
        else {
            ivSlide.setVisibility(View.VISIBLE);
            ivSlide.setImageResource(slide.getImageRes());
            tvSlideCenter.setVisibility(View.GONE);
        }

        //set typeface
        tvSlideCenter.setTypeface(typeFaceManager.getCircularAirBold());
        //tvSlideTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvSlideSubTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvSlideMsg.setTypeface(typeFaceManager.getCircularAirBook());
        container.addView(layout);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickCallback != null)
                    clickCallback.onSlideItemClick(position);
            }
        });
        return layout;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
