package com.pairzy.com.likes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LikesDataPojo {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("likersName")
    @Expose
    private String likersName;
    @SerializedName("PhoneNo")
    @Expose
    private String phoneNo;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("gender")
    @Expose
    private String gender;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLikersName() {
        return likersName;
    }

    public void setLikersName(String likersName) {
        this.likersName = likersName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
