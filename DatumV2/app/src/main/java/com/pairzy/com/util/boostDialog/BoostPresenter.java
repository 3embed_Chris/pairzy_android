package com.pairzy.com.util.boostDialog;

/**
 * <h>BoostPresenter class</h>
 * @author 3Embed.
 * @since 9/5/18.
 * @version 1.0.
 */
public class BoostPresenter implements BoostContract.Presenter{

    private BoostContract.View view;

    BoostPresenter(){}

    @Override
    public void takeView(BoostContract.View view) {
        this.view = view;
    }

    @Override
    public void dropView() {
        this.view = null;
    }

    @Override
    public void init() {
        if(view != null)
            view.applyFont();
    }
}
