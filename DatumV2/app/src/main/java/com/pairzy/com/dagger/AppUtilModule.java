package com.pairzy.com.dagger;

import android.content.Context;

import com.androidinsta.com.InstagramManger;
import com.couchbase.lite.android.AndroidContext;
import com.pairzy.com.BuildConfig;
import com.pairzy.com.MatchedView.DateAlertDialog;
import com.pairzy.com.MatchedView.Its_Match_alert;
import com.pairzy.com.MatchedView.MatchAlertObserver;
import com.pairzy.com.MatchedView.MatchAnimation;
import com.pairzy.com.MatchedView.MatchedModel;
import com.pairzy.com.MqttChat.Database.CouchDbController;
import com.pairzy.com.MqttManager.MqttRxObserver;
import com.pairzy.com.R;
import com.pairzy.com.boostDetail.model.CoinPlan;
import com.pairzy.com.boostDetail.model.SubsPlan;
import com.pairzy.com.data.local.PreferencesHelper;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.ProUserModel;
import com.pairzy.com.data.model.ProfileDataChangeHolder;
import com.pairzy.com.data.model.UserActionHolder;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PassportLocationDataSource;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.HomeModel.HomeAnimation;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.networking.RxNetworkObserver;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.CalendarEventHelper;
import com.pairzy.com.util.CloudManager.UploadManager;
import com.pairzy.com.util.CustomObserver.AdminCoinObserver;
import com.pairzy.com.util.CustomObserver.BoostEndObserver;
import com.pairzy.com.util.CustomObserver.BoostViewCountObserver;
import com.pairzy.com.util.CustomObserver.CoinBalanceObserver;
import com.pairzy.com.util.CustomObserver.DateObserver;
import com.pairzy.com.util.CustomObserver.DateRefreshObserver;
import com.pairzy.com.util.CustomObserver.LocationObserver;
import com.pairzy.com.util.CustomObserver.SettingsDataChangeObserver;
import com.pairzy.com.util.CustomObserver.dataChangeObserver.DatumDataChangeObserver;
import com.pairzy.com.util.DatumActivateLifeListener;
import com.pairzy.com.util.DeviceUuidFactory;
import com.pairzy.com.util.FileUtil.AppFileManger;
import com.pairzy.com.util.ImageChecker.ImageProcessor;
import com.pairzy.com.util.LocationProvider.LocationApiManager;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.accountKit.AccountKitManager;
import com.pairzy.com.util.accountKit.AccountKitManagerImpl;
import com.pairzy.com.util.boostDialog.Offer;
import com.pairzy.com.util.boostDialog.Slide;
import com.pairzy.com.util.notificationHelper.NotificationHelper;
import com.pairzy.com.util.notificationHelper.NotificationHelperImpl;
import com.facebookmanager.com.FacebookManager;

import java.util.ArrayList;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppUtilModule
{
    @Singleton
    @Provides
    PreferenceTaskDataSource preferencesHelper(Context context)
    {
        return new PreferencesHelper(context);
    }

    @Singleton
    @Provides
    TypeFaceManager provideTypefaceManager(Context context){
        return new TypeFaceManager(context);
    }

    @Singleton
    @Provides
    FacebookManager provideFacebookManager(Context context)
    {
        return new FacebookManager(context, context.getString(R.string.facebook_app_id));
    }

    @Singleton
    @Provides
    InstagramManger getInstagramManger(Context context)
    {
        return  new InstagramManger(context,AppConfig.INSTA_CLINT_KEY,AppConfig.INSTA_SECRET_KEY);
    }

    @Singleton
    @Provides
    DeviceUuidFactory getDeviceUuidFactory(Context context)
    {
        return new DeviceUuidFactory(context);
    }

    @Singleton
    @Provides
    Utility getUtility(Context context)
    {
        return new Utility(context);
    }

    @Singleton
    @Provides
    UploadManager uploadManagerg(PreferenceTaskDataSource dataSource)
    {
        return new UploadManager(true,dataSource.getCloudName(),
                dataSource.getCloudinaryApiKey(),
                dataSource.getCloudinaryApiSecret());
    }


    @Singleton
    @Provides
    AppFileManger appFileManger(Context context)
    {
        return new AppFileManger(context);
    }



    @Singleton
    @Provides
    LocationApiManager getLocationApiManager()
    {
        return new LocationApiManager(BuildConfig.GOOGLE_API_KEY);
    }

    @Singleton
    @Provides
    NetworkStateHolder getNetworkStateHolder()
    {
        return new NetworkStateHolder();
    }

    @Singleton
    @Provides
    MatchAlertObserver getMatchAlertObserver()
    {
        return new MatchAlertObserver();
    }

    @Singleton
    @Provides
    RxNetworkObserver getRxNetworkObserver()
    {
        return new RxNetworkObserver();
    }


    @Singleton
    @Provides
    ImageProcessor getImageProcessor()
    {
        return new ImageProcessor(AppConfig.IMAGE_PROCESSOR_KEY,AppConfig.IMAGE_PROCESSOR_SECRET_KEY);
    }

    @Singleton
    @Provides
    MatchAnimation getAnimationHandler(Context context)
    {
        return new MatchAnimation(context);
    }

    @Singleton
    @Provides
    HomeAnimation provideHomeAnimation(Context context)
    {
        return new HomeAnimation(context);
    }

    @Singleton
    @Provides
    DateAlertDialog getDateAlertDialog(Context context,TypeFaceManager typeFaceManager)
    {
        return new DateAlertDialog(context,typeFaceManager);
    }

    @Singleton
    @Provides
    Its_Match_alert getIts_Match_alert(Context context, PreferenceTaskDataSource dataSource, TypeFaceManager typeFaceManager,MatchAnimation animationHandler,DateAlertDialog dateAlertDialog)
    {
        return new Its_Match_alert(context,dataSource,typeFaceManager,animationHandler,dateAlertDialog);
    }

    @Singleton
    @Provides
    MatchedModel getMatchedModel(Context context,PreferenceTaskDataSource dataSource,Utility utility)
    {
        return new MatchedModel(context,dataSource,utility);
    }

    @Singleton
    @Provides
    DatumActivateLifeListener getDatumActivateLifeListener()
    {
        return new DatumActivateLifeListener();
    }

    @Singleton
    @Provides
    CouchDbController getCouchDbController(Context context)
    {
        return new CouchDbController(new AndroidContext(context));
    }

    @Singleton
    @Provides
    DateObserver provideDateObserver(){
        return new DateObserver();
    }

    @Singleton
    @Provides
    LocationObserver provideLoationObserver(){
        return new LocationObserver();
    }

    @Singleton
    @Provides
    ProfileDataChangeHolder provideProfileDataChangeHolder(){
        return new ProfileDataChangeHolder();
    }

    @Singleton
    @Provides
    ArrayList<Slide> provideSlideList(Context context)
    {
        ArrayList<Slide> slideArrayList = new ArrayList<>();
        CharSequence[] boostSubTitles = context.getResources().getTextArray(R.array.boost_dialog_subtitle);
        CharSequence[] boostMsgs = context.getResources().getTextArray(R.array.boost_dialog_msg);
        int[] colors = context.getResources().getIntArray(R.array.boost_dialog_color);
        int[] drawable = {R.drawable.unlimited_likes ,R.drawable.dialog_boost ,R.drawable.who_sees_you,
                R.drawable.swipe_around_the_world, R.drawable.control_your_profile,
                R.drawable.super_likes, R.drawable.unlimited_rewinds, R.drawable.turn_off_adverts,
                0};
        for(int i = 0; i< boostSubTitles.length;i++){
            slideArrayList.add(new Slide(colors[i],drawable[i],(String)boostSubTitles[i], (String) boostMsgs[i]));
        }
        return slideArrayList;
    }

    @Singleton
    @Provides
    ArrayList<Offer> provideOfferList(){
        ArrayList<Offer> offerList = new ArrayList<>();
        offerList.add(new Offer("",12,"$ 350.00/mth",false));
        offerList.add(new Offer("MOST POPULAR",6,"$ 450.00/mth",true));
        offerList.add(new Offer("",1,"$ 150.00/mth",false));
        return offerList;
    }

    @Singleton
    @Provides
    CoinBalanceHolder provideCoinBalanceHolder(){
        return new CoinBalanceHolder();
    }

    @Singleton
    @Provides
    CoinBalanceObserver provideCoinBalanceObserver(){
        return new CoinBalanceObserver();
    }

    @Singleton
    @Provides
    ArrayList<CoinPlan> provideCoinPlanList(){
        return new ArrayList<>();
    }

    @Singleton
    @Provides
    ArrayList<SubsPlan> provideSubsPlanList(){
        return new ArrayList<>();
    }

    @Singleton
    @Provides
    PassportLocationDataSource passportLocationHelper(Context context)
    {
        return new PreferencesHelper(context);
    }

    @Singleton
    @Provides
    CoinConfigWrapper provideCoinConfigWrapper(){
        return new CoinConfigWrapper();
    }

    @Singleton
    @Provides
    CalendarEventHelper provideCalendarEventHelper(Context context){
        return new CalendarEventHelper(context);
    }

    @Singleton
    @Provides
    MqttRxObserver provideMqttRxObserver(){
        return new MqttRxObserver();
    }

    @Singleton
    @Provides
    DateRefreshObserver provideDateRefreshObserver(){
        return new DateRefreshObserver();
    }


    @Singleton
    @Provides
    ProUserModel provideProUserModel(Context context, PreferenceTaskDataSource dataSource, Utility utility,BoostViewCountObserver countObserver,NotificationHelper notificationHelper)
    {
        return new ProUserModel(context,dataSource,utility,countObserver,notificationHelper);
    }

    @Singleton
    @Provides
    BoostViewCountObserver boostViewCountObserver()
    {
        return new BoostViewCountObserver();
    }

    @Singleton
    @Provides
    BoostEndObserver boostEndObserver()
    {
        return new BoostEndObserver();
    }

    @Singleton
    @Provides
    AdminCoinObserver adminCoinObserver()
    {
        return new AdminCoinObserver();
    }

    @Singleton
    @Provides
    SettingsDataChangeObserver settingsDataChangeObserver(){
        return new SettingsDataChangeObserver();
    }

    @Singleton
    @Provides
    NotificationHelper provideNotificationHelper(Context context,DatumActivateLifeListener datumActivateLifeListener){
        return new NotificationHelperImpl(context,datumActivateLifeListener);
    }

    @Singleton
    @Provides
    DatumDataChangeObserver datumDataChangeObserver(){
        return new DatumDataChangeObserver();
    }

    @Singleton
    @Provides
    UserActionHolder provideUserActionHolder(PreferenceTaskDataSource dataSource){
        return new UserActionHolder(dataSource);
    }

    @Singleton
    @Provides
    AccountKitManager provideAccountKitManager(){
        return new AccountKitManagerImpl();

    }
}
