package com.pairzy.com.home.Dates.CustomAlertDialog;
/**
 * <h2>ChatMenuCallback</h2>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface CustomAlertDialogCallBack
{
    void onOk();
}
