package com.pairzy.com.data.model.coinBalance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 *<h>CoinResponse</h>
 *@author 3Embed.
 *@since 4/6/18.
 */

public class CoinResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private WalletData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public WalletData getData() {
        return data;
    }

    public void setData(WalletData data) {
        this.data = data;
    }
}
