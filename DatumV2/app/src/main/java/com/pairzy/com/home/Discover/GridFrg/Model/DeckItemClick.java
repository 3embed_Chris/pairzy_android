package com.pairzy.com.home.Discover.GridFrg.Model;
import android.view.MotionEvent;
import android.view.View;
/**
 * <h2>DeckItemClick</h2>
 * @since  3/23/2018.
 * @author 3Embed.
 */
public abstract class DeckItemClick implements View.OnTouchListener
{
    private float startX;
    private float startY;
    private static final int MOVE_ACTION_THRESHOLD =10;
    private boolean isMove=false;

    abstract void onClick();
    abstract void notHandling(MotionEvent event);

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent)
    {
        switch (motionEvent.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                startX = motionEvent.getX();
                startY = motionEvent.getY();
                isMove=false;
                break;
            case MotionEvent.ACTION_MOVE:
                float endX = motionEvent.getX();
                float endY = motionEvent.getY();
                if(!isMove)
                    isMove=isMove(startX, endX, startY, endY);
                break;
            case MotionEvent.ACTION_UP:
                if(!isMove)
                {
                    onClick();
                }
                break;
        }
        try
        {
            notHandling(motionEvent);
        }/*catch (IllegalArgumentException e){}*/
        catch (Exception e){}
        return true;
    }

    private boolean isMove(float startX, float endX, float startY, float endY)
    {
        float differenceX = Math.abs(startX - endX);
        float differenceY = Math.abs(startY - endY);
        return (differenceX > MOVE_ACTION_THRESHOLD|| differenceY > MOVE_ACTION_THRESHOLD);
    }
}
