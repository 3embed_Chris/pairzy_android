package com.pairzy.com.coinWallet;

import android.app.Activity;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.coinWallet.Model.CoinWalletModel;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.CustomObserver.CoinBalanceObserver;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h>CoinWalletPresenter class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */
public class CoinWalletPresenter implements CoinWalletContract.Presenter{

    @Inject
    CoinWalletContract.View view;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    NetworkService service;
    @Inject
    Activity activity;
    @Inject
    CoinWalletModel model;
    @Inject
    CoinBalanceObserver coinBalanceObserver;
    @Inject
    CoinBalanceHolder coinBalanceHolder;

    private CompositeDisposable compositeDisposable;
    private Boolean updateCoinBalance = true;
    private final int LIMIT = 50;
    private final int PAGE_COUNT = 0;
    private boolean isRefresh = false;


    @Inject
    public CoinWalletPresenter(){
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void init() {
        if(view != null)
            view.applyFont();
    }

//    @Override
//    public void fetchWalletHistory(boolean isRefresh){
//        this.isRefresh=isRefresh;
//        if(isRefresh){
//            loadMoreStatus.setNo_more_data(false);
//            PAGE_SIZE = 0;
//            if(view != null){
//                view.showLoading(isPending);
//            }
//        }
//        else{
//            PAGE_SIZE += LIMIT;
//        }
//        loadCurrentDate(PAGE_SIZE,PAGE_SIZE+ LIMIT);
//    }

    @Override
    public void getCoinHistory() {
        if(networkStateHolder.isConnected()) {
            if (view != null)
                view.showLoading();
            service.getCoinHistory(dataSource.getToken(), model.getLanguage(),PAGE_COUNT,LIMIT)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                try {
                                    model.parseCoinHistory(value.body().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                if(view != null)
                                    view.notifyAdapter();
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else {
                                try {
                                    String error = value.errorBody().string();
                                    if (view != null)
                                        view.showError(error);
                                    if (view != null)
                                        view.showNetworkError(error);
                                }catch (Exception e){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null)
                                view.showError(activity.getString(R.string.failed_to_fetch_transaction_history));
                            if (view != null)
                                view.showNetworkError(activity.getString(R.string.failed_to_fetch_transaction_history));
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));

        }

    }

    @Override
    public void observeCoinBalanceChange() {
        coinBalanceObserver.getObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean value) {
                        if(value){
                            updateCoinBalance = value;
                        }
                    }

                    @Override
                    public void onError(Throwable e) {}

                    @Override
                    public void onComplete() {}
                });
    }

    @Override
    public boolean isCoinBalanceUpdateRequired() {
        return  updateCoinBalance;
    }


    public void dispose() {
        compositeDisposable.clear();
    }
}
