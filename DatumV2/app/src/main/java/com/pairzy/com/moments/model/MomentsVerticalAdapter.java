package com.pairzy.com.moments.model;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;
import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.R;
import com.pairzy.com.data.local.PreferencesHelper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * <h2>MomentsGridAdapter</h2>
 * <P> this is a MomentsGridAdapter  class for momentsActivity</P>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */
public class MomentsVerticalAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = MomentsVerticalAdapter.class.getSimpleName();

    private ArrayList<MomentsData> lists;
    private Context mcontext;
    private MomentPostClickCallBack clickCallBack;
    private ItemActionCallBack vhCallback;
    private TypeFaceManager typeFaceManager;
    private PreferenceTaskDataSource dataSource;
    private Utility utility;
    private boolean  doubleClick = false;
    @Inject
    PreferencesHelper data;
    private RequestManager requestManager;

    public MomentsVerticalAdapter(Utility utility,PreferenceTaskDataSource dataSource, TypeFaceManager typeFaceManager, Context context, RequestManager requestManager, ArrayList<MomentsData> lists) {
        this.dataSource = dataSource;
        this.lists=lists;
        this.requestManager= requestManager;
        this.mcontext=context;
        this.typeFaceManager = typeFaceManager;
        this.utility = utility;

        initCallback();
    }

    public void setAdapterCallback(MomentPostClickCallBack clickCallBack){
        this.clickCallBack = clickCallBack;
    }

    private void initCallback() {
        Log.e(TAG, "intiCallback: " );
        vhCallback = (id, position) -> {
            {
                switch (id) {
                    case R.id.tvPostlistViewAll:
                        if (clickCallBack != null)
                            clickCallBack.onViewAllComment(position);
                        break;

                    case R.id.ivPostlistPostComment:
                        if (clickCallBack != null)
                            clickCallBack.onComment(position);
                        break;

                    case R.id.rvPostlistPost:
                        /*if(list.get(position).getLiked())
                            if (adapterItemCallback != null)
                                adapterItemCallback.onLikePost(position);
*/
                        break;
                    case R.id.ivPostlistPostMessage:
                        clickCallBack.onChatMessage(position);
                        break;

                    case R.id.ivPostlistPostLike:
                        if(lists.get(position).getLiked())
                        {
                            if (clickCallBack != null)
                                clickCallBack.onUnlikePost(position);

                        }
                        else{
                            if (clickCallBack != null)
                                clickCallBack.onLikePost(position);
                        }
                        break;

                    case R.id.tvPostlistLikedCount:
                        if (clickCallBack != null)
                            clickCallBack.onViewAllLikes(position);
                        break;
                    case R.id.fl_delete_button:
                        if (clickCallBack != null)
                            clickCallBack.onDeletePost(position);
                        break;
                }
            }

        };
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_list_item,parent, false);
        return new MomentsVerticalItemHolder(view,typeFaceManager,vhCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        MomentsVerticalItemHolder momentsHolder= (MomentsVerticalItemHolder) holder;
        momentsHolder.onBind(lists.get(position),requestManager);
        if(lists.get(position).getLiked())
            momentsHolder.postLike.setImageResource(R.drawable.islike);
        else
            momentsHolder.postLike.setImageResource(R.drawable.ic_like);

        momentsHolder.sdvPostPreview.setOnTouchListener(new View.OnTouchListener() {
            private GestureDetector gestureDetector = new GestureDetector(mcontext, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    momentsHolder.postLiked.setVisibility(View.VISIBLE);
                    if(!lists.get(position).getLiked())
                        if (clickCallBack != null)
                            clickCallBack.onLikePost(position);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            momentsHolder.postLiked.setVisibility(View.GONE);
                        }
                    }, 2000);

                    return super.onDoubleTap(e);
                }

            });

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TEST", "Raw event: " + event.getAction() + ", (" + event.getRawX() + ", " + event.getRawY() + ")");
                gestureDetector.onTouchEvent(event);
                return true;
            }
        });
        handleViewData(momentsHolder,position);

    }
    private void handleViewData(MomentsVerticalItemHolder momentsLinearItemHolder, int i) {
        momentsLinearItemHolder.profileName.setTypeface(typeFaceManager.getCircularAirBold());
        momentsLinearItemHolder.tvPostTime.setTypeface(typeFaceManager.getCircularAirLight());
        momentsLinearItemHolder.tvPostLikeCount.setTypeface(typeFaceManager.getCircularAirBold());
        momentsLinearItemHolder.tvPostCommentViewAll.setTypeface(typeFaceManager.getCircularAirBold());
        momentsLinearItemHolder.tvUpdatedBio.setTypeface(typeFaceManager.getCircularAirBook());

        momentsLinearItemHolder.profileName.setText(lists.get(i).getUserName());
        momentsLinearItemHolder.tvPostLikeCount.setText(String.format("%s %s", lists.get(i).getLikeCount(), mcontext.getString(R.string.likes_text_lower)));
        momentsLinearItemHolder.tvPostCommentViewAll.setText(String.format("%s %s", lists.get(i).getCommentCount(), mcontext.getString(R.string.comments_text)));
        momentsLinearItemHolder.profilePic.setImageURI(lists.get(i).getProfilePic());

        if(lists.get(i).getType().equalsIgnoreCase("match"))
        { momentsLinearItemHolder.PostMatch.setVisibility(View.VISIBLE);
            momentsLinearItemHolder.postLike.setVisibility(View.GONE);
            momentsLinearItemHolder.postLiked.setVisibility(View.GONE);
            momentsLinearItemHolder.tvPostLikeCount.setVisibility(View.GONE);
            momentsLinearItemHolder.postComment.setVisibility(View.GONE);
            momentsLinearItemHolder.tvPostCommentViewAll.setVisibility(View.GONE);

        }
        else { momentsLinearItemHolder.PostMatch.setVisibility(View.GONE);
            momentsLinearItemHolder.postLike.setVisibility(View.VISIBLE);
            momentsLinearItemHolder.postLiked.setVisibility(View.GONE);
            momentsLinearItemHolder.tvPostLikeCount.setVisibility(View.VISIBLE);
            momentsLinearItemHolder.postComment.setVisibility(View.VISIBLE);
            momentsLinearItemHolder.tvPostCommentViewAll.setVisibility(View.VISIBLE);
        }
        momentsLinearItemHolder.tvUpdatedBio.setText(lists.get(i).getDescription());
        momentsLinearItemHolder.tvPostTime.setText(utility.getdate(lists.get(i).getPostedOn()));

        if(dataSource.getUserId().equals(lists.get(i).getUserId())) {
            momentsLinearItemHolder.profileMessage.setVisibility(View.GONE);
            momentsLinearItemHolder.flDeleteButton.setVisibility(View.VISIBLE);
        }else {
            momentsLinearItemHolder.profileMessage.setVisibility(View.VISIBLE);
            momentsLinearItemHolder.flDeleteButton.setVisibility(View.GONE);
        }
        if(lists.get(i).getTypeFlag()==4) {
            momentsLinearItemHolder.Bio.setVisibility(View.VISIBLE);
            momentsLinearItemHolder.BioText.setVisibility(View.VISIBLE);
            momentsLinearItemHolder.Bio.setText(lists.get(i).getDescription());
            momentsLinearItemHolder.tvUpdatedBio.setText("");
            momentsLinearItemHolder.bioBlurBackground.setVisibility(View.VISIBLE);
        }
        else {
            momentsLinearItemHolder.Bio.setVisibility(View.GONE);
            momentsLinearItemHolder.BioText.setVisibility(View.GONE);
            momentsLinearItemHolder.bioBlurBackground.setVisibility(View.GONE);
        }

        ArrayList<String> postUrls = lists.get(i).getUrl();
       /* if(lists.get(i).getUrl().contains(".mp4") || lists.get(i).getUrl().contains(".mov"))
            momentsLinearItemHolder.videoIcon.setVisibility(View.VISIBLE);
        else
            momentsLinearItemHolder.videoIcon.setVisibility(View.GONE);
*/
        if(postUrls != null && !postUrls.isEmpty()){
            String postUrlPreview = postUrls.get(0);
            postUrlPreview = postUrlPreview.replace(".mp4",".jpg").replace(".mov","");
            momentsLinearItemHolder.sdvPostPreview.setImageURI(postUrlPreview);
        }
        //        //TODO setting adapter
//        PostSubAdapter subAdapter=new PostSubAdapter(lists.get(i).getUrl(),null);
//        postListViewHolder.rvPostlistPost.setAdapter(subAdapter);
//        postListViewHolder.rvPostlistPost.setLayoutManager(new LinearLayoutManager(context,RecyclerView.HORIZONTAL,false));
//        SnapHelper startSnapHelper = new StartSnapHelper();
//        postListViewHolder.rvPostlistPost.setOnFlingListener(null);
//        startSnapHelper.attachToRecyclerView(postListViewHolder.rvPostlistPost);
//        subAdapter.notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return lists.size();
    }
}
