package com.pairzy.com.locationScreen.locationMap;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h>CustomLocModule class</h>
 * @author 3Embed.
 * @since 8/5/18.
 * @version 1.0.
 */

@Module
public abstract class CustomLocModule {

    @ActivityScoped
    @Binds
    abstract CustomLocContract.Presenter bindsCustomLocPresenter(CustomLocPresenter presenter);

    @ActivityScoped
    @Binds
    abstract CustomLocContract.View bindsCustomLocView(CustomLocActivity customLocActivity);

    @ActivityScoped
    @Binds
    abstract Activity bindsCustomLocActivity(CustomLocActivity customLocActivity);
}
