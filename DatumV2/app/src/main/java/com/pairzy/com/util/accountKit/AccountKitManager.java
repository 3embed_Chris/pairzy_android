package com.pairzy.com.util.accountKit;

import android.app.Activity;
import android.content.Intent;

public interface AccountKitManager {
    void initPhoneLogin(Activity activity, String countryCode,
                        String mobileNumber, AccountKitManagerImpl.GetPhoneDetailCallback callback);
    void initPhoneLogin(Activity activity, AccountKitManagerImpl.GetPhoneDetailCallback callback);
    void logout();
    void getPhoneDetail(AccountKitManagerImpl.GetPhoneDetailCallback callback);
    boolean isLoggedIn();
    void onActivityResult(int requestCode, int resultCode, Intent data);
}
