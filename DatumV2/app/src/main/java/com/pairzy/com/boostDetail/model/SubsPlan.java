package com.pairzy.com.boostDetail.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by ankit on 1/6/18.
 */

public class SubsPlan
{
    @SerializedName("planId")
    @Expose
    private String id;

    @SerializedName("actualId")
    @Expose
    private String actualId;
    @SerializedName("actualIdForAndroid")
    @Expose
    private String actualIdForAndroid;

    @SerializedName("planName")
    @Expose
    private String planName;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("currencyCode")
    @Expose
    private String currencyCode;

    @SerializedName("currencySymbol")
    @Expose
    private String currencySymbol;

    @SerializedName("cost")
    @Expose
    private Integer cost;

    private String price_text;

    private boolean selected = false;

    @SerializedName("tag")
    @Expose
    private String headerTag = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public void setActualId(String actualId) {
        this.actualId = actualId;
    }

    public String getActualId() {
        return actualId;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setHeaderTag(String tag) {
        this.headerTag = tag;
    }

    public String getHeaderTag() {
        return headerTag;
    }

    public void setPrice_text(String price_text) {
        this.price_text = price_text;
    }

    public String getPrice_text() {
        return price_text;
    }

    public String getActualIdForAndroid() {
        return actualIdForAndroid;
    }

    public void setActualIdForAndroid(String actualIdForAndroid) {
        this.actualIdForAndroid = actualIdForAndroid;
    }
}
