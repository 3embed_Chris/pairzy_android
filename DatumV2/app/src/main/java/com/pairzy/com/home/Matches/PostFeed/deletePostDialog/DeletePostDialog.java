package com.pairzy.com.home.Matches.PostFeed.deletePostDialog;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;

public class DeletePostDialog {

    private Activity activity;
    private TypeFaceManager typeFaceManager;
    private PostDeleteCallback callback;
    private AlertDialog alertDialog;

    public DeletePostDialog(Activity activity,TypeFaceManager typeFaceManager) {
        this.activity = activity;
        this.typeFaceManager = typeFaceManager;
    }

    public void showAlert(String postId, int position,PostDeleteCallback callback){
        this.callback = callback;
        cancelDialog();
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = activity.findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_post_delete, viewGroup, false);
        TextView tvDelete = dialogView.findViewById(R.id.tv_delete);
        tvDelete.setTypeface(typeFaceManager.getCircularAirBook());
        tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(callback != null)
                    callback.onPostDelete(postId,position);
                cancelDialog();
            }
        });

        TextView tvCancel = dialogView.findViewById(R.id.tv_cancel);
        tvCancel.setTypeface(typeFaceManager.getCircularAirBook());
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelDialog();
            }
        });

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        //finally creating the alert dialog and displaying it
        alertDialog = builder.create();
        alertDialog.show();
    }

    public void cancelDialog(){
        if(alertDialog != null && alertDialog.isShowing()){
            alertDialog.cancel();
        }
    }
}
