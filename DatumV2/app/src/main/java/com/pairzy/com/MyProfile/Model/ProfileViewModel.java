package com.pairzy.com.MyProfile.Model;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.MyProfile.MyProfilePagePresenter;
import com.pairzy.com.R;
import com.pairzy.com.data.model.PrefData;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;
import java.util.Locale;
import javax.inject.Inject;

/**
 * <h2>ProfileViewModel</h2>
 * <P>
 *     Presenting the view details of the user to get the
 *     data details.
 * </P>
 * @since  4/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ProfileViewModel
{
    @Inject
    Activity activity;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    PreferenceTaskDataSource dataSource;

    private OnUserDetailClickCallback clickCallback;
    private OnAboutTextChangeCallback aboutTextChangeCallback;

    public interface OnUserDetailClickCallback{

        void onNameClick(TextView tvName);
        void onGenderClick(TextView tvGender);
        void onAgeClick(TextView tvAge);
        void onPhoneClick(TextView tvPhone);
        void onEmailClick(TextView tvEmail);
        void onLocationClick(TextView tvLocation);
        void onRetry();
    }
    public interface OnAboutTextChangeCallback{
        void onTextChange(String newText);
    }

    public void setClickCallback(OnUserDetailClickCallback clickCallback) {
        this.clickCallback = clickCallback;
    }

    @Inject
    ProfileViewModel(){}

    public View loadingView()
    {
        @SuppressLint("InflateParams")
        View convertView = activity.getLayoutInflater().inflate(R.layout.user_loading_onfo_view, null);
        convertView.findViewById(R.id.loading_progress).setVisibility(View.VISIBLE);
        convertView.findViewById(R.id.connection_error_icon).setVisibility(View.GONE);
        convertView.findViewById(R.id.message_text).setVisibility(View.VISIBLE);
        convertView.findViewById(R.id.error_message).setVisibility(View.GONE);
        return convertView;
    }


    /*
    *Adding the error view on loading failed.*/
    public View errorOnLoadingView(String error,MyProfilePagePresenter presenter)
    {
        setClickCallback(presenter);
        @SuppressLint("InflateParams")
        View convertView = activity.getLayoutInflater().inflate(R.layout.user_loading_onfo_view, null);
        convertView.findViewById(R.id.loading_progress).setVisibility(View.GONE);
        convertView.findViewById(R.id.connection_error_icon).setVisibility(View.VISIBLE);
        TextView tvError = convertView.findViewById(R.id.message_text);
        tvError.setVisibility(View.GONE);
        tvError.setText(error);
        convertView.findViewById(R.id.error_message).setVisibility(View.VISIBLE);
        Button btnRetry = convertView.findViewById(R.id.btn_retry);
        btnRetry.setTypeface(typeFaceManager.getCircularAirBold());
        btnRetry.setVisibility(View.VISIBLE);
        btnRetry.setOnClickListener( view ->{
            if(clickCallback != null)
                clickCallback.onRetry();
        });
        return convertView;
    }


    /*
     * Getting item details view*/
    public View addDetailsView(ArrayList<PrefData> myPrefrance, String categoryName, MyProfilePagePresenter presenter)
    {
        @SuppressLint("InflateParams")
        View convertView = activity.getLayoutInflater().inflate(R.layout.profile_list_item_row, null);
        TextView profileCategoryTv = convertView.findViewById(R.id.profileCategoryTv);
        profileCategoryTv.setText(categoryName);
        profileCategoryTv.setTypeface(typeFaceManager.getCircularAirBold());
        RecyclerView profileCategoryRv = convertView.findViewById(R.id.profileCategoryRv);
        MyPreferenceAdapter madapter = new MyPreferenceAdapter(myPrefrance,typeFaceManager);
        madapter.setCallback(presenter);
        profileCategoryRv.setHasFixedSize(true);
        RecyclerView.LayoutManager mlayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        profileCategoryRv.setLayoutManager(mlayoutManager);
        profileCategoryRv.setItemAnimator(new DefaultItemAnimator());
        profileCategoryRv.setAdapter(madapter);
        profileCategoryRv.setNestedScrollingEnabled(false);
        return convertView;
    }


    public View getUserDetailsView(MyProfilePagePresenter presenter,String username,String gender,int age,String phno,String email,String location) {
        @SuppressLint("InflateParams")
        View view = activity.getLayoutInflater().inflate(R.layout.profile_user_info_details, null);
        TextView user_name = view.findViewById(R.id.user_name);
        user_name.setTypeface(typeFaceManager.getCircularAirBold());
        user_name.setText(username);
        user_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickCallback != null)
                    clickCallback.onNameClick(user_name);
            }
        });

        /*Gender*/
        TextView gender_title = view.findViewById(R.id.gender_title);
        gender_title.setTypeface(typeFaceManager.getCircularAirLight());
        TextView selected_gender = view.findViewById(R.id.selected_gender);
        selected_gender.setTypeface(typeFaceManager.getCircularAirLight());
        selected_gender.setText(gender);
        RelativeLayout gender_view = view.findViewById(R.id.gender_view);
        setClickCallback(presenter);
        gender_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(clickCallback != null)
//                    clickCallback.onGenderClick(selected_gender);
            }
        });
        /*Age*/
        TextView age_title = view.findViewById(R.id.age_title);
        age_title.setTypeface(typeFaceManager.getCircularAirLight());
        TextView selected_age = view.findViewById(R.id.selected_age);
        selected_age.setTypeface(typeFaceManager.getCircularAirLight());
        if (age == 0){//login from facebook
            selected_age.setText(String.format(Locale.ENGLISH, "%s","NA" ));
        }
        else{
            selected_age.setText(String.format(Locale.ENGLISH, "%d", age));
        }
        RelativeLayout age_view=view.findViewById(R.id.age_view);
        age_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(age == 0) {
                    if (clickCallback != null)
                        clickCallback.onAgeClick(selected_age);
                }
            }
        });
        /*phone*/
        TextView phone_title=view.findViewById(R.id.phone_title);
        phone_title.setTypeface(typeFaceManager.getCircularAirLight());
        TextView selected_phone=view.findViewById(R.id.selected_phone);
        selected_phone.setTypeface(typeFaceManager.getCircularAirLight());
        if(!TextUtils.isEmpty(phno))
            selected_phone.setText(phno);
        else{
            selected_phone.setText("NA");
        }
        RelativeLayout phone_view=view.findViewById(R.id.phone_view);
        phone_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(clickCallback != null)
                    clickCallback.onPhoneClick(selected_phone);
            }
        });
        /*email*/
        TextView email_title=view.findViewById(R.id.email_title);
        email_title.setTypeface(typeFaceManager.getCircularAirLight());
        TextView selected_email=view.findViewById(R.id.selected_email);
        selected_email.setTypeface(typeFaceManager.getCircularAirLight());
        if(!TextUtils.isEmpty(email))
            selected_email.setText(email);
        else{
            selected_email.setText("NA");
        }
        RelativeLayout email_view=view.findViewById(R.id.email_view);
        email_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(clickCallback != null)
                    clickCallback.onEmailClick(selected_email);
            }
        });
        /*location*/
        TextView location_title=view.findViewById(R.id.location_title);
        location_title.setTypeface(typeFaceManager.getCircularAirLight());
        TextView selected_location=view.findViewById(R.id.selected_location);
        selected_location.setTypeface(typeFaceManager.getCircularAirLight());
        selected_location.setText(location);
        RelativeLayout location_view=view.findViewById(R.id.location_view);
        location_view.setOnClickListener(view1 -> {
            if(clickCallback != null)
              clickCallback.onLocationClick(selected_location);
        });
        return view;
    }

     /*
      * Getting Edit text view data.*/
    public View addEditTextView(String abouts,MyProfilePagePresenter presenter)
    {
        setAboutTextChangeCallback(presenter);
        @SuppressLint("InflateParams")
        View convertView = activity.getLayoutInflater().inflate(R.layout.user_profile_about_you_row, null);
        TextView aboutYouTv = convertView.findViewById(R.id.aboutYouTv);
        aboutYouTv.setTypeface(typeFaceManager.getCircularAirBold());
        TextView countTv = convertView.findViewById(R.id.countTv);
        EditText aboutYouDescEt = convertView.findViewById(R.id.aboutYouDescEt);
        aboutYouTv.setText(activity.getString(R.string.about_you_Text));
        aboutYouDescEt.setText(abouts);
        aboutYouDescEt.setSelection(abouts.length());
        countTv.setText(String.format(Locale.ENGLISH,"%d/%d", abouts.length(),130));
        aboutYouDescEt.setTypeface(typeFaceManager.getCircularAirLight());
        aboutYouDescEt.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                int length=charSequence.length();
                if(aboutTextChangeCallback != null){
                    aboutTextChangeCallback.onTextChange(charSequence.toString());
                }
                countTv.setText(String.format(Locale.ENGLISH,"%d/%d", length,250));

            }
            @Override
            public void afterTextChanged(Editable editable) {
                dataSource.setProfileUpdateType(ProfileUpdateType.PROFILE_ABOUT.value);
            }
        });
        return convertView;
    }

    public void setAboutTextChangeCallback(OnAboutTextChangeCallback aboutTextChangeCallback) {
        this.aboutTextChangeCallback = aboutTextChangeCallback;
    }

    private void getUserInstagramList(String user_id)
    {
        @SuppressLint("InflateParams")
        View view = activity.getLayoutInflater().inflate(R.layout.instagram_post_view, null);
        ViewPager content_pager=view.findViewById(R.id.content_pager);

    }
}
