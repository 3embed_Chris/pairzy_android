package com.pairzy.com.MySearchPreference;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatDelegate;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.passportLocation.PassportActivity;
import com.pairzy.com.util.TypeFaceManager;
import com.suresh.innapp_purches.InnAppSdk;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * @since 3/7/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MySearchPref extends BaseDaggerActivity implements MySearchPrefContract.View
{
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    MySearchPrefContract.Presenter presenter;
    @Inject
    Activity activity;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    AnimatorHandler animatorHandler;
    @BindView(R.id.first_title)
    TextView first_title;
    @BindView(R.id.second_title)
    TextView second_title;
    @BindView(R.id.item_view)
    NestedScrollView data_found;
    @BindView(R.id.loading_view)
    RelativeLayout loading_view;
    @BindView(R.id.loading_progress)
    ProgressBar loading_progress;
    @BindView(R.id.connection_error_icon)
    ImageView connection_error_icon;
    @BindView(R.id.message_text)
    TextView message_text;
    @BindView(R.id.error_message)
    TextView error_message;
    @BindView(R.id.parent_item)
    SwipeRefreshLayout parent_item;
    @BindView(R.id.child_container)
    LinearLayout child_container;
    @BindView(R.id.btnNext)
    RelativeLayout btnNext;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.tv_appbar_title)
    TextView tvAppbarTitle;
    private Unbinder unbinder;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mysearch_pref_layout);
        unbinder= ButterKnife.bind(this);
        initUi();
        initView();
        presenter.updateStoredPref();
        presenter.observeLocationChange();
    }

    private void initView() {
        tvAppbarTitle.setText(getString(R.string.my_preferences_text));
        tvAppbarTitle.setTypeface(typeFaceManager.getCircularAirBold());
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow;
            int scrollRange = -1;
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if(scrollRange == -1){
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if(scrollRange + verticalOffset == 0){
                    isShow = true;
                    showToolbarTitle(true);
                } else if(isShow){
                    isShow = false;
                    showToolbarTitle(false);
                }
            }
        });
    }

    private void showToolbarTitle(boolean show) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if(show){
                    tvAppbarTitle.setVisibility(View.VISIBLE);
                }
                else {
                    tvAppbarTitle.setVisibility(View.GONE);
                }
            }
        });
    }

    /*
     * Updating the UI*/
    private void initUi()
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            loading_progress.setProgressTintList(ColorStateList.valueOf(ContextCompat.getColor(activity,R.color.colorAccent)));
        }else
        {
            Drawable progressDrawable = loading_progress.getProgressDrawable().mutate();
            progressDrawable.setColorFilter(ContextCompat.getColor(activity,R.color.colorAccent), PorterDuff.Mode.SRC_IN);
            loading_progress.setProgressDrawable(progressDrawable);
        }
        first_title.setTypeface(typeFaceManager.getCircularAirBold());
        second_title.setTypeface(typeFaceManager.getCircularAirBold());
        parent_item.setColorSchemeResources(R.color.refresh2, R.color.refresh1,R.color.refresh);
        parent_item.setOnRefreshListener(
                () -> {
                    presenter.getMyPreference();
                }
        );

        parent_item.setOnChildScrollUpCallback(new SwipeRefreshLayout.OnChildScrollUpCallback() {
            @Override
            public boolean canChildScrollUp(@NonNull SwipeRefreshLayout parent, @Nullable View child) {
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        this.finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }


    @Override
    protected void onDestroy()
    {
        unbinder.unbind();
        presenter.dropView();
        super.onDestroy();
    }

    @OnClick(R.id.close_button)
    void onClose()
    {
        onBackPressed();
    }

    @OnClick(R.id.error_message)
    void onRetry()
    {
        loading_view.setVisibility(View.VISIBLE);
        hideConnectionFailed();
    }

    @OnClick(R.id.btnNext)
    void onUpdate()
    {
        presenter.updatePreference();
    }

    @Override
    public void prefUpdated(boolean isRequired)
    {
        if(isRequired)
        {
            Intent resultIntent = new Intent();
            setResult(Activity.RESULT_OK, resultIntent);
        }
        onBackPressed();
    }

    @Override
    public void showLoadingProgress()
    {
        hideConnectionFailed();
    }

    @Override
    public void showError(String error)
    {
        parent_item.setRefreshing(false);
        Snackbar snackbar = Snackbar
                .make(parent_item,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(String message)
    {
        parent_item.setRefreshing(false);
        Snackbar snackbar = Snackbar
                .make(parent_item,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }


    @Override
    public void updatePrefView(List<View> views)
    {
        parent_item.setRefreshing(false);
        if(child_container.getChildCount()>0)
            child_container.removeAllViews();

        for(View view:views)
        {
            child_container.addView(view);
        }
        showDataFound();
    }

    @Override
    public void onConnectionError()
    {
        parent_item.setRefreshing(false);
        if(child_container.getChildCount()<1)
        {
            data_found.setVisibility(View.GONE);
            loading_view.setVisibility(View.VISIBLE);
            hideProgress();
        }
    }

    @Override
    public void launchLocationScreen()
    {
 /*       if(dataSource.getSubscription() == null)
        {
            if(presenter!=null)
                presenter.openBoostDialog();
        }else*/
        {
            Intent intent = new Intent(activity, PassportActivity.class);
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
        }
    }

    /*
     * Hiding the progress bar*/
    private void hideProgress()
    {
        message_text.setVisibility(View.GONE);
        loading_progress.setVisibility(View.GONE);
        Animation animation=animatorHandler.getScaleDown();
        animation.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                showConnectionFailed();
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        loading_progress.startAnimation(animation);
    }

    /*
    *Showing connection failed.*/
    private void showConnectionFailed()
    {
        connection_error_icon.setVisibility(View.VISIBLE);
        Animation in_anim=animatorHandler.getScaleUp();
        in_anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                error_message.setVisibility(View.VISIBLE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        connection_error_icon.startAnimation(in_anim);
    }


    /*
   *Showing connection failed.*/
    private void hideConnectionFailed()
    {
        error_message.setVisibility(View.GONE);
        connection_error_icon.setVisibility(View.GONE);
        Animation in_anim=animatorHandler.getScaleDown();
        in_anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                showProgress();
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        connection_error_icon.startAnimation(in_anim);
    }

    /*
    * Hiding the progress bar*/
    private void showProgress()
    {
        message_text.setVisibility(View.VISIBLE);
        loading_progress.setVisibility(View.VISIBLE);
        Animation animation=animatorHandler.getScaleUp();
        animation.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                presenter.getMyPreference();
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        loading_progress.startAnimation(animation);
    }

    /*
     * Hiding the progress bar*/
    private void showDataFound()
    {
        loading_view.setVisibility(View.GONE);
        data_found.setVisibility(View.VISIBLE);
        btnNext.setVisibility(View.VISIBLE);
        btnNext.setEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(!InnAppSdk.getVendor().onActivity_result(requestCode,resultCode,data))
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
