package com.pairzy.com.util.CustomObserver;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;

/**
 * Created by ankit on 25/5/18.
 */

public class SettingsDataChangeObserver{

    private ConnectableObservable<Boolean> connectableObservable;
    private ObservableEmitter<Boolean> emitor;
    public SettingsDataChangeObserver()
    {
        Observable<Boolean> observable=Observable.create(e -> emitor=e);
        connectableObservable=observable.publish();
        connectableObservable.share();
        connectableObservable.replay();
        connectableObservable.connect();
    }
    public ConnectableObservable<Boolean> getObservable()
    {
        return connectableObservable;
    }

    public void publishData(Boolean flag)
    {
        if(emitor!=null)
        {
            emitor.onNext(flag);
        }
    }
}

