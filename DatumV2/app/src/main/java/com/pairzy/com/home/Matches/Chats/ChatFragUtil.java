package com.pairzy.com.home.Matches.Chats;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.home.Matches.Chats.Model.ChatListAdapter;
import com.pairzy.com.home.Matches.Chats.Model.ChatListData;
import com.pairzy.com.home.Matches.Chats.Model.ChatListItem;
import com.pairzy.com.home.Matches.Chats.Model.MatchListAdapter;
import com.pairzy.com.util.ChatOptionDialog.ChatOptionDialog;
import com.pairzy.com.util.ReportUser.ReportUserDialog;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
/**
 * @since  4/27/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class ChatFragUtil
{
    public static final String HORIZONTAL_LIST="horizontal_list";
    public static final String VERTICAL_LIST="vertical_list";

    @Provides
    @FragmentScoped
    ArrayList<ChatListData> getMatchListItemPojo()
    {
        ArrayList<ChatListData> arrayList = new ArrayList<>();
        ChatListData firstBoostItem = new ChatListData();
        firstBoostItem.setForBoost(true);
        arrayList.add(firstBoostItem);
        return arrayList;
    }

    @Provides
    @FragmentScoped
    ArrayList<ChatListItem> getChatListItemPojo()
    {
        return new ArrayList<>();
    }

    @Provides
    @FragmentScoped
    MatchListAdapter getMatchListAdapter(Activity activity,ArrayList<ChatListData> list, TypeFaceManager typeFaceManager)
    {
        return new MatchListAdapter(activity,list,typeFaceManager);
    }

    @Provides
    @FragmentScoped
    ChatListAdapter getChatListAdapter(Activity activity,ArrayList<ChatListItem> chat_item,TypeFaceManager typeFaceManager)
    {
        return new ChatListAdapter(activity,chat_item,typeFaceManager);
    }

    @Named(HORIZONTAL_LIST)
    @Provides
    @FragmentScoped
    LinearLayoutManager getHorizontalLayoutManger(Activity activity)
    {
        return new LinearLayoutManager(activity,LinearLayoutManager.HORIZONTAL, false);
    }

    @Named(VERTICAL_LIST)
    @Provides
    @FragmentScoped
    LinearLayoutManager getVerticalLayoutManger(Activity activity)
    {
        return new LinearLayoutManager(activity);
    }

    @Provides
    @FragmentScoped
    ArrayList<String> reportReasonList(){
        return new ArrayList<>();
    }


    @Provides
    @FragmentScoped
    ReportUserDialog provideReportUserDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility){
        return new ReportUserDialog(activity,typeFaceManager,utility);
    }

    @Provides
    @FragmentScoped
    ChatOptionDialog provideChatOptionDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility){
        return new ChatOptionDialog(activity,typeFaceManager,utility);
    }

}
