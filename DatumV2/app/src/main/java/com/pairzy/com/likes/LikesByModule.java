package com.pairzy.com.likes;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>LikesByModule</h2>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */
@Module
public abstract class LikesByModule {

    @Binds
    @ActivityScoped
    abstract LikesByContract.Presenter providePresenter(LikesByPresenter presenter);

    @Binds
    @ActivityScoped
    abstract LikesByContract.View provideView(LikesByActivity  likesByActivity);

    @Binds
    @ActivityScoped
    abstract Activity provideActivity(LikesByActivity activity);

}
