package com.pairzy.com.likes;

import android.app.Activity;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.likes.model.LikesAdapter;
import com.pairzy.com.likes.model.LikesDataPojo;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * <h2>LikesByUtil</h2>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */
@Module
public  class LikesByUtil {
    public static final String LIKES_LAYOUT_MANAGER ="likes_layout_manager" ;
    public static final String LIKES_POST = "likes_post";

    @Named(LIKES_POST)
    @Provides
    @ActivityScoped
    ArrayList<LikesDataPojo> providelikesPostPojos() {
        return  new ArrayList<>();
    }

    @Provides
    @ActivityScoped
    LikesAdapter getCommentPostAdapter(@Named(LIKES_POST)ArrayList<LikesDataPojo> list, TypeFaceManager typeFaceManager) {
        return new LikesAdapter(list,typeFaceManager);
    }

    @Named(LikesByUtil.LIKES_LAYOUT_MANAGER)
    @Provides
    @ActivityScoped
    LinearLayoutManager provideLinearLayoutManager(Activity activity)
    {
        return  new LinearLayoutManager(activity);
    }
}
