package com.pairzy.com.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;


public class SubscriptionData
{
    @SerializedName("subscription")
    @Expose
    private ArrayList<Subscription> subscription = null;

    public ArrayList<Subscription> getSubscription()
    {
        return subscription;
    }
    public void setSubscription(ArrayList<Subscription> subscription)
    {
        this.subscription = subscription;
    }
}