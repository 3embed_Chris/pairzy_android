package com.pairzy.com.data.model;

/**
 * Created by ankit on 4/9/18.
 */

public class DiscoverTabPositionHolder {
    int currentPosition = 1;

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }
}
