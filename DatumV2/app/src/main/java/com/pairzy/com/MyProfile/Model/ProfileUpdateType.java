package com.pairzy.com.MyProfile.Model;

public enum ProfileUpdateType {
    PROFILE_PICTURE(1),
    PROFILE_VIDEO(2),
    OTHER_IMAGES(3),
    PROFILE_ABOUT(4),
    DEFAULT(10);
    public int value;

    ProfileUpdateType(int value) {
        this.value = value;
    }
}
