package com.pairzy.com.editProfile.Model;

import android.content.Context;
import android.net.Uri;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.pairzy.com.R;

import java.io.File;
import java.util.ArrayList;

import static android.view.View.GONE;

/**
 * <h>ProfilePhotoAdapter class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public class ProfilePhotoAdapter extends RecyclerView.Adapter {

    private ArrayList<ProfilePicture> profilePictures = new ArrayList<>();
    private PicAdapterClickCallback callback = null;
    private Context context;
    private ViewHolderClickCallback vhCallback;


    public ProfilePhotoAdapter(Context context, ArrayList<ProfilePicture> profilePictures) {
        this.profilePictures = profilePictures;
        this.context = context;
        initCallback();
    }

    private void initCallback() {
        vhCallback = (view, position) -> {
            switch (view.getId()){
                case R.id.rl_close:
                    if(callback != null)
                        callback.onPicRemove(position);
                    break;
                case R.id.sdv_profile_pic:
                    if(callback != null)
                        callback.onPicPreview(profilePictures.get(position).getProfilePic(),view);
                    break;
                case R.id.ll_set_profile:
                    if(callback != null)
                        callback.onSetAsProfilePic(position);
                    break;
                case R.id.iv_empty_item_icon:
                case R.id.rl_empty_item:
                    if(callback != null)
                        callback.onEmptyPicClick(position);
            }
        };
    }

    public void setAdapterClickCallback(PicAdapterClickCallback clickCallback){
        this.callback = clickCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case ProfilePicture.PROFILE_PIC:
                view =LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_picture_item, parent, false);
                return new ProfilePicItemViewHolder(view,vhCallback);
            case ProfilePicture.PROFILE_PIC_LOADING:
                view =LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_picture_loading, parent, false);
                return new ProfilePicLoadingViewHolder(view);
            case ProfilePicture.PROFILE_PIC_EMPTY:
                view =LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_picture_empty, parent, false);
                return new ProfilePicEmptyViewHolder(view,vhCallback);
            default:
                view =LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_picture_item, parent, false);
                return new ProfilePicItemViewHolder(view,vhCallback);
        }
    }


    @Override
    public int getItemViewType(int position) {
        ProfilePicture profilePic = profilePictures.get(position);
        switch (profilePic.getType()){
            case 0:
                return ProfilePicture.PROFILE_PIC;
            case 1:
                return ProfilePicture.PROFILE_PIC_LOADING;
            case 2:
                return ProfilePicture.PROFILE_PIC_EMPTY;
            default:
        }
        return super.getItemViewType(position);
    }


    public void setData(ArrayList<ProfilePicture> profilePictures){
        this.profilePictures = profilePictures;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ProfilePicture profilePic  = profilePictures.get(position);
        switch (profilePic.getType()){
            case ProfilePicture.PROFILE_PIC:
                try {
                    bindProfilePicItem((ProfilePicItemViewHolder) holder);
                }catch (Exception ignored){}
                break;
            case ProfilePicture.PROFILE_PIC_LOADING:
                try{
                    bindLoadingPicItem((ProfilePicLoadingViewHolder)holder);
                }catch (Exception e){}
                break;
            case ProfilePicture.PROFILE_PIC_EMPTY:
                break;
            default:
        }
    }

    private void bindLoadingPicItem(ProfilePicLoadingViewHolder holder) {
        ProfilePicture profilePic = profilePictures.get(holder.getAdapterPosition());
        if(!TextUtils.isEmpty(profilePic.getProfilePicPath())){
            holder.sdvProfilePic.setImageURI(Uri.fromFile(new File(profilePic.getProfilePicPath())));
        }
    }

    private void bindProfilePicItem(ProfilePicItemViewHolder holder) {
        ProfilePicture profilePic = profilePictures.get(holder.getAdapterPosition());
        holder.sdvProfilePic.setImageURI(Uri.parse(profilePic.getProfilePic()));
        if(profilePictures.size() == 2)
            holder.rlClose.setVisibility(GONE);
        else
            holder.rlClose.setVisibility(View.VISIBLE);

        if(profilePic.isProfilePic()){
            holder.rlClose.setVisibility(GONE);
        }
        setSelected(profilePic.isProfilePic(),holder.ivProfileCheckBox);
    }

    public void setSelected(boolean isSelected, ImageView compatCheckBox)
    {
        if(isSelected)
        {
            compatCheckBox.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.selected_tick_circle));
        }else
        {
            compatCheckBox.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.selected_untick_circle));
        }
    }


    @Override
    public int getItemCount() {
        return profilePictures.size();
    }

}
