package com.pairzy.com.home.Dates.Pending_page;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.data.model.DateEvent;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.CustomAlertDialog.CustomAlertDialog;
import com.pairzy.com.home.Dates.CustomAlertDialog.CustomAlertDialogCallBack;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Dates.Model.DateResponseType;
import com.pairzy.com.home.Dates.Model.DateType;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;
import com.pairzy.com.home.Dates.Pending_page.Model.PendingListAdapter;
import com.pairzy.com.home.Dates.Pending_page.Model.PendingModel;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.CalendarEventHelper;
import com.pairzy.com.util.CustomObserver.CoinBalanceObserver;
import com.pairzy.com.util.CustomObserver.DateObserver;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.SpendCoinDialog.CoinDialogCallBack;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.pairzy.com.util.ApiConfig.PostDateResponse;

/**
 * <h2>PastDateFrgPresenter</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */

public class PendingFrgPresenter implements PendingFrgContract.Presenter,ItemActionCallBack,App_permission.Permission_Callback,CoinDialogCallBack,CustomAlertDialogCallBack
{
    private final String CALENDER_TAG = "calender_tag";
    private CompositeDisposable compositeDisposable;
    private PendingFrgContract.View view;
    public static final String  RESCHEDULE_TAG = "reschedule_tag";
    int currentPosition = -1;
    private DateListPojo dateListPojo;
    private  PendingFrg pendingFrg;
    private DateResponseType currentDateResponse;

    @Inject
    NetworkService service;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    PendingModel model;
    @Inject
    NetworkStateHolder holder;
    @Inject
    DateObserver dateObserver;
    @Inject
    CalendarEventHelper calendarEventHelper;
    @Inject
    App_permission app_permission;
    @Inject
    Activity activity;
    @Inject
    Utility utility;
    @Inject
    CoinDialog spendCoinDialog;
    @Inject
    CoinBalanceObserver coinBalanceObserver;
    @Inject
    CustomAlertDialog customAlertDialog;
    private DateResponseType dateResponseType = DateResponseType.ACCEPTED;


    @Inject
    PendingFrgPresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(Object view)
    {
        this.view= (PendingFrgContract.View) view;
        this.pendingFrg = (PendingFrg) view;
    }

    @Override
    public void dropView() {
        this.view = null;
        compositeDisposable.clear();
    }

    @Override
    public void observeDateListChange(){
        dateObserver.getObservable().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(Boolean flag) {
                        if(flag){
                            if(view != null){
                                view.refreshList();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void notifyPendingAdapter() {
        if(model.getListSize() == 0){
            if(view != null)
                view.refreshList();
        }
        else{
            if(view != null)
                view.notifyAdapter();
        }
    }

    @Override
    public void doLoadMore(int currentPos) {
        if(model.loadMoreRequired(currentPos))
        {
            if(view!=null)
                view.doLoadMore();
        }
    }

    @Override
    public void setAdapterCallBack(PendingListAdapter pendingListAdapter) {
        pendingListAdapter.setCallBack(this);
    }

    @Override
    public boolean checkDateExist() {
        if(model.checkDatesExist())
            return true;
        return false;
    }

    @Override
    public void parseDateData(int requestCode, int resultCode, Intent data) {
        if(requestCode == PendingFrg.RESCHEDULE_REQ_CODE){
            if(resultCode == -1){
                DateListPojo dateData = (DateListPojo) data.getSerializableExtra("date_data");
                if(dateData != null)
                    callDateResponseApi(dateData, DateResponseType.RESCHEDULE);
            }
        }
    }

    /*
     * help to decide date screen to launch.
     */
    private void launchRescheduleScreen(DateListPojo dateListPojo){
        if(dateListPojo != null) {
            if (dateListPojo.getDateType() == DateType.IN_PERSON_DATE.getValue()) {
                if(view != null)
                    view.launchPhyDateScreen(dateListPojo);
            }
            else if (dateListPojo.getDateType() == DateType.VIDEO_DATE.getValue()) {
                if(view != null)
                    view.launchCallDateScreen(dateListPojo);
            }
            else if (dateListPojo.getDateType() == DateType.AUDIO_DATE.getValue()) {
                if(view != null)
                    view.launchCallDateScreen(dateListPojo);
            }
        }
    }
    @Override
    public void onClick(int id, int position) {
        DateListPojo dateListPojo = model.getPendingDateItem(position);
        currentPosition = position;
        switch (id){
            case R.id.reschedule_button:
                    if(dateListPojo != null) {
                        //launchRescheduleScreen(dateListPojo);
                        currentDateResponse = DateResponseType.RESCHEDULE;
                        loadAlertDialog(dateListPojo);
                    }
                break;
            case R.id.cancel_button:
                if(dateListPojo != null){
                    currentDateResponse = DateResponseType.DENIED;
                    loadAlertDialog(dateListPojo);
                    //callDateResponseApi(dateListPojo,DateResponseType.DENIED);
                }
                break;
            case R.id.accept_button:
                if(dateListPojo != null){
                    currentDateResponse = DateResponseType.ACCEPTED;
                    loadAlertDialog(dateListPojo);
                    //callDateResponseApi(dateListPojo,DateResponseType.ACCEPTED);
                }
                break;
            case R.id.reject_button:
                if(dateListPojo != null){
                    currentDateResponse = DateResponseType.DENIED;
                    loadAlertDialog(dateListPojo);
                    //callDateResponseApi(dateListPojo,DateResponseType.DENIED);
                }
                break;
            case R.id.rl_header_confirm:
            case R.id.rl_header_reschedule:
            case R.id.main_image_view:
                if(dateListPojo != null){
                    if(view != null)
                        view.launchUserProfile(dateListPojo);
                }
                break;
            default:
        }
    }

    private void loadAlertDialog(DateListPojo dateListPojo) {
        DateType dateType = DateType.IN_PERSON_DATE;
        if(dateListPojo.getDateType() == DateType.AUDIO_DATE.getValue()){
            dateType = DateType.AUDIO_DATE;
        }
        else if(dateListPojo.getDateType() == DateType.VIDEO_DATE.getValue()){
            dateType = DateType.VIDEO_DATE;
        }
        customAlertDialog.showDialog(dateListPojo.getOpponentName(),dateType,currentDateResponse,this);
    }

    private void launchWalletEmptyDialog(){
        spendCoinDialog.showDialog("",true,"",activity.getString(R.string.date_reschedule_empty_wallet_msg),"",this,true,false);
    }

    private void callDateResponseApi(DateListPojo dateListPojo, DateResponseType dateResponseType) {

        if(holder.isConnected()) {
            //need response type after permission result
            this.dateResponseType = dateResponseType;
            this.dateListPojo = dateListPojo;
            if(dateResponseType == DateResponseType.RESCHEDULE){
                model.replaceDateItem(dateListPojo,currentPosition);
            }
            else {
                DateListPojo removedDate = model.removeDateFromList(currentPosition);
                if (removedDate != null) {
                    if (view != null) {
                        if (dateResponseType == DateResponseType.ACCEPTED)
                            view.addToTheUpcomingList(removedDate);
                        else if(dateResponseType == DateResponseType.DENIED){
                            //canceled from upcoming
                            removedDate.setProposedOn(System.currentTimeMillis());
                            try {
                                PastDateListPojo pastDateListPojo = new PastDateListPojo(removedDate, utility.formatString(removedDate.getOpponentName()));
                                view.addTothePastDate(pastDateListPojo);
                            }catch (Exception e){}
                        }
                    }
                }
            }
            if(view != null)
                view.notifyAdapter();
            Map<String, Object> body = new HashMap<>();
            body.put(PostDateResponse.RESPONSE, dateResponseType.getValue());
            body.put(PostDateResponse.DATE_ID, dateListPojo.getDataId());
            body.put(PostDateResponse.LATITUDE, dateListPojo.getLatitude());
            body.put(PostDateResponse.LONGITUDE, dateListPojo.getLongitude());
            body.put(PostDateResponse.PROPOSED_ON, dateListPojo.getProposedOn());
            body.put(PostDateResponse.DATE_TYPE, dateListPojo.getDateType());
            body.put(PostDateResponse.PLACE_NAME, dateListPojo.getPlaceName());
            service.postDateResponse(dataSource.getToken(), "en", body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                if (view != null)
                                    view.notifyAdapter();

                                if(view != null)
                                    view.updatePendingDateCount(model.getListSize());
                                if(dateResponseType == DateResponseType.RESCHEDULE){
                                    try {
                                        model.parsePlanDateResponse(value.body().string());
                                        coinBalanceObserver.publishData(true);
                                    }catch (Exception e){}
                                }
                                if(dateResponseType == DateResponseType.RESCHEDULE || dateResponseType == DateResponseType.DENIED){
                                    checkForCalenderPermission();
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            if(value.code() == 402)
                            {
                                try {
//                                    if (view != null)
//                                        view.showError(activity.getString(R.string.insufficient_balance_msg));
                                    /**
                                     * Opening the empty wallet dialog.
                                     */
                                    launchWalletEmptyDialog();
                                }catch (Exception ignored){}
                            }
                            else{
                                try {
                                    if(view != null)
                                        view.showError(value.errorBody().string());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            if (view != null) {
                                view.showError(activity.getString(R.string.failed_to_update_date_response));
                            }
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(R.string.no_internet_error);
        }
    }


    private void setDateReminder() {
        if(dateListPojo != null) {
            String personName = dateListPojo.getOpponentName();
            String location = dateListPojo.getPlaceName();
            String _userId = dateListPojo.getOpponentId();
            String dateType = "In person date";
            if(dateListPojo.getDateType() ==DateType.AUDIO_DATE.getValue()){
                dateType = "Audio Date";
            }else if(dateListPojo.getDateType() ==DateType.VIDEO_DATE.getValue()){
                dateType = "Video Date";
            }

            deletePreviousReminder(_userId);
            int eventId = calendarEventHelper.addDateReminder(dateListPojo.getProposedOn(), personName, location,dateType);
            DateEvent dateEvent = new DateEvent();
            dateEvent.setUserId(_userId);
            dateEvent.setEventId(String.valueOf(eventId));
            model.saveEventId(dateEvent);
        }
    }


    private void deletePreviousReminder(String userId) {
        String eventId = model.checkIfReminderExist(userId);
        if(!TextUtils.isEmpty(eventId)){
            calendarEventHelper.deleteEvent(eventId);
        }
    }


    private void checkForCalenderPermission() {
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.READ_CALENDER);
        permissions.add(App_permission.Permission.WRITE_CALENDER);
        app_permission.getPermission_for_Sup_v4Fragment(CALENDER_TAG,permissions,pendingFrg,this);
    }


    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        if(this.dateResponseType == DateResponseType.RESCHEDULE)
            setDateReminder();
        else{
            if(dateListPojo != null)
            deletePreviousReminder(dateListPojo.getOpponentId());
        }
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        app_permission.show_Alert_Permission(activity.getString(R.string.calender_access_text),activity.getString(R.string.calender_access_subtitle),
                activity.getString(R.string.location_acess_message),stringArray);
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean parmanent) {
    }

    @Override
    public void onCoinButton(boolean dontShowAgain) {
    }


    @Override
    public void onBuyCoin() {
        if(view != null)
            view.launchCoinWallet();
    }

    @Override
    public void onOk() {
        DateListPojo dateListPojo = model.getPendingDateItem(currentPosition);
        if(dateListPojo != null) {
            if(currentDateResponse == DateResponseType.RESCHEDULE)
                launchRescheduleScreen(dateListPojo);
            else
                callDateResponseApi(dateListPojo,currentDateResponse);
        }
    }
}
