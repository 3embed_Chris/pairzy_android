package com.pairzy.com.userProfile.Model;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.androidinsta.com.ImageData;
import com.pairzy.com.AppController;
import com.pairzy.com.BaseModel;
import com.pairzy.com.BuildConfig;
import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.boostDetail.model.SubsPlan;
import com.pairzy.com.boostDetail.model.SubsPlanResponse;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.Subscription;
import com.pairzy.com.data.model.SubscriptionResponse;
import com.pairzy.com.data.model.coinBalance.CoinResponse;
import com.pairzy.com.data.model.coinBalance.Coins;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigResponse;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.userProfile.Profile_util;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.ReportUser.ReportReasonResponse;
import com.pairzy.com.util.Utility;
import com.suresh.innapp_purches.SkuDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * <h2>DataModel</h2>
 * <P>
 *
 * </P>
 * @since  4/4/2018.
 * @author 3Embd.
 * @version 1.0.
 */
public class DataModel extends BaseModel
{
    private  UserDataDetails item;

    private String userId;
    @Inject
    ArrayList<String> reportReasonList;
    @Inject
    ViewModel viewModel;
    @Inject
    ArrayList<MediaPojo> media_list;
    @Inject
    UserMediaAdapter adapter;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    ArrayList<SubsPlan> subsPlanList;
    @Inject
    Activity activity;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    CoinBalanceHolder coinBalanceHolder;



    @Named(Profile_util.USER_PROFILE_MOMEMT)
    @Inject
    ArrayList<MomentsData> momentsData;

    private String name;
    @Inject
    Utility utility;
    private UserProfileData userData;
    private String instaGramProfileId,instagramName, instaToken;
    private boolean aboutTextEmpty;
    private boolean remainsLikesCount;

    @Inject
    DataModel(){}

    private ArrayList<UserPreference> userPreferences;
    private ArrayList<View> views=new ArrayList<>();

    public String userProfileUrl()
    {
        Log.d("23er",""+BuildConfig.BASEURL+"profile/"+ userId);
        Log.d("23er",""+dataSource.getToken());
        return BuildConfig.BASEURL+"profile/"+ userId;
    }

    public String userProfileUrl(String profileId)
    {
        Log.d("23er",""+BuildConfig.BASEURL+"profile/"+profileId);
        Log.d("23er",""+dataSource.getToken());
        return BuildConfig.BASEURL+"profile/"+profileId;
    }


    /*
     *Creating user image list. */
    public UserDataDetails createMediaList(String data)
    {
        item=utility.getGson().fromJson(data,UserDataDetails.class);
        userId =item.getOpponentId();
        Log.d("23er",""+ userId);
        manageMediaList(item);
        int position_data=item.getCurrentImagePos();
        if(position_data>media_list.size()-1)
        {
            item.setCurrentImagePos(0);
        }
        return item;
    }

    public void createMediaList()
    {
        Log.d("23er",""+ userId);
        manageMediaList(userData);
    }


    /*
     *Providing the user name. */
    public String getUserName()
    {
        if(item!=null && item.getFirstName() != null)
        {
            return item.getFirstName();
        }
        else if(userData != null) {
            return userData.getFirstName();
        }
        return "";
    }

    /*
     * Parsing the data from the server*/
    public void parseData(String response) throws EmptyData,DataParsingException
    {
        Log.d("outq2", ""+response);
        try
        {
            UserResponseHolder result_data=utility.getGson().fromJson(response,UserResponseHolder.class);
            UserProfileData data=result_data.getData();
            if(data!=null)
            {
                userData = data;
                momentsData.clear();
                if(data.getMoments()!=null)
                    momentsData.addAll(data.getMoments());

                userId = data.getId();
                name=data.getFirstName();
                instaGramProfileId=data.getInstaGramProfileId();
                instaToken = data.getInstaGramToken();
                instagramName=data.getInstagramName();
                userPreferences=data.getmyPreferences();
                userPreferences.size();
            }else
            {
                throw new EmptyData("User Details is empty");
            }
        }catch (Exception e)
        {
            throw new DataParsingException(e.getMessage());
        }
    }
    public ArrayList<MomentsData> getMomentList() {
        return momentsData;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNameAge()
    {
        try {
            if (userData.getAge().getIsHidden() == 0)
                return utility.formatString(name + ", " + userData.getAge().getValue());
            else
                return utility.formatString(name + " ");
        }catch (Exception e){}
        return utility.formatString(name + " ");
    }


    /*
     * Getting the details view*/
    public ArrayList<View> getDetailsView()
    {
        views.clear();
        userPreferences.size();
        for(UserPreference preference: userPreferences){
            View view1=viewModel.addDetailsView(preference.getData(),preference.getTitle());
            views.add(view1);
        }
        return views;
    }


    public View getLoadingView()
    {
        return viewModel.loadingView();
    }


    public View getErrorOnLoadingView()
    {
        return viewModel.errorOnLoadingView(false);
    }

    /*
     * Media total*/
    public int getMediaTotal()
    {
        return media_list.size();
    }

    /**
     *Storing the media file in a local list
     * to mange the media file in local list. */
    private void manageMediaList(UserProfileData userData)
    {
        media_list.clear();
        if(!TextUtils.isEmpty(userData.getProfileVideoThumbnail()))
        {
            MediaPojo video_item=new MediaPojo();
            video_item.setVideo_thumbnail(userData.getProfileVideoThumbnail());
            video_item.setVideo_url(userData.getProfileVideo());
            video_item.setVideo(true);
            media_list.add(video_item);
        }
        /*
         *Adding the user image in list */
        MediaPojo user_image=new MediaPojo();
        user_image.setImage_url(userData.getProfilePic());
        user_image.setVideo(false);
        media_list.add(user_image);
        try
        {
            for(String other_img:userData.getOtherImages())
            {
                MediaPojo other_media=new MediaPojo();
                other_media.setImage_url(other_img);
                other_media.setVideo(false);
                media_list.add(other_media);
            }
        }catch (Exception e){}
        adapter.notifyDataSetChanged();
    }

    /**
     *Storing the media file in a local list
     * to mange the media file in local list. */
    private void manageMediaList(UserDataDetails item)
    {
        media_list.clear();
        if(!TextUtils.isEmpty(item.getProfileVideoThumbnail()))
        {
            MediaPojo video_item=new MediaPojo();
            video_item.setVideo_thumbnail(item.getProfileVideoThumbnail());
            video_item.setVideo_url(item.getProfileVideo());
            video_item.setVideo(true);
            media_list.add(video_item);
        }

        /*
         *Adding the user image in list */
        MediaPojo user_image=new MediaPojo();
        user_image.setImage_url(item.getProfilePic());
        user_image.setVideo(false);
        media_list.add(user_image);
        try
        {
            for(String other_img:item.getOtherImages())
            {
                MediaPojo other_media=new MediaPojo();
                other_media.setImage_url(other_img);
                other_media.setVideo(false);
                media_list.add(other_media);
            }
        }catch (Exception e){}
        adapter.notifyDataSetChanged();
    }

    public String getUser_id()
    {
        return userId;
    }

    public void parseReportReasonData(String response) {
        try {
            ReportReasonResponse reasonResponse = utility.getGson().fromJson(response,ReportReasonResponse.class);
            if(reasonResponse.getData() != null){
                reportReasonList = (ArrayList<String>) reasonResponse.getData();
            }
        }catch (Exception e){}
    }

    public boolean isReportReasonsEmpty() {
        return reportReasonList.isEmpty();
    }

    public ArrayList<String> getReasonsList() {
        return reportReasonList;
    }

    public boolean isDistanceHidden() {
        try {
            return userData.getDistance().getIsHidden() == 1;
        }catch (Exception ignored){}
        return false;
    }

    public double getUserDistance() {
        try {
            return userData.getDistance().getValue();
        }catch (Exception e){}
        return 0.0;
    }

    public String getUserWork() {
        try {
            return userData.getWork();
        }catch (Exception e){}
        return "";
    }

    public String getUserSchool() {
        try {
            return userData.getSchool();
        }catch (Exception e){}
        return "";
    }

    public boolean isUserOnline() {
        try {
            return userData.getOnlineStatus() > 0;
        }catch (Exception e){}
        return false;
    }

    public boolean isUnliked() {
        try {
            return userData.getUnliked() > 0;
        }catch (Exception ignored){}
        return false;
    }

    public boolean isSuperliked() {
        try {
            return userData.getSuperliked() > 0;
        }catch (Exception ignored){}
        return false;
    }

    public boolean isLiked() {
        try {
            return userData.getLiked() > 0;
        }catch (Exception ignored){}
        return false;
    }


    //BoostResponse dialog methods
    public boolean isSubsPlanEmpty() {
        return  subsPlanList.isEmpty();
    }

    public void selectMiddleItem() {
        if(!isSubsPlanEmpty()){
            for(SubsPlan subsPlan : subsPlanList)
                if(subsPlan.isSelected())
                    return;
            int pos = getSubsPlanListSize() / 2;
            SubsPlan subsPlan = subsPlanList.get(pos);
            subsPlan.setSelected(true);
            subsPlan.setHeaderTag("MOST POPULAR");
        }
    }

    public int getSubsPlanListSize() {
        return subsPlanList.size();
    }

    public ArrayList<SubsPlan> getSubsPlanList() {
        return subsPlanList;
    }

    public void parseSubsPlanList(String response) {
        try{
            subsPlanList.clear();
            SubsPlanResponse subsPlanResponse = utility.getGson().fromJson(response,SubsPlanResponse.class);
            ArrayList<SubsPlan> subsPlans = subsPlanResponse.getData();
            if(subsPlans != null){
                subsPlanList.addAll(subsPlans);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getSubsPurchaseId(int position) {
        try {
            if (position < getSubsPlanListSize()) {
                return subsPlanList.get(position).getActualId();
            }
        }catch (Exception ignored){}
        return "";
    }


    public boolean isUserBlockedByMe() {
        try{
            return userData.getBlockedByMe() > 0;
        }catch (Exception e){}
        return false;
    }

    public UserProfileData getUserProfileData() {
        return userData;
    }


    public boolean isInstaDataExist()
    {
        return !TextUtils.isEmpty(instaGramProfileId);
    }

    public String getInstaGramProfileId()
    {
        return instaGramProfileId;
    }

    public String getOtherUserInstaToken(){
        return instaToken;
    }


    public String getInstagramName() {
        return instagramName;
    }


    public List<List<ImageData>> prepareInstaResponse(ArrayList<ImageData> response)
    {
        response.add(new ImageData());
        return chopIntoParts(response,6);
    }

    private  <T>List<List<T>> chopIntoParts(final List<T> ls, int part_size)
    {
        final List<List<T>> lsParts = new ArrayList<List<T>>();
        int lat_position=part_size;
        int start_pos=0;
        for( int i=0;i<ls.size();i++ )
        {
            boolean isLast=false;
            if(lat_position>ls.size()||lat_position==ls.size())
            {
                isLast=true;
                lat_position=ls.size();
            }
            lsParts.add( new ArrayList<T>(ls.subList(start_pos,lat_position)));
            if(isLast)
            {
                break;
            }else
            {
                start_pos=lat_position;
                lat_position=lat_position+part_size;
            }
        }
        return lsParts;
    }


    /**
     * @param id : instagram id
     * @return parameters
     */
    public Map<String, Object> updateInstaId(String id, String name, String token)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.UpdateInstaId.ID, id);
        map.put(ApiConfig.UpdateInstaId.TOKEN, token);
        map.put(ApiConfig.UpdateInstaId.USER_NAME, name);
        return map;
    }


    public List<String> collectsIds()
    {
        List<String> items=new ArrayList<>();
        for(SubsPlan item :subsPlanList)
        {
            items.add(item.getActualId());
        }
        return items;
    }


    public void updateDetailsData(List<SkuDetails> details)
    {
        for(SkuDetails item:details)
        {
            Log.d("8ry", ":"+item.toString());
            String id=item.getProductId();
            for(SubsPlan product :subsPlanList)
            {
                Log.d("8ry", ":"+product.toString());
                if(id.equals(product.getActualId()))
                {
                    product.setPrice_text(item.getPriceText());
                    product.setCurrencySymbol(item.getCurrency());
                }
            }
        }
    }

    public void clearProductList()
    {
        subsPlanList.clear();
    }

    public String extractIDFromKey(String productID)
    {
        for(SubsPlan plan:subsPlanList)
        {
            if(plan.getActualId().equals(productID))
            {
                return plan.getId();
            }
        }
        return "";
    }


    public void setUserBlockedByMe(boolean userBlockedByMe) {
        if(userData != null) {
            userData.setBlockedByMe(userBlockedByMe?1:0);
            AppController.getInstance().getDbController().addBlockedUser(AppController.getInstance().getBlockedDocId(),getUser_id(),getUser_id(),userBlockedByMe,userData.getBlocked()>0);
        }
    }

    public void parseSubscription(String response) {
        try{
            Log.d("343rt", ":"+response);
            SubscriptionResponse subsResponse = utility.getGson().fromJson(response,SubscriptionResponse.class);
            if(subsResponse != null){
                Subscription subscription = subsResponse.getData().getSubscription().get(0);
                if(subscription != null){
                    String jsonSubs = utility.getGson().toJson(subscription,Subscription.class);
                    dataSource.setSubscription(jsonSubs);
                }
            }
        }catch (Exception e){}
    }

    public void setOnRetryCallback(ViewModel.OnRetryCallback onRetryCallback) {
        viewModel.setOnRetryCallback(onRetryCallback);
    }

    public View getInternetErrorView() {
        return viewModel.errorOnLoadingView(true);
    }

    public boolean isMatched() {
        try{
            return userData.getIsMatch() > 0;
        }catch (Exception e){}
        return false;
    }

    public boolean isDialogDontNeedToShowForChat() {
        return dataSource.getShowChatCoinDialog();
    }

    public void parseCoinConfig(String response)
    {
        try
        {
            CoinConfigResponse coinConfigResponse =
                    utility.getGson().fromJson(response,CoinConfigResponse.class);
            if(!coinConfigResponse.getData().isEmpty())
                coinConfigWrapper.setCoinData(coinConfigResponse.getData().get(0));
        }catch (Exception e){}
    }

    public void updateShowCoinDialogForChat(boolean dontShowAgain) {
        //update don't show pref
        dataSource.setShowChatCoinDialog(dontShowAgain);
    }

    public boolean isDialogDontNeedToShow() {
        return dataSource.getShowSuperlikeCoinDialog();
    }

    public void updateSpendCoinShowPref(boolean dontShowAgain) {
        //update don't show pref
        dataSource.setShowSuperlikeCoinDialog(dontShowAgain);
    }

    public void parseCoinBalance(String response) {
        try{
            CoinResponse coinResponse = utility.getGson().fromJson(response,CoinResponse.class);
            Coins coins = coinResponse.getData().getCoins();
            coinBalanceHolder.setCoinBalance(String.valueOf(coins.getCoin()));
            coinBalanceHolder.setUpdated(true);
        }catch (Exception e){
        }
    }

    public String getAboutText() {
        return userData.getAbout();
    }

    public boolean isAboutTextEmpty() {
        return TextUtils.isEmpty(getAboutText());
    }

    public boolean isUserFemale() {
        try {
            return userData.getGender().equalsIgnoreCase("female");
        }catch (Exception e){
            e.printStackTrace();
            return true;
        }
    }

    public int getRemainsLikeCount() {
        try {
            String str = dataSource.getRemainsLikesCount();
            if(str.equalsIgnoreCase("unlimited"))
                return 1;
            return Integer.parseInt(str);
        }catch (Exception e){
            return 0;
        }
    }

    public boolean isEnoughWalletBalanceToSuperLike() {

        if(coinBalanceHolder.getCoinBalance() != null && coinConfigWrapper.getCoinData() != null){
            int coinRequired = coinConfigWrapper.getCoinData().getSuperLike().getCoin();
            int walletBalance = Integer.parseInt(coinBalanceHolder.getCoinBalance());

            if(coinRequired <= walletBalance){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public boolean isEnoughWalletBalanceToChat() {

        if(coinBalanceHolder.getCoinBalance() != null && coinConfigWrapper.getCoinData() != null){
            int coinRequired = coinConfigWrapper.getCoinData().getPerMsgWithoutMatch().getCoin();
            int walletBalance = Integer.parseInt(coinBalanceHolder.getCoinBalance());

            if(coinRequired <= walletBalance){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public String getCoinBalance() {
        return coinBalanceHolder.getCoinBalance();
    }

    public String getCoinForUnmatchChat() {
        try{
            return ""+coinConfigWrapper.getCoinData().getPerMsgWithoutMatch().getCoin();
        }catch (Exception ignored){}
        return "";
    }

    public void refreshBlockedFromMqtt() {
        try {
            if (userData != null) {
                userData.setBlocked(AppController.getInstance().getDbController().checkIfBlocked(AppController.getInstance().getBlockedDocId(),getUser_id())?1:0);
                userData.setBlockedByMe(AppController.getInstance().getDbController().checkIfBlockedByMe(AppController.getInstance().getBlockedDocId(),getUser_id())?1:0);
            }
        }catch (Exception e){}
    }

    public double getUserLocation() {
        try {
            if (userData != null) {
                return userData.getDistance().getValue();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0.0;
    }

    public boolean isLocationNeedToShow() {
        try{
            return ! (userData.getDistance().isHidden > 0);
        }catch (Exception e){
        }
        return false;
    }
}
