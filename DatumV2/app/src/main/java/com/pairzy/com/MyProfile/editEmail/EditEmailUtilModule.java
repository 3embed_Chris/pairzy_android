package com.pairzy.com.MyProfile.editEmail;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.TypeFaceManager;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ankit on 27/4/18.
 */

@Module
public class EditEmailUtilModule {

    @Provides
    @ActivityScoped
    DatumProgressDialog datumProgressDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        return  new DatumProgressDialog(activity,typeFaceManager);
    }

}
