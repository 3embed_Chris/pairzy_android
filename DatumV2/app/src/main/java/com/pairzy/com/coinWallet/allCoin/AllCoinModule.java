package com.pairzy.com.coinWallet.allCoin;

import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 *<h>AllCoinModule</h>
 * <p>bind and Provide the helper class instance.</p>
 *@author 3Embed.
 *@since 30/5/18.
 */
@Module
public abstract class AllCoinModule {

    @FragmentScoped
    @Binds
    abstract AllCoinFrag allCoinFrag(AllCoinFrag allCoinFrag);

    @FragmentScoped
    @Binds
    abstract AllCoinContract.Presenter bindsAllCoinPresenter(AllCoinPresenter presenter);

}
