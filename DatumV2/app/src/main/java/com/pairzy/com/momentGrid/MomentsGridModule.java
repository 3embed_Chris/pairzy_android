package com.pairzy.com.momentGrid;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>MomentsGridModule</h2>
 * <P> this is a module class</P>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */

@Module
public abstract class MomentsGridModule {

    @Binds
    @ActivityScoped
    abstract MomentsGridContract.View provideView(MomentsGridActivity momentsVerticalActivity );

    @Binds
    @ActivityScoped
    abstract MomentsGridContract.Presenter providePresenter(MomentsGridPresenter momentsVerticalPresenter );

    @Binds
    @ActivityScoped
    abstract Activity provideActivity(MomentsGridActivity momentsVerticalActivity );

}
