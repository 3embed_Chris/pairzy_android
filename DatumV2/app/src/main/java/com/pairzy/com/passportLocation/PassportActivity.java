package com.pairzy.com.passportLocation;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PassportLocationDataSource;
import com.pairzy.com.locationScreen.locationMap.CustomLocActivity;
import com.pairzy.com.passportLocation.model.PassportAdapter;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.localization.activity.BaseDaggerActivity;

import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h>Settings activity</h>
 * <p> This Settings activity user settings and Logout and Delete account function.</p>
 * @author 3Embed.
 * @since 22/05/18.
 * @version 1.0.
 */
public class PassportActivity extends BaseDaggerActivity implements PassportContract.View
{
    public static final int LOCATION_REQ_CODE = 32;
    @Inject
    App_permission app_permission;
    @Inject
    PassportPresenter presenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    LinearLayoutManager linearLayoutManager;
    @Inject
    PassportAdapter passportAdapter;
    @Inject
    Activity activity;
    @Inject
    PassportLocationDataSource locationDataSource;
    @BindView(R.id.title_text)
    TextView title_text;
    @BindView(R.id.tv_current_location_title)
    TextView tvCurrentLocationTitle;
    @BindView(R.id.tv_current_location)
    TextView tvCurrentLocation;
    @BindView(R.id.progress_location)
    ProgressBar progressLocation;
    @BindView(R.id.location_list)
    RecyclerView recyclerLocation;
    @BindView(R.id.recent_location)
    TextView recent_location;
    @BindView(R.id.add_new_loc_text)
    TextView add_new_loc_text;
    @BindView(R.id.location_check_box)
    ImageView compatCheckBox;
    @BindView(R.id.current_loc_detail)
    LinearLayout current_loc_detail;
    private Unbinder unbinder;
    private boolean isCurrentSelected=false;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passport);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        presenter.init();
        presenter.checkForCurrentLocation();
        presenter.loadSavedLocations();
    }

    @Override
    public void applyFont()
    {
        title_text.setTypeface(typeFaceManager.getCircularAirBold());
        tvCurrentLocationTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvCurrentLocation.setTypeface(typeFaceManager.getCircularAirBook());
        recent_location.setTypeface(typeFaceManager.getCircularAirBold());
        add_new_loc_text.setTypeface(typeFaceManager.getCircularAirBold());
    }

    @Override
    public void initView()
    {
        recyclerLocation.setLayoutManager(linearLayoutManager);
        recyclerLocation.setHasFixedSize(false);
        recyclerLocation.setAdapter(passportAdapter);
    }

    @Override
    public void showError(String msgId)
    {

    }
    @Override
    public void showError(int msgId)
    {

    }

    @Override
    public void showMessage(String msg)
    {

    }

    @Override
    public void showCurrentUserLocation(String currentLocation)
    {
        tvCurrentLocation.setText(currentLocation);
        tvCurrentLocation.setVisibility(View.VISIBLE);
        compatCheckBox.setVisibility(View.VISIBLE);
        current_loc_detail.setVisibility(View.VISIBLE);
    }

    @Override
    public void showCurrentLocationLoading(boolean show)
    {
        if(show)
        {
            progressLocation.setVisibility(View.VISIBLE);
            compatCheckBox.setVisibility(View.GONE);
            current_loc_detail.setVisibility(View.GONE);
        } else{
            progressLocation.setVisibility(View.GONE);
            compatCheckBox.setVisibility(View.VISIBLE);
            current_loc_detail.setVisibility(View.VISIBLE);
        }
    }


    @OnClick(R.id.location_check_box)
    public void onCurretnSelect()
    {
        selectCurrent(true);
        presenter.onCurretnSelected();
    }

    @OnClick(R.id.add_new_location)
    public void changeLocation()
    {
        Intent intent = new Intent(activity, CustomLocActivity.class);
        startActivityForResult(intent,CustomLocActivity.CHOOSE_LOC_REQ_CODE);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @OnClick(R.id.close_button)
    public void close()
    {
        onBackPressed();
    }

    @Override
    public void onBackPressed()
    {
        finishActivity();
    }


    public void finishActivity()
    {
        this.finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        super.onBackPressed();
    }

    @Override
    public void setAdapterListener(PassportAdapter.OnItemClickListener listener)
    {
        passportAdapter.setListener(listener);
    }


    @Override
    public void selectCurrent(boolean isSelected)
    {
        isCurrentSelected=isSelected;
        if(isSelected)
        {
            compatCheckBox.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.selected_tick_circle));
        }else
        {
            compatCheckBox.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.selected_untick_circle));
        }
    }


    @Override
    public void onListExist(boolean isExist)
    {
        if(isExist)
        {
            recent_location.setVisibility(View.VISIBLE);
            recyclerLocation.setVisibility(View.VISIBLE);
        }else
        {
            recent_location.setVisibility(View.GONE);
            recyclerLocation.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.handleOnActivityResult(requestCode,resultCode,data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        app_permission.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        presenter.setLocationListener();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        presenter.dropLocationListener();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        unbinder.unbind();
        presenter.dispose();
    }

    @OnClick(R.id.tick_mark)
    public void tickMark()
    {
        presenter.saveLocations(isCurrentSelected);
        finishActivity();
    }

}
