package com.pairzy.com.userProfile.Model;

import com.pairzy.com.home.Discover.Model.AgeResponse;
import com.pairzy.com.home.Discover.Model.DistanceResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * @since  4/4/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class UserDataDetails
{
    private int currentImagePos =0;

    @SerializedName("opponentId")
    @Expose
    private String opponentId;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dateOfBirth")
    @Expose
    private Double dateOfBirth;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("distance")
    @Expose
    private DistanceResponse distance;
    @SerializedName("age")
    @Expose
    private AgeResponse age;
    @SerializedName("onlineStatus")
    @Expose
    private int onlineStatus;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("instaGramProfileId")
    @Expose
    private String instaGramProfileId;

    @SerializedName("profileVideo")
    @Expose
    private String profileVideo;

    @SerializedName("otherImages")
    @Expose
    private ArrayList<String> otherImages;

    @SerializedName("profileVideoThumbnail")
    @Expose
    private String profileVideoThumbnail;

    @SerializedName("isMatched")
    @Expose
    private boolean isMatched;

    @SerializedName("work")
    @Expose
    private String work;

    @SerializedName("education")
    @Expose
    private String education;


    public boolean isMatched()
    {
        return isMatched;
    }

    public void setMatched(boolean matched)
    {
        isMatched = matched;
    }

    public int getCurrentImagePos() {
        return currentImagePos;
    }

    public void setCurrentImagePos(int currentImagePos) {
        this.currentImagePos = currentImagePos;
    }

    public String getProfileVideoThumbnail() {
        return profileVideoThumbnail;
    }

    public void setProfileVideoThumbnail(String profileVideoThumbnail)
    {
        this.profileVideoThumbnail = profileVideoThumbnail;
    }


    public String getProfileVideo() {
        return profileVideo;
    }

    public void setProfileVideo(String profileVideo) {
        this.profileVideo = profileVideo;
    }

    public ArrayList<String> getOtherImages()
    {
        return otherImages;
    }

    public void setOtherImages(ArrayList<String> otherImages)
    {
        this.otherImages = otherImages;
    }


    public String getOpponentId() {
        return opponentId;
    }

    public void setOpponentId(String opponentId) {
        this.opponentId = opponentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Double getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Double dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public DistanceResponse getDistance() {
        return distance;
    }

    public void setDistance(DistanceResponse distance) {
        this.distance = distance;
    }

    public AgeResponse getAge() {
        return age;
    }

    public void setAge(AgeResponse age) {
        this.age = age;
    }

    public int getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(int onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getInstaGramProfileId() {
        return instaGramProfileId;
    }

    public void setInstaGramProfileId(String instaGramProfileId)
    {
        this.instaGramProfileId = instaGramProfileId;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }
}
