package com.pairzy.com.home.Prospects.Online.Model;

import com.pairzy.com.home.Discover.Model.AgeResponse;
import com.pairzy.com.home.Discover.Model.DistanceResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * <h2>OnlineItemPojo</h2>
 * @since  3/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class OnlineItemPojo
{
    private boolean isLoading;
    private boolean isLoadingFailed;
    @SerializedName("creation")
    @Expose
    private Integer creation;
    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("opponentId")
    @Expose
    private String opponentId;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("dateOfBirth")
    @Expose
    private Long dateOfBirth;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("otherImages")
    @Expose
    private ArrayList<String> otherImages = null;

    @SerializedName("profileVideo")
    @Expose
    private String profileVideo;

    @SerializedName("about")
    @Expose
    private String about;

    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("work")
    @Expose
    private String work;
    @SerializedName("age")
    @Expose
    private AgeResponse ageResponse;
    @SerializedName("distance")
    @Expose
    private DistanceResponse distanceResponse;

    public void setEducation(String education) {
        this.education = education;
    }

    public String getEducation() {
        return education;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isLoadingFailed() {
        return isLoadingFailed;
    }

    public void setLoadingFailed(boolean loadingFailed) {
        isLoadingFailed = loadingFailed;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public Integer getCreation() {
        return creation;
    }

    public void setCreation(Integer creation) {
        this.creation = creation;
    }

    public String getOpponentId() {
        return opponentId;
    }

    public void setOpponentId(String opponentId) {
        this.opponentId = opponentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public Long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public ArrayList<String> getOtherImages() {
        return otherImages;
    }

    public void setOtherImages(ArrayList<String> otherImages) {
        this.otherImages = otherImages;
    }

    public String getProfileVideo() {
        return profileVideo;
    }

    public void setProfileVideo(String profileVideo) {
        this.profileVideo = profileVideo;
    }

    public String getAbout()
    {
        return about;
    }

    public void setAbout(String about)
    {
        this.about = about;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public AgeResponse getAgeResponse() {
        return ageResponse;
    }

    public void setAgeResponse(AgeResponse ageResponse) {
        this.ageResponse = ageResponse;
    }

    public DistanceResponse getDistanceResponse() {
        return distanceResponse;
    }

    public void setDistanceResponse(DistanceResponse distanceResponse) {
        this.distanceResponse = distanceResponse;
    }
}
