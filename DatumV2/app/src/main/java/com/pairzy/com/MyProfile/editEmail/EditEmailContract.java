package com.pairzy.com.MyProfile.editEmail;

/**
 * Created by ankit on 27/4/18.
 */

public interface EditEmailContract {

    interface View{

        void  showError(String message);

        void finishActivity(String email);
        /**
         * <p>Action when email is not available in database</p>
         */
        void emailNotAvailable();
    }

    interface Presenter{

        boolean validateEmail(String mail);

        /**
         * <p>Checks email address is available in database or not</p>
         *
         * @param email: email address
         */
        void checkEmailIdExist(String email);
    }
}
