package com.pairzy.com.home.MakeMatch.model;

import android.location.Address;
import android.location.Geocoder;
import android.text.TextUtils;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.source.PassportLocationDataSource;
import com.pairzy.com.home.MakeMatch.MakeMatchUtil;
import com.pairzy.com.home.MakeMatch.swipeCardModel.MatchMakerAdapter;
import com.pairzy.com.locationScreen.model.LocationHolder;
import com.pairzy.com.passportLocation.model.PassportLocation;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

public class MakeMatchModel extends BaseModel {

    @Named(MakeMatchUtil.MAKE_MATCH)
    @Inject
    ArrayList<MakeMatchDataPOJO> arrayList;


    @Inject
    PassportLocationDataSource locationDataSource;

    @Inject
    Utility utility;

    @Inject
    Geocoder geocoder;

    @Inject
    MatchMakerAdapter matchMakerAdapter;



    @Inject
    public MakeMatchModel() { }

;

    public void parseMakeMatchResponse(String data) {
        MakeMatchPOJO matchPOJO=  utility.getGson().fromJson(data,MakeMatchPOJO.class);
        try{
            if(matchPOJO!=null) {
                arrayList.addAll(matchPOJO.getData());
                matchMakerAdapter.notifyDataSetChanged();
            }
        }catch (Exception e) {

        }
    }

    public Map<String,Object> sendUserAction(int userAction, String pairId) {
        Map<String,Object> body=new HashMap<>();
        body.put(ApiConfig.MATCH.ACTION,userAction);
        body.put(ApiConfig.MATCH.POST_ID,pairId);
        return  body;
    }

    public PassportLocation getLocationName(LocationHolder locationHolder) {
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(locationHolder.getLatitude(),locationHolder.getLongitude(),1);
            if(addresses != null){
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String addressName = addresses.get(0).getFeatureName();
                boolean cityEmpty = TextUtils.isEmpty(city);
                boolean stateEmpty = TextUtils.isEmpty(state);
                boolean countryEmpty = TextUtils.isEmpty(country);
                String subAddress = ((cityEmpty)?"":city+((stateEmpty)?"":","))
                        +((stateEmpty)?"":state+((countryEmpty)?"":","))
                        +((countryEmpty)?"":country+".");
                PassportLocation currentLocation = new PassportLocation();
                currentLocation.setLocationName(addressName);
                currentLocation.setSubLocationName(subAddress);
                currentLocation.setCity(city);
                currentLocation.setLatitude(locationHolder.getLatitude());
                currentLocation.setLongitude(locationHolder.getLongitude());
                return currentLocation;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String parsePassportLocationList(ArrayList<String> locationArray) {
        String city="";
        PassportLocation featureLocation=getFeatureLocation();
        try
        {
            arrayList.clear();
            boolean isFound=false;
            for(String locationJson: locationArray)
            {
                PassportLocation passportLocation = utility.getGson().fromJson(locationJson,PassportLocation.class);
                if(passportLocation != null)
                {
                    passportLocation.setSelected(false);
                    try
                    {
                        if(featureLocation!=null)
                        {
                            if(passportLocation.getLongitude().equals(featureLocation.getLongitude())
                                    &&passportLocation.getLatitude().equals(featureLocation.getLatitude()))
                            {
                                passportLocation.setSelected(true);
                                isFound=true;
                            }
                        }
                    }catch (Exception e){}
                    city=passportLocation.getCity();
                }
            }
            if(!isFound&&featureLocation!=null)
            {
                city=featureLocation.getCity();
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return city;
    }


    /*
     * it will check for feature location first then saved current location.
     */
    public PassportLocation getFeatureLocation()
    {
        try {
            String locationJson = locationDataSource.getFeatureLocation();
            if(!TextUtils.isEmpty(locationJson))
            {
                PassportLocation featureLocation = utility.getGson().fromJson(locationJson, PassportLocation.class);
                if(!TextUtils.isEmpty(featureLocation.getLocationName()))
                {
                    return featureLocation;
                }
            }
        }catch (Exception e)
        {}
        return null;
    }
}
