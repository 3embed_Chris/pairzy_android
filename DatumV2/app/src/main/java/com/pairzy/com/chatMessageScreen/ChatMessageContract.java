package com.pairzy.com.chatMessageScreen;

import android.content.Intent;

import com.pairzy.com.BasePresenter;

/**
 * <h>ChatMessageContract interface</h>
 * @author 3Embed.
 * @since 26/12/18.
 * @version 1.0.
 */
public interface ChatMessageContract {
    interface View{

        void showCoinBalance(String formatCoinBalance);

        void showError(String error);
        void showMessage(String msg);
        void startCoinAnimation();
        void launchWalletScreen();
        void makeUserMatched(String recipientId);
        void unMatchUser(String userId);
        void invalidateBlockUi();
        void refreshUserBlocked();
    }

    interface Presenter extends BasePresenter<ChatMessageContract.View> {

        void initMatchAndUnmatchUserObserver();
        void observeCoinBalanceChange();
        void updateCoinBalance();
        void loadWalletBalance();
        void callCoinConfigApi();
        void dispose();
        boolean onHandelActivityResult(int requestCode, int resultCode, Intent data);
        void doSuperLike(String user_id);
        void launchWalletEmptyDialog();
        void doLiked(final String user_id);
        void doDislike(String user_id);
        void initDataChangeObserver();

    }
}
