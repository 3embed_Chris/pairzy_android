package com.pairzy.com.util.CloudManager;

import android.text.TextUtils;

import com.pairzy.com.util.AppConfig;

public class UploadManager
{
    public static String UN_CAUGHT_ERROR = "unCaughtError";
    private String cloudName;
    private String apiKey;
    private String apiSecret;
    private boolean newCreds;

    public void setNewCreds(boolean newCreds, String cloudName, String apiKey, String apiSecret){
        this.newCreds = newCreds;
        this.cloudName = cloudName;
        this.apiKey = apiKey;
        this.apiSecret = apiSecret;
    }

    public UploadManager(boolean newCreds, String cloudName, String apiKey, String apiSecret){
        this.newCreds = newCreds;
        this.cloudName = cloudName;
        this.apiKey = apiKey;
        this.apiSecret = apiSecret;
    }

    public UploadManager(UploaderCallback callback) {
        this.callback = callback;
    }

    private UploaderCallback callback;



    public void uploadFile(String file_path,UploaderCallback uploaderCallback)
    {
        this.callback=uploaderCallback;

        if(file_path==null|| TextUtils.isEmpty(file_path))
        {
            this.callback=null;
            throw  new IllegalArgumentException("File path is empty !");
        }

        Cloudinary_Upload.getInstance(newCreds,cloudName,apiKey,apiSecret).upload_File_Cloudinary(file_path, new Cloudinary_Upload.Upload_callback()
        {
            @Override
            public void onSucess(String path,int height,int width)
            {
                String thumb_nail=create_Thumbnail(path);
                if(callback!=null)
                {
                    callback.onSuccess(path,thumb_nail,height,width);
                }
            }
            @Override
            public void onError(String error)
            {
                if(callback!=null)
                {
                    callback.onError(error);
                }
            }
        });
    }


    public void uploadVideoFile(String file_path,UploaderCallback uploaderCallback)
    {
        this.callback=uploaderCallback;

        if(file_path==null|| TextUtils.isEmpty(file_path))
        {
            this.callback=null;
            throw  new IllegalArgumentException("File path is empty !");
        }

        Cloudinary_Upload.getInstance(newCreds,cloudName,apiKey,apiSecret).upload_Video_Cloudinary(file_path, new Cloudinary_Upload.Upload_callback()
        {
            @Override
            public void onSucess(String path,int height,int width)
            {
                String thumb_nail=create_Thumbnail(path);
                if(callback!=null)
                {
                    callback.onSuccess(path,thumb_nail,height,width);
                }
            }
            @Override
            public void onError(String error)
            {
                if(callback!=null)
                {
                    callback.onError(error);
                }
            }
        });
    }

    /**
     * <h2>Creating the thumbnail image url for the image view.</h2>*/
    private String create_Thumbnail(String url)
    {
        String extension=getFileExtension(url);
        if(extension.equalsIgnoreCase("jpeg")||extension.equalsIgnoreCase("jpg")||extension.equalsIgnoreCase("png"))
        {
            //Image file
            return create_Image_thumbnail(url);
        }else
        {
            //Video file
            return   create_Video_thumbnail(url);
        }
    }


    private static String create_Video_thumbnail(String video_url)
    {
        int index_dot=video_url.lastIndexOf(".");
        String front_part=video_url.substring(0,index_dot+1);
        String  image_url=front_part+"jpeg";
        String key_word="upload";
        int length_key=key_word.length();
        int index=image_url.indexOf("upload");
        if(index>0)
        {
            String firs_sub_String=image_url.substring(0,index+length_key);
            String last_sub_String=image_url.substring(index+length_key);
            return firs_sub_String+ AppConfig.CloudinaryDetails.VIDEO_THUMBNAIL_SIZE+last_sub_String;

        }else
        {
            return image_url;
        }
    }

    private static String create_Image_thumbnail(String image_url)
    {
        String key_word="upload";
        int length_key=key_word.length();
        int index=image_url.indexOf("upload");
        if(index>0)
        {
            String firs_sub_String=image_url.substring(0,index+length_key);
            String last_sub_String=image_url.substring(index+length_key);
            return firs_sub_String+ AppConfig.CloudinaryDetails.IMAGE_THUMBNAIL_SIZE+last_sub_String;

        }else
        {
            return image_url;
        }
    }

    private String getFileExtension(String file_path)
    {
        String ext = "";
        int i = file_path.lastIndexOf('.');
        if (i > 0 &&  i < file_path.length() - 1)
        {
            ext = file_path.substring(i + 1).toLowerCase();
        }
        return ext;
    }

}
