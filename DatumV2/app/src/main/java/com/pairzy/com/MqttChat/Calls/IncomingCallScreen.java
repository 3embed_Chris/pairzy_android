package com.pairzy.com.MqttChat.Calls;

/**
 * Created by moda on 04/05/17.
 */

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.pairzy.com.AppController;
import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.pairzy.com.MqttChat.Database.CouchDbController;
import com.pairzy.com.MqttChat.SlideLayout.ISlideListener;
import com.pairzy.com.MqttChat.SlideLayout.Renderers.TranslateRenderer;
import com.pairzy.com.MqttChat.SlideLayout.SlideLayout;
import com.pairzy.com.MqttChat.SlideLayout.Sliders.Direction;
import com.pairzy.com.MqttChat.SlideLayout.Sliders.VerticalSlider;
import com.pairzy.com.MqttChat.Utilities.MqttEvents;
import com.pairzy.com.MqttChat.Utilities.TextDrawable;
import com.pairzy.com.MqttChat.Utilities.Utilities;
import com.pairzy.com.R;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;


public class IncomingCallScreen extends AppCompatActivity implements View.OnClickListener {

    private String TAG = IncomingCallScreen.class.getSimpleName();

    private Ringtone r;

    /* Parameters required to start a call */
    String call_id;

    private String caller_id;
    private String callType, callerName, callerImageUrl, callerIdentifier;

    private Bus bus = AppController.getBus();
    private TextView tvCallerName, tvAudioVideoCall;

    private ImageView callerImage, callerImageIcon;
    private CountDownTimer timer;

    CouchDbController db = AppController.getInstance().getDbController();

    private boolean isAttendButtonClicked = false;
    private String dateId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        Window window = this.getWindow();

        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            window.getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        } else {
            window.getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                    );

        }


        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_incoming_call);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if (AppController.getInstance().getSharedPreferences().getBoolean("applicationKilled", false)) {
            timer = new CountDownTimer(15000, 1000) {
                public void onTick(long millisUntilFinished) {
                }
                public void onFinish() {
                    //Perform the click of cancel button here
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("status", 1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    AppController.getInstance().publish(MqttEvents.CallsAvailability.value + "/" + AppController.getInstance().getUserId(), obj, 0, true);
                    AppController.getInstance().setActiveOnACall(false, false);

                    if (!isAttendButtonClicked) {
                        closeActivity();
                    }
                }
            };
            timer.start();
        }
//      View accept = (View) findViewById(R.id.btAcceptCall);
        SlideLayout slider = (SlideLayout) findViewById(R.id.slider);
        slider.setRenderer(new TranslateRenderer());


        slider.setSlider(new VerticalSlider(Direction.INVERSE));


        slider.setChildId(R.id.fl);
        slider.setThreshold(0.7f);

        slider.addSlideListener(new ISlideListener() {
            @Override
            public void onSlideDone(SlideLayout slider, boolean done) {
                if (done) {
                    // restore start state


                    isAttendButtonClicked = true;

                    /*
                     * MQtt
                     */

                    /*
                     * To tell the caller that i have accepted the call
                     */

                    try {
                        JSONObject obj = new JSONObject();
                        obj.put("type", 1);
                        obj.put("callId", call_id);
                        /*
                         * Not useful as of now,but can be useful in future if we add groupcalling
                         */

                        obj.put("userId", AppController.getInstance().getUserId());

                        AppController.getInstance().publish(MqttEvents.Calls.value + "/" + caller_id, obj, 0, false);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    /*
                     * Last parameter is to specify the call is the incoming call or the outgoing call
                     */

                    CallingApis.startCall(IncomingCallScreen.this,dateId, callType, call_id, caller_id, callerName, callerImageUrl, true, callerIdentifier);

                    closeActivity();
                }
            }
        });

        slider.startAnimation(AnimationUtils.loadAnimation(IncomingCallScreen.this, R.anim.call_animation));

        tvCallerName = (TextView)

                findViewById(R.id.tvCallerName);

        tvAudioVideoCall = (TextView)

                findViewById(R.id.tvAudioVideoCall);


        Button btRejectCall = (Button) findViewById(R.id.btRejectCall);


        btRejectCall.setOnClickListener(this);


        Button sendMessage = (Button)

                findViewById(R.id.button);

        sendMessage.setOnClickListener(this);
        callerImage = (ImageView)

                findViewById(R.id.userImage);


        callerImageIcon = (ImageView)

                findViewById(R.id.user_icon);


        /* Register for bus events */
        bus.register(this);



        /* Get the mobile number of the user from shared preferences */


        Bundle extras = getIntent().getExtras();

        if (extras != null)

        {

            dateId = extras.getString("dateId","");
            call_id = extras.getString("callId", "");
            caller_id = extras.getString("callerId", "");
            callType = extras.getString("callType", "");

            callerName = extras.getString("callerName", "");
            callerImageUrl = extras.getString("callerImage", "");

            callerIdentifier = extras.getString("callerIdentifier", "");

        }


        try

        {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (
                Exception e)

        {
            e.printStackTrace();
        }

        setupView();

        /* Implement the countdown times */
        timer = new

                CountDownTimer(60000, 1000) {
                    public void onTick(long millisUntilFinished) {
                    }

                    public void onFinish() {
                        /* Perform the click of cancel button here */
                        //  Toast.makeText(IncomingCallScreen.this, "Timeout", Toast.LENGTH_LONG).show();
                        try {
                            JSONObject obj = new JSONObject();
                            obj.put("type", 3);
                            obj.put("callId", call_id);
                            /*
                             * Not useful as of now,but can be useful in future if we add groupcalling
                             */
                            obj.put("userId", AppController.getInstance().getUserId());
                            AppController.getInstance().publish(MqttEvents.Calls.value + "/" + caller_id, obj, 0, false);

                            obj = new JSONObject();
                            obj.put("status", 1);
                            AppController.getInstance().publish(MqttEvents.CallsAvailability.value + "/" + AppController.getInstance().getUserId(), obj, 0, true);
                            AppController.getInstance().setActiveOnACall(false, true);
                            closeActivity();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                };
        timer.start();

        addNewCall();
    }

    /* Setup the view for incoming call screen */
    @SuppressWarnings("TryWithIdenticalCatches")
    private void setupView() {


        /*
         * If userId doesn't exists in contact
         */
        Bundle extras = getIntent().getExtras();


        //callerName = extras.getString("callerIdentifier");
        callerName = extras.getString("callerName");
        tvCallerName.setText(callerName);

        callerImageUrl = extras.getString("callerImage");

        if (callerImageUrl == null || callerImageUrl.isEmpty()) {


            callerImage.setImageDrawable(TextDrawable.builder()


                    .beginConfig()
                    .textColor(Color.WHITE)
                    .useFont(Typeface.DEFAULT)
                    .fontSize(124 * (int) getResources().getDisplayMetrics().density) /* size in px */
                    .bold()
                    .toUpperCase()
                    .endConfig()


                    .buildRect((extras.getString("callerIdentifier").trim()).charAt(0) + "", Color.parseColor(AppController.getInstance().getColorCode(5))));


            callerImageIcon.setImageDrawable(TextDrawable.builder()


                    .beginConfig()
                    .textColor(Color.WHITE)
                    .useFont(Typeface.DEFAULT)
                    .fontSize(24 * (int) getResources().getDisplayMetrics().density) /* size in px */
                    .bold()
                    .toUpperCase()
                    .endConfig()

                    .buildRound((extras.getString("callerIdentifier").trim()).charAt(0) + "", Color.parseColor(AppController.getInstance().getColorCode(5))));


        } else {

            try {
                Glide.with(IncomingCallScreen.this).load(callerImageUrl).transition(withCrossFade())

                        .centerCrop().placeholder(R.drawable.chat_attachment_profile_default_image_frame).
                        into(callerImage);


                Glide.with(IncomingCallScreen.this)
                        .asBitmap()
                        .load(callerImageUrl)
                        .centerCrop()


                        .placeholder(R.drawable.chat_attachment_profile_default_image_frame).
                        into(new BitmapImageViewTarget(callerImageIcon) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                callerImageIcon.setImageDrawable(circularBitmapDrawable);
                            }
                        });


            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }
        if (callType.contentEquals("0")) {
            /* It is an audio call */

            tvAudioVideoCall.setText(getResources().getString(R.string.Mqtt_Audio_Call));


            if (ActivityCompat.checkSelfPermission(IncomingCallScreen.this, Manifest.permission.RECORD_AUDIO)
                    != PackageManager.PERMISSION_GRANTED) {


                ActivityCompat.requestPermissions(IncomingCallScreen.this, new String[]{Manifest.permission.RECORD_AUDIO},
                        0);

            }


        } else {
            /* It is a video call */
            tvAudioVideoCall.setText(getResources().getString(R.string.Mqtt_Video_Call));


            ArrayList<String> arr1 = new ArrayList<>();
            if (ActivityCompat.checkSelfPermission(IncomingCallScreen.this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {

                arr1.add(Manifest.permission.CAMERA);
            }


            if (ActivityCompat.checkSelfPermission(IncomingCallScreen.this, Manifest.permission.RECORD_AUDIO)
                    != PackageManager.PERMISSION_GRANTED) {


                arr1.add(Manifest.permission.RECORD_AUDIO);

            }


            if (arr1.size() > 0) {

                ActivityCompat.requestPermissions(IncomingCallScreen.this, arr1.toArray(new String[arr1.size()]),
                        1);
            }

        }


    }

    @Override
    public void onBackPressed() {
        /* Disable the functionality of back button on the calling screen */
        return;
    }

    @Override
    public void onDestroy() {
        /* Disable the functionality of back button on the calling screen */
        super.onDestroy();
        try {
            timer.cancel();
            r.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
        bus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            //If the draw over permission is not available open the settings screen
            //to grant the permission.

            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivity(intent);
            }

            if (!Settings.System.canWrite(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + getPackageName()));
                startActivity(intent);
            }
        }

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btRejectCall: {

                rejectCall(false);
                break;
            }


            case R.id.button: {
                rejectCall(true);

                break;

            }
        }
    }


    @SuppressWarnings("TryWithIdenticalCatches")
    @Subscribe
    public void getMessage(JSONObject object) {


        try {
            if (object.getString("eventName").equals(MqttEvents.Calls.value + "/" + AppController.getInstance().getUserId())) {
                if (object.getInt("type") == 2) {
                    /*
                     * So many if are used just to avoid any stray messages calling interruption in ongoing or incoming call
                     */
                    if (object.getString("callId").equals(call_id) && object.getString("userId").equals(caller_id)) {

                        r.stop();
                        closeActivity();
                    }
                    /*
                     * To make myself available for receiving the new call
                     */
                    JSONObject obj = new JSONObject();

                    obj.put("status", 1);
                    AppController.getInstance().publish(MqttEvents.CallsAvailability.value + "/" + AppController.getInstance().getUserId(), obj, 0, true);
                    AppController.getInstance().setActiveOnACall(false, true);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void addNewCall() {

        Map<String, Object> callItem = new HashMap<>();
        String id = Utilities.tsInGmt();


        callItem.put("receiverName", callerName);
        callItem.put("receiverImage", callerImageUrl);
        callItem.put("receiverUid", caller_id);
        callItem.put("callTime", id);
        callItem.put("callInitiated", true);
        callItem.put("callId", String.valueOf(id));
        if (callType.equals("1")) {
            callItem.put("callType", getString(R.string.VideoCall));

        } else {
            callItem.put("callType", getString(R.string.AudioCall));

        }
        callItem.put("receiverIdentifier", callerIdentifier);
        //db.addNewCall(AppController.getInstance().getCallsDocId(), callItem);
        Common.callerName = callerName;
    }


    @SuppressWarnings("TryWithIdenticalCatches")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == 0) {


            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_DENIED) {


                /*
                 * Not required essentially
                 */
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                        != PackageManager.PERMISSION_GRANTED) {

//
//                    try {
//                        JSONObject obj = new JSONObject();
//
//                        obj.put("callId", call_id);
//                        obj.put("userId", AppController.getInstance().getUserId());
//                        obj.put("type", callType);
//
//
//                        AppController.getInstance().publish(MqttEvents.Calls.value + "/" + caller_id, obj, 0, false);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
                    makeMyselfAvailableForCall();
                    closeActivity();
                }
            }

        } else if (requestCode == 1) {

            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_DENIED) {

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                        != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
//                    try {
//                        JSONObject obj = new JSONObject();
//
//                        obj.put("callId", call_id);
//                        obj.put("userId", AppController.getInstance().getUserId());
//                        obj.put("type", callType);
//
//
//                        AppController.getInstance().publish(MqttEvents.Calls.value + "/" + caller_id, obj, 0, false);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
                    makeMyselfAvailableForCall();
                    closeActivity();
                }
            } else if (grantResults.length == 2 && (grantResults[0] == PackageManager.PERMISSION_DENIED || grantResults[1] == PackageManager.PERMISSION_DENIED)) {


                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                        != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
//                    try {
//                        JSONObject obj = new JSONObject();
//
//                        obj.put("callId", call_id);
//                        obj.put("userId", AppController.getInstance().getUserId());
//                        obj.put("type", callType);
//
//
//                        AppController.getInstance().publish(MqttEvents.Calls.value + "/" + caller_id, obj, 0, false);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
                    makeMyselfAvailableForCall();
                    closeActivity();

                }


            }

        }
    }


    private void makeMyselfAvailableForCall() {

        try {
            JSONObject obj = new JSONObject();

            obj.put("callId", call_id);
            obj.put("userId", AppController.getInstance().getUserId());
            obj.put("type", callType);

            AppController.getInstance().publish(MqttEvents.Calls.value + "/" + caller_id, obj, 0, false);

            obj = new JSONObject();
            obj.put("status", 1);


            AppController.getInstance().publish(MqttEvents.CallsAvailability.value + "/" + AppController.getInstance().getUserId(), obj, 0, true);
            AppController.getInstance().setActiveOnACall(false, true);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("TryWithIdenticalCatches")
    private void rejectCall(boolean toStartChat) {

        /*
         * Have to send the reject call event and also to make myself availabvle for the next call
         */

        try {

            /*
             * To tell the caller that i have rejected the call
             */

            r.stop();
            JSONObject obj = new JSONObject();
            obj.put("type", 2);
            obj.put("callId", call_id);

            /*
             * Not useful as of now,but can be useful in future if we add groupcalling
             */
            obj.put("userId", AppController.getInstance().getUserId());
            AppController.getInstance().publish(MqttEvents.Calls.value + "/" + caller_id, obj, 0, false);

            /*
             * To make myself available for receiving the new call
             */

            obj = new JSONObject();
            obj.put("status", 1);

            AppController.getInstance().publish(MqttEvents.CallsAvailability.value + "/" + AppController.getInstance().getUserId(), obj, 0, true);
            AppController.getInstance().setActiveOnACall(false, true);

        } catch (JSONException e2) {
            e2.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (toStartChat) {

            //TODO address
            String docId = AppController.findDocumentIdOfReceiver(caller_id, Utilities.tsInGmt(), tvCallerName.getText().toString(),
                    callerImageUrl,"",false,callerIdentifier,"",false);
            try {
                Intent intent = new Intent(IncomingCallScreen.this, ChatMessageActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                intent.putExtra("receiverUid", caller_id);
                intent.putExtra("receiverName", tvCallerName.getText().toString());
                intent.putExtra("documentId", docId);
                intent.putExtra("receiverImage", callerImageUrl);
                intent.putExtra("colorCode", AppController.getInstance().getColorCode(5));

                startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        closeActivity();
    }

    private void closeActivity(){
        supportFinishAfterTransition();
        if(AppController.getInstance().isApplicationKilled()){
            startActivity(new Intent(IncomingCallScreen.this,InCallFinishActivity.class));
            Log.d(TAG, "closeActivity: isApplicationKilled "+AppController.getInstance().isApplicationKilled());
        }
    }
}