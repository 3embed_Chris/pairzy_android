package com.pairzy.com.fbRegister.Gender;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.fbRegister.FbRegisterContact;
import com.pairzy.com.fbRegister.FbRegisterPage;
import com.pairzy.com.fbRegister.ProfilePic.FbProfilePicFrg;
import com.pairzy.com.fbRegister.AnimatorHandler;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * <h2>FbGenderFragment</h2>
 * <P>
 *
 * </P>
 * A simple {@link Fragment} subclass.
 * @author 3Embed.
 * @since 15.02.2017.
 * @version 1.0.
 */
@ActivityScoped
public class FbGenderFragment extends DaggerFragment implements FbGenderContract.View
{
    @Inject
    Activity activity;
    @Inject
    FbRegisterContact.Presenter mainpresenter;
    @Inject
    FbGenderContract.Presenter presenter;
    @Inject
    AnimatorHandler animatorHandler;

    @Inject
    Utility utility;

    @Inject
    TypeFaceManager typeFaceManager;

    @BindView(R.id.close_button)
    RelativeLayout close_button;
    @BindView(R.id.skip_page)
    TextView skip_page;
    @BindView(R.id.first_title)
    TextView first_title;
    @BindView(R.id.second_title)
    TextView second_title;

    @BindView(R.id.btnNext)
    RelativeLayout btnNext;

    @BindView(R.id.male_selection)
    RelativeLayout male_selection;
    @BindView(R.id.male_text)
    TextView male_text;
    @BindView(R.id.male_tick)
    ImageView male_tick;
    @BindView(R.id.female_selection)
    RelativeLayout female_selection;
    @BindView(R.id.female_text)
    TextView female_text;
    @BindView(R.id.female_tick)
    ImageView female_tick;
    private Unbinder unbinder;
    @Inject
    public FbGenderFragment() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gender, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder=ButterKnife.bind(this,view);
        btnNext.setEnabled(false);
        first_title.setTypeface(typeFaceManager.getCircularAirBold());
        second_title.setTypeface(typeFaceManager.getCircularAirBold());
        male_text.setTypeface(typeFaceManager.getCircularAirBook());
        female_text.setTypeface(typeFaceManager.getCircularAirBook());
    }

    @Override
    public void onResume() {
        super.onResume();
        first_title.requestFocus();
        presenter.takeView(this);
        utility.closeSpotInputKey(activity,male_text);
        mainpresenter.updateProgress(4);
        if(female_tick.getVisibility() != View.VISIBLE && male_tick.getVisibility() != View.VISIBLE )
            setInitialSelection();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
    }


    @OnClick(R.id.male_selection)
    void onMaleSelection()
    {
        if(male_tick.getVisibility()!=View.VISIBLE)
        {
            switchScale(female_text,female_tick,male_text,male_tick);
        }
    }

    @OnClick(R.id.female_selection)
    void onFemaleSelection()
    {
        if(female_tick.getVisibility()!=View.VISIBLE)
        {
            switchScale(male_text,male_tick,female_text,female_tick);
        }
    }

    @OnClick(R.id.parent_layout)
    void onParentClick(){}

    @OnClick(R.id.close_button)
    void onClose()
    {
        activity.onBackPressed();
    }

    @OnClick(R.id.skip_page)
    void onSkipClicked()
    {
        moverFragment(0);
    }


    @OnClick(R.id.btnNext)
    void onNext()
    {
        int gender=1;
        if(female_tick.getVisibility()==View.VISIBLE)
        {
            gender=2;;
        }
        moverFragment(gender);
    }

    @Override
    public void moverFragment(int gender)
    {
        FbProfilePicFrg profilePicFrg=new FbProfilePicFrg();
        Bundle data=getArguments();
        if(data==null)
        {
            data=new Bundle();
        }
        data.putInt(FbRegisterContact.Presenter.GENDER_DATA,gender);
        profilePicFrg.setArguments(data);
        mainpresenter.launchFragment(profilePicFrg,true);

        if(gender == 1)
            FbRegisterPage.details.setGender("male");
        else{
            FbRegisterPage.details.setGender("female");
        }
        mainpresenter.launchNextValidFrag(3,true);
    }


    /*
     * Setting the initial selection*/
    private void setInitialSelection()
    {
        utility.closeSpotInputKey(activity,first_title);
        scaleUp(male_text,male_tick);
    }

    @Override
    public void showMessage(String message)
    {
        mainpresenter.showMessage(message);
    }

    @Override
    public void showError(String message)
    {
        mainpresenter.showError(message);
    }

    @Override
    public void upDateSelection(int position, boolean isSelected)
    {

    }

    private void scaleUp(TextView textView,ImageView imageView)
    {
        textView.setTextColor(ContextCompat.getColor(activity,R.color.black));
        imageView.setVisibility(View.VISIBLE);
        Animation animation=animatorHandler.getItemScaleUp();
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                handelNextButton(true);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        imageView.setAnimation(animation);
        animation.start();
    }

    /*
    * Switching the animation*/
    private void switchScale(TextView fist,ImageView firstImg,TextView second,ImageView secImg)
    {
        fist.setTextColor(ContextCompat.getColor(activity,R.color.softLightGray));
        Animation animation=animatorHandler.getItemScaleDown();
        firstImg.setVisibility(View.INVISIBLE);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                scaleUp(second,secImg);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        firstImg.setAnimation(animation);
        animation.start();
    }

    /*
   *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            btnNext.setEnabled(true);
        }
        else {
            btnNext.setEnabled(false);
        }
    }
}
