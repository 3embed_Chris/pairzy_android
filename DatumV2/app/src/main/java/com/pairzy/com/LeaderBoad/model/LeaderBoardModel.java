package com.pairzy.com.LeaderBoad.model;

import android.util.Log;

import com.pairzy.com.BaseModel;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Inject;

public class LeaderBoardModel extends BaseModel {

    @Inject
    Utility utility;

    @Inject
    ArrayList<LeaderBoardDataPOJO> arrayList;

    @Inject
    LeaderBoadAdapter adapter;

    @Inject
    public LeaderBoardModel() { }

    public void parseModel(String data) {
        try{
            LeaderBoardPOJO pojo=utility.getGson().fromJson(data,LeaderBoardPOJO.class);
            if(pojo!=null) {
                arrayList.clear();
                arrayList.addAll(pojo.getData());
                adapter.notifyDataSetChanged();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
