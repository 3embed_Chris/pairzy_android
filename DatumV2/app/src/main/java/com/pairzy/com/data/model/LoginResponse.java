package com.pairzy.com.data.model;

import com.pairzy.com.data.model.coinBalance.Coins;
import com.pairzy.com.data.model.coinCoinfig.CoinData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * <h2>LoginResponse</h2>
 *
 * @author 3Embed
 * @version 1.0
 * @since 1/19/2018.
 */

public class LoginResponse implements Serializable
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private LoginData data;

    @SerializedName("coinConfig")
    @Expose
    private CoinData coinConfigData;

    @SerializedName("coinWallet")
    @Expose
    private Coins walletCoins;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }

    public CoinData getCoinConfigData() {
        return coinConfigData;
    }

    public void setCoinConfigData(CoinData coinConfigData) {
        this.coinConfigData = coinConfigData;
    }
    public Coins getWalletCoins() {
        return walletCoins;
    }

    public void setWalletCoins(Coins walletCoins) {
        this.walletCoins = walletCoins;
    }
}
