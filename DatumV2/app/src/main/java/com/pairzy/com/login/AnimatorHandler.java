package com.pairzy.com.login;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.pairzy.com.R;
/**
  */
public class AnimatorHandler
{
    private Context context;

     AnimatorHandler(Context context)
    {
        this.context=context;
    }

    public Animation getScaleUp()
    {
        return AnimationUtils.loadAnimation(context,R.anim.scale_up);
    }

    public Animation getScaleDown()
    {
     return AnimationUtils.loadAnimation(context,R.anim.scale_down);
    }

    public Animation getTopOut()
    {
        return AnimationUtils.loadAnimation(context,R.anim.slide_out_top);
    }
    public Animation getTopIn()
    {
        return AnimationUtils.loadAnimation(context,R.anim.slide_in_top);
    }

    public Animation getBottomIn()
    {
        return AnimationUtils.loadAnimation(context,R.anim.slide_in_y);
    }
    public Animation getBottomOut()
    {
        return AnimationUtils.loadAnimation(context,R.anim.slide_out_y);
    }
}


