package com.pairzy.com.util.CustomObserver;

import com.pairzy.com.passportLocation.model.PassportLocation;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;
/**
 * <h2>RxNetworkObserver</h2>
 * <P>
 *  Its the network observer class to observe the network
 *  changes in the App.
 * </P>
 * @version 1.0.
 * @author 3Embed.*/
public class LocationObserver
{
    private  ConnectableObservable<PassportLocation> connectableObservable;
    private ObservableEmitter<PassportLocation> emitor;
    public LocationObserver()
    {
        Observable<PassportLocation> observable=Observable.create(e -> emitor=e);
        connectableObservable=observable.publish();
        connectableObservable.share();
        connectableObservable.replay();
        connectableObservable.connect();
    }

    public ConnectableObservable<PassportLocation> getObservable()
    {
        return connectableObservable;
    }

    public void publishData(PassportLocation data)
    {
        if(emitor!=null && data != null)
        {
            emitor.onNext(data);
        }
    }
}


