package com.pairzy.com.MatchedView;

import android.content.Context;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Matches.Chats.Model.MqttUnmatchResponse;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.Exception.SimilarDataException;
import com.pairzy.com.util.Utility;
/**
 * @since  4/13/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MatchedModel
{
    private PreferenceTaskDataSource dataSource;
    private Utility utility;
    private Context context;
    public MatchedModel(Context context,PreferenceTaskDataSource dataSource,Utility utility)
    {
        this.context=context;
        this.dataSource=dataSource;
        this.utility=utility;
    }

    public  OpponentDetails parseMatch(String response) throws Exception,SimilarDataException,EmptyData
    {
        try
        {
            MatchResponseData data=utility.getGson().fromJson(response,MatchResponseData.class);
            if(data==null)
            {
                throw  new EmptyData("CoinData is empty!");
            }
            OpponentDetails details=new OpponentDetails();
            if(data.getFirstLikedBy().equals(dataSource.getUserId()))
            {
                details.setName(data.getSecondLikedByName());
                details.setProfile_pic(data.getSecondLikedByPhoto());
                details.setUser_id(data.getSecondLikedBy());
                details.setIsSuperlikedMe(data.getIsSecondSuperLiked());

            }else
            {
                details.setName(data.getFirstLikedByName());
                details.setProfile_pic(data.getFirstLikedByPhoto());
                details.setUser_id(data.getFirstLikedBy());
                details.setIsSuperlikedMe(data.getIsFirstSuperLiked());
            }
            details.setChatId(data.getChatId());
            return details;
        }catch (Exception e)
        {
            throw new DataParsingException(e.getMessage());
        }
    }


    public String parseUnmatchResponse(String response) {
        try{
            MqttUnmatchResponse unmatchResponse = utility.getGson().fromJson(response,MqttUnmatchResponse.class);
            return unmatchResponse.getTargetId();
        }catch (Exception e){e.printStackTrace();}
        return "";
    }
}
