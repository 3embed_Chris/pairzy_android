package com.pairzy.com.util.FeatureLocAlert;

/**
 * <h>RatingAlertCallback interface</h>
 * @author 3Embed.
 * @since 1/6/2018.
 * @version 1.0.
 */
public interface FeatureLocAlertCallback
{
    void onOk();
    void onCancel();
}
