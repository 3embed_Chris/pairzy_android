package com.pairzy.com.register;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.UserPreference.MyPreferencePage;
import com.pairzy.com.register.Email.EmailFragment;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.CustomVideoView.NoInterNetView;
import javax.inject.Inject;
import javax.inject.Named;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 *<h2>{@link RegisterPage}</h2>
 * <P>
 *
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 15/02/2017.*/
public class RegisterPage extends BaseDaggerActivity implements RegisterContact.View
{
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String COUNTRY_CODE= "countryCode";
    public static final String OTP="otp";
    @Inject
    @Named(RegisterActivityBuilder.ACTIVITY_FRAGMENT_MANAGER)
    FragmentManager fragmentManager;
    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;
    @Inject
    App_permission app_permission;
    @Inject
    RegisterContact.Presenter presenter;
    @Inject
    Activity activity;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.no_internetView)
    NoInterNetView noInterNetView;
    private String country_code;
    private String mobile_number;
    private String entered_otp;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register__page);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder=ButterKnife.bind(this);
        progress_bar.setMax(4);
        progress_bar.setProgress(1);
        Bundle data=getIntent().getExtras();
        if(data!=null)
        {
            country_code=data.getString(COUNTRY_CODE);
            mobile_number=data.getString(MOBILE_NUMBER);
            entered_otp=data.getString(OTP);
        }
        //presenter.initNetworkObserver();
        launchEmailFrg();
    }

    /*
     * launching the initial fragment.*/
    private void launchEmailFrg()
    {
        EmailFragment emailFragment=new EmailFragment();
        presenter.launchFragment(emailFragment,true);
    }

    @Override
    public void onBackPressed()
    {
        if(!handelBackPressed())
        {
            this.finish();
        }else
        {
            super.onBackPressed();
        }
    }
    /*
    *Handling the back press */
    public boolean handelBackPressed()
    {
        int count=fragmentManager.getBackStackEntryCount();
        updateProgress(count-1);
        return count > 1;
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onDestroy()
    {
        unbinder.unbind();
        presenter.dropView();
        super.onDestroy();
    }

    @Override
    public void openPreferencePage()
    {
        Intent intent=new Intent(this,MyPreferencePage.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    @Override
    public void updateProgress(int progress)
    {
        progress_bar.setProgress(progress);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if(!app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults))
        {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void showError(String message)
    {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }



    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void openFragment(DaggerFragment fragment, boolean keepBack)
    {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.frg_enter_from_right, R.anim.frg_exit_from_left);
        fragmentTransaction.add(R.id.register_container, fragment);
        if(keepBack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
        fragmentTransaction.commit();
    }

    @Override
    public String getCountryCode() {
        return country_code;
    }

    @Override
    public String getMobileNumber() {
        return mobile_number;
    }

    @Override
    public String getEnteredOtp() {
        return entered_otp;
    }

    @Override
    public void updateInterNetStatus(boolean status)
    {
        if(noInterNetView!=null)
        {
            if(status)
            {
                noInterNetView.internetFound();
            }else
            {
                noInterNetView.showNoInternet();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(!presenter.onHandelActivityResult(requestCode,resultCode,data))
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
