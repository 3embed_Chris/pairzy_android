package com.pairzy.com.home.Dates.Model;

/**
 * Created by ankit on 22/8/18.
 */

public enum DateResponseType {

    ACCEPTED(1),
    DENIED(2),
    RESCHEDULE(3);
    int value;

    DateResponseType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}
