package com.pairzy.com.MqttChat.DocumentPicker;

/**
 * Created by moda on 22/08/17.
 */

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


import java.util.ArrayList;

import com.pairzy.com.MqttChat.DocumentPicker.Fragments.DocFragment;
import com.pairzy.com.MqttChat.DocumentPicker.Fragments.DocPickerFragment;
import com.pairzy.com.MqttChat.DocumentPicker.Fragments.MediaPickerFragment;
import com.pairzy.com.MqttChat.DocumentPicker.Fragments.PhotoPickerFragmentListener;
import com.pairzy.com.MqttChat.DocumentPicker.Utils.FragmentUtil;
import com.pairzy.com.R;

import butterknife.Unbinder;

public class FilePickerActivity extends BaseFilePickerActivity implements
        PhotoPickerFragmentListener,
        DocFragment.DocFragmentListener,
        DocPickerFragment.DocPickerFragmentListener,
        MediaPickerFragment.MediaPickerFragmentListener {

    private static final String TAG = FilePickerActivity.class.getSimpleName();
    private int type;

    TextView tvToolbarTitle;
    androidx.appcompat.widget.Toolbar toolbar;
    ArrayList<String> selectedPaths;

    private Unbinder unbinder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, R.layout.activity_file_picker);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    protected void initView() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tvToolbarTitle = findViewById(R.id.tv_toolbar_title);
        Intent intent = getIntent();
        if (intent != null) {
            if (getSupportActionBar() != null)
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            selectedPaths = intent.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA);
            type = intent.getIntExtra(FilePickerConst.EXTRA_PICKER_TYPE, FilePickerConst.MEDIA_PICKER);

            if (selectedPaths != null) {

                if (PickerManager.getInstance().getMaxCount() == 1) {
                    selectedPaths.clear();
                }

                if (type == FilePickerConst.MEDIA_PICKER) {
                    PickerManager.getInstance().add(selectedPaths, FilePickerConst.FILE_TYPE_MEDIA);
                } else {


                    PickerManager.getInstance().addDocumentWithMimeType(selectedPaths, FilePickerConst.FILE_TYPE_DOCUMENT, null);
                    //PickerManager.getInstance().add(selectedPaths, FilePickerConst.FILE_TYPE_DOCUMENT);
                }
            } else
                selectedPaths = new ArrayList<>();

//            setToolbarTitle(PickerManager.getInstance().getCurrentCount());
            setToolbarTitle();
            openSpecificFragment(type, selectedPaths);
        }
    }

    private void openSpecificFragment(int type, @Nullable ArrayList<String> selectedPaths) {
        if (type == FilePickerConst.MEDIA_PICKER) {
            MediaPickerFragment photoFragment = MediaPickerFragment.newInstance();
            FragmentUtil.addFragment(this, R.id.container, photoFragment);
        } else {
            if (PickerManager.getInstance().isDocSupport())
                PickerManager.getInstance().addDocTypes();

            DocPickerFragment photoFragment = DocPickerFragment.newInstance(selectedPaths);
            FragmentUtil.addFragment(this, R.id.container, photoFragment);
        }
    }

    //    private void setToolbarTitle(int count) {
    private void setToolbarTitle() {
        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            //actionBar.setTitle("");
//            ColorDrawable colorDrawable = new ColorDrawable(getResources().getColor(R.color.white));
//            actionBar.setBackgroundDrawable(colorDrawable);
//            if (type == FilePickerConst.MEDIA_PICKER) {
//                actionBar.setTitle(R.string.select_photo_text);
//            }
//            else {
//                actionBar.setTitle(R.string.select_doc_text);
//            }
//
//        }

        tvToolbarTitle.setTypeface(circularAirBold);
        if (type == FilePickerConst.MEDIA_PICKER) {
            tvToolbarTitle.setText(R.string.select_photo_text);
        }
        else {
            tvToolbarTitle.setText(R.string.select_doc_text);
        }
//            if (PickerManager.getInstance().getMaxCount() > 1) {
//                actionBar.setTitle(String.format(getString(R.string.attachments_title_text), count, PickerManager.getInstance().getMaxCount()));
//            } else {
//                if (type == FilePickerConst.MEDIA_PICKER)
//                    actionBar.setTitle(R.string.select_photo_text);
//                else
//                    actionBar.setTitle(R.string.select_doc_text);
//            }
//        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.picker_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.action_done) {
            if (type == FilePickerConst.MEDIA_PICKER)
                returnData(PickerManager.getInstance().getSelectedPhotos(), null);
            else
                returnData(PickerManager.getInstance().getSelectedFiles(), PickerManager.getInstance().getSelectedMimeTypes());

            return true;
        } else if (i == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        try{
        super.onBackPressed();
        setResult(RESULT_CANCELED);
        finish();}catch(Exception e){e.printStackTrace();}
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_MEDIA_DETAIL:
                if (resultCode == Activity.RESULT_OK) {
                    if (type == FilePickerConst.MEDIA_PICKER)
                        returnData(PickerManager.getInstance().getSelectedPhotos(), null);
                    else
                        returnData(PickerManager.getInstance().getSelectedFiles(), PickerManager.getInstance().getSelectedMimeTypes());
                }//added extra
                else{
                    if(selectedPaths != null)
                        openSpecificFragment(type, selectedPaths);
                }


//                else {
//                    setToolbarTitle(PickerManager.getInstance().getCurrentCount());
//                }
                break;
        }
    }

    private void returnData(ArrayList<String> paths, ArrayList<String> mimeTypes) {
        Intent intent = new Intent();
        if (type == FilePickerConst.MEDIA_PICKER)
            intent.putStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA, paths);
        else {
            intent.putStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS, paths);

            intent.putStringArrayListExtra(FilePickerConst.SELECTED_DOCS_MIME_TYPES, mimeTypes);


        }
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onItemSelected() {
        //  setToolbarTitle(PickerManager.getInstance().getCurrentCount());

        if (PickerManager.getInstance().getMaxCount() == 1) {

            if (type == FilePickerConst.MEDIA_PICKER) {


                returnData(PickerManager.getInstance().getSelectedPhotos(),null);
            } else {

                returnData(PickerManager.getInstance().getSelectedFiles(),PickerManager.getInstance().getSelectedMimeTypes());
            }

        }

    }
}