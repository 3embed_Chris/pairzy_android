package com.pairzy.com.util.boostDialog;

import android.os.Handler;

import java.util.TimerTask;

/**
 * <h>SlideTimerTask class</h>
 * @author 3Embed.
 * @since 10/5/18.
 * @version 1.0.
 */

public class SlideTimerTask extends TimerTask
{
    Handler handler;
    Runnable runnable;

    public SlideTimerTask(Runnable runnable){
        this.runnable = runnable;
        handler = new Handler();
    }
    @Override
    public void run() {
        handler.post(runnable);
    }
}
