package com.pairzy.com.MySearchPreference;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.pairzy.com.AppController;
import com.pairzy.com.MySearchPreference.Model.LocationClickCallback;
import com.pairzy.com.MySearchPreference.Model.SearchPrefModel;
import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.model.SearchPreference;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.passportLocation.model.PassportLocation;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.CustomObserver.LocationObserver;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.boostDialog.BoostAlertCallback;
import com.pairzy.com.util.boostDialog.BoostDialog;
import com.pairzy.com.util.boostDialog.Slide;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.suresh.innapp_purches.InAppCallBack;
import com.suresh.innapp_purches.InnAppSdk;
import com.suresh.innapp_purches.SkuDetails;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * <h2>MySearchPrefPresenter</h2>
 * <P>
 *
 * </P>
 *@since  3/7/2018.
 */
@ActivityScoped
public class MySearchPrefPresenter implements MySearchPrefContract.Presenter,LocationClickCallback,BoostAlertCallback
{
    @Inject
    LoadingProgress loadingProgress;
    @Inject
    Utility utility;
    @Inject
    NetworkService networkService;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    SearchPrefModel model;
    @Inject
    MySearchPrefContract.View view;
    @Inject
    LocationObserver locationObserver;
    @Inject
    BoostDialog boostDialog;
    @Inject
    ArrayList<Slide> slideArrayList;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    Activity activity;
    private CompositeDisposable compositeDisposable;
    private View locationView;
    private boolean isLocationChanged = false;

    @Inject
    MySearchPrefPresenter(){
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(Object view) {}

    @Override
    public void dropView() {
        compositeDisposable.clear();
    }

    public void observeLocationChange()
    {
        locationObserver.getObservable().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PassportLocation>() {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(PassportLocation value) {
                        if(value != null){
                            try {
                                TextView selectedLocation = locationView.findViewById(R.id.selected_location_text);
                                selectedLocation.setText(value.getSubLocationName());
                                isLocationChanged = true;
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                    @Override
                    public void onError(Throwable e){}

                    @Override
                    public void onComplete(){}
                });

    }

    /*
     *fetching location view first.
     */
    private void getLocationView()
    {
        String locationName = model.getSavedLocation();
        if(locationName.isEmpty()){
            locationName = "Add Your Location...";
        }
        this.locationView = model.parseLocationView(locationName,this);
    }

    @Override
    public void openBoostDialog()
    {
        if(!model.isSubsPlanEmpty())
        {
            model.selectMiddleItem();
            boostDialog.showAlert(this,model.getSubsPlanList(), slideArrayList);
        } else {
            getSubsPlan();
        }
    }


    private void getSubsPlan()
    {
        if(networkStateHolder.isConnected())
        {
            loadingProgress.show();
            networkService.getSubsPlans(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value)
                        {
                            if(value.code() == 200)
                            {
                                try
                                {
                                    model.parseSubsPlanList(value.body().string());
                                    if (model.isSubsPlanEmpty())
                                    {
                                        loadingProgress.cancel();
                                        if(view != null)
                                            view.showError(activity.getString(R.string.subs_list_empty));
                                    }else
                                    {
                                        model.selectMiddleItem();
                                        List<String> actual_ids=model.collectsIds();
                                        InnAppSdk.getVendor().getProductDetails(activity,actual_ids, InnAppSdk.Type.SUB, new InAppCallBack.DetailsCallback()
                                        {
                                            @Override
                                            public void onSuccess(List<SkuDetails> list, List<String> errorList)
                                            {

                                                loadingProgress.cancel();
                                                if(list.size()>0)
                                                {
                                                    model.updateDetailsData(list);
                                                    if(!model.isSubsPlanEmpty())
                                                    {
                                                        model.selectMiddleItem();
                                                        boostDialog.showAlert(MySearchPrefPresenter.this,model.getSubsPlanList(), slideArrayList);
                                                    }
                                                }else
                                                {
                                                    model.clearProductList();
                                                    if(view != null)
                                                        view.showError(activity.getString(R.string.subs_list_empty));
                                                }
                                            }
                                        });
                                    }
                                }catch (Exception e)
                                {
                                    loadingProgress.cancel();
                                    if(view != null)
                                        view.showError(activity.getString(R.string.subs_list_empty));
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else
                            {
                                loadingProgress.cancel();
                            }
                        }
                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if (view != null)
                                view.showError(e.getMessage());
                        }
                    });
        } else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }



    @Override
    public void updateStoredPref()
    {
/*        try
        {
            ArrayList<SearchPreference> data_list=model.parserSavedData();
            List<View> prefViewList  = model.parseRelatedView(data_list);
            getLocationView();
            prefViewList.add(0,this.locationView);
            if(view!=null)
                view.updatePrefView(prefViewList);
        } catch (DataParsingException e)
        {
            e.printStackTrace();
            *//*
             *Not able to collect the details so updating the data from server */
            getMyPreference();
//        }
    }

    @Override
    public void getMyPreference()
    {
        networkService.getSearchPreferences(dataSource.getToken(),model.getLanguage())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> value)
                    {
                        try
                        {
                            if(value.code() == 200)
                            {
                                String data=value.body().string();
                                ArrayList<SearchPreference> data_list=model.parseResponse(data);
                                if(data_list!=null&&data_list.size()>0)
                                {
                                    if(view != null) {
                                        List<View> prefViewList  = model.parseRelatedView(data_list, MySearchPrefPresenter.this);
                                        getLocationView();
                                        prefViewList.add(0,locationView);
                                        view.updatePrefView(prefViewList);
                                    }
                                }else
                                {
                                    view.onConnectionError();
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();

                            } else
                            {
                                if(view!=null)
                                {
                                    view.onConnectionError();
                                    view.showError(model.getError(value));
                                }
                            }
                        } catch (Exception e)
                        {
                            if(view!=null)
                                view.showError(activity.getString(R.string.failed_to_get_pref_error));
                        }
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        if(view!=null)
                            view.onConnectionError();
                    }
                    @Override
                    public void onComplete() {}
                });
    }


    @Override
    public void updatePreference()
    {
        if(model.isUpdateRequired()||isLocationChanged)
        {
            isLocationChanged = false;
            loadingProgress.show();
            networkService.updatePreferenceService(dataSource.getToken(),model.getLanguage(), model.prepareUpdateData())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> value)
                        {
                            loadingProgress.cancel();
                            Log.d("234q",""+value.code());
                            try
                            {
                                if(value.code() == 200)
                                {
                                    if(view!=null)
                                        view.prefUpdated(true);
                                }
                                else if(value.code() == 401){
                                    AppController.getInstance().appLogout();
                                } else
                                {
                                    if(view!=null)
                                        view.showError(model.getError(value));
                                }
                            } catch (Exception e)
                            {
                                if(view!=null)
                                    view.showError(e.getMessage());
                            }
                        }

                        @Override
                        public void onError(Throwable e)
                        {
                            loadingProgress.cancel();
                            if(view!=null)
                                view.showError(activity.getString(R.string.failed_to_update_pref_settings));
                        }
                        @Override
                        public void onComplete() {}
                    });

        }else
        {
            if(view!=null)
                view.prefUpdated(false);
        }
    }

    @Override
    public void onLocationClick() {
        if(view != null)
            view.launchLocationScreen();
    }

    @Override
    public void onInappSubscribe(int position)
    {
        String purchaseId=model.getSubsPurchaseId(position);
        if(!TextUtils.isEmpty(purchaseId))
        {
            InnAppSdk.getVendor().purchasedItem(activity, purchaseId, InnAppSdk.Type.SUB, dataSource.getUserId(), new InAppCallBack.OnPurchased()
            {
                @Override
                public void onSuccess(String productId)
                {
                    String id=model.extractIDFromKey(productId);
                    if(TextUtils.isEmpty(id))
                    {
                        if(view!=null)
                            view.showError("Error on updating !");
                    }else
                    {
                        callSubscriptionApi(id);
                    }
                }
                @Override
                public void onError(String id, String error)
                {
                    if(view != null)
                        view.showError(error);
                }
            });
        }else
        {
            if(view != null)
                view.showError("Error on purchasing "+purchaseId);
        }
    }

    private void callSubscriptionApi(String id){

        if(networkStateHolder.isConnected())
        {
            loadingProgress.show();
            Map<String, Object> body = new HashMap<>();
            body.put(ApiConfig.SubsPlan.PLAN_ID,id);
            body.put(ApiConfig.SubsPlan.PAYMENT_GETWAY_TAX_ID,"1234");
            body.put(ApiConfig.SubsPlan.PAYMENT_GETWAY,"XYZ");
            body.put(ApiConfig.SubsPlan.USER_PURCHASE_TIME,String.valueOf(System.currentTimeMillis()));
            networkService.postSubscription(dataSource.getToken(), "en", body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if(value.code() == 200){
                                try{
                                    model.parseSubscription(value.body().string());
                                }catch (Exception e){}
                                if(view != null)
                                    view.showMessage(activity.getString(R.string.plan_purchase_successful));
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception ignored){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }

    }


    @Override
    public void onNoThanks(){}

}
