package com.pairzy.com.MyProfile.editPreference.newListScrollerFrag;

import com.pairzy.com.BaseModel;
import com.pairzy.com.util.ApiConfig;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

/**
 * <h2>NewListdataFrgModel</h2>
 * <P>
 *
 * </P>
 * @since  2/22/2018.
 */
class NewListdataFrgModel extends BaseModel
{
    @Inject
    NewListdataFrgModel(){}

    String seingleSelectionValues(String data)
    {
        ArrayList<String> temp_data=new ArrayList<>();
        temp_data.add(data);
        JSONArray arr_strJson = new JSONArray(temp_data);
        return arr_strJson.toString();
    }

    /*
     *foramt the multiple values. */
    String multiSelectionValues(ArrayList<String> datas)
    {
        JSONArray arr_strJson = new JSONArray(datas);
        return arr_strJson.toString();
    }

    /*
     *provide the params. */
    Map<String, Object> getParams(String pref_id,String values)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.updatePreferenceKey.PREF_ID,pref_id);
        map.put(ApiConfig.updatePreferenceKey.PREF_VALUES,values);
        return map;
    }

}
