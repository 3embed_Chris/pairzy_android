package com.pairzy.com.MqttChat.ViewHolders;
/*
 * Created by moda on 02/04/16.
 */

import android.animation.ObjectAnimator;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;

import com.pairzy.com.util.TypeFaceManager;


/**
 * View holder for contact received recycler view item
 */
public class ViewHolderContactReceived extends RecyclerView.ViewHolder {
    public  TextView senderName;

    private TypeFaceManager typeFaceManager;

    public TextView time, contactName, contactNumber, date, previousMessage_head, previousMessage_content;

    public ImageView forward,previousMessage_iv;

    public RelativeLayout contact_rl;
    public RelativeLayout messageRoot,previousMessage_rl;

    public ViewHolderContactReceived(View view,TypeFaceManager typeFaceManager) {
        super(view);
        this.typeFaceManager = typeFaceManager;

        senderName = (TextView) view.findViewById(R.id.lblMsgFrom);

        forward = (ImageView) view.findViewById(R.id.forward_iv);

        messageRoot=(RelativeLayout) view.findViewById(R.id.message_root);
           /*
         * For message reply feature
         */
        previousMessage_rl = (RelativeLayout) view.findViewById(R.id.initialMessage_rl);
        previousMessage_head = (TextView) view.findViewById(R.id.senderName_tv);
        previousMessage_iv= (ImageView) view.findViewById(R.id.initialMessage_iv);
        previousMessage_content = (TextView) view.findViewById(R.id.message_tv);

        contact_rl=(RelativeLayout) view.findViewById(R.id.relative_layout_message);
        ObjectAnimator animation = ObjectAnimator.ofFloat(forward, "rotationY", 0.0f, 180f);
        animation.setDuration(0);

        animation.start();

        date = (TextView) view.findViewById(R.id.date);
        time = (TextView) view.findViewById(R.id.ts);
        contactName = (TextView) view.findViewById(R.id.contactName);
        contactNumber = (TextView) view.findViewById(R.id.contactNumber);

        //Typeface tf = AppController.getInstance().getRobotoCondensedFont();

        time.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.ITALIC);
        date.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.ITALIC);
        contactName.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.NORMAL);
        contactNumber.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.NORMAL);
        if (previousMessage_head != null) {
            previousMessage_head.setTypeface(typeFaceManager.getCircularAirBold());
            previousMessage_content.setTypeface(typeFaceManager.getCircularAirBook());
        }
    }
}
