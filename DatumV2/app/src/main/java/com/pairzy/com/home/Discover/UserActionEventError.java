package com.pairzy.com.home.Discover;

import com.pairzy.com.home.Discover.Model.UserAssetData;

public interface UserActionEventError
{
    void onRevertAction(UserAssetData userAssetData);

    void onError(UserAssetData userAssetData);
}
