package com.pairzy.com.util.timerDialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.CountDownTimer;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

/**
 * <h2>ChatMenuDialog</h2>
 * <P>
 *
 * </P>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class TimerDialog
{
    private Activity activity;
    private Dialog dialog;
    private TypeFaceManager typeFaceManager;
    private Utility utility;
    private TimerDialogCallBack callBack;
    private TimerDialogCallBack boostCallBack;
    private  AppCompatCheckBox checkBox;
    private PreferenceTaskDataSource dataSource;
    private long boostExpireTime = 0;
    private long nextLikeTime = 0;
    private CountDownTimer countDownTimer;

    public TimerDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility,PreferenceTaskDataSource dataSource)
    {
        this.activity = activity;
        this.typeFaceManager = typeFaceManager;
        this.utility = utility;
        this.dataSource = dataSource;
    }


    public void showDialog(boolean forBoostTimer)
    {
        if(dialog != null && dialog.isShowing())
        {
            dialog.cancel();
        }
        @SuppressLint("InflateParams")
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.timer_dialog, null);
        builder.setView(dialogView);

        ImageView ivSlideImage = dialogView.findViewById(R.id.slide_iv);

        if(forBoostTimer){
            ivSlideImage.setImageResource(R.drawable.boost);
        }
        else{
            ivSlideImage.setImageResource(R.drawable.unlimited_likes);
        }

        TextView tvTitle =dialogView.findViewById(R.id.title_tv);
        tvTitle.setTypeface(typeFaceManager.getCircularAirBold());
        TextView tvMessage =dialogView.findViewById(R.id.msg_tv);
        tvMessage.setTypeface(typeFaceManager.getCircularAirBold());


        if(forBoostTimer){
            tvTitle.setText(R.string.boosted_profile_title);
            boostExpireTime = dataSource.getBoostExpireTime();
            long currentTime = System.currentTimeMillis();
            if(currentTime < boostExpireTime){
                tvMessage.setText(utility.getTimerFormatDateTime(boostExpireTime-currentTime));
            }
            else{
                tvMessage.setText("00:00:00");
            }
        }
        else {
            tvTitle.setText(R.string.next_like_time_title);
            nextLikeTime = dataSource.getNextLikeTime();
            long currentTime = System.currentTimeMillis();
            if(currentTime < nextLikeTime){
                tvMessage.setText(utility.getTimerFormatDateTime(nextLikeTime-currentTime));
            }
            else{
                tvMessage.setText("00:00:00");
            }
        }

        Button btnOk=dialogView.findViewById(R.id.ok_button);
        btnOk.setTypeface(typeFaceManager.getCircularAirBook());
        btnOk.setOnClickListener(view -> {
            cancelDialog();
        });

        countDownTimer = new CountDownTimer(60*60*24*1000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if(forBoostTimer){
                    tvTitle.setText(R.string.boosted_profile_title);
                    boostExpireTime = dataSource.getBoostExpireTime();
                    long currentTime = System.currentTimeMillis();
                    if(currentTime < boostExpireTime){
                        tvMessage.setText(utility.getTimerFormatDateTime(boostExpireTime-currentTime));
                    }
                    else{
                        tvMessage.setText("00:00:00");
                    }
                }
                else {
                    tvTitle.setText(R.string.next_like_time_title);
                    nextLikeTime = dataSource.getNextLikeTime();
                    long currentTime = System.currentTimeMillis();
                    if(currentTime < nextLikeTime){
                        tvMessage.setText(utility.getTimerFormatDateTime(nextLikeTime-currentTime));
                    }
                    else{
                        tvMessage.setText("00:00:00");
                    }
                }
            }

            @Override
            public void onFinish() {
                countDownTimer.cancel();
            }
        };
        countDownTimer.start();

        dialog = builder.create();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
    }

    private void cancelDialog() {
        if(countDownTimer != null)
            countDownTimer.onFinish();
        if(dialog!=null)
            dialog.cancel();
    }
}
