package com.pairzy.com.home.Discover.ListFrg;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.pairzy.com.R;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.home.Discover.DiscoveryFragPresenter;
import com.pairzy.com.home.Discover.Model.AdapterCallback;
import com.pairzy.com.home.Discover.Model.UserItemPojo;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.SpendCoinDialog.CoinSpendDialogCallback;
import com.pairzy.com.util.SpendCoinDialog.WalletEmptyDialogCallback;

import java.util.Locale;

import javax.inject.Inject;
/**
 * <h2>ListDataViewFrgPresenter</h2>
 * <P>
 *
 * </P>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ListDataViewFrgPresenter implements ListDataViewFrgContract.Presenter,AdapterCallback,CoinSpendDialogCallback,WalletEmptyDialogCallback
{
    private ListDataViewFrgContract.View view;
    @Inject
    ListModel model;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    CoinDialog coinDialog;
    @Inject
    Activity activity;

    private int last_Position;
    private int currentPosition = -1;

    @Inject
    ListDataViewFrgPresenter(){}

    @Override
    public void takeView(ListDataViewFrgContract.View view)
    {
        this.view=view;
    }

    @Override
    public void dropView()
    {
        view=null;
    }

    @Override
    public void initListener()
    {
        if(view!=null)
            view.setAdapterListen(this);
    }


    @Override
    public void updateDataChanged()
    {
        if(view!=null)
            view.notifyDataChanged(DiscoveryFragPresenter.PAGE_COUNT==0);
    }

    @Override
    public void doLoadMore(int currentPos)
    {
        if(model.loadMoreRequired(currentPos))
        {
            if(view!=null)
                view.doLoadMore();
        }
    }


    @Override
    public void isEmpty()
    {

        if(model.isDataEmpty())
        {
            if(view!=null)
                view.showLoadingView();
        }
    }


    @Override
    public void onLike(int position)
    {
        UserItemPojo userItemPojo = model.getUserPojo(position);
        if(userItemPojo != null)
            if(userItemPojo.isAdView())
                return;
        if(view!=null)
            view.onLike(position);
    }

    @Override
    public void onDislike(int position)
    {
        UserItemPojo userItemPojo = model.getUserPojo(position);
        if(userItemPojo != null)
            if(userItemPojo.isAdView())
                return;
        if(view!=null)
            view.onDislike(position);
    }


    private void launchSpendCoinDialogForSuperlike(UserItemPojo userItemPojo) {
        Integer coinSpend = coinConfigWrapper.getCoinData().getSuperLike().getCoin();
        boolean isFemale = userItemPojo.getGender().equalsIgnoreCase("female");
        String title = String.format(Locale.ENGLISH,"Spend %s coins to let %s know you really like %s by super liking.",
                coinSpend,userItemPojo.getFirstName(),isFemale?"her":"him");
        String btnText = String.format(Locale.ENGLISH, " I am OK to spend %s coins", coinSpend);
        coinDialog.showCoinSpendDialog(title,activity.getString(R.string.superlike_spend_coin_title), btnText,this);
    }

    private void launchWalletEmptyDialogForSuperlike(){
        coinDialog.showWalletEmptyDialog(activity.getString(R.string.wallet_empty_title),activity.getString(R.string.empty_wallet_superlike_msg),this);
    }

    private void checkWalletForSuperlike(UserItemPojo userItemPojo) {
        if(model.isEnoughWalletBalanceToSuperLike()) {
            if(model.isSuperlikeSpendDialogNeedToShow()){
                launchSpendCoinDialogForSuperlike(userItemPojo);
            }
            else {
                initiateSuperlike();
            }
        }
        else{
            //show wallet empty dialog for superlike.
            launchWalletEmptyDialogForSuperlike();
        }
    }

    @Override
    public void onSuperLike(int position)
    {
        UserItemPojo userItemPojo = model.getUserPojo(position);
        if(userItemPojo != null) {
            if (userItemPojo.isAdView())
                return;
            currentPosition = position;
            checkWalletForSuperlike(userItemPojo);
        }
    }

    @Override
    public void onBoots(int position)
    {
        if(view!=null)
            view.openBoost();
    }


    @Override
    public void openUserDetails(int position,android.view.View data_view)
    {
        try {
            UserItemPojo userItemPojo = model.getUserPojo(position);
            if(userItemPojo != null)
                if(userItemPojo.isAdView())
                    return;

            String user_Details=model.getUserDetails(position);
            if(view!=null)
            {
                last_Position=position;
                view.openUserProfile(user_Details,data_view);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void openChatScreen(UserItemPojo userItemPojo) {
        if(userItemPojo != null)
            if(userItemPojo.isAdView())
                return;

        if(view != null)
        view.openChat(userItemPojo);
    }

    /*
     *On Handel result */
    @Override
    public boolean onHandelActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode== AppConfig.PROFILE_REQUEST&&resultCode== Activity.RESULT_OK)
        {
            if(last_Position==-1)
                return false ;
            Bundle result_data=data.getExtras();
            assert result_data != null;
            int result_action=result_data.getInt(AppConfig.RESULT_DATA);
            if(result_action==AppConfig.ON_SUPER_LIKE)
            {
                if(view!=null)
                    view.onSuperLike(last_Position);
            }else if(result_action==AppConfig.ON_LIKE)
            {
                if(view!=null)
                    view.onLike(last_Position);
            }else if(result_action==AppConfig.ON_DISLIKE)
            {
                if(view!=null)
                    view.onDislike(last_Position);
            }
            else if(result_action==AppConfig.ON_CHAT)
            {
                if(view!=null)
                    view.setChatListNeedToUpdate(true);
            }
            last_Position=-1;
            return true;
        }else
        {
            last_Position=-1;
            return false;
        }
    }


    @Override
    public void revertAction(UserItemPojo item)
    {
        if(item!=null)
        {
            model.addDataInPosition(item);
            if(view!=null)
                view.updateBothViewData();
        }
    }

    @Override
    public void initiateSuperlike() {
        if(view!=null)
            view.onSuperLike(currentPosition);
    }

    @Override
    public int getCurrentProfilePosition() {
        return currentPosition;
    }

    @Override
    public void showBoostViewCounter(boolean show, int viewCount) {
        if(view != null)
            view.showBoostViewCounter(show,viewCount);
    }

    @Override
    public void startCoinAnimation() {
        if(view != null)
            view.startCoinAnimation();
    }

    /*
     * coin dialog callback
     */
    @Override
    public void onOkToSpendCoin(boolean dontShowAgain) {
        initiateSuperlike();
    }

    /*
     * coin dialog callback
     */
    @Override
    public void onByMoreCoin() {
        if(view != null)
            view.launchCoinWallet();
    }
}

