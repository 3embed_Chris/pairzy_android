package com.pairzy.com.util.LocationProvider;

/**
 * @since  3/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface LocationApiCallback
{
    void onSuccess(Double lat, Double lng, double accuracy);
    void onError(String error);
}
