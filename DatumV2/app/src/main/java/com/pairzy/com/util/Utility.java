package com.pairzy.com.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;
import androidx.core.content.FileProvider;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.pairzy.com.BuildConfig;
import com.google.gson.Gson;
import java.io.File;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
/**
 * <h2>Utility</h2>
 * <P>
 *
 * </P>
 * @since  12/15/2017.
 * @author 3Embed.
 * @version 1.0.
 */
public class Utility
{
    private Handler handler;
    private Context mcontext;
    private Gson gson;
    public Utility(Context context)
    {
        mcontext=context;
        gson=new Gson();
        handler = new Handler();
    }

    /*
     * Get the vector drawable*/
    public Drawable getVectorDrawable(int resId)
    {
        Drawable icon;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            icon = VectorDrawableCompat.create(mcontext.getResources(),resId,mcontext.getTheme());
        } else {
            icon =mcontext.getResources().getDrawable(resId, mcontext.getTheme());
        }
        return icon;
    }

    public Gson getGson()
    {
        return gson;
    }

    /*
     * Printing the log.*/
    public void printLog(String title,String message)
    {
        if(BuildConfig.DEBUG)
            Log.d(title,message);
    }


    public String getDeviceMaker()
    {
        String deviceMan="";
        try
        {
            deviceMan = Build.MANUFACTURER;
        }catch (Exception e){
            e.printStackTrace();
        }
        return deviceMan;
    }

    public String getModel()
    {
        String model="";
        try
        {
            model = Build.MODEL;
        }catch (Exception e){
            e.printStackTrace();
        }
        return model;
    }

    //CoinConfigResponse  05/22/1990
    public double getTimeFromDate(String date)
    {
        try
        {
            SimpleDateFormat simpleFormatter=new SimpleDateFormat("dd/MM/yyyy",Locale.US);
            Date date_data =simpleFormatter.parse(date);
            return date_data.getTime();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return 0;
    }

    public  long timestamp(String date)
    {
        @SuppressLint("SimpleDateFormat")
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date formattedDate = null;
        try {
            formattedDate = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formattedDate.getTime();
    }

    public boolean isValidEmail(CharSequence target)
    {
        return (target.length() > 5 && !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }


    public void closeSpotInputKey(Activity activity,View view)
    {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void openSpotInputKey(Activity activity,View view)
    {
        if (view != null) {
            view.requestFocus();
            InputMethodManager imm =(InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    /**
     * open soft input key after a delay of 1 sec
     */
    public void openSoftInputKeyInDelay(Activity activity, View view) {
        try {

            handler.postDelayed(() -> {
                if (view != null) {
                    view.requestFocus();
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }
            }, 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*
     * Getting the file path*/
    public  String getRealPathFromURI(Uri contentUri)
    {
        String file_path=null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
        {
            try {
                file_path=getFilePath(mcontext,contentUri);
            } catch (URISyntaxException e)
            {
                e.printStackTrace();
            }
        }else
        {
            file_path=getRealPathFromURI_Under19(mcontext,contentUri);
        }
        return file_path;
    }


    /*
     * Under 19 api.*/
    private String getRealPathFromURI_Under19(Context context,Uri contentUri)
    {
        String[] proj = { MediaStore.Images.Media.DATA };
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if(cursor != null){
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }


    @SuppressLint("NewApi")
    private String getFilePath(Context context, Uri uri)throws URISyntaxException
    {
        String selection = null;
        String[] selectionArgs = null;
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri))
        {
            if (isExternalStorageDocument(uri))
            {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                //added for crash solution on video pick
                //"raw:/storage/emulated/0/Download/1533826061357.mp4"
                if (id.startsWith("raw:")) {
                    return id.replaceFirst("raw:", "");
                }
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme()))
        {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }



    /**
     * <h2>getUri_Path</h2>
     * <P>
     *
     * </P>*/
    public Uri getUri_Path(File file)
    {
        Uri uri;
        /*
         * Checking if the build version is greater then 25 then no need ask for runtime permission.*/
      //  if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N)
      //  {
            uri= FileProvider.getUriForFile(mcontext,mcontext.getApplicationContext().getPackageName() + ".provider",file);// }else
      //  {
      //      uri=Uri.fromFile(file);
      //  }
        return uri;
    }

    /*
     *Format the string */
    public String formatString(String sentence)
    {
        if(sentence==null)
            return "";
        String[] words=sentence.split(" ");
        StringBuilder text=new StringBuilder();
        int length=words.length;
        for(int count=0;count<length;count++)
        {
            text.append(properCase(words[count]));
            if(count!=(length-1))
                text.append(" ");
        }
        return text.toString();
    }

    /*
     * formatting the the words.*/
    private String properCase (String inputVal)
    {
        if (inputVal.length() == 0)
            return "";
        if (inputVal.length() == 1)
            return inputVal.toUpperCase();
        return inputVal.substring(0,1).toUpperCase()
                + inputVal.substring(1).toLowerCase();
    }


    public float pxToDp(final float px) {
        return px / mcontext.getResources().getDisplayMetrics().density;
    }

    /**
     * Converting dp to pixel
     */
    public int dpToPx(int dp)
    {
        Resources r =mcontext.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


    /*
     * Taking the screen shot.*/
    public Bitmap takeScreenShot(Activity activity)
    {
        View view = activity.getWindow().getDecorView();
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap b1 = view.getDrawingCache();
        Rect frame = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        int statusBarHeight = frame.top;
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        Bitmap b = Bitmap.createBitmap(b1, 0, statusBarHeight, width, height - statusBarHeight);
        view.destroyDrawingCache();
        return b;
    }


    public String getLocalIpAddress()
    {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();)
            {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();)
                {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress())
                    {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("ert23", "***** IP="+ ip);
                        return ip;
                    }
                }
            }
        } catch (SocketException ex)
        {
            Log.e("ert23", ex.toString());
        }
        return null;
    }

    /*
     * Search time zone.*/
    public String getLocalTimeZoneId()
    {
        TimeZone tz = TimeZone.getDefault();
        System.out.println("TimeZone   "+tz.getDisplayName(false, TimeZone.SHORT)+" Timezon id :: " +tz.getID());
        return tz.getID();
    }

    /*
     * Getting age*/
    public int getAgeFromDob(Double milliseconds)
    {
//        int year=getYearFromTime(milliseconds);
//        Calendar c = Calendar.getInstance();
//        c.setTimeInMillis(System.currentTimeMillis());
//        int current_year=c.get(Calendar.YEAR);
//        return current_year-year;
        long timeBetween = (long) (System.currentTimeMillis() - milliseconds);
        double yearsBetween = timeBetween / 3.15576e+10;
        int age = (int) Math.floor(yearsBetween);
        return age;
    }

    /*
     *Getting year value from long value */
    public int getYearFromTime(Double milliseconds)
    {
        try
        {
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(milliseconds.longValue());
            return c.get(Calendar.YEAR);
        }catch (Exception e)
        {
            return 0;
        }
    }


    /*
     *return data array format DD/MM/YYYY
     */
    public ArrayList<Integer> getDataArrayFromMiliSec(Long milliSec){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSec);
        calendar.setTimeZone(TimeZone.getDefault());
        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH);
        mMonth = mMonth+1;
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);

        ArrayList<Integer> dates =new ArrayList<>(9);
        dates.add(mDay/10);
        dates.add(mDay%10);
        dates.add(mMonth/10);
        dates.add(mMonth%10);
        dates.add(mYear/1000);
        dates.add((mYear/100)%10);
        dates.add((mYear%100)/10);
        dates.add(mYear%10);
        return dates;
    }


    /* Defining the times array list.*/
    private static final List<Long> times = Arrays.asList(
            TimeUnit.DAYS.toMillis(365),
            TimeUnit.DAYS.toMillis(30),
            TimeUnit.DAYS.toMillis(1),
            TimeUnit.HOURS.toMillis(1),
            TimeUnit.MINUTES.toMillis(1),
            TimeUnit.SECONDS.toMillis(1));
    /**
     * Defining the times string data.*/
    private static final List<String> timesString = Arrays.asList("year","month","day","hour","minute","second");

    /**
     * Time converter data.*/
    public String time_converter(long duration)
    {
        duration=System.currentTimeMillis()-duration;

        StringBuilder res = new StringBuilder();
        for(int i=0;i< times.size(); i++)
        {
            Long current = times.get(i);
            long temp = duration/current;
            if(temp>0)
            {
                res.append(temp).append(" ").append(timesString.get(i)).append(temp > 1 ? "s" : "").append(" ago");
                break;
            }
        }
        if("".equals(res.toString()))
            return "Just now";
        else
            return res.toString();
    }

    /**
     * Defining the times string data.*/
    private static final List<String> timesString_shfm= Arrays.asList("y","m","d","h","m","s");
    public String time_Converter_sort_fm(long duration)
    {
        duration=System.currentTimeMillis()-duration;

        StringBuilder res = new StringBuilder();
        for(int i=0;i< times.size(); i++)
        {
            Long current = times.get(i);
            long temp = duration/current;
            if(temp>0)
            {
                res.append(temp).append(timesString_shfm.get(i));
                break;
            }
        }
        if("".equals(res.toString()))
            return "";
        else
            return res.toString();
    }


    public String time_Converter_upcoming(long duration)
    {
        duration=duration-System.currentTimeMillis();

        StringBuilder res = new StringBuilder();
        for(int i=0;i< times.size(); i++)
        {
            Long current = times.get(i);
            long temp = duration/current;
            if(temp>0)
            {
                res.append(temp).append(timesString_shfm.get(i));
                break;
            }
        }
        if("".equals(res.toString()))
            return "";
        else
            return res.toString();
    }


    /**
     * Formatting the given data
     */
    public String getTimeFormatToShow(String time)
    {
        try
        {
            long l_time = Long.parseLong(time);
            return DateUtils.getRelativeTimeSpanString(l_time,
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS, 0)
                    .toString();
        }catch (Exception e)
        {
            return "";
        }
    }

    public String  getTimerFormatDateTime(Long dateMilli) {
        try {
            long sec = dateMilli / 1000 % 60;
            long min = dateMilli / (60 * 1000)  % 60;
            long hour = dateMilli / (60 * 60 * 1000) % 24 ;

            return String.format(Locale.ENGLISH, "%02d:%02d:%02d",hour,min,sec);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "00:00:00";
    }

    public String  getFormatDateTime(Long dateMilli) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("E, MMM dd @ hh:mm a");
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(dateMilli);
            return formatter.format(calendar.getTime());
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public String getAppCurrentVersion() {
        try {
            PackageInfo packageInfo = mcontext.getPackageManager().getPackageInfo(mcontext.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }


    public int getTheScreenHeight(Activity activity,int percent)
    {
        Display display =activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int mHeight = size.y;
        return mHeight*percent/100;
    }


    public int getTheScreenWidth(Activity activity,int percent)
    {
        Display display =activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int mWitdh = size.x;
        return mWitdh*percent/100;
    }

    public String formatCoinBalance(String coins){
        int formatBalance = -1;
        String symbol = "";
        try{
            Integer coinsInt = Integer.parseInt(coins);
            if(coinsInt / 10000000 == 0){
                if(coinsInt / 100000 == 0){
                    if(coinsInt / 1000 == 0){
                        symbol = "";
                    }
                    else{
                        symbol = "K";
                        formatBalance = coinsInt/1000;
                        if(coinsInt % 1000 != 0){
                            symbol = "K +";
                        }
                    }
                }
                else{
                    symbol = "L";
                    formatBalance = coinsInt/100000;
                    if(coinsInt % 100000 != 0){
                        symbol = "L +";
                    }
                }
            }
            else {
                symbol = "C";
                formatBalance = coinsInt/10000000;
                if(coinsInt% 10000000 != 0){
                    symbol = "K +";
                }
            }
            if(formatBalance == -1){
                formatBalance = Integer.parseInt(coins);
            }
            return formatBalance+symbol;
        }catch (Exception e){}
        return "";



    }

    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public boolean isValidVideoSize(long actualVideo){ //byte
        return  actualVideo <= AppConfig.PROFILE_VIDEO_SIZE;
    }

    public double kmToMiles(double dist) {
        try{
            return dist * 0.621371;
        }catch (Exception e){}
        return 0;
    }

    public String getdate(String stringDate)
    {
        Date startDate=new Date(Long.parseLong(stringDate));
        Date endDate = new Date();
        long different = endDate.getTime() - startDate.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;
        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;
        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;
        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;
        long elapsedSeconds = different / secondsInMilli;
        if(elapsedDays>0)
            return elapsedDays +" days ago";
        else if(elapsedHours>0)
            return elapsedHours+ " hours ago";
        else if(elapsedMinutes>0)
            return elapsedMinutes + " minutes ago";
        else return elapsedSeconds+ " seconds ago";
    }
}
