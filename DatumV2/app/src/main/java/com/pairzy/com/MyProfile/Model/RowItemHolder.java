package com.pairzy.com.MyProfile.Model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;

/**
 * @since  4/11/2018.
 */
public class RowItemHolder extends RecyclerView.ViewHolder
{
    TextView title;
    TextView selected_test;
    TextView count_data;
    View itemView;

    public RowItemHolder(View itemView, TypeFaceManager typeFaceManager)
    {
        super(itemView);
        this.itemView =itemView;
        title=itemView.findViewById(R.id.title);
        title.setTypeface(typeFaceManager.getCircularAirLight());
        selected_test=itemView.findViewById(R.id.selected_text);
        selected_test.setTypeface(typeFaceManager.getCircularAirBook());
        count_data=itemView.findViewById(R.id.count_data);
        count_data.setTypeface(typeFaceManager.getCircularAirBook());
    }
}
