package com.pairzy.com.coinWallet;

/**
 * <h>CoinWalletContract interface</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */
public interface CoinWalletContract {

    interface View{
        void applyFont();
        void showError(String error);
        void showMessage(String msg);
        void showMessage(int msgId);
        void showLoading();
        void showNetworkError(String error);
        void notifyAdapter();
    }

    interface Presenter{
        void init();
        void observeCoinBalanceChange();
        boolean isCoinBalanceUpdateRequired();
        void getCoinHistory();
    }
}