package com.pairzy.com.home.Prospects.Online.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * Created by user on 3/29/2018.
 */

public class OnlineResponseHolder
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<OnlineItemPojo> data = null;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public ArrayList<OnlineItemPojo> getData()
    {
        return data;
    }

    public void setData(ArrayList<OnlineItemPojo> data)
    {
        this.data = data;
    }
}
