package com.pairzy.com.boostLikes.Model;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigResponse;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Discover.Model.superLike.SuperLikeResponse;
import com.pairzy.com.home.HomeModel.LikeResponse;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by ankit on 7/9/18.
 */

public class BoostLikeModel extends BaseModel{


    @Inject
    ArrayList<BoostLikeData> arrayList;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Utility utility;
    @Inject
    BoostLikeAdapter adapter;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    CoinConfigWrapper coinConfigWrapper;


    @Inject
    public BoostLikeModel() {
    }

    public void parseLikeList(String response) {
        try{
            BoostLikeResponse boostLikeResponse = utility.getGson().fromJson(response,BoostLikeResponse.class);
            ArrayList<BoostLikeData> boostLikeData = boostLikeResponse.getDataList();
            if(boostLikeData != null && !boostLikeData.isEmpty()){
                arrayList.clear();
                arrayList.addAll(boostLikeData);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void notifyAdapter(){
        adapter.notifyDataSetChanged();
    }

    public boolean isListEmpty(){
        return  arrayList.isEmpty();
    }

    /*
     *Creating the form data for like service. */
    public Map<String, Object> setUserDetails(String user_id)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.DoLikeService.USER_ID, user_id);
        return map;
    }

    public void addDataInPosition(BoostLikeData item) {
        try
        {
            int actual_pos=item.item_actual_pos;
            if(actual_pos<1&&actual_pos>arrayList.size()-1)
            {
                arrayList.add(0,item);
            }else
            {
                arrayList.add(actual_pos,item);
            }
        }catch (Exception e){}
    }

    public void parseLikeResponse(String response) {
        try{
            LikeResponse likeResponse = utility.getGson().fromJson(response,LikeResponse.class);
            dataSource.setRemainsLinksCount(likeResponse.getRemainsLikesInString());
            dataSource.setNextLikeTime(likeResponse.getNextLikeTime());
        }catch (Exception e){

        }
    }

    public void parseSuperLike(String response)
    {
        try
        {
            SuperLikeResponse superLikeResponse = utility.getGson().fromJson(response, SuperLikeResponse.class);
            if(superLikeResponse.getCoinWallet() != null) {;
                coinBalanceHolder.setCoinBalance(String.valueOf(superLikeResponse.getCoinWallet().getCoin()));
                coinBalanceHolder.setUpdated(true);
            }
        }catch (Exception e){}

    }

    public boolean isDialogDontNeedToShow() {
        return dataSource.getShowSuperlikeCoinDialog();
    }

    public void parseCoinConfig(String response)
    {
        try
        {
            CoinConfigResponse coinConfigResponse =
                    utility.getGson().fromJson(response,CoinConfigResponse.class);
            if(!coinConfigResponse.getData().isEmpty())
                coinConfigWrapper.setCoinData(coinConfigResponse.getData().get(0));
        }catch (Exception e){}
    }

    public BoostLikeData getBoostLikeItem(int currentPosition) {
        try {
            return arrayList.get(currentPosition);
        }catch (Exception e){}
        return null;
    }
}

