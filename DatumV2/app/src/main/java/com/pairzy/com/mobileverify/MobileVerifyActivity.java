package com.pairzy.com.mobileverify;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatDelegate;
import android.view.View;
import android.widget.TextView;
import com.pairzy.com.MyProfile.MyProfilePagePresenter;
import com.pairzy.com.R;
import com.pairzy.com.home.HomeActivity;
import com.pairzy.com.mobileverify.main.PhnoFragment;
import com.pairzy.com.mobileverify.otp.Otp_Fragment;
import com.pairzy.com.register.RegisterPage;
import com.pairzy.com.util.CustomVideoView.NoInterNetView;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * <h2>MobileVerifyActivity</h2>
 * <p>
 * It verifies the mobile number and sends OTP
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 06/01/2018.
 **/
public class MobileVerifyActivity extends BaseDaggerActivity implements MobileVerifyContract.View
{
    public static final String param1="param1";
    public static final String param2="param2";
    public static final String param3="otp";
    public static String EDIT_PROFILE_TAG = null;
    public String phno;
    public String code;

    @Inject
    Activity activity;
    @Inject
    @Named(MobileVerifyActivityBuilder.ACTIVITY_FRAGMENT_MANAGER)
    FragmentManager fragmentManager;
    @Inject
    MobileVerifyContract.Presenter presenter;
    @Inject
    PhnoFragment mainFragment;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.no_internetView)
    protected NoInterNetView noInterNetView;
    private Unbinder unbinder;
    private boolean isActivityVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mobile_activity);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
        unbinder = ButterKnife.bind(this);
        //presenter.initNetworkObserver();
        presenter.moveFragment(mainFragment,true);
        getTag(getIntent());
    }

    @Override
    public void onResume() {
        super.onResume();
        isActivityVisible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActivityVisible = false;
    }

    private void getTag(Intent intent) {
        EDIT_PROFILE_TAG = intent.getStringExtra("edit_mobile");
        if(EDIT_PROFILE_TAG != null) {
            Bundle bundle = intent.getBundleExtra(MyProfilePagePresenter.PHONE_DATA_BUNDLE);
            phno = bundle.getString(MyProfilePagePresenter.PHONE_DATA);
            code = bundle.getString(MyProfilePagePresenter.COUNTRY_CODE);
        }
    }

    @Override
    protected void onDestroy()
    {
        unbinder.unbind();
        presenter.dropView();
        super.onDestroy();
    }

    @Override
    public void onBackPressed()
    {
        if(!handelBackPressed())
        {
            this.finish();
        }else
        {
            super.onBackPressed();
        }
    }

    /*
     *Handling the back press */
    public boolean handelBackPressed()
    {
        int count=fragmentManager.getBackStackEntryCount();
        return count > 1;
    }


    /**
     * remove top fragment from the container
     */
    public void removeTopFragment()
    {
        new Handler().postDelayed(() -> {
            if(handelBackPressed())
            {
                try
                {
                    List<Fragment> chield_list=fragmentManager.getFragments();
                    Fragment fragment_data=chield_list.get(chield_list.size()-1);
                    if(fragment_data != null)
                        fragmentManager.beginTransaction().remove(fragment_data).commitAllowingStateLoss();
                }catch (Exception e){}

            }
        },1000);
    }

    @Override
    public void openSignUP(Bundle data)
    {
        Intent intent=new Intent(this,RegisterPage.class);
        intent.putExtras(data);
        startActivity(intent);
        removeTopFragment();
    }

    @Override
    public void openHomePage(Bundle data)
    {
        Intent intent=new Intent(this,HomeActivity.class);
        intent.putExtras(data);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void moveFragment(DaggerFragment fragment, boolean isBackRequired)
    {
        if(fragment instanceof Otp_Fragment)
        {
            clearBackFrgFragments();
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container,fragment);
        if(isBackRequired) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void updateInterNetStatus(boolean status)
    {
        if(noInterNetView!=null)
        {
            if(status)
            {
                noInterNetView.internetFound();
            }else
            {
                noInterNetView.showNoInternet();
            }
        }
    }

    /*
     *openMainPhnoFrg*/
    @Override
    public void openMainPhnoAgainFrg()
    {
        int count=fragmentManager.getBackStackEntryCount();
        count=count-1;
        if(count>0 && isActivityVisible)
        {
            for(int i=0;i<count;i++)
            {
                fragmentManager.popBackStack();
            }
        }
    }

    /*
     * re open otp page*/
    public void clearBackFrgFragments()
    {
        int count=fragmentManager.getBackStackEntryCount();
        count=count-1;
        if(count>0 && isActivityVisible)
        {
            for(int i=0;i<count;i++)
            {
                fragmentManager.popBackStack();
            }
        }
    }

    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void setResultData(String phno) {
        if(phno != null) {
            Intent intent = new Intent();
            intent.putExtra(MyProfilePagePresenter.PHONE_DATA, phno);
            setResult(RESULT_OK, intent);
        }
        else {
            setResult(RESULT_CANCELED);
        }
        this.finish();
        activity.overridePendingTransition(R.anim.slide_from_left,R.anim.slide_to_right);
    }
}

