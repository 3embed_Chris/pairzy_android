package com.pairzy.com.home.Matches.Chats.Model;

/**
 * @since  4/28/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface MatchAdapterCallback
{
    void openChat(int position);
}
