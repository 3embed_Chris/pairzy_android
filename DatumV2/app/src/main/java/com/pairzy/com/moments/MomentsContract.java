package com.pairzy.com.moments;

import android.content.Intent;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.MyProfile.Model.MomentsData;

/**
 * <h2>MomentsContract</h2>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */

public interface MomentsContract  {

    interface View extends BaseView {

        void showMessage(String message);
        void showError(String error);

        void notifyDataAdapter(int position);
        void launchChatScreen(Intent intent);
        void launchWriteCommit(String postId);

        void launchCommentView(String postId);
        void launchLikeView(String postId);
        void notifyDataAdapter();

    }

    interface  Presenter extends BasePresenter<View> {
        void setAdapterCallback();
        void likeDislikeUserPost(double type, String postId);
        void launchChat(MomentsData momentsData);
        void parseDateData(int requestCode, int resultCode, Intent data);
    }
}
