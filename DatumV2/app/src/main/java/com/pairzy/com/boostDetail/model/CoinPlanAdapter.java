package com.pairzy.com.boostDetail.model;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pairzy.com.R;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

/**
 * Created by ankit on 1/6/18.
 */

public class CoinPlanAdapter extends RecyclerView.Adapter<CoinPlanViewHolder> {

    private ArrayList<CoinPlan> coinPlanList;
    private TypeFaceManager typeFaceManager;
    private ItemActionCallBack callBack;

    public CoinPlanAdapter(ArrayList<CoinPlan> coinPlanList,TypeFaceManager typeFaceManager) {
        this.coinPlanList = coinPlanList;
        this.typeFaceManager = typeFaceManager;
    }

    @NonNull
    @Override
    public CoinPlanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.coin_plan_item,parent,false);
        return new CoinPlanViewHolder(view,callBack,typeFaceManager);
    }

    @Override
    public void onBindViewHolder(@NonNull CoinPlanViewHolder holder, int position) {
        try {
            bindsCoinPlanView(holder);
        }catch (Exception ignored){}
    }

    private void bindsCoinPlanView(CoinPlanViewHolder holder) {
        CoinPlan coinPlan = coinPlanList.get(holder.getAdapterPosition());
        holder.tvCoinPlanTitle.setText(coinPlan.getPlanName());
        holder.tvCoinPlanButtonText.setText(coinPlan.getCurrencySymbole().concat(" "+coinPlan.getCost()));
    }

    @Override
    public int getItemCount() {
        return coinPlanList.size();
    }

    public void setClickCallback(ItemActionCallBack clickCallback) {
        this.callBack = clickCallback;
    }
}
