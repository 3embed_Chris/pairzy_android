package com.pairzy.com.MqttChat.DocumentPicker.Utils;

/**
 * Created by moda on 22/08/17.
 */

public enum Orientation {
    PORTRAIT_ONLY,
    LANDSCAPE_ONLY,
    UNSPECIFIED
}