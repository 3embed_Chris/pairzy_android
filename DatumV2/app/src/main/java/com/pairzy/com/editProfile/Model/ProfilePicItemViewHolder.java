package com.pairzy.com.editProfile.Model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfilePicItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private ViewHolderClickCallback clickCallback;

    @BindView(R.id.sdv_profile_pic)
    SimpleDraweeView sdvProfilePic;
    @BindView(R.id.rl_close)
    RelativeLayout rlClose;
    @BindView(R.id.btn_play)
    ImageView ivChecked;
    @BindView(R.id.tv_item_title)
    TextView tvProfileText;
    @BindView(R.id.profile_check_box)
    ImageView ivProfileCheckBox;
    @BindView(R.id.ll_set_profile)
    LinearLayout llSetProfile;

    public ProfilePicItemViewHolder(View itemView, ViewHolderClickCallback clickCallback) {
        super(itemView);
        this.clickCallback = clickCallback;

        ButterKnife.bind(this,itemView);
        rlClose.setOnClickListener(this);
        sdvProfilePic.setOnClickListener(this);
        ivChecked.setImageResource(R.drawable.ic_check_circle);
        tvProfileText.setText("Profile_pic");
        llSetProfile.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(clickCallback != null)
            clickCallback.onClick(v, getAdapterPosition());
    }
}