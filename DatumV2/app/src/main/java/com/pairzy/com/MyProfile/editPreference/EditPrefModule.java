package com.pairzy.com.MyProfile.editPreference;

import android.app.Activity;

import com.pairzy.com.MyProfile.editPreference.newEditInputFrag.NewUserInputFrg;
import com.pairzy.com.MyProfile.editPreference.newEditInputFrag.NewUserInputFrgBuilder;
import com.pairzy.com.MyProfile.editPreference.newListScrollerFrag.NewListdataFrg;
import com.pairzy.com.MyProfile.editPreference.newListScrollerFrag.NewListdataFrgBuilder;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * <h>EditPrefModule</h>
 * @author 3Embed.
 * @since 23/4/18.
 */

@Module
public abstract class EditPrefModule {

    @ActivityScoped
    @Binds
    abstract EditPrefContract.Presenter editPrefPresenter(EditPrefPresenter presenter);

    @ActivityScoped
    @Binds
    abstract EditPrefContract.View editProfileView(EditPrefActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity editPreferanceActivity(EditPrefActivity activity);

    @FragmentScoped
    @ContributesAndroidInjector(modules = {NewListdataFrgBuilder.class})
    abstract NewListdataFrg getNewListdataFrg();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {NewUserInputFrgBuilder.class})
    abstract NewUserInputFrg getNewUserInputFrg();


}
