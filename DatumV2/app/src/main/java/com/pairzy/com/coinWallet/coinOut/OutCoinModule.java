package com.pairzy.com.coinWallet.coinOut;

import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 *<h>OutCoinModule</h>
 * <p></p>
 *@author 3Embed.
 *@since 30/5/18.
 */

@Module
public abstract class OutCoinModule {

    @FragmentScoped
    @Binds
    abstract OutCoinFrag outCoinFrag(OutCoinFrag outCoinFrag);

    @FragmentScoped
    @Binds
    abstract OutCoinContract.Presenter bindsOutCoinPresenter(OutCoinPresenter presenter);

}
