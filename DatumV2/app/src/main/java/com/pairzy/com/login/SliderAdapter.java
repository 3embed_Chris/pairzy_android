package com.pairzy.com.login;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
/**
 * <h2>SliderAdapter</h2>
 * <P>Adapter for horizontal swipe screens</P>
 *
 * @author 3Embed.
 * @version 1.0.
 * @see LoginContract.Presenter
 * @since 04/01/2018.
 */
public class SliderAdapter extends PagerAdapter
{
    @BindView(R.id.image)
    ImageView ivImage;

    @BindView(R.id.title)
    TextView tvTitle;

    private Context context;
    private TypeFaceManager typeFaceManager;

    private List<Integer> imgs;
    private List<String> title;


    public SliderAdapter(Context context,TypeFaceManager typeFaceManager)
    {
        this.context = context;
        this.typeFaceManager=typeFaceManager;
        intiData();
    }

    @Override
    public int getCount()
    {
        return imgs.size();
    }

    /*
     *Initialization of the adapter */
    private void intiData()
    {
        imgs = new ArrayList<>();
        imgs.add(R.drawable.walk_one);
        imgs.add(R.drawable.walk_two);
        imgs.add(R.drawable.walk_three);
        imgs.add(R.drawable.walk_four);

        title = new ArrayList<>();
        title.add(context.getResources().getString(R.string.landing_title1));
        title.add(context.getResources().getString(R.string.landing_title2));
        title.add(context.getResources().getString(R.string.landing_title3));
        title.add(context.getResources().getString(R.string.landing_title4));
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position)
    {
        LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        @SuppressLint("InflateParams")
        View view = inflater.inflate(R.layout.item_slider, null);
        ButterKnife.bind(this, view);
        ivImage.setImageResource(imgs.get(position));
        tvTitle.setText(title.get(position));
        tvTitle.setTypeface(typeFaceManager.getCircularAirBold());
        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);
        return view;
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }

}
