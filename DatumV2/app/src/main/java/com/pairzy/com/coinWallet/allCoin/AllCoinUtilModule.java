package com.pairzy.com.coinWallet.allCoin;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.coinWallet.CoinWalletUtil;
import com.pairzy.com.coinWallet.Model.AllCoinAdapter;
import com.pairzy.com.coinWallet.Model.CoinPojo;
import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 *<h>AllCoinUtilModule</h>
 * <p></p>
 *@author 3Embed.
 *@since 30/5/18.
 */
@Module
public class AllCoinUtilModule {

    @FragmentScoped
    @Provides
    LinearLayoutManager provideLayoutManager(Activity activity){
        return new LinearLayoutManager(activity);
    }

    @FragmentScoped
    @Provides
    AllCoinAdapter provideAllCoinAdapter(@Named(CoinWalletUtil.COIN_ALL_LIST)ArrayList<CoinPojo> coinList,
                                         TypeFaceManager typeFaceManager, Utility utility){
        return new AllCoinAdapter(coinList,typeFaceManager,utility);
    }
}
