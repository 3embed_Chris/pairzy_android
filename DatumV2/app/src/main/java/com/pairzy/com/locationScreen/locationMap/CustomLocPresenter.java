package com.pairzy.com.locationScreen.locationMap;

import javax.inject.Inject;

/**
 * <h>CustomLocPresenter class</h>
 * @author 3Embed.
 * @since 8/5/18.
 * @version 1.0.
 */

public class CustomLocPresenter implements CustomLocContract.Presenter {

    @Inject
    CustomLocContract.View view;

    @Inject
    CustomLocPresenter(){
    }


    @Override
    public void init() {
        if(view != null){
            view.applyFont();
        }
    }

    @Override
    public void takeView(CustomLocContract.View view) {
        //already injected
    }

    @Override
    public void dropView() {
        this.view = null;
    }
}
