package com.pairzy.com.home.Dates.Pending_page;

import android.content.Intent;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;
import com.pairzy.com.home.Dates.Pending_page.Model.PendingListAdapter;

/**
 * <h2>PastDateFrgContract</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface PendingFrgContract
{
    interface View extends BaseView
    {
        void showLoading();
        void notifyAdapter();
        void doLoadMore();
        void showMessage(String s);
        void refreshList();
        void showError(String error);
        void showError(int errorId);
        void showNetworkError(String error);
        void emptyData();
        void addToTheUpcomingList(DateListPojo removedDate);
        void launchCallDateScreen(DateListPojo dateListPojo);
        void launchPhyDateScreen(DateListPojo dateListPojo);
        void launchUserProfile(DateListPojo dateListPojo);
        void addTothePastDate(PastDateListPojo pastDateListPojo);
        void launchCoinWallet();
        void updatePendingDateCount(int pendingDateCount);
    }

    interface Presenter extends BasePresenter
    {
        void notifyPendingAdapter();
        void doLoadMore(int positionView);
        void setAdapterCallBack(PendingListAdapter pendingListAdapter);
        boolean checkDateExist();
        void parseDateData(int requestCode, int resultCode, Intent data);
        void observeDateListChange();
    }
}
