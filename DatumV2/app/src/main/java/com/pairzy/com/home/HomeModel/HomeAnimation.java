package com.pairzy.com.home.HomeModel;

import android.content.Context;
import android.media.MediaPlayer;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.pairzy.com.R;

/**
 * Created by ankit on 6/9/18.
 */

public class HomeAnimation {

    private Context context;
    private boolean isCoinAnimRunning = false;
    private MediaPlayer mediaPlayer;
    private Animation coinAnimOne, coinAnimTwo, coinAnimThree;


    public HomeAnimation(Context context) {
        this.context = context;
        mediaPlayer = MediaPlayer.create(context,R.raw.coin_spend);
    }

    public Animation getSlowBlinkingAnimation()
    {
        return AnimationUtils.loadAnimation(context, R.anim.superlike_blink);
    }

    private Animation getPopupAnimation()
    {
        return AnimationUtils.loadAnimation(context, R.anim.pop);
    }

    private Animation getPopDownAnimation()
    {
        return AnimationUtils.loadAnimation(context, R.anim.pop_down);
    }

    public void startPopUpAnimation(View view) {
        Animation popUpAnim = getPopupAnimation();
        view.startAnimation(popUpAnim);
        view.setVisibility(View.VISIBLE);
        popUpAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.clearAnimation();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void startPopDownAnimation(View view) {
        Animation popDownAnim = getPopDownAnimation();
        view.startAnimation(popDownAnim);
        popDownAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.clearAnimation();
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    private Animation getCoinAnimationOne()
    {
        return AnimationUtils.loadAnimation(context,R.anim.coin_anim_one);
    }
    private Animation getCoinAnimationTwo()
    {
        return AnimationUtils.loadAnimation(context,R.anim.coin_anim_two);
    }
    private Animation getCoinAnimationThree()
    {
        return AnimationUtils.loadAnimation(context,R.anim.coin_anim_three);
    }

    public void startCoinAnimation(View textView, View coinViewOne,View coinViewTwo,View coinViewThree) {
        if(!isCoinAnimRunning) {
            mediaPlayer.start();
            textView.startAnimation(getCoinAnimationTwo());
            coinViewOne.startAnimation(getCoinAnimationOne());
            coinViewTwo.startAnimation(getCoinAnimationTwo());
            coinViewThree.startAnimation(getCoinAnimationThree());
            getCoinAnimationThree().setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }
                @Override
                public void onAnimationEnd(Animation animation) {
                    isCoinAnimRunning = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }
}
