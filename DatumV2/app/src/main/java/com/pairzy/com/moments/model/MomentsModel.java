package com.pairzy.com.moments.model;

import com.pairzy.com.BaseModel;
import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class MomentsModel extends BaseModel {

    @Inject
    Utility utility;
    @Inject
    ArrayList<MomentsData> momentsDataList;

    @Inject
    public MomentsModel() {
    }


    public MomentsData getMomentPostItem(int position) {
        try{
            return momentsDataList.get(position);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public Map<String, Object> getLikeUnlikeData(double type, String postId) {
        Map<String, Object> map=new HashMap<>();
        map.put(ApiConfig.LIKE_UNLIKE.POST_ID,postId);
        map.put(ApiConfig.LIKE_UNLIKE.TYPE,type);
        return map;
    }

    public boolean deleteMomentPost(String postId, int position) {
        try{
            MomentsData momentsData = momentsDataList.get(position);
            if(momentsData.getPostId().equals(postId)){
                momentsDataList.remove(position);
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public Map<String, Object> getDeletePostBody(String postId) {
        Map<String,Object> map = new HashMap<>();
        map.put(ApiConfig.DELETE_POST.POST_ID,postId);
        return map;
    }
}
