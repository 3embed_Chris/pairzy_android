package com.pairzy.com.moments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.pairzy.com.moments.model.ExoPlayerRecyclerView;
import com.pairzy.com.moments.model.MomentsVerticalAdapter;
import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.R;
import com.pairzy.com.commentPost.CommentPostActivity;
import com.pairzy.com.likes.LikesByActivity;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.TypeFaceManager;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h2>MomentsActivity</h2>
 * <P> this is a activity provide grid view</P>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */
public class MomentsActivity extends DaggerAppCompatActivity implements MomentsContract.View {

    @Inject
    MomentsContract.Presenter presenter;
    @Inject
    ArrayList<MomentsData> momentsData;
    @Inject
    MomentsVerticalAdapter momentsAdapter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    Activity activity;

    @BindView(R.id.rvMoments)
    ExoPlayerRecyclerView rvMoments;
    @BindView(R.id.tvMomentsTitle)
    TextView tvMomentTitle;
    @BindView(R.id.cl_root)
    ConstraintLayout clRoot;

    private boolean firstTime;
    private Unbinder unbinder;
    private int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moments);
        unbinder = ButterKnife.bind(this);

        applyFont();
        ArrayList<MomentsData> momentsDataList = getIntent().getParcelableArrayListExtra(ApiConfig.MOMENTS.MOMENTS_LIST);
        position = getIntent().getIntExtra(ApiConfig.MOMENTS.POSITION,0);

        momentsData.clear();
        momentsData.addAll(momentsDataList);

        rvMoments.setHasFixedSize(true);
        rvMoments.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL,false));
        rvMoments.setMediaObjects(momentsData);
        rvMoments.setAdapter(momentsAdapter);
        rvMoments.scrollToPosition(position);

        RecyclerView.ItemAnimator animator = rvMoments.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        presenter.setAdapterCallback();
        if(firstTime){
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    rvMoments.playVideo(false);
                }
            });
            firstTime = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(rvMoments != null)
            rvMoments.resumePlayer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(rvMoments != null)
            rvMoments.pausePlayer();
    }

    private void applyFont() {
        tvMomentTitle.setTypeface(typeFaceManager.getCircularAirBold());
    }

    @OnClick(R.id.fl_back_button)
    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        intent.putExtra(ApiConfig.MOMENTS.MOMENTS_LIST,momentsData);
        setResult(RESULT_OK,intent);
        super.onBackPressed();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        presenter.dropView();
    }

    @Override
    public void showMessage(String message) {
        Snackbar snackbar = Snackbar
                .make(clRoot,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar
                .make(clRoot,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void notifyDataAdapter(int position) {
        momentsAdapter.notifyItemChanged(position);
    }

    @Override
    public void launchChatScreen(Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    @Override
    public void launchWriteCommit(String postId) {
        Intent intent=new Intent(this, CommentPostActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(ApiConfig.COMMENT.POST_ID, postId);
        startActivityForResult(intent, AppConfig.COMMENT_SCREEN_REQ_CODE);
    }

    @Override
    public void launchCommentView(String postId) {
        launchWriteCommit(postId);
    }

    @Override
    public void  launchLikeView(String postId) {
        Intent intent=new Intent(this, LikesByActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(ApiConfig.COMMENT.POST_ID, postId);
        startActivity(intent);
    }

    @Override
    public void notifyDataAdapter() {
        momentsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.parseDateData(requestCode,resultCode,data);
    }
}
