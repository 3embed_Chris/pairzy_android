package com.pairzy.com.home.Prospects.LikesMe;
import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * @since  3/23/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface LikesMeBuilder
{
    @FragmentScoped
    @Binds
    LikesMeFrg getProspectItemFragment(LikesMeFrg likesMeFrg);

    @FragmentScoped
    @Binds
    LikesMeContract.Presenter taskPresenter(LikesMePresenter presenter);
}
