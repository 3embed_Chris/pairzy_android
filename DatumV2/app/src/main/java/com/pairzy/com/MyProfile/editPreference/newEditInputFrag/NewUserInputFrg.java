package com.pairzy.com.MyProfile.editPreference.newEditInputFrag;


import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.MyProfile.editPreference.EditPrefPresenter;
import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.model.PrefData;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * <h2>NewUserInputFrg</h2>
 * <P>
 * A simple {@link Fragment} subclass.
 * </P>
 * @author 3embed.
 * @version 1.0.
 * @since 17-02-2017.
 */
@ActivityScoped
public class NewUserInputFrg extends DaggerFragment implements NewUserInputContract.View,TextWatcher
{

    @Inject
    Utility utility;
    @Inject
    Activity activity;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    EditPrefPresenter mainpresenter;
    @Inject
    NewUserInputContract.Presenter presenter;

    private Unbinder unbinder;
    @BindView(R.id.skip_page)
    TextView skip_page;
    @BindView(R.id.first_title)
    TextView first_title;
    @BindView(R.id.second_title)
    TextView second_title;
    @BindView(R.id.input_name)
    EditText input_name;
    @BindView(R.id.btnNext)
    RelativeLayout btnNext;
    @BindView(R.id.back_button_img)
    ImageView back_button_img;
    private PrefData currentPrefData;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle bundle=getArguments();
        assert bundle != null;
        currentPrefData = (PrefData) bundle.getSerializable("pref_data");
    }

    @Inject
    public NewUserInputFrg() {}
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.new_fragment_user_input_frg, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder= ButterKnife.bind(this,view);
        presenter.takeView(this);
        upDateUI();
        collectData();
    }

    private void setCurrentData() {
        if(currentPrefData != null){
            ArrayList<String> selectedList = new ArrayList<>();
            selectedList = currentPrefData.getSelectedValues();
            if(selectedList != null && !selectedList.isEmpty()){
                input_name.setText(selectedList.get(0));
            }
        }
    }

    /*
   * Updating the required ui updating like fonts etc.*/
    private void upDateUI()
    {
        //btnNext.setEnabled(false);
        skip_page.setTypeface(typeFaceManager.getCircularAirBook());
        first_title.setTypeface(typeFaceManager.getCircularAirBold());
        second_title.setTypeface(typeFaceManager.getCircularAirBold());
        input_name.setTypeface(typeFaceManager.getCircularAirBook());
        input_name.addTextChangedListener(this);
        input_name.setOnEditorActionListener((v, actionId, event) -> {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE))
            {
                if(input_name.getText().toString().length()>0)
                {
                    presenter.updatePreference(currentPrefData.getId(),input_name.getText().toString());
                }else
                {
                    presenter.showError();
                }
            }
            return false;
        });
        utility.openSpotInputKey(activity,input_name);
    }

    /*
     *collecting the details. */
    private void collectData()
    {
        first_title.setText(utility.formatString(currentPrefData.getTitle()));
        second_title.setText(utility.formatString(currentPrefData.getLabel()));
        if(currentPrefData.getOptions().size()>0)
        {
            setCurrentData();
            input_name.setHint(currentPrefData.getOptions().get(0));
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
    }

    @OnClick(R.id.close_button)
    void onBackClicked()
    {
        utility.closeSpotInputKey(activity,input_name);
        activity.onBackPressed();
    }

    @OnClick(R.id.parentLayout)
    void onParentClicked()
    {

    }

    @OnClick(R.id.skip_page)
    void onSkip()
    {
        activity.onBackPressed();
    }
    @OnClick(R.id.btnNext)
    void onNextClicked()
    {
        if(input_name.getText().toString().length()>0)
        {
            ArrayList<String> selectedValue = new ArrayList<>();
            selectedValue.add(input_name.getText().toString());
            currentPrefData.setSelectedValues(selectedValue);
            mainpresenter.updatePreferenceData(currentPrefData);
            onBackClicked();
        }else
        {
            presenter.showError();
        }
    }
    @Override
    public void showMessage(String message)
    {
        mainpresenter.showMessage(message);
    }

    @Override
    public void showError(String error)
    {
        mainpresenter.showError(error);
    }

    @Override
    public String getErrorTitle() {
        return second_title.getText().toString();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2){}
    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {
        handelNextButton(charSequence.toString().length()>0);
    }
    @Override
    public void afterTextChanged(Editable editable) {}

    /*
     *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            btnNext.setEnabled(true);
        }
        else {
            btnNext.setEnabled(false);
        }
    }
}
