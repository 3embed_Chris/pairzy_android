package com.pairzy.com.home.Matches.Chats.Model;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.util.DatumCallbacks.ListItemClick;
import com.pairzy.com.util.TypeFaceManager;
import com.facebook.drawee.view.SimpleDraweeView;
/**
 * <h2>MatchedItemHolder</h2>
 * @since  4/28/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MatchedItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener
{
    private ListItemClick callback;
    public SimpleDraweeView user_image;
    public ImageView user_status_dot, superLikedMeIcon;
    public TextView user_name;
    public MatchedItemHolder(View itemView, TypeFaceManager typeFaceManager, ListItemClick callback)
    {
        super(itemView);
        this.callback=callback;
        this.user_image=itemView.findViewById(R.id.user_profile_pic);
        this.user_status_dot=itemView.findViewById(R.id.user_status_dot);
        this.superLikedMeIcon = itemView.findViewById(R.id.user_super_liked_me);
        this.user_name=itemView.findViewById(R.id.user_name);
        this.user_name.setTypeface(typeFaceManager.getCircularAirBook());
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        if(callback!=null)
            callback.onClicked(R.id.parent_view,this.getAdapterPosition());
    }
}
