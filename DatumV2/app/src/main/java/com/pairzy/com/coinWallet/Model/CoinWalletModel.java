package com.pairzy.com.coinWallet.Model;

import com.pairzy.com.BaseModel;
import com.pairzy.com.coinWallet.CoinWalletUtil;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.coinBalance.CoinResponse;
import com.pairzy.com.data.model.coinBalance.Coins;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * <h>CoinWalletModel</h>
 * <p>Contain helper method for CoinWalletPresenter</p>
 * @author 3Embed.
 * @since 4/6/18.
 */
public class CoinWalletModel extends BaseModel {
    @Inject
    Utility utility;
    @Inject
    CoinBalanceHolder coinBalanceHolder;

    @Named(CoinWalletUtil.COIN_ALL_LIST)
    @Inject
    ArrayList<CoinPojo> allCoinList;
    @Named(CoinWalletUtil.COIN_IN_LIST)
    @Inject
    ArrayList<CoinPojo> inCoinList;
    @Named(CoinWalletUtil.COIN_OUT_LIST)
    @Inject
    ArrayList<CoinPojo> outCoinList;

    @Inject
    public CoinWalletModel() {
    }

    public void parseCoinBalance(String response) {
        try{
            CoinResponse coinResponse = utility.getGson().fromJson(response,CoinResponse.class);
            Coins coins = coinResponse.getData().getCoins();
            coinBalanceHolder.setCoinBalance(String.valueOf(coins.getCoin()));
            coinBalanceHolder.setUpdated(true);
        }catch (Exception e){
        }
    }

    public String getCoinBalance() {
        return coinBalanceHolder.getCoinBalance();
    }

    public void parseCoinHistory(String response) {
        try {
            allCoinList.clear();
            inCoinList.clear();
            outCoinList.clear();
            CoinHistory coinHistory = utility.getGson().fromJson(response,CoinHistory.class);
            Data data = coinHistory.getData();
            if(data.getAllCoins() != null){
                allCoinList.addAll(data.getAllCoins());
            }
            if(data.getCoinIn() != null){
                inCoinList.addAll(data.getCoinIn());
            }
            if(data.getCoinOut() != null){
                outCoinList.addAll(data.getCoinOut());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
