package com.pairzy.com.util.ChatDetailMenu;
/**
 * <h2>ChatMenuCallback</h2>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface ChatMenuCallback
{
    void onDeleteChat();
    void onMuteNotification(boolean mute);
    void onReportUser();
    void onBlockUser();
    void onUnblockUser();
    void onUnmatch();
}
