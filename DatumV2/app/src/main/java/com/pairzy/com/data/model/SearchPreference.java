package com.pairzy.com.data.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * <h2>SearchPreference</h2>
 * <P>
 *
 * </P>
 * @since  4/11/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class SearchPreference
{
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("PreferenceTitle")
    @Expose
    private String preferenceTitle;
    @SerializedName("TypeOfPreference")
    @Expose
    private String typeOfPreference;
    @SerializedName("OptionsValue")
    @Expose
    private ArrayList<String> optionsValue = null;
    private ArrayList<String> optionsValueSecond = null;
    @SerializedName("Priority")
    @Expose
    private Integer priority;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("selectedValue")
    @Expose
    private ArrayList<String> selectedValue = null;

    @SerializedName("optionsUnits")
    @Expose
    private ArrayList<String> optionsUnits = null;
    @SerializedName("selectedUnit")
    @Expose
    private String selectedUnit;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPreferenceTitle() {
        return preferenceTitle;
    }

    public void setPreferenceTitle(String preferenceTitle) {
        this.preferenceTitle = preferenceTitle;
    }

    public String getTypeOfPreference() {
        return typeOfPreference;
    }

    public void setTypeOfPreference(String typeOfPreference) {
        this.typeOfPreference = typeOfPreference;
    }

    public ArrayList<String> getOptionsValue() {
        return optionsValue;
    }

    public void setOptionsValue(ArrayList<String> optionsValue) {
        this.optionsValue = optionsValue;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public ArrayList<String> getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(ArrayList<String> selectedValue) {
        this.selectedValue = selectedValue;
    }

    public ArrayList<String> getOptionsUnits() {
        return optionsUnits;
    }

    public void setOptionsUnits(ArrayList<String> optionsUnits) {
        this.optionsUnits = optionsUnits;
    }

    public String getSelectedUnit() {
        return selectedUnit;
    }

    public void setSelectedUnit(String selectedUnit) {
        this.selectedUnit = selectedUnit;
    }

    public ArrayList<String> getOptionsValueSecond() {
        return optionsValueSecond;
    }

    public void setOptionsValueSecond(ArrayList<String> optionsValueSecond) {
        this.optionsValueSecond = optionsValueSecond;
    }
}
