package com.pairzy.com.editProfile.Model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pairzy.com.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfilePicEmptyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private ViewHolderClickCallback clickCallback;

    @BindView(R.id.iv_empty_item_icon)
    ImageView ivEmptyItemIcon;
    @BindView(R.id.rl_empty_item)
    RelativeLayout rlEmptyItem;
    ProfilePicEmptyViewHolder(View itemView, ViewHolderClickCallback clickCallback) {
        super(itemView);
        this.clickCallback = clickCallback;
        ButterKnife.bind(this,itemView);
        ivEmptyItemIcon.setOnClickListener(this);
        rlEmptyItem.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(clickCallback != null)
            clickCallback.onClick(v,getAdapterPosition());
    }

}