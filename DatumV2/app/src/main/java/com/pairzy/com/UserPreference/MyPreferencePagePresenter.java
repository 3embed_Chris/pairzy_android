package com.pairzy.com.UserPreference;

import android.app.Activity;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.UserPreference.Model.PreferenceResponsePojo;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.networking.RxNetworkObserver;
import com.pairzy.com.util.Utility;
import java.io.IOException;
import javax.inject.Inject;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * <h2>MyPreferencePagePresenter</h2>
 * @since 1/22/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MyPreferencePagePresenter implements MyPreferencePageContract.Presenter
{
    @Inject
    Activity activity;
    @Inject
    NetworkStateHolder holder;
    @Inject
    RxNetworkObserver rxNetworkObserver;
    @Inject
    Utility utility;
    @Inject
    MyPreferenceModel model;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    NetworkService service;
    @Inject
    MyPreferencePageContract.View view;
    private CompositeDisposable compositeDisposable;
    @Inject
    MyPreferencePagePresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }
    @Override
    public void takeView(Object view)
    {}
    @Override
    public void dropView()
    {
        view=null;
        compositeDisposable.clear();
    }

    @Override
    public void hidePreview() {
        if(view!=null)
            view.hidePreview();
    }

    @Override
    public void showError(String message)
    {
        if(view!=null)
            view.showError(message);
    }

    @Override
    public void showMessage(String message)
    {
        if(view!=null)
            view.showMessage(message);
    }

    @Override
    public void openNextFrag(int fromList,int next)
    {
        if(view!=null)
            view.openNextFrag(fromList,next);
    }

    @Override
    public void initNetworkObserver()
    {
        view.updateInterNetStatus(holder.isConnected());
        Observer<NetworkStateHolder> observer = new Observer<NetworkStateHolder>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {
                compositeDisposable.add(d);
            }
            @Override
            public void onNext(NetworkStateHolder value)
            {
                view.updateInterNetStatus(value.isConnected());
            }
            @Override
            public void onError(Throwable e)
            {
                e.printStackTrace();
            }
            @Override
            public void onComplete()
            {}
        };
        rxNetworkObserver.getObservable().subscribeOn(Schedulers.newThread());
        rxNetworkObserver.getObservable().observeOn(AndroidSchedulers.mainThread());
        rxNetworkObserver.getObservable().subscribe(observer);
    }

    @Override
    public void openHomePage() {
        if(view != null)
            view.openHomePage();
    }

    @Override
    public void getPreference()
    {

        if(!holder.isConnected())
        {
            if(view!=null)
            {
                view.onConnectionError();
                view.showError(activity.getString(R.string.no_internet_error));
            }
        }else
        {
            service.getPreferences(dataSource.getToken(),model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> value)
                        {
                            try
                            {
                                if(value.code() == 200)
                                {
                                    String data=value.body().string();
                                    PreferenceResponsePojo reault_data=utility.getGson().fromJson(data,PreferenceResponsePojo.class);
                                    if(view!=null)
                                        view.updatePreference(reault_data.getData().getmyPreferences());

                                } else if(value.code() == 401){
                                    AppController.getInstance().appLogout();
                                }
                                else
                                {
                                    if(view!=null)
                                    {
                                        view.onConnectionError();
                                        view.showError(model.getError(value));
                                    }
                                }
                            } catch (IOException e)
                            {
                                if(view!=null)
                                    view.showError(e.getMessage());
                            }
                        }
                        @Override
                        public void onError(Throwable e)
                        {
                            if(view!=null)
                                view.onConnectionError();
                        }
                        @Override
                        public void onComplete() {}
                    });
        }
    }

}
