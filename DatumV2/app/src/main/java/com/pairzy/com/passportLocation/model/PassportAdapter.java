package com.pairzy.com.passportLocation.model;

import android.app.Activity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
import java.util.ArrayList;
/**
 * <h>LocationAdapter class.</h>
 * <h2>
 *
 * </h2>
 * @author 3Embed.
 * @since 10/5/18.
 * @version 1.0.
 */
public class PassportAdapter extends RecyclerView.Adapter<PassportLocViewHolder>
{
    private TypeFaceManager typeFaceManager;
    private Activity mactivity;

    public interface OnItemClickListener
    {
        void onItemClose(int position);
        void onItemChecked(int position);
    }
    private OnItemClickListener listener;
    private ArrayList<PassportLocation> passportLocations;
    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public PassportAdapter(Activity activity, ArrayList<PassportLocation> passportLocList, TypeFaceManager typeFaceManager)
    {
        this.mactivity=activity;
        this.passportLocations = passportLocList;
        this.typeFaceManager = typeFaceManager;
    }

    @Override
    public PassportLocViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.passport_loc_item, parent, false);
        return new PassportLocViewHolder(v,listener,typeFaceManager);
    }

    @Override
    public void onBindViewHolder(PassportLocViewHolder holder, int position)
    {
        try
        {
            handelView(holder);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void handelView(PassportLocViewHolder holder)
    {
        int  position=holder.getAdapterPosition();
        PassportLocation passportLocation = passportLocations.get(position);
        holder.locationName.setText(passportLocation.getLocationName());
        holder.subLocation.setText(passportLocation.getSubLocationName());
        setSelected(passportLocation.isSelected(),holder.locationCheckBox);
        if(passportLocation.isSelected())
        {
            holder.btnLocationClose.setVisibility(View.GONE);
        } else
        {
            holder.btnLocationClose.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount()
    {
        return passportLocations.size();
    }

    public void setSelected(boolean isSelected, ImageView compatCheckBox)
    {
        if(isSelected)
        {
            compatCheckBox.setImageDrawable(ContextCompat.getDrawable(mactivity,R.drawable.selected_tick_circle));
        }else
        {
            compatCheckBox.setImageDrawable(ContextCompat.getDrawable(mactivity,R.drawable.selected_untick_circle));
        }
    }
}