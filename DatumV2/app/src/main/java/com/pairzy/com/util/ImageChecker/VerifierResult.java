package com.pairzy.com.util.ImageChecker;

/**
 * @since  4/17/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class VerifierResult
{
    private boolean isError=false;
    private String  message;
    private boolean isVerified;

    public String getMedia_path() {
        return media_path;
    }

    void setMedia_path(String media_path) {
        this.media_path = media_path;
    }

    private String  media_path;

    public boolean isVerified()
    {
        return isVerified;
    }

    void setVerified(boolean verified)
    {
        isVerified = verified;
    }

    boolean isError() {
        return isError;
    }

    void setError(boolean error) {
        isError = error;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
