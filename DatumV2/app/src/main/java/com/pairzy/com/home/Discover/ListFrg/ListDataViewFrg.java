package com.pairzy.com.home.Discover.ListFrg;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pairzy.com.R;
import com.pairzy.com.addCoin.AddCoinActivity;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.home.Discover.DiscoveryFragContract;
import com.pairzy.com.home.Discover.DiscoveryFragUtil;
import com.pairzy.com.home.Discover.GridFrg.GridDataViewContract;
import com.pairzy.com.home.Discover.Model.AdapterCallback;
import com.pairzy.com.home.Discover.Model.UserItemPojo;
import com.pairzy.com.home.HomeContract;
import com.pairzy.com.home.HomeModel.HomeAnimation;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.userProfile.UserProfilePage;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.CustomView.SquizeButton;
import com.pairzy.com.util.GridSpacingItemDecoration;
import com.pairzy.com.util.RecycleViewAnimation.SlideInOutRightAnimator;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * <h2>ListDataViewFrg</h2>
 * A simple {@link Fragment} subclass.
 * @author 3Embed.
 * @version 1.0.
 * @since 02-03-2018.
 */
@ActivityScoped
public class ListDataViewFrg extends DaggerFragment implements ListDataViewFrgContract.View
{
    @Inject
    Activity activity;
    @Inject
    NetworkStateHolder holder;

    @Inject
    ListDataAdapter listDataAdapter;

    @Inject
    DiscoveryFragContract.Presenter sub_main_presenter;
    @Inject
    ListDataViewFrgContract.Presenter presenter;
    @Inject
    GridDataViewContract.Presenter gridpresenter;
    @Inject
    GridSpacingItemDecoration gridSpacingItemDecoration;
    @Inject
    GridLayoutManager gridLayoutManager;
    @Inject
    @Named(DiscoveryFragUtil.USER_LIST)
    ArrayList<UserItemPojo> userList;

    @BindView(R.id.parent_layout)
    SwipeRefreshLayout parent_layout;
    @BindView(R.id.item_list)
    RecyclerView item_list;
    @BindView(R.id.boost_view)
    SquizeButton boost_view;
    @BindView(R.id.pb_boost_progress)
    ProgressBar pbBoostProgress;
    @BindView(R.id.rl_boost_view)
    RelativeLayout rlBoostView;
    @BindView(R.id.tv_boost_counter)
    TextView tvBoostCounter;

    @BindView(R.id.fl_coin_one)
    FrameLayout flCoinOne;
    @BindView(R.id.fl_coin_two)
    FrameLayout flCoinTwo;
    @BindView(R.id.fl_coin_three)
    FrameLayout flCoinThree;
    @BindView(R.id.tv_coin)
    TextView tvCoin;

    private Unbinder unbinder;
    private boolean fromList = false;
    @Inject
    HomeContract.Presenter homePresenter;
    @Inject
    HomeAnimation homeAnimation;

    public ListDataViewFrg() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_list_data_view_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder=ButterKnife.bind(this,view);
        presenter.takeView(this);
        presenter.initListener();
        initUI();
    }

    /*
     * Initialization of the xml */
    private void initUI()
    {
        boost_view.setClickListener(() ->{
            fromList = true;
            homePresenter.checkForProfileBoost();
        });
        parent_layout.setColorSchemeResources(R.color.refresh2, R.color.refresh1,R.color.refresh);
        parent_layout.setOnRefreshListener(() -> {
            parent_layout.setRefreshing(false);
            sub_main_presenter.updateCurrentLocation(true);
        });
        item_list.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {
                return false;
            }
        });
        item_list.setItemAnimator(new SlideInOutRightAnimator(item_list));
        item_list.setHasFixedSize(true);
        item_list.setLayoutManager(gridLayoutManager);
        item_list.addItemDecoration(gridSpacingItemDecoration);
        item_list.setAdapter(listDataAdapter);
        listDataAdapter.notifyDataSetChanged();
        item_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                super.onScrolled(recyclerView, dx, dy);
                int positionView = gridLayoutManager.findLastVisibleItemPosition();
                presenter.doLoadMore(positionView);
                sub_main_presenter.preFetchImage(positionView);
            }
        });
    }

    @Override
    public void onDestroy()
    {
        unbinder.unbind();
        super.onDestroy();
    }


    @Override
    public void onLike(int position)
    {
        if(holder.isConnected())
        {
            UserItemPojo item=listDataAdapter.removeItem(position);
            if(item!=null)
                sub_main_presenter.doLike(item);
            gridpresenter.updateDataChanged();
            presenter.isEmpty();

            if(userList.isEmpty())
                homePresenter.stopPlayer();
        }else
        {
            //Show toast
        }

    }

    @Override
    public void onDislike(int position)
    {
        if(holder.isConnected())
        {
            UserItemPojo item=listDataAdapter.removeItem(position);
            if(item!=null)
                sub_main_presenter.doDisLike(item);

            gridpresenter.updateDataChanged();
            presenter.isEmpty();

            if(userList.isEmpty())
                homePresenter.stopPlayer();
        }else
        {
            //Show toast
        }
    }

    @Override
    public void onSuperLike(int position)
    {
        if(holder.isConnected())
        {
            UserItemPojo item=listDataAdapter.removeItem(position);
            if(item!=null)
                sub_main_presenter.doSuperLike(item);
            gridpresenter.updateDataChanged();
            presenter.isEmpty();

            if(userList.isEmpty())
                homePresenter.stopPlayer();
        }else
        {
            //Show toast
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(!presenter.onHandelActivityResult(requestCode,resultCode,data))
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void openUserProfile(String data,View view)
    {
        Intent intent=new Intent(activity,UserProfilePage.class);
        Bundle intent_data=new Bundle();
        intent_data.putString(UserProfilePage.USER_DATA,data);
        intent.putExtras(intent_data);
        this.startActivityForResult(intent, AppConfig.PROFILE_REQUEST);
    }

    @Override
    public void setAdapterListen(AdapterCallback callback)
    {
        listDataAdapter.setEventBack(callback);
    }

    @Override
    public void openBoost()
    {
        //homePresenter.openBoostDialog();
        //homePresenter.checkForProfileBoost();
    }

    @Override
    public void updateBothViewData()
    {
        gridpresenter.updateDataChanged();
        listDataAdapter.notifyDataSetChanged();
    }

    @Override
    public void openChat(UserItemPojo userItemPojo) {
        sub_main_presenter.openChat(userItemPojo);
    }

    @Override
    public void setChatListNeedToUpdate(boolean yes) {
        homePresenter.setNeedToUpdateChat(yes);
    }

    @Override
    public void showBoostViewCounter(boolean show, int viewCount) {
        if(show) {
            tvBoostCounter.setText(viewCount + " views");
            if (pbBoostProgress.getVisibility() == View.GONE) {
                pbBoostProgress.setVisibility(View.VISIBLE);
                homeAnimation.startPopUpAnimation(rlBoostView);
            }
        }
        else{
            if (pbBoostProgress.getVisibility() == View.VISIBLE) {
                pbBoostProgress.setVisibility(View.GONE);
                homeAnimation.startPopDownAnimation(rlBoostView);
            }
        }
    }

    @Override
    public void startCoinAnimation() {
        if(fromList) {
            fromList = false;
            homeAnimation.startCoinAnimation(tvCoin, flCoinOne, flCoinTwo, flCoinThree);
        }
    }

    @Override
    public void launchCoinWallet() {
        Intent intent = new Intent(activity,AddCoinActivity.class);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }


    @Override
    public void showError(String error) {}

    @Override
    public void showMessage(String message)
    {}

    @Override
    public void showLoadingView()
    {
        sub_main_presenter.showSearchView();
    }


    @Override
    public void doLoadMore()
    {
        try
        {
            item_list.post(() -> listDataAdapter.notifyDataSetChanged());
        }catch (Exception e){}
        sub_main_presenter.getUsers();
    }

    @Override
    public void notifyDataChanged(boolean isScrooTop)
    {
        listDataAdapter.notifyDataSetChanged();
        if(isScrooTop)
        {
            try
            {
                item_list.smoothScrollToPosition(0);
            }catch (Exception e){}
        }
    }
}
