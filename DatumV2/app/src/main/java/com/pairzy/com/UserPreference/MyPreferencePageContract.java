package com.pairzy.com.UserPreference;
import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.UserPreference.Model.PrefranceDataDetails;

import java.util.ArrayList;
import dagger.android.support.DaggerFragment;
/**
 * <h2>MyPreferencePageContract</h2>
 * <P>
 *
 * </P>
 * @since  2/22/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface MyPreferencePageContract
{
    interface Presenter extends BasePresenter
    {
        void hidePreview();
        void showError(String message);
        void showMessage(String message);
        void openNextFrag(int from_list, int next);
        void getPreference();
        void initNetworkObserver();
        void openHomePage();
    }

    interface View extends BaseView
    {
        void hidePreview();
        void openNextFrag(int fromList, int next);
        void openHomePage();
        void updatePreference(ArrayList<PrefranceDataDetails> data);
        void onConnectionError();
        void openFragment(DaggerFragment fragment, boolean keepBack);
        void showError(String error);
        void showMessage(String message);
        void updateInterNetStatus(boolean status);
    }
}
