package com.pairzy.com.MatchedView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;

/**
 * <h2>DateAlertDialog</h2>
 * <P>
 *
 * </P>
 * @since  3/21/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class DateAlertDialog
{
    private DateAlertCallback callback;
    private Context context;
    private Dialog dialog;
    private TypeFaceManager typeFaceManager;

    public DateAlertDialog(Context context, TypeFaceManager typeFaceManager)
    {
     this.context=context;
     this.typeFaceManager = typeFaceManager;
    }
    /*
    * Showing the alert*/
    public void showAlert(Activity activity,Animation animation,DateAlertCallback back)
    {
        this.callback=back;
        if(dialog !=null)
        {
            if(dialog.isShowing())
            {
                dialog.dismiss();
            }
        }
        ColorDrawable cd=new ColorDrawable();
        cd.setColor(Color.TRANSPARENT);
        dialog = new Dialog(activity);
        Window window1=dialog.getWindow();
        assert window1 != null;
        window1.setBackgroundDrawable(cd);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = LayoutInflater.from(context);
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.date_alert_dialog_layout,null);
        TextView tvCallDate = dialogView.findViewById(R.id.tv_call_date);
        tvCallDate.setTypeface(typeFaceManager.getCircularAirBold());
        TextView tvPhysicalDate = dialogView.findViewById(R.id.tv_physical_date);
        tvPhysicalDate.setTypeface(typeFaceManager.getCircularAirBold());
        TextView tvVideoDate = dialogView.findViewById(R.id.tv_video_date);
        tvVideoDate.setTypeface(typeFaceManager.getCircularAirBold());
        LinearLayout center_item_holder=dialogView.findViewById(R.id.center_item_holder);
        if(animation!=null)
        {
            handleAnimation(center_item_holder,animation);
        }else
        {
            center_item_holder.setVisibility(View.VISIBLE);
            center_item_holder.setEnabled(true);
        }
        LinearLayout ll_call_date=dialogView.findViewById(R.id.ll_call_date);
        ll_call_date.setOnClickListener(view -> {
            dialog.cancel();
            //if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                if (callback != null)
                    callback.onCallDate();
            //}
        });
        LinearLayout ll_physical_date=dialogView.findViewById(R.id.ll_physical_date);
        ll_physical_date.setOnClickListener(view -> {
            dialog.cancel();
            if(callback!=null)
                callback.onPhysicalDate();
        });
        LinearLayout ll_video_date=dialogView.findViewById(R.id.ll_video_date);
        ll_video_date.setOnClickListener(view -> {
            dialog.cancel();
            //if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                if (callback != null)
                    callback.onVideoDate();
            //}
        });
        RelativeLayout rlParent = dialogView.findViewById(R.id.date_alert_view);
        rlParent.setOnClickListener(view -> {
            if(dialog != null)
                dialog.cancel();
        });
        dialog.setCancelable(true);
        dialog.setContentView(dialogView);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        dialog.show();
    }


    private void handleAnimation(View view,Animation animation)
    {
        view.setEnabled(false);
        view.setVisibility(View.VISIBLE);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation) {
                view.setEnabled(true);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        view.setAnimation(animation);
        animation.start();
    }
}
