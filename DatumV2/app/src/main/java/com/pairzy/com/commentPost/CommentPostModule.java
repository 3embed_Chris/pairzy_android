package com.pairzy.com.commentPost;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class CommentPostModule {

    @Binds
    @ActivityScoped
    abstract CommentPostContract.Presenter providePresenter (CommentPostPresenter commentPostPresenter);


    @Binds
    @ActivityScoped
    abstract CommentPostContract.View provideView(CommentPostActivity commentPostActivity);

    @Binds
    @ActivityScoped
    abstract Activity provideActivity(CommentPostActivity commentPostActivity);

}
