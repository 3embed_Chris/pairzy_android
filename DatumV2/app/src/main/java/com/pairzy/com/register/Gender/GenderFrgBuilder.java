package com.pairzy.com.register.Gender;

import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.register.RegisterPage;

import dagger.Binds;
import dagger.Module;

/**
 * <h2>GenderFrgBuilder</h2>
 * <P>
 *     Dagger module class for the Gender selection and it is
 *     part of the registration activity.
 *     {@link RegisterPage}
 * </P>
 * @since  2/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface GenderFrgBuilder
{
    @FragmentScoped
    @Binds
    GenderFragment getEmailFragment(GenderFragment genderFragment);

    @FragmentScoped
    @Binds
    GenderContract.Presenter taskPresenter(GenderPresenter presenter);
}
