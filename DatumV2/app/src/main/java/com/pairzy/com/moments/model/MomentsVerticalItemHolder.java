package com.pairzy.com.moments.model;

import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;
import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.util.TypeFaceManager;
import com.facebook.drawee.view.SimpleDraweeView;
import com.pairzy.com.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * <h2>MomentsVerticalItemHolder</h2>
 * <P> this is a MomentsVerticalItemHolder  class for momentsActivity</P>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */
public class MomentsVerticalItemHolder extends  RecyclerView.ViewHolder  implements View.OnClickListener {

    private static final String TAG = MomentsVerticalItemHolder.class.getSimpleName();

    private ItemActionCallBack callBack;

    @BindView(R.id.media_container)
    ConstraintLayout flMediaContainer;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.ivVolumeControl)
    ImageView ivVolumeControl;

    @BindView(R.id.ivPostlistProfile)
    SimpleDraweeView profilePic;

    @BindView(R.id.tvPostlistName)
    AppCompatTextView profileName;

    @BindView(R.id.ivPostlistPostMatch)
    SimpleDraweeView PostMatch;

    @BindView(R.id.ivPostlistPostLiked)
    SimpleDraweeView postLiked;

    @BindView(R.id.rvPostlistPost)
    RecyclerView rvPostlistPost;

    @BindView(R.id.ivPostlistPostLike)
    AppCompatImageView postLike;

    @BindView(R.id.tvPostlistLikedCount)
    AppCompatTextView tvPostLikeCount;

    @BindView(R.id.ivPostlistPostComment)
    AppCompatImageButton postComment;

    @BindView(R.id.ivPostlistPostShare)
    AppCompatImageButton postShare;

    @BindView(R.id.ivPostlistPostMessage)
    FloatingActionButton profileMessage;

    @BindView(R.id.tvPostlistPostTime)
    AppCompatTextView tvPostTime;

    @BindView(R.id.tvPostlistUBtext)
    AppCompatTextView tvUpdatedBio;

    @BindView(R.id.postParentView)
    ConstraintLayout layout;

    @BindView(R.id.tvPostlistViewAll)
    AppCompatTextView tvPostCommentViewAll;
    @BindView(R.id.sdv_post_review)
    SimpleDraweeView sdvPostPreview;
    @BindView(R.id.fl_delete_button)
    FrameLayout flDeleteButton;
    @BindView(R.id.tvPostlistBioUpdated)
    AppCompatTextView BioText;
    @BindView(R.id.ivPostlistBioUpdatedText)
    AppCompatTextView Bio;
    @BindView(R.id.ivBlurImage)
    ImageView bioBlurBackground;

    public RequestManager requestManager;
    private View parent;

    public MomentsVerticalItemHolder(@NonNull View itemView, TypeFaceManager typeFaceManager, ItemActionCallBack callBack) {
        super(itemView);
        this.parent = itemView;
        this.callBack=callBack;
        Log.e(TAG, "PostGridItemViewHolder: " );
        ButterKnife.bind(this,itemView);
        //TODO typeface
        rvPostlistPost.setHasFixedSize(true);
        layout.setOnClickListener(this);
        rvPostlistPost.setOnClickListener(this);
        postComment.setOnClickListener(this);
        profileMessage.setOnClickListener(this);
        tvPostCommentViewAll.setOnClickListener(this);
        postLike.setOnClickListener(this);
        tvPostLikeCount.setOnClickListener(this);
        flDeleteButton.setOnClickListener(this);
    }

    void onBind(MomentsData mediaObject, RequestManager requestManager) {
        this.requestManager = requestManager;
        parent.setTag(this);
    }


    @Override
    public void onClick(View v) {
        if (callBack != null) {
            Log.e(TAG, "onClick: "+v.getId()+"  "+this.getAdapterPosition());
            callBack.onClick(v.getId(), this.getAdapterPosition());
        }
    }

}
