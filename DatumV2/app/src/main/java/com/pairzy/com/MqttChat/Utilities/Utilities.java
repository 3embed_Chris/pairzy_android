package com.pairzy.com.MqttChat.Utilities;

/**
 * Created by moda on 04/05/17.
 */


import android.os.Build;

import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.pairzy.com.AppController;


public class Utilities {


    public static String tsInGmt() {

        Date localTime = new Date(System.currentTimeMillis() - AppController.getInstance().getTimeDelta());

        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);
        formater.setTimeZone(TimeZone.getTimeZone("GMT"));


        String s = formater.format(localTime);


        return s;
    }

    //converting time to localtime zone from gmt time

    public static String tsFromGmt(String tsingmt) {

        Date d = null;
        String s = null;

        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);

        try {
            d = formater.parse(tsingmt);
            TimeZone tz = TimeZone.getDefault();
            formater.setTimeZone(tz);
            s = formater.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return s;
    }


    public String gmtToEpoch(String tsingmt) {


        Date d = null;

        long epoch = 0;


        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);

        try {
            d = formater.parse(tsingmt);

            epoch = d.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return String.valueOf(epoch);
    }


    public static long Daybetween(String date1, String date2, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.US);

        sdf.setTimeZone(TimeZone.getDefault());


        Date startDate = null, endDate = null;
        try {
            startDate = sdf.parse(date1);
            endDate = sdf.parse(date2);
        } catch (Exception e) {
            e.printStackTrace();


            sdf = new SimpleDateFormat(pattern.substring(0, 19), Locale.US);
            try {
                startDate = sdf.parse(date1);
                endDate = sdf.parse(date2);

            } catch (ParseException ef) {
                ef.printStackTrace();
            }
        }


        Calendar sDate = getDatePart(startDate);
        Calendar eDate = getDatePart(endDate);

        long daysBetween = 0;
        while (sDate.before(eDate)) {
            sDate.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;


    }

    public static Calendar getDatePart(Date date) {
        Calendar cal = Calendar.getInstance();       // get calendar instance
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
        cal.set(Calendar.MINUTE, 0);                 // set minute in hour
        cal.set(Calendar.SECOND, 0);                 // set second in minute
        cal.set(Calendar.MILLISECOND, 0);            // set millisecond in second

        return cal;                                  // return the date part
    }


    public static String formatDate(String ts) {

        String s = null;
        Date d = null;


        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);

        try {
            d = formater.parse(ts);

            formater = new SimpleDateFormat("HH:mm:ss EEE dd/MMM/yyyy z", Locale.US);
            s = formater.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return s;
    }

    public static String formatDateChatList(String ts) {

        String s = null;
        Date d = null;


        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);

        try {
            d = formater.parse(ts);

            formater = new SimpleDateFormat("HH:mm", Locale.US);
            s = formater.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return s;
    }

    public static String epochtoGmt(String tsingmt) {


        Date d = null;
        String s = null;
        long epoch = 0;


        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);
        formater.setTimeZone(TimeZone.getTimeZone("GMT"));
        epoch = Long.parseLong(tsingmt);

        d = new Date(epoch);
        s = formater.format(d);


        return s;
    }

    public static String changeStatusDateFromGMTToLocal(String ts) {


        String s = null;
        Date d;


        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);


        try {
            d = formater.parse(ts);

            TimeZone tz = TimeZone.getDefault();
            formater.setTimeZone(tz);

            s = formater.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return s;
    }


    /**
     * To get the epoch time of the current time in gmt
     */

    public static long getGmtEpoch() {

        Date localTime = new Date(System.currentTimeMillis() - AppController.getInstance().getTimeDelta());

        Date d;
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);
        formater.setTimeZone(TimeZone.getTimeZone("GMT"));

        String tsingmt = formater.format(localTime);

        long epoch = 0;


        try {
            d = formater.parse(tsingmt);

            epoch = d.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return epoch;


    }


    /*
     * Get calls time based on the timezone
     */
    public static String tsFromGmtToLocalTimeZone(String tsingmt) {


        String s = null;
        Date d = null;


        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmssSSS z", Locale.US);

        try {
            d = formater.parse(tsingmt);

            formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z", Locale.US);
            formater.setTimeZone(TimeZone.getDefault());

            s = formater.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return s;

    }

    /*
      * to convert string from the 24 hour format to 12 hour format
      */
    public static String convert24to12hourformat(String d) {

        String datein12hour = null;

        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm", Locale.US);
            final Date dateObj = sdf.parse(d);


            datein12hour = new SimpleDateFormat("h:mm a", Locale.US).format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }


        return datein12hour;

    }




    public static String findOverlayDate(String date) {
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("EEE dd/MMM/yyyy", Locale.US);


            String m1 = "", m2 = "";


            String month1, month2;

            String d1, d2;


            d1 = sdf.format(new Date(System.currentTimeMillis() - AppController.getInstance().getTimeDelta()));

            d2 = date;


            month1 = d1.substring(7, 10);


            month2 = d2.substring(7, 10);

            if (month1.equals("Jan")) {
                m1 = "01";
            } else if (month1.equals("Feb")) {
                m1 = "02";
            } else if (month2.equals("Mar")) {
                m2 = "03";
            } else if (month1.equals("Apr")) {
                m1 = "04";
            } else if (month1.equals("May")) {
                m1 = "05";
            } else if (month1.equals("Jun")) {
                m1 = "06";
            } else if (month1.equals("Jul")) {
                m1 = "07";
            } else if (month1.equals("Aug")) {
                m1 = "08";
            } else if (month1.equals("Sep")) {
                m1 = "09";
            } else if (month1.equals("Oct")) {
                m1 = "10";
            } else if (month1.equals("Nov")) {
                m1 = "11";
            } else if (month1.equals("Dec")) {
                m1 = "12";
            }


            if (month2.equals("Jan")) {
                m2 = "01";
            } else if (month2.equals("Feb")) {
                m2 = "02";
            } else if (month1.equals("Mar")) {
                m1 = "03";
            } else if (month2.equals("Apr")) {
                m2 = "04";
            } else if (month2.equals("May")) {
                m2 = "05";
            } else if (month2.equals("Jun")) {
                m2 = "06";
            } else if (month2.equals("Jul")) {
                m2 = "07";
            } else if (month2.equals("Aug")) {
                m2 = "08";
            } else if (month2.equals("Sep")) {
                m2 = "09";
            } else if (month2.equals("Oct")) {
                m2 = "10";
            } else if (month2.equals("Nov")) {
                m2 = "11";
            } else if (month2.equals("Dec")) {
                m2 = "12";
            }
            month1 = null;
            month2 = null;


            if (sdf.format(new Date(System.currentTimeMillis() - AppController.getInstance().getTimeDelta())).equals(date)) {


                m2 = null;
                m1 = null;
                d2 = null;
                d1 = null;
                sdf = null;
                return "Today";
            } else if ((Integer.parseInt(d1.substring(11) + m1 + d1.substring(4, 6)) - Integer.parseInt(d2.substring(11) + m2 + d2.substring(4, 6))) == 1) {

                m2 = null;
                m1 = null;
                d2 = null;
                d1 = null;
                sdf = null;
                return "Yesterday";

            } else {

                m2 = null;
                m1 = null;
                d2 = null;
                d1 = null;
                sdf = null;
                return date;
            }

        } catch (Exception e) {
            e.printStackTrace();

            return date;
        }
    }


    /**
     * To remove an element from the jsonarray even below api level 19
     */
    public static JSONArray removeElementFromJSONArray(JSONArray jarray, int pos) {


        if (Build.VERSION.SDK_INT >= 19) {


            jarray.remove(pos);
            return jarray;
        }

        JSONArray Njarray = new JSONArray();
        try {
            for (int i = 0; i < jarray.length(); i++) {
                if (i != pos)
                    Njarray.put(jarray.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Njarray;
    }

}