package com.pairzy.com.MyProfile.Model;

public interface InstaMediaClickCallback {
    void onInstaPhotoClick(int page, int position);
}
