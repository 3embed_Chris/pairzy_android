package com.pairzy.com.passportLocation;

import android.content.Intent;
import com.pairzy.com.passportLocation.model.PassportAdapter;
/**
 * <h>PassportContract interface</h>
 * @author 3Embed.
 * @since 22/05/18.
 * @version 1.0.
 */

public interface PassportContract {

    interface View{


        void applyFont();
        void initView();
        void showError(String msg);
        void showError(int msgId);
        void showMessage(String msg);
        void showCurrentUserLocation(String currentLocation);
        void showCurrentLocationLoading(boolean show);
        void finishActivity();
        void setAdapterListener(PassportAdapter.OnItemClickListener listener);
        void selectCurrent(boolean isSelected);
        void onListExist(boolean isExist);
    }

    interface Presenter
    {
        void init();
        void askLocationPermission();
        void dispose();
        void loadSavedLocations();
        void saveLocations(boolean isCurrent);
        void onCurretnSelected();
        void setLocationListener();
        void handleOnActivityResult(int requestCode, int resultCode, Intent data);
        void checkForCurrentLocation();
    }
}