package com.pairzy.com.home;

import android.content.Intent;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.home.Discover.UserActionEventError;

import dagger.android.support.DaggerFragment;
/**
 * <h2>HomeContract</h2>
 * @since  2/27/2018.
 * @author 3embed.
 * @version 1.o.
 */
public interface HomeContract
{
    interface View extends BaseView
    {
        void moveFragment(FragmentTransaction ft, DaggerFragment fragment);
        void showMessage(String message);
        void showError(String error);
        void verify_FragmentLoad(Fragment fragment);
        void updateInterNetStatus(boolean status);
        void openProfile(Intent intent);
        void launchCampaignScreen(Intent intent1);
        void showCoinBalance(String coinBalance);
        void launchCoinWallet();
        void showBoostViewCounter(boolean show);
        void startCoinAnimation();
        void openProspectScreen();
        void showUnreadChatCount(String unreadChatCount, boolean empty);
        void selectTab(int pos);
        void stopPlayer();
        void loadInterstialAds();
        void showLoadedProfileAds();
        void showUpdateDialog(boolean mandatory);

        void dismissUpdateDialog();
        void pausePlayer();
        void updateDiscoveryProfilePicture();
        void showPendingDateCount(String pendingDateCount, boolean empty);

        void launchAppSettingsPage();
    }

    interface Presenter extends BasePresenter
    {
        void initNetworkObserver();
        void openFragmentPage(FragmentTransaction ft, DaggerFragment fragment);
        void showMessage(String message);
        void showError(String error);
        void doLiked(boolean fromUserProfile, String user_id);
        void doDislike(String user_id);
        void doSuperLike(String user_id);
        boolean onHandelActivityResult(int requestCode, int resultCode, Intent data);
        void openBoostDialog();
        void checkForDynamicLink(Intent intent);
        void subscribeToFirebaseTopic();
        void parseIntent(Intent intent);
        void setRevertCallback(UserActionEventError callback);
        void observeCoinBalanceChange();
        void loadCoinBalance();
        void updateCoinBalance();
        void launchCoinWallet();
        void setNeedToUpdateChat(boolean yes);
        boolean isNeedToUpdateChatList();
        void checkForProfileBoost();
        void checkOnlyForProfileBoost();
        void openProspectScreen();
        void updateUnreadChatBadgeCount(String unreadChatCount);
        void observeAdminCoinAdd();
        void stopPlayer();
        void handleAdShow();
        void showLoadedProfileAds();

        void checkForForceUpdate();
        void updateDiscoveryProfilePicture();
        void updatePendingDateBadgeCount(String unreadPendingDateCount);
        boolean isFeedtabNeedToOpen();
        void setFeedTabOpen(boolean feedTabOpen);

        boolean checkToUpcomingFrom();

        void openAppSettingPage();
    }
}
