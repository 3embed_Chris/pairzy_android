package com.pairzy.com.home.HomeModel;

/**
 * @since  4/12/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class LoadMoreStatus
{
    public boolean isNo_more_data()
    {
        return this.no_more_data;
    }

    public void setNo_more_data(boolean no_more_data)
    {
        this.no_more_data = no_more_data;
    }

    private boolean no_more_data;
}
