package com.pairzy.com.dagger;

import com.pairzy.com.ImageCropper.CropImageActivity;
import com.pairzy.com.LeaderBoad.LeaderBoadActivity;
import com.pairzy.com.LeaderBoad.LeaderBoadModule;
import com.pairzy.com.LeaderBoad.LeaderBoadUtil;
import com.pairzy.com.MqttChat.ForwardMessage.ActivityForwardMessage;
import com.pairzy.com.MqttChat.Giphy.SelectGIF;
import com.pairzy.com.MqttChat.Wallpapers.Activities.DrawActivity;
import com.pairzy.com.MqttChat.Wallpapers.Activities.SolidColorActivity;
import com.pairzy.com.MyProfile.MyProfileBuilder;
import com.pairzy.com.MyProfile.MyProfilePage;
import com.pairzy.com.MyProfile.ProfileUtil;
import com.pairzy.com.MyProfile.editAge.EditDobActivity;
import com.pairzy.com.MyProfile.editAge.EditDobModule;
import com.pairzy.com.MyProfile.editEmail.EditEmailActivity;
import com.pairzy.com.MyProfile.editEmail.EditEmailModule;
import com.pairzy.com.MyProfile.editEmail.EditEmailUtilModule;
import com.pairzy.com.MyProfile.editGender.EditGenderActivity;
import com.pairzy.com.MyProfile.editGender.EditGenderModule;
import com.pairzy.com.MyProfile.editGender.EditGenderUtilModule;
import com.pairzy.com.MyProfile.editName.EditNameActivity;
import com.pairzy.com.MyProfile.editName.EditNameModule;
import com.pairzy.com.MyProfile.editPreference.EditPrefActivity;
import com.pairzy.com.MyProfile.editPreference.EditPrefModule;
import com.pairzy.com.MyProfile.editPreference.EditPrefUtilModule;
import com.pairzy.com.MySearchPreference.MySearchPref;
import com.pairzy.com.MySearchPreference.MySearchPrefBuilder;
import com.pairzy.com.MySearchPreference.MySearchPrefUtil;
import com.pairzy.com.PostMoments.PostActivity;
import com.pairzy.com.PostMoments.PostModule;
import com.pairzy.com.PostMoments.PostUtilModule;
import com.pairzy.com.UserPreference.MyPreferencePage;
import com.pairzy.com.UserPreference.MyPreferencePageBuilder;
import com.pairzy.com.UserPreference.UserPrefUtil;
import com.pairzy.com.addCoin.AddCoinActivity;
import com.pairzy.com.addCoin.AddCoinModule;
import com.pairzy.com.addCoin.AddCoinUtil;
import com.pairzy.com.appSettings.AppSettingsActivity;
import com.pairzy.com.appSettings.AppSettingsModule;
import com.pairzy.com.appSettings.AppSettingsUtil;
import com.pairzy.com.boostDetail.BoostDetailActivity;
import com.pairzy.com.boostDetail.BoostDetailModule;
import com.pairzy.com.boostDetail.BoostUtilModule;
import com.pairzy.com.boostLikes.BoostLikeActivity;
import com.pairzy.com.boostLikes.BoostLikeModule;
import com.pairzy.com.boostLikes.BoostLikeUtil;
import com.pairzy.com.campaignScreen.CampaignActivity;
import com.pairzy.com.campaignScreen.CampaignModule;
import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.pairzy.com.chatMessageScreen.ChatMessageModule;
import com.pairzy.com.chatMessageScreen.ChatMessageUtil;
import com.pairzy.com.coinWallet.CoinWalletActivity;
import com.pairzy.com.coinWallet.CoinWalletModule;
import com.pairzy.com.coinWallet.CoinWalletUtil;
import com.pairzy.com.commentPost.CommentPostActivity;
import com.pairzy.com.commentPost.CommentPostModule;
import com.pairzy.com.commentPost.CommentPostUtil;
import com.pairzy.com.createPost.CreatePostActivity;
import com.pairzy.com.createPost.CreatePostModule;
import com.pairzy.com.createPost.CreatePostUtil;
import com.pairzy.com.editProfile.EditProfileActivity;
import com.pairzy.com.editProfile.EditProfileModule;
import com.pairzy.com.editProfile.EditProfileUtilModule;
import com.pairzy.com.fbRegister.FbRegisterActivityBuilder;
import com.pairzy.com.fbRegister.FbRegisterPage;
import com.pairzy.com.fbRegister.FbRegisterUtil;
import com.pairzy.com.googleLocationSearch.GoogleLocSearchActivity;
import com.pairzy.com.googleLocationSearch.GoogleLocSearchModule;
import com.pairzy.com.googleLocationSearch.GoogleLocSearchUtilModule;
import com.pairzy.com.home.AppSetting.AppSettingFragUtil;
import com.pairzy.com.home.Dates.DatesFragUtil;
import com.pairzy.com.home.Discover.DiscoveryFragUtil;
import com.pairzy.com.home.HomeActivity;
import com.pairzy.com.home.HomeActivityBuilder;
import com.pairzy.com.home.HomeUtil;
import com.pairzy.com.home.MakeMatch.MakeMatchUtil;
import com.pairzy.com.home.Matches.MatchesUtilModule;
import com.pairzy.com.home.Prospects.ProspectsFragUtil;
import com.pairzy.com.likes.LikesByActivity;
import com.pairzy.com.likes.LikesByModule;
import com.pairzy.com.likes.LikesByUtil;
import com.pairzy.com.locationScreen.LocSearchModule;
import com.pairzy.com.locationScreen.LocSearchUtilModule;
import com.pairzy.com.locationScreen.LocationSearchActivity;
import com.pairzy.com.locationScreen.locationMap.CustomLocActivity;
import com.pairzy.com.locationScreen.locationMap.CustomLocModule;
import com.pairzy.com.locationScreen.locationMap.CustomLocUtilModule;
import com.pairzy.com.login.LoginActivity;
import com.pairzy.com.login.LoginDaggerModule;
import com.pairzy.com.login.LoginUtil;
import com.pairzy.com.messageInfo.MessageInfoActivity;
import com.pairzy.com.messageInfo.MessageInfoModule;
import com.pairzy.com.messageInfo.MessageinfoUtil;
import com.pairzy.com.mobileverify.MobileVerifyActivity;
import com.pairzy.com.mobileverify.MobileVerifyActivityBuilder;
import com.pairzy.com.mobileverify.MobileVerifyUtil;
import com.pairzy.com.momentGrid.MomentsGridActivity;
import com.pairzy.com.momentGrid.MomentsGridModule;
import com.pairzy.com.momentGrid.MomentsGridUtils;
import com.pairzy.com.moments.MomentUtilModule;
import com.pairzy.com.moments.MomentsActivity;
import com.pairzy.com.moments.MomentsModule;
import com.pairzy.com.passportLocation.PassportActivity;
import com.pairzy.com.passportLocation.PassportModule;
import com.pairzy.com.passportLocation.PassportUtilModule;
import com.pairzy.com.photoVidPreview.PhotoVidActivity;
import com.pairzy.com.photoVidPreview.PhotoVidModule;
import com.pairzy.com.photoVidPreview.PhotoVidUtilModule;
import com.pairzy.com.planCallDate.CallDateActivity;
import com.pairzy.com.planCallDate.CallDateModule;
import com.pairzy.com.planCallDate.CallDateUtilModule;
import com.pairzy.com.planDate.DateActivity;
import com.pairzy.com.planDate.DateModule;
import com.pairzy.com.planDate.DateUtilModule;
import com.pairzy.com.register.RegisterActivityBuilder;
import com.pairzy.com.register.RegisterPage;
import com.pairzy.com.register.RegisterUtil;
import com.pairzy.com.selectLanguage.SelectLanguageActivity;
import com.pairzy.com.selectLanguage.SelectLanguageModule;
import com.pairzy.com.selectLanguage.SelectLanguageUtil;
import com.pairzy.com.settings.SettingsActivity;
import com.pairzy.com.settings.SettingsModule;
import com.pairzy.com.settings.SettingsUtilModule;
import com.pairzy.com.splash.SplashActivity;
import com.pairzy.com.splash.SplashDaggerModule;
import com.pairzy.com.splash.SplashUtil;
import com.pairzy.com.userProfile.Profile_util;
import com.pairzy.com.userProfile.UserProfileBuilder;
import com.pairzy.com.userProfile.UserProfilePage;
import com.pairzy.com.webPage.WebActivity;
import com.pairzy.com.webPage.WebModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
/**
 * We want Dagger.Android to create a Subcomponent which has a parent Component of whichever module ActivityBindingModule is on,
 * in our case that will be AppComponent.
 * The beautiful part about this setup is that you never need to tell AppComponent that it is going to have all these subcomponents
 * nor do you need to tell these subcomponents that AppComponent exists.
 * We are also telling Dagger.Android that this generated SubComponent needs to include the specified modules and be aware of a scope annotation @ActivityScoped
 * When Dagger.Android annotation processor runs it will create 4 subcomponents for us.
 */

@Module
public abstract class ActivityBindingModule
{
//    @ActivityScoped
//    @ContributesAndroidInjector(modules = {MapDirectionModule.class, MapDirectionUtil.class})
//    abstract MapDirectionActivity mapDirectionActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = { PhotoVidModule.class, PhotoVidUtilModule.class } )
    abstract PhotoVidActivity photoVidActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SelectLanguageUtil.class, SelectLanguageModule.class})
    abstract SelectLanguageActivity selectLanguageActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SplashDaggerModule.class, SplashUtil.class})
    abstract SplashActivity splashActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={LoginDaggerModule.class, LoginUtil.class})
    abstract LoginActivity loginActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {MobileVerifyActivityBuilder.class,MobileVerifyUtil.class})
    abstract MobileVerifyActivity mobileVerifyActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={RegisterActivityBuilder.class, RegisterUtil.class})
    abstract RegisterPage getRegisterActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={FbRegisterActivityBuilder.class, FbRegisterUtil.class})
    abstract FbRegisterPage getFbRegisterActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={MyPreferencePageBuilder.class,UserPrefUtil.class})
    abstract MyPreferencePage myPreferencePage();


    @ActivityScoped
    @ContributesAndroidInjector(modules ={HomeActivityBuilder.class,HomeUtil.class
            , DiscoveryFragUtil.class,ProspectsFragUtil.class,DatesFragUtil.class, MatchesUtilModule.class, MakeMatchUtil.class,AppSettingFragUtil.class})
    abstract HomeActivity homeActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={MySearchPrefBuilder.class,MySearchPrefUtil.class})
    abstract MySearchPref homeMySearchPref();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={UserProfileBuilder.class,Profile_util.class})
    abstract UserProfilePage homeUserProfileAct();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={MyProfileBuilder.class,ProfileUtil.class})
    abstract MyProfilePage MyProfilePageAct();

    @ActivityScoped
    @ContributesAndroidInjector
    abstract CropImageActivity getCropImageActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {SettingsModule.class, SettingsUtilModule.class})
    abstract SettingsActivity settingsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditProfileModule.class, EditProfileUtilModule.class})
    abstract EditProfileActivity editProfileActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditPrefModule.class, EditPrefUtilModule.class})
    abstract EditPrefActivity editPrefActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditGenderModule.class,EditGenderUtilModule.class})
    abstract EditGenderActivity editGenderActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditDobModule.class})
    abstract EditDobActivity editDobActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditEmailModule.class, EditEmailUtilModule.class})
    abstract EditEmailActivity editEmailActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {EditNameModule.class})
    abstract EditNameActivity editNameActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {DateModule.class, DateUtilModule.class})
    abstract DateActivity dateActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {CallDateModule.class, CallDateUtilModule.class})
    abstract CallDateActivity callDateActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {LocSearchModule.class, LocSearchUtilModule.class})
    abstract LocationSearchActivity location_search_activity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {CustomLocModule.class, CustomLocUtilModule.class})
    abstract CustomLocActivity customLocActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = WebModule.class)
    abstract WebActivity webActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {PassportModule.class, PassportUtilModule.class})
    abstract PassportActivity passportActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {GoogleLocSearchModule.class, GoogleLocSearchUtilModule.class})
    abstract GoogleLocSearchActivity googleLocSearchActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {BoostDetailModule.class, BoostUtilModule.class})
    abstract BoostDetailActivity boostDetailActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {AddCoinModule.class, AddCoinUtil.class})
    abstract AddCoinActivity addCoinActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {CoinWalletModule.class, CoinWalletUtil.class})
    abstract CoinWalletActivity coinWalletActivity();

    @ActivityScoped
    @ContributesAndroidInjector()
    abstract ActivityForwardMessage activityForwardMessage();

    @ActivityScoped
    @ContributesAndroidInjector()
    abstract SelectGIF selectGIF();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {ChatMessageModule.class, ChatMessageUtil.class})
    abstract ChatMessageActivity chatMessageScreen();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {BoostLikeModule.class, BoostLikeUtil.class})
    abstract BoostLikeActivity boostLikeActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {CampaignModule.class})
    abstract CampaignActivity campaignActivity();

    @ActivityScoped
    @ContributesAndroidInjector()
    abstract DrawActivity drawActivity();

    @ActivityScoped
    @ContributesAndroidInjector()
    abstract SolidColorActivity solidColorActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {MessageInfoModule.class, MessageinfoUtil.class})
    abstract MessageInfoActivity messageInfoActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {CreatePostModule.class, CreatePostUtil.class})
    abstract CreatePostActivity createPostActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules ={CommentPostModule.class, CommentPostUtil.class})
    abstract CommentPostActivity commentPostActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {PostModule.class, PostUtilModule.class})
    abstract PostActivity postActivity();


    @ActivityScoped
    @ContributesAndroidInjector(modules = {MomentsModule.class, MomentUtilModule.class})
    abstract MomentsActivity momentsActivity();


    @ActivityScoped
    @ContributesAndroidInjector(modules = {MomentsGridUtils.class, MomentsGridModule.class})
    abstract MomentsGridActivity momentsVerticalActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {LikesByModule.class, LikesByUtil.class})
    abstract LikesByActivity likesByActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {AppSettingsModule.class, AppSettingsUtil.class,AppSettingFragUtil.class})
    abstract AppSettingsActivity appSettingsActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = { LeaderBoadModule.class,  LeaderBoadUtil.class})
    abstract LeaderBoadActivity leaderBoadActivity();

}
