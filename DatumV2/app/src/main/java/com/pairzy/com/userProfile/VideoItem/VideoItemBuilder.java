package com.pairzy.com.userProfile.VideoItem;

import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * <h2>VideoItemBuilder</h2>
 * <P>
 *
 * </P>
 * @since 4/4/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface VideoItemBuilder
{
    @FragmentScoped
    @Binds
    VideoItemFrg getProspectItemFragment(VideoItemFrg likesMeFrg);

    @FragmentScoped
    @Binds
    VideoItemContract.Presenter taskPresenter(VideoItemPresenter presenter);
}
