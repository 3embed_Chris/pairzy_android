package com.pairzy.com.appSettings;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.home.AppSetting.AppSettingFrag;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public interface AppSettingsModule {

    @ActivityScoped
    @Binds
    AppSettingsContract.View bindsView(AppSettingsActivity appSettingsActivity);

    @ActivityScoped
    @Binds
    AppSettingsContract.Presenter bindsPresenter(AppSettingsPresenter presenter);

    @ActivityScoped
    @Binds
    Activity bindsActivity(AppSettingsActivity appSettingsActivity);

    @FragmentScoped
    @ContributesAndroidInjector
    AppSettingFrag appSettingsFrag();

}
