package com.pairzy.com.passportLocation;

import android.app.Activity;
import android.location.Geocoder;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.passportLocation.model.PassportAdapter;
import com.pairzy.com.passportLocation.model.PassportLocation;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.FeatureLocAlert.FeatureLocDialog;
import com.pairzy.com.util.LocationProvider.Location_service;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;
import java.util.Locale;

import dagger.Module;
import dagger.Provides;

/**
 * <h>PassportUtilModule class</h>
 * @author 3Embed.
 * @since 22/05/18.
 * @version 1.0.
 */

@Module
public class PassportUtilModule {

    @ActivityScoped
    @Provides
    App_permission provideAppPermission(Activity activity, TypeFaceManager typeFaceManager){
        return new App_permission(activity,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    ArrayList<PassportLocation> providePassportLoctionList(){
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    PassportAdapter providePassportAdapter(Activity activity,ArrayList<PassportLocation> passportLocations,TypeFaceManager typeFaceManager){
        return new PassportAdapter(activity,passportLocations,typeFaceManager);
    }

    @ActivityScoped
    @Provides
    LinearLayoutManager provideLinearLayoutManager(Activity activity){
        return new LinearLayoutManager(activity);
    }

    @ActivityScoped
    @Provides
    Location_service getLocation_service(Activity activity)
    {
        return new Location_service(activity);
    }

    @ActivityScoped
    @Provides
    Geocoder provideGeocoder(Activity activity)
    {
        return new Geocoder(activity,Locale.getDefault());
    }

    @ActivityScoped
    @Provides
    FeatureLocDialog provideFeatureLocAlert(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new FeatureLocDialog(activity,typeFaceManager);
    }

}
