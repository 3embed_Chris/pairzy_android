package com.pairzy.com.Service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.policy.TimeWindow;
import com.pairzy.com.AppController;
import com.pairzy.com.Database.PostDb;
import com.pairzy.com.PostMoments.model.Post;
import com.pairzy.com.PostMoments.model.PostData;
import com.pairzy.com.R;
import com.pairzy.com.Utilities.Constants;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.util.ApiConfig;
import com.google.gson.Gson;
import com.squareup.otto.Bus;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;


/**
 * Created by moda on 18/12/18.
 */

public class PublishPost {
    String TAG=PublishPost.class.getSimpleName();
    private NotificationManager notificationManager;
    private NotificationCompat.Builder builder2;

    private NetworkService service;
    private PostDb postDb;
    private String appName;
    private PostObserver postObserver;
    private static final String CHANNEL_ONE_ID = "datum_notification_channel";
    private static final String CHANNEL_ONE_NAME = "datum_notification";



    String type,path,caption;
    private Map<String, Object> postDetails;

    private List<PostData> posts;

    private static Bus bus = AppController.getBus();

    private String folderPath;

    @SuppressWarnings("unchecked")
    public void retryPublishingPosts(NetworkService service) {
        Log.e(TAG, "retryPublishingPosts: " );
        this.service = service;

        appName = "Demo";//AppController.getInstance().getResources().getString(R.string.app_name);

        final File imageFolder;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            imageFolder = new File(Environment.getExternalStorageDirectory() + "/" + AppController.getInstance().getResources().getString(R.string.app_name) + "/Media/");

        } else {

            imageFolder = new File(AppController.getInstance().getFilesDir() + "/" + AppController.getInstance().getResources().getString(R.string.app_name) + "/Media/");
        }
        if (!imageFolder.exists() && !imageFolder.isDirectory())
            imageFolder.mkdirs();


        folderPath = imageFolder.getAbsolutePath();


        postDb = new PostDb(AppController.getInstance());
        postObserver = new PostObserver();
//        uploadObserver = new UploadObserver();

//        loadFFMpegBinary();
        new PostAsync().execute();
    }

    @SuppressWarnings("TryWithIdenticalCatches")
    private class PostAsync extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] params) {
            Log.e(TAG, "doInBackground: " );
            try {
                postIt();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private void postIt() {
        Log.e(TAG, "postIt: " );
        posts = postDb.getAllData();

        int size = posts.size();
        postDetails = new HashMap<>();

        if (size > 0) {

            for (int i = 0; i < size; i++) {
                final int j = i;

                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        publishPost(posts.get(j));

                    }
                });
                thread.start();
            }
        }
    }


    private void normalPost(Post post) {
        Log.e(TAG, "normalPost: " );
        try {
            String requestId = MediaManager.get().upload(post.getPathForCloudinary())
                    .option("folder",  "posts")
                    .option(Constants.Post.RESOURCE_TYPE, post.getTypeForCloudinary())
                    .callback(new UploadCallback() {

                        @Override
                        public void onStart(String requestId) {
                            Log.e(TAG, "onStart: " );
                        }

                        @Override
                        public void onProgress(String requestId, long bytes, long totalBytes) {
                            Double progress = (double) bytes / totalBytes;
                            Log.i("Cloudinary", "onProgress: " + progress);
                        }

                        @Override
                        public void onSuccess(String requestId, Map resultData) {
                            Log.e(TAG, "onSuccess: " );
                            mediaPost(requestId, post, resultData);

                        }

                        @Override
                        public void onError(String requestId, ErrorInfo error) {
                            Log.e(TAG, "onError: ");
                            if (notificationManager != null) {

                                Map<String, Object> postDetails = fetchPostDetailsFromRequestId(requestId);
                                if (postDetails != null) {

                                    RemoteViews contentView = (RemoteViews) postDetails.get("contentView");
                                    contentView.setTextViewText(R.id.title, "Failed to post..");
                                    contentView.setProgressBar(R.id.progress, 100, 100, false);

                                    String postId = ((Post) postDetails.get("post")).getId();
                                    notificationManager.notify(Integer.parseInt(postId), builder2.setContent(contentView).build());
                                }

                                boolean deleted = postDb.delete(post.getId());
                                Log.i("DELETED", "" + deleted);
                                try {
                                    File fdelete = new File(post.getPathForCloudinary());
                                    if (fdelete.exists()) {
                                        fdelete.delete();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onReschedule(String requestId, ErrorInfo error) {

                        }

                    }).constrain(TimeWindow.immediate()).dispatch();


            addRequestIdForPost(post.getId(), requestId);
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }


    }


    private void mediaPost(String requestId, Post post, Map resultData) {
        String type = resultData.get("resource_type").equals("video") ? "1" : "0";
        String url = String.valueOf(resultData.get("url"));
        String public_id=String.valueOf(resultData.get("public_id"));

        Map<String, Object> postDetails = fetchPostDetailsFromRequestId(requestId);

        Log.e(TAG, "mediaPost: ");
        if (postDetails != null) {

            Map<String, Object> map = new HashMap<>();

           /* map.put("thumbnailUrl1", url.replace("mp4", "jpg"));//url.replace("upload/", "upload/t_media_lib_thumb/"));
            map.put("imageUrl1", url);
            map.put("cloudinary_public_id", public_id);*/

            map.put(ApiConfig.POST.TYPE_FLAG,type);
            map.put(ApiConfig.POST.DESC,caption);
            map.put(ApiConfig.POST.URL,url);


            startPost(type, url, post, (RemoteViews) postDetails.get("contentView"), map, post.getPathForCloudinary());
        }
    }


    private void startPost(String type, String mediaUrl, Post post, RemoteViews contentView, Map<String, Object> map, String path) {
        Log.e(TAG, "startPost: " );
        service.sendPost(AppController.getInstance().getApiToken(), Constants.LANGUAGE, map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>(){
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {

                        if (responseBodyResponse.code() == 200) {
                            if (notificationManager != null) {
                                contentView.setTextViewText(R.id.title, "Posted successfully");
                                contentView.setProgressBar(R.id.progress, 100, 100, false);
                                notificationManager.notify(Integer.parseInt(post.getId()), builder2.setContent(contentView).build());
                                notificationManager.cancel(Integer.parseInt(post.getId()));
                            }

                            boolean deleted = postDb.delete(post.getId());
                            Log.i("DELETED", "" + deleted);
                            if (!post.isGallery())
                                deleteFileFromDisk(path);

                            JSONObject obj = new JSONObject();
                            try {
                                obj.put("eventName", "postCompleted");
                                obj.put("data", responseBodyResponse);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            bus.post(obj);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        if (notificationManager != null) {
                            contentView.setTextViewText(R.id.title, "Failed to post..");
                            contentView.setProgressBar(R.id.progress, 100, 100, false);
                            notificationManager.notify(Integer.parseInt(post.getId()), builder2.setContent(contentView).build());

                        }

                        boolean deleted = postDb.delete(post.getId());
                        Log.i("DELETED", "" + deleted);
                        if (!post.isGallery())
                            deleteFileFromDisk(path);

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


    @SuppressWarnings("unchecked")

    private void addRequestIdForPost(String postId, String requestId) {
        Log.e(TAG, "addRequestIdForPost: " );
        Map<String, Object> postDetail = (Map<String, Object>) (postDetails.get(postId));

        if (postDetail != null) {

            postDetail.put("requestId", requestId);

            postDetails.put(postId, postDetail);
        }
    }


    @SuppressWarnings("unchecked")

    private Map<String, Object> fetchPostDetailsFromRequestId(String requestId) {
        Log.e(TAG, "fetchPostDetailsFromRequestId: " );
        Map<String, Object> postDetail = null;

        Set<String> keys = postDetails.keySet();
        for (String key : keys) {
            postDetail = (Map<String, Object>) (postDetails.get(key));

            if (postDetail != null) {
                if (postDetail.get("requestId") != null) {

                    if (postDetail.get("requestId").equals(requestId)) {


                        return postDetail;
                    }
                }
            }
        }
        return postDetail;
    }


    private void publishPost(PostData postData) {
        Log.e(TAG, "publishPost: ");
        String data = postData.getData();
        Post post = new Gson().fromJson(data, Post.class);
        RemoteViews contentView = new RemoteViews(AppController.getInstance().getPackageName(), R.layout.custom_push);

        this.type=post.getTypeForCloudinary();
        this.path=post.getPathForCloudinary();
        this.caption=post.getCaption();
        if (post.getTypeForCloudinary().equals(Constants.Post.IMAGE)) {
            //image
            Bitmap bitmap = BitmapFactory.decodeFile(post.getPathForCloudinary());
            contentView.setImageViewBitmap(R.id.image, bitmap);
            contentView.setTextViewText(R.id.title, "Posting...");
        } else {
            // normal video
            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(post.getPathForCloudinary(), MediaStore.Video.Thumbnails.MICRO_KIND);
            contentView.setImageViewBitmap(R.id.image, bMap);
            contentView.setImageViewResource(R.id.play, R.drawable.ic_play_circle_outline_black_24dp);
            contentView.setTextViewText(R.id.title, "Posting...");
        }

        contentView.setProgressBar(R.id.progress, 100, 0, true);

        Map<String, Object> postDetail = new HashMap<>();

        postDetail.put("contentView", contentView);
        postDetail.put("post", post);
        if(postDetails!=null)
            postDetails.put(post.getId(), postDetail);
        postDetails = new HashMap<>();
        postDetails.put(post.getId(), postDetail);

        NotificationCompat.Builder builder1 = new NotificationCompat.Builder(AppController.getInstance(), CHANNEL_ONE_ID)
                .setContent(contentView)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(AppController.getInstance().getResources(), R.mipmap.ic_launcher))
                .setAutoCancel(false).setOngoing(false);

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder2 = new NotificationCompat.Builder(AppController.getInstance(), CHANNEL_ONE_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(AppController.getInstance().getResources(), R.mipmap.ic_launcher))
                .setAutoCancel(true).setOngoing(false)
                .setSound(soundUri);

        notificationManager = (NotificationManager) AppController.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationChannel notificationChannel;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(CHANNEL_ONE_ID, CHANNEL_ONE_NAME, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setShowBadge(true);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        normalPost(post);

        //start notification
        if (notificationManager != null) {
            notificationManager.notify(Integer.parseInt(post.getId()), builder1.build());
        }

    }

    public void addNewPost(PostData postData) {
        Log.e(TAG, "addNewPost: " );
        publishPost(postData);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void deleteFileFromDisk(String filePath) {
        try {
            File file = new File(filePath);

            if (file.exists()) {

                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
