package com.pairzy.com.MatchedView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * <h2>MatchResponseHolder</h2>
 * @since  4/13/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MatchResponseHolder
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private MatchResponseData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MatchResponseData getData() {
        return data;
    }

    public void setData(MatchResponseData data) {
        this.data = data;
    }
}
