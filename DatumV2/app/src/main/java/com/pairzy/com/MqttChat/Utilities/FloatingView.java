package com.pairzy.com.MqttChat.Utilities;

/*
 * Created by moda on 04/04/16.
 */

import android.animation.Animator;
import android.app.Activity;
import android.graphics.Point;
import androidx.core.content.ContextCompat;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import com.pairzy.com.R;





 /* Floating view is used to display a custom view for attachments in the chat screen */

public class FloatingView {


    private static PopupWindow popWindow;

    private FloatingView() {
    }

    public static void onShowPopup(View root, Activity activity, View inflatedView) {

        Display display = activity.getWindowManager().getDefaultDisplay();
        float density = activity.getResources().getDisplayMetrics().density;
        final Point size = new Point();
        display.getSize(size);
        popWindow = new PopupWindow(inflatedView, size.x, ViewGroup.LayoutParams.WRAP_CONTENT,
                true);
        popWindow.setBackgroundDrawable(ContextCompat.getDrawable(activity,
                R.drawable.comment_popup_bg));
        popWindow.setFocusable(true);
        popWindow.setOutsideTouchable(true);
        popWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        if(activity.getCurrentFocus() != null) {
            popWindow.showAtLocation(activity.getCurrentFocus(), Gravity.TOP, 0,
                    Math.round(86 * density));
        }
        else{
            popWindow.showAtLocation(root, Gravity.TOP, 0,
                    Math.round(86 * density));
        }

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            inflatedView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    inflatedView.removeOnLayoutChangeListener(this);
                    openAnimation(inflatedView);
                }
            });
        }
    }


    public static void dismissWindow() {
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//            closeAnimation(null);
//        }
//        else{
            if (popWindow != null) {
                popWindow.dismiss();
            }
//        }
    }

    private static void closeAnimation(View layoutContent) {
        int x = layoutContent.getRight();
        int y = layoutContent.getBottom();
        int startRadius = 0;
        int endRadius = (int) Math.hypot(layoutContent.getWidth(), layoutContent.getHeight());
        Animator anim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(layoutContent, x, y, startRadius, endRadius);
        }
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {}
            @Override
            public void onAnimationEnd(Animator animation) {
                if (popWindow != null) {
                    popWindow.dismiss();
                }
            }
            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });
        anim.start();
    }

    private static void openAnimation(View layoutContent) {
        int x = layoutContent.getRight();
        int y = layoutContent.getTop();
        int startRadius = 0;
        int endRadius = Math.max(layoutContent.getWidth(), layoutContent.getHeight());
        Animator anim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(layoutContent, x, y, startRadius, endRadius);
        }
        anim.start();
    }


    private void viewMenu(boolean isOpen,View layoutContent, RelativeLayout rlBtnCover) {
        if (!isOpen) {
            rlBtnCover.setVisibility(View.VISIBLE);
            int x = layoutContent.getRight();
            int y = layoutContent.getBottom();
            int startRadius = 0;
            int endRadius = (int) Math.hypot(layoutContent.getWidth(), layoutContent.getHeight());
            Animator anim = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                anim = ViewAnimationUtils.createCircularReveal(layoutContent, x, y, startRadius, endRadius);
            }
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (popWindow != null) {
                        popWindow.dismiss();
                    }
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            anim.start();
            isOpen = true;

        } else {
            int x = layoutContent.getRight();
            int y = layoutContent.getBottom();
            int startRadius = Math.max(layoutContent.getWidth(), layoutContent.getHeight());
            int endRadius = 0;
            Animator anim = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                anim = ViewAnimationUtils.createCircularReveal(layoutContent, x, y, startRadius, endRadius);
            }
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    layoutContent.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            anim.start();

            isOpen = false;
        }
    }

}
