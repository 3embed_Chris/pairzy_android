package com.pairzy.com.mobileverify.result;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.pairzy.com.AppController;
import com.pairzy.com.BaseModel;
import com.pairzy.com.MqttChat.Database.CouchDbController;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.LocationResponse;
import com.pairzy.com.data.model.LoginData;
import com.pairzy.com.data.model.LoginResponse;
import com.pairzy.com.data.model.MyPrefrance;
import com.pairzy.com.data.model.SearchPreference;
import com.pairzy.com.data.model.Subscription;
import com.pairzy.com.data.model.coinBalance.Coins;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.model.coinCoinfig.CoinData;
import com.pairzy.com.data.source.PassportLocationDataSource;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.locationScreen.model.LocationHolder;
import com.pairzy.com.passportLocation.model.PassportLocation;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.DeviceUuidFactory;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * <h2>ResultModel</h2>
 * <p>
 * ListModel of @{@link ResultFragment}
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 06/01/2018.
 **/
@ActivityScoped
class ResultModel extends BaseModel
{
    private static final String TAG = ResultModel.class.getSimpleName();

    @Inject
    Utility utility;

    @Inject
    Context context;

    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    PassportLocationDataSource locationDataSource;

    @Inject
    CouchDbController couchDbController;
    @Inject
    DeviceUuidFactory deviceUuidFactory;
    @Inject
    Geocoder geocoder;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    CoinBalanceHolder coinBalanceHolder;

    @Inject
    ResultModel() {}

    /*
    * Parsing the otp verification data*/
    public boolean parseVerificationRes(String response) throws EmptyData,DataParsingException
    {
        try
        {
            LoginResponse response_data=utility.getGson().fromJson(response,LoginResponse.class);
            LoginData data=response_data.getData();
            if(data!=null)
            {
                if(data.getIsNewUser())
                {
                   return true;
                }else
                {
                    try {
                        if (data.isPassportLocation())
                        {
                            LocationResponse locationResponse = data.getLocationResponse();
                            if (locationResponse != null) {
                                PassportLocation passportLocation = null;
                                LocationHolder locationHolder = new LocationHolder();
                                locationHolder.setLongitude(locationResponse.getLongitude());
                                locationHolder.setLatitude(locationHolder.getLatitude());
                                passportLocation = getLocationName(locationHolder);
                                if (passportLocation != null) {
                                    String jsonPassport = utility.getGson().toJson(passportLocation, PassportLocation.class);
                                    locationDataSource.setFeatureLocation(jsonPassport);
                                    locationDataSource.setFeaturedLcoationActive(true);
                                }else
                                {
                                    locationDataSource.setFeaturedLcoationActive(false);
                                }
                            }
                        }else
                        {
                            locationDataSource.setFeaturedLcoationActive(false);
                        }

                        ArrayList<Subscription> subscriptionList = data.getSubscription();
                        if(subscriptionList != null && !subscriptionList.isEmpty()){
                            Subscription subscription = subscriptionList.get(0);
                            String subsJson = utility.getGson().toJson(subscription,Subscription.class);
                            dataSource.setSubscription(subsJson);
                        }
                    }catch (Exception ignored){}
                    dataSource.setMyDetails(response);
                    dataSource.setToken(data.getToken());
                    dataSource.setName(data.getFirstName());
                    dataSource.setUserId(data.getId());
                    dataSource.setBirthDate(data.getDob());
                    dataSource.setCountryCode(data.getCountryCode());
                    dataSource.setMobileNumber(data.getContactNumber());
                    dataSource.setGender(data.getGender());
                    dataSource.setEmail(data.getEmail());
                    dataSource.setProfilePicture(data.getProfilePic());
                    dataSource.setUserVideoThumbnail(data.getProfileVideoThumbnail());
                    dataSource.setUserVideo(data.getProfileVideo());
                    dataSource.setMyEducation(data.getEducation());
                    dataSource.setMyWorkPlace(data.getWork());
                    dataSource.setMyJob(data.getJob());
                    ArrayList<String> other_images=data.getOtherImages();
                    if(other_images==null)
                    {
                        other_images=new ArrayList<>();
                    }
                    dataSource.setUserOtherImages(other_images);

                    /*
                     *Storing preference */
                    ArrayList<SearchPreference> search_data=data.getSearchPreferences();
                    String search_details=utility.getGson().toJson(search_data);
                    dataSource.setSearchPreference(search_details);
                     /*
                     *Storing My preference */
                    ArrayList<MyPrefrance> mySearch_data = data.getmyPreferences();
                    String mySearch_details=utility.getGson().toJson(mySearch_data);
                    dataSource.setMyPreference(mySearch_details);

                    /*
                     *coin config data
                     */
                    CoinData coinData = response_data.getCoinConfigData();
                    coinConfigWrapper.setCoinData(coinData);

                    /*
                     *wallet
                     */
                    Coins coins = response_data.getWalletCoins();
                    coinBalanceHolder.setCoinBalance(coins.getCoin().toString());

                    dataSource.setLoggedIn(true);
                    initialChatSetup(data);
                    return false;
                }
            }else
            {
                throw new EmptyData("CoinData is empty!");
            }
        }catch (Exception e)
        {
            dataSource.setLoggedIn(false);
            e.printStackTrace();
            throw new DataParsingException(e.getMessage());
        }
    }

    /*
     * Code from hola live on success of mob verification.
     */
    private void initialChatSetup(LoginData loginData){

        String profilePic = "", userName = "";

        CouchDbController db = AppController.getInstance().getDbController();

        Map<String, Object> map = new HashMap<>();
        if (!TextUtils.isEmpty(loginData.getProfilePic())) {
            profilePic = loginData.getProfilePic();
            map.put("userImageUrl", profilePic);

        } else {
            map.put("userImageUrl", "");
        }
        if (!TextUtils.isEmpty(loginData.getFirstName())) {
            userName = loginData.getFirstName();
            map.put("userName", userName);
        }
        map.put("userId", loginData.getId());
                            /*
                             * To save the social status as the text value in the properties
                             *
                             */
        map.put("userIdentifier", loginData.getId());
        map.put("apiToken", loginData.getToken());
        AppController.getInstance().getSharedPreferences().edit().putString("token", loginData.getToken()).apply();
                            /*
                             * By phone number verification
                             */
        map.put("userLoginType", 1);
        Log.d(TAG, "initialChatSetup: indexDocId"+AppController.getInstance().getIndexDocId());
        if (!db.checkUserDocExists(AppController.getInstance().getIndexDocId(), loginData.getId())) {
            String userDocId = db.createUserInformationDocument(map);
            db.addToIndexDocument(AppController.getInstance().getIndexDocId(), loginData.getId(), userDocId);
        } else {
            db.updateUserDetails(db.getUserDocId(loginData.getId(), AppController.getInstance().getIndexDocId()), map);
        }
        if (!userName.isEmpty()) {
            db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(), loginData.getId(), 1, true);
        } else {
            db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(), loginData.getId(), 1, false);
        }
                        /*
                         * To update myself as available for call
                         */
        AppController.getInstance().setSignedIn(false,true,loginData.getId() , userName, loginData.getId());
        //AppController.getInstance().setSignStatusChanged(true);
    }

    /**
     * @param mobile_number : mobile number
     * @return parameters
     */
    Map<String, Object> verifyParams(String mobile_number)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.VerifyMobileKey.PHONE_NUMBER, mobile_number);
        return map;
    }


    /**
     * @param mobile_number : mobile_number
     * @return parameters
     */
    Map<String, Object> requestOtpParams(String mobile_number,Boolean isPhoneUpdate)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.RequestOtpKey.PHONE_NUMBER, mobile_number);
        if(isPhoneUpdate)
            map.put(ApiConfig.RequestOtpKey.TYPE, 3);
        else
            map.put(ApiConfig.RequestOtpKey.TYPE, 1);
        map.put(ApiConfig.RequestOtpKey.DEVICE_ID, Build.ID);
        return map;
    }
    /**
     * @param mobile_number : mobile_number
     * @return parameters
     */
    Map<String, Object> verifyOtpData(String mobile_number,String otp)
    {
        Integer otp_data=Integer.parseInt(otp);
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.VerifyOtpKey.PHONE_NUMBER, mobile_number);
        map.put(ApiConfig.VerifyOtpKey.TYPE, ApiConfig.VerificationType.NEW_REGISTRATION_OR_LOGIN);
        map.put(ApiConfig.VerifyOtpKey.OTP,otp_data);
        map.put(ApiConfig.VerifyOtpKey.PUSH_TOKEN,dataSource.getPushToken());
        map.put(ApiConfig.VerifyOtpKey.DEVICE_ID,deviceUuidFactory.getDeviceUuid());
        map.put(ApiConfig.VerifyOtpKey.DEVICE_MAKER,utility.getDeviceMaker());
        map.put(ApiConfig.VerifyOtpKey.DEVICE_MODEL,utility.getModel());
        map.put(ApiConfig.VerifyOtpKey.DEVICE_TYPE, ApiConfig.DeviceType.ANDROID);
        map.put(ApiConfig.VerifyOtpKey.DEVICE_OS, ""+android.os.Build.VERSION.RELEASE);
        map.put(ApiConfig.VerifyOtpKey.APP_VERSION, utility.getAppCurrentVersion());
        return map;
    }

    public Observable getDeviceInfo(){
        return Observable.create(new ObservableOnSubscribe<DeviceInfo>() {
            @Override
            public void subscribe(ObservableEmitter<DeviceInfo> e) throws Exception {
                e.onNext(new DeviceInfo());
            }
        });
    }

    class DeviceInfo{
        public String deviceId = "";
        public String appVersion = "";
        public DeviceInfo(){
            deviceId = deviceUuidFactory.getDeviceUuid();
            appVersion = utility.getAppCurrentVersion();
        }
    }
    /**
     * Fetch the full address via lat and long.
     * @param locationHolder contain lat , long.
     * @return PassportLocation.
     */
    public PassportLocation getLocationName(LocationHolder locationHolder) {
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(locationHolder.getLatitude(),locationHolder.getLongitude(),1);
            if(addresses != null){
                String  subLocation = addresses.get(0).getSubLocality();
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String addressName = addresses.get(0).getFeatureName();

                boolean cityEmpty = TextUtils.isEmpty(city);
                boolean stateEmpty = TextUtils.isEmpty(state);
                boolean countryEmpty = TextUtils.isEmpty(country);
                String subAddress = ((cityEmpty)?"":city+((stateEmpty)?"":","))
                        +((stateEmpty)?"":state+((countryEmpty)?"":","))
                        +((countryEmpty)?"":country+".");
                PassportLocation currentLocation = new PassportLocation();
                currentLocation.setLocationName(addressName);
                currentLocation.setSubLocationName(subAddress);
                currentLocation.setLatitude(locationHolder.getLatitude());
                currentLocation.setLongitude(locationHolder.getLongitude());
                return currentLocation;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Map<String, Object> getVerifyOptForChangeNumData(String countryCode, String mobile_number, String otp) {
        Integer otp_data=Integer.parseInt(otp);
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.VerifyOtpKey.COUNTRY_CODE, countryCode);
        map.put(ApiConfig.VerifyOtpKey.PHONE_NUMBER, mobile_number);
        map.put(ApiConfig.VerifyOtpKey.OTP,otp_data);
        map.put(ApiConfig.VerifyOtpKey.DEVICE_ID,deviceUuidFactory.getDeviceUuid());
        return map;
    }
}
