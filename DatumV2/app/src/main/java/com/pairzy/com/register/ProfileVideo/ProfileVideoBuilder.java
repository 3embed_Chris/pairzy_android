package com.pairzy.com.register.ProfileVideo;

import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;

/**
 * @since 1/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface ProfileVideoBuilder
{
    @FragmentScoped
    @Binds
    ProfileVideoFrg getProfileVideo(ProfileVideoFrg profileVideoFrg);

    @FragmentScoped
    @Binds
    ProfileVideoContact.Presenter taskPresenter(ProfileVideoFrgPresenter presenter);
}
