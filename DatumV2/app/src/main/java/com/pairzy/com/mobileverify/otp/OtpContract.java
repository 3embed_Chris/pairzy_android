package com.pairzy.com.mobileverify.otp;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.data.model.LoginResponse;

public interface OtpContract
{
    interface View
    {
        void showMessage(String message);
        void notHaveOtp();
        void openPrivacyPolice();
        void TermAndServices();
        void otpNotConfirmed(String message);
        void loginSuccessful(LoginResponse response);
        void disableNext();
        void verifiedOtp(String otp);
    }


    interface Presenter extends BasePresenter
    {
       void notHaveOtpEvent();
       void validateInput(String data);
       void onPrivacyEvent();
       void onTermsEvent();
       void onContinueEvent();
        void showError(String message);
    }
}
