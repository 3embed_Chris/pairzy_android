package com.pairzy.com.util.ImageChecker;

import java.util.ArrayList;
/**
 * @since  4/3/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class Multiple extends PropertiesModel  implements PropertiesExtractor
{
    private ArrayList<Properties> list;
    public Multiple(ArrayList<Properties> list)
    {
        this.list=list;
    }

    @Override
    public String getProperties()
    {
        return arrangedProperties(list);
    }

    /*
     *Extractor the data */
    private String arrangedProperties(ArrayList<Properties> list)
    {
        int size=list.size();
        if(size==1)
        {
            return list.get(0).value;
        }else
        {
            String data="";
            for(int count=0;count<size;count++)
            {
                data=list.get(count).value;
                if(count<size-1)
                {
                    data=data+",";
                }
            }
            return data;
        }
    }
}
