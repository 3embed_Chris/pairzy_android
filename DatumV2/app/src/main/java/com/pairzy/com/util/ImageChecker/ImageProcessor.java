package com.pairzy.com.util.ImageChecker;

import android.net.Uri;
import android.util.Log;
import com.pairzy.com.util.ImageChecker.ModelClasses.Celebrity;
import com.pairzy.com.util.ImageChecker.ModelClasses.Face;
import com.pairzy.com.util.ImageChecker.ModelClasses.ResponseHolder;
import com.google.gson.Gson;
import java.io.File;
import java.util.ArrayList;
/**
 * <h2>ImageProcessor</h2>
 * <P>
 *
 * </P>
 * @since  4/3/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ImageProcessor
{
    private String API_USER="",API_SECRET="";
    private String media_path;
    public ImageProcessor(String user_api_key,String api_secret)
    {
        this.API_USER=user_api_key;
        this.API_SECRET=api_secret;
    }

    /*
     *Checking local path for the image */
    public RxImgVerifierObservable checkLocalPathImage(String path,PropertiesModel propertiesModel)
    {
        if(path==null||propertiesModel==null)
        {
            handelError("File path is NUll!");
        }else
        {
            String extension=((PropertiesExtractor)propertiesModel).getProperties();
            media_path =path;
            try
            {
                File local_file=new File(path);
                Uri uri = Uri.fromFile(local_file);
                String url_path=createUrlForLocalPath(extension,uri.toString());
                localImageProcess(url_path);
            } catch (Exception e)
            {
                e.printStackTrace();
                handelError(""+e.getMessage());
            }
        }
        return RxImgVerifierObservable.getInstance();
    }

    /*
     *Checking URL path for the image */
    public RxImgVerifierObservable checkImageFromUrl(String url_path,PropertiesModel propertiesModel)
    {
        if(url_path==null||propertiesModel==null)
        {
            handelError("File path is NUll!");
        }else
        {
            try {
              //  String extension=((PropertiesExtractor)propertiesModel).getProperties();
                String extension="faces,celebrities,wad";
                String url_data=createUrlServerPath(extension,url_path);
                media_path =url_path;
                Log.d("qwqe12", ""+media_path);
                localImageProcess(url_data);
            } catch (Exception e)
            {
                handelError("File path is NUll!");
            }
        }
        return RxImgVerifierObservable.getInstance();
    }


    /**
     * Checking local path requirement
     */
    private void localImageProcess(String url)
    {
        Log.d("12er", ""+url);
        try
        {
            String data= new LoadDetailsTask(url).execute().get();
            Log.d("12er", ""+data);
            ResponseHolder  holder=new Gson().fromJson(data,ResponseHolder.class);
            if(holder.getStatus().equals("status"))
            {
                if(holder.getFaces().size()>0)
                {
                    Face face_details= holder.getFaces().get(0);
                    ArrayList<Celebrity> celeb_list=face_details.getCelebrity();
                    Celebrity celebrity=celeb_list.get(0);
                    if(celebrity.getProb()>0.7)
                    {
                        handleFiled("Seems to be a celebrity "+ celebrity.getName());
                    }else
                    {
                        handleSuccess();
                    }
                }else
                {
                    handleFiled("Not able to detect your face!");
                }
            }else
            {
                handelError("Failed to verify your profile image!");
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            handelError(e.getMessage());
        }
    }


    private String createUrlForLocalPath(String extension,String localPath)
    {
        return "https://api.sightengine.com/1.0/check.json?api_user="+API_USER+"&api_secret="+API_SECRET+"&media=@"+localPath+"&models="+extension;
    }

    private String createUrlServerPath(String extension,String url_path)
    {
        return  "https://api.sightengine.com/1.0/check.json?models="+extension+"&api_user="+API_USER+"&api_secret="+API_SECRET+"&url="+url_path;
    }


    private void handelError(String error)
    {
        VerifierResult failedData=new VerifierResult();
        failedData.setError(true);
        failedData.setMedia_path(null);
        failedData.setMessage(error);
        RxImgVerifierObservable.getInstance().publishData(failedData);
    }

    private void handleFiled(String message)
    {
        VerifierResult failedData=new VerifierResult();
        failedData.setError(false);
        failedData.setVerified(false);
        failedData.setMedia_path(null);
        failedData.setMessage(message);
        RxImgVerifierObservable.getInstance().publishData(failedData);
    }

    private void handleSuccess()
    {
        VerifierResult failedData=new VerifierResult();
        failedData.setError(false);
        failedData.setVerified(true);
        failedData.setMedia_path(media_path);
        failedData.setMessage("Sucess");
        RxImgVerifierObservable.getInstance().publishData(failedData);
    }
}
