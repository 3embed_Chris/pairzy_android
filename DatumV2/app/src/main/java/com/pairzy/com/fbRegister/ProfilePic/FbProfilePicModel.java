package com.pairzy.com.fbRegister.ProfilePic;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ImageView;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.model.cloudinaryDetail.CloudinaryData;
import com.pairzy.com.data.model.cloudinaryDetail.CloudinaryDetailResponse;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.CloudManager.UploadManager;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Utility;

import javax.inject.Inject;

/**
 * @since  2/20/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class FbProfilePicModel extends BaseModel
{
    @Inject
    Utility utility;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    UploadManager uploadManager;


    @Inject
    FbProfilePicModel()
    {}

    public void processImage(String mCurrentPhotoPath,ImageView mImageView) throws Exception
    {
        mImageView.setVisibility(View.VISIBLE);
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
    }

    void parseClodinaryDetail(String response) throws DataParsingException {
        try{
            CloudinaryDetailResponse cloudinaryDetailResponse =
                    utility.getGson().fromJson(response,CloudinaryDetailResponse.class);
            CloudinaryData cloudinaryData = cloudinaryDetailResponse.getCloudinaryData();

            dataSource.setCloudName(cloudinaryData.getCloudName());
            dataSource.setCloudinaryApiKey(cloudinaryData.getApiKey());
            dataSource.setClodinaryApiSecret(cloudinaryData.getApiSecret());

            reinitCloudnaryUpload();
        }catch (Exception e){
            throw new DataParsingException(e.getMessage());
        }
    }

    private void reinitCloudnaryUpload() {
        uploadManager.setNewCreds(true,
                dataSource.getCloudName(),
                dataSource.getCloudinaryApiKey(),
                dataSource.getCloudinaryApiSecret());
    }
}
