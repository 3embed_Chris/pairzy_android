package com.pairzy.com.googleLocationSearch.model;

/**
 * Created by ankit on 23/5/18.
 */

public class AddressListPojo {

    String Address_title;
    String sub_Address;
    Double latitude = 0.0;
    Double logitude = 0.0;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLogitude() {
        return logitude;
    }

    public void setLogitude(Double logitude) {
        this.logitude = logitude;
    }

    public String getAddress_title() {
        return Address_title;
    }

    public void setAddress_title(String address_title) {
        Address_title = address_title;
    }

    public String getSub_Address() {
        return sub_Address;
    }

    public void setSub_Address(String sub_Address) {
        this.sub_Address = sub_Address;
    }

    @Override
    public String toString() {
        return "Address_list_item_pojo{" +
                "Address_title='" + Address_title + '\'' +
                ", sub_Address='" + sub_Address + '\'' +
                ", latitude='" + latitude + '\'' +
                ", logitude='" + logitude + '\'' +
                '}';
    }
}
