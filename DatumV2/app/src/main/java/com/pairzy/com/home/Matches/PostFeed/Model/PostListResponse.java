package com.pairzy.com.home.Matches.PostFeed.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*
 * <h>PostListResponse</h>
 * <p> this is pojo class</P>
 * */
public class PostListResponse {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private ArrayList<PostListPojo> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<PostListPojo> getData() {
        return data;
    }

    public void setData(ArrayList<PostListPojo> data) {
        this.data = data;
    }
}
