package com.pairzy.com.home.Dates.pastDatePage.Model;

import com.pairzy.com.home.Dates.DatesFragUtil;
import com.pairzy.com.home.Dates.Model.LoadMorePastDateStatus;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;
import com.pairzy.com.home.HomeUtil;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by ankit on 16/5/18.
 */

public class PastDateModel {

    @Named(DatesFragUtil.PAST_LIST)
    @Inject
    ArrayList<PastDateListPojo> pastDateList;

    @Named(HomeUtil.LOAD_MORE_PAST_DATE_STATUS)
    @Inject
    LoadMorePastDateStatus loadMorePastDateStatus;

    @Inject
    PastDateModel(){
    }

    public boolean loadMoreRequired(int currentPos) {

        if(loadMorePastDateStatus.isNo_more_data()){
            return false;
        }
        try
        {
            int size=pastDateList.size();
            int pending=size-currentPos;
            if(pending<5)
            {
                PastDateListPojo last_item=pastDateList.get(size-1);
                if(!last_item.isLoading())
                {
                    PastDateListPojo loading_item=new PastDateListPojo();
                    loading_item.setLoading(true);
                    pastDateList.add(loading_item);
                    return true;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public int getListSize() {
        return pastDateList.size();
    }

    public boolean checkDateExist() {
        if(pastDateList.size() > 0)
            return true;
        return false;
    }

    public PastDateListPojo getDateItem(int position) {
        if(pastDateList.size() > position)
            return pastDateList.get(position);
        return null;
    }

    public boolean raplaceDateItem(PastDateListPojo pastDateItem, int position) {
        try {
            if (pastDateList.size() > 0) {
                pastDateList.set(position, pastDateItem);
                return true;
            }
        }catch (Exception e){}
        return false;
    }
}
