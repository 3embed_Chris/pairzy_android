package com.pairzy.com.MyProfile.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MomentsData implements Parcelable {

    @SerializedName("postId")
    @Expose
    private String postId;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("typeFlag")
    @Expose
    private Integer typeFlag;
    @SerializedName("postedOn")
    @Expose
    private String postedOn;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("url")
    @Expose
    private ArrayList<String> url = null;
    @SerializedName("likeCount")
    @Expose
    private Integer likeCount;
    @SerializedName("commentCount")
    @Expose
    private Integer commentCount;
    @SerializedName("distance")
    @Expose
    private String distance;


    @SerializedName("liked")
    @Expose
    private Boolean liked;

    public Boolean getLiked() {
        return liked;
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }

    protected MomentsData(Parcel in) {
        postId = in.readString();
        userId = in.readString();
        userName = in.readString();
        type = in.readString();
        liked=in.readInt()>0;



        if (in.readByte() == 0) {
            typeFlag = null;
        } else {
            typeFlag = in.readInt();
        }
        if (in.readByte() == 0) {
            postedOn = null;
        } else {
            postedOn = in.readString();
        }
        profilePic = in.readString();
        date = in.readString();
        description = in.readString();
        url = in.createStringArrayList();
        if (in.readByte() == 0) {
            likeCount = null;
        } else {
            likeCount = in.readInt();
        }
        if (in.readByte() == 0) {
            commentCount = null;
        } else {
            commentCount = in.readInt();
        }
        distance = in.readString();

    }

    public static final Creator<MomentsData> CREATOR = new Creator<MomentsData>() {
        @Override
        public MomentsData createFromParcel(Parcel in) {
            return new MomentsData(in);
        }

        @Override
        public MomentsData[] newArray(int size) {
            return new MomentsData[size];
        }
    };

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getTypeFlag() {
        return typeFlag;
    }

    public void setTypeFlag(Integer typeFlag) {
        this.typeFlag = typeFlag;
    }

    public String getPostedOn() {
        return postedOn;
    }

    public void setPostedOn(String postedOn) {
        this.postedOn = postedOn;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getUrl() {
        return url;
    }

    public void setUrl(ArrayList<String> url) {
        this.url = url;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(postId);
        dest.writeString(userId);
        dest.writeString(userName);
        dest.writeString(type);
        dest.writeInt(liked?1:0);
        if (typeFlag == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(typeFlag);
        }
        if (postedOn == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeString(postedOn);
        }
        dest.writeString(profilePic);
        dest.writeString(date);
        dest.writeString(description);
        dest.writeStringList(url);
        if (likeCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(likeCount);
        }
        if (commentCount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(commentCount);
        }
        dest.writeString(distance);
    }
}