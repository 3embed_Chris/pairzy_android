package com.pairzy.com.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 12/7/18.
 */

public class LocationResponse {

    @SerializedName("longitude")
    @Expose
    Double longitude;

    @SerializedName("latitude")
    @Expose
    Double latitude;

    public Double getLongitude() {
        return longitude;
    }

    public Double getLatitude() {
        return latitude;
    }
}
