package com.pairzy.com.googleLocationSearch;

import android.content.Intent;

import com.pairzy.com.googleLocationSearch.model.DataHolder;

/**
 * <h>GoogleLocSearchContract interface.</h>
 * @author 3Embed.
 * @since 22/5/18.
 * @version 1.0.
 */

public interface GoogleLocSearchContract {

    interface View{

        void showMessage(String msg);
        void showMessage(int msgId);
        void showError(String errorMsg);
        void showError(int id);
        void showNetworkError(String errorMsg);
        void applyFont();
        void setData(Intent data);
        void emptyData();
        void showData();
        void showLoading();
    }

    interface Presenter{
        void init();
        void dispose();
        void parseLocation(int requestCode, int resultCode, Intent intent);
        void askLocationPermission();
        void callPlacesApi(DataHolder dataHolder);
    }
}
