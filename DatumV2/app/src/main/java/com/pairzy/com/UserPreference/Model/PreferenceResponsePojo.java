package com.pairzy.com.UserPreference.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PreferenceResponsePojo
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Preference_sub_item data;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Preference_sub_item getData() {
        return data;
    }

    public void setData(Preference_sub_item data)
    {
        this.data = data;
    }

}
