package com.pairzy.com.googleLocationSearch.model;

/**
 * Created by ankit on 23/5/18.
 */

public class DataHolder {
    public int isNearest_data;
    public double latitude;
    public double logitude;
    public String Search_text_data;
    public int index_number;
}
