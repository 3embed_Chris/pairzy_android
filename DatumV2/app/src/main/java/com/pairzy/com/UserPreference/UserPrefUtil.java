package com.pairzy.com.UserPreference;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.MediaBottomSelector;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.TypeFaceManager;
import dagger.Module;
import dagger.Provides;

/**
 * @since /22/2018.
 */
@Module
public class UserPrefUtil
{
    @Provides
    @ActivityScoped
    AnimatorHandler animatorHandler(Activity activity)
    {
        return new AnimatorHandler(activity);
    }

    @Provides
    @ActivityScoped
    DatumProgressDialog datumProgressDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        return  new DatumProgressDialog(activity,typeFaceManager);
    }

    @Provides
    @ActivityScoped
    App_permission getApp_permission(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new App_permission(activity,typeFaceManager);
    }

    @Provides
    @ActivityScoped
    MediaBottomSelector getMediaBottomSelector(Activity activity, TypeFaceManager typeFaceManager)
    {
        return new MediaBottomSelector(activity,typeFaceManager);
    }
}
