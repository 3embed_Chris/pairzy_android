package com.pairzy.com.util.FeatureLocAlert;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import androidx.appcompat.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;

/**
 * <h2>RatingDialog class</h2>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class FeatureLocDialog
{
    private FeatureLocAlertCallback callback;
    private Activity activity;
    private Dialog  alert = null;
    private TypeFaceManager typeFaceManager;
    private TextView message;


    public FeatureLocDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        this.typeFaceManager=typeFaceManager;
        this.activity=activity;
    }

    /*
     * Showing the alert.*/
    public void showAlert(FeatureLocAlertCallback callback1)
    {
        this.callback=callback1;
        if(alert!=null&&alert.isShowing())
        {
            alert.cancel();
        }

        AlertDialog.Builder builder=new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);
        LayoutInflater inflater =activity.getLayoutInflater();
        @SuppressLint("InflateParams")
        View alertLayout = inflater.inflate(R.layout.dialog_feature_loc, null);
        builder.setView(alertLayout);
        message=alertLayout.findViewById(R.id.tvMessage);
        message.setTypeface(typeFaceManager.getCircularAirBook());

        Button btnOk=alertLayout.findViewById(R.id.btn_ok);
        btnOk.setTypeface(typeFaceManager.getCircularAirBook());
        btnOk.setOnClickListener(view -> {
            if(callback != null)
                callback.onOk();
            if(alert!=null&&alert.isShowing())
            {
                alert.cancel();
            }
        });
        Button onCanceled=alertLayout.findViewById(R.id.btn_cancel);
        onCanceled.setTypeface(typeFaceManager.getCircularAirBook());
        onCanceled.setOnClickListener(view -> {
            if(callback!=null)
                callback.onCancel();
            if(alert!=null&&alert.isShowing())
            {
                alert.cancel();
            }
        });
        alert=builder.create();

        Window window = alert.getWindow();
        assert window != null;
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        alert.show();
    }

}
