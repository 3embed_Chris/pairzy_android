package com.pairzy.com.home.Dates.Model;

/**
 * Created by ankit on 16/5/18.
 */

public class LoadMorePastDateStatus {
    public boolean isNo_more_data()
    {
        return this.no_more_data;
    }

    public void setNo_more_data(boolean no_more_data)
    {
        this.no_more_data = no_more_data;
    }

    private boolean no_more_data;
}
