package com.pairzy.com.PostMoments;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.pairzy.com.R;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.Module;

@Module
public class ProgressLoading
{
    private static final String TAG=ProgressLoading.class.getSimpleName();

    Context context;

    Dialog dialog;

    @Inject
    public ProgressLoading(Context context) {

        this.context = context;

    }

    public void creates()
    {
        Log.e(TAG, "creates: " );
        dialog=new Dialog(context, R.style.full_screen_dialog);
        View view= LayoutInflater.from(context).inflate(R.layout.custom_progress_bar ,null,false);
        ButterKnife.bind(view);
        dialog.setContentView(view);
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        WindowManager.LayoutParams wlp = window.getAttributes();
        dialog.show();
    }

    public   void dismissDialog()
    {
        Log.e(TAG, "dismissDialog: " );
        if(dialog!=null)
            dialog.dismiss();
    }
}


