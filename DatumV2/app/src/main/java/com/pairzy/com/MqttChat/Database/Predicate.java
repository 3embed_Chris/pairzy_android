package com.pairzy.com.MqttChat.Database;

/*
 * Created by moda on 09/01/17.
 */

interface Predicate<T> {
    boolean apply(T type);
}
