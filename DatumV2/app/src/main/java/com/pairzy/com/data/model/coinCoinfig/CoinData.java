package com.pairzy.com.data.model.coinCoinfig;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 7/7/18.
 */

public class CoinData {

    @SerializedName("perMsgWithoutMatch")
    @Expose
    private PerMsgWithoutMatch perMsgWithoutMatch;
    @SerializedName("boostProfileForADay")
    @Expose
    private BoostProfileForADay boostProfileForADay;
    @SerializedName("physicalDate")
    @Expose
    private InPersonDateInitiated inPersonDateInitiated;
    @SerializedName("superLike")
    @Expose
    private SuperLike superLike;
    @SerializedName("rewind")
    @Expose
    private Rewind rewind;
    @SerializedName("videoDate")
    @Expose
    private VideoDateInitiate videoDateInitiate;
    @SerializedName("audioDate")
    @Expose
    private AudioDateInitiate audioDateInitiate;
    @SerializedName("resheduleDate")
    @Expose
    private ResheduleDate resheduleDate;


    public PerMsgWithoutMatch getPerMsgWithoutMatch() {
        return perMsgWithoutMatch;
    }

    public void setPerMsgWithoutMatch(PerMsgWithoutMatch perMsgWithoutMatch) {
        this.perMsgWithoutMatch = perMsgWithoutMatch;
    }

    public BoostProfileForADay getBoostProfileForADay() {
        return boostProfileForADay;
    }

    public void setBoostProfileForADay(BoostProfileForADay boostProfileForADay) {
        this.boostProfileForADay = boostProfileForADay;
    }

    public InPersonDateInitiated getInPersonDateInitiated() {
        return inPersonDateInitiated;
    }

    public void setInPersonDateInitiated(InPersonDateInitiated inPersonDateInitiated) {
        this.inPersonDateInitiated = inPersonDateInitiated;
    }

    public SuperLike getSuperLike() {
        return superLike;
    }

    public void setSuperLike(SuperLike superLike) {
        this.superLike = superLike;
    }

    public Rewind getRewind() {
        return rewind;
    }

    public void setRewind(Rewind rewind) {
        this.rewind = rewind;
    }

    public VideoDateInitiate getVideoDateInitiate() {
        return videoDateInitiate;
    }

    public void setVideoDateInitiate(VideoDateInitiate videoDateInitiate) {
        this.videoDateInitiate = videoDateInitiate;
    }

    public AudioDateInitiate getAudioDateInitiate() {
        return audioDateInitiate;
    }

    public void setAudioDateInitiate(AudioDateInitiate audioDateInitiate) {
        this.audioDateInitiate = audioDateInitiate;
    }

    public ResheduleDate getResheduleDate() {
        return resheduleDate;
    }

    public void setResheduleDate(ResheduleDate resheduleDate) {
        this.resheduleDate = resheduleDate;
    }
}
