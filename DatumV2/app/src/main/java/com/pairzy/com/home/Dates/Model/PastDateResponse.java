package com.pairzy.com.home.Dates.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ankit on 16/5/18.
 */

public class PastDateResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<PastDateListPojo> data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<PastDateListPojo> getData() {
        return data;
    }

    public void setData(ArrayList<PastDateListPojo> data) {
        this.data = data;
    }
}
