package com.pairzy.com.userProfile.Model;

import com.pairzy.com.MyProfile.Model.MomentsData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
/**
 * @since  4/4/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class UserProfileData
{
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("dob")
    @Expose
    private Long dob;
    @SerializedName("profilePic")
    @Expose
    private String profilePic;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("heightInFeet")
    @Expose
    private String heightInFeet;
    @SerializedName("profileVideo")
    @Expose
    private String profileVideo;
    @SerializedName("profileVideoThumbnail")
    @Expose
    private String profileVideoThumbnail;
    @SerializedName("otherImages")
    @Expose
    private ArrayList<String> otherImages = null;
    @SerializedName("dontShowMyAge")
    @Expose
    private Integer dontShowMyAge;
    @SerializedName("dontShowMyDist")
    @Expose
    private Integer dontShowMyDist;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("dateOfBirth")
    @Expose
    private Long dateOfBirth;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("activityStatus")
    @Expose
    private Integer activityStatus;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("contactNumber")
    @Expose
    private String contactNumber;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("instaGramProfileId")
    @Expose
    private String instaGramProfileId;
    @SerializedName("instaGramToken")
    @Expose
    private String instaGramToken;

    @SerializedName("instagramName")
    @Expose
    private String instagramName;

    @SerializedName("age")
    @Expose
    private AgeResponse age;
    @SerializedName("distance")
    @Expose
    private DistanceResponse distance;
    @SerializedName("myPreferences")
    @Expose
    private ArrayList<UserPreference> myPreferences = null;
    @SerializedName("moments")
    @Expose
    private ArrayList<MomentsData> moments = null;
    @SerializedName("work")
    @Expose
    private String work;
    @SerializedName("school")
    @Expose
    private String school;
    @SerializedName("liked")
    @Expose
    private Integer liked;
    @SerializedName("superliked")
    @Expose
    private Integer superliked;
    @SerializedName("unliked")
    @Expose
    private Integer unliked;
    @SerializedName("blocked")
    @Expose
    private Integer blocked;

    @SerializedName("blockedByMe")
    @Expose
    private Integer blockedByMe;

    @SerializedName("onlineStatus")
    @Expose
    private Integer onlineStatus;
//    @SerializedName("isSuperLikedMe")
//    @Expose
//    private Integer isSuperLikedMe = 1;
    @SerializedName("isMatched")
    @Expose
    private Integer isMatch = 0;


    public ArrayList<MomentsData> getMoments() {
        return moments;
    }

    public void setMoments(ArrayList<MomentsData> moments) {
        this.moments = moments;
    }

    public void setInstagramName(String instagramName) {
        this.instagramName = instagramName;
    }

    public String getInstagramName() {
        return instagramName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Long getDob() {
        return dob;
    }

    public void setDob(Long dob) {
        this.dob = dob;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getHeightInFeet() {
        return heightInFeet;
    }

    public void setHeightInFeet(String heightInFeet) {
        this.heightInFeet = heightInFeet;
    }

    public String getProfileVideo() {
        return profileVideo;
    }

    public void setProfileVideo(String profileVideo) {
        this.profileVideo = profileVideo;
    }

    public ArrayList<String> getOtherImages() {
        return otherImages;
    }

    public void setOtherImages(ArrayList<String> otherImages) {
        this.otherImages = otherImages;
    }

    public Integer getDontShowMyAge() {
        return dontShowMyAge;
    }

    public void setDontShowMyAge(Integer dontShowMyAge) {
        this.dontShowMyAge = dontShowMyAge;
    }

    public Integer getDontShowMyDist() {
        return dontShowMyDist;
    }

    public void setDontShowMyDist(Integer dontShowMyDist) {
        this.dontShowMyDist = dontShowMyDist;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getActivityStatus() {
        return activityStatus;
    }

    public void setActivityStatus(Integer activityStatus) {
        this.activityStatus = activityStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInstaGramProfileId() {
        return instaGramProfileId;
    }

    public void setInstaGramProfileId(String instaGramProfileId) {
        this.instaGramProfileId = instaGramProfileId;
    }

    public String getInstaGramToken() {
        return instaGramToken;
    }

    public void setInstaGramToken(String instaGramToken) {
        this.instaGramToken = instaGramToken;
    }

    public AgeResponse getAge() {
        return age;
    }

    public void setAge(AgeResponse age) {
        this.age = age;
    }

    public DistanceResponse getDistance() {
        return distance;
    }

    public void setDistance(DistanceResponse distance) {
        this.distance = distance;
    }

    public ArrayList<UserPreference> getmyPreferences() {
        return myPreferences;
    }

    public void setmyPreferences(ArrayList<UserPreference> myPreferences) {
        this.myPreferences = myPreferences;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public Integer getLiked() {
        return liked;
    }

    public void setLiked(Integer liked) {
        this.liked = liked;
    }

    public Integer getSuperliked() {
        return superliked;
    }

    public void setSuperliked(Integer superliked) {
        this.superliked = superliked;
    }

    public Integer getUnliked() {
        return unliked;
    }

    public void setUnliked(Integer unliked) {
        this.unliked = unliked;
    }

    public Integer getBlocked() {
        return blocked;
    }

    public void setBlocked(Integer blocked) {
        this.blocked = blocked;
    }

    public Integer getOnlineStatus() {
        return onlineStatus;
    }

    public void setOnlineStatus(Integer onlineStatus) {
        this.onlineStatus = onlineStatus;
    }

    public String getProfileVideoThumbnail() {
        return profileVideoThumbnail;
    }

    public void setProfileVideoThumbnail(String profileVideoThumbnail) {
        this.profileVideoThumbnail = profileVideoThumbnail;
    }

//    public int isSuperlikedMe() {
//        return isSuperLikedMe;
//    }

    public Integer getIsMatch() {
        return isMatch;
    }

    public void setIsMatch(Integer isMatch) {
        this.isMatch = isMatch;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public Integer getBlockedByMe() {
        return blockedByMe;
    }

    public void setBlockedByMe(Integer blockedByMe) {
        this.blockedByMe = blockedByMe;
    }
}
