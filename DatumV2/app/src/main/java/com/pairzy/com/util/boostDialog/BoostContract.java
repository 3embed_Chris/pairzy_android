package com.pairzy.com.util.boostDialog;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.boostDetail.model.SubsPlan;
import com.pairzy.com.data.model.Subscription;

import java.util.ArrayList;
/**
 * <h>BoostContract interface</h>
 * @author 3Embed.
 * @since 9/5/18.
 * @version 1.0.
 */

public interface BoostContract
{
    interface View{
        void dismiss();
        void showAlert(BoostAlertCallback callback1, ArrayList<SubsPlan> offerList, ArrayList<Slide> slideList);
        void showAlert(Subscription subs);
        void applyFont();
    }

    interface Presenter extends BasePresenter<View>
    {
        void init();
    }
}
