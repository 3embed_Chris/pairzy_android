package com.pairzy.com.home.Prospects.Passed;
import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;
/**
 * @since  3/23/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface PassedBuilder
{
    @FragmentScoped
    @Binds
    PassedFrg getProspectItemFragment(PassedFrg passedFrg);

    @FragmentScoped
    @Binds
    PassedContract.Presenter taskPresenter(PassedPresenter presenter);
}
