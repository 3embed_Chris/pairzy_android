package com.pairzy.com.util.CloudManager;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.pairzy.com.util.CloudManager.UploadManager.UN_CAUGHT_ERROR;

/**
 * <h1>Cloud_UploadData</h1>
 * <P>
 *   Uploading the image file to upload.
 * </P>
 * @since 19/12/16.
 * @author 3Embed.
 */
class Cloudinary_Upload
{
    private String fiel_path;
    private Upload_callback upload_callback;
    private Cloudinary cloudinary;
    private String api_key;
    private String api_secret_key;
    private static Cloudinary_Upload cloudinary_uploadData;


    /**
     * Single tone class details.*/
    private Cloudinary_Upload(String cloudName,String apiKey,String apiScret)
    {
        Map config = new HashMap();
        config.put("cloud_name",cloudName);
        cloudinary= new Cloudinary(config);
        api_key= apiKey;
        this.api_secret_key = apiScret;
    }


    class Data_packet
    {
        private String file_path;
        private Upload_callback upload_callback;
    }

    static Cloudinary_Upload getInstance(boolean newCreds, String cloudName, String apiKey, String apiSecret)
    {

        if(cloudinary_uploadData!=null && !newCreds)
        {
            return cloudinary_uploadData;
        }else
        {
            cloudinary_uploadData=new Cloudinary_Upload(cloudName,apiKey,apiSecret);
            return cloudinary_uploadData;
        }
    }

    /**
     * <h2>upload_File_Cloudinary</h2>
     * <P>
     *     Method is first getting all the cloudinary details from the server.
     * </P>
     * @param mfile_path contains the file to upload.
     * @param mupload_callback contains the call back to upload.*/
    public void upload_File_Cloudinary(String mfile_path, Upload_callback mupload_callback)
    {
        this.fiel_path=mfile_path;
        this.upload_callback =mupload_callback;
        /*
         *Upload file details.*/
        Upload_Files(fiel_path,this.upload_callback);
    }

    public void upload_Video_Cloudinary(String mfile_path, Upload_callback mupload_callback)
    {
        this.fiel_path=mfile_path;
        this.upload_callback =mupload_callback;
        /*
         *Upload file details.*/
        Upload_Video_Files(fiel_path,this.upload_callback);
    }


    /**
     * <h2>Upload_Files</h2>
     *<P>
     *     Uploading the file path.
     *</P> */
    void Upload_Files(String mfile_path, Upload_callback mupload_callback)
    {
        Data_packet data_packet=new Data_packet();
        data_packet.file_path=mfile_path;
        data_packet.upload_callback=mupload_callback;
        /*
         * Calling async task to upload file.*/
        new UploadData().execute(data_packet);
    }

    @SuppressLint("StaticFieldLeak")
    private class UploadData extends AsyncTask<Data_packet,Void,JSONObject>
    {
        boolean isError=false;
        JSONObject resutl;
        Upload_callback mupload_callback;
        @Override
        protected JSONObject doInBackground(Data_packet... data_packets)
        {
            Data_packet data_packet=data_packets[0];
            mupload_callback=data_packet.upload_callback;
            resutl = new JSONObject();
            try
            {
                Map  cloudinaryResult= cloudinary.uploader().upload(data_packet.file_path, ObjectUtils.asMap("api_secret", api_secret_key, "api_key", api_key));
                JSONObject obj=new JSONObject(cloudinaryResult);
                resutl=obj;
                resutl.getString("url");
                resutl.getInt("height");
                resutl.getInt("width");
                isError=false;
            } catch (IOException e)
            {
                e.printStackTrace();
                try {
                    resutl.put("error","Upload Error !");
                    isError=true;
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }catch (JSONException e)
            {
                e.printStackTrace();
                try {
                    resutl.put("error","Response Parsing  Error !");
                    isError=true;
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }catch (Exception e)
            {
                e.printStackTrace();
                try {
                    resutl.put("error",UN_CAUGHT_ERROR);
                    isError=true;
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
            return resutl;
        }

        @Override
        protected void onPostExecute(JSONObject resultObj)
        {
            if(isError)
            {
                try {

                    mupload_callback.onError(resultObj.getString("error"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    mupload_callback.onError(e.getMessage());
                }
            }else
            {
                try {
                    mupload_callback.onSucess(resultObj.getString("url"),resultObj.getInt("height"),resultObj.getInt("width"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    mupload_callback.onError(e.getMessage());
                }
            }
        }
    }


    /**
     * <h2>Upload_Files</h2>
     *<P>
     *     Uploading the file path.
     *</P> */
    void Upload_Video_Files(String mfile_path, Upload_callback mupload_callback)
    {
        Data_packet data_packet=new Data_packet();
        data_packet.file_path=mfile_path;
        data_packet.upload_callback=mupload_callback;
        /*
         * Calling async task to upload file.*/
        new UploadVideoData().execute(data_packet);
    }

    @SuppressLint("StaticFieldLeak")
    private class UploadVideoData extends AsyncTask<Data_packet,Void,JSONObject >
    {
        boolean isError=false;
        JSONObject resutl = new JSONObject();
        Upload_callback mupload_callback;
        @Override
        protected JSONObject doInBackground(Data_packet... data_packets)
        {
            Data_packet data_packet=data_packets[0];
            mupload_callback=data_packet.upload_callback;
            try
            {
                Map  cloudinaryResult= cloudinary.uploader().upload(data_packet.file_path, ObjectUtils.asMap("api_secret", api_secret_key, "api_key", api_key,"resource_type","video"));
                JSONObject obj=new JSONObject(cloudinaryResult);
                Log.w("Cloudinary result", " : "+cloudinaryResult.toString());
                resutl=obj;
                resutl.getString("url");
                resutl.getInt("height");
                resutl.getInt("width");
                isError=false;
            } catch (IOException e)
            {
                e.printStackTrace();
                try {
                    resutl.put("error",e.getMessage());
                    isError=true;
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }catch (JSONException e)
            {
                e.printStackTrace();
                try {
                    resutl.put("error","Response Parsing  Error !");
                    isError=true;
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }catch (Exception e)
            {
                e.printStackTrace();
                try {
                    resutl.put("error",UN_CAUGHT_ERROR);
                    isError=true;
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
            return resutl;
        }

        @Override
        protected void onPostExecute(JSONObject resultObj)
        {
            if(isError)
            {
                try {
                    mupload_callback.onError(resultObj.getString("error"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    mupload_callback.onError(e.getMessage());
                }
            }else
            {
                try {
                    mupload_callback.onSucess(resultObj.getString("url"),resultObj.getInt("height"),resultObj.getInt("width"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    mupload_callback.onError(e.getMessage());
                }
            }
        }
    }


    interface Upload_callback
    {
        void onSucess(String path,int height,int width);

        void onError(String error);
    }

}