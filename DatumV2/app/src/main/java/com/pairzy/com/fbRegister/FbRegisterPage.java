package com.pairzy.com.fbRegister;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.UserPreference.MyPreferencePage;
import com.pairzy.com.fbRegister.Email.FbEmailFragment;
import com.pairzy.com.fbRegister.Gender.FbGenderFragment;
import com.pairzy.com.fbRegister.Name.FbNameFragment;
import com.pairzy.com.fbRegister.ProfilePic.FbProfilePicFrg;
import com.pairzy.com.fbRegister.ProfileVideo.FbProfileVideoFrg;
import com.pairzy.com.fbRegister.Userdob.FbDobFragment;
import com.pairzy.com.home.HomeActivity;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.CustomVideoView.NoInterNetView;
import com.facebookmanager.com.FacebookUserDetails;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 *<h2>{@link FbRegisterPage}</h2>
 * <P>
 *
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 15/02/2017.*/
public class FbRegisterPage extends BaseDaggerActivity implements FbRegisterContact.View
{
    private final String FB_DATA = "fb_data";
    @Inject
    @Named(FbRegisterActivityBuilder.ACTIVITY_FRAGMENT_MANAGER)
    FragmentManager fragmentManager;
    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;
    @Inject
    App_permission app_permission;
    @Inject
    FbRegisterContact.Presenter presenter;
    @Inject
    Activity activity;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.no_internetView)
    NoInterNetView noInterNetView;
    public static FacebookUserDetails details;
    private Unbinder unbinder;
    boolean email = false;
    boolean name = false;
    boolean dob = false;
    boolean gender = false;
    boolean profilePic = false;
    //boolean profileVid = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register__page);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        unbinder=ButterKnife.bind(this);
        progress_bar.setMax(4);
        progress_bar.setProgress(1);
        Bundle data=getIntent().getExtras();
        if(data!=null)
        {
            details= (FacebookUserDetails) data.getSerializable(FB_DATA);
        }
        //presenter.initNetworkObserver();
        presenter.launchNextValidFrag(-1,true);
    }

    @Override
    public void launchAppropriateFrag(int currentPos, boolean keepBacktrack) {

        if((email || TextUtils.isEmpty(details.getEmail())) && currentPos < 0){ //email
            email = true;
            launchFrg(0,keepBacktrack);
        }
        else if((name || TextUtils.isEmpty(details.getName())) && currentPos < 1){ //name
            name = true;
            launchFrg(1,keepBacktrack);
        }
        else if((dob || TextUtils.isEmpty(details.getBirthDate())) && currentPos < 2){ //dob
            dob = true;
            launchFrg(2,keepBacktrack);
        }
        else if((gender || TextUtils.isEmpty(details.getGender())) && currentPos < 3){ //gender
            gender = true;
            launchFrg(3,keepBacktrack);
        }
        else if((profilePic || TextUtils.isEmpty(details.getUserPic())) && currentPos < 4){ //profile pic
            profilePic = true;
            launchFrg(4,keepBacktrack);
        }
        else{  //profile video
            launchFrg(5,keepBacktrack);  //
        }
    }

    @Override
    public FacebookUserDetails getFbDetails() {
        return details;
    }

    @Override
    public void facebookLoginSuccess()
    {
        Intent home_intent=new Intent(this,HomeActivity.class);
        home_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(home_intent);
        finish();
    }

    @Override
    public void facebookSignUpSuccess()
    {
        Intent intent=new Intent(this,MyPreferencePage.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        //startActivity(new Intent(this,MyPreferencePage.class));
    }


    /*
     * launching the initial fragment.*/
    private void launchFrg(int pos,boolean keepBacktrack)
    {
        Bundle bundle = new Bundle();
        bundle.putSerializable(FB_DATA,details);

        switch (pos){
            case 0: //email
                FbEmailFragment fbEmailFragment=new FbEmailFragment();
                fbEmailFragment.setArguments(bundle);
                presenter.launchFragment(fbEmailFragment,keepBacktrack);
                break;
            case 1:  //name
                FbNameFragment fbNameFragment=new FbNameFragment();
                fbNameFragment.setArguments(bundle);
                presenter.launchFragment(fbNameFragment,keepBacktrack);
                break;
            case 2: //dob
                FbDobFragment fbDobFragment=new FbDobFragment();
                presenter.launchFragment(fbDobFragment,keepBacktrack);
                break;
            case 3: //gender
                FbGenderFragment fbGenderFragment =new FbGenderFragment();
                presenter.launchFragment(fbGenderFragment,keepBacktrack);
                break;
            case 4: //profile pic
                FbProfilePicFrg fbProfilePicFrg=new FbProfilePicFrg();
                presenter.launchFragment(fbProfilePicFrg,keepBacktrack);
                break;
            default: //profile video
                FbProfileVideoFrg fbProfileVideoFrg=new FbProfileVideoFrg();
                presenter.launchFragment(fbProfileVideoFrg,keepBacktrack);
        }
    }

    @Override
    public void onBackPressed()
    {
        if(!handelBackPressed())
        {
            this.finish();
        }else
        {
            super.onBackPressed();
        }
    }

    /*
     *Handling the back press
     **/
    public boolean handelBackPressed()
    {
        int count=fragmentManager.getBackStackEntryCount();
        updateProgress(count-1);
        return count > 1;
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onDestroy()
    {
        unbinder.unbind();
        presenter.dropView();
        super.onDestroy();
    }

    @Override
    public void openPreferencePage()
    {
        Intent intent=new Intent(this,MyPreferencePage.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    @Override
    public void updateProgress(int progress)
    {
        progress_bar.setProgress(progress);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if(!app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults))
        {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void showError(String message)
    {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void openFragment(DaggerFragment fragment ,boolean keepBack)
    {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.frg_enter_from_right, R.anim.frg_exit_from_left);
        fragmentTransaction.add(R.id.register_container, fragment);
        if(keepBack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
        fragmentTransaction.commit();
    }

    @Override
    public void updateInterNetStatus(boolean status)
    {
        if(noInterNetView!=null)
        {
            if(status)
            {
                noInterNetView.internetFound();
            }else
            {
                noInterNetView.showNoInternet();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(!presenter.onHandelActivityResult(requestCode,resultCode,data))
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
