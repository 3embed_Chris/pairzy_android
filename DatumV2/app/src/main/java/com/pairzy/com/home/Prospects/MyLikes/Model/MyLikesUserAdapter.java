package com.pairzy.com.home.Prospects.MyLikes.Model;
import android.annotation.SuppressLint;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pairzy.com.R;
import com.pairzy.com.home.Prospects.OnAdapterItemClicked;
import com.pairzy.com.util.DatumCallbacks.ListItemClick;
import com.pairzy.com.util.LoadingViews.LoadMoreViewHolder;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
/**
 * <h2>MyLikesUserAdapter</h2>
 * <P>
 *
 * </P>
 * @since  3/23/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MyLikesUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private final int ITEM=0,LOADING_ITEM=1;
    private ArrayList<MyLikesItemPojo> items;
    private TypeFaceManager typeFaceManager;
    private ListItemClick callBack;
    private OnAdapterItemClicked adpCallback;
    private Utility utility;

    public MyLikesUserAdapter(ArrayList<MyLikesItemPojo> items, TypeFaceManager typeFaceManager,Utility utility)
    {
        this.typeFaceManager=typeFaceManager;
        this.items=items;
        this.utility=utility;
        intiCallback();
    }
    /*
    * Adding the listener*/
    public void setClickListener(OnAdapterItemClicked callback)
    {
        this.adpCallback=callback;
    }

    @SuppressLint("InflateParams")
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        RecyclerView.ViewHolder holder = null;
        switch (viewType)
        {
            case ITEM:
                View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.userlist_item_view,parent,false);
                holder=new MyLikesItemHolder(view,typeFaceManager,callBack);
                break;
            case LOADING_ITEM:
                View loading_item = LayoutInflater.from(parent.getContext()).inflate(R.layout.load_more_layout,parent, false);
                holder=new LoadMoreViewHolder(loading_item,typeFaceManager,callBack);
                break;
            default:
                View view_default= LayoutInflater.from(parent.getContext()).inflate(R.layout.userlist_item_view,parent,false);
                holder=new MyLikesItemHolder(view_default,typeFaceManager,callBack);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        switch (holder.getItemViewType())
        {
            case ITEM:
                try
                {
                    handelView((MyLikesItemHolder)holder);
                }catch (Exception e){}
                break;
            case LOADING_ITEM:
                try
                {
                    loadingView((LoadMoreViewHolder)holder);
                }catch (Exception e){}
                break;
            default:
                try
                {
                    handelView((MyLikesItemHolder)holder);
                }catch (Exception e){}
        }
    }

    @Override
    public int getItemViewType(int position)
    {
        if(items.get(position).isLoading())
        {
            return LOADING_ITEM;
        }else
        {
            return ITEM;
        }
    }

    @Override
    public int getItemCount()
    {
        return items.size();
    }

    /*
     * inti callback*/
    private void intiCallback()
    {
        callBack= (id, position) -> {
            switch (id)
            {
                case R.id.load_more_view:
                    if(adpCallback!=null)
                        adpCallback.tryLoadAgain();
                    break;
                case R.id.parent_view:
                    if(adpCallback!=null)
                        adpCallback.openUserProfile(position);
                    break;
            }
        };
    }

    /*
     * Showing the loading view for the load more*/
    private void loadingView(LoadMoreViewHolder holder)
    {
        int position=holder.getAdapterPosition();
        MyLikesItemPojo temp_data=items.get(position);
        if(temp_data.isLoadingFailed())
        {
            holder.setFailed();
        }else
        {
            holder.showLoadingLoadingAgain();
        }
    }

    /*
     * Handling the image view*/
    private void handelView(MyLikesItemHolder holder)
    {
        int position=holder.getAdapterPosition();
        MyLikesItemPojo temp_data=items.get(position);
        if(temp_data==null)
        {
            return;
        }
        holder.simpleDraweeView.setImageURI(temp_data.getProfilePic());
        holder.user_name.setText(utility.formatString(temp_data.getFirstName()));
        holder.time_view.setText(utility.getTimeFormatToShow(temp_data.getCreation()));
        if(temp_data.getEducation()!=null&&!TextUtils.isEmpty(temp_data.getEducation()))
        {
            holder.school_name.setVisibility(View.VISIBLE);
            holder.school_name.setText(utility.formatString(temp_data.getEducation()));
        }else
        {
            holder.school_name.setVisibility(View.GONE);
        }
    }
}
