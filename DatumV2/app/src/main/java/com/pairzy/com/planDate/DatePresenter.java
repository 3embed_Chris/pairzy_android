package com.pairzy.com.planDate;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.data.model.DateEvent;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Dates.Pending_page.PendingFrgPresenter;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.planDate.model.DateModel;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.CalendarEventHelper;
import com.pairzy.com.util.CustomObserver.CoinBalanceObserver;
import com.pairzy.com.util.CustomObserver.DateObserver;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.SpendCoinDialog.CoinSpendDialogCallback;
import com.pairzy.com.util.SpendCoinDialog.WalletEmptyDialogCallback;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * <h>CallDatePresenter class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public class DatePresenter implements DateContract.Presenter,SingleDateAndTimePickerDialog.Listener,
        App_permission.Permission_Callback, WalletEmptyDialogCallback, CoinSpendDialogCallback{

    private final String CALENDER_TAG = "calender_tag";
    @Inject
    DateContract.View view;
    @Inject
    NetworkService service;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    DateModel dateModel;
    @Inject
    LoadingProgress loadingProgress;
    @Inject
    Activity activity;
    @Inject
    DateObserver dateObserver;
    @Inject
    CalendarEventHelper calendarEventHelper;
    @Inject
    CoinDialog spendCoinDialog;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    App_permission app_permission;
    @Inject
    CoinBalanceObserver coinBalanceObserver;

    @Inject
    SingleDateAndTimePickerDialog.Builder dateTimeDialog;

    private SelectedLocationHolder locationHolder;
    private String userId;
    private String userImage;
    private String userName;
    private Long selectedTime = 0L;
    private DateListPojo date_data;
    private String rescheduleTag;
    private CompositeDisposable compositeDisposable;

    @Inject
    public DatePresenter(){
        locationHolder = new SelectedLocationHolder();
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void init() {
        if(view != null) {
            view.applyFont();
        }
    }

    @Override
    public void launchDateTimePicker() {
        dateTimeDialog.listener(this).display();
    }


    @Override
    public void handleOnActivityResult(int requestCode, int resultCode, Intent data) {
        parseLocation(requestCode,resultCode,data);
    }


    @Override
    public void parseLocation(int requestCode, int resultCode, Intent data) {
        if(resultCode  == RESULT_OK && requestCode == AppConfig.DateActivity.LOACTION_REQUEST_CODE){
            locationHolder = (SelectedLocationHolder) data.getSerializableExtra("location_holder");
            if(locationHolder != null){
                if(view != null) {
                    view.showSelectedAddress(locationHolder);
                }
            }
        }
    }

    @Override
    public void initData(Intent intent) {
        rescheduleTag = intent.getStringExtra("reschedule");
        if(rescheduleTag != null && rescheduleTag.equals(PendingFrgPresenter.RESCHEDULE_TAG)){
            date_data = (DateListPojo) intent.getSerializableExtra("date_data");
            if(view != null){
                view.initUserData(date_data);
            }
        }
        else {
            userId = intent.getStringExtra("user_id");
            userImage = intent.getStringExtra("user_image");
            userName = intent.getStringExtra("user_name");
            if (userImage != null && userName != null & userId != null) {
                if(view != null)
                    view.initUserData(userName, userImage);
            }
        }
        String yourName = dataSource.getName();
        String profilePic = dataSource.getProfilePicture();
        if( yourName!= null && profilePic != null ){
            view.initOwnData(yourName,profilePic);
        }
    }

    @Override
    public void onDateSelected(Date date) {
        if(view != null) {
            try {
                SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd @ hh:mm a");
                Calendar calendar = Calendar.getInstance();
                selectedTime = date.getTime();
                calendar.setTimeInMillis(selectedTime);
                view.showSelectedDate(formatter.format(calendar.getTime()));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    private void launchWalletEmptyDialog(){
        spendCoinDialog.showWalletEmptyDialog(activity.getString(R.string.wallet_empty_title),activity.getString(R.string.in_person_date_empty_wallet_msg),this);
    }


    private void callDateApi() {
        if(rescheduleTag != null && rescheduleTag.equals(PendingFrgPresenter.RESCHEDULE_TAG) && date_data != null){
            if(view != null){
                if(!dateModel.isMissingData(locationHolder)){
                    date_data.setLatitude(locationHolder.getLatitude());
                    date_data.setLatitude(locationHolder.getLongitude());
                    date_data.setPlaceName(locationHolder.getLocationTitle());
                }
                if(selectedTime != 0){
                    if(!isValidDate(selectedTime))
                        return;
                    date_data.setProposedOn(selectedTime);
                }
                view.returnFinalData(date_data);
            }
            return;
        }
        if(dateModel.isMissingData(userId,selectedTime,locationHolder))
            return;

        if(!isValidDate(selectedTime))
            return;

        if(networkStateHolder.isConnected()) {
            Map<String, Object> mapBody = dateModel.getBodyMap(userId, selectedTime, locationHolder);
            loadingProgress.show();
            service.planDate(dataSource.getToken(), "en", mapBody)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.clear();
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if (value.code() == 200) {
                                try {
                                    dateModel.parsePlanDateResponse(value.body().string());
                                    coinBalanceObserver.publishData(true);
                                }catch (Exception e){}
                                if (view != null)
                                    view.showMessage(R.string.date_plan_successful);
                                dateObserver.publishData(true);
                                checkForCalenderPermission();
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else if(value.code() == 402){
                                launchWalletEmptyDialog();
                            }
                            else {
                                try {
                                    if(view != null)
                                        view.showError(dateModel.getErrorMessage(value.errorBody().string()));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            e.printStackTrace();
                            if (view != null)
                                view.showError(activity.getString(R.string.failed_to_schedule_date));
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }
        else {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    private boolean isValidDate(Long _selecteTime) {
        boolean isValid = true;
        if(System.currentTimeMillis() > _selecteTime){
            if (view != null)
                view.showError(activity.getString(R.string.invalid_date_time_msg));
            isValid = false;
        }
        return isValid;
    }



    @Override
    public void checkForCalenderPermission() {

        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.READ_CALENDER);
        permissions.add(App_permission.Permission.WRITE_CALENDER);
        app_permission.getPermission(CALENDER_TAG,permissions,this);
    }

    private void setDateReminder(Long dateReminder) {
        String personName = "";
        String location = "";
        String _userId = "";
        String dateType = "In person date";
        if(date_data != null) {
            personName = date_data.getOpponentName();
            userId = date_data.getOpponentId();
        }
        else{
            personName = userName;
            _userId = userId;
        }
        if(locationHolder != null)
            location = locationHolder.getLocationTitle();
        deletePreviousReminder(userId);
        int eventId = calendarEventHelper.addDateReminder(selectedTime,personName,location,dateType);
        DateEvent dateEvent = new DateEvent();
        dateEvent.setUserId(_userId);
        dateEvent.setEventId(String.valueOf(eventId));
        dateModel.saveEventId(dateEvent);
        finishActivity();
    }

    private void finishActivity(){
        activity.finish();
    }
    private void deletePreviousReminder(String userId) {
        String eventId = dateModel.checkIfReminderExist(userId);
        if(!TextUtils.isEmpty(eventId)){
            calendarEventHelper.deleteEvent(eventId);
        }
    }

    public void loadCoinDialog() {
        if(dateModel.isEnoughWalletBalance()) {
            if (dateModel.isDialogNeedToShow(rescheduleTag)) {
                launchSpendCoinDialog();
            } else {
                callDateApi();
            }
        }
        else{
            //launch wallet empty dialog
            launchWalletEmptyDialog();
        }
    }

    private void launchSpendCoinDialog() {
        try {
            if(rescheduleTag != null && rescheduleTag.equals(PendingFrgPresenter.RESCHEDULE_TAG)){
                Integer coinSpend = coinConfigWrapper.getCoinData().getResheduleDate().getCoin();
                String btnText = String.format(Locale.ENGLISH, " I am OK to spend %s coins", coinSpend);
                spendCoinDialog.showCoinSpendDialog(activity.getString(R.string.spend_coin_on_reschedule_date_dialog_title), activity.getString(R.string.spend_coin_on_reschedule_date_dialog_msg),
                        btnText, this);
            }
            else {
                Integer coinSpend = coinConfigWrapper.getCoinData().getInPersonDateInitiated().getCoin();
                String title = String.format(Locale.ENGLISH,"Spend %d coins to setup a %s with %s.",coinSpend,activity.getString(R.string.In_person_date),userName);
                String btnText = String.format(Locale.ENGLISH, " I am OK to spend %s coins", coinSpend);
                spendCoinDialog.showCoinSpendDialog(title, "", btnText, this);
            }
        }catch (Exception e){}
    }


    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        setDateReminder(selectedTime);
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        app_permission.show_Alert_Permission(activity.getString(R.string.calender_access_text),activity.getString(R.string.calender_access_subtitle),
                activity.getString(R.string.location_acess_message),stringArray);
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean parmanent) {
        finishActivity();
    }


    public void dispose() {
        compositeDisposable.clear();
    }

    @Override
    public void checkForValidInput() {
        if(locationHolder == null || locationHolder.getLatitude() == 0.0 || locationHolder.getLongitude() == 0){
            if(view != null)
                view.showMessage(activity.getString(R.string.empty_date_location_msg));
        } else if(selectedTime == 0L){
            if(view != null)
                view.showMessage(activity.getString(R.string.empty_date_time_msg));
        }else{
            loadCoinDialog();
        }
    }

    /*
     * coin dialog callback
     */
    @Override
    public void onByMoreCoin() {
        if(view != null)
            view.launchCoinWallet();
    }

    /*
     * coin dialog callback
     */
    @Override
    public void onOkToSpendCoin(boolean dontShowAgain) {
        callDateApi();
        if(rescheduleTag != null && rescheduleTag.equals(PendingFrgPresenter.RESCHEDULE_TAG)) {
            dateModel.updateRescheduleSpendCoinShowPref(!dontShowAgain);  //false means dont show
        }
        else{
            dateModel.updateSpendCoinShowPref(!dontShowAgain);  //false means dont show
        }
    }

}
