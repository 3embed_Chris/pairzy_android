package com.pairzy.com.appSettings;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.AppSetting.AppSettingFrag;
import com.pairzy.com.util.SuggestionAleret.SuggestionDialog;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.boostDialog.BoostDialog;
import com.pairzy.com.util.progressbar.LoadingProgress;

import dagger.Module;
import dagger.Provides;

@Module
public class AppSettingsUtil {

    @ActivityScoped
    @Provides
    AppSettingFrag appSettingFrag(){
        return new AppSettingFrag();
    }

    @Provides
    @ActivityScoped
    SuggestionDialog getSuggestionAlert(Activity activity, TypeFaceManager typeFaceManager, Utility utility, PreferenceTaskDataSource dataSource)
    {
        return new SuggestionDialog(activity,typeFaceManager,utility,dataSource);
    }


    @Provides
    @ActivityScoped
    BoostDialog getBootsDialog(Activity activity, TypeFaceManager typeFaceManager, PreferenceTaskDataSource dataSource, Utility utility)
    {
        return new BoostDialog(activity,typeFaceManager,dataSource,utility);
    }

    @Provides
    @ActivityScoped
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }}
