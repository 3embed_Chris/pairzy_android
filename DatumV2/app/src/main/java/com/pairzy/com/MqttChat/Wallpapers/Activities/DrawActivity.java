package com.pairzy.com.MqttChat.Wallpapers.Activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.util.FileUtil.Config;
import com.pairzy.com.util.TypeFaceManager;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.pairzy.com.AppController;
import com.pairzy.com.MqttChat.Doodle.DrawView;
import com.pairzy.com.MqttChat.Utilities.Utilities;

import javax.inject.Inject;

/**
 * Created by moda on 10/10/17.
 */

public class DrawActivity extends BaseDaggerActivity {

    @Inject
    TypeFaceManager typeFaceManager;

    private Bus bus = AppController.getBus();
    private DrawView drawView;

    private RelativeLayout mainLinearLayout, colourId, root;
    private ImageView redButton, blackButton, greenButton, blueButton;
    private static final int IMAGE_QUALITY = 50;


    private String docId;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallpaper_doodle);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        docId = getIntent().getExtras().getString("documentId");
        root = (RelativeLayout) findViewById(R.id.root);
        mainLinearLayout = (RelativeLayout) findViewById(R.id.mainlayout);
        colourId = (RelativeLayout) findViewById(R.id.colourid);
        drawView = new DrawView(this);


        if (drawView.getParent() != null)
            ((ViewGroup) drawView.getParent()).removeView(drawView);

        if (colourId.getParent() != null)
            ((ViewGroup) colourId.getParent()).removeView(colourId);

        mainLinearLayout.addView(drawView);
        mainLinearLayout.addView(colourId);

        redButton = (ImageView) findViewById(R.id.button4);
        blackButton = (ImageView) findViewById(R.id.button1);
        greenButton = (ImageView) findViewById(R.id.button3);
        blueButton = (ImageView) findViewById(R.id.button2);

        blackButton.setSelected(true);
        redButton.setSelected(false);
        greenButton.setSelected(false);
        blueButton.setSelected(false);
        redButton.setClickable(true);
        blackButton.setClickable(true);
        greenButton.setClickable(true);
        blueButton.setClickable(true);
        blackButton.setSelected(true);

        redButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                blackButton.setSelected(false);
                redButton.setSelected(true);
                greenButton.setSelected(false);
                blueButton.setSelected(false);
                drawView.mPaint.setColor(ContextCompat.getColor(DrawActivity.this, R.color.doodle_color_red));
            }
        });


        blackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blackButton.setSelected(true);
                redButton.setSelected(false);
                greenButton.setSelected(false);
                blueButton.setSelected(false);
                drawView.mPaint.setColor(ContextCompat.getColor(DrawActivity.this, R.color.doodle_color_black));
            }
        });


        blueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blackButton.setSelected(false);
                redButton.setSelected(false);
                greenButton.setSelected(false);
                blueButton.setSelected(true);
                drawView.mPaint.setColor(ContextCompat.getColor(DrawActivity.this, R.color.doodle_color_blue));
            }
        });

        greenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blackButton.setSelected(false);
                redButton.setSelected(false);
                greenButton.setSelected(true);
                blueButton.setSelected(false);
                drawView.mPaint.setColor(ContextCompat.getColor(DrawActivity.this, R.color.doodle_color_green));
            }
        });

        final ImageView deletebutton = (ImageView) findViewById(R.id.deleteToddle);

        deletebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainLinearLayout.removeAllViews();

                mainLinearLayout.addView(drawView);
                mainLinearLayout.addView(colourId);
                redButton.setClickable(true);
                blackButton.setClickable(true);
                greenButton.setClickable(true);
                blueButton.setClickable(true);
                blackButton.setSelected(true);
                redButton.setSelected(false);
                greenButton.setSelected(false);
                blueButton.setSelected(false);
                drawView.mPaint.setColor(Color.parseColor("#000000"));

            }
        });


        final ImageView colurSelected = (ImageView) findViewById(R.id.done);

        colurSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Bitmap imageOnView = ConvertToBitmap();


                doodleBitmap(imageOnView);
                mainLinearLayout.removeAllViews();
                mainLinearLayout.invalidate();


                drawView = new DrawView(DrawActivity.this);
                mainLinearLayout.addView(drawView);
                mainLinearLayout.addView(colourId);
                redButton.setClickable(true);
                blackButton.setClickable(true);
                greenButton.setClickable(true);
                blueButton.setClickable(true);
                blackButton.setSelected(true);
                redButton.setSelected(false);
                greenButton.setSelected(false);
                blueButton.setSelected(false);
                drawView.mPaint.setColor(Color.parseColor("#000000"));

            }
        });


        ImageView close = (ImageView) findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        //Typeface tf = AppController.getInstance().getRobotoCondensedFont();
        TextView title = (TextView) findViewById(R.id.title);
        title.setTypeface(typeFaceManager.getCircularAirBold());
        bus.register(this);
    }

    private void minimizeCallScreen(JSONObject obj) {
        try {
            Intent intent = new Intent(DrawActivity.this, ChatMessageActivity.class);

            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra("receiverUid", obj.getString("receiverUid"));
            intent.putExtra("receiverName", obj.getString("receiverName"));
            intent.putExtra("documentId", obj.getString("documentId"));

            intent.putExtra("receiverImage", obj.getString("receiverImage"));
            intent.putExtra("colorCode", obj.getString("colorCode"));

            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Subscribe
    public void getMessage(JSONObject object) {
        try {
            if (object.getString("eventName").equals("callMinimized")) {

                minimizeCallScreen(object);
            }

        } catch (
                JSONException e)

        {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


        bus.unregister(this);
    }

    @Override
    public void onBackPressed() {
try{

        if (AppController.getInstance().isActiveOnACall()) {
            if (AppController.getInstance().isCallMinimized()) {
                super.onBackPressed();
                supportFinishAfterTransition();
            }
        } else {
            super.onBackPressed();
            supportFinishAfterTransition();
        }
}catch(Exception e){e.printStackTrace();}
    }

    protected Bitmap ConvertToBitmap() {
        return drawView.getmBitmap();
    }


    public void doodleBitmap(Bitmap bitmap) {


        if (bitmap != null) {
            try {


                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                bitmap.compress(Bitmap.CompressFormat.JPEG, IMAGE_QUALITY, baos);


                byte[] br = baos.toByteArray();
                String path = createDoodleUri(br);
                try {
                    baos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                AppController.getInstance().getDbController().addWallpaper(docId, 5, path);


                try {


                    JSONObject obj = new JSONObject();
                    obj.put("eventName", "WallpaperUpdated");

                    obj.put("type", 5);
                    obj.put("wallpaperDetails", path);


                    bus.post(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (root != null) {

                    Snackbar snackbar = Snackbar.make(root, getString(R.string.WallpaperUpdated), Snackbar.LENGTH_SHORT);


                    snackbar.show();
                    View view2 = snackbar.getView();
                    TextView txtv = (TextView) view2.findViewById(com.google.android.material.R.id.snackbar_text);
                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onBackPressed();
                    }
                }, 500);

                baos = null;


                br = null;


            } catch (OutOfMemoryError e)

            {


                if (root != null) {

                    Snackbar snackbar = Snackbar.make(root, getString(R.string.WallpaperFailed), Snackbar.LENGTH_SHORT);


                    snackbar.show();
                    View view = snackbar.getView();
                    TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                }


            }


        } else

        {
            if (root != null) {
                Snackbar snackbar = Snackbar.make(root, getString(R.string.WallpaperFailed), Snackbar.LENGTH_SHORT);
                snackbar.show();
                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
            }
        }


    }

    @SuppressWarnings("all")
    private String createDoodleUri(byte[] data) {
        String name = Utilities.tsInGmt();
        name = new Utilities().gmtToEpoch(name);


        File folder = new File(Environment.getExternalStorageDirectory().getPath() + Config.CHAT_DOODLES_FOLDER);

        if (!folder.exists() && !folder.isDirectory()) {
            folder.mkdirs();
        }


        File file = new File(Environment.getExternalStorageDirectory().getPath() + Config.CHAT_DOODLES_FOLDER, name + ".jpg");

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        try {
            FileOutputStream fos = new FileOutputStream(file);

            fos.write(data);
            fos.flush();
            fos.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
        name = null;
        folder = null;

        return file.getAbsolutePath();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);


        docId = intent.getExtras().getString("documentId");
    }

}
