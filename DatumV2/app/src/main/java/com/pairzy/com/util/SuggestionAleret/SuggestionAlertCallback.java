package com.pairzy.com.util.SuggestionAleret;

/**
 * @since 1/6/2018.
 */
public interface SuggestionAlertCallback
{
    void onReport(String name, String email, String reason);
    void onSuggestion(String name, String email, String reason);
    void onQuestion(String name, String email, String reason);
    void onSuggestionCanceled();
}
