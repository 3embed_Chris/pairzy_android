package com.pairzy.com.home.MakeMatch.swipeCardModel;

import android.view.View;

interface ItemViewCallBack {
    void onViewItemCallBack(View view, int position);
}
