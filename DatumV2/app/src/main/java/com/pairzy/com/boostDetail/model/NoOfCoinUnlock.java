package com.pairzy.com.boostDetail.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 4/6/18.
 */

public class NoOfCoinUnlock {

    @SerializedName("Coin")
    @Expose
    private Integer coin;

    public Integer getCoin() {
        return coin;
    }
    public void setCoin(Integer coin) {
        this.coin = coin;
    }
}
