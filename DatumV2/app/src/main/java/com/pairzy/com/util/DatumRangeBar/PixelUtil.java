package com.pairzy.com.util.DatumRangeBar;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * @since  3/7/2018.
 * @author 3Embed.
 * @version 1.0.
 */
class PixelUtil
{
    private PixelUtil() {
    }
    static int dpToPx(Context context, int dp)
    {
        int px = Math.round(dp * getPixelScaleFactor(context));
        return px;
    }

    static int pxToDp(Context context, int px)
    {
        int dp = Math.round(px / getPixelScaleFactor(context));
        return dp;
    }

    private static float getPixelScaleFactor(Context context)
    {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT);
    }

}
