package com.pairzy.com.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ankit on 20/4/18.
 */

public class PrefData implements Serializable{

    @SerializedName("pref_id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("options")
    @Expose
    private List<String> options = null;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("iconNonSelected")
    @Expose
    private String iconNonSelected;
    @SerializedName("iconSelected")
    @Expose
    private String iconSelected;
    @SerializedName("isDone")
    @Expose
    private Boolean isDone;
    @SerializedName("selectedValues")
    @Expose
    private ArrayList<String> selectedValues = null;

    public PrefData(PrefData prefData) {
        this.id = prefData.id;
        this.title = prefData.title;
        this.label = prefData.label;
        this.options = prefData.options;
        this.type = prefData.type;
        this.priority = prefData.priority;
        this.iconNonSelected = prefData.iconNonSelected;
        this.iconSelected = prefData.iconSelected;
        this.isDone = prefData.isDone;
    }

    public void setSelectedValues(ArrayList<String> selectedValues) {
        this.selectedValues = selectedValues;
    }
    public ArrayList<String> getSelectedValues() {
        return selectedValues;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getIconNonSelected() {
        return iconNonSelected;
    }

    public void setIconNonSelected(String iconNonSelected) {
        this.iconNonSelected = iconNonSelected;
    }

    public String getIconSelected() {
        return iconSelected;
    }

    public void setIconSelected(String iconSelected) {
        this.iconSelected = iconSelected;
    }

    public Boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(Boolean isDone) {
        this.isDone = isDone;
    }

}
