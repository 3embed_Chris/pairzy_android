package com.pairzy.com.networking;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.annotation.RequiresApi;

import com.pairzy.com.AppController;

/**
 * Created by ankit on 14/9/18.
 */


@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class NetworkJobService extends JobService {
    @Override
    public boolean onStartJob(JobParameters params) {
        if(AppController.getInstance().isForeground()) {
            Intent service = new Intent(getApplicationContext(), NetworkCheckerService.class);
            getApplicationContext().startService(service);
            scheduleJob(getApplicationContext());
            return true;
        }
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Intent service = new Intent(getApplicationContext(), NetworkCheckerService.class);
        getApplicationContext().stopService(service);
        return true;
    }

    public static void scheduleJob(Context context) {
        ComponentName serviceComponent = new ComponentName(context, NetworkJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
        builder.setMinimumLatency(1 * 1000); // wait at least
        builder.setOverrideDeadline(3 * 1000); // maximum delay
        //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
        //builder.setRequiresDeviceIdle(true); // device should be idle
        //builder.setRequiresCharging(false); // we don't care if the device is charging or not
        try {
            JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
            jobScheduler.schedule(builder.build());
        }catch (Exception e){
        }

    }

    public static void cancelNetworkJob(Context context) {
        try {
            JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
            jobScheduler.cancel(0);
        }catch (Exception e){}

    }

}

