package com.pairzy.com.coinWallet.coinOut.model;

import com.pairzy.com.coinWallet.CoinWalletUtil;
import com.pairzy.com.coinWallet.Model.AllCoinAdapter;
import com.pairzy.com.coinWallet.Model.CoinPojo;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

/**
 *<h>OutCoinModel</h>
 * <p></p>
 *@author 3Embed.
 *@since 30/5/18.
 */

public class OutCoinModel {

    @Named(CoinWalletUtil.COIN_OUT_LIST)
    @Inject
    ArrayList<CoinPojo> arrayList;
    @Inject
    AllCoinAdapter allCoinAdapter;

    @Inject
    public OutCoinModel() {
    }

    public boolean isCoinHistoryEmpty(){
        return arrayList.size() == 0;
    }

    public void notifyAdapter(){
        allCoinAdapter.notifyDataSetChanged();
    }
}
