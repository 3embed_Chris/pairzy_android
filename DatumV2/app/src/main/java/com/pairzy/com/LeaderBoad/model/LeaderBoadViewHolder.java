package com.pairzy.com.LeaderBoad.model;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LeaderBoadViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvLeaderBoadIndex)
    TextView tvIndex;

    @BindView(R.id.ivLeaderBoadIndex)
    ImageView ivIndex;

    @BindView(R.id.tvLeaderBoadProfile)
    TextView tvProfile;

    @BindView(R.id.sdvLeaderBoadProfile)
    SimpleDraweeView sdvProfile;

    @BindView(R.id.tvLeaderBoadCount)
    TextView count;

    @BindView(R.id.tvLeaderBoadCoin)
    TextView coin;


    private TypeFaceManager typeFaceManager;

    public LeaderBoadViewHolder(@NonNull View itemView, TypeFaceManager typeFaceManager) {
        super(itemView);
        this.typeFaceManager=typeFaceManager;
        ButterKnife.bind(this,itemView);
        initFont();
    }

    private void initFont() {
    tvIndex.setTypeface(typeFaceManager.getCircularAirBook());
    tvProfile.setTypeface(typeFaceManager.getCircularAirBook());
    coin.setTypeface(typeFaceManager.getCircularAirBook());
    count.setTypeface(typeFaceManager.getCircularAirBook());
    }
}
