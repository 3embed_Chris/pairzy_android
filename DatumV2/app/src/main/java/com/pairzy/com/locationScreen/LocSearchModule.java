package com.pairzy.com.locationScreen;
import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h>LocationSearchModule interface</h>
 * @author 3Embed.
 * @since 3/4/18.
 * @version 1.0.
 */

@Module
public interface LocSearchModule {

    @ActivityScoped
    @Binds
    LocationSearchContract.Presenter bindsLocationSearchPresenter(LocationSearchPresenter presenter);

    @ActivityScoped
    @Binds
    LocationSearchContract.View bindsLocationSearchView(LocationSearchActivity activity);

    @ActivityScoped
    @Binds
    Activity provideActivity(LocationSearchActivity activity);
}
