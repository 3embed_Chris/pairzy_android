package com.pairzy.com.boostDetail.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ankit on 1/6/18.
 */

public class CoinPlan implements Serializable{

    @SerializedName("planId")
    @Expose
    private String _id;

    @SerializedName("planName")
    @Expose
    private String planName;

    @SerializedName("currency")
    @Expose
    private String currency;

    @SerializedName("noOfCoinUnlock")
    @Expose
    private NoOfCoinUnlock noOfCoinUnlock;

    @SerializedName("currencySymbole")
    @Expose
    private String currencySymbole;

    @SerializedName("cost")
    @Expose
    private Double cost;

    @SerializedName("actualId")
    @Expose
    private String actualId;

    @SerializedName("actualIdForAndroid")
    @Expose
    private String actualIdForAndroid;

    private String price_text;

    public String getActualIdForAndroid() {
        return actualIdForAndroid;
    }

    public void setActualIdForAndroid(String actualIdForAndroid) {
        this.actualIdForAndroid = actualIdForAndroid;
    }

    public void setPrice_text(String price_text) {
        this.price_text = price_text;
    }

    public String getPrice_text() {
        return price_text;
    }

    public void setActualId(String actualId) {
        this.actualId = actualId;
    }

    public String getActualId() {
        return actualId;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public com.pairzy.com.boostDetail.model.NoOfCoinUnlock getNoOfCoinUnlock() {
        return noOfCoinUnlock;
    }

    public void setNoOfCoinUnlock(com.pairzy.com.boostDetail.model.NoOfCoinUnlock noOfCoinUnlock) {
        this.noOfCoinUnlock = noOfCoinUnlock;
    }

    public String getCurrencySymbole() {
        return currencySymbole;
    }

    public void setCurrencySymbole(String currencySymbole) {
        this.currencySymbole = currencySymbole;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

}
