package com.pairzy.com.fbRegister.ProfileVideo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.CloudManager.UploadManager;
import com.pairzy.com.util.CloudManager.UploaderCallback;
import com.pairzy.com.util.FileUtil.AppFileManger;
import com.pairzy.com.util.MediaBottomSelector;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.videocompressor.com.DataModel.CompressedData;
import com.videocompressor.com.RxCompressObservable;
import com.videocompressor.com.VideoCompressor;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h2>FbProfileVideoFrgPresenter</h2>
 * @since  2/19/2018.
 * @version 1.0.
 * @author 3Embed.
 */
public class FbProfileVideoFrgPresenter implements FbProfileVideoContact.Presenter,App_permission.Permission_Callback, MediaBottomSelector.Callback
{
    private  final String GALLERY="gallery";
    private  final String CAMERA="camera";
    private File temp_file=null;
    private File currentVideo=null;
    @Inject
    UploadManager uploadManager;
    @Inject
    VideoCompressor videoCompressor;
    @Inject
    LoadingProgress progress;
    @Inject
    FbProfileVideoModel model;
    @Inject
    AppFileManger appFileManger;
    @Inject
    Utility utility;
    @Inject
    Activity activity;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    NetworkService service;

    private CompositeDisposable compositeDisposable;
    @Inject
    FbProfileVideoFrgPresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }

    private FbProfileVideoFrg profilePicFrg;
    @Inject
    App_permission app_permission;

    @Inject
    MediaBottomSelector mediaBottomSelector;

    private FbProfileVideoContact.View view;

    public void takeView(Object view)
    {
        this.profilePicFrg= (FbProfileVideoFrg) view;
        this.view= (FbProfileVideoContact.View) view;
    }

    @Override
    public void dropView()
    {
        view=null;
    }

    @Override
    public String getRecentTempVideo()
    {
        return temp_file.getPath();
    }

    @Override
    public String getActualVIdeo()
    {
        return currentVideo.getPath();
    }

    @Override
    public void upDateToGallery()
    {
        if(temp_file==null)
            return;
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(temp_file.getPath());
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        activity.sendBroadcast(mediaScanIntent);
    }

    @Override
    public void openChooser()
    {
        temp_file=null;
        mediaBottomSelector.showBottomSheet(this);
    }

    @Override
    public void showError(String message)
    {
        if(view!=null)
        {
            view.showError(message);
        }
    }

    @Override
    public void uploadVideo(String filePath,boolean firstTimeUpload)
    {
        if(networkStateHolder.isConnected())
        {
            progress.show();
            uploadManager.uploadVideoFile(filePath, new UploaderCallback()
            {
                @Override
                public void onSuccess(String main_url, String thumb_nail,int height,int width) {
                    progress.cancel();
                    if (view != null)
                        view.mediaUploaded(main_url, thumb_nail);
                }
                @Override
                public void onError(String error) {
                    if(error.equals(UploadManager.UN_CAUGHT_ERROR) && firstTimeUpload){
                        getCloudinaryDetails(filePath);
                    }else {
                        progress.cancel();
                        if (view != null) {
                            view.showError(error);
                        }
                    }
                }
            });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void getCloudinaryDetails(String filePath) {
        service.getCloudinaryDetail()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(Response<ResponseBody> responseBodyResponse) {
                        progress.cancel();
                        if(responseBodyResponse.code() == 200){
                            try {
                                model.parseClodinaryDetail(responseBodyResponse.body().string());
                                uploadVideo(filePath,false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else if(responseBodyResponse.code() == 401){
                            AppController.getInstance().appLogout();
                        }else{
                            if(view != null)
                                view.showError(model.getError(responseBodyResponse));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        progress.cancel();
                    }
                });
    }

    @Override
    public void compressedVideo(String file_path)
    {
        progress.show();
        Observer<CompressedData> observer = new Observer<CompressedData>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(CompressedData value)
            {
                progress.cancel();
                try
                {
                    model.deleteFile(temp_file);
                    currentVideo=new File(value.getPath());
                    if(view!=null)
                        view.onVideoCompressed(currentVideo.getPath());
                    if(view != null)
                        view.showChangeButton(true);
                }catch (Exception e)
                {
                    e.printStackTrace();
                    if(view!=null)
                        view.imageCollectError();
                }
            }
            @Override
            public void onError(Throwable e)
            {
                progress.cancel();
                e.printStackTrace();
                if(view!=null)
                    view.imageCollectError();
            }

            @Override
            public void onComplete() {}
        };
        RxCompressObservable observable=videoCompressor.compressVideo(file_path);
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag)
    {
        if(isAllGranted&&tag.equals(CAMERA))
        {
            if(view!=null)
            {
                try {
                    temp_file = appFileManger.getVideoFile();
                    view.openCamera(utility.getUri_Path(temp_file));
                }catch (Exception e){
                    if(view != null)
                        view.showError(e.getMessage());
                }
            }
        }else if(isAllGranted&&tag.equals(GALLERY))
        {
            if(view!=null)
                view.openGallery();
        }
    }


    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag)
    {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        if(tag.equals(GALLERY))
        {
            app_permission.show_Alert_Permission(activity.getString(R.string.video_access_text),activity.getString(R.string.gallery_video_subtitle),
                    activity.getString(R.string.gallery_video_message),stringArray);
        }else if(tag.equals(CAMERA))
        {
            app_permission.show_Alert_Permission(activity.getString(R.string.camera_video_text),activity.getString(R.string.camera_video_subtitle),
                    activity.getString(R.string.camera_video_message),stringArray);
        }
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag)
    {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }


    @Override
    public void onPermissionPermanent_Denied(String tag,boolean parmanent)
    {
        if(parmanent)
        {
            if(tag.equals(GALLERY))
            {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.video_denied_text),activity.getString(R.string.gallery_denied_video_subtitle),
                        activity.getString(R.string.gallery_denied_video_message));
            }else if(tag.equals(CAMERA))
            {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.camera_denied_video_text),activity.getString(R.string.camera_denied_video_subtitle),
                        activity.getString(R.string.camera_denied_video_message));
            }
        }
    }


    @Override
    public void onCamera()
    {
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.CAMERA);
        app_permission.getPermission_for_Sup_v4Fragment(CAMERA,permissions,profilePicFrg,this);
    }


    @Override
    public void onGallery()
    {
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        app_permission.getPermission_for_Sup_v4Fragment(GALLERY,permissions,profilePicFrg,this);
    }

}
