package com.pairzy.com.home.Discover.Model;

public class UserAssetData
{
    private String id;
    private boolean isError;
    public UserAssetData(String id,boolean isError)
    {
        this.id=id;
        this.isError=isError;
    }

    public String getId() {
        return id;
    }

    public boolean isError() {
        return isError;
    }
}
