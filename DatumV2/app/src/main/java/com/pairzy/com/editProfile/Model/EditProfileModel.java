package com.pairzy.com.editProfile.Model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.model.cloudinaryDetail.CloudinaryData;
import com.pairzy.com.data.model.cloudinaryDetail.CloudinaryDetailResponse;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.CloudManager.UploadManager;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * <h>EditProfileModel class</h>
 * @author 3Embed.
 * @since  23/4/18.
 * @version 1.0.
 */

public class EditProfileModel  extends BaseModel {

    @Named("PIC_LIST")
    @Inject
    ArrayList<ProfilePicture> profilePictureList;
    @Inject
    public ProfilePhotoAdapter adapter;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Utility utility;
    @Inject
    UploadManager uploadManager;

    EditProfileData profileNewData, profileOldData;
    private int maxPic = 5;

    @Inject
    public EditProfileModel(){
        profileOldData = new EditProfileData();
        profileNewData = new EditProfileData();
    }

    public void keepOldVideo(String videoUrl , String videoThumb){
        profileOldData.setVideoUrl(videoUrl);
        profileOldData.setVideoThumb(videoThumb);
        profileNewData.setVideoUrl(videoUrl);
        profileNewData.setVideoThumb(videoThumb);
    }

    /*
    *Deleting the file */
    public void deleteFile(File file)
    {
        try
        {
            if(file.exists())
            {
                file.delete();
            }
        }catch (Exception e){}
    }

    public void processThumbImage(String filePath, String thumbPth, ImageView mImageView) throws Exception
    {
        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(filePath, MediaStore.Video.Thumbnails.MINI_KIND);
        File file=saveBitmap(thumb,thumbPth);
        String mCurrentPhotoPath=file.getPath();
        mImageView.setVisibility(View.VISIBLE);
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        mImageView.setImageBitmap(bitmap);
    }

    /*
    * Saving the file d*/
    private File saveBitmap(Bitmap bmp,String destination)
    {
        OutputStream outStream = null;
        File file = new File(destination);
        try {
            outStream = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG,100, outStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally
        {
            if(outStream!=null)
            {
                try
                {
                    outStream.flush();
                    outStream.close();
                }catch (Exception e) {}
            }
        }
        return file;
    }


    public  void addToAdapter(int viewType,String url,int atPosition){
        if(profilePictureList.size() > atPosition){
            ProfilePicture profilePicture = profilePictureList.get(atPosition);
            profilePicture.setType(viewType);
            profilePicture.setProfilePic(url);
            profilePictureList.set(atPosition,profilePicture);
        }
        else{
            ProfilePicture profilePicture = new ProfilePicture();
            profilePicture.setType(viewType);
            profilePicture.setProfilePic(url);
            profilePictureList.add(atPosition,profilePicture);

        }
        if(viewType==ProfilePicture.PROFILE_PIC) {
            profileNewData.setProfilePicList(profilePictureList);
            appendEmptybox();
        }
        adapter.setData(profilePictureList);
    }

    public void parsePictures(String profilePic, ArrayList<String> othersPic) {
        if (profilePic != null && !profilePic.isEmpty()) {
            ProfilePicture profilePicture = new ProfilePicture();
            profilePicture.setProfilePic(profilePic);
            profilePicture.setAsProfilePic(true);
            profilePictureList.add(profilePicture);
        }
        if (othersPic != null && othersPic.size() > 0) {
            for(String pic: othersPic){
                ProfilePicture profilePicture = new ProfilePicture();
                profilePicture.setProfilePic(pic);
                profilePicture.setAsProfilePic(false);
                profilePictureList.add(profilePicture);
            }
        }
        ArrayList<ProfilePicture> profileList = new ArrayList<>();
        profileList.addAll(profilePictureList);
        profileOldData.setProfilePicList(profileList);
        appendEmptybox();
        adapter.setData(profilePictureList);
    }

    /*
     *append empty box at the end if array size < maxSize.
     */
    public void appendEmptybox(){
        if(profilePictureList.size() < maxPic) {
            ProfilePicture profilePicture = new ProfilePicture();
            profilePicture.setType(ProfilePicture.PROFILE_PIC_EMPTY);
            profilePictureList.add(profilePicture);
        }
    }

    /*
     *delete profile pic from adapter list at position @param pos.
     */
    public ProfilePicture deletePicWith(int pos){
        ProfilePicture profilePic = null;
        if(profilePictureList.size() > pos && profilePictureList.size() > 2){
            profilePic = profilePictureList.remove(pos);
        }
        if(profilePictureList.size() > 0){
            ProfilePicture profilePicture = profilePictureList.get(profilePictureList.size()-1);
            if(profilePicture.getType() == ProfilePicture.PROFILE_PIC)
                appendEmptybox();
        }
        adapter.setData(profilePictureList);
        return profilePic;
    }

    /*
     *save profile video to preference manager.
     */
    public void saveProfileVideo(String currentVideoUrl,String currentThumbUrl,int height,int width) {
        if (currentVideoUrl != null) {
            dataSource.setUserVideo(currentVideoUrl);
            dataSource.setUserVideoHeight(height);
            dataSource.setUserVideoWidth(width);
            profileNewData.setVideoUrl(currentVideoUrl);
        }
        if (currentThumbUrl != null) {
            dataSource.setUserVideoThumbnail(currentThumbUrl);
            profileNewData.setVideoUrl(currentThumbUrl);
        }
    }

    /*
     *save profile pic and other image list to preference manager.
     **/
    public void saveProfilePicture() {
        if(!profilePictureList.isEmpty()){
            String pic;
            ArrayList<String> otherImage = new ArrayList<>();
            ProfilePicture profilePic;
            ProfilePicture profilePicture = null;
            for(int i =0;i< profilePictureList.size();i++){
                profilePic= profilePictureList.get(i);
                pic = profilePic.getProfilePic();

                if(profilePic.getType()==ProfilePicture.PROFILE_PIC && profilePic.isProfilePic()) {
                    profilePicture = pic == null ? null : profilePic;
                }
                else if(profilePic.getType()==ProfilePicture.PROFILE_PIC && !profilePic.isProfilePic()) {
                    otherImage.add(pic == null ? "" : pic);
                }
            }
            if(profilePicture != null)
                dataSource.setProfilePicture(profilePicture.getProfilePic());

            if(!otherImage.isEmpty())
                dataSource.setUserOtherImages(otherImage);
            else{
                dataSource.setUserOtherImages(new ArrayList<>());
            }
        }
    }

    public boolean isProfileChanged(){
        profileNewData.setProfilePicList(profilePictureList);
        if(!profileNewData.getVideoUrl().equals(profileOldData.getVideoUrl())){
            return true;
        }
        if(!profileNewData.getVideoUrl().equals(profileOldData.getVideoUrl())){
            return true;
        }
        if(!profileNewData.getVideoThumb().equals(profileOldData.getVideoThumb())){
            return true;
        }

        //get rid of empty box
        if(profileNewData.getProfilePicList().size() < 5){
            profileNewData.getProfilePicList().remove(profileNewData.getProfilePicList().size()-1);
        }

        if(profileNewData.getProfilePicList().size() != profileOldData.getProfilePicList().size()){
            return true;
        }

        ArrayList<ProfilePicture> newPicList, oldPicList;
        newPicList = profileNewData.getProfilePicList();
        oldPicList = profileOldData.getProfilePicList();

        for(int i =0 ;i < oldPicList.size();i++){
            ProfilePicture profilePicture_new = newPicList.get(i);
            ProfilePicture profilePicture_old = oldPicList.get(i);
            if(profilePicture_new.getProfilePic() ==null){
                profilePicture_new.setProfilePic("");
            }
            if(profilePicture_old.getProfilePic()== null) {
                profilePicture_old.setProfilePic("");
            }
            if(!profilePicture_new.getProfilePic().equalsIgnoreCase(profilePicture_old.getProfilePic())) {
                return true;
            }

        }

        String oldProfilePicUrl ="";
        for(int i =0 ;i < oldPicList.size();i++){
            ProfilePicture profilePicture_old = oldPicList.get(i);
            if(profilePicture_old.isProfilePic())
                oldProfilePicUrl = profilePicture_old.getProfilePic();
        }
        String newProfilePicUrl ="";
        for(int i =0 ;i < newPicList.size();i++){
            ProfilePicture profilePicture_new = newPicList.get(i);
            if(profilePicture_new.isProfilePic())
                newProfilePicUrl = profilePicture_new.getProfilePic();
        }
        if(oldProfilePicUrl ==null){
            oldProfilePicUrl = "";
        }
        if(newProfilePicUrl == null) {
            newProfilePicUrl = "";
        }

        if(!oldProfilePicUrl.equalsIgnoreCase(newProfilePicUrl))
            return true;
        return false;
    }

    public void addMediaToAdapter(String filePath,int curAdapterPosition) {
        if(curAdapterPosition < profilePictureList.size()){
            profilePictureList.get(curAdapterPosition).setProfilePicPath(filePath);
            adapter.setData(profilePictureList);
        }
    }

    public boolean makeProfilePic(int pos) {
        try{
            if(pos < profilePictureList.size()){
                for(ProfilePicture profilePicture: profilePictureList){
                    profilePicture.setAsProfilePic(false);
                }
                profilePictureList.get(pos).setAsProfilePic(true);
                adapter.notifyDataSetChanged();
                return profilePictureList.get(pos).isProfilePic();
            }
        }catch (Exception e){
        }
        return false;
    }

    public boolean isProfilePicture(int pos) {
        try{
            if(pos < profilePictureList.size()){
                return profilePictureList.get(pos).isProfilePic();
            }
        }catch (Exception e){
        }
        return false;
    }

    public void parseClodinaryDetail(String response) throws DataParsingException{
        try{
            CloudinaryDetailResponse cloudinaryDetailResponse =
                    utility.getGson().fromJson(response,CloudinaryDetailResponse.class);
            CloudinaryData cloudinaryData = cloudinaryDetailResponse.getCloudinaryData();

            dataSource.setCloudName(cloudinaryData.getCloudName());
            dataSource.setCloudinaryApiKey(cloudinaryData.getApiKey());
            dataSource.setClodinaryApiSecret(cloudinaryData.getApiSecret());

            reinitCloudnaryUpload();
        }catch (Exception e){
            throw new DataParsingException(e.getMessage());
        }
    }

    private void reinitCloudnaryUpload() {
        uploadManager.setNewCreds(true,
                dataSource.getCloudName(),
                dataSource.getCloudinaryApiKey(),
                dataSource.getCloudinaryApiSecret());
    }
}
