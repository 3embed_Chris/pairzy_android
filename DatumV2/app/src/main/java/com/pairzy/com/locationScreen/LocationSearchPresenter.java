package com.pairzy.com.locationScreen;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.R;
import com.pairzy.com.data.model.fourSq.FourResponse;
import com.pairzy.com.data.model.fourSq.Venue;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.locationScreen.locationMap.CustomLocActivity;
import com.pairzy.com.locationScreen.model.LocationHolder;
import com.pairzy.com.locationScreen.model.LocationSearchModel;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.planDate.SelectedLocationHolder;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.LocationProvider.LocationApiCallback;
import com.pairzy.com.util.LocationProvider.LocationApiManager;
import com.pairzy.com.util.LocationProvider.Location_service;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * <h>GoogleLocSearchPresenter class</h>
 * @author 3Embed.
 * @since 9/5/18.
 * @version 1.0.
 */

public class LocationSearchPresenter implements LocationSearchContract.Presenter,App_permission.Permission_Callback,Location_service.GetLocationListener,ItemActionCallBack{

    private  final String LOCATION_TAG="location_tag";

    @Inject
    LocationSearchContract.View view;
    @Inject
    App_permission app_permission;
    @Inject
    Activity activity;
    @Inject
    Location_service location_service;
    @Inject
    LocationApiManager locationApiManager;
    @Inject
    NetworkService service;
    @Inject
    LocationSearchModel model;
    @Inject
    LinearLayoutManager linearLayoutManager;
    @Inject
    NetworkStateHolder networkStateHolder;

    LocationHolder locationHolder;
    private String query = "";
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int page = 0;
    private int PAGE_SIZE = 15;
    private CompositeDisposable compositeDisposable;

    @Inject
    LocationSearchPresenter(){
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void init() {
        if(view != null){
            view.applyFont();
        }
    }

    @Override
    public void loadPlaces() {
        query = "";
        model.clearAddressList();
        page =0;
        isLoading = false;
        isLastPage = false;
        if(locationHolder == null)
            askLocationPermission();
        else
            callFoursquareApi();
    }

    @Override
    public void searchPlace(String newText) {
        model.clearAddressList();
        query = newText==null?"":newText;
        if(locationHolder == null)
            askLocationPermission();
        else
            callFoursquareApi();
    }

    @Override
    public void handleOnScroll(RecyclerView recyclerView, int dx, int dy) {
        int visibleItemCount = linearLayoutManager.getChildCount();
        int totalItemCount = linearLayoutManager.getItemCount();
        int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
        if (!isLoading && !isLastPage) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                    && firstVisibleItemPosition >= 0
                    && totalItemCount >= PAGE_SIZE) {
                page++;
                //loadData(page *PAGE_SIZE,PAGE_SIZE);
                callFoursquareApi();
            }
        }
    }


    private void callFoursquareApi(){

        if(networkStateHolder.isConnected()) {
            if (view != null) {
                if (page == 0)
                    view.showLoading();
                else {
                    model.showLoadingItem();
                    model.notifyAdapter();
                }
            }
            isLoading = true;
            service.getNearByPlaces(AppConfig.Foursquare.CLIENT_ID, AppConfig.Foursquare.CLIENT_SECRET,
                    20180323, locationHolder.getLatitude() + "," + locationHolder.getLongitude(),
                    0, 50000, query, page * PAGE_SIZE, PAGE_SIZE)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<FourResponse>>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<FourResponse> value) {
                            isLoading = false;
                            if (value.code() == 200) {
                                model.removeLoadingItem();
                                FourResponse fourResponse = value.body();
                                int responseSize = 0;
                                if (fourResponse != null) {
                                    responseSize = model.getResonseSize(fourResponse);
                                    model.filterResponse(fourResponse);
                                }
                                if (responseSize == 0 || responseSize < PAGE_SIZE) {
                                    isLastPage = true;
                                } else {
                                    isLastPage = false;
                                }
                                if (view != null)
                                    view.showData();
                            } else {
                                if (isListEmpty())
                                    if (view != null)
                                        view.showNetworkError(activity.getString(R.string.api_server_error));
                                model.showLoadingError();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            isLoading = false;
                            isLastPage = false;
                            if (view != null) {
                                view.showError(e.getMessage());
                                if (isListEmpty())
                                    if (view != null)
                                        view.showNetworkError(activity.getString(R.string.failed_to_get_near_by_places));
                                model.showLoadingError();
                                model.notifyAdapter();
                            }
                            e.printStackTrace();
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }else {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }
    /*
     * getting user location from GPS
     * */
    private void getUserLocationFromService()
    {
        location_service.setListener(this);
        location_service.checkLocationSettings();
    }

    public void askLocationPermission() {
        if(view != null)
            view.showLoading();
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.LOCATION);
        app_permission.getPermission(LOCATION_TAG,permissions,this);
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        if (tag.equalsIgnoreCase(LOCATION_TAG) && isAllGranted) {
            getUserLocationFromService();
        }
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        if(tag.equals(LOCATION_TAG))
        {
            String[] stringArray = deniedPermission.toArray(new String[0]);
            app_permission.show_Alert_Permission(activity.getString(R.string.location_access_text),activity.getString(R.string.location_acess_subtitle),
                    activity.getString(R.string.location_acess_message),stringArray);
        }
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean parmanent) {
        if(tag.equals(LOCATION_TAG))
        {
            getUserLocationFromApi();
        }
    }


    /*
    * getting user location from link*/
    private void getUserLocationFromApi()
    {
        locationApiManager.getLocation(new LocationApiCallback()
        {
            @Override
            public void onSuccess(Double lat, Double lng, double accuracy)
            {
                //TODO ready foursquare api.
                LocationHolder holder = new LocationHolder();
                holder.setLatitude(lat);
                holder.setLongitude(lng);
                locationHolder = holder;
                callFoursquareApi();
            }

            @Override
            public void onError(String error)
            {
                if(view!=null)
                    view.showError(error);
                if(view != null)
                    view.showNetworkError(error);
            }
        });
    }


    @Override
    public void updateLocation(Location location) {
        location_service.stop_Location_Update();
        LocationHolder holder = new LocationHolder();
        holder.setLatitude(location.getLatitude());
        holder.setLongitude(location.getLongitude());
        locationHolder = holder;
        callFoursquareApi();
    }

    @Override
    public void location_Error(String error) {
        getUserLocationFromApi();
    }

    @Override
    public void dispose(){
        compositeDisposable.clear();
        this.view = null;
    }

    @Override
    public void parseLocation(int requestCode, int resultCode, Intent intent) {
        if(requestCode == CustomLocActivity.CHOOSE_LOC_REQ_CODE && resultCode == -1){
            if(view != null){
                view.setData(intent);
            }
        }
    }

    public boolean isListEmpty() {
        return model.getListSize() == 0;
    }

    @Override
    public void onClick(int id, int position) {
        switch (id){
            case R.id.retry_text:
                callFoursquareApi();
                break;
            case R.id.item:
                Venue venue = model.getItem(position);
                if(view != null && venue != null) {
                    SelectedLocationHolder locationholder = new SelectedLocationHolder();
                    locationholder.setLocationTitle(venue.getName());
                    locationholder.setLocationDetail(venue.getLocation().getAddress());
                    locationholder.setLatitude(venue.getLocation().getLat());
                    locationholder.setLongitude(venue.getLocation().getLng());
                    Intent data = new Intent();
                    data.putExtra("location_holder",locationholder);
                    view.setData(data);
                }
                break;
        }
    }
}
