package com.pairzy.com.home;
import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.pairzy.com.R;
/**
 * @since  3/21/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class AnimationHandler
{
    private Context context;
    AnimationHandler(Context context)
    {
        this.context=context;
    }

    public  Animation getSlowBlinkingAnimation()
    {
        return AnimationUtils.loadAnimation(context, R.anim.heart_blink_slow);
    }

    public  Animation getBitSlowBlinkingAnimation()
    {
        return AnimationUtils.loadAnimation(context, R.anim.heart_blink_bit_slow);
    }

    public  Animation getFastBlinkingAnimation()
    {
        return AnimationUtils.loadAnimation(context, R.anim.heart_blink_fast);
    }

    public Animation getPlaceAnimation()
    {
        return AnimationUtils.loadAnimation(context,R.anim.place_shaking_anim);
    }


}
