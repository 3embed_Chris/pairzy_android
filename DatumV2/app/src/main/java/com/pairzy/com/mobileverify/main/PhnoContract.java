package com.pairzy.com.mobileverify.main;

import android.view.View;
import com.pairzy.com.BasePresenter;

import dagger.android.support.DaggerFragment;

/**
 * <h2>PhnoContract</h2>
 * <p>
 * Contains @{@link PhnoFragment}'s @{@link ViewIml} and @{@link Presenter}
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 06/01/2018.
 **/
public interface PhnoContract
{
    interface ViewIml
    {
        void initView(View view);
        /*
        * init data.*/
        void intiData();
        /*
        * Opening the new fragment*/
        void openVerifyPage(DaggerFragment fragment, boolean isBackrequired);
        /**
         * <p>Displays message</p>
         *
         * @param message : String message
         */
        void showMessage(String message);
    }

    interface Presenter extends BasePresenter
    {
       void validateNumber(String code, String number);
    }
}
