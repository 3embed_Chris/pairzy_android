package com.pairzy.com.home.AppSetting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.androidinsta.com.InstagramManger;
import com.pairzy.com.MyProfile.MyProfilePage;
import com.pairzy.com.MySearchPreference.MySearchPref;
import com.pairzy.com.R;
import com.pairzy.com.appSettings.AppSettingsContract;
import com.pairzy.com.settings.SettingsActivity;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.Locale;

import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * <h2>AppSettingFrag</h2>
 * <P>
 * A simple {@link Fragment} subclass.
 * </P>
 * @author 3Embed.
 * @version 1.0.
 */
@ActivityScoped
public class AppSettingFrag extends DaggerFragment implements AppSettingContract.View
{
    @Inject
    Activity activity;
    @Inject
    AppSettingsContract.Presenter AppPresenter;
    @Inject
    TypeFaceManager typeFaceManager;
    //    @Inject
//    ProfileDataChangeHolder profileDataChangeHolder;
    @Inject
    Utility utility;

    @BindView(R.id.user_name)
    TextView user_name;
    @BindView(R.id.user_school)
    TextView user_school;
    @BindView(R.id.user_profile_pic)
    SimpleDraweeView user_profile_pic;
    @BindView(R.id.my_preference_text)
    TextView my_preference_text;
    @BindView(R.id.invite_friend_text)
    TextView invite_friend_text;
    @BindView(R.id.AppSettings_text)
    TextView appSettings_text;
    @BindView(R.id.needhelp_text)
    TextView tvNeedHelp;
    @BindView(R.id.datumplus_text)
    TextView tvDatumPlus;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    InstagramManger instagramManger;

    @BindView(R.id.parentCoordinatorLayout)
    CoordinatorLayout parentLayout;


    private Unbinder unbinder;

    @Inject
    AppSettingPresenter presenter;

    public AppSettingFrag() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_app_setting_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder=ButterKnife.bind(this,view);
        presenter.takeView(this);
        initUI();
        initData();
        presenter.initSettingsDataChangeObserver();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void initDataChange() {
        showMessage(activity.getString(R.string.profile_updated_successful_msg));
        user_profile_pic.setImageURI(dataSource.getProfilePicture());

        String age = "";
        if(!TextUtils.isEmpty(dataSource.getBirthDate()))
            age = String.valueOf(utility.getAgeFromDob(Double.parseDouble(dataSource.getBirthDate())));
        else
            age = dataSource.getBirthDate();

        if(!TextUtils.isEmpty(age)){
            user_name.setText(String.format(Locale.ENGLISH,"%s, %s",dataSource.getName(),age));
        }
        else{
            user_name.setText(dataSource.getName());
        }

        String string ="";
        if(!TextUtils.isEmpty(dataSource.getMyWorkPlace())){
            string = dataSource.getMyWorkPlace();
        }
        else{
            if(!TextUtils.isEmpty(dataSource.getMyEducation())){
                string = dataSource.getMyEducation();
            }
            else if(!TextUtils.isEmpty(dataSource.getGender())){
                string = dataSource.getGender();
            }
        }
        user_school.setText(string);
    }

    private void showMessage(String string) {
    }

    @Override
    public void onDestroy() {
        presenter.dropView();
        unbinder.unbind();
        super.onDestroy();
    }

    /*
     * initialization of the xml content.*/
    private void initUI()
    {
        user_name.setTypeface(typeFaceManager.getCircularAirBold());
        user_school.setTypeface(typeFaceManager.getCircularAirLight());
        my_preference_text.setTypeface(typeFaceManager.getCircularAirLight());
        invite_friend_text.setTypeface(typeFaceManager.getCircularAirLight());
        appSettings_text.setTypeface(typeFaceManager.getCircularAirLight());
        tvNeedHelp.setTypeface(typeFaceManager.getCircularAirLight());
        tvDatumPlus.setTypeface(typeFaceManager.getCircularAirLight());
    }

    /*
     * intialization details.*/
    private void initData()
    {
        user_profile_pic.setImageURI(dataSource.getProfilePicture());
        //user_name.setText(String.format(Locale.ENGLISH,"%s, %d",dataSource.getName(),dataSource.getUserAge()));
        String age = "";
        try {
            if(!TextUtils.isEmpty(dataSource.getBirthDate()) && !dataSource.getBirthDate().equalsIgnoreCase("0"))
                age = String.valueOf(utility.getAgeFromDob(Double.parseDouble(dataSource.getBirthDate())));
            else
                age = dataSource.getBirthDate();
        }catch (Exception e){}
        if(!TextUtils.isEmpty(age) && !age.equalsIgnoreCase("0")){
            user_name.setText(String.format(Locale.ENGLISH,"%s, %s",dataSource.getName(),age));
        }
        else{
            user_name.setText(dataSource.getName());
        }
        String string ="";
        if(!TextUtils.isEmpty(dataSource.getMyWorkPlace())){
            string = dataSource.getMyWorkPlace();
        }
        else{
            if(!TextUtils.isEmpty(dataSource.getMyEducation())){
                string = dataSource.getMyEducation();
            }
            else if(!TextUtils.isEmpty(dataSource.getGender())){
                string = dataSource.getGender();
            }
        }
        user_school.setText(string);
    }


    @OnClick(R.id.parentCoordinatorLayout)
    void onParentClicked(){}


    @OnClick(R.id.datumplus)
    void onDatumPlus()
    {
        presenter.showBoostDialog();
    }
    @OnClick(R.id.needhelp)
    void onHelpClicked()
    {
        presenter.showSuggestionAlert();
    }

    @OnClick(R.id.invite_friend)
    void inviteFriend(){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT,getString(R.string.invite_msg)+" "+getString(R.string.datum_play_store));
        intent.setType("text/plain");
        Intent chooser = Intent.createChooser(intent, getString(R.string.select_app_title));
        startActivity(chooser);
    }

    @OnClick({R.id.user_profile_pic,R.id.user_name,R.id.user_school})
    void onOpenProfile()
    {
        Intent profile=new Intent(activity, MyProfilePage.class);
        profile.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        activity.startActivityForResult(profile,AppConfig.SEARCH_REQUEST);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }


    @OnClick(R.id.my_preference)
    void openPreference()
    {
        Intent intent = new Intent(activity,MySearchPref.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        activity.startActivityForResult(intent, AppConfig.SEARCH_REQUEST);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }


    @OnClick(R.id.AppSettings)
    public void openAppSettings(){
        Intent intent = new Intent(activity, SettingsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @Override
    public void openBootDialog()
    {
        AppPresenter.openBoostDialog();
    }


    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @OnClick(R.id.back_button)
    public void back(){
        activity.onBackPressed();
    }
}
