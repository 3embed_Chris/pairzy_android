package com.pairzy.com.UserPreference;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatDelegate;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.UserPreference.EditInputFrg.UserInputFrg;
import com.pairzy.com.UserPreference.Model.PreferenceItem;
import com.pairzy.com.UserPreference.Model.PrefranceDataDetails;
import com.pairzy.com.UserPreference.PreviewMessage.PreviewFrg;
import com.pairzy.com.UserPreference.listScrollerFrg.ListdataFrg;
import com.pairzy.com.home.HomeActivity;
import com.pairzy.com.util.CustomVideoView.NoInterNetView;
import com.pairzy.com.util.Exception.NoMoreDataException;
import com.pairzy.com.util.TypeFaceManager;
import java.util.ArrayList;
import javax.inject.Inject;
import javax.inject.Named;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatActivity;
import dagger.android.support.DaggerFragment;
/**
 *<h2>MyPreferencePage</h2>
 * <P>
 * {@link DaggerAppCompatActivity}
 * </P>
 * @author 3Embed.
 * @since 17-02-2017.
 * @version 1.0.*/
public class MyPreferencePage extends BaseDaggerActivity implements MyPreferencePageContract.View
{
    @Inject
    Activity activity;
    @Inject
    MyPreferencePageContract.Presenter presenter;
    @Inject
    AnimatorHandler animatorHandler;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    @Named(MyPreferencePageBuilder.ACTIVITY_FRAGMENT_MANAGER)
    FragmentManager fragmentManager;
    private Unbinder unbinder;
    @BindView(R.id.parent_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.loading_view)
    RelativeLayout loading_view;
    @BindView(R.id.loading_progress)
    ProgressBar loading_progress;
    @BindView(R.id.connection_error_icon)
    ImageView connection_error_icon;
    @BindView(R.id.message_text)
    TextView message_text;
    @BindView(R.id.error_message)
    TextView error_message;
    @BindView(R.id.preview_container)
    FrameLayout preview_container;
    @BindView(R.id.no_internetView)
    NoInterNetView noInterNetView;
    private ArrayList<PrefranceDataDetails> data;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_preference_page);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder=ButterKnife.bind(this);
        initUI();
        presenter.getPreference();
      openPreviewFrg();
        //presenter.initNetworkObserver();
    }

    /*
     * Update the UI.*/
    private void initUI()
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
        message_text.setTypeface(typeFaceManager.getCircularAirBook());
        error_message.setTypeface(typeFaceManager.getCircularAirBook());
    }

    @Override
    public void onBackPressed()
    {
       int count= fragmentManager.getBackStackEntryCount();
       if(count>1)
       {
           super.onBackPressed();
       }else
       {
           this.finish();
       }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        unbinder.unbind();
        presenter.dropView();
    }

    @OnClick(R.id.error_message)
    void onRetry()
    {
        loading_view.setVisibility(View.VISIBLE);
        hideConnectionFailed();
    }

    /*
     *opening the preview page */
    private void openPreviewFrg()
    {
        PreviewFrg previewFrg=new PreviewFrg();
        FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.frg_enter_from_right, R.anim.frg_exit_from_left);
        fragmentTransaction.add(R.id.preview_container, previewFrg);
        fragmentTransaction.commit();
    }

    @Override
    public void hidePreview()
    {
        preview_container.setVisibility(View.GONE);
    }


    @Override
    public void openNextFrag(int FromList,int next)
    {
        try
        {
            PreferenceItem item=getRequiredItem(FromList,next);
            DaggerFragment frg=openGetRequiredFragment(item);
            openFragment(frg,true);
        } catch (NoMoreDataException e)
        {
            openHomePage();

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void openHomePage()
    {
        Intent intent=new Intent(this,HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void updatePreference(ArrayList<PrefranceDataDetails> data)
    {
        this.data=data;
        if(data!=null&&data.size()>0)
        {
            loading_view.setVisibility(View.GONE);
            openNextFrag(0,0);
        }else
        {
            onConnectionError();
        }
    }

    @Override
    public void onConnectionError()
    {
        loading_view.setVisibility(View.VISIBLE);
        hideProgress();
    }
    /*
     * Hiding the progress bar*/
    private void hideProgress()
    {
        message_text.setVisibility(View.GONE);
        loading_progress.setVisibility(View.GONE);
        Animation animation=animatorHandler.getScaleDown();
        animation.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                showConnectionFailed();
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        loading_progress.setAnimation(animation);
    }
    /*
     *Showing connection failed.*/
    private void showConnectionFailed()
    {
        connection_error_icon.setVisibility(View.VISIBLE);
        Animation in_anim=animatorHandler.getScaleUp();
        in_anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                error_message.setVisibility(View.VISIBLE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        connection_error_icon.setAnimation(in_anim);
    }

    /*
    *Showing connection failed.*/
    private void hideConnectionFailed()
    {
        error_message.setVisibility(View.GONE);
        connection_error_icon.setVisibility(View.GONE);
        Animation in_anim=animatorHandler.getScaleDown();
        in_anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                showProgress();
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        connection_error_icon.setAnimation(in_anim);
    }

    /*
     * Hiding the progress bar*/
    private void showProgress()
    {
        message_text.setVisibility(View.VISIBLE);
        loading_progress.setVisibility(View.VISIBLE);
        Animation animation=animatorHandler.getScaleUp();
        animation.setAnimationListener(new Animation.AnimationListener()
        {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                presenter.getPreference();
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        loading_progress.setAnimation(animation);
    }

    /*
     * Open required fragment.*/
    private DaggerFragment openGetRequiredFragment(PreferenceItem item)
    {
        Bundle data;
        int item_type=item.getType();
        if(item_type==5)
        {
            UserInputFrg userInputFrg=new UserInputFrg();
            userInputFrg.setPreferenceItem(item);
            data=new Bundle();
            data.putInt(ListdataFrg.ITEM_POSITION,item.getPosition());
            userInputFrg.setArguments(data);
            return userInputFrg;
        }else
        {
            ListdataFrg listdataFrg=new ListdataFrg();
            listdataFrg.setPreferenceItem(item);
            data=new Bundle();
            data.putInt(ListdataFrg.ITEM_POSITION,item.getPosition());
            listdataFrg.setArguments(data);
            return listdataFrg;
        }
    }

    @Override
    public void openFragment(DaggerFragment fragment, boolean keepBack)
    {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.frg_enter_from_right, R.anim.frg_exit_from_left);
        fragmentTransaction.add(R.id.parent_container, fragment);
        if(keepBack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void showError(String message)
    {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void updateInterNetStatus(boolean status)
    {
        if(noInterNetView!=null)
        {
            if(status)
            {
                noInterNetView.internetFound();
            }else
            {
                noInterNetView.showNoInternet();
            }
        }
    }

    /*
     *Collecting the required item. */
    private PreferenceItem getRequiredItem(int fromList,int next) throws NoMoreDataException
    {
        if(fromList<data.size())
        {
            ArrayList<PreferenceItem> items=data.get(fromList).getData();
            if(next<items.size())
            {
                PreferenceItem preferenceItem=items.get(next);
                preferenceItem.setPosition(next);
                preferenceItem.setMax_count(items.size());
                preferenceItem.setList_no(fromList);
                return preferenceItem;
            }else
            {
                fromList=fromList+1;
                next=0;
                return getRequiredItem(fromList,next);
            }
        }else
        {
          throw new NoMoreDataException("No more data!");
        }
    }
}
