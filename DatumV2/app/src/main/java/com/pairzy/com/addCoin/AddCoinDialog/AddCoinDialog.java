package com.pairzy.com.addCoin.AddCoinDialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import androidx.appcompat.app.AlertDialog;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.Locale;

/**
 * <h2>ChatMenuDialog</h2>
 * <P>
 *
 * </P>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class AddCoinDialog
{
    private Activity activity;
    private Dialog dialog;
    private TypeFaceManager typeFaceManager;
    private Utility utility;
    private AddCoinDialogCallBack callBack;


    public AddCoinDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility)
    {
        this.activity=activity;
        this.typeFaceManager=typeFaceManager;
        this.utility=utility;
    }


    public void showDialog(String coinPlanName, AddCoinDialogCallBack callBack)
    {

        this.callBack = callBack;
        if(dialog != null && dialog.isShowing())
        {
            dialog.cancel();
        }
        @SuppressLint("InflateParams")
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.add_coin_dialog, null);
        builder.setView(dialogView);
        TextView tvCoinPurchase =dialogView.findViewById(R.id.tv_coin_purchase);
        tvCoinPurchase.setTypeface(typeFaceManager.getCircularAirBold());

        TextView tvCoinPurchaseMsg =dialogView.findViewById(R.id.tv_coin_purchase_msg);
        tvCoinPurchaseMsg.setTypeface(typeFaceManager.getCircularAirBook());
        tvCoinPurchaseMsg.setText(String.format(Locale.ENGLISH,"%s %s plan?", activity.getString(R.string.coin_purchase_msg_text),coinPlanName));

        Button onCanceled=dialogView.findViewById(R.id.onCanceled);
        onCanceled.setTypeface(typeFaceManager.getCircularAirBook());
        onCanceled.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();
        });
        Button onOk=dialogView.findViewById(R.id.onOk);
        onOk.setTypeface(typeFaceManager.getCircularAirBook());
        onOk.setOnClickListener(view -> {
            if(callBack != null)
                callBack.onOk();
            if(dialog!=null)
                dialog.cancel();
        });
        dialog = builder.create();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
    }
}
