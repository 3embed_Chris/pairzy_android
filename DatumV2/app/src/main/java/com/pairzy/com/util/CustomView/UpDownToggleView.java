package com.pairzy.com.util.CustomView;

import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import com.google.android.material.appbar.AppBarLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class UpDownToggleView extends RelativeLayout
{
    private static final int DURATION = 285;
    private int last_height,last_width;

    public UpDownToggleView(Context context)
    {
        super(context);
    }

    public UpDownToggleView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public UpDownToggleView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public UpDownToggleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    boolean isForUp=false;
    public void hideViewUP()
    {
        last_height=this.getHeight();
        last_width=this.getWidth();
        isForUp=true;
        pushTop(this.getHeight(),0,this);
    }


    public void showOriginal()
    {

    }

    /* *
     * Pushing to the top position */
    private void pushTop(int current,int end,final View view)
    {
        ValueAnimator heightAnimator = ValueAnimator.ofInt(current,end);
        heightAnimator.setDuration(DURATION);
        heightAnimator.addUpdateListener(animation -> {
            int data_push=(int)animation.getAnimatedValue();
            view.setLayoutParams(new AppBarLayout.LayoutParams(view.getWidth(),data_push));
        });
        heightAnimator.start();
    }


    /* *
     * Pushing to the down*/
    private void pushDown(int current,int end,final View view)
    {
        ValueAnimator heightAnimator = ValueAnimator.ofInt(current,end);
        heightAnimator.setDuration(DURATION);
        heightAnimator.addUpdateListener(animation -> {
            int data_push=(int)animation.getAnimatedValue();
            view.setLayoutParams(new AppBarLayout.LayoutParams(view.getWidth(),data_push));
        });
        heightAnimator.start();
    }
}
