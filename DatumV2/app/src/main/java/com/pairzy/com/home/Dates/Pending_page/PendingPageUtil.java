package com.pairzy.com.home.Dates.Pending_page;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.home.Dates.DatesFragUtil;
import com.pairzy.com.home.Dates.Pending_page.Model.PendingListAdapter;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
/**
 * <h2>PastDatePageUtil</h2>
 * <P>
 *     User pending list adapter.
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class PendingPageUtil
{
    public static final String PENDING_LAYOUT_MANAGER = "pending_layout_manager";

    @Provides
    @FragmentScoped
    PendingListAdapter getAdapter(Utility utility, @Named(DatesFragUtil.PENDING_LIST)ArrayList<DateListPojo> list, TypeFaceManager typeFaceManager)
    {
        return new PendingListAdapter(utility,list,typeFaceManager);
    }

    @Named(PendingPageUtil.PENDING_LAYOUT_MANAGER)
    @Provides
    @FragmentScoped
    LinearLayoutManager provideLinearLayoutManager(Activity activity)
    {
        return  new LinearLayoutManager(activity);
    }
}
