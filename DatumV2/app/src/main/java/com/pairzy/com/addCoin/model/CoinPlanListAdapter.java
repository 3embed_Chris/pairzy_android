package com.pairzy.com.addCoin.model;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pairzy.com.R;
import com.pairzy.com.boostDetail.model.CoinPlan;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;
import java.util.Locale;


/**
 * <h>CoinPlanListAdapter</h>
 * <p> shows the coin plan list.</p>
 * @author 3Embed.
 * @since 1/6/18.
 * @version 1.0.
 */
public class CoinPlanListAdapter extends RecyclerView.Adapter<CoinPlanViewHolder> {

    private ArrayList<CoinPlan> coinPlanList;
    private TypeFaceManager typeFaceManager;
    private ItemActionCallBack callBack;

    public CoinPlanListAdapter(ArrayList<CoinPlan> coinPlanList, TypeFaceManager typeFaceManager) {
        this.coinPlanList = coinPlanList;
        this.typeFaceManager = typeFaceManager;
    }

    @NonNull
    @Override
    public CoinPlanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.coin_plan_item,parent,false);
        return new CoinPlanViewHolder(view,callBack,typeFaceManager);
    }

    @Override
    public void onBindViewHolder(@NonNull CoinPlanViewHolder holder, int position) {
        try {
            bindsCoinPlanView(holder);
        }catch (Exception ignored){}
    }

    private void bindsCoinPlanView(CoinPlanViewHolder holder) {
        CoinPlan coinPlan = coinPlanList.get(holder.getAdapterPosition());
        if(coinPlan.getPlanName() != null) {
            holder.tvCoinPlanTitle.setText(coinPlan.getPlanName());
        }
        else {
            holder.tvCoinPlanTitle.setText(String.format(Locale.ENGLISH, "%d Coins", coinPlan.getNoOfCoinUnlock().getCoin()));
        }
        holder.tvCoinPlanButtonText.setText(coinPlan.getPrice_text());

    }

    @Override
    public int getItemCount() {
        return coinPlanList.size();
    }

    public void setClickCallback(ItemActionCallBack clickCallback) {
        this.callBack = clickCallback;
    }
}
