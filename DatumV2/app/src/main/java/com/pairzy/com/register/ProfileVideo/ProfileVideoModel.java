package com.pairzy.com.register.ProfileVideo;

import android.graphics.Bitmap;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.model.cloudinaryDetail.CloudinaryData;
import com.pairzy.com.data.model.cloudinaryDetail.CloudinaryDetailResponse;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.CloudManager.UploadManager;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.inject.Inject;
/**
 * <h2>ProfileVideoModel</h2>
 * <P>
 *
 * </P>
 * @since  2/20/2018.
 * @author 3Embed.
 * @version 1.0.
 */
class ProfileVideoModel extends BaseModel
{
    @Inject
    Utility utility;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    UploadManager uploadManager;

    @Inject
    ProfileVideoModel()
    {}

    /*
     * Saving the file d*/
    private File saveBitmap(Bitmap bmp,String destination)
    {
        OutputStream outStream = null;
        File file = new File(destination);
        try {
            outStream = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG,100, outStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally
        {
            if(outStream!=null)
            {
                try
                {
                    outStream.flush();
                    outStream.close();
                }catch (Exception e) {}
            }
        }
        return file;
    }

    /*
     *Deleting the file */
    public void deleteFile(File file)
    {
        try
        {
            if(file.exists())
            {
                file.delete();
            }
        }catch (Exception e){}
    }

    public void parseClodinaryDetail(String response) throws DataParsingException {
        try{
            CloudinaryDetailResponse cloudinaryDetailResponse =
                    utility.getGson().fromJson(response,CloudinaryDetailResponse.class);
            CloudinaryData cloudinaryData = cloudinaryDetailResponse.getCloudinaryData();

            dataSource.setCloudName(cloudinaryData.getCloudName());
            dataSource.setCloudinaryApiKey(cloudinaryData.getApiKey());
            dataSource.setClodinaryApiSecret(cloudinaryData.getApiSecret());

            reinitCloudnaryUpload();
        }catch (Exception e){
            throw new DataParsingException(e.getMessage());
        }
    }

    private void reinitCloudnaryUpload() {
        uploadManager.setNewCreds(true,
                dataSource.getCloudName(),
                dataSource.getCloudinaryApiKey(),
                dataSource.getCloudinaryApiSecret());
    }
}
