package com.pairzy.com.home.Prospects.Passed.Model;

/**
 * @since  4/12/2018.
 */
public class PassedLoadMore
{
    private boolean no_more_data;
    public boolean isNo_more_data() {
        return no_more_data;
    }

    public void setNo_more_data(boolean no_more_data) {
        this.no_more_data = no_more_data;
    }

}
