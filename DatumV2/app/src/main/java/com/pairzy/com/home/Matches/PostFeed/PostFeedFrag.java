package com.pairzy.com.home.Matches.PostFeed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.commentPost.CommentPostActivity;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.HomeContract;
import com.pairzy.com.home.Matches.PostFeed.Model.ExoPlayerRecyclerView;
import com.pairzy.com.home.Matches.PostFeed.Model.PostListAdapter;
import com.pairzy.com.home.Matches.PostFeed.Model.PostListPojo;
import com.pairzy.com.likes.LikesByActivity;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.TypeFaceManager;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

import static com.pairzy.com.home.Matches.PostFeed.PostFeedFragUtil.NEWS_FEED_LIST;

/**
 * <h2>PostFeedFrag</h2>
 * <P>
 *
 * </P>
 * A simple {@link Fragment} subclass.
 * @author 3Embed.
 * @version 1.0.
 * @since 17-03-2018.
 */

@ActivityScoped
public class PostFeedFrag extends DaggerFragment implements PostFeedFragContract.View,SwipeRefreshLayout.OnRefreshListener
{
    private static final String TAG = PostFeedFrag.class.getSimpleName();

    @Inject
    Activity activity;

    @Inject
    HomeContract.Presenter main_presenter;

    @Inject
    PostFeedFragContract.Presenter presenter;

    @Inject
    TypeFaceManager typeFaceManager;

    @Inject
    PostListAdapter newsFeedAdapter;

    @Named(PostFeedFragUtil.NEWS_FEED_LAYOUT_MANAGER)
    @Inject
    LinearLayoutManager linearLayoutManager;

    @Named(NEWS_FEED_LIST)
    @Inject
    ArrayList<PostListPojo> postList;

    @BindView(R.id.upcoming_list)
    ExoPlayerRecyclerView rvPostList;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.loading_progress)
    ProgressBar pbLoading;
    @BindView(R.id.loading_text)
    TextView tvLoading;
    @BindView(R.id.error_icon)
    ImageView ivErrorIcon;
    @BindView(R.id.error_msg)
    TextView tvErrorMsg;
    @BindView(R.id.empty_data)
    RelativeLayout rlEmptyData;
    @BindView(R.id.loading_view)
    RelativeLayout rlLoadingView;
    @BindView(R.id.no_more_data)
    TextView tvNoMoreDataTitle;
    @BindView(R.id.empty_details)
    TextView tvEmptyDetail;
    @BindView(R.id.btn_retry)
    Button btnRetry;

    private Bus bus = AppController.getBus();

    private Unbinder unbinder;
    private boolean firstTime = true;

    @Inject
    public PostFeedFrag() {}


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            bus.register(this);
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_news_feed, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder=ButterKnife.bind(this,view);
        presenter.takeView(this);
        presenter.observeMatchAndUnmatchUser();
        initUi();
        applyFont();
        presenter.checkAndLoadPost();
    }

    @Subscribe
    public void getMessage(JSONObject object) {
        try {
            if (object.getString("eventName").equals("postCompleted")) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        onRefresh();
                    }
                }, 1500);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called !!");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: called !!");
        pausePlayer();
    }

    private void applyFont() {
        tvNoMoreDataTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvEmptyDetail.setTypeface(typeFaceManager.getCircularAirBook());
       // btnPost.setTypeface(typeFaceManager.getCircularAirBook());
        btnRetry.setTypeface(typeFaceManager.getCircularAirBold());
    }

    /*
     * initialization of the required data.*/
    private void initUi()
    {
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh2, R.color.refresh1,R.color.refresh);
        rvPostList.setHasFixedSize(true);

        rvPostList.setLayoutManager(linearLayoutManager);
        rvPostList.setItemAnimator(new DefaultItemAnimator());
        rvPostList.setMediaObjects(postList);
        rvPostList.setAdapter(newsFeedAdapter);
        RecyclerView.ItemAnimator animator = rvPostList.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        if(firstTime){
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if(rvPostList != null)
                        rvPostList.playVideo(false);
                }
            });
            firstTime = false;
        }
        presenter.setAdapterCallBack(newsFeedAdapter);
//        rvPostList.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                int positionView = linearLayoutManager.findLastVisibleItemPosition();
//                presenter.doLoadMore(positionView);
//                datePresenter.preFetchImage(false,positionView);
//            }
//        });
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
        bus.unregister(this);
    }

    @Override
    public void showLoading() {
        if(btnRetry != null) {
            btnRetry.setVisibility(View.GONE);
            tvErrorMsg.setVisibility(View.GONE);
            ivErrorIcon.setVisibility(View.GONE);
            tvLoading.setVisibility(View.VISIBLE);
            pbLoading.setVisibility(View.VISIBLE);
            rlLoadingView.setVisibility(View.VISIBLE);
            //rvPostList.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
        }
    }

    @Override
    public void showData() {
        if(rlEmptyData != null) {
            newsFeedAdapter.notifyDataSetChanged();
            rlLoadingView.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
            rvPostList.setVisibility(View.VISIBLE);
            rvPostList.scrollToPosition(0);
            rvPostList.playVideo(false);
        }
    }

    @Override
    public void showMessage(String message) {
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showNetworkError(String errorMsg) {
        if(tvErrorMsg != null) {
            btnRetry.setVisibility(View.VISIBLE);
            tvErrorMsg.setVisibility(View.VISIBLE);
            tvErrorMsg.setText(errorMsg);
            ivErrorIcon.setVisibility(View.VISIBLE);
            tvLoading.setVisibility(View.GONE);
            pbLoading.setVisibility(View.GONE);
            rvPostList.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void emptyData()
    {
        if(rvPostList != null) {
            //rvPostList.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void launchUserProfile(DateListPojo upcomingItemPojo) {
        //datePresenter.launchUserProfile(upcomingItemPojo);
    }

    @Override
    public void notifyDataAdapter(int position) {
        newsFeedAdapter.notifyItemChanged(position);
    }

    @Override
    public void launchWriteCommit(String postId) {
        Intent intent=new Intent(getContext(), CommentPostActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(ApiConfig.COMMENT.POST_ID, postId);
        startActivityForResult(intent, AppConfig.COMMENT_SCREEN_REQ_CODE);
    }

    @Override
    public void launchCommentView(String postId) {
        launchWriteCommit(postId);
    }

    @Override
    public void  launchLikeView(String postId) {
        Intent intent=new Intent(getContext(), LikesByActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(ApiConfig.COMMENT.POST_ID, postId);
        startActivity(intent);
    }

    @Override
    public void launchChatScreen(Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    @Override
    public void notifyDataAdapter() {
        newsFeedAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.parseDateData(requestCode,resultCode,data);
    }

    @Override
    public void showError(int id)
    {
        String message=getString(id);
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showError(String error)
    {
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @OnClick(R.id.btn_retry)
    public void onRetry(){
        onRefresh();
    }

    @Override
    public void onRefresh() {
        if(swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(false);
            presenter.checkAndLoadPost();
        }
    }

    @Override
    public void pausePlayer() {
        if(rvPostList != null)
            rvPostList.pausePlayer();
    }

    //on post success.
    @Override
    public void updatePostfeedList() {
        if(presenter != null)
            presenter.checkAndLoadPost();
    }


}
