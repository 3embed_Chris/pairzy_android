package com.pairzy.com.mobileverify.otpreceive_error;
import com.pairzy.com.mobileverify.MobileVerifyContract;
import com.pairzy.com.mobileverify.Model.TimerCommunicator;
import javax.inject.Inject;
/**
 * <h2>ResendOtpPresenter</h2>
 * <P>
 *
 * </P>
 * @since  2/7/2018.
 */
public class ResendOtpPresenter implements ResentOtpContract.Presenter
{
    @Inject
    MobileVerifyContract.Presenter  main_presenter;

    private ResentOtpContract.View view;

    @Inject
    ResendOtpPresenter()
    {}

    @Override
    public void takeView(Object view)
    {
       this.view= (ResentOtpContract.View) view;
    }

    @Override
    public void dropView()
    {
      view=null;
    }

    @Override
    public void listenTimer()
    {
        main_presenter.setTimerListener(new TimerCommunicator()
        {
            @Override
            public void onUpdateTime(String time)
            {
                if(view!=null)
                    view.updateTimer(time);
            }
            @Override
            public void onFinished()
            {
                if(view!=null)
                    view.showResentButton();
            }
            @Override
            public void onError(String error)
            {
                if(view!=null)
                    view.showResentButton();
            }
        });
    }


    @Override
    public void doEditPhno()
    {
      if(view!=null)
      view.verifyNumber();
    }

    @Override
    public void doSmsAgain()
    {
        if(view!=null)
        view.sendSMSAgain();

    }

    @Override
    public void sendOtpOnFB()
    {
        if(view==null)
            return;
    }

    @Override
    public void doOtpCall()
    {

    }

    @Override
    public void showError(String message)
    {
        if(view!=null)
        view.showMessage(message);
    }
}
