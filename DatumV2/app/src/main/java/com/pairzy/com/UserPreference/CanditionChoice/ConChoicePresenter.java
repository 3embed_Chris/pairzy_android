package com.pairzy.com.UserPreference.CanditionChoice;
import com.pairzy.com.data.model.Generic;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.util.Utility;
import org.json.JSONArray;
import java.io.IOException;
import javax.inject.Inject;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * <h2>ConChoicePresenter</h2>
 * <P>
 *
 * </P>
 * @since  2/21/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ConChoicePresenter implements ConChoiceContract.Presenter
{
    @Inject
    Utility utility;
    @Inject
    CondChoiceModel model;
    @Inject
    NetworkService service;

    private CompositeDisposable disposable;

    ConChoiceContract.View view;
    @Inject
    ConChoicePresenter()
    {
        disposable=new CompositeDisposable();
    }


    @Override
    public void takeView(ConChoiceContract.View view) {
        this.view= view;
    }

    @Override
    public void dropView()
    {
        view=null;
        disposable.clear();
    }

    @Override
    public void updatePreference(String pref_id, JSONArray values)
    {
        service.setPreferences(model.getAuthorization(),model.getLanguage(),model.getParams(pref_id,values)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        disposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> value)
                    {
                        try
                        {
                            if(value.code() == 200)
                            {
                                String data=value.body().string();
                                Generic result_data=utility.getGson().fromJson(data,Generic.class);
                            }else
                            {
                                if(view!=null)
                                    view.showError(model.getError(value));
                            }
                        } catch (IOException e)
                        {
                            if(view!=null)
                                view.showError(e.getMessage());
                        }
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        if(view!=null)
                        {
                            view.showError(e.getMessage());
                        }
                    }
                    @Override
                    public void onComplete() {}
                });
    }
}
