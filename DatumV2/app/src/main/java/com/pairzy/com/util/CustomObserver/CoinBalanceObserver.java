package com.pairzy.com.util.CustomObserver;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;

/**
 * Created by ankit on 5/6/18.
 */

public class CoinBalanceObserver {
    private ConnectableObservable<Boolean> connectableObservable;
    private ObservableEmitter<Boolean> emitor;
    public CoinBalanceObserver()
    {
        Observable<Boolean> observable=Observable.create(e -> emitor=e);
        connectableObservable=observable.publish();
        connectableObservable.share();
        connectableObservable.replay();
        connectableObservable.connect();
    }

    public ConnectableObservable<Boolean> getObservable()
    {
        return connectableObservable;
    }

    public void publishData(Boolean flag)
    {
        if(emitor!=null)
        {
            emitor.onNext(flag);
        }
    }
}
