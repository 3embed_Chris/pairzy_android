package com.pairzy.com.passportLocation;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h>PassportModule class</h>
 * @author 3Embed.
 * @since 22/05/18.
 * @version 1.0.
 */

@Module
public abstract class PassportModule {

    @ActivityScoped
    @Binds
    abstract PassportContract.Presenter passportPresenter(PassportPresenter presenter);

    @ActivityScoped
    @Binds
    abstract PassportContract.View passportView(PassportActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity bindsActivity(PassportActivity activity);
}
