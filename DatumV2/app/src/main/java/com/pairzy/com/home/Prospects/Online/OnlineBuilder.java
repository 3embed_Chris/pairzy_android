package com.pairzy.com.home.Prospects.Online;

import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * <h2>OnlineBuilder</h2>
 * <P>
 *
 * </P>
 * @since  2/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface OnlineBuilder
{
    @FragmentScoped
    @Binds
    Online_frg getProspectItemFragment(Online_frg prospectItem_frg);

    @FragmentScoped
    @Binds
    OnlineContract.Presenter taskPresenter(OnlinePresenter presenter);

}
