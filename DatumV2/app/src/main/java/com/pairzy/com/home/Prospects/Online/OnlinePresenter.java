package com.pairzy.com.home.Prospects.Online;

import android.app.Activity;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Prospects.Online.Model.OnlineAdpCallBack;
import com.pairzy.com.home.Prospects.Online.Model.OnlineDataModel;
import com.pairzy.com.home.Prospects.Online.Model.OnlineItemPojo;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import java.util.ArrayList;
import javax.inject.Inject;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * <h2>OnlinePresenter</h2>
 * <P>
 *
 * </P>
 * @since  3/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class OnlinePresenter implements OnlineContract.Presenter,OnlineAdpCallBack
{
    private OnlineContract.View view;
    @Inject
    NetworkService service;
    @Inject
    NetworkStateHolder holder;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    OnlineDataModel model;
    @Inject
    Activity activity;
    private CompositeDisposable compositeDisposable;
    public static final int LIMIT=10;
    public static int PAGE_COUNT=0;
    private boolean isTryAgain;

    @Inject
    ArrayList<OnlineItemPojo> list_data;

    @Inject
    OnlinePresenter(){
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(OnlineContract.View view)
    {
        this.view= view;
    }

    @Override
    public void dropView()
    {
        view=null;
        compositeDisposable.clear();
    }

    @Override
    public void tryAgain()
    {
        PAGE_COUNT=0;
        tryLoadAgain();
    }

    @Override
    public void getListData(boolean isLoadMore)
    {
        if(holder.isConnected())
        {
            if(!isTryAgain)
            {
                if(!isLoadMore)
                {
                    PAGE_COUNT=0;
                }else
                {
                    PAGE_COUNT+=LIMIT;
                }
            }

            isTryAgain=false;
            if(!model.handelLoadMore())
            {
                if(view!=null)
                    view.showProgress();
            }
            service.getOnLineList(dataSource.getToken(),model.getLanguage(),PAGE_COUNT,PAGE_COUNT+LIMIT)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value)
                        {
                            try
                            {
                                if(value.code()==200)
                                {
                                    try {
                                        String responseData = value.body().string();
                                        model.parseData(responseData);
                                        if (view != null)
                                            view.onDataUpdate();
                                    } catch (Exception ignored){
                                        //some getting empty data with 200 code
                                        if(!model.handelLoadMoreError())
                                        {
                                            if(view!=null)
                                                view.emptyData();
                                        }
                                    }
                                }
                                else if(value.code() == 401){
                                    AppController.getInstance().appLogout();
                                }
                                else if(value.code()==412)
                                {
                                    if(!model.handelLoadMoreError())
                                    {
                                        if(view!=null)
                                            view.emptyData();
                                    }
                                }else
                                {
                                    if(!model.handelLoadMoreError())
                                    {
                                        if(view!=null)
                                            view.onApiError(activity.getString(R.string.failed_get_members));
                                    }

                                    if(view!=null)
                                        view.showError(model.getError(value));
                                }
                            } catch (Exception e)
                            {
                                if(!model.handelLoadMoreError())
                                {
                                    if(view!=null)
                                        view.onApiError(activity.getString(R.string.failed_get_members));
                                }
                                if(view!=null)
                                    view.showError(R.string.failed_get_members);
                            }
                        }
                        @Override
                        public void onError(Throwable e)
                        {
                            if(!model.handelLoadMoreError())
                            {
                                if(view!=null)
                                    view.onApiError(activity.getString(R.string.failed_get_members));
                            }

                            if(view!=null)
                                view.showError(R.string.failed_get_members);
                        }
                        @Override
                        public void onComplete() {}
                    });
        }else
        {
            isTryAgain=false;
            if(!model.handelLoadMoreError())
            {
                if(view!=null)
                    view.onApiError(activity.getString(R.string.no_internet_error));
            }
            if(view!=null)
                view.showError(R.string.no_internet_error);
        }
    }

    @Override
    public void pee_fetchProfile(int position)
    {
        model.prefetchImage(position);
    }

    @Override
    public boolean canDoLoadMore(int position)
    {
        return model.checkLoadMoreRequired(position);
    }

    @Override
    public void setAdapterCallabck() {
        if(view!=null)
            view.addAdapterCallback(this);
    }

    @Override
    public void openProfile(int position)
    {
        try {
            String user_Details=model.getUserDetails(position);
            if(view!=null)
            {
                view.openUserProfile(user_Details);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void tryLoadAgain()
    {
        isTryAgain=true;
        getListData(false);
    }
}
