package com.pairzy.com.boostDetail.model;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.pairzy.com.R;

/**
 * <h>SlideOffPagerTransformer class</h>
 * @author 3Embed.
 * @since 10/5/18.
 */

public class BoostDeatilPageTransformer implements ViewPager.PageTransformer {

    public void transformPage(@NonNull View view, float position) {
        int pageWidth = view.getWidth();
        TextView tvPageMsg = view.findViewById(R.id.slide_msg_tv);

        if(position < -1){
            // This page is way off-screen to the left.
            view.setAlpha(0);
        }
        else if (position <= 1) { // [-1,1]
            float transformFactor = 1 - Math.abs(position);
            float horzShift = pageWidth * (1 - transformFactor) / 2;
            if (position < 0) {
                //view.setTranslationX(-horzShift);
                tvPageMsg.setTranslationX(-(horzShift*2));
            } else {
                //view.setTranslationX(horzShift);
                tvPageMsg.setTranslationX(horzShift*2);
            }
            view.setAlpha(1);
        }
    }
}
