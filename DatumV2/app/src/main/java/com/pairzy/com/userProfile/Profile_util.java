package com.pairzy.com.userProfile;

import android.app.Activity;
import androidx.fragment.app.FragmentManager;

import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.userProfile.AleretBox.BottomDialog;
import com.pairzy.com.userProfile.Model.MediaPojo;
import com.pairzy.com.userProfile.Model.UserMediaAdapter;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.ReportUser.ReportUserDialog;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.boostDialog.BoostDialog;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.pairzy.com.util.timerDialog.TimerDialog;

import java.util.ArrayList;
import javax.inject.Named;
import dagger.Module;
import dagger.Provides;
/**
 *<h2>Profile_util</h2>
 * <P>
 *     Use proile util class to provide required details.
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 17-03-2018.*/
@Module
public class Profile_util {
    public static final String USER_PROFILE_MOMEMT = "profileMomnets";
    public static final String USER_PROFILE_ADAPTER = "profileAdapter";

    @ActivityScoped
    @Provides
    ArrayList<MediaPojo> getImageList() {
        return new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    UserMediaAdapter getListAdapter(@Named(UserProfileBuilder.USER_FRAGMENT_MANAGER) FragmentManager fm, ArrayList<MediaPojo> data) {
        return new UserMediaAdapter(fm, data);
    }

    @Provides
    @ActivityScoped
    BottomDialog getBottomDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility) {
        return new BottomDialog(activity, typeFaceManager, utility);
    }

    @Provides
    @ActivityScoped
    ReportUserDialog provideReportUserDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility) {
        return new ReportUserDialog(activity, typeFaceManager, utility);
    }

    @Provides
    @ActivityScoped
    ArrayList<String> reportReasonList() {
        return new ArrayList<>();
    }

    @Provides
    @ActivityScoped
    LoadingProgress provideLoadingProgress(Activity activity) {
        return new LoadingProgress(activity);
    }

    @Provides
    @ActivityScoped
    BoostDialog getBootsDialog(Activity activity, TypeFaceManager typeFaceManager, PreferenceTaskDataSource dataSource, Utility utility) {
        return new BoostDialog(activity, typeFaceManager, dataSource, utility);
    }

    @Provides
    @ActivityScoped
    DatumProgressDialog getDatumProgressDialog(Activity activity, TypeFaceManager typeFaceManager) {
        return new DatumProgressDialog(activity, typeFaceManager);
    }

    @ActivityScoped
    @Provides
    CoinDialog provideCoinDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility) {
        return new CoinDialog(activity, typeFaceManager, utility);
    }

    @ActivityScoped
    @Provides
    TimerDialog provideTimerDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility, PreferenceTaskDataSource dataSource) {
        return new TimerDialog(activity, typeFaceManager, utility, dataSource);
    }

    @Named(USER_PROFILE_MOMEMT)
    @Provides
    @ActivityScoped
    ArrayList<MomentsData> provideUserProfileData() {
        return new ArrayList<>();
    }
}




