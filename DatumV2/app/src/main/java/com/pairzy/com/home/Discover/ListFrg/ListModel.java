package com.pairzy.com.home.Discover.ListFrg;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Discover.DiscoveryFragUtil;
import com.pairzy.com.home.Discover.Model.UserItemPojo;
import com.pairzy.com.home.HomeModel.LoadMoreStatus;
import com.pairzy.com.home.HomeUtil;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.Utility;
import java.util.ArrayList;
import javax.inject.Inject;
import javax.inject.Named;
/**
 * @since  4/10/2018.
 */
public class ListModel extends BaseModel
{
    @Named(HomeUtil.LOAD_MORE_STATUS)
    @Inject
    LoadMoreStatus no_more_data;
    @Inject
    Utility utility;
    @Inject
    @Named(DiscoveryFragUtil.USER_LIST)
    ArrayList<UserItemPojo> userList;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    PreferenceTaskDataSource dataSource;

    @Inject
    ListModel(){}

    /*
     *Check loadMore Required */
    public boolean loadMoreRequired(int currentPos)
    {
        if(no_more_data.isNo_more_data())
        {
            return false;
        }

        try
        {
            int size=userList.size();
            int pending=size-currentPos;
            if(pending<5)
            {
                UserItemPojo last_item=userList.get(size-1);
                if(!last_item.isLoading())
                {
                    UserItemPojo loading_item=new UserItemPojo();
                    loading_item.setLoading(true);
                    userList.add(loading_item);
                    return true;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }


    public boolean isDataEmpty()
    {
        int size_data=userList.size();
        return size_data == 0;
    }


    public String getUserDetails(int position) throws EmptyData
    {
        try
        {
            UserItemPojo temp=userList.get(position);
            if(temp!=null)
            {
                  return utility.getGson().toJson(temp);
            }else
            {
                throw new EmptyData("CoinData is empty!");
            }
        }catch (Exception e){
            throw new EmptyData("CoinData is empty!");
        }
    }

    public void addDataInPosition(UserItemPojo userItemPojo)
    {
        try
        {
            int actual_pos=userItemPojo.item_actual_pos;
            if(actual_pos<1&&actual_pos>userList.size()-1)
            {
                userList.add(0,userItemPojo);
            }else
            {
                userList.add(actual_pos,userItemPojo);
            }
        }catch (Exception e){}
    }


    public UserItemPojo getUserPojo(int position) {
        if(userList.size() >position){
            return userList.get(position);
        }
        return null;
    }

    public boolean isEnoughWalletBalanceToSuperLike() {
        if(coinBalanceHolder.getCoinBalance() != null && coinConfigWrapper.getCoinData() != null){
            int coinRequired = coinConfigWrapper.getCoinData().getSuperLike().getCoin();
            int walletBalance = Integer.parseInt(coinBalanceHolder.getCoinBalance());
            if(coinRequired <= walletBalance){
                return true;
            }
            else{
                return false;
            }
        }
        else {
            return false;
        }
    }

    public boolean isSuperlikeSpendDialogNeedToShow() {
        return dataSource.getShowSuperlikeCoinDialog();
    }
}
