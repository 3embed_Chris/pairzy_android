package com.pairzy.com.photoVidPreview;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

public interface PhotoVidContract {

    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter<View> {

    }
}
