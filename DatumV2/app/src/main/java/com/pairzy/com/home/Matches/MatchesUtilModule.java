package com.pairzy.com.home.Matches;


import androidx.fragment.app.Fragment;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.home.Matches.Chats.ChatsFragment;
import com.pairzy.com.home.Matches.PostFeed.PostFeedFrag;

import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class MatchesUtilModule {

    static final String MATCHES_FRAG_LIST="matches_frag_list";

    @Provides
    @ActivityScoped
    ChatsFragment provideChatsFragment(){
        return new ChatsFragment();
    }

    @Provides
    @ActivityScoped
    PostFeedFrag provideNewsFeedFrag(){
        return new PostFeedFrag();
    }

    @Provides
    @ActivityScoped
    @Named(MATCHES_FRAG_LIST)
    ArrayList<Fragment> provideFrgList(ChatsFragment chatsFragment, PostFeedFrag newsFeedFrag)
    {
        return new ArrayList<>(Arrays.asList(chatsFragment,newsFeedFrag));
    }
}
