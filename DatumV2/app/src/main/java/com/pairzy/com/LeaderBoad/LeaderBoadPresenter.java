package com.pairzy.com.LeaderBoad;

import android.app.Activity;

import com.pairzy.com.LeaderBoad.model.LeaderBoardModel;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkServicePy;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.progressbar.LoadingProgress;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class LeaderBoadPresenter implements LeaderBoadContract.Presenter {

    @Inject
    NetworkServicePy service;

    @Inject
    PreferenceTaskDataSource dataSource;

    @Inject
    LeaderBoadContract.View view;

    @Inject
    NetworkStateHolder networkStateHolder;

    @Inject
    Activity activity;

    @Inject
    LeaderBoardModel model;

    @Inject
    LoadingProgress  loadingProgress;

    @Inject
    public LeaderBoadPresenter() {}

    @Inject

    @Override
    public void fetchDataFromApi() {
        if(networkStateHolder.isConnected()){
            loadingProgress.show();
            service.getLeaderBoard(dataSource.getToken(),0,500)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) { }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if(value.code()==200){
                                try {
                                    String data=value.body().string();
                                    model.parseModel(data);
                                    view.isEmpty();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            view.isEmpty();
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view!=null)
                                view.showError(e.getMessage());
                        }

                        @Override
                        public void onComplete() {}
                    });

        }else{
            loadingProgress.cancel();
            if(view!=null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }
}
