package com.pairzy.com.mobileverify.otp;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.data.model.LoginResponse;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.mobileverify.MobileVerifyContract;
import com.pairzy.com.mobileverify.otpreceive_error.ResentOtpFragment;
import com.pairzy.com.mobileverify.result.ResultFragment;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import java.util.Locale;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * <h2>Otp_Fragment</h2>
 * <P>
 *
 * </P>
 * A simple {@link Fragment} subclass.
 */
public class Otp_Fragment extends DaggerFragment implements OtpContract.View, TextWatcher
{
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String COUNTRY_CODE= "countryCode";
    private String mobile_number;
    private String country_code;
    @Inject
    Activity activity;
    @Inject
    OtpPresenter presenter;
    @Inject
    PreferenceTaskDataSource preferencesHelper;
    @Inject
    MobileVerifyContract.Presenter main_presenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @BindView(R.id.etOtp)
    EditText etOtp;
    @BindView(R.id.btnNext)
    RelativeLayout btnNext;
    @BindView(R.id.tvMobileNumber)
    TextView tvMobileNumber;
    @BindView(R.id.titleOne)
    TextView tvTitleOne;
    @BindView(R.id.titleTwo)
    TextView tvTitleTwo;
    @BindView(R.id.btnResendOtp)
    Button btnResendOtp;
    @Inject
    Utility utility;

    private Unbinder unbinder;
    private InputOtpFilter inputOtpFilter;
    @Inject
    public Otp_Fragment() {}


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        try {
            Bundle data=getArguments();
            assert data != null;
            mobile_number = data.getString(MOBILE_NUMBER);
            country_code=data.getString(COUNTRY_CODE);
        } catch (Exception e)
        {
            presenter.showError(e.getMessage());
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_otp_, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this,view);
        presenter.takeView(this);
        applyingFont();
        iniView();
        etOtp.setText("111111");
    }

    private void applyingFont()
    {
        tvTitleOne.setTypeface(typeFaceManager.getCircularAirBold());
        tvTitleTwo.setTypeface(typeFaceManager.getCircularAirBold());
        btnResendOtp.setTypeface(typeFaceManager.getCircularAirBook());
    }



    @Override
    public void notHaveOtp()
    {
        Bundle data=new Bundle();
        data.putString(ResultFragment.COUNTRY_CODE,country_code);
        data.putString(ResultFragment.MOBILE_NUMBER,mobile_number);
        ResentOtpFragment fragment=new ResentOtpFragment();
        fragment.setArguments(data);
        main_presenter.moveFragment(fragment,true);
    }
    @Override
    public void openPrivacyPolice()
    {}
    @Override
    public void TermAndServices()
    {}
    /*
      *Init the view data data */
    private void iniView()
    {
        btnNext.setEnabled(false);
        tvMobileNumber.setText(String.format(Locale.ENGLISH,"%s%s", country_code, mobile_number));
        etOtp.addTextChangedListener(this);
        inputOtpFilter=new InputOtpFilter(etOtp);
        utility.openSpotInputKey(activity,etOtp);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        unbinder.unbind();
        presenter.dropView();
    }


    @OnClick(R.id.ibClose)
    void back()
    {
        activity.onBackPressed();
    }


    @OnClick(R.id.btnResendOtp)
    public void btnResend()
    {
        etOtp.setText("");
        etOtp.requestFocus();
        presenter.notHaveOtpEvent();
    }


    @OnClick(R.id.btnNext)
    public void btnContinue()
    {
//        etOtp.setText(etOtp.getText());
//        etOtp.setSelection(etOtp.getText().length());
        etOtp.requestFocus();
        utility.closeSpotInputKey(activity,etOtp);
        String  otp = "" + etOtp.getText();
        presenter.validateInput(otp);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        etOtp.setSelection(etOtp.getText().length());
        etOtp.requestFocus();
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    @Override
    public void showMessage(String message)
    {
        main_presenter.showMessage(message);
    }

    @Override
    public void otpNotConfirmed(String message) {}
    @Override
    public void loginSuccessful(LoginResponse response)
    {}

    @Override
    public void disableNext() {
        btnNext.setEnabled(false);
    }

    @Override
    public void verifiedOtp(String otp)
    {
        utility.closeSpotInputKey(activity,etOtp);
        btnNext.setEnabled(false);
        Bundle data=new Bundle();
        data.putString(ResultFragment.COUNTRY_CODE,country_code);
        data.putString(ResultFragment.MOBILE_NUMBER,mobile_number);
        data.putString(ResultFragment.OTP,otp);
        data.putString(ResultFragment.CODE,ResultFragment.VERIFY_OTP_OPERATION);
        ResultFragment fragment=new ResultFragment();
        fragment.setArguments(data);
        main_presenter.moveFragment(fragment,true);
        btnNext.setEnabled(true);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {
        String code = charSequence.toString();
        if(!code.isEmpty())
        {
            if(code.length() == 6)
            {
                btnNext.setEnabled(true);
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        btnContinue();
                    }
                },500);
            }
            else {
                btnNext.setEnabled(false);
            }
        }
    }
    @Override
    public void afterTextChanged(Editable editable)
    {}

}
