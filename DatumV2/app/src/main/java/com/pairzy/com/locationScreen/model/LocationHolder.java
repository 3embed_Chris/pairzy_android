package com.pairzy.com.locationScreen.model;

/**
 * <h>LocationHolder data class</h>
 * @author 3Embed.
 * @since 9/5/18.
 * @version 1.0.
 */

public class LocationHolder {

    Double latitude = 0.0;
    Double longitude = 0.0;

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }
}
