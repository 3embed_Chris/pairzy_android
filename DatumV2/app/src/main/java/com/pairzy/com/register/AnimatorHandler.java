package com.pairzy.com.register;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.pairzy.com.R;
/**
 * @since  2/6/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class AnimatorHandler
{
    private Context context;

     public AnimatorHandler(Context context)
    {
        this.context=context;
    }

    public Animation getItemScaleUp()
    {
        return AnimationUtils.loadAnimation(context, R.anim.item_selection_scal_up);
    }

    public Animation getItemScaleDown()
    {
        return AnimationUtils.loadAnimation(context,R.anim.item_selection_scal_down);
    }

    public Animation getBottomUp()
    {
        return AnimationUtils.loadAnimation(context, R.anim.bottom_up_anim);
    }

    public Animation getBottomDown()
    {
        return AnimationUtils.loadAnimation(context,R.anim.bottom_down_anim);
    }
}
