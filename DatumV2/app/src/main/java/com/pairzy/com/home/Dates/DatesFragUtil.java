package com.pairzy.com.home.Dates;

import android.app.Activity;
import androidx.fragment.app.Fragment;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.home.Dates.CustomAlertDialog.CustomAlertDialog;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;
import com.pairzy.com.home.Dates.Pending_page.PendingFrg;
import com.pairzy.com.home.Dates.pastDatePage.PastDateFrg;
import com.pairzy.com.home.Dates.upcomingPage.UpcomingFrg;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
/**
 * @since  3/5/2018.
 * @version 1.0.
 * @author 3Embed.
 */
@Module
public class DatesFragUtil
{

    public static final String DATE_FRAG_LIST="date_frag_list";
    public static final String PENDING_LIST="pending_list";
    public static final String UPCOMING_LIST="upcoming_list";
    public static final String PAST_LIST="past_list";

    @Provides
    @ActivityScoped
    PendingFrg providePendingFrg(){
        return new PendingFrg();
    }

    @Provides
    @ActivityScoped
    UpcomingFrg provideUpcomingFrg(){
        return new UpcomingFrg();
    }

    @Provides
    @ActivityScoped
    PastDateFrg providePastDateFrg(){
        return new PastDateFrg();
    }

    @Provides
    @ActivityScoped
    @Named(DATE_FRAG_LIST)
    ArrayList<Fragment> provideFrgList(PendingFrg pendingFrg,UpcomingFrg upcomingFrg,PastDateFrg pastDateFrg)
    {
        return new ArrayList<>(Arrays.asList(pendingFrg,upcomingFrg,pastDateFrg));
    }

    @Named(UPCOMING_LIST)
    @Provides
    @ActivityScoped
    ArrayList<DateListPojo> provideUpcomingDateList()
    {
        return  new ArrayList<>();
    }

    @Named(PENDING_LIST)
    @Provides
    @ActivityScoped
    ArrayList<DateListPojo> providePendingList()
    {
        return  new ArrayList<>();
    }

    @Named(PAST_LIST)
    @Provides
    @ActivityScoped
    ArrayList<PastDateListPojo> providePastDateList()
    {
        return  new ArrayList<>();
    }

    @Provides
    @ActivityScoped
    CustomAlertDialog provideCustomAlertDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility)
    {
        return new CustomAlertDialog(activity,typeFaceManager,utility);
    }

}
