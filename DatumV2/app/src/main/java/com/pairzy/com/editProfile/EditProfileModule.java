package com.pairzy.com.editProfile;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h>EditPrefModule class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

@Module
public abstract class EditProfileModule {

    @ActivityScoped
    @Binds
    abstract EditProfileContract.Presenter editProfilePresenter(EditProfilePresenter presenter);

    @ActivityScoped
    @Binds
    abstract EditProfileContract.View editProfileView(EditProfileActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity editProfileActivity(EditProfileActivity activity);

}
