package com.pairzy.com.util.LogoutDialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
/**
 * <h2>RatingDialog class</h2>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class LogoutDialog
{
    private LogoutAlertCallback callback;
    private Activity activity;
    private Dialog  dialog = null;
    private TypeFaceManager typeFaceManager;
    private TextView message;


    public LogoutDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        this.typeFaceManager=typeFaceManager;
        this.activity=activity;
    }

    /*
     * Showing the alert.*/
    public void showAlert(LogoutAlertCallback callback1)
    {
        this.callback=callback1;
        if(dialog!=null&&dialog.isShowing())
        {
            dialog.cancel();
        }
        ColorDrawable cd=new ColorDrawable();
        cd.setColor(Color.TRANSPARENT);
        dialog =new Dialog(activity,R.style.Datum_AlertDialog);
        Window window1=dialog.getWindow();
        assert window1 != null;
        window1.setBackgroundDrawable(cd);
        LayoutInflater inflater =activity.getLayoutInflater();
        @SuppressLint("InflateParams")
        View alertLayout = inflater.inflate(R.layout.dialog_logout, null);
        message=alertLayout.findViewById(R.id.tvMessage);
        message.setTypeface(typeFaceManager.getCircularAirBook());

        Button btnLogout=alertLayout.findViewById(R.id.btn_logout);
        btnLogout.setTypeface(typeFaceManager.getCircularAirBook());
        btnLogout.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();

            if(callback != null)
                callback.onLogout();
        });
        Button onCanceled=alertLayout.findViewById(R.id.btn_cancel);
        onCanceled.setTypeface(typeFaceManager.getCircularAirBook());
        onCanceled.setOnClickListener(view -> {
            if(callback!=null)
                callback.onLogoutCancel();
            if(dialog!=null)
            {
                dialog.cancel();
            }
        });
        dialog.setContentView(alertLayout);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        lp.width = (int) (displaymetrics.widthPixels * 0.93);
        lp.height = (int) (displaymetrics.heightPixels * 0.93);
        window.setAttributes(lp);
        dialog.show();
    }

}
