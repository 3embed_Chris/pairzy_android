package com.pairzy.com.addCoin;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h>PassportModule class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */
@Module
public abstract class AddCoinModule {

    @ActivityScoped
    @Binds
    abstract AddCoinContract.Presenter boostDetailPresenter(AddCoinPresenter presenter);

    @ActivityScoped
    @Binds
    abstract AddCoinContract.View boostDetailView(AddCoinActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity boostDetailActivity(AddCoinActivity activity);
}
