package com.pairzy.com.home.MakeMatch;

import android.app.Activity;
import android.graphics.Typeface;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.home.MakeMatch.model.MakeMatchDataPOJO;
import com.pairzy.com.home.MakeMatch.swipeCardModel.MatchMakerAdapter;
import com.pairzy.com.home.MakeMatch.swipeCardModel.PairMakerAdapter;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class MakeMatchUtil {

    public static final String MAKE_MATCH="makeMatch";
    @Named(MAKE_MATCH)
    @Provides
    @ActivityScoped
    ArrayList<MakeMatchDataPOJO> provideMakeMatchDataPOJOS()
    {
        return  new ArrayList<>();
    }

    @ActivityScoped
    @Provides
    MatchMakerAdapter provideAdapter(Activity activity, TypeFaceManager typeFaceManager,  @Named(MakeMatchUtil.MAKE_MATCH) ArrayList<MakeMatchDataPOJO> arrayList){
        return new MatchMakerAdapter(typeFaceManager,arrayList);
    }

    @ActivityScoped
    @Provides
    PairMakerAdapter providePairMakerAdapter(Activity activity, TypeFaceManager typeFaceManager, @Named(MakeMatchUtil.MAKE_MATCH) ArrayList<MakeMatchDataPOJO> arrayList){
        return new PairMakerAdapter(typeFaceManager,arrayList);
    }
}
