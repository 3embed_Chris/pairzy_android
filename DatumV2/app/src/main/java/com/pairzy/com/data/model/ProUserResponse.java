package com.pairzy.com.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProUserResponse {

@SerializedName("data")
@Expose
private Subscription subs;

public Subscription getData() {
return subs;
}

public void setData(Subscription subs) {
this.subs = subs;
}

}