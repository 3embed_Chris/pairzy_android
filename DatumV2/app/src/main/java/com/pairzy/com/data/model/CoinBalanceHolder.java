package com.pairzy.com.data.model;
/**
 * Created by ankit on 5/6/18.
 */
public class CoinBalanceHolder
{
    private String coinBalance = "";
    private Boolean isUpdated = false;


    public String getCoinBalance() {
        return coinBalance;
    }

    public Boolean getUpdated() {
        return isUpdated;
    }

    public void setCoinBalance(String coinBalance) {
        this.coinBalance = coinBalance;
    }

    public void setUpdated(Boolean updated) {
        isUpdated = updated;
    }
}
