package com.pairzy.com.passportLocation.model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
/**
 * Created by ankit on 22/5/18.
 */
public class PassportLocViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    TextView locationName;
    TextView subLocation;
    ImageView locationCheckBox;
    RelativeLayout btnLocationClose;
    private PassportAdapter.OnItemClickListener listener;
    private TypeFaceManager typeFaceManager;
    public PassportLocViewHolder(View itemView, PassportAdapter.OnItemClickListener listener, TypeFaceManager typeFaceManager) {
        super(itemView);
        this.listener = listener;
        this.typeFaceManager =typeFaceManager;
        locationName = itemView.findViewById(R.id.location_name);
        locationName.setTypeface(typeFaceManager.getCircularAirBold());
        subLocation = itemView.findViewById(R.id.sub_location_tv);
        subLocation.setTypeface(typeFaceManager.getCircularAirBook());
        locationCheckBox = itemView.findViewById(R.id.location_check_box);
        btnLocationClose = itemView.findViewById(R.id.location_close);
        btnLocationClose.setOnClickListener(this);
        locationCheckBox.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId()){
            case R.id.location_close:
                if(listener != null){
                    listener.onItemClose(getAdapterPosition());
                }
                break;
            case R.id.location_check_box:
                if(listener != null)
                {
                    listener.onItemChecked(getAdapterPosition());
                }
                break;
        }

    }
}
