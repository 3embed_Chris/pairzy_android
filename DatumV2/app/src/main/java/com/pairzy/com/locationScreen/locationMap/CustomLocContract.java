package com.pairzy.com.locationScreen.locationMap;

import com.pairzy.com.BasePresenter;

/**
 * <h>CustomLocContract interface</h>
 * @author 3Embed.
 * @since 8/5/18.
 * @version 1.0.
 */

public interface CustomLocContract {

    interface View{

        void applyFont();
    }

    interface Presenter extends BasePresenter<View> {
        void init();
    }
}
