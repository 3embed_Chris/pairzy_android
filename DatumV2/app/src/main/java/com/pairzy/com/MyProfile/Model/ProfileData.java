package com.pairzy.com.MyProfile.Model;
import com.pairzy.com.data.model.MyPrefrance;
import com.pairzy.com.userProfile.Model.AgeResponse;
import com.pairzy.com.userProfile.Model.DistanceResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * @since  4/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ProfileData
{
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("dob")
    @Expose
    private double dob;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("myPreferences")
    @Expose
    private ArrayList<MyPrefrance> myPreferences = null;
    @SerializedName("moments")
    @Expose
    private ArrayList<MomentsData> moments = null;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("profileVideoThumbnail")
    @Expose
    private String profileVideoThumbnail;

    @SerializedName("profileVideo")
    @Expose
    private String profileVideo;

    @SerializedName("profilePic")
    @Expose
    private String profilePic;

    @SerializedName("about")
    @Expose
    private String about;

    @SerializedName("age")
    @Expose
    private AgeResponse ageResponse;

    @SerializedName("distance")
    @Expose
    private DistanceResponse distResponse;

    @SerializedName("otherImages")
    @Expose
    private ArrayList<String> otherImages;

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }


    public ArrayList<String> getOtherImages()
    {
        return otherImages;
    }

    public void setOtherImages(ArrayList<String> otherImages)
    {
        this.otherImages = otherImages;
    }

    public String getProfilePic()
    {
        return profilePic;
    }

    public void setProfilePic(String profilePic)
    {
        this.profilePic = profilePic;
    }

    public String getProfileVideo() {
        return profileVideo;
    }

    public void setProfileVideo(String profileVideo) {
        this.profileVideo = profileVideo;
    }


    public String getProfileVideoThumbnail() {
        return profileVideoThumbnail;
    }

    public void setProfileVideoThumbnail(String profileVideoThumbnail) {
        this.profileVideoThumbnail = profileVideoThumbnail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Double getDob() {
        return dob;
    }

    public void setDob(Integer dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ArrayList<MyPrefrance> getMyPreference() {
        return myPreferences;
    }

    public void setMyPreference(ArrayList<MyPrefrance> myPreferences) {
        this.myPreferences = myPreferences;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public AgeResponse getAgeResponse() {
        return ageResponse;
    }

    public void setAgeResponse(AgeResponse ageResponse) {
        this.ageResponse = ageResponse;
    }

    public DistanceResponse getDistResponse() {
        return distResponse;
    }

    public void setDistResponse(DistanceResponse distResponse) {
        this.distResponse = distResponse;
    }

    public ArrayList<MomentsData> getMoments() {
        return moments;
    }

    public void setMoments(ArrayList<MomentsData> moments) {
        this.moments = moments;
    }
}
