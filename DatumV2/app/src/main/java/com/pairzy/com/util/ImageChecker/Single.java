package com.pairzy.com.util.ImageChecker;

/**
 * <h2>Single</h2>
 * @since  4/3/2018.
 * @version 1.0.
 * @author 3Embed.
 */
public class Single extends PropertiesModel implements PropertiesExtractor
{
    private Properties properties;
    public Single(Properties properties)
    {
        this.properties=properties;
    }

    @Override
    public String getProperties() {
        return properties.value;
    }
}
