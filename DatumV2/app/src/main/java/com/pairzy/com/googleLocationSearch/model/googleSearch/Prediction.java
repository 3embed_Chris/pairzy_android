package com.pairzy.com.googleLocationSearch.model.googleSearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 23/5/18.
 */

public class Prediction {

    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("reference")
    @Expose
    private String reference;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
