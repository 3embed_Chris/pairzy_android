package com.pairzy.com.fbRegister.Gender;

import javax.inject.Inject;

/**
 * <h2>FbGenderPresenter</h2>
 * <P>
 *
 * </P>
 * @since  2/15/2018.
 * @version 1.0.
 * @author 3Embed.
 */
public class FbGenderPresenter implements FbGenderContract.Presenter
{
    @Inject
    FbGenderPresenter(){}
    private FbGenderContract.View view;
    @Override
    public void handleSelection(int position, boolean isSelected)
    {

    }

    @Override
    public void takeView(FbGenderContract.View view) {
        this.view= view;
    }

    @Override
    public void dropView() {
      view=null;
    }
}
