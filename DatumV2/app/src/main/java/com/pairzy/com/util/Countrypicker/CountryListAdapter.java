package com.pairzy.com.util.Countrypicker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.R.drawable;
import com.pairzy.com.util.TypeFaceManager;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Locale;
/**
 * <h2>CountryListAdapter</h2>
 * <P>
 *
 * </P>
 * @since /9/16.
 *@author 3Embed.
 * @version 1.0.
 */
class CountryListAdapter extends BaseAdapter
{
    private List<Country> countries;
    private LayoutInflater inflater;
    private TypeFaceManager typeFaceManager;
    CountryListAdapter(Context context, List<Country> countries,TypeFaceManager typeFaceManager)
    {
        super();
        this.typeFaceManager=typeFaceManager;
        this.countries = countries;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    private int getResId(String drawableName)
    {

        try
        {
            Class<drawable> res = drawable.class;
            Field field = res.getField(drawableName);
            return field.getInt(null);
        } catch (Exception e) {
            Log.e("CountryCodePicker", "Failure to get drawable id.", e);
        }
        return -1;
    }

    @Override
    public int getCount() {
        return countries.size();
    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View cellView = convertView;
        Cell cell;
        Country country = countries.get(position);

        if (convertView == null) {
            cell = new Cell();
            cellView = inflater.inflate(R.layout.row, null);
            cell.textView = cellView.findViewById(R.id.row_title);
            cell.textView.setTypeface(typeFaceManager.getCircularAirBook());
            cell.imageView = cellView.findViewById(R.id.row_icon);
            cell.code_text=cellView.findViewById(R.id.code_text);
            cell.code_text.setTypeface(typeFaceManager.getCircularAirBook());
            cellView.setTag(cell);
        } else {
            cell = (Cell) cellView.getTag();
        }

        cell.textView.setText(country.getName());
        cell.code_text.setText(country.getDial_code());
        String drawableName = "flag_"
                + country.getCode().toLowerCase(Locale.ENGLISH);
        int drawableId = getResId(drawableName);
        country.setFlag(drawableId);
        cell.imageView.setImageResource(getResId(drawableName));
        return cellView;
    }

    private static class Cell {
        TextView textView;
        ImageView imageView;
        TextView code_text;
    }

}
