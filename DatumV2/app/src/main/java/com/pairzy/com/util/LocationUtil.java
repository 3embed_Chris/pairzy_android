package com.pairzy.com.util;

import android.app.Activity;
import android.location.LocationManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * Created by ankit on 11/5/18.
 */

public class LocationUtil {
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private Activity activity;

    public LocationUtil(Activity activity) {
        this.activity = activity;
    }

    public boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            return false;
        }
        return true;
    }


    public boolean isLocationEnabled() {
        LocationManager lm = (LocationManager) activity.getSystemService(activity.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        return gps_enabled && network_enabled;
    }

    public  String removeNullFromAddress(String formatted_address) {
        String[] formatted_array = formatted_address.split(",");
        StringBuilder formatted = new StringBuilder();
        boolean attachComma = false;
        for (int i = 0; i < (formatted_array.length); i++) {

            if ((formatted_array[i].trim() != null) && (!formatted_array[i].trim().equals("")) && (!formatted_array[i].trim().equals("null"))) {
                if (attachComma)
                    formatted.append(",");
                formatted.append(formatted_array[i]);
                attachComma = true;
            }

        }
        return formatted.toString();
    }

}
