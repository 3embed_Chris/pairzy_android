package com.pairzy.com.MyProfile;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.androidinsta.com.ImageData;
import com.androidinsta.com.InstaLoginHolder;
import com.androidinsta.com.InstaReponseHolder;
import com.androidinsta.com.InstagramManger;
import com.pairzy.com.AppController;
import com.pairzy.com.MyProfile.Model.EditProfileData;
import com.pairzy.com.MyProfile.Model.InstaMediaAdapter;
import com.pairzy.com.MyProfile.Model.InstaMediaClickCallback;
import com.pairzy.com.MyProfile.Model.MomentClickCallback;
import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.MyProfile.Model.MomentsGridAdapter;
import com.pairzy.com.MyProfile.Model.MyPreferenceAdapter;
import com.pairzy.com.MyProfile.Model.ProfileModel;
import com.pairzy.com.MyProfile.Model.ProfileUpdateType;
import com.pairzy.com.MyProfile.Model.ProfileViewModel;
import com.pairzy.com.R;
import com.pairzy.com.data.model.PrefData;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.passportLocation.model.PassportLocation;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.CustomObserver.LocationObserver;
import com.pairzy.com.util.CustomObserver.SettingsDataChangeObserver;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.accountKit.AccountKitManager;
import com.pairzy.com.util.accountKit.AccountKitManagerImpl;
import com.pairzy.com.util.boostDialog.BoostAlertCallback;
import com.pairzy.com.util.boostDialog.BoostDialog;
import com.pairzy.com.util.boostDialog.Slide;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.suresh.innapp_purches.InAppCallBack;
import com.suresh.innapp_purches.InnAppSdk;
import com.suresh.innapp_purches.SkuDetails;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h2>MyProfilePagePresenter</h2>
 * <P>
 *  Presenter class for the User Profile page.
 * </P>
 * @since  4/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */

public class MyProfilePagePresenter implements MyProfilePageContract.Presenter,ViewPager.OnPageChangeListener,MyPreferenceAdapter.AdapterClickCallback,
        ProfileViewModel.OnUserDetailClickCallback,ProfileViewModel.OnAboutTextChangeCallback,BoostAlertCallback, MomentClickCallback,
        InstaMediaClickCallback
{
    public static final String GENDER_DATA = "gender_data";
    public static final String DOB_DATA = "dob_data";
    public static final String PHONE_DATA = "phone_data";
    public static final String COUNTRY_CODE = "country_code";
    public static final String PHONE_DATA_BUNDLE = "phone_data_bundle";
    public static final String EMAIL_DATA = "email_data";
    public static final String NAME_DATA = "name_data";

    @Inject
    InstagramManger instagramManger;
    @Inject
    Activity activity;
    @Inject
    MyProfilePageContract.View view;
    @Inject
    NetworkService service;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    ProfileModel model;
    @Inject
    DatumProgressDialog datumProgressDialog;
    @Inject
    Utility utility;
    @Inject
    LoadingProgress progress;
    @Inject
    ArrayList<Slide> slideArrayList;
    @Inject
    BoostDialog boostDialog;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    LoadingProgress loadingProgress;
    @Inject
    LocationObserver locationObserver;
    @Inject
    SettingsDataChangeObserver settingsDataChangeObserver;
    @Inject
    AccountKitManager accountKitManager;

    private CompositeDisposable compositeDisposable;
    private View itemView;
    private TextView tvName,tvGender,tvAge,tvPhone,tvEmail,tvLocation;
    private boolean isLocationChanged = false;
    private boolean isSettingsDataChange = false;
    private boolean ageSwitchChangeFromApi = false;
    private boolean distSwitchChangeFromApi = false;
    private String currentCountryCode;
    private String currentPhone;

    @Inject
    MyProfilePagePresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(MyProfilePageContract.View view)
    {}

    @Override
    public void dropView() {}

    @Override
    public void observeLocationChange()
    {
        locationObserver.getObservable().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PassportLocation>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(PassportLocation value) {
                        if(value != null){
                            try {
                                if(tvLocation != null) {
                                    tvLocation.setText(value.getSubLocationName());
                                    isLocationChanged = true;
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    @Override
    public void getUserData()
    {

        if(networkStateHolder.isConnected()) {
            if (view != null)
                view.showToolbarTitle(utility.formatString(dataSource.getName()));

            if (view != null)
                view.addLoadingView(model.getLoadingView());

            service.getMyProfile(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {

                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            try {
                                if (value.code() == 200) {
                                    String data = value.body().string();
                                    model.parseData(data);
                                    if (view != null) {
                                        view.showToolbarTitle(utility.formatString(dataSource.getName()));
                                        view.addChildView(model.getDetailsView(MyProfilePagePresenter.this));
                                        ageSwitchChangeFromApi = true;
                                        view.invalidateAgeSwitch(model.isAgeHidden());
                                        distSwitchChangeFromApi = true;
                                        view.invalidateDistSwitch(model.isDistHidden());
                                    }
                                    updateUserStoredDetails();
                                } else if (value.code() == 401) {
                                    AppController.getInstance().appLogout();
                                } else {
                                    if (view != null)
                                        view.addLoadingView(model.getErrorOnLoadingView(activity.getString(R.string.server_error),MyProfilePagePresenter.this));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                if (view != null)
                                    view.showError(e.getMessage());
                                if (view != null)
                                    view.addLoadingView(model.getErrorOnLoadingView(activity.getString(R.string.server_error),MyProfilePagePresenter.this));

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null)
                                view.addLoadingView(model.getErrorOnLoadingView(activity.getString(R.string.server_error),MyProfilePagePresenter.this));
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }else {

            if (view != null)
                view.addLoadingView(model.getErrorOnLoadingView(activity.getString(R.string.no_internet_error), MyProfilePagePresenter.this));
        }
    }

    @Override
    public void initListener()
    {
        view.intPageChangeListener(this);
    }

    @Override
    public void updateUserStoredDetails()
    {
        model.createMediaList();
        if(view != null)
            view.updateSegmentBar(model.getMediaTotal(),0);
        if(view != null)
            view.showMomentData(model.getMomentList());
    }

    @Override
    public void doInstagramLogin()
    {
        if(instagramManger.isUserLoggedIn())
        {
            try
            {
                InstaLoginHolder data=instagramManger.getUserDetails();
                updateInstagramDetails(data.getUser_id(),data.getUser_name(),data.getToken());
            } catch (Exception e)
            {
                doActualInstaLogin();
            }
        }else
        {
            doActualInstaLogin();
        }
    }

    @Override
    public void parseEditedPreferenceData(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == -1) { //RESULT_OK
            PrefData prefData = (PrefData) data.getSerializableExtra("pref_data");
            if(prefData != null){
                if(itemView != null) {
                    ArrayList<String> selectedValue = prefData.getSelectedValues();
                    TextView tvSelected = itemView.findViewById(R.id.selected_text);
                    TextView tvCount = itemView.findViewById(R.id.count_data);
                    if(selectedValue != null) {

                        if(selectedValue.isEmpty()){
                            tvSelected.setText(R.string.not_set_Text);
                            tvCount.setVisibility(View.GONE);
                        }

                        if(selectedValue.size() >= 1)
                            tvSelected.setText(selectedValue.get(0));
                        if(selectedValue.size() >= 2) {
                            tvCount.setText(String.valueOf("+" + (selectedValue.size() - 1)));
                            tvCount.setVisibility(View.VISIBLE);
                        }
                        else
                            tvCount.setVisibility(View.GONE);
                        model.keepEditedPrefData(prefData);
                    }
                    else {
                        tvSelected.setText(R.string.not_set_Text);
                        tvCount.setVisibility(View.GONE);
                    }
                }
            }
            if(view != null)
                view.showTickMark(true);
        }
    }

    @Override
    public void parseEditedPhone(int requestCode, int resultCode, Intent data) {
        if(resultCode == -1){
            String editedPhone = data.getStringExtra(PHONE_DATA);
            if(editedPhone != null){
                if(tvPhone != null){
                    if(!editedPhone.equals(tvPhone.getText().toString())){
                        if(view != null)
                            view.showTickMark(true);
                        tvPhone.setText(editedPhone);
                        model.keepEditedPhone(editedPhone);
                    }
                }
            }
        }
    }

    @Override
    public void parseEditedDob(int requestCode, int resultCode, Intent data) {
        if(resultCode == -1){
            Double editedDobMiliSec = data.getDoubleExtra(DOB_DATA,0);
            if(editedDobMiliSec != 0){
                if(tvAge != null){
                    if(editedDobMiliSec != Double.parseDouble(dataSource.getBirthDate())){
                        if(view != null)
                            view.showTickMark(true);
                        tvAge.setText(String.valueOf(utility.getAgeFromDob(editedDobMiliSec)));
                        model.keepEditedDob(editedDobMiliSec);
                    }
                }
            }
        }
    }

    @Override
    public void parseEditedEmail(int requestCode, int resultCode, Intent data) {
        if(resultCode == -1){
            String editedEmail = data.getStringExtra(EMAIL_DATA);
            if(editedEmail != null){
                if(tvEmail != null){
                    if(!editedEmail.equals(tvEmail.getText().toString())) {
                        if(view != null)
                            view.showTickMark(true);
                        tvEmail.setText(editedEmail);
                        model.keepEditedEmail(editedEmail);
                    }
                }
            }
        }
    }

    @Override
    public void parseEditedName(int requestCode, int resultCode, Intent data) {
        if(resultCode == -1){
            String editedName = data.getStringExtra(NAME_DATA);
            if(editedName != null){
                if(tvName != null){
                    if(!editedName.equals(tvName.getText().toString())) {
                        tvName.setText(editedName);
                        if(view != null)
                            view.showTickMark(true);
                        model.keepEditedName(editedName);
                    }
                }
            }
        }
    }

    @Override
    public void parseEditedGender(int requestCode, int resultCode, Intent data) {
        if(resultCode == -1){
            String editedGender = data.getIntExtra(GENDER_DATA,2)==1?
                    activity.getResources().getString(R.string.male_text):
                    activity.getResources().getString(R.string.female_text);
            if(tvGender != null){
                if(!editedGender.equals(tvGender.getText().toString())) {
                    if(view != null)
                        view.showTickMark(true);
                    tvGender.setText(editedGender);
                    model.keepEditedGender(editedGender);
                }
            }
        }
    }

    @Override
    public void parseEditedPhotoVideos(int requestCode, int resultCode, Intent data) {
        if(resultCode == -1){
            updateUserStoredDetails();
            model.keepEditPhotoVideos();
            if(view != null){
                view.showTickMark(true);
            }
        }
    }

    @Override
    public void callEditProfileApi() {
        if(networkStateHolder.isConnected()) {
            progress.show();
            EditProfileData editProfileDataBody = model.getMapBody();
            service.editProfile(dataSource.getToken(), model.getLanguage(),editProfileDataBody)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            progress.cancel();
                            if (value.code() == 200) {
                                //required in app setting page.
                                isSettingsDataChange = true;
                                model.saveEditedProfile();
                                dataSource.setProfileUpdateType(ProfileUpdateType.DEFAULT.value);
                                if (view != null)
                                    view.finishActivity();
                            } else if (value.code() == 401) {
                                AppController.getInstance().appLogout();
                            } else {
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                } catch (Exception e) {
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            progress.cancel();
                            if (view != null)
                                view.showError(activity.getString(R.string.failed_to_update_profile));
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        } else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void saveDistancePref(boolean dontShow) {
        if(dataSource.getSubscription() != null){
            if(model.saveDistancePref(dontShow)){
                if(view != null)
                    view.showTickMark(true);
            }
        }
        else{
            if(dontShow != model.isDistHidden()) {
                if (view != null)
                    view.invalidateDistSwitch(model.isDistHidden());
            }
            if(distSwitchChangeFromApi){
                distSwitchChangeFromApi = false;
            }else {
                launchBoostDialog();
            }
        }
    }

    @Override
    public void saveAgePref(boolean dontShow) {
        if(dataSource.getSubscription() != null){
            if(model.saveAgePref(dontShow)){
                if(view != null)
                    view.showTickMark(true);
            }
        }
        else{
            if(dontShow != model.isAgeHidden()) {
                if (view != null)
                    view.invalidateAgeSwitch(model.isAgeHidden());
            }
            if(ageSwitchChangeFromApi){
                ageSwitchChangeFromApi = false;
            }else {
                launchBoostDialog();
            }
        }
    }

    /*
     *Doing actual instagram login */
    private void doActualInstaLogin()
    {
        instagramManger.doLogin(activity,null)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<InstaLoginHolder>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(InstaLoginHolder value)
                    {
                        updateInstagramDetails(value.getUser_id(),value.getUser_name(),value.getToken());
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        if(view!=null)
                            view.showError(e.getMessage());
                    }
                    @Override
                    public void onComplete() {}
                });
    }

    @Override
    public void updateInstagramDetails(String userId,String name,String token)
    {
        if(networkStateHolder.isConnected()) {
            datumProgressDialog.isCancelable(false);
            datumProgressDialog.show();
            service.updateInstagramId(dataSource.getToken(), model.getLanguage(), model.updateInstaId(userId, name, token))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                getUserDetails();
                            } else if (value.code() == 401) {
                                AppController.getInstance().appLogout();
                            } else {
                                datumProgressDialog.cancel();
                                if (view != null)
                                    view.showError(activity.getString(R.string.failed_to_update_insta_id));
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            datumProgressDialog.cancel();
                            if (view != null)
                                view.showError(activity.getString(R.string.failed_to_update_insta_id));
                        }

                        @Override
                        public void onComplete() {
                        }
                    });
        }else {
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    /**
     * Getting media data
     */
    @Override
    public   void getUserDetails()
    {
        instagramManger.getUserRecentPost()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<InstaReponseHolder>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(InstaReponseHolder value)
                    {
                        datumProgressDialog.cancel();
                        List<List<ImageData>> list_data=model.prepareInstaResponse(value.getImageData());
                        if(view!=null)
                            view.updateInstaData(list_data);
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        datumProgressDialog.cancel();
                        if(view!=null)
                            view.showError(e.getMessage());
                    }
                    @Override
                    public void onComplete() {}
                });
    }


    @Override
    public boolean isLocationCahnged()
    {
        return isLocationChanged;
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position)
    {
        view.updateSegmentBar(model.getMediaTotal(),position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {}

    @Override
    public void onPreferenceClick(View itemView, PrefData prefData) {
        this.itemView = itemView;
        if(view != null){
            if(prefData != null)
                view.launchEditPreference(prefData);
        }
    }

    @Override
    public void onNameClick(TextView tvName) {
        this.tvName = tvName;
        if(view != null){
            view.launchNameScreen(tvName.getText().toString());
        }
    }

    @Override
    public void onGenderClick(TextView tvGender) {
        this.tvGender = tvGender;
        if(view != null){
            view.launchGenderScreen(tvGender.getText().toString());
        }
    }

    @Override
    public void onAgeClick(TextView tvAge) {
        this.tvAge = tvAge;
        if(view != null){
            view.launchDobScreen(dataSource.getBirthDate());
        }
    }

    /**
     * <p>
     *     Update the user entered mobile number to server.
     * </p>
     * @param countryCode countryCode.
     * @param phno  phoneNumber.
     * @param otp Otp.
     */
    private void callVerifyOptApiToChangeNum(String countryCode, String phno, String otp) {
        if(networkStateHolder.isConnected()) {
            loadingProgress.show();
            service.verifyOtpToChangeNumber(dataSource.getToken(), model.getLanguage(), model.getVerifyOptForChangeNumData(countryCode, phno, otp))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> result) {
                            try {
                                loadingProgress.cancel();
                                if (result.code() == 200) {
                                    String data = result.body().string();
                                    model.updatePhoneNumber(currentCountryCode,currentPhone);
                                    if(tvPhone!= null)
                                        tvPhone.setText(currentCountryCode+currentPhone);
                                } else if (view != null)
                                    view.showError(model.getError(result));
                            } catch (Exception e) {
                                if(view != null)
                                    view.showError(e.getMessage());
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if (view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
    }

    /**
     * <p>
     *     Sent otp to @param mobile_number for verification.
     * </p>
     * @param mobile_number
     */
    private void sendOtp(String mobile_number)
    {
        if(!networkStateHolder.isConnected())
        {
            if(view!=null)
            {
                view.showMessage(activity.getString(R.string.no_internet_error));
            }
        }else
        {
            progress.show();
            service.requestOtp(model.getAuthorization(), model.getLanguage(), model.requestOtpParams(mobile_number, true))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> result)
                        {
                            try
                            {
                                if (result.code() == 200)
                                {
                                    callVerifyOptApiToChangeNum(currentCountryCode,currentPhone,"111111");
                                }
                                else
                                {
                                    progress.cancel();
                                    if(view!=null)
                                        view.showMessage(model.getError(result));
                                }
                            }catch (Exception e)
                            {
                                progress.cancel();
                                if(view!=null)
                                    view.showMessage(activity.getString(R.string.failed_sendOtp));
                            }
                        }
                        @Override
                        public void onError(Throwable errorMsg)
                        {
                            progress.cancel();
                            if(view!=null)
                                view.showMessage(activity.getString(R.string.failed_sendOtp));
                        }
                        @Override
                        public void onComplete() {}
                    });
        }
    }

    /**
     * <p>
     *     Call the api for checking weather this mobile is already registered on server or
     *     Not.
     * </p>
     * @param mobile_number
     */
    private void doApiCall(String mobile_number)
    {
        if(!networkStateHolder.isConnected())
        {
            if(view!=null)
            {
                loadingProgress.show();
            }
        }else
        {
            service.verifyMobile(model.getAuthorization(), model.getLanguage(),model.verifyParams(mobile_number))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> result)
                        {
                            try
                            {
                                if(result.code()==200)
                                {
                                    if(view != null)
                                        view.showError(activity.getString(R.string.phone_already_exist_msg));
                                }else if(result.code() == 412)
                                {
                                    sendOtp(currentCountryCode+currentPhone);
                                }else
                                {
                                    if(view != null)
                                        view.showError(model.getError(result));
                                }
                            }catch (Exception e)
                            {
                                e.printStackTrace();
                                if(view != null)
                                    view.showError(e.getMessage());

                            }
                        }
                        @Override
                        public void onError(Throwable errorMsg) {
                            if(view != null)
                                view.showError(errorMsg.getMessage());

                        }
                        @Override
                        public void onComplete() {}
                    });
        }
    }

    @Override
    public void onPhoneClick(TextView tvPhone) {
        this.tvPhone = tvPhone;
//        if(view != null){
//            Bundle bundle = new Bundle();
//            bundle.putString(COUNTRY_CODE, dataSource.getCountryCode());
//            bundle.putString(PHONE_DATA, dataSource.getMobileNumber());
//            view.launchMobileVerifyActivity(bundle);
//        }
        accountKitManager.initPhoneLogin(activity,dataSource.getCountryCode(),dataSource.getMobileNumber(),new AccountKitManagerImpl.GetPhoneDetailCallback() {
            @Override
            public void onSuccess(String countryCode, String phone) {
                currentCountryCode =countryCode;
                currentPhone=phone;
                doApiCall(currentCountryCode+currentPhone);
            }

            @Override
            public void onError(String error) {
                if(view != null)
                    view.showMessage(error);
            }
        });
    }

    @Override
    public void onEmailClick(TextView tvEmail) {
        this.tvEmail = tvEmail;
        if(view != null){
            view.launchEmailScreen(tvEmail.getText().toString());
        }
    }

    @Override
    public void onLocationClick(TextView tvLocation)
    {
        this.tvLocation = tvLocation;
        if(dataSource.getSubscription()!= null)
        {
            if(view!= null)
            {
                view.launchLocationScreen();
            }
        }else
        {
            launchBoostDialog();
        }
    }



    @Override
    public void onTextChange(String newText) {
        model.keepNewAboutText(newText);
        if(view != null)
            view.showTickMark(true);
    }


    @Override
    public void launchBoostDialog() {
        getBoostDialog();
    }

    private void getBoostDialog()
    {
        if(!model.isSubsPlanEmpty())
        {
            model.selectMiddleItem();
            boostDialog.showAlert(this, model.getSubsPlanList(), slideArrayList);
        }
        else {
            getSubsPlan(true);
        }
    }

    private void getSubsPlan(boolean showDialog)
    {
        if(networkStateHolder.isConnected())
        {
            loadingProgress.show();
            service.getSubsPlans(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>()
                    {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value)
                        {
                            if(value.code() == 200)
                            {
                                try
                                {
                                    model.parseSubsPlanList(value.body().string());
                                    if (model.isSubsPlanEmpty())
                                    {
                                        loadingProgress.cancel();
                                        if(view != null)
                                            view.showError(activity.getString(R.string.subs_list_empty));
                                    }else
                                    {
                                        model.selectMiddleItem();
                                        List<String> actual_ids=model.collectsIds();
                                        InnAppSdk.getVendor().getProductDetails(activity,actual_ids, InnAppSdk.Type.SUB, new InAppCallBack.DetailsCallback()
                                        {
                                            @Override
                                            public void onSuccess(List<SkuDetails> list, List<String> errorList)
                                            {
                                                loadingProgress.cancel();
                                                if(list.size()>0)
                                                {
                                                    model.updateDetailsData(list);
                                                    if(!model.isSubsPlanEmpty())
                                                    {
                                                        if(showDialog) {
                                                            model.selectMiddleItem();
                                                            boostDialog.showAlert(MyProfilePagePresenter.this, model.getSubsPlanList(), slideArrayList);
                                                        }
                                                    }
                                                }else
                                                {
                                                    model.clearProductList();
                                                    if(view != null)
                                                        view.showError(activity.getString(R.string.subs_list_empty));
                                                }
                                            }
                                        });
                                    }
                                }catch (Exception e)
                                {
                                    loadingProgress.cancel();
                                    if(view != null)
                                        view.showError(activity.getString(R.string.subs_list_empty));
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else
                            {
                                loadingProgress.cancel();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if (view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }


    private void callSubscriptionApi(String planId){

        if(networkStateHolder.isConnected())
        {
            loadingProgress.show();
            Map<String, Object> body = new HashMap<>();
            body.put(ApiConfig.SubsPlan.PLAN_ID,planId);
            body.put(ApiConfig.SubsPlan.PAYMENT_GETWAY_TAX_ID,"1234");
            body.put(ApiConfig.SubsPlan.PAYMENT_GETWAY, AppConfig.DEFAULT_PAYMENT_GETWAY);
            body.put(ApiConfig.SubsPlan.USER_PURCHASE_TIME,String.valueOf(System.currentTimeMillis()));
            service.postSubscription(dataSource.getToken(), model.getLanguage() , body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value)
                        {
                            loadingProgress.cancel();
                            if(value.code() == 200){
                                try {
                                    model.parseSubscription(value.body().string());
                                }catch (IOException e){}
                                if(view != null)
                                    view.showMessage(activity.getString(R.string.plan_purchase_successful));
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception ignored){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view != null)
                                view.showError(e.getMessage());
                        }
                    });
        } else{
            loadingProgress.cancel();
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }

    }

    @Override
    public void onInappSubscribe(int position)
    {
        String purchaseId=model.getSubsPurchaseId(position);
        if(!TextUtils.isEmpty(purchaseId))
        {
            InnAppSdk.getVendor().purchasedItem(activity, purchaseId, InnAppSdk.Type.SUB, dataSource.getUserId(), new InAppCallBack.OnPurchased()
            {
                @Override
                public void onSuccess(String productId)
                {
                    String id=model.extractIDFromKey(productId);
                    if(TextUtils.isEmpty(id))
                    {
                        if(view!=null)
                            view.showError("Error on updating !");
                    }else
                    {
                        callSubscriptionApi(id);
                    }
                }
                @Override
                public void onError(String id, String error)
                {
                    if(view != null)
                        view.showError(error);
                }
            });
        }else
        {
            if(view != null)
                view.showError("Error on purchasing "+purchaseId);
        }
    }

    @Override
    public void onNoThanks(){}

    @Override
    public void onRetry() {
        getUserData();
    }

    @Override
    public void dispose() {
        compositeDisposable.clear();
    }

    @Override
    public void notifySettingsDataChange() {
        if(isSettingsDataChange){
            isSettingsDataChange = false;
            settingsDataChangeObserver.publishData(true);
        }
    }

    @Override
    public void handleDistanceSwitchClick() {
        if(dataSource.getSubscription() == null){
            if(view != null)
                view.invalidateDistSwitch(false);
            getSubsPlan(false);
        }
        else{
            //no need to show subscribed dialog.
            //launchBoostDialog();
        }
    }

    @Override
    public void handleAgeSwitchClick() {
        if(dataSource.getSubscription() == null){
            if(view != null)
                view.invalidateAgeSwitch(false);
            getSubsPlan(false);
        }
        else{
            //no need to show subscribed dialog.
            //launchBoostDialog();
        }
    }

    @Override
    public void disconnectInstagram() {
        instagramManger.loggedOut();
        if(view != null)
            view.hideInstaMediaUi();
    }

    @Override
    public void setMomentClickCallback(MomentsGridAdapter momentsAdapter) {
        momentsAdapter.setClickCallback(this);
    }

    @Override
    public void loadMomentScreen() {
        ArrayList<MomentsData> momentsDataList = model.getMomentList();
        if(view != null)
            view.launchMomentScreen(momentsDataList,0 /*default post position*/);
    }

    @Override
    public void parseMomentDataList(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK) {
            ArrayList<MomentsData> momentsData = data.getParcelableArrayListExtra(ApiConfig.MOMENTS.MOMENTS_LIST);
            model.updateMomentData(momentsData);
            if (view != null)
                view.showMomentData(model.getMomentList());
        }
    }

    @Override
    public void setInstaAdapterCallback(InstaMediaAdapter mediaAdapter) {
        mediaAdapter.setCallback(this);
    }


    @Override
    public void onMomentClick(int position) {
        ArrayList<MomentsData> momentsDataList =  model.getMomentList();
        if(view != null)
            view.launchMomentVerticalScreen(momentsDataList,position);
    }

    @Override
    public void onInstaPhotoClick(int page, int position) {
        if(view != null) {
            view.launchInstaPhotoPreviewScreen(model.getInstaMedia(),model.getExactPosition(page,position));
        }
    }
}
