package com.pairzy.com.util.DatumTextView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import com.pairzy.com.R;
/**
 * @since  3/26/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class GradientTextView extends androidx.appcompat.widget.AppCompatTextView
{
    public GradientTextView(Context context)
    {
        super(context);
    }

    public GradientTextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public GradientTextView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (changed) {
            getPaint().setShader(new LinearGradient(0, 0, getWidth(), getHeight(),
                    ContextCompat.getColor(getContext(), R.color.datum),
                    ContextCompat.getColor(getContext(), R.color.orange_lite),
                    Shader.TileMode.CLAMP));
        }
    }
}
