package com.pairzy.com.coinWallet.coinOut;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.coinWallet.CoinWalletPresenter;
import com.pairzy.com.coinWallet.Model.AllCoinAdapter;
import com.pairzy.com.util.TypeFaceManager;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 *<h>OutCoinFrag</h>
 * <p>Shows Out coin history list.</p>
 *@author 3Embed.
 *@since 30/5/18.
 */

public class OutCoinFrag extends DaggerFragment implements OutCoinContract.View,SwipeRefreshLayout.OnRefreshListener {

    @Inject
    OutCoinPresenter presenter;
    @Inject
    AllCoinAdapter allCoinAdapter;
    @Inject
    LinearLayoutManager linearLayoutManager;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    CoinWalletPresenter coinWalletPresenter;

    @BindView(R.id.recycler_coin)
    RecyclerView recyclerCoin;
    @BindView(R.id.loading_progress)
    ProgressBar pbLoading;
    @BindView(R.id.loading_text)
    TextView tvLoading;
    @BindView(R.id.error_icon)
    ImageView ivErrorIcon;
    @BindView(R.id.error_msg)
    TextView tvErrorMsg;
    @BindView(R.id.empty_data)
    RelativeLayout rlEmptyData;
    @BindView(R.id.loading_view)
    RelativeLayout rlLoadingView;
    @BindView(R.id.no_more_data)
    TextView tvNoMoreDataTitle;
    @BindView(R.id.empty_details)
    TextView tvEmptyDetail;
    @BindView(R.id.parent_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.btn_retry)
    Button btnRetry;

    private Unbinder unbinder;
    
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tab_coin,container,false);
        unbinder = ButterKnife.bind(this,rootView);
        initView();
        applyFont();
        return rootView;
    }

    private void applyFont() {
        tvNoMoreDataTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvEmptyDetail.setTypeface(typeFaceManager.getCircularAirBook());
        btnRetry.setTypeface(typeFaceManager.getCircularAirBold());

    }

    private void initView() {
        tvNoMoreDataTitle.setText(getString(R.string.empty_coin_out_title));
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh2, R.color.refresh1,R.color.refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerCoin.setLayoutManager(linearLayoutManager);
        recyclerCoin.setHasFixedSize(false);
        recyclerCoin.setAdapter(allCoinAdapter);
        notifyAdapter();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.takeView(this);
        //presenter.loadCoinList();
    }

    @Override
    public void showLoading() {
        if(tvErrorMsg != null) {
            btnRetry.setVisibility(View.GONE);
            tvErrorMsg.setVisibility(View.GONE);
            ivErrorIcon.setVisibility(View.GONE);
            tvLoading.setVisibility(View.VISIBLE);
            pbLoading.setVisibility(View.VISIBLE);
            rlLoadingView.setVisibility(View.VISIBLE);
            //pendingRecycler.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
        }
    }

    @Override
    public void showNetworkError(String errorMsg) {
        if(tvErrorMsg != null) {
            btnRetry.setVisibility(View.VISIBLE);
            tvErrorMsg.setVisibility(View.VISIBLE);
            tvErrorMsg.setText(errorMsg);
            ivErrorIcon.setVisibility(View.VISIBLE);
            tvLoading.setVisibility(View.GONE);
            pbLoading.setVisibility(View.GONE);
            //pendingRecycler.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void emptyData()
    {
        if(recyclerCoin != null) {
            //pendingRecycler.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void notifyAdapter() {
        if(presenter != null && presenter.isCoinHistoryEmpty()){
            emptyData();
        }
        else {
            if(rlLoadingView != null) {
                allCoinAdapter.notifyDataSetChanged();
                rlLoadingView.setVisibility(View.GONE);
                rlEmptyData.setVisibility(View.GONE);
                recyclerCoin.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        presenter.dropView();
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        coinWalletPresenter.getCoinHistory();
    }

    @OnClick(R.id.btn_retry)
    public void onRetry(){
        onRefresh();
    }
}
