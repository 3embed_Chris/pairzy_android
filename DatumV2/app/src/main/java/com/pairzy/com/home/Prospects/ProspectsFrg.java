package com.pairzy.com.home.Prospects;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.home.HomeContract;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * <h2>ProspectsFrg</h2>
 * A simple {@link Fragment} subclass.
 * @author 3Embed.
 * @version 1.0.
 * @since 05-03-2018.
 */

@ActivityScoped
public class ProspectsFrg extends DaggerFragment implements ProspectsContract.View
{
    private static final int DURATION = 285;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    Activity activity;
    @Inject
    Utility utility;
    @Inject
    ProspectsContract.Presenter presenter;
    @Inject
    HomeContract.Presenter homePresenter;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tab_layout;
    @BindView(R.id.title_text)
    TextView title_text;
    @BindView(R.id.title_content)
    RelativeLayout title_content;
    @BindView(R.id.input_search)
    RelativeLayout input_search;
    @BindView(R.id.input_text)
    EditText input_text;
    @BindView(R.id.tv_coin_balance)
    TextView tvCoinBalance;


    private FragmentManager fragmentManager;
    private ProspectsAdapter adapter;
    private Unbinder unbinder;
    private String[] prospectTabTitles;
    public ProspectsFrg() {}


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_prospects_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder= ButterKnife.bind(this,view);
        presenter.takeView(this);
        initUi();
        homePresenter.updateCoinBalance();
    }

    /*
    * initialization of the xml content*/
    private void initUi()
    {
        tvCoinBalance.setTypeface(typeFaceManager.getCircularAirLight());
        title_text.setTypeface(typeFaceManager.getCircularAirBold());
        input_text.setTypeface(typeFaceManager.getCircularAirBold());

        prospectTabTitles = getResources().getStringArray(R.array.prospect_tab_titles);
        fragmentManager = getChildFragmentManager();
        adapter = new ProspectsAdapter(fragmentManager,prospectTabTitles);
        adapter.notifyDataSetChanged();
        viewPager.setAdapter(adapter);
        tab_layout.setupWithViewPager(viewPager);
        ViewGroup vg = (ViewGroup) tab_layout.getChildAt(0);
        int tabsCount = vg.getChildCount();

        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(typeFaceManager.getCircularAirBold());
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }
            @Override
            public void onPageSelected(int position)
            {
                if(searchViewStatus)
                {
                    handelEditSearchView();
                    handelTabViewVisibility(false);
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }


    @OnClick(R.id.main_layout)
    void onParentClick(){}


    @OnClick(R.id.search_view)
    void onSearchIconClicked()
    {
        title_content.setVisibility(View.GONE);
        input_search.setVisibility(View.VISIBLE);
        input_text.requestFocus();
        utility.openSoftInputKeyInDelay(activity,input_text);
        handelTabViewVisibility(true);
    }


    @OnClick(R.id.cross_button)
    void onCrossClicked()
    {
        input_text.setText("");
    }


    @OnClick(R.id.back_button)
    void onBack()
    {
        handelEditSearchView();
        handelTabViewVisibility(false);
    }


    private void handelEditSearchView()
    {
        input_text.setText("");
        tab_layout.setVisibility(View.VISIBLE);
        input_search.setVisibility(View.GONE);
        title_content.setVisibility(View.VISIBLE);
        utility.closeSpotInputKey(activity,input_text);
    }


    @Override
    public void onResume()
    {
        super.onResume();
        homePresenter.updateCoinBalance();
    }

    @Override
    public void onDestroy()
    {
        unbinder.unbind();
        presenter.dropView();
        super.onDestroy();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void circleReveal(View myView, int posFromRight,boolean containsOverflow, final boolean isShow)
    {
        int width=myView.getWidth();
        if(posFromRight>0)
            width-=(posFromRight*getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material))-(getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material)/ 2);
        if(containsOverflow)
            width-=getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material);
        int cx=width;
        int cy=myView.getHeight()/2;
        Animator anim;
        if(isShow)
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0,(float)width);
        else
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, (float)width, 0);

        anim.setDuration((long)220);
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if(!isShow)
                {
                    super.onAnimationEnd(animation);
                    myView.setVisibility(View.INVISIBLE);
                }
            }
        });

        // make the view visible and start the animation
        if(isShow)
            myView.setVisibility(View.VISIBLE);
        // start the animation
        anim.start();
    }

    private int lastHeight;
    private boolean searchViewStatus;
    private void handelTabViewVisibility(boolean isHide)
    {
        if(isHide)
        {
            searchViewStatus=true;
            lastHeight=tab_layout.getHeight();
            pushTop(tab_layout.getHeight(),0,tab_layout);
        }else
        {
            searchViewStatus=false;
            pushDown(0,lastHeight,tab_layout);
        }
    }

    /* *
       * Pushing to the top position */
    private void pushTop(int current,int end,final View view)
    {
        ValueAnimator heightAnimator = ValueAnimator.ofInt(current,end);
        heightAnimator.setDuration(DURATION);
        heightAnimator.addUpdateListener(animation -> {
            int data_push=(int)animation.getAnimatedValue();
            view.setLayoutParams(new AppBarLayout.LayoutParams(view.getWidth(),data_push));
        });
        heightAnimator.start();
    }

    /* *
     * Pushing to the down*/
    private void pushDown(int current,int end,final View view)
    {
        ValueAnimator heightAnimator = ValueAnimator.ofInt(current,end);
        heightAnimator.setDuration(DURATION);
        heightAnimator.addUpdateListener(animation -> {
            int data_push=(int)animation.getAnimatedValue();
            view.setLayoutParams(new AppBarLayout.LayoutParams(view.getWidth(),data_push));
        });
        heightAnimator.start();
    }

    @Override
    public void showCoinBalance(String coinBalance)
    {
        if(tvCoinBalance != null)
            tvCoinBalance.setText(""+coinBalance);
    }

    @OnClick(R.id.coin_view)
    void onCoinView()
    {
        homePresenter.launchCoinWallet();
    }
}
