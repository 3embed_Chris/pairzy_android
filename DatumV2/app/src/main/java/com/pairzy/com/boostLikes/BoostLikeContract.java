package com.pairzy.com.boostLikes;

import com.pairzy.com.BaseView;
import com.pairzy.com.boostLikes.Model.BoostLikeAdapterCallback;

/**
 * Created by ankit on 7/9/18.
 */

public interface BoostLikeContract {

    interface View extends BaseView{
        void showMessage(String msg);
        void showError(String error);
        void emptyData();
        void onApiError(String message);
        void showProgress();
        void onDataUpdate();
        void openUserProfile(String data);
        void adapterListener(BoostLikeAdapterCallback adapterCallabck);
        void launchCoinWallet();
    }

    interface Presenter{

        void getLikeList();

        void dropView();
        void initAdapterListener();
    }
}
