package com.pairzy.com.util.DeleteAccountDialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import androidx.appcompat.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.Locale;

/**
 * <h2>ChatMenuDialog</h2>
 * <P>
 *
 * </P>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class DeleteAccountDialog
{
    private Activity activity;
    private Dialog dialog;
    private TypeFaceManager typeFaceManager;
    private Utility utility;
    private DeleteAccDialogCallBack callBack;


    public DeleteAccountDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility)
    {
        this.activity=activity;
        this.typeFaceManager=typeFaceManager;
        this.utility=utility;
    }


    public void showDialog(String accountName,DeleteAccDialogCallBack callBack)
    {

        this.callBack = callBack;
        if(dialog != null && dialog.isShowing())
        {
            dialog.cancel();
        }
        @SuppressLint("InflateParams")
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.delete_account_dialog, null);
        builder.setView(dialogView);
        TextView tvDeleteAccount =dialogView.findViewById(R.id.tv_delete_account);
        tvDeleteAccount.setTypeface(typeFaceManager.getCircularAirBold());

        TextView tvDeleteAccountMsg =dialogView.findViewById(R.id.tv_delete_acc_msg);
        tvDeleteAccountMsg.setTypeface(typeFaceManager.getCircularAirBook());
        tvDeleteAccountMsg.setText(String.format(Locale.ENGLISH,"%s %s Pairzy Account?", activity.getString(R.string.delete_acc_msg_text),accountName.toUpperCase()));
        Button onCanceled=dialogView.findViewById(R.id.onCanceled);
        onCanceled.setTypeface(typeFaceManager.getCircularAirBook());
        onCanceled.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();
        });
        Button onDeleteAccount=dialogView.findViewById(R.id.onDeleteAccount);
        onDeleteAccount.setTypeface(typeFaceManager.getCircularAirBook());
        onDeleteAccount.setOnClickListener(view -> {
            if(callBack != null)
                callBack.onDeleteAccount();
            if(dialog!=null)
                dialog.cancel();
        });
        dialog = builder.create();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
    }
}
