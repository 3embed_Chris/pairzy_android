package com.pairzy.com.util.ReportUser;
/**
 * <h2>ChatMenuCallback</h2>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface ReportUserCallBack
{
    void onReportReasonSelect(String reason);
}
