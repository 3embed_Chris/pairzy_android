package com.pairzy.com.MyProfile.ImageItem;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

/**
 * @since  4/4/2018.
 */

public interface ImageItemContract
{
    interface View extends BaseView
    {

    }

    interface Presenter extends BasePresenter<View>
    {

    }
}
