package com.pairzy.com.mobileverify.Model;

/**
 * <h2>TimerCommunicator</h2>
 * @since  3/1/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface TimerCommunicator
{
    void onUpdateTime(String time);

    void onFinished();

    void onError(String error);
}
