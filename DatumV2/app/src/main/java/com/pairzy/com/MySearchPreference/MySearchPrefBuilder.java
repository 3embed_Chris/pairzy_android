package com.pairzy.com.MySearchPreference;

import android.app.Activity;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import com.pairzy.com.dagger.ActivityScoped;
import javax.inject.Named;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
/**
 * <h2>MySearchPrefBuilder</h2>
 * @since  3/7/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public abstract class MySearchPrefBuilder
{
    static final String ACTIVITY_FRAGMENT_MANAGER = "MyPref.FragmentManager";
    @ActivityScoped
    @Binds
    abstract Activity provideActivity(MySearchPref mySearchPref);
    @ActivityScoped
    @Binds
    abstract MySearchPrefContract.View provideView(MySearchPref mySearchPref);

    @ActivityScoped
    @Binds
    abstract MySearchPrefContract.Presenter homePresenter(MySearchPrefPresenter presenter);

    @Provides
    @Named(ACTIVITY_FRAGMENT_MANAGER)
    @ActivityScoped
    static FragmentManager activityFragmentManager(Activity activity)
    {
        return ((AppCompatActivity)activity).getSupportFragmentManager();
    }

}
