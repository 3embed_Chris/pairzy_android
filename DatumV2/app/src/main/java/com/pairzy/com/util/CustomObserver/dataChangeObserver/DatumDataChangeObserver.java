package com.pairzy.com.util.CustomObserver.dataChangeObserver;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;

public class DatumDataChangeObserver {

    private ConnectableObservable<DataChangeType> connectableObservable;
    private ObservableEmitter<DataChangeType> emitor;

    public DatumDataChangeObserver()
    {
        Observable<DataChangeType> observable=Observable.create(e -> emitor=e);
        connectableObservable=observable.publish();
        connectableObservable.share();
        connectableObservable.replay();
        connectableObservable.connect();
    }
    public ConnectableObservable<DataChangeType> getObservable()
    {
        return connectableObservable;
    }


    public void publishData(DataChangeType flag)
    {
        if(emitor!=null)
        {
            emitor.onNext(flag);
        }
    }
}
