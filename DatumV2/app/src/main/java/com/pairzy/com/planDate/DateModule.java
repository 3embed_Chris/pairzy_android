package com.pairzy.com.planDate;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h>CallDateModule class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

@Module
public abstract class DateModule {

    @ActivityScoped
    @Binds
    abstract DateContract.Presenter datePresenter(DatePresenter presenter);

    @ActivityScoped
    @Binds
    abstract DateContract.View dateView(DateActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity dateActivity(DateActivity activity);

}
