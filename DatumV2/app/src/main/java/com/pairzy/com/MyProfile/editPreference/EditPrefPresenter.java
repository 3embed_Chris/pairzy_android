package com.pairzy.com.MyProfile.editPreference;

import android.content.Intent;

import com.pairzy.com.data.model.PrefData;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * <h>EditPrefPresenter</h>
 * @author 3Embed.
 * @since 23/4/18.
 */

public class EditPrefPresenter implements EditPrefContract.Presenter{

    @Inject
    EditPrefContract.View view;

    @Inject
    public EditPrefPresenter(){
    }

    @Override
    public void init() {
        if(view != null) {
            view.applyFont();
        }
    }

    @Override
    public void loadPrefData(Intent intent) {
        PrefData prefData = (PrefData) intent.getSerializableExtra("pref_data");
        if(prefData != null){
            if(view != null)
                view.showPrefData(prefData);
        }
    }

    @Override
    public void showError(String errorMsg) {
        if(view != null)
            view.showError(errorMsg);
    }

    @Override
    public void showMessage(String errorMsg) {
        if(view != null)
            view.showMessage(errorMsg);
    }

    @Override
    public void updatePreferenceData(PrefData currentPrefData) {
        if(view != null)
            view.saveNewPrefData(currentPrefData);
    }

    @Override
    public boolean isChangeDetected(PrefData newPrefData, PrefData oldPrefData) {
        ArrayList<String> newSelection =  newPrefData.getSelectedValues();
        ArrayList<String> oldSelection =  oldPrefData.getSelectedValues();
        if(newSelection == null){
            newSelection = new ArrayList<>();
        }
        if(oldSelection == null){
            oldSelection = new ArrayList<>();
        }

        if(newSelection.size() != oldSelection.size())
            return true;

        for(String newValue : newSelection){
            for(String oldValue : oldSelection) {
                if(!oldValue.equals(newValue))
                    return true;
            }
        }
        return false;
    }
}
