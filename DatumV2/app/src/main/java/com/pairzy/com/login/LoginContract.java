package com.pairzy.com.login;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.pairzy.com.BasePresenter;
import com.facebookmanager.com.FacebookUserDetails;

/**
 * <h2>LoginContract</h2>
 * <p>
 * Contains @{@link LoginActivity}'s @{@link View} and @{@link Presenter}
 * </P>
 *
 * @author 3Embed.
 * @version 1.0.
 * @since 04/01/2018.
 **/

public interface LoginContract {

    interface View {
        void showMessage(String message);

        void facebookError(String error);

        void facebookCancel(String cancel);

        void facebookLoginSuccess();

        void facebookSignUpSuccess();

        void showWebActivity(String title, @NonNull String url);

        void showBottomView();

        void hideBottomView();

        void updateInterNetStatus(boolean status);

        void openRegisterPage(Intent intent);

        void showUpdateDialog(boolean mandatory);

        void dismissUpdateDialog();

        void openSingUp(Bundle bundle);

        void onLogin(Bundle bundle);
    }

    interface Presenter extends BasePresenter<View>
    {
        void initFacebook();
        void initNetworkObserver();
        void facebookUserDetails(String id);
        void facebookApiCall(FacebookUserDetails userDetails);
        void loadWebActivity(String title, @NonNull String url);
        void  showBottomView();
        void hideBottomView();
        void dropView();

        void checkForForceUpdate();
        void initPhoneLogin();
    }
}
