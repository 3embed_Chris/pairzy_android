package com.pairzy.com.commentPost.model;

import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.R;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentPostViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    static private String TAG=CommentPostViewHolder.class.getSimpleName();

    @BindView(R.id.CommentUserProfile)
    SimpleDraweeView userProfile;

    @BindView(R.id.CommentUserText)
    AppCompatTextView userComment;

    @BindView(R.id.CommentUserTime)
    AppCompatTextView userCommentTime;

//    @BindView(R.id.CommentDelete)
//    AppCompatTextView deleteComment;
//
//    @BindView(R.id.CommentEdit)
//    AppCompatTextView editComment;

    private ItemViewCallBack callBack;

    public CommentPostViewHolder(@NonNull View itemView, ItemViewCallBack callBack) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        this.callBack=callBack;
//        editComment.setOnClickListener(this);
//        deleteComment.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (callBack != null) {
            Log.e(TAG, "onClick: "+v.getId()+"  "+this.getAdapterPosition());
            callBack.onViewItemCallBack(v, this.getAdapterPosition());
        }
    }
}
