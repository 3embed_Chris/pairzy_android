package com.pairzy.com.util.boostDialog;

import android.content.Context;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.boostDetail.model.SubsPlan;
import com.pairzy.com.util.TypeFaceManager;
import java.util.ArrayList;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
/**
 * <h>BoostOfferAdapter class</h>
 * @author 3Embed.
 * @since 1/5/18.
 * @version 1.0.
 */
public class BoostOfferAdapter extends RecyclerView.Adapter<BoostOfferAdapter.ViewHolder>{

    private TypeFaceManager typeFaceManager;
    private ArrayList<SubsPlan> offerList;
    private int colorBlack;
    private int colorDatum;
    private int selectedPosition = 0;
    private Animation popAnim;

    public int getSelectedPostion(){
        return selectedPosition;
    }

    @Inject
    BoostOfferAdapter(Context context, TypeFaceManager typeFaceManager, ArrayList<SubsPlan> offerList)
    {
        popAnim = AnimationUtils.loadAnimation(context,R.anim.pop);
        this.typeFaceManager = typeFaceManager;
        this.offerList = offerList;
        colorBlack = context.getResources().getColor(R.color.liteBlack);
        colorDatum = context.getResources().getColor(R.color.datum);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.boost_dialog_item,parent,false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            bindsDialogView(holder);
        }catch (Exception e){}
    }

    private void bindsDialogView(ViewHolder holder)
    {
        int position = holder.getAdapterPosition();
        SubsPlan offer = offerList.get(position);
        holder.tvMonthCount.setText(offer.getPlanName());
        holder.tvOfferPrize.setText(offer.getPrice_text()+"/mon");

        if(offer.isSelected())
        {
            selectedPosition = position;
            String tag = offer.getHeaderTag();
            if(tag != null && !tag.isEmpty()){
                holder.tvOfferHeader.setText(offer.getHeaderTag());
                holder.tvOfferHeader.setVisibility(View.VISIBLE);
            }
            holder.tvMonthCount.setTextColor(colorDatum);
            holder.tvMonthTitle.setTextColor(colorDatum);
            holder.tvOfferPrize.setTextColor(colorDatum);
            holder.rlBoundary.setBackgroundResource(R.drawable.border_datum_square);
            holder.rl_root.setBackgroundColor(Color.TRANSPARENT);
            holder.rl_root.setScaleX(1.02f);
            holder.rl_root.setScaleY(1.02f);
            holder.rlShadowBg.setVisibility(View.VISIBLE);
            startPopAnimation(offer,holder.tvOfferHeader);
        }
        else{
            holder.tvOfferHeader.setVisibility(View.GONE);
            holder.tvMonthCount.setTextColor(colorBlack);
            holder.tvMonthTitle.setTextColor(colorBlack);
            holder.tvOfferPrize.setTextColor(colorBlack);
            holder.rlBoundary.setBackgroundResource(0);
            holder.rl_root.setBackgroundColor(Color.TRANSPARENT);
            holder.rl_root.setScaleX(1.0f);
            holder.rl_root.setScaleY(1.0f);
            holder.rlShadowBg.setVisibility(View.GONE);
        }
    }

    private void startPopAnimation(SubsPlan offer,View view) {
        if(offer.getHeaderTag() != null && !offer.getHeaderTag().isEmpty()){
            view.startAnimation(popAnim);
            popAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    view.clearAnimation();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        @BindView(R.id.offer_header_tv)
        TextView tvOfferHeader;
        @BindView(R.id.offer_month_count_tv)
        TextView tvMonthCount;
        @BindView(R.id.offer_month_title_tv)
        TextView tvMonthTitle;
        @BindView(R.id.offer_prize_tv)
        TextView tvOfferPrize;
        @BindView(R.id.rl_boundary)
        RelativeLayout rlBoundary;
        @BindView(R.id.rl_root)
        RelativeLayout rl_root;
        @BindView(R.id.shadow_bg)
        RelativeLayout rlShadowBg;
        View itemView;
        public ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);
            tvOfferHeader.setTypeface(typeFaceManager.getCircularAirBook());
            tvMonthCount.setTypeface(typeFaceManager.getCircularAirBold());
            tvMonthTitle.setTypeface(typeFaceManager.getCircularAirBold());
            tvOfferPrize.setTypeface(typeFaceManager.getCircularAirBold());
            tvOfferHeader.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v)
        {
            offerList.get(selectedPosition).setSelected(false);
            SubsPlan offer = offerList.get(getAdapterPosition());
            offer.setSelected(true);
            selectedPosition = getAdapterPosition();
            notifyDataSetChanged();
        }

    }
}
