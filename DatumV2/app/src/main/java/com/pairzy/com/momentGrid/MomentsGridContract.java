package com.pairzy.com.momentGrid;

import android.content.Intent;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

/**
 * <h2>MomentsGridContract</h2>
 * <P> this is a interface b.w view and presenter</P>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */

public interface MomentsGridContract {

    interface View extends BaseView{

        void launchMomentListScreen(int positon);

        void notifyAdapter();
    }

    interface Presenter extends BasePresenter<View>
    {

        void handleOnActivityResult(int requestCode, int resultCode, Intent data);
    }
}
