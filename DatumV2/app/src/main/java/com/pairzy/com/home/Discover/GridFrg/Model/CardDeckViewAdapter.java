package com.pairzy.com.home.Discover.GridFrg.Model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.pairzy.com.R;
import com.pairzy.com.data.model.DiscoverTabPositionHolder;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Discover.Model.AgeResponse;
import com.pairzy.com.home.Discover.Model.UserItemPojo;
import com.pairzy.com.home.Discover.Model.UserMediaPojo;
import com.pairzy.com.home.HomeActivity;
import com.pairzy.com.home.HomeModel.HomeAnimation;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.CardDeckView.SwipeFlingAdapterView;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

import java.util.ArrayList;
import java.util.Locale;

/**
 * <h2>MatchMakerSwipeAdapter</h2>
 * <P>
 *
 * </P>
 * @since  2/3/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class CardDeckViewAdapter extends BaseAdapter
{
    private static final String TAG = CardDeckViewAdapter.class.getSimpleName();

    private ArrayList<UserItemPojo> userList;
    private Context mContext;
    private final LayoutInflater mLayoutInflater;
    private TypeFaceManager typeFaceManager;
    private Utility utility;
    private SwipeFlingAdapterView flingContainer;
    private DeckCardItemClicked deckCardItemClicked;
    private DiscoverTabPositionHolder tabPositionHolder;
    private HomeAnimation homeAnimation;

    public CardDeckViewAdapter(
            Context context,
            TypeFaceManager typeFaceManager,
            Utility utility,
            ArrayList<UserItemPojo> users,
            PreferenceTaskDataSource dataSource,
            DiscoverTabPositionHolder tabPostionHolder,
            HomeAnimation homeAnimation
    )
    {
        this.utility=utility;
        this.typeFaceManager=typeFaceManager;
        this.userList = users;
        this.mContext = context;
        //this.datumVideoPlayer = ((HomeActivity)mContext).datumVideoPlayer;
        this.mLayoutInflater = LayoutInflater.from(context);
        this.tabPositionHolder = tabPostionHolder;
        this.homeAnimation = homeAnimation;
    }

    /*
     * Providing the parent view*/
    public void setPareContainer(SwipeFlingAdapterView flingContainer)
    {
        this.flingContainer=flingContainer;
    }

    public void setItemClickListener(DeckCardItemClicked listenr)
    {
        this.deckCardItemClicked=listenr;
    }
    @Override
    public int getCount()
    {
        return userList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return position;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }


    private void populateNativeAdView(UnifiedNativeAd nativeAd,
                                      UnifiedNativeAdView adView) {
        // Some assets are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        NativeAd.Image icon = nativeAd.getIcon();

        if (icon == null) {
            adView.getIconView().setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(icon.getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAd);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View getView(final int position, View cardview, ViewGroup parent)
    {
        EachCardviewItem eachCardviewItem;
        if (cardview == null)
        {
            cardview = mLayoutInflater.inflate(R.layout.card_deck_item, parent, false);
            eachCardviewItem =new EachCardviewItem();
            eachCardviewItem.parent_view=cardview.findViewById(R.id.parent_card_deck_view);
            eachCardviewItem.background = cardview.findViewById(R.id.background);
            eachCardviewItem.video_container=cardview.findViewById(R.id.video_container);
            eachCardviewItem.name_Text=cardview.findViewById(R.id.name_Text);
            eachCardviewItem.user_status=cardview.findViewById(R.id.user_status);
            eachCardviewItem.name_Text.setTypeface(typeFaceManager.getCircularAirBold());
            eachCardviewItem.school_name=cardview.findViewById(R.id.school_name);
            eachCardviewItem.school_name.setTypeface(typeFaceManager.getCircularAirBold());
            eachCardviewItem.super_like_indicator=cardview.findViewById(R.id.super_like_indicator);
            eachCardviewItem.item_swipe_left_indicator=cardview.findViewById(R.id.item_swipe_left_indicator);
            eachCardviewItem.item_swipe_right_indicator=cardview.findViewById(R.id.item_swipe_right_indicator);
            eachCardviewItem.cardImage=cardview.findViewById(R.id.cardImage);
            eachCardviewItem.prev_img=cardview.findViewById(R.id.prev_img);
            eachCardviewItem.next_img=cardview.findViewById(R.id.next_img);
            eachCardviewItem.view_profile=cardview.findViewById(R.id.view_profile);
            eachCardviewItem.segmentedProgressBar=cardview.findViewById(R.id.segmented_progressbar);
            eachCardviewItem.video_play_icon=cardview.findViewById(R.id.video_play_icon);
            eachCardviewItem.content_view=cardview.findViewById(R.id.content_view);
            eachCardviewItem.ivChat = cardview.findViewById(R.id.iv_chat);
            eachCardviewItem.ivProfile = cardview.findViewById(R.id.iv_profile);
            eachCardviewItem.ivSupedlikedMe = cardview.findViewById(R.id.iv_superliked_me);
            eachCardviewItem.cardButtonLayout = cardview.findViewById(R.id.card_button_layout);
            eachCardviewItem.muteButton = cardview.findViewById(R.id.mute_btn);
            eachCardviewItem.pbProgress = cardview.findViewById(R.id.pb_progressbar);

            eachCardviewItem.yesVote = cardview.findViewById(R.id.tvYesVote);
            eachCardviewItem.noVote = cardview.findViewById(R.id.tvNoVote);
            eachCardviewItem.yesVote.setTypeface(typeFaceManager.getCircularAirBold());
            eachCardviewItem.noVote.setTypeface(typeFaceManager.getCircularAirBold());



            if(position == 0)
                EachCardviewItem.ijkVideoView = cardview.findViewById(R.id.ijk_video_view);

            eachCardviewItem.mHudView = cardview.findViewById(R.id.hud_view);

            //addview init
            eachCardviewItem.adView = cardview.findViewById(R.id.ad_view);

            // The MediaView will display a video asset if one is present in the ad, and the
            // first image asset otherwise.
            eachCardviewItem.adView.setMediaView((MediaView) eachCardviewItem.adView.findViewById(R.id.ad_media));

            // Register the view used for each individual asset.
            eachCardviewItem.adView.setHeadlineView(eachCardviewItem.adView.findViewById(R.id.ad_headline));
            eachCardviewItem.adView.setBodyView(eachCardviewItem.adView.findViewById(R.id.ad_body));
            eachCardviewItem.adView.setCallToActionView(eachCardviewItem.adView.findViewById(R.id.ad_call_to_action));
            eachCardviewItem.adView.setIconView(eachCardviewItem.adView.findViewById(R.id.ad_icon));
            eachCardviewItem.adView.setPriceView(eachCardviewItem.adView.findViewById(R.id.ad_price));
            eachCardviewItem.adView.setStarRatingView(eachCardviewItem.adView.findViewById(R.id.ad_stars));
            eachCardviewItem.adView.setStoreView(eachCardviewItem.adView.findViewById(R.id.ad_store));
            eachCardviewItem.adView.setAdvertiserView(eachCardviewItem.adView.findViewById(R.id.ad_advertiser));

            cardview.setTag(eachCardviewItem);
        } else
        {
            eachCardviewItem = (EachCardviewItem)cardview.getTag();
        }
        if(position == 0)
            stopPlayer();

        UserItemPojo temp_data=userList.get(position);

        if(temp_data.isLoading())
        {
            eachCardviewItem.parent_view.setVisibility(View.INVISIBLE);
        }
        else if(temp_data.isAdView()){

            eachCardviewItem.adView.setVisibility(View.VISIBLE);
            eachCardviewItem.video_play_icon.setVisibility(View.GONE);
            eachCardviewItem.muteButton.setVisibility(View.GONE);
            eachCardviewItem.cardButtonLayout.setVisibility(View.GONE);
            UnifiedNativeAd nativeAd = temp_data.getAd();

            if(nativeAd != null)
                populateNativeAdView(nativeAd,eachCardviewItem.adView);

            eachCardviewItem.prev_img.setOnTouchListener(new DeckItemClick()
            {
                @Override
                void onClick() {
                }
                @Override
                void notHandling(MotionEvent event) {
                    flingContainer.getTopCardListener().onTouch(flingContainer.getSelectedView(),event);
                }
            });

            eachCardviewItem.next_img.setOnTouchListener(new DeckItemClick() {
                @Override
                void onClick() {
                }

                @Override
                void notHandling(MotionEvent event) {
                    flingContainer.getTopCardListener().onTouch(flingContainer.getSelectedView(),event);
                }
            });

            eachCardviewItem.view_profile.setOnTouchListener(new DeckItemClick() {
                @Override
                void onClick() {
                }
                @Override
                void notHandling(MotionEvent event)
                {
                    flingContainer.getTopCardListener().onTouch(flingContainer.getSelectedView(),event);
                }
            });
        }
        else {

            if(temp_data.isSuperlikedMe() > 0)
            {
                eachCardviewItem.ivSupedlikedMe.setVisibility(View.VISIBLE);
                eachCardviewItem.ivSupedlikedMe.startAnimation(homeAnimation.getSlowBlinkingAnimation());
            }
            else
            {
                eachCardviewItem.ivSupedlikedMe.setVisibility(View.GONE);
            }

            eachCardviewItem.parent_view.setVisibility(View.VISIBLE);
            if(temp_data.getMedia_list() != null)
            {
                UserMediaPojo media_temp=temp_data.getMedia_list().get(temp_data.getCurrentImagePos());

                if(media_temp.isVideo())
                {
                    eachCardviewItem.cardImage.setImageURI(media_temp.getVideo_thumbnail());
                    eachCardviewItem.cardImage.setVisibility(View.VISIBLE);
                    final String video_path = media_temp.getVideo_url();
                    eachCardviewItem.video_play_icon.setVisibility(View.VISIBLE);

                    /*if(isCardTouched){
                        if(position == 0) {
                            isCardTouched = false;
                            if (temp_data.getCurrentImagePos() == 0 && !HomeActivity.isHomeActivityPaused) {
                                if (datumVideoPlayer != null) {
                                    datumVideoPlayer.playPlayer();
                                }
                            }
                            else{
                                if(datumVideoPlayer != null) {
                                    datumVideoPlayer.pausePlayer();
                                }
                            }
                        }
                    }
                    else*/
                    if(position == 0) {
                        if(HomeActivity.isDiscoverVisible && !HomeActivity.isHomeActivityPaused && tabPositionHolder.getCurrentPosition() == 0){
                            EachCardviewItem.ijkVideoView.setOnPreparedListener(iMediaPlayer -> {
                                if(eachCardviewItem.pbProgress != null)
                                    eachCardviewItem.pbProgress.setVisibility(View.GONE);
                                if(eachCardviewItem.video_play_icon != null)
                                    eachCardviewItem.video_play_icon.setVisibility(View.GONE);
                                if(eachCardviewItem.cardImage != null)
                                    eachCardviewItem.cardImage.setVisibility(View.GONE);
                            });

                            EachCardviewItem.ijkVideoView.setOnCompletionListener(iMediaPlayer -> {

                                if(EachCardviewItem.ijkVideoView != null){
                                    EachCardviewItem.ijkVideoView.seekTo(0);
                                    if(HomeActivity.isDiscoverVisible && !HomeActivity.isHomeActivityPaused && tabPositionHolder.getCurrentPosition()==0){
                                        playPlayer();
                                    }
                                }
                            });

                            EachCardviewItem.ijkVideoView.setOnErrorListener((iMediaPlayer, i, i1) -> {
                                if(eachCardviewItem.pbProgress != null)
                                    eachCardviewItem.pbProgress.setVisibility(View.GONE);
                                if(eachCardviewItem.video_play_icon != null)
                                    eachCardviewItem.video_play_icon.setVisibility(View.VISIBLE);
                                if(eachCardviewItem.cardImage != null)
                                    eachCardviewItem.cardImage.setVisibility(View.VISIBLE);
                                return true;
                            });

                            eachCardviewItem.pbProgress.setVisibility(View.VISIBLE);
                            create_Media_player(eachCardviewItem,create_Handel_video(video_path));
                            eachCardviewItem.video_play_icon.setVisibility(View.GONE);
                        }
                    }
                    eachCardviewItem.video_play_icon.setOnTouchListener(new DeckItemClick() {
                        @Override
                        void onClick()
                        {
                            if(HomeActivity.isDiscoverVisible && !HomeActivity.isHomeActivityPaused && tabPositionHolder.getCurrentPosition() == 0){
                                EachCardviewItem.ijkVideoView.setOnPreparedListener(iMediaPlayer -> {
                                    if(eachCardviewItem.pbProgress != null)
                                        eachCardviewItem.pbProgress.setVisibility(View.GONE);
                                    if(eachCardviewItem.video_play_icon != null)
                                        eachCardviewItem.video_play_icon.setVisibility(View.GONE);
                                    if(eachCardviewItem.cardImage != null)
                                        eachCardviewItem.cardImage.setVisibility(View.GONE);
                                });

                                EachCardviewItem.ijkVideoView.setOnCompletionListener(iMediaPlayer -> {

                                    if(EachCardviewItem.ijkVideoView != null){
                                        EachCardviewItem.ijkVideoView.seekTo(0);
                                        if(HomeActivity.isDiscoverVisible && !HomeActivity.isHomeActivityPaused && tabPositionHolder.getCurrentPosition()==0){
                                            playPlayer();
                                        }
                                    }
                                });

                                EachCardviewItem.ijkVideoView.setOnErrorListener((iMediaPlayer, i, i1) -> {
                                    if(eachCardviewItem.pbProgress != null)
                                        eachCardviewItem.pbProgress.setVisibility(View.GONE);
                                    if(eachCardviewItem.video_play_icon != null)
                                        eachCardviewItem.video_play_icon.setVisibility(View.VISIBLE);
                                    if(eachCardviewItem.cardImage != null)
                                        eachCardviewItem.cardImage.setVisibility(View.VISIBLE);
                                    return true;
                                });

                                eachCardviewItem.pbProgress.setVisibility(View.VISIBLE);
                                create_Media_player(eachCardviewItem,create_Handel_video(video_path));
                                eachCardviewItem.video_play_icon.setVisibility(View.GONE);
                            }
                        }
                        @Override
                        void notHandling(MotionEvent event) {}
                    });

                    eachCardviewItem.muteButton.setOnTouchListener(new DeckItemClick() {
                        @Override
                        void onClick()
                        {
                            //CustomVideoSurfaceView.getInstance(this).toggleMute()                            //create_Media_player(eachCardviewItem.video_container,eachCardviewItem.video_play_icon,video_path);
                            //eachCardviewItem.video_play_icon.setVisibility(View.GONE);
                        }
                        @Override
                        void notHandling(MotionEvent event) {}
                    });

                }else {
                    if(position == 0){
                        //if(temp_data.getCurrentImagePos() != 0){
                        pausePlayer();
                        //}
//                        ArrayList<UserMediaPojo> media_list=temp_data.getMedia_list();
//                        if(!media_list.isEmpty()){
//                            UserMediaPojo userMediaPojo = media_list.get(0);
//                            if(userMediaPojo.isVideo()){
//                                if(isCardTouched){
//                                    isCardTouched = false;
//                                    if(temp_data.getCurrentImagePos() == 0 && !HomeActivity.isHomeActivityPaused) {
//                                        playPlayer();
//                                    }
//                                    else{
//                                        pausePlayer();
//                                    }
//                                }
//                            }
//                        }
                    }
                    eachCardviewItem.cardImage.setImageURI(media_temp.getImage_url());
                    eachCardviewItem.video_play_icon.setVisibility(View.GONE);
                    eachCardviewItem.cardImage.setVisibility(View.VISIBLE);
                }
            }else
            {
                eachCardviewItem.cardImage.setImageURI(temp_data.getProfilePic());
                eachCardviewItem.cardImage.setVisibility(View.VISIBLE);
                temp_data.setCurrentImagePos(1);
            }

            Drawable drawable=temp_data.getOnlineStatus()==0?null:ContextCompat.getDrawable(mContext,R.drawable.online_dot);
            eachCardviewItem.user_status.setImageDrawable(drawable);
            AgeResponse ageResponse = userList.get(position).getAge();

            if(ageResponse != null) {
                if(ageResponse.getIsHidden() == 0) {
                    eachCardviewItem.name_Text.setText(String.format(Locale.ENGLISH, "%s, %d", utility.formatString(userList.get(position).getFirstName()), ageResponse.getValue()));
                }
                else{
                    eachCardviewItem.name_Text.setText(String.format(Locale.ENGLISH, "%s ", utility.formatString(userList.get(position).getFirstName())));
                }
            }

            if(userList.get(position).getYesVotes()!=0){
                eachCardviewItem.yesVote.setVisibility(View.VISIBLE);
                eachCardviewItem.noVote.setVisibility(View.VISIBLE);
                eachCardviewItem.yesVote.setText("Yes "+userList.get(position).getYesVotes()+"%");
                eachCardviewItem.noVote.setText("No "+userList.get(position).getNoVotes()+"%");
            }else {
                eachCardviewItem.yesVote.setVisibility(View.GONE);
                eachCardviewItem.noVote.setVisibility(View.GONE);
            }

            String schoolText = "";
            if(!TextUtils.isEmpty(temp_data.getWork())){
                schoolText = temp_data.getWork();
            }
            else{
                if(!TextUtils.isEmpty(temp_data.getEducation())){
                    schoolText = temp_data.getEducation();
                }
            }

            eachCardviewItem.school_name.setText(utility.formatString(schoolText));
            eachCardviewItem.segmentedProgressBar.setSegmentCount(temp_data.getMedia_list().size());
            eachCardviewItem.segmentedProgressBar.setContainerColor(ContextCompat.getColor(mContext,R.color.black30));
            eachCardviewItem.segmentedProgressBar.setFillColor(ContextCompat.getColor(mContext,R.color.white));
            eachCardviewItem.segmentedProgressBar.playSegment(50);
            eachCardviewItem.segmentedProgressBar.setCompletedSegments(temp_data.getCurrentImagePos()+1);
            eachCardviewItem.prev_img.setOnTouchListener(new DeckItemClick()
            {
                @Override
                void onClick()
                {
                    pausePlayer();
                    loadPrev();
                }
                @Override
                void notHandling(MotionEvent event) {
                    flingContainer.getTopCardListener().onTouch(flingContainer.getSelectedView(),event);
                }
            });

            eachCardviewItem.next_img.setOnTouchListener(new DeckItemClick() {
                @Override
                void onClick() {
                    pausePlayer();
                    loadNextImage();
                }
                @Override
                void notHandling(MotionEvent event) {
                    flingContainer.getTopCardListener().onTouch(flingContainer.getSelectedView(),event);
                }
            });

            eachCardviewItem.view_profile.setOnTouchListener(new DeckItemClick() {
                @Override
                void onClick() {
                    openProfile(eachCardviewItem.content_view);
                }

                @Override
                void notHandling(MotionEvent event)
                {
                    flingContainer.getTopCardListener().onTouch(flingContainer.getSelectedView(),event);
                }
            });

            ((View)eachCardviewItem.ivProfile).setOnTouchListener(new DeckItemClick() {
                @Override
                void onClick() {
                    openProfile(eachCardviewItem.content_view);
                }
                @Override
                void notHandling(MotionEvent event) {
                }
            });

            ((View)eachCardviewItem.ivChat).setOnTouchListener(new DeckItemClick() {
                @Override
                void onClick() {
                    openChat(eachCardviewItem.content_view);
                }
                @Override
                void notHandling(MotionEvent event) {
                }
            });
        }
        return cardview;
    }

    /**
     * stop player
     */
    public static void stopPlayer() {
        Log.d(TAG, "stopPlayer: called");
        if(EachCardviewItem.ijkVideoView != null)
            EachCardviewItem.ijkVideoView.release(true);
    }

    public static void pausePlayer(){
        Log.d(TAG, "pausePlayer: called");
        if(EachCardviewItem.ijkVideoView != null)
            EachCardviewItem.ijkVideoView.pause();
    }


    public static void playPlayer(){
        Log.d(TAG, "playPlayer: called");
        if (EachCardviewItem.ijkVideoView != null) {
            EachCardviewItem.ijkVideoView.start();
        }
    }

    /*
     * Open Chat Screen
     */
    private void openChat(View view)
    {
        UserItemPojo temp=userList.get(0);
        try
        {
            if(temp!=null)
            {
                if(deckCardItemClicked!=null)
                    deckCardItemClicked.openChatScreen(temp,view);
            }
        }catch (Exception ignored){}
    }

    /*
     *Open profile details */
    private void openProfile(View view)
    {
        UserItemPojo temp=userList.get(0);
        try
        {
            if(temp!=null)
            {
                String item_details=utility.getGson().toJson(temp);
                if(deckCardItemClicked!=null)
                    deckCardItemClicked.openUserProfile(item_details,view);
            }
        }catch (Exception ignored){}
    }

    /*
     * Loading the next item*/
    private void loadNextImage()
    {
        //onStopPlayer();
        UserItemPojo temp=userList.get(0);
        ArrayList<UserMediaPojo> media_list=temp.getMedia_list();
        int position=temp.getCurrentImagePos()+1;
        if(position<media_list.size())
        {
            temp.setCurrentImagePos(position);
        }else
        {
            vibrateMobile();
        }
        flingContainer.notifyi_data_setChanged();
        notifyDataSetChanged();
    }

    /*
     *Loading the previous images */
    private void loadPrev()
    {
        UserItemPojo temp=userList.get(0);
        ArrayList<UserMediaPojo> media_list=temp.getMedia_list();
        int position=temp.getCurrentImagePos();
        position=position-1;
        if(position>=0&&position<media_list.size())
        {
            temp.setCurrentImagePos(position);
        }else
        {
            vibrateMobile();
        }
        flingContainer.notifyi_data_setChanged();
        notifyDataSetChanged();
    }

    /*
     * vibrating to notify user that reached end*/
    private void vibrateMobile()
    {
        Vibrator v = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            assert v != null;
            v.vibrate(VibrationEffect.createOneShot(500,VibrationEffect.DEFAULT_AMPLITUDE));
        }else{
            assert v != null;
            v.vibrate(500);
        }
    }

    /*
     * Creating the media player to play for the given url.
     * */
    private void create_Media_player(EachCardviewItem eachCardviewItem, final String path) {
        try
        { if (eachCardviewItem == null || EachCardviewItem.ijkVideoView == null)
        {
            return;
        }
            EachCardviewItem.ijkVideoView.setHudView(eachCardviewItem.mHudView);
            EachCardviewItem.ijkVideoView.setVideoURI(Uri.parse(path));
            EachCardviewItem.ijkVideoView.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * Handling the video quality.*/
    private static String create_Handel_video(String video_url)
    {
        video_url=change_video_foramte(video_url);
        if(video_url.contains(AppConfig.CloudinaryDetails.VIDEO_QUALITY))
        {
            return video_url;
        }
        String key_word="upload";
        int length_key=key_word.length();
        int index=video_url.indexOf("upload");
        if(index>0)
        {
            String firs_sub_String=video_url.substring(0,index+length_key);
            String last_sub_String=video_url.substring(index+length_key);
            return firs_sub_String+AppConfig.CloudinaryDetails.VIDEO_QUALITY +last_sub_String;

        }else
        {
            return video_url;
        }
    }
    /*
     * changing the video format.*/
    private static String change_video_foramte(String video_url)
    {
        if(video_url.contains(AppConfig.CloudinaryDetails.VIDEO_FORMATE))
        {
            return video_url;
        }else
        {
            int index_dot=video_url.lastIndexOf(".");
            String front_part=video_url.substring(0,index_dot+1);
            return front_part+AppConfig.CloudinaryDetails.VIDEO_FORMATE;
        }
    }

}
