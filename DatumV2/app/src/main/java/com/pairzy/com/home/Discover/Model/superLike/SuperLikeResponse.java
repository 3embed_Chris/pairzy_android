package com.pairzy.com.home.Discover.Model.superLike;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SuperLikeResponse {

@SerializedName("message")
@Expose
private String message;
@SerializedName("coinWallet")
@Expose
private CoinWallet coinWallet;

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public CoinWallet getCoinWallet() {
return coinWallet;
}

public void setCoinWallet(CoinWallet coinWallet) {
this.coinWallet = coinWallet;
}

}