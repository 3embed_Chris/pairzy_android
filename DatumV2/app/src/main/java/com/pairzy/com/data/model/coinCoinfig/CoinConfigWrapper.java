package com.pairzy.com.data.model.coinCoinfig;

/**
 * Created by ankit on 7/7/18.
 */

public class CoinConfigWrapper {

    private CoinData coinData;

    public CoinData getCoinData() {
        return coinData;
    }

    public void setCoinData(CoinData coinData) {
        this.coinData = coinData;
    }
}
