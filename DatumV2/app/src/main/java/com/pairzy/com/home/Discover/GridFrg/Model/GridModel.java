package com.pairzy.com.home.Discover.GridFrg.Model;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.source.PreferenceTaskDataSource;

import javax.inject.Inject;

public class GridModel extends BaseModel{

    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    PreferenceTaskDataSource dataSource;

    @Inject
    public GridModel() {
    }


    public boolean isEnoughWalletBalanceToSuperLike() {
        if(coinBalanceHolder.getCoinBalance() != null && coinConfigWrapper.getCoinData() != null){
            int coinRequired = coinConfigWrapper.getCoinData().getSuperLike().getCoin();
            int walletBalance = Integer.parseInt(coinBalanceHolder.getCoinBalance());
            if(coinRequired <= walletBalance){
                return true;
            }
            else{
                return false;
            }
        }
        else {
            return false;
        }
    }


    public boolean isSuperlikeSpendDialogNeedToShow() {
        return dataSource.getShowSuperlikeCoinDialog();
    }
}
