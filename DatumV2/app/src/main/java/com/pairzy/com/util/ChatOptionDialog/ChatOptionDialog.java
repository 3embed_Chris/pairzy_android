package com.pairzy.com.util.ChatOptionDialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.Locale;

/**
 * <h2>ChatMenuDialog</h2>
 * <P>
 *
 * </P>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ChatOptionDialog
{
    private Activity activity;
    private BottomSheetDialog dialog;
    private TypeFaceManager typeFaceManager;
    private Utility utility;
    private ChatOptionCallBack callBack;

    public ChatOptionDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility)
    {
        this.activity=activity;
        this.typeFaceManager=typeFaceManager;
        this.utility=utility;
        dialog = new BottomSheetDialog(activity,R.style.DatumBottomDialog);
    }


    public void showDialog(String user_name,int position, ChatOptionCallBack callBack)
    {
        this.callBack = callBack;
        if(dialog.isShowing())
        {
            dialog.cancel();
        }
        user_name=utility.formatString( user_name);
        @SuppressLint("InflateParams")
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.chat_option_dialog, null);
        Button btnChat=dialogView.findViewById(R.id.btn_chat);
        btnChat.setText(String.format(Locale.ENGLISH,"Chat with %s",user_name));
        btnChat.setTypeface(typeFaceManager.getCircularAirBook());
        btnChat.setOnClickListener(view -> {
            //pending
            if(callBack != null)
                callBack.onChatClick(position);
            if(dialog!=null)
                dialog.cancel();
        });

        Button btnUnmatch=dialogView.findViewById(R.id.btn_unmatch);
        btnUnmatch.setText(String.format(Locale.ENGLISH,"Unmatch %s",user_name));
        btnUnmatch.setTypeface(typeFaceManager.getCircularAirBook());
        btnUnmatch.setOnClickListener(view -> {
            //pending
            if(callBack != null)
                callBack.onUnMatchClick(position);
            if(dialog!=null)
                dialog.cancel();
        });

        Button onCanceled=dialogView.findViewById(R.id.onCanceled);
        onCanceled.setTypeface(typeFaceManager.getCircularAirBook());
        onCanceled.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();
        });
        dialog.setContentView(dialogView);
        dialog.show();
    }
}
