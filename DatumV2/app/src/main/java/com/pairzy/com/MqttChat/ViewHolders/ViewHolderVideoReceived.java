package com.pairzy.com.MqttChat.ViewHolders;
/*
 * Created by moda on 02/04/16.
 */

import android.animation.ObjectAnimator;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.MqttChat.Utilities.AdjustableImageView;
import com.pairzy.com.MqttChat.Utilities.RingProgressBar;
import com.pairzy.com.util.TypeFaceManager;


/**
 * View holder for video received recycler view item
 */
public class ViewHolderVideoReceived extends RecyclerView.ViewHolder {

    private TypeFaceManager typeFaceManager;

    public TextView senderName, time, date, fnf, previousMessage_head, previousMessage_content;


    public ImageView download, cancel,forward,previousMessage_iv;


    public ProgressBar progressBar2;

    public RingProgressBar progressBar;

    public AdjustableImageView thumbnail;

    public RelativeLayout messageRoot, previousMessage_rl;
    public ViewHolderVideoReceived(View view,TypeFaceManager typeFaceManager) {
        super(view);
        this.typeFaceManager = typeFaceManager;

        senderName = (TextView) view.findViewById(R.id.lblMsgFrom);

        forward= (ImageView) view.findViewById(R.id.forward_iv);
        messageRoot=(RelativeLayout) view.findViewById(R.id.message_root);
          /*
         * For message reply feature
         */
        previousMessage_rl = (RelativeLayout) view.findViewById(R.id.initialMessage_rl);
        previousMessage_head = (TextView) view.findViewById(R.id.senderName_tv);
        previousMessage_iv= (ImageView) view.findViewById(R.id.initialMessage_iv);
        previousMessage_content = (TextView) view.findViewById(R.id.message_tv);
        ObjectAnimator animation = ObjectAnimator.ofFloat(forward, "rotationY", 0.0f, 180f);
        animation.setDuration(0);

        animation.start();
        date = (TextView) view.findViewById(R.id.date);
        time = (TextView) view.findViewById(R.id.ts);
        thumbnail = (AdjustableImageView) view.findViewById(R.id.vidshow);


        cancel = (ImageView) view.findViewById(R.id.cancel);

        progressBar2 = (ProgressBar) view.findViewById(R.id.progress2);
        progressBar = (RingProgressBar) view.findViewById(R.id.progress);


        download = (ImageView) view.findViewById(R.id.download);
        fnf = (TextView) view.findViewById(R.id.fnf);
        time.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.ITALIC);
        date.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.ITALIC);
        fnf.setTypeface(typeFaceManager.getCircularAirBook());
        if (previousMessage_head != null) {
            previousMessage_head.setTypeface(typeFaceManager.getCircularAirBold());
            previousMessage_content.setTypeface(typeFaceManager.getCircularAirBook());
        }
    }
}
