package com.pairzy.com.momentGrid;


import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.moments.MomentsActivity;
import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.R;
import com.pairzy.com.momentGrid.model.MomentsGridAdapter;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerAppCompatActivity;

/**
 * <h2>MomentsGridActivity</h2>
 * <P> this is a activity provide vertical view</P>
 * @since 25-06-2019
 * @author Hemanth.
 * @version 1.0.
 */

public class MomentsGridActivity extends DaggerAppCompatActivity implements MomentsGridContract.View {

    private Unbinder unbinder;


    @BindView(R.id.tvMomentsTitle)
    TextView tvMomentsTitle;
    @BindView(R.id.post_list)
    RecyclerView rvMoments;

    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    ArrayList<MomentsData> momentsData;
    @Inject
    MomentsGridAdapter momentsAdapter;
    @Inject
    MomentsGridPresenter presenter;

    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_moments);
        unbinder = ButterKnife.bind(this);
        init();
        applyFont();
    }

    private void applyFont() {
        tvMomentsTitle.setTypeface(typeFaceManager.getCircularAirBold());
    }

    private void init() {
        rvMoments.setHasFixedSize(true);
        rvMoments.setLayoutManager(new GridLayoutManager(this,3));

        ArrayList<MomentsData> momentsDataArrayList = getIntent().getParcelableArrayListExtra(ApiConfig.MOMENTS.MOMENTS_LIST);
        momentsData.clear();
        momentsData.addAll(momentsDataArrayList);
        position= getIntent().getIntExtra(ApiConfig.MOMENTS.POSITION,0);
        rvMoments.setHasFixedSize(true);
        rvMoments.scrollToPosition(position);
        rvMoments.setAdapter(momentsAdapter);
        presenter.setAdapterCallback(momentsAdapter);
    }

    @OnClick(R.id.fl_back_button)
    void back(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        intent.putExtra(ApiConfig.MOMENTS.MOMENTS_LIST,momentsData);
        setResult(RESULT_OK,intent);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void launchMomentListScreen(int position) {
        Intent intent = new Intent(this, MomentsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(ApiConfig.MOMENTS.MOMENTS_LIST,momentsData);
        intent.putExtra(ApiConfig.MOMENTS.POSITION,position);
        startActivityForResult(intent, AppConfig.MOMENT_VERTICAL_REQ_CODE);
    }

    @Override
    public void notifyAdapter() {
        momentsAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.handleOnActivityResult(requestCode,resultCode,data);
    }
}
