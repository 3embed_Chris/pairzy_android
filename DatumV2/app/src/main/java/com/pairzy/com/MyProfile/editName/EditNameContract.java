package com.pairzy.com.MyProfile.editName;

/**
 * Created by ankit on 27/4/18.
 */

public interface EditNameContract {

    interface View{

        void invalidateName(String message);

        void onValidName(String name);

        void showMessage(String message);

        void onError(String message);
    }

    interface Presenter{

        void validateName(String name);
    }
}
