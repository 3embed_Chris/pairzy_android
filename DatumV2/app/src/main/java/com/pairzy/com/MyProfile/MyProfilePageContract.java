package com.pairzy.com.MyProfile;

import android.content.Intent;
import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import com.androidinsta.com.ImageData;
import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.MyProfile.Model.InstaMediaAdapter;
import com.pairzy.com.MyProfile.Model.MomentsGridAdapter;
import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.data.model.PrefData;

import java.util.ArrayList;
import java.util.List;

/**
 * <h2>MyProfilePageContract</h2>
 * <P>
 *     View model data communication model class
 *     to represent the view communication .
 * </P>
 * @since  4/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface MyProfilePageContract
{
    interface View extends BaseView
    {
        void intPageChangeListener(ViewPager.OnPageChangeListener listener);
        void addChildView(ArrayList<android.view.View> views);
        void updateSegmentBar(int total, int selected);
        void showError(String message);
        void showMessage(String message);
        void addLoadingView(android.view.View view);
        void launchEditPreference(PrefData prefData);
        void launchMobileVerifyActivity(Bundle phoneBundle);
        void launchGenderScreen(String date);
        void launchDobScreen(String dob);
        void launchNameScreen(String name);
        void launchEmailScreen(String email);
        void showTickMark(boolean show);

        void showToolbarTitle(String name);
        void finishActivity();
        void invalidateAgeSwitch(boolean isAgeShow);
        void invalidateDistSwitch(boolean isDistShow);
        void launchLocationScreen();
        void updateInstaData(List<List<ImageData>> lists);
        void showDisconnectInstaDialog();
        void hideInstaMediaUi();

        void showMomentData(List<MomentsData> momentList);
        void launchMomentScreen(ArrayList<MomentsData> momentsDataList, int position);
        void launchMomentVerticalScreen(ArrayList<MomentsData> momentsDataList, int position);
        void launchInstaPhotoPreviewScreen(ArrayList<ImageData> instaMedia, int position);
    }


    interface Presenter extends BasePresenter<View> {
        void updateInstagramDetails(String id,String name ,String token);
        void getUserData();
        void initListener();
        void updateUserStoredDetails();
        void doInstagramLogin();
        void parseEditedPreferenceData(int requestCode, int resultCode, Intent data);
        void parseEditedPhone(int requestCode, int resultCode, Intent data);
        void parseEditedDob(int requestCode, int resultCode, Intent data);
        void parseEditedEmail(int requestCode, int resultCode, Intent data);
        void parseEditedName(int requestCode, int resultCode, Intent data);
        void parseEditedGender(int requestCode, int resultCode, Intent data);
        void parseEditedPhotoVideos(int requestCode, int resultCode, Intent data);
        void callEditProfileApi();
        void saveDistancePref(boolean isChecked);
        void saveAgePref(boolean isChecked);
        void launchBoostDialog();
        void observeLocationChange();
        void getUserDetails();
        boolean isLocationCahnged();
        void dispose();
        void notifySettingsDataChange();
        void handleDistanceSwitchClick();
        void handleAgeSwitchClick();
        void disconnectInstagram();
        void setMomentClickCallback(MomentsGridAdapter momentsAdapter);

        void loadMomentScreen();

        void parseMomentDataList(int requestCode, int resultCode, Intent data);
        void setInstaAdapterCallback(InstaMediaAdapter mediaAdapter);
    }
}
