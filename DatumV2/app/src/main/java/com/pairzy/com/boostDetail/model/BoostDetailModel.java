package com.pairzy.com.boostDetail.model;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by ankit on 1/6/18.
 */

public class BoostDetailModel extends BaseModel{

    @Inject
    ArrayList<CoinPlan> coinPlanList;

    @Inject
    ArrayList<SubsPlan> subsPlanList;

    @Inject
    CoinPlanAdapter coinPlanAdapter;
    @Inject
    Utility utility;
    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    BoostDetailModel(){
    }


    public void parseCoinPlanList(String response) {
        try {
            coinPlanList.clear();
            CoinPlanResponse coinPlanResponse =  utility.getGson().fromJson(response,CoinPlanResponse.class);
            ArrayList<CoinPlan> coinPlans = coinPlanResponse.getData();
            if(coinPlans != null) {
                coinPlanList.addAll(coinPlans);
            }
        } catch (Exception e){}
    }

    public int getCoinPlanListSize() {
        return coinPlanList.size();
    }

    public int getSubsPlanListSize() {
        return subsPlanList.size();
    }

    public boolean isCoinPlanEmpty() {
        return getCoinPlanListSize() == 0;
    }

    public ArrayList<CoinPlan> getCoinPlanList() {
        return coinPlanList;
    }

    public void parseSubsPlanList(String response) {
        try{
            subsPlanList.clear();
            SubsPlanResponse subsPlanResponse = utility.getGson().fromJson(response,SubsPlanResponse.class);
            ArrayList<SubsPlan> subsPlans = subsPlanResponse.getData();
            if(subsPlans != null){
                subsPlanList.addAll(subsPlans);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean isSubsPlanEmpty() {
        return getSubsPlanListSize() == 0;
    }

    public void notifyCoinPlanAdapter() {
        coinPlanAdapter.notifyDataSetChanged();
    }

    public ArrayList<SubsPlan> getSubsPlanList() {
        return subsPlanList;
    }

    public void selectMiddleItem() {
        if(!isSubsPlanEmpty()){
            for(SubsPlan subsPlan : subsPlanList)
                if(subsPlan.isSelected())
                    return;
            int pos = getSubsPlanListSize() / 2;
            SubsPlan subsPlan = subsPlanList.get(pos);
            subsPlan.setSelected(true);
            subsPlan.setHeaderTag("MOST POPULAR");
        }
    }

    public String getSubsPurchaseId(int position) {
        try {
            if (position < getSubsPlanListSize()) {
                return subsPlanList.get(position).getId();
            }
        }catch (Exception ignored){}
        return "";
    }

    public CoinPlan getCoinPlan(int position) {
        try {
            if (position < getCoinPlanListSize()) {
                return coinPlanList.get(position);
            }
        }catch (Exception ignored){}
        return null;
    }

    public void addToCoinBalance(Integer coin) {
        try{
            coinBalanceHolder.setCoinBalance(String.valueOf(Integer.parseInt(coinBalanceHolder.getCoinBalance())+coin));
            coinBalanceHolder.setUpdated(true);
        }catch (Exception e){}
    }
}
