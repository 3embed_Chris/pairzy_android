package com.pairzy.com.editProfile.Model;

import java.util.ArrayList;

/**
 * <h>EditProfileData data class</h>
 * @author 3Embed.
 * @since 25/4/18.
 * @version 1.0.
 */

public class EditProfileData {

    String videoUrl;
    String videoThumb;
    ArrayList<ProfilePicture> profilePicList;

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public void setProfilePicList(ArrayList<ProfilePicture> profilePicList) {
        this.profilePicList = profilePicList;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoThumb(String videoThumb) {
        this.videoThumb = videoThumb;
    }

    public ArrayList<ProfilePicture> getProfilePicList() {
        return profilePicList;
    }

    public String getVideoThumb() {
        return videoThumb;
    }
}
