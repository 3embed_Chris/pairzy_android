package com.pairzy.com.userProfile.VideoItem;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.pairzy.com.R;
import com.pairzy.com.userProfile.UserProfilePage;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.CustomVideoView.widget.media.IjkVideoView;
import com.pairzy.com.util.Utility;
import com.facebook.drawee.view.SimpleDraweeView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoItemFrg extends DaggerFragment implements VideoItemContract.View
{
    private static final String TAG = VideoItemFrg.class.getSimpleName();

    private Unbinder unbinder;
    private String THUMB_NAIL,VIDEO_URL;
    @Inject
    Utility utility;
    @BindView(R.id.video_parent_view)
    FrameLayout video_parent_view;
    @BindView(R.id.thumbnail)
    SimpleDraweeView thumbnail;
    //private DatumVideoPlayer videoSurfaceView;
    @BindView(R.id.video_play_icon)
    ImageView play_icon;
    @BindView(R.id.mute_btn)
    ImageView muteButton;
    @BindView(R.id.video_container)
    FrameLayout video_container;
    @BindView(R.id.pb_progressbar)
    ProgressBar pbProgress;
    @BindView(R.id.ijk_video_view)
    IjkVideoView ijkVideoView;
    @BindView(R.id.hud_view)
    TableLayout mHudView;


    @Inject
    public VideoItemFrg() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        UserProfilePage userProfilePage= (UserProfilePage) getActivity();
        assert userProfilePage != null;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_videoitem_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder= ButterKnife.bind(this,view);
        initData();
    }

    private void initData()
    {
        ijkVideoView.setHudView(mHudView);

        thumbnail.setImageURI(THUMB_NAIL);

        //auto play
        play_icon.setVisibility(View.GONE);
        create_Media_player(video_container,VIDEO_URL);

        play_icon.setOnClickListener(v -> {
            play_icon.setVisibility(View.GONE);
            create_Media_player(video_container,VIDEO_URL);
        });

        muteButton.setOnClickListener(v -> {
            //if(videoSurfaceView != null)
            //    videoSurfaceView.setMute(!videoSurfaceView.isMuted);
        });


    }


    /*
     *Setting the video url. */
    public void setMediaFile(String thumbnail,String video_url)
    {

        this.THUMB_NAIL=thumbnail;
        this.VIDEO_URL=video_url;
    }


    @Override
    public void onDestroy()
    {
        unbinder.unbind();
        super.onDestroy();
    }

    /*
     * Creating the media player to play for the given url.*/
    private void create_Media_player(final FrameLayout holder, final String path)
    {
        try
        {
            //onPausePlayer();
            if (holder == null)
            {
                return;
            }

            ijkVideoView.setOnPreparedListener(iMediaPlayer -> {
                if(pbProgress != null)
                    pbProgress.setVisibility(View.GONE);
                if(thumbnail != null)
                    thumbnail.setVisibility(View.GONE);
            });

            ijkVideoView.setOnCompletionListener(iMediaPlayer -> {
                if(ijkVideoView != null){
                    ijkVideoView.seekTo(0);
                    ijkVideoView.start();
                }else{
                    if(pbProgress != null)
                        pbProgress.setVisibility(View.GONE);
                    if(play_icon != null)
                        play_icon.setVisibility(View.VISIBLE);
                    if(thumbnail != null)
                        thumbnail.setVisibility(View.VISIBLE);
                }
            });
            ijkVideoView.setOnErrorListener((iMediaPlayer, i, i1) -> {
                if(pbProgress != null)
                    pbProgress.setVisibility(View.GONE);
                if(play_icon != null)
                    play_icon.setVisibility(View.VISIBLE);
                if(thumbnail != null)
                    thumbnail.setVisibility(View.VISIBLE);
                return true;
            });
            ijkVideoView.setVideoURI(Uri.parse(create_Handel_video(path)));
            ijkVideoView.start();

            //videoSurfaceView.startPlayer(holder,Uri.parse(create_Handel_video(path)),play_icon,muteButton,pbProgress);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    /*
     * Handling the video quality.*/
    private static String create_Handel_video(String video_url)
    {
        video_url=change_video_foramte(video_url);
        if(video_url.contains(AppConfig.CloudinaryDetails.VIDEO_QUALITY))
        {
            return video_url;
        }
        String key_word="upload";
        int length_key=key_word.length();
        int index=video_url.indexOf("upload");
        if(index>0)
        {
            String firs_sub_String=video_url.substring(0,index+length_key);
            String last_sub_String=video_url.substring(index+length_key);
            return firs_sub_String+AppConfig.CloudinaryDetails.VIDEO_QUALITY +last_sub_String;

        }else
        {
            return video_url;
        }
    }
    /*
     * changing the video format.*/
    private static String change_video_foramte(String video_url)
    {
        if(video_url.contains(AppConfig.CloudinaryDetails.VIDEO_FORMATE))
        {
            return video_url;
        }else
        {
            int index_dot=video_url.lastIndexOf(".");
            String front_part=video_url.substring(0,index_dot+1);
            return front_part+AppConfig.CloudinaryDetails.VIDEO_FORMATE;
        }
    }

    private void onPlayerResume(){
        if (ijkVideoView != null)
            ijkVideoView.start();
    }


    private void onPlayerPause(){
        if(ijkVideoView != null)
            ijkVideoView.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: called");
        UserProfilePage userProfilePage= (UserProfilePage) getActivity();
        if(userProfilePage != null && userProfilePage.viewPager != null && userProfilePage.viewPager.getCurrentItem() == 0) {
            onPlayerResume();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: called");
        if(ijkVideoView != null)
            ijkVideoView.release(true);
    }

    //play listener
    public void onPlay(String tag) {
        Log.d(TAG, "onPlay: tag "+tag);
        onPlayerResume();
    }

    //pause listener
    public void onPause(String tag) {
        Log.d(TAG, "onPause: tag "+tag);
        onPlayerPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
