package com.pairzy.com.Service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.cloudinary.android.policy.TimeWindow;
import com.pairzy.com.AppController;
import com.pairzy.com.Database.PostDb;
import com.pairzy.com.PostMoments.model.Post;
import com.pairzy.com.PostMoments.model.PostData;
import com.pairzy.com.Utilities.Constants;
import com.pairzy.com.networking.NetworkService;
import com.google.gson.Gson;
import com.pairzy.com.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import dagger.android.DaggerService;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by DELL on 3/8/2018.
 */

public class PostService extends DaggerService implements UploadCallback {
    private static final String TAG = "PostService";

    private NotificationManager notificationManager;
    private NotificationCompat.Builder builder1, builder2;


    @Inject
    NetworkService service;
    @Inject
    PostDb postDb;

    @Inject
    PostObserver postObserver;
    //    Post post;
    File fileMain, fileMp4, finalFilterFile;
    //    private boolean fbShare = false;
//    private boolean twitterShare = false;
//    private boolean instaShare = false;
//    private boolean isFbShareCompleted = false;
//    private boolean isTwitterShareCompleted = false;
    public static final String CHANNEL_ONE_ID = "com.howdoo.chat.TWO";
    public static final String CHANNEL_ONE_NAME = "Channel Two";




    private Context context;
    private int j;

    private Map<String, Object> postDetails;
    private Map<String, Object> postDetail;
    private List<PostData> posts;
//    private Bus bus = AppController.getBus();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.i(TAG, "onBind: ");
        return null;
    }


    @Override
    public void onTaskRemoved(Intent rootIntent) {
        if (notificationManager != null) {
            if (posts != null) {
                int size = posts.size();
                if (size > 0) {

                    for (int i = 0; i < size; i++) {
                        notificationManager.cancel(Integer.parseInt(posts.get(i).getId()));
                    }

                }
            }
        }
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        context = this;
        postIt();
        return START_NOT_STICKY;
    }

    private void postIt() {


        posts = postDb.getAllData();

        int size = posts.size();



        if (size > 0) {

            postDetails = new HashMap<>();

            for (int i = 0; i < size; i++) {
                j = i;


                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {


                        PostData postData = posts.get(j);
                        String data = postData.getData();
                        Post post = new Gson().fromJson(data, Post.class);



//                        fbShare = postData.isFbShare();
//                        twi tterShare = postData.isTwitterShare();
//                        instaShare = postData.isInstaShare();

                        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_push);
                        if (post.getTypeForCloudinary().equals(Constants.Post.IMAGE)) {
                            //image
                            Bitmap bitmap = BitmapFactory.decodeFile(post.getPathForCloudinary());
                            contentView.setImageViewBitmap(R.id.image, bitmap);
                            contentView.setTextViewText(R.id.title, "Posting...");
                        } else {
                            // normal video
                            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(post.getPathForCloudinary(), MediaStore.Video.Thumbnails.MICRO_KIND);
                            contentView.setImageViewBitmap(R.id.image, bMap);
                            contentView.setImageViewResource(R.id.play, R.drawable.ic_play_circle_outline_black_24dp);
                            contentView.setTextViewText(R.id.title, "Posting...");
                        }

                        contentView.setProgressBar(R.id.progress, 100, 0, true);


                        postDetail = new HashMap<>();


                        postDetail.put("contentView", contentView);
                        postDetail.put("post", post);

                        postDetails.put(post.getId(), postDetail);

                        builder1 = new NotificationCompat.Builder(context, CHANNEL_ONE_ID)
                                .setContent(contentView)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                                .setAutoCancel(false).setOngoing(false);

                        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        builder2 = new NotificationCompat.Builder(context, CHANNEL_ONE_ID)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                                .setAutoCancel(true).setOngoing(false)
                                .setSound(soundUri);

                        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                        NotificationChannel notificationChannel;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            notificationChannel = new NotificationChannel(CHANNEL_ONE_ID, CHANNEL_ONE_NAME, NotificationManager.IMPORTANCE_HIGH);
                            notificationChannel.enableLights(true);
                            notificationChannel.setLightColor(Color.RED);
                            notificationChannel.setShowBadge(true);
                            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                            notificationManager.createNotificationChannel(notificationChannel);
                        }

                        normalPost(post);
                        //start notification
                        if (notificationManager != null) {
                            notificationManager.notify(Integer.parseInt(post.getId()), builder1.build());
                        }

                    }
                });
                thread.start();
            }

        } else {
            stopSelf();
        }
    }

    private void normalPost(Post post) {

        try {
            String requestId = MediaManager.get().upload(post.getPathForCloudinary())
                    .option("folder", "posts")
                    .option(Constants.Post.RESOURCE_TYPE, post.getTypeForCloudinary())
                    .callback(this).constrain(TimeWindow.immediate()).dispatch();

            addRequestIdForPost(post.getId(), requestId);
        } catch (Exception ignored) {

        }

    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    @Override
    public void onStart(String requestId) {

        Log.i("Cloudinary", "onStart: ");
    }

    @Override
    public void onProgress(String requestId, long bytes, long totalBytes) {
        Double progress = (double) bytes / totalBytes;
        Log.i("Cloudinary", "onProgress: " + progress);
    }

    @Override
    public void onSuccess(String requestId, Map resultData) {
        String type = resultData.get("resource_type").equals("video") ? "1" : "0";
        String url = String.valueOf(resultData.get("url"));

        Map<String, Object> postDetails = fetchPostDetailsFromRequestId(requestId);


        if (postDetails != null) {

            Post post = (Post) postDetails.get("post");


            Map<String, Object> map = new HashMap<>();

            map.put("thumbnailUrl1", url.replace("mp4", "jpg"));//url.replace("upload/", "upload/t_media_lib_thumb/"));
            map.put("imageUrl1", url);
            map.put("mediaType1", type);
            map.put("cloudinaryPublicId1", resultData.get("public_id"));
            map.put("imageUrl1Width", String.valueOf(resultData.get("width")));
            map.put("imageUrl1Height", String.valueOf(resultData.get("height")));
//
            map.put("title", post.getTitle());

            //userTag
            String regexPattern1 = "(@\\w+)";
            Pattern p1 = Pattern.compile(regexPattern1);
            Matcher m1 = p1.matcher(post.getTitle());
            int i = 0;
            ArrayList<String> strings = new ArrayList<>();
            String userTag[] = new String[i + 1];
            while (m1.find()) {
                strings.add(m1.group(1).replace("@", ""));
                // userTag[i++] = m1.group(1).replace("@", "");
            }

            map.put("userTags", strings);


            startPost(type, url, (post).getId(), (RemoteViews) postDetails.get("contentView"), map);
        }
    }

    private void startPost(String type, String mediaUrl, String postId, RemoteViews contentView, Map<String, Object> map) {
        Log.i(TAG, "startPost: ");
        service.sendPost(AppController.getInstance().getApiToken(), Constants.LANGUAGE, map)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        Log.i(TAG, "onSuccess: 2");
                        if (responseBodyResponse.code() == 200) {
                            Log.i(TAG, "onSuccess: 3");
                            postObserver.postData(true);
                            if (notificationManager != null) {
                                contentView.setTextViewText(R.id.title, "Posted successfully");
                                contentView.setProgressBar(R.id.progress, 100, 100, false);
                                notificationManager.notify(Integer.parseInt(postId), builder2.setContent(contentView).build());
                                notificationManager.cancel(Integer.parseInt(postId));
                            }



                            postDb.delete(postId);
//                            if (postDb.delete(postId))
//                                onStartCommand(null, 0, 0);
                        } else {
                            stopSelf();
                        }
                    }

                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(TAG, "onSuccess: 4");
                        if (notificationManager != null) {
                            contentView.setTextViewText(R.id.title, "Failed to post..");
                            contentView.setProgressBar(R.id.progress, 100, 100, false);
                            notificationManager.notify(Integer.parseInt(postId), builder2.setContent(contentView).build());
//                            postDb.delete(postId);
                        }

                    }

                    @Override
                    public void onComplete() {
                        Log.i(TAG, "onSuccess: 5");
                    }
                });
    }

    @Override
    public void onError(String requestId, ErrorInfo error) {
        Log.i(TAG, "onError: ");
        if (notificationManager != null) {

            Map<String, Object> postDetails = fetchPostDetailsFromRequestId(requestId);
            if (postDetails != null) {

                RemoteViews contentView = (RemoteViews) postDetails.get("contentView");
                contentView.setTextViewText(R.id.title, "Failed to post..");
                contentView.setProgressBar(R.id.progress, 100, 100, false);

                String postId = ((Post) postDetails.get("post")).getId();
                notificationManager.notify(Integer.parseInt(postId), builder2.setContent(contentView).build());
//                postDb.delete(postId);

            }


        }
    }

    @Override
    public void onReschedule(String requestId, ErrorInfo error) {

    }

    @SuppressWarnings("unchecked")

    private void addRequestIdForPost(String postId, String requestId) {


        Map<String, Object> postDetail = (Map<String, Object>) (postDetails.get(postId));

        if (postDetail != null) {


            postDetail.put("requestId", requestId);


            postDetails.put(postId, postDetail);
        }


    }


    @SuppressWarnings("unchecked")

    private Map<String, Object> fetchPostDetailsFromRequestId(String requestId) {

        Map<String, Object> postDetail = null;
        Set<String> keys = postDetails.keySet();
        for (String key : keys) {
            postDetail = (Map<String, Object>) (postDetails.get(key));

            if (postDetail.get("requestId") != null) {


                if (postDetail.get("requestId").equals(requestId)) {


                    return postDetail;

                }


            }


        }

        return postDetail;
    }
}
