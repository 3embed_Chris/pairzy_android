package com.pairzy.com.dagger;

import android.app.Application;
import com.pairzy.com.AppController;
import com.pairzy.com.FireBaseManger.MyFirebaseMessagingService;
import com.pairzy.com.networking.NetworkCheckerService;
import com.pairzy.com.networking.NetworkModule;
import javax.inject.Singleton;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Component(modules = {AppModule.class,
        ActivityBindingModule.class,
        AndroidSupportInjectionModule.class,
        AppUtilModule.class,
        NetworkModule.class})
@Singleton
public interface AppComponent extends AndroidInjector<AppController>
{
    @Override
    void inject(AppController instance);

    void inject(NetworkCheckerService networkCheckerService);

    void inject(MyFirebaseMessagingService service);


    @Component.Builder
    interface Builder
    {
        @BindsInstance
        Builder application(Application application);

        @BindsInstance
        Builder netModule(NetworkModule networkModule);

        @BindsInstance
        Builder appUtil(AppUtilModule appUtilModule);

        AppComponent build();
    }
}


