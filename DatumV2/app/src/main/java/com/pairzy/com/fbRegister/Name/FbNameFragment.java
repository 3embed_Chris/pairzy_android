package com.pairzy.com.fbRegister.Name;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.fbRegister.FbRegisterContact;
import com.pairzy.com.fbRegister.FbRegisterPage;
import com.pairzy.com.fbRegister.Userdob.FbDobFragment;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class FbNameFragment extends DaggerFragment implements FbNameContract.View,TextWatcher
{
    @Inject
    Activity activity;
    @Inject
    Utility utility;
    @Inject
    FbNameContract.Presenter presenter;
    @Inject
    FbRegisterContact.Presenter mainpresenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @BindView(R.id.close_button)
    RelativeLayout close_button;
    @BindView(R.id.skip_page)
    TextView skip_page;
    @BindView(R.id.first_title)
    TextView first_title;
    @BindView(R.id.second_title)
    TextView second_title;
    @BindView(R.id.input_name)
    EditText input_name;
    @BindView(R.id.hint_details)
    TextView hint_details;
    private Unbinder unbinder;
    @BindView(R.id.btnNext)
    RelativeLayout btnNext;


    @Inject
    public FbNameFragment() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_name, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder= ButterKnife.bind(this,view);
        presenter.takeView(this);
        initUIDetails();
        utility.openSoftInputKeyInDelay(activity,input_name);
    }

    @Override
    public void onStart() {
        super.onStart();
        //input_name.requestFocus();
    }

    @Override
    public void onResume() {
        super.onResume();
        //input_name.requestFocus();
        mainpresenter.updateProgress(2);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
    }

    @OnClick(R.id.skip_page)
    void onSkipClick()
    {
        onValidName("");
    }

    @OnClick(R.id.close_button)
    void onClose()
    {
        activity.onBackPressed();
    }

    @OnClick(R.id.btnNext)
    void onNext()
    {
        presenter.validateName(input_name.getText().toString());
    }

    @OnClick(R.id.parentLayout)
    void onParentClick(){}

    /*
    * intialization of the xml content.*/
    private void initUIDetails()
    {
        btnNext.setEnabled(false);
        skip_page.setTypeface(typeFaceManager.getCircularAirBook());
        first_title.setTypeface(typeFaceManager.getCircularAirBold());
        second_title.setTypeface(typeFaceManager.getCircularAirBold());
        input_name.setTypeface(typeFaceManager.getCircularAirBook());
        hint_details.setTypeface(typeFaceManager.getCircularAirLight());
        input_name.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        input_name.addTextChangedListener(this);
        input_name.setOnEditorActionListener((v, actionId, event) -> {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE))
            {
                presenter.validateName(input_name.getText().toString());
            }
            return false;
        });
        utility.openSpotInputKey(activity,input_name);
    }

    @Override
    public void invalidateName(String message)
    {
      onError(message);
    }

    @Override
    public void onValidName(String name)
    {
        FbDobFragment fragment=new FbDobFragment();
        Bundle data=getArguments();
        if(data==null)
        {
            data=new Bundle();
        }
        data.putString(FbRegisterContact.Presenter.NAME_DATA,name);
        fragment.setArguments(data);
        //mainpresenter.launchFragment(fragment,true);
        FbRegisterPage.details.setFirstName(name);
        mainpresenter.launchNextValidFrag(1,true);
    }

    @Override
    public void showMessage(String message)
    {
         mainpresenter.showMessage(message);
    }

    @Override
    public void onError(String message)
    {
        mainpresenter.showError(message);
    }

    /*
    *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            btnNext.setEnabled(true);
        }
        else {
            btnNext.setEnabled(false);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {
        if(charSequence.length()>0)
        {
            handelNextButton(true);
        }else
        {
            handelNextButton(false);
        }
    }
    @Override
    public void afterTextChanged(Editable editable) {

    }
}
