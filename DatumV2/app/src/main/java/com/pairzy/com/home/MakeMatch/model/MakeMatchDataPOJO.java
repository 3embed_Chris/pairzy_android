package com.pairzy.com.home.MakeMatch.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MakeMatchDataPOJO {
    @SerializedName("pairId")
    @Expose
    private String pairId;
    @SerializedName("user1_place")
    @Expose
    private String user1Place;
    @SerializedName("matchedPreferencesCount")
    @Expose
    private Integer matchedPreferencesCount;
    @SerializedName("user2")
    @Expose
    private String user2;
    @SerializedName("user2_name")
    @Expose
    private String user2Name;
    @SerializedName("matchedPreferencesPercentage")
    @Expose
    private Double matchedPreferencesPercentage;
    @SerializedName("user1_age")
    @Expose
    private Integer user1Age;
    @SerializedName("user2_age")
    @Expose
    private Integer user2Age;
    @SerializedName("user2_place")
    @Expose
    private String user2Place;
    @SerializedName("user2_worksAt")
    @Expose
    private String user2WorksAt;
    @SerializedName("isMatch")
    @Expose
    private Boolean isMatch;
    @SerializedName("user1_heightInFeet")
    @Expose
    private String user1HeightInFeet;
    @SerializedName("user2_profilePic")
    @Expose
    private String user2ProfilePic;
    @SerializedName("user1")
    @Expose
    private String user1;
    @SerializedName("user1_worksAt")
    @Expose
    private String user1WorksAt;
    @SerializedName("user2_heightInFeet")
    @Expose
    private String user2HeightInFeet;
    @SerializedName("user1_name")
    @Expose
    private String user1Name;
    @SerializedName("user1_profilePic")
    @Expose
    private String user1ProfilePic;

    public String getPairId() {
        return pairId;
    }

    public void setPairId(String pairId) {
        this.pairId = pairId;
    }

    public String getUser1Place() {
        return user1Place;
    }

    public void setUser1Place(String user1Place) {
        this.user1Place = user1Place;
    }

    public Integer getMatchedPreferencesCount() {
        return matchedPreferencesCount;
    }

    public void setMatchedPreferencesCount(Integer matchedPreferencesCount) {
        this.matchedPreferencesCount = matchedPreferencesCount;
    }

    public String getUser2() {
        return user2;
    }

    public void setUser2(String user2) {
        this.user2 = user2;
    }

    public String getUser2Name() {
        return user2Name;
    }

    public void setUser2Name(String user2Name) {
        this.user2Name = user2Name;
    }

    public Double getMatchedPreferencesPercentage() {
        return matchedPreferencesPercentage;
    }

    public void setMatchedPreferencesPercentage(Double matchedPreferencesPercentage) {
        this.matchedPreferencesPercentage = matchedPreferencesPercentage;
    }

    public Integer getUser1Age() {
        return user1Age;
    }

    public void setUser1Age(Integer user1Age) {
        this.user1Age = user1Age;
    }

    public Integer getUser2Age() {
        return user2Age;
    }

    public void setUser2Age(Integer user2Age) {
        this.user2Age = user2Age;
    }

    public String getUser2Place() {
        return user2Place;
    }

    public void setUser2Place(String user2Place) {
        this.user2Place = user2Place;
    }

    public String getUser2WorksAt() {
        return user2WorksAt;
    }

    public void setUser2WorksAt(String user2WorksAt) {
        this.user2WorksAt = user2WorksAt;
    }

    public Boolean getIsMatch() {
        return isMatch;
    }

    public void setIsMatch(Boolean isMatch) {
        this.isMatch = isMatch;
    }

    public String getUser1HeightInFeet() {
        return user1HeightInFeet;
    }

    public void setUser1HeightInFeet(String user1HeightInFeet) {
        this.user1HeightInFeet = user1HeightInFeet;
    }

    public String getUser2ProfilePic() {
        return user2ProfilePic;
    }

    public void setUser2ProfilePic(String user2ProfilePic) {
        this.user2ProfilePic = user2ProfilePic;
    }

    public String getUser1() {
        return user1;
    }

    public void setUser1(String user1) {
        this.user1 = user1;
    }

    public String getUser1WorksAt() {
        return user1WorksAt;
    }

    public void setUser1WorksAt(String user1WorksAt) {
        this.user1WorksAt = user1WorksAt;
    }

    public String getUser2HeightInFeet() {
        return user2HeightInFeet;
    }

    public void setUser2HeightInFeet(String user2HeightInFeet) {
        this.user2HeightInFeet = user2HeightInFeet;
    }

    public String getUser1Name() {
        return user1Name;
    }

    public void setUser1Name(String user1Name) {
        this.user1Name = user1Name;
    }

    public String getUser1ProfilePic() {
        return user1ProfilePic;
    }

    public void setUser1ProfilePic(String user1ProfilePic) {
        this.user1ProfilePic = user1ProfilePic;
    }

}
