package com.pairzy.com.boostDetail.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ankit on 1/6/18.
 */

public class CoinPlanResponse {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<CoinPlan> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CoinPlan> getData() {
        return data;
    }

    public void setData(ArrayList<CoinPlan> data) {
        this.data = data;
    }

}
