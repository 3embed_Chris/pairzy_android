package com.pairzy.com.boostLikes.Model;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pairzy.com.R;
import com.pairzy.com.util.DatumCallbacks.ListItemClick;
import com.pairzy.com.util.LoadingViews.LoadMoreViewHolder;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

/**
 * Created by ankit on 7/9/18.
 */

public class BoostLikeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int LOADING=0,IMAGE_ITEM=1;
    private ArrayList<BoostLikeData> arrayList;
    private TypeFaceManager typeFaceManager;
    private Utility utility;
    private Context context;
    private ListItemClick callback;
    private BoostLikeAdapterCallback adapterCallabck;

    public BoostLikeAdapter(Context context,
                            ArrayList<BoostLikeData> arrayList,
                            TypeFaceManager typeFaceManager,
                            Utility utility
                            ) {
        this.context = context;
        this.arrayList = arrayList;
        this.typeFaceManager = typeFaceManager;
        this.utility = utility;
        initCallBack();
    }

    /*
    *inti xml content */
    private void initCallBack()
    {
        callback= (id, position) -> {
            try
            {
                switch (id)
                {
                    case R.id.dislike_view:
                        adapterCallabck.onDislike(position);
                        break;
                    case R.id.boots_view:
                        //adapterCallabck.onBoots(position);
                        break;
                    case R.id.like_view:
                        adapterCallabck.onLike(position);
                        break;
                    case R.id.superlike_view:
                        adapterCallabck.onSuperLike(position);
                        break;
                    case R.id.video_view:
                    case R.id.card_view:
                        adapterCallabck.openUserDetails(position,null);
                        break;
                    case R.id.chat_view:
//                        BoostLikeData boostLikeData = null;
//                        try {
//                            boostLikeData = arrayList.get(position);
//                        }catch (Exception e){}
//                        if(boostLikeData != null)
//                            adapterCallabck.openChatScreen(boostLikeData);
                }
            }catch (Exception e){}
        };
    }

    @Override
    public int getItemViewType(int position)
    {
        BoostLikeData temp=arrayList.get(position);
        if(temp.isLoading())
        {
            return LOADING;
        }else
        {
            return IMAGE_ITEM;
        }
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case LOADING:
                View loading_item = LayoutInflater.from(parent.getContext()).inflate(R.layout.load_more_layout,parent, false);
                return new LoadMoreViewHolder(loading_item,typeFaceManager,null);
            case IMAGE_ITEM:
                View view = LayoutInflater.from(context).inflate(R.layout.user_list_item_view, parent, false);
                return new BoostLikeViewHolder(view, typeFaceManager, callback);
            default:
                View view1 = LayoutInflater.from(context).inflate(R.layout.user_list_item_view, parent, false);
                return new BoostLikeViewHolder(view1, typeFaceManager, callback);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType())
        {
            case LOADING:
                loadingView((LoadMoreViewHolder) holder);
                break;
            case IMAGE_ITEM:
                try {
                    initData((BoostLikeViewHolder) holder);
                }catch (Exception e){}
                break;
            default:
                try
                {
                    initData((BoostLikeViewHolder) holder);
                }catch (Exception e){}
        }
    }

    private void initData(BoostLikeViewHolder holder) {

        int position=holder.getAdapterPosition();
        BoostLikeData data=arrayList.get(position);
//        if(data.getMedia_list()!=null)
//        {
//            UserMediaPojo media_temp=data.getMedia_list().get(data.getCurrentImagePos());
//            if(media_temp.isVideo()) {
//                String uri =media_temp.getVideo_url();
//                //Log.d(ListAdapter.class.getName(),"video url "+uri);
//                try {
//                    uri = uri.replace("upload/", "upload/vs_20,dl_200,h_200,e_loop/").replace(".mp4", ".gif").replace(".mov",".gif");
//                    //Log.d(ListAdapter.class.getName(),"gif url "+uri);
//                }catch (Exception e){}
//                DraweeController controller  = Fresco.newDraweeControllerBuilder()
//                        .setUri(uri)
//                        .setAutoPlayAnimations(true)
//                        .build();
//                //holder.thumbnail.setImageURI(media_temp.getVideo_thumbnail());
//                holder.thumbnail.setController(controller);
//            }
//            else{
//                holder.thumbnail.setImageURI(data.getProfilePic());
//            }
//        }
//        else {
//            holder.thumbnail.setImageURI(data.getProfilePic());
//        }

        holder.thumbnail.setImageURI(data.getProfilePic());

//        if(data.isSuperlikedMe() > 0)
//        {
//            holder.ivSuperlikeMe.setVisibility(View.VISIBLE);
//            holder.ivSuperlikeMe.startAnimation(homeAnimation.getSlowBlinkingAnimation());
//        }
//        else
//        {
//            holder.ivSuperlikeMe.setVisibility(View.GONE);
//        }

        holder.user_name.setTypeface(typeFaceManager.getCircularAirBold());
        holder.user_name.setText( utility.formatString(data.getFirstName()));
//        AgeResponse ageResponse =  data.getAge();
//        if(ageResponse != null ) {
//            if (ageResponse.getIsHidden() == 0) {
//                holder.user_name.setText(String.format(Locale.ENGLISH, "%s, %d", utility.formatString(data.getFirstName()), ageResponse.getValue()));
//            } else{
//                holder.user_name.setText(String.format(Locale.ENGLISH, "%s ", utility.formatString(data.getFirstName())));
//            }
//        }

//        if(data.getOnlineStatus() == 1){
//            holder.user_status.setImageResource(R.drawable.online_dot);
//        }
//        else{
//            holder.user_status.setImageResource(0);
//        }

        holder.user_status.setImageResource(0);

//        if(!TextUtils.isEmpty(data.getProfileVideoThumbnail())){
//            holder.videoView.setVisibility(View.VISIBLE);
//        }
//        else{
//            holder.videoView.setVisibility(View.INVISIBLE);
//        }

    }

    /*
    *Handling the loading view */
    private void loadingView(LoadMoreViewHolder holder)
    {
        int position=holder.getAdapterPosition();
        BoostLikeData data=arrayList.get(position);
        if(data.isLoadingFailed())
        {
            holder.setFailed();
        }else
        {
            holder.showLoadingLoadingAgain();
        }
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public void setAdapterCallabck(BoostLikeAdapterCallback adapterCallabck) {
        this.adapterCallabck = adapterCallabck;
    }

    /*
     *Remove item from list . */
    public BoostLikeData removeItem(int index)
    {
        BoostLikeData temp_data=arrayList.remove(index);
        temp_data.item_actual_pos = index;
        notifyDataSetChanged();
        return temp_data;
    }
}
