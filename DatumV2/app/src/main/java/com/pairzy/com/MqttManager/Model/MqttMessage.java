package com.pairzy.com.MqttManager.Model;

import org.json.JSONObject;

/**
 * @since  3/13/2018.
 * @author 3Embed.
 * @version 1.0.
 */

public class MqttMessage
{
    private String topic;
    private JSONObject data;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }
}
