package com.pairzy.com.home.Prospects.MyLikes;
import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * @since  3/23/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface MyLikesBuilder
{
    @FragmentScoped
    @Binds
    MyLikesFrg getProspectItemFragment(MyLikesFrg myLikesFrg);

    @FragmentScoped
    @Binds
    MyLikesContract.Presenter taskPresenter(MyLikesPresenter presenter);
}
