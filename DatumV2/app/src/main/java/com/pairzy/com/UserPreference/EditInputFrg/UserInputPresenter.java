package com.pairzy.com.UserPreference.EditInputFrg;
import com.pairzy.com.data.model.Generic;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.Utility;

import javax.inject.Inject;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * <h2>UserInputPresenter</h2>
 * <P>
 *
 * </P>
 * @since  2/26/2018.
 * @author 3Embed.
 * @version 1.0.
 */
class UserInputPresenter implements UserInputContract.Presenter
{
    @Inject
    Utility utility;
    private UserInputContract.View view;
    private CompositeDisposable compositeDisposable;
    @Inject
    DatumProgressDialog progressDialog;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    UserInputModel  model;
    @Inject
    NetworkService service;

    @Inject
    UserInputPresenter(){
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(UserInputContract.View view) {
        this.view= view;
    }

    @Override
    public void dropView()
    {
        view=null;
        compositeDisposable.clear();
    }

    @Override
    public void updatePreference(String pref_Id,String value)
    {
        progressDialog.show();
        service.setPreferences(dataSource.getToken(),model.getLanguage(),model.getParams(pref_Id,model.formatValues(value)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        compositeDisposable.add(d);
                    }
                    @Override
                    public void onNext(Response<ResponseBody> result)
                    {
                        progressDialog.cancel();
                        try
                        {
                            if(result.code()==200)
                            {
                                String data=result.body().string();
                                Generic result_data=utility.getGson().fromJson(data,Generic.class);
                                if(view!=null)
                                view.updateUserInput(value);
                            }else
                            {
                                if(view!=null)
                                view.showError(model.getError(result));
                            }

                        } catch (Exception e)
                        {
                            if(view!=null)
                            view.showError(e.getMessage());
                        }
                    }
                    @Override
                    public void onError(Throwable errorMsg)
                    {
                        progressDialog.cancel();
                        if(view!=null)
                        view.showError(errorMsg.getMessage());

                    }
                    @Override
                    public void onComplete()
                    {}
                });

    }

    @Override
    public void showError() {
        if(view!=null)
        {
            view.showError("Please enter your "+view.getErrorTitle().toLowerCase()+"!");
        }
    }
}
