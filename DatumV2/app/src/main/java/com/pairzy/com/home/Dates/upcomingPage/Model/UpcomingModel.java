package com.pairzy.com.home.Dates.upcomingPage.Model;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.model.DateEvent;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.DatesFragUtil;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.HomeModel.LoadMoreStatus;
import com.pairzy.com.home.HomeUtil;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by ankit on 15/5/18.
 */

public class UpcomingModel extends BaseModel{

    @Named(DatesFragUtil.UPCOMING_LIST)
    @Inject
    ArrayList<DateListPojo> upcomingList;
    @Named(DatesFragUtil.PENDING_LIST)
    @Inject
    ArrayList<DateListPojo> pendingList;
    @Named(HomeUtil.LOAD_MORE_DATE_STATUS)
    @Inject
    LoadMoreStatus loadMoreStatus;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Utility utility;

    @Inject
    UpcomingModel(){
    }

    public int getListSize() {
        return upcomingList.size();
    }

    public void clearList() {
        upcomingList.clear();
    }

    public boolean loadMoreRequired(int currentPos) {
        if(loadMoreStatus.isNo_more_data()) {
            return false;
        }

        try
        {
            int size=upcomingList.size();
            int pending=size-currentPos;
            if(pending<5)
            {
                DateListPojo last_item=upcomingList.get(size-1);
                if(!last_item.isLoading())
                {
                    DateListPojo loading_item=new DateListPojo();
                    loading_item.setLoading(true);
                    upcomingList.add(loading_item);
                    return true;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public DateListPojo getUpcomingDateItem(int position) {
        if(upcomingList.size() > position)
            return upcomingList.get(position);
        return null;
    }

    public boolean checkDateExist() {
        if(upcomingList.size() > 0)
            return true;
        return false;
    }

    public DateListPojo removeDateFromList(int currentPosition) {
        DateListPojo  dateData = null;
        try {
            if (getListSize() > currentPosition) {
                dateData  = upcomingList.remove(currentPosition);
            }
        }catch (Exception e){
        }
        return dateData;
    }

    private ArrayList<DateEvent> getEventId(){
        ArrayList<String> jsonEventList = dataSource.getEvents();
        ArrayList<DateEvent> dateEvents = new ArrayList<>();
        try {
            for (String json : jsonEventList) {
                DateEvent dateEvent = utility.getGson().fromJson(json, DateEvent.class);
                if (dateEvent != null)
                    dateEvents.add(dateEvent);
            }
        }catch (Exception e){}
        return dateEvents;
    }

    public String checkIfReminderExist(String userId) {
        ArrayList<DateEvent> savedEventList =  getEventId();
        boolean found = false;
        String eventId = "";
        for(DateEvent dateEvent : savedEventList){
            if(dateEvent.getUserId().equals(userId)){
                found = true;
                eventId = dateEvent.getEventId();
            }
        }
        if(found)
            return eventId;
        return "";
    }

    //calender reminder helper methods

    public void saveEventId(DateEvent dateEvent) {
        ArrayList<DateEvent> savedEventList =  getEventId();
        if(!savedEventList.contains(dateEvent))
            savedEventList.add(dateEvent);

        try {
            ArrayList<String> finalEventList = new ArrayList<>();
            for (DateEvent dateEvent1 : savedEventList) {
                String json = utility.getGson().toJson(dateEvent1, DateEvent.class);
                finalEventList.add(json);
            }
            dataSource.setEvents(finalEventList);
        }catch (Exception e){}
    }

    public void replaceDateItem(DateListPojo dateListPojo, int currentPosition) {
        try {
            if(upcomingList.size() > currentPosition) {
                upcomingList.remove(currentPosition);
            }
            dateListPojo.setIsInitiatedByMe(1);
            pendingList.add(0, dateListPojo);
        }catch (Exception e){}
    }
}
