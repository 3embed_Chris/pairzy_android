package com.pairzy.com.MyProfile.editGender;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 27/4/18.
 */

@Module
public abstract class EditGenderModule {

    @ActivityScoped
    @Binds
    abstract Activity editGenderActivity(EditGenderActivity activity);

    @ActivityScoped
    @Binds
    abstract EditGenderContract.View editGenderView(EditGenderActivity activity);

    @ActivityScoped
    @Binds
    abstract EditGenderContract.Presenter editGenderPresenter(EditGenderPresenter presenter);
}
