package com.pairzy.com.MyProfile.Model;

public interface MomentClickCallback {
    void onMomentClick(int position);
}
