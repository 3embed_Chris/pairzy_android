package com.pairzy.com.coinWallet.Model;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * <h>CoinHistoryPagerAdapter</h>
 * @author 3Embed.
 * @since 30/5/18.
 */
public class CoinHistoryPagerAdapter extends FragmentPagerAdapter {
    ArrayList<Fragment> fragmentList;
    private String title[]={"All","Coin-In","Coin-Out"};

    public CoinHistoryPagerAdapter(ArrayList<Fragment> fragmentList, FragmentManager fm) {
        super(fm);
        this.fragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}
