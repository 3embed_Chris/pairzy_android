package com.pairzy.com.selectLanguage.model;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.pairzy.com.R;
import com.pairzy.com.editProfile.Model.ViewHolderClickCallback;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

public class AppLanguageAdapter extends RecyclerView.Adapter<LanguageItemViewHolder> {

    private ArrayList<LanguageItem> languageItems;
    private LanguageAdapterCallback callback;
    private ViewHolderClickCallback vhCallback;

    private TypeFaceManager typeFaceManager;
    private Utility utility;

    public AppLanguageAdapter(ArrayList<LanguageItem> languageItems,TypeFaceManager typeFaceManager,Utility utility) {
        this.languageItems = languageItems;
        this.typeFaceManager = typeFaceManager;
        this.utility = utility;
        initCallback();
    }

    private void initCallback() {
        vhCallback = new ViewHolderClickCallback() {
            @Override
            public void onClick(View view, int position) {
                switch (view.getId()){
                    case R.id.ll_root:
                        if(callback != null)
                            callback.onLanguageClick(position);
                        break;
                }
            }
        };
    }

    public void setAdapterCallback(LanguageAdapterCallback callback){
        this.callback = callback;
    }
    @NonNull
    @Override
    public LanguageItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.language_items,parent,false);
        return new LanguageItemViewHolder(view,typeFaceManager,vhCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull LanguageItemViewHolder holder, int position) {
        try{
            bindLanguageItem(holder);
        }catch (Exception e){}
    }

    private void bindLanguageItem(LanguageItemViewHolder holder) {
        int position = holder.getAdapterPosition();
        LanguageItem languageItem = languageItems.get(position);
        holder.tvLanguageName.setText(languageItem.getLanguageName());
        if(languageItem.isSelected()){
            holder.ivCheck.setImageResource(R.drawable.ic_check_circle);
        }else{
            holder.ivCheck.setImageResource(R.drawable.ic_unchecked_circle_datum);
        }
    }

    @Override
    public int getItemCount() {
        return languageItems.size();
    }
}
