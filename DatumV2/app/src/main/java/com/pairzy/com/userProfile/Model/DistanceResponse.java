package com.pairzy.com.userProfile.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 4/6/18.
 */

public class DistanceResponse {

    @SerializedName("value")
    @Expose
    double value;

    @SerializedName("isHidden")
    @Expose
    Integer isHidden;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Integer getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(Integer isHidden) {
        this.isHidden = isHidden;
    }
}
