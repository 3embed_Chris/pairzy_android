package com.pairzy.com.UserPreference.PreviewMessage;
import javax.inject.Inject;
/**
 * <h2>{@link PreviewFrgPresenter}</h2>
 * <P>
 *     Preview builder data for the user and the
 *     preview presenter for the preview page.
 * </P>
 *@since  2/26/2018.
 * @author 3Embed.
 * @version 1.0.
 */
class PreviewFrgPresenter implements PreviewFrgContract.Presenter
{

    private PreviewFrgContract.View view;
    @Inject
    PreviewFrgPresenter(){}
    public void takeView(Object view)
    {
        this.view= (PreviewFrgContract.View) view;
    }

    @Override
    public void dropView()
    {
        view=null;
    }
}
