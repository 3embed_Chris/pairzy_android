package com.pairzy.com.networking;

import com.pairzy.com.MyProfile.Model.EditProfileData;
import com.pairzy.com.data.model.fourSq.FourResponse;
import com.pairzy.com.googleLocationSearch.model.fourSqSearch.FourSearchResponse;

import java.util.ArrayList;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;



public interface NetworkService
{
    @POST("/login")
    Observable<Response<ResponseBody>> login(@Header("authorization") String auth,
                                             @Header("lang") String lang,
                                             @Body Map<String, String> params);

    @PATCH("/phoneNumberExistsVerificaton")
    Observable<Response<ResponseBody>> verifyMobile(@Header("authorization") String auth,
                                                    @Header("lang") String lang,
                                                    @Body Map<String, Object> params);

    @POST("/requestOtp")
    Observable<Response<ResponseBody>> requestOtp(@Header("authorization") String auth,
                                                  @Header("lang") String lang,
                                                  @Body Map<String, Object> params);

    @POST("/verificaitonCode")
    Observable<Response<ResponseBody>> verifyOtp(@Header("authorization") String auth,
                                                 @Header("lang") String lang,
                                                 @Body Map<String, Object> params);

    @PATCH("/mobileNumber")
    Single<Response<ResponseBody>> verifyOtpToChangeNumber(@Header("authorization") String auth,
                                                 @Header("lang") String lang,
                                                 @Body Map<String, Object> params);


    @PATCH("/emailIdExistsVerificaiton")
    Observable<Response<ResponseBody>> checkEmailAvailability(@Header("authorization") String auth,
                                                              @Header("lang") String lang,
                                                              @Body Map<String, Object> params);


    @POST("/unCompleteUser")
    Observable<Response<ResponseBody>> registerUnCompleteUser(@Header("authorization") String auth,
                                                              @Header("lang") String lang,
                                                              @Body Map<String, Object> params);

    @POST("/profile")
    Observable<Response<ResponseBody>> signUp(@Header("authorization") String auth,
                                              @Header("lang") String lang,
                                              @Body Map<String, Object> params);

    @GET("/preferences")
    Observable<Response<ResponseBody>> getPreferences(@Header("authorization") String auth,
                                                      @Header("lang") String lang);

    @PATCH("/preferences")
    Observable<Response<ResponseBody>> setPreferences(@Header("authorization") String auth,
                                                      @Header("lang") String lang,
                                                      @Body Map<String, Object> params);

    @POST("/faceBooklogin")
    Observable<Response<ResponseBody>> facebookLogin(@Header("authorization") String auth,
                                                     @Header("lang") String lang,
                                                     @Body Map<String, Object> params);

    @GET("/searchPreferences")
    Observable<Response<ResponseBody>> getSearchPreferences(@Header("authorization") String auth,
                                                            @Header("lang") String lang);

    @POST("/searchPreferences")
    Observable<Response<ResponseBody>> updatePreferenceService(@Header("authorization") String auth,
                                                               @Header("lang") String lang,
                                                               @Body Map<String, Object> params);
    @GET("/searchResult")
    Observable<Response<ResponseBody>> getsearchResult(@Query("offset") int offset, @Query("limit") int limit, @Header("authorization") String auth,
                                                       @Header("lang") String lang);


    @POST("/like")
    Observable<Response<ResponseBody>> doLikeService(@Header("authorization") String auth,
                                                     @Header("lang") String lang,
                                                     @Body Map<String, Object> params);

    @POST("/unLike")
    Observable<Response<ResponseBody>> doUnLikeService(@Header("authorization") String auth,
                                                       @Header("lang") String lang,
                                                       @Body Map<String, Object> params);
    @POST("/supperLike")
    Observable<Response<ResponseBody>> doSupperLike(@Header("authorization") String auth,
                                                    @Header("lang") String lang,
                                                    @Body Map<String, Object> params);

    @PATCH("/location")
    Observable<Response<ResponseBody>> updateLocation(@Header("authorization") String auth,
                                                      @Header("lang") String lang,
                                                      @Body Map<String, Object> params);

    @GET("/likesBy")
    Observable<Response<ResponseBody>> getlikesByResult(@Header("authorization") String auth,
                                                        @Header("lang") String lang, @Query("offset") int offset, @Query("limit") int limit);

    @GET("/myLikes")
    Observable<Response<ResponseBody>> getMyLikesResult(@Header("authorization") String auth,
                                                        @Header("lang") String lang, @Query("offset") int offset, @Query("limit") int limit);
    @GET("/mySupperLikes")
    Observable<Response<ResponseBody>> getMySupperLikesResult(@Header("authorization") String auth,
                                                              @Header("lang") String lang, @Query("offset") int offset, @Query("limit") int limit);

    @GET("/myUnLikes")
    Observable<Response<ResponseBody>> getMyUnLikesResult(@Header("authorization") String auth,
                                                          @Header("lang") String lang, @Query("offset") int offset, @Query("limit") int limit);

    @GET("/supperLikesBy")
    Observable<Response<ResponseBody>> getSupperLikedMeResult(@Header("authorization") String auth,
                                                              @Header("lang") String lang, @Query("offset") int offset, @Query("limit") int limit);


    @GET("/recentVisitors")
    Observable<Response<ResponseBody>> getrecentVisitors(@Header("authorization") String auth,
                                                         @Header("lang") String lang, @Query("offset") int offset, @Query("limit") int limit);

    @GET("/profile")
    Observable<Response<ResponseBody>> getMyProfile(@Header("authorization") String auth,
                                                    @Header("lang") String lang);

    @GET
    Observable<Response<ResponseBody>> getUserProfile(@Url String url, @Header("authorization") String auth,
                                                      @Header("lang") String lang);


    @PATCH("/instagramId")
    Observable<Response<ResponseBody>> updateInstagramId(@Header("authorization") String auth,
                                                         @Header("lang") String lang,
                                                         @Body Map<String, Object> params);


    @GET("/match")
    Observable<Response<ResponseBody>> getMatchedUserList(@Header("authorization") String auth,
                                                          @Header("lang") String lang);


    @PUT("/profile")
    Observable<Response<ResponseBody>> editProfile(@Header("authorization") String auth,
                                                   @Header("lang") String lang,
                                                   @Body EditProfileData editProfileData);

    @POST("/date")
    Observable<Response<ResponseBody>> planDate(@Header("authorization") String auth, @Header("lang") String lang,
                                                @Body Map<String, Object> mapBody);

    @GET("https://api.foursquare.com/v2/venues/explore")
    Observable<Response<FourResponse>> getNearByPlaces(@Query("client_id") String clientId,
                                                       @Query("client_secret") String clientSecret,
                                                       @Query("v") long v, @Query("ll") String ll, @Query("alt") long alt,
                                                       @Query("radius") long radius, @Query("query") String query, @Query("offset") int offset, @Query("limit") int limit);
    @GET
    Observable<Response<FourSearchResponse>> getNearByPlaces(@Url String url);

    @GET
    Observable<Response<ResponseBody>> getGoogleSearchResult(@Url String url);

    @GET
    Observable<Response<ResponseBody>> getPlaceDetail(@Url String url);

    @GET("/currentDates")
    Observable<Response<ResponseBody>> getCurrentDateList(@Header("authorization") String auth, @Header("lang") String lang,
                                                          @Query("offset") int offset, @Query("limit") int limit);

    @POST("/dateResponse")
    Observable<Response<ResponseBody>> postDateResponse(@Header("authorization") String auth, @Header("lang") String lang,
                                                        @Body Map<String, Object> body);


    @GET("/pastDates")
    Observable<Response<ResponseBody>> getPastDateList(@Header("authorization") String auth, @Header("lang") String lang,
                                                       @Query("offset") int offset, @Query("limit") int limit);

    @POST("/dateRating")
    Observable<Response<ResponseBody>> postDateRating(@Header("authorization") String auth, @Header("lang") String lang,
                                                      @Body Map<String, Object> body);

    @GET("/onlineUsers")
    Observable<Response<ResponseBody>> getOnLineList(@Header("authorization") String auth, @Header("lang") String lang,
                                                     @Query("offset") int offset, @Query("limit") int limit);

    @POST("/askAQuestion")
    Single<Response<ResponseBody>> postAskQuestion(@Header("authorization") String auth, @Header("lang") String lang,
                                                   @Body Map<String, Object> body);

    @POST("/makeASuggestion")
    Single<Response<ResponseBody>> postMakeSuggestion(@Header("authorization") String auth, @Header("lang") String lang,
                                                      @Body Map<String, Object> body);

    @POST("/reportAnIssue")
    Single<Response<ResponseBody>> postReportIssue(@Header("authorization") String auth, @Header("lang") String lang,
                                                   @Body Map<String, Object> body);

    @GET("/coinPlans")
    Single<Response<ResponseBody>> getCoinPlans(@Header("authorization") String auth, @Header("lang") String lang);

    @GET("/plan")
    Single<Response<ResponseBody>> getSubsPlans(@Header("authorization") String auth, @Header("lang") String lang);

    @POST("/subscription")
    Single<Response<ResponseBody>> postSubscription(@Header("authorization") String auth, @Header("lang") String lang,
                                                    @Body Map<String, Object> body);

    @GET("/currentCoinBalance")
    Single<Response<ResponseBody>> getCoinBalance(@Header("authorization") String auth, @Header("lang") String lang);

    @POST("/coinPlans")
    Single<Response<ResponseBody>> postCoinPlans(@Header("authorization") String auth, @Header("lang") String lang,
                                                 @Body Map<String, Object> body);

    @GET("/coinHistory")
    Single<Response<ResponseBody>> getCoinHistory(@Header("authorization") String auth, @Header("lang") String lang,
                                                  @Query("offset") int offset, @Query("limit") int limit);

    @GET("/reportUserReasons")
    Single<Response<ResponseBody>> getReportReasons(@Header("authorization") String auth, @Header("lang") String lang);

    @POST("/reportUser")
    Single<Response<ResponseBody>> postReportUser(@Header("authorization") String auth, @Header("lang") String lang,
                                                  @Body Map<String,Object> mapBody);

    @PATCH("/block")
    Single<Response<ResponseBody>> postBlockUser(@Header("authorization") String auth, @Header("lang") String lang,
                                                 @Body Map<String,Object> mapBody);
    @PATCH("/unBlock")
    Single<Response<ResponseBody>> postUnBlockUser(@Header("authorization") String auth, @Header("lang") String lang,
                                                   @Body Map<String,Object> mapBody);

    @POST("/unMatch")
    Single<Response<ResponseBody>> postUnMatchUser(@Header("authorization") String auth, @Header("lang") String lang,
                                                   @Body Map<String,Object> mapBody);

    @GET("/coinConfig")
    Single<Response<ResponseBody>> getCoinConfig(@Header("authorization") String auth, @Header("lang") String lang);


    @GET("/Chats/{pageNo}")
    Observable<Response<ResponseBody>> getChats(@Header("authorization") String auth,
                                                @Header("lang") String lang,@Path("pageNo") String pageNum);


    @POST("/messageWithoutMatch")
    Single<Response<ResponseBody>> postMessageWithoutMatch(@Header("authorization") String auth, @Header("lang") String lang,
                                                           @Body Map<String,Object> mapBody);

    @PATCH("/rewind")
    Observable<Response<ResponseBody>> reWind(@Header("authorization") String auth,@Header("lang") String lang);


    @DELETE("/profile")
    Single<Response<ResponseBody>> deleteAcoount(@Header("authorization") String auth,@Header("lang") String lang);

    @HTTP(method = "DELETE", hasBody = true,path = "/Chats")
    Single<Response<ResponseBody>> deleteChat(@Header("authorization") String auth,@Header("lang") String lang,
                                              @Body Map<String, Object> mapBody);

    @DELETE("/Messages/{all}/{messageIds}")
    Single<Response<ResponseBody>> deleteMessage(@Header("authorization")String auth,@Header("lang") String lang,
                                                 @Path("all")String all,
                                                 @Path("messageIds")ArrayList<String> messageIds);

    @GET("/boost")
    Single<Response<ResponseBody>> getBoostData(@Header("authorization") String auth,
                                            @Header("lang") String lang);

    @PATCH("/boost")
    Single<Response<ResponseBody>> boostProfile(@Header("authorization") String auth,
                                            @Header("lang") String lang);

    @GET("/boostWithLike")
    Single<Response<ResponseBody>> getBoostLikeList(@Header("authorization") String auth,
                                                @Header("lang") String lang);

    @POST("/coinFromVideo")
    Single<Response<ResponseBody>> addCoinToWallet(@Header("authorization") String auth, @Header("lang") String lang,
                                                           @Body Map<String,Object> mapBody);


    @GET("/Messages/{chatId}/{timestamp}/{pageSize}")
    Single<Response<ResponseBody>> getChatMessage(@Header("authorization") String auth,
                                                    @Header("lang") String lang,@Path("chatId")String chatId,@Path("timestamp")long timestamp,@Path("pageSize")int pageSize);


    @GET("/checkFbId/{fbId}")
    Single<Response<ResponseBody>> checkFbIdExist(@Header("authorization") String auth,
                                                  @Header("lang") String lang,@Path("fbId")String fbId);

    @GET("/version/1")
    Single<Response<ResponseBody>> getAppVersion();

    //post related api
    @GET("/userPost")
    Observable<Response<ResponseBody>> requestPost(@Header("authorization") String auth,
                                                   @Header("lang") String lang);

    @POST("/userPostComment")
    Observable<Response<ResponseBody>> postUserPostCommment(@Header("authorization") String auth,
                                                            @Header("lang") String lang,@Body Map<String,Object> mapBody);


    @GET("/userPostComment/{postId}")
    Observable<Response<ResponseBody>> getUserPostCommment(@Header("authorization") String auth,
                                                           @Header("lang") String lang,@Path("postId") String postId);


    @GET("/userPostLike/{postId}")
    Observable<Response<ResponseBody>> getUserPostLike(@Header("authorization") String auth,
                                                       @Header("lang") String lang,@Path("postId") String postId);

    @POST("/userPostLikeUnlike")
    Observable<Response<ResponseBody>> postUserPostLike(@Header("authorization") String auth,
                                                        @Header("lang") String lang,@Body Map<String,Object> mapBody);

    @POST("/userPost")
    Observable<Response<ResponseBody>> sendPost(@Header("authorization") String auth,
                                                @Header("lang") String lang,@Body Map<String,Object> mapBody);

    @PUT("/userPost")
    Observable<Response<ResponseBody>> deletePost(@Header("authorization") String auth,
                                                @Header("lang") String lang,@Body Map<String,Object> mapBody);

    @GET("/cloudinaryDetails")
    Single<Response<ResponseBody>> getCloudinaryDetail();

}
