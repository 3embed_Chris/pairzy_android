package com.pairzy.com.util.FileUtil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import java.io.File;
import java.io.IOException;

/**
 * @since  2/20/2018.
 */
public class AppFileManger
{
    private Context mcontext;
    public AppFileManger(Context context)
    {
        mcontext=context;
    }

    public File getImageFile()
    {
       String image_dir=dirImageDir();
       String file_name="Datum_"+System.currentTimeMillis()+".jpeg";
       File file = new File(image_dir,file_name);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }


    public File getVideoFile()
    {
        String video_dir=dirVideoDir();
        String file_name="Datum_"+System.currentTimeMillis()+".mp4";
        return new File(video_dir,file_name);
    }

    /*
    * Video dir*/
    private String dirVideoDir()
    {
        File f;
        if(checkWriteExternalPermission())
        {
            f = new File(Environment.getExternalStorageDirectory(), File.separator+ Config.APP_FOLDER);
            f.mkdirs();
            f = new File(Environment.getExternalStorageDirectory(), File.separator + Config.APP_FOLDER+ Config.VIDEOS_DIR);
            f.mkdirs();
        }else
        {
            f = new File(mcontext.getFilesDir(), File.separator+ Config.APP_FOLDER);
            f.mkdirs();
            f = new File(mcontext.getFilesDir(), File.separator + Config.APP_FOLDER+ Config.VIDEOS_DIR);
            f.mkdirs();
        }
        return f.getPath();
    }

    /*
   * Video dir*/
   private String dirImageDir()
    {
        File f;
        if(checkWriteExternalPermission())
        {
            f = new File(Environment.getExternalStorageDirectory(), File.separator+ Config.APP_FOLDER);
            f.mkdirs();
            f = new File(Environment.getExternalStorageDirectory(), File.separator + Config.APP_FOLDER+ Config.IMAGE_DIR);
            f.mkdirs();
        }else
        {
            f = new File(mcontext.getFilesDir(), File.separator+ Config.APP_FOLDER);
            f.mkdirs();
            f = new File(mcontext.getFilesDir(), File.separator + Config.APP_FOLDER+ Config.IMAGE_DIR);
            f.mkdirs();
        }
        return f.getPath();
    }

    /*
     *Checking external storage is granted or not.*/
    private boolean checkWriteExternalPermission()
    {
        String permission = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = mcontext.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
}
