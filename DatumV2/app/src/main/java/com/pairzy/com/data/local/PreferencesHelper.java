package com.pairzy.com.data.local;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.pairzy.com.MyProfile.Model.ProfileUpdateType;
import com.pairzy.com.data.model.Subscription;
import com.pairzy.com.data.model.coinCoinfig.CoinData;
import com.pairzy.com.data.source.PassportLocationDataSource;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
/**
 * <h2>PreferencesHelper</h2>
 * <p>
 * </p>
 * @author 3Embed
 * @version 1.0
 * @since 11/14/2017.
 */
public class PreferencesHelper implements PreferenceTaskDataSource ,PassportLocationDataSource
{
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @SuppressLint("CommitPrefEdits")
    public PreferencesHelper(Context context)
    {
        preferences = context.getSharedPreferences(PreferenceTaskKey.PreferenceTaskEntry.PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = context.getSharedPreferences(PreferenceTaskKey.PreferenceTaskEntry.PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
    }


    @Override
    public void clear() {
        editor.clear().apply();
    }

    @Override
    public String getUserId() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.USER_ID, "");
    }

    @Override
    public void setUserId(String userId)
    {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.USER_ID, userId).apply();
    }

    @Override
    public String getProfilePicture() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.PROFILE_PICTURE, "");
    }

    @Override
    public void setProfilePicture(String url)
    {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.PROFILE_PICTURE, url).apply();
    }

    @Override
    public String getToken() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.TOKEN, "");
    }

    @Override
    public void setToken(String token) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.TOKEN, token).apply();
    }

    @Override
    public String getName() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.FIRST_NAME, "");
    }

    @Override
    public void setName(String name) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.FIRST_NAME, name).apply();
    }

    @Override
    public String getMobileNumber() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.MOBILE_NUMBER, "");
    }

    @Override
    public void setMobileNumber(String mobileNumber) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.MOBILE_NUMBER, mobileNumber).apply();

    }

    @Override
    public String getCountryCode() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.COUNTRY_CODE, "");
    }

    @Override
    public void setCountryCode(String countryCode) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.COUNTRY_CODE, countryCode).apply();
    }

    @Override
    public boolean isSignUp() {
        return preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.IS_SIGNUP, false);
    }

    @Override
    public void setIsSignUp(boolean isSignUp) {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.IS_SIGNUP, isSignUp).apply();
    }

    @Override
    public String getOtp() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.OTP, "");
    }

    @Override
    public void setOtp(String otp) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.OTP, otp).apply();
    }

    @Override
    public String getGender() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.GENDER, "");
    }

    @Override
    public void setGender(String gender) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.GENDER, gender).apply();
    }

    @Override
    public String getEmail() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.EMAIL, "");
    }

    @Override
    public void setEmail(String email) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.EMAIL, email).apply();
    }

    @Override
    public int getUserAge()
    {
        return preferences.getInt(PreferenceTaskKey.PreferenceTaskEntry.USER_AGE,0);
    }

    @Override
    public void serUserAge(int age) {
        editor.putInt(PreferenceTaskKey.PreferenceTaskEntry.USER_AGE,age).apply();
    }

    @Override
    public String getBirthDate() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.DOB, "");
    }

    @Override
    public void setBirthDate(String birthDate) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.DOB, birthDate).apply();
    }

    @Override
    public void setPushToken(String pushToken) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.PUSH_TOKEN,pushToken).apply();
    }

    @Override
    public String getPushToken()
    {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.PUSH_TOKEN,"");
    }

    @Override
    public void setLoggedIn(boolean loggedIn)
    {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.LOGGED_IN,loggedIn).apply();
    }

    @Override
    public boolean isLoggedIn()
    {
        return preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.LOGGED_IN,false);
    }

    @Override
    public void setSearchPreference(String preference)
    {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.SEARCHPREF,preference).apply();
    }

    @Override
    public String getSearchPreference() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.SEARCHPREF,"");
    }

    @Override
    public void setUserVideo(String video_url)
    {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.USER_PROFILE_VIDEO,video_url).apply();
    }

    @Override
    public String getUserVideoUrl()
    {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.USER_PROFILE_VIDEO,"");
    }

    @Override
    public void setUserVideoHeight(int height) {
        editor.putInt(PreferenceTaskKey.PreferenceTaskEntry.USER_VIDEO_HEIGHT, height).apply();
    }

    @Override
    public void setUserVideoWidth(int width) {
        editor.putInt(PreferenceTaskKey.PreferenceTaskEntry.USER_VIDEO_WIDTH, width).apply();
    }

    @Override
    public int getUserVideoHeight() {
        return preferences.getInt(PreferenceTaskKey.PreferenceTaskEntry.USER_VIDEO_HEIGHT,640);
    }

    @Override
    public int getUserVideoWidth() {
        return preferences.getInt(PreferenceTaskKey.PreferenceTaskEntry.USER_VIDEO_WIDTH,360);
    }

    @Override
    public void setUserVideoThumbnail(String videoThumbnail)
    {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.USER_VIDEO_THUMBNAIL,videoThumbnail).apply();
    }

    @Override
    public String getUserVideoThumbnail() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.USER_VIDEO_THUMBNAIL,"");
    }

    @Override
    public void setUserOtherImages(ArrayList<String> otherImages)
    {
        Set<String> set= new HashSet<>(otherImages);
        editor.putStringSet(PreferenceTaskKey.PreferenceTaskEntry.USER_OTHER_IMAGES,set).apply();
    }

    @Override
    public ArrayList<String> getUserOtherImages()
    {
        Set<String> set_data=preferences.getStringSet(PreferenceTaskKey.PreferenceTaskEntry.USER_OTHER_IMAGES, new HashSet<>());
        return new ArrayList<>(set_data);
    }

    @Override
    public void setMyDetails(String data)
    {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.MY_DETAILS,data).apply();
    }


    @Override
    public String getMyDetails()
    {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.MY_DETAILS,"");
    }

    @Override
    public void setIsMile(boolean isMile) {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.IS_MILE,isMile).apply();
    }

    @Override
    public boolean getIsMile() {
        return preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.IS_MILE,false);
    }

    @Override
    public void setIsFacebookLogin(boolean isFbLogin) {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.IS_FACEBOOK,isFbLogin).apply();
    }

    @Override
    public boolean getIsFacebookLogin() {
        return preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.IS_FACEBOOK,false);
    }

    @Override
    public void setMyPreference(String preference)
    {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.MY_PREFERENCE,preference).apply();
    }

    @Override
    public String getSMyPreference()
    {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.MY_PREFERENCE,"");
    }

    @Override
    public void setMyAbout(String about) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.MY_ABOUTS,about).apply();
    }

    @Override
    public String getMyAbout() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.MY_ABOUTS,"");
    }

    @Override
    public void setMyEducation(String education) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.MY_EDUCATION,education).apply();
    }

    @Override
    public String getMyEducation() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.MY_EDUCATION,"");
    }

    @Override
    public void setMyWorkPlace(String work) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.MY_WORK_PLACE,work).apply();
    }

    @Override
    public String getMyWorkPlace() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.MY_WORK_PLACE,"");
    }

    @Override
    public void setMyJob(String job) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.MY_JOB,job).apply();
    }

    @Override
    public String getMyJob() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.MY_JOB,"");
    }

    @Override
    public void setEvents(ArrayList<String> eventList) {
        Set<String> set= new HashSet<>(eventList);
        editor.putStringSet(PreferenceTaskKey.PreferenceTaskEntry.DATE_EVENTS,set).apply();
    }

    @Override
    public ArrayList<String> getEvents() {
        Set<String> set_data=preferences.getStringSet(PreferenceTaskKey.PreferenceTaskEntry.DATE_EVENTS, new HashSet<>());
        return new ArrayList<>(set_data);
    }

    @Override
    public void setShowChatCoinDialog(boolean show) {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.COIN_DIALOG_CHAT,show).apply();
    }

    @Override
    public Boolean getShowChatCoinDialog() {
        return preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.COIN_DIALOG_CHAT,false);
    }

    @Override
    public void setShowSuperlikeCoinDialog(boolean show) {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.COIN_DIALOG_SUPERLIKE,show).apply();
    }

    @Override
    public Boolean getShowSuperlikeCoinDialog() {
        return preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.COIN_DIALOG_SUPERLIKE,false);
    }

    @Override
    public void setShowDateCoinDialog(boolean show) {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.COIN_DIALOG_DATE,show).apply();
    }

    @Override
    public Boolean getShowDateCoinDialog() {
        return preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.COIN_DIALOG_DATE,true);
    }

    @Override
    public void setShowCallDateCoinDialog(boolean show) {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.COIN_DIALOG_CALL_DATE,show).apply();
    }

    @Override
    public Boolean getShowRescheduleDateCoinDialog() {
        return preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.COIN_DIALOG_RESCHEDULE_DATE,true);
    }

    @Override
    public void setShowRescheduleDateCoinDialog(boolean show) {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.COIN_DIALOG_RESCHEDULE_DATE,show).apply();
    }

    @Override
    public Boolean getShowVideoDateCoinDialog() {
        return preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.COIN_DIALOG_VIDEO_DATE,true);
    }

    @Override
    public void setShowVideoDateCoinDialog(boolean show) {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.COIN_DIALOG_VIDEO_DATE,show).apply();
    }

    @Override
    public Boolean getShowCallDateCoinDialog() {
        return preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.COIN_DIALOG_CALL_DATE,true);
    }

    @Override
    public void setSubscription(String subs) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.SUBSCRIPTIONS,subs).apply();
    }

    @Override
    public Subscription getSubscription() {
        try {
            String jsonSubs = preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.SUBSCRIPTIONS, "");
            Gson gson = new Gson();
            return gson.fromJson(jsonSubs, Subscription.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void setCoinConfig(String coinConfig) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.COIN_CONFIG,"").apply();
    }


    @Override
    public CoinData getCoinConfigData() {
        try {
            String jsonCoinConfig = preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.COIN_CONFIG, "");
            Gson gson = new Gson();
            return gson.fromJson(jsonCoinConfig, CoinData.class);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean getShowCardTutorialView() {
        return  preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.CARD_TUTORIAL_VIEW,true);
    }

    @Override
    public void setShowCardTutorialView(boolean show) {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.CARD_TUTORIAL_VIEW,show).apply();
    }

    @Override
    public void setBoostExpireTime(long boostExpireTime) {
        editor.putLong(PreferenceTaskKey.PreferenceTaskEntry.BOOST_EXPIRE_TIME,boostExpireTime).apply();
    }

    @Override
    public long getBoostExpireTime() {
        return preferences.getLong(PreferenceTaskKey.PreferenceTaskEntry.BOOST_EXPIRE_TIME,-1);
    }

    @Override
    public void setRemainsLinksCount(String likeCount) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.REMAINS_LIKE_COUNT,likeCount).apply();
    }

    @Override
    public String getRemainsLikesCount() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.REMAINS_LIKE_COUNT,"0");
    }

    @Override
    public void setRemainsRewindsCount(String rewindCount) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.REMAINS_REWIND_COUNT,rewindCount).apply();
    }

    @Override
    public String getRemainsRewindsCount() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.REMAINS_REWIND_COUNT,"0");
    }

    @Override
    public void setNextLikeTime(long nextLikeTime) {
        editor.putLong(PreferenceTaskKey.PreferenceTaskEntry.NEXT_LIKE_TIME, nextLikeTime).apply();
    }

    @Override
    public long getNextLikeTime() {
        return preferences.getLong(PreferenceTaskKey.PreferenceTaskEntry.NEXT_LIKE_TIME,0L);
    }

    @Override
    public void setDuringBoostViewCount(int viewCount) {
        editor.putInt(PreferenceTaskKey.PreferenceTaskEntry.BOOST_PROFILE_VIEW_COUNT, viewCount).apply();
    }

    @Override
    public int getDuringBoostViewCount() {
        return preferences.getInt(PreferenceTaskKey.PreferenceTaskEntry.BOOST_PROFILE_VIEW_COUNT,0);
    }

    @Override
    public boolean isSplashFirstTime() {
        return preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.FIRST_SPLASH_LAUNCH,true);
    }

    @Override
    public void setSplashFirstTimeDone() {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.FIRST_SPLASH_LAUNCH, false).apply();
    }

    @Override
    public void setPassportLocations(ArrayList<String> passportLocations) {
        Set<String> set= new LinkedHashSet<>(passportLocations);
        editor.putStringSet(PreferenceTaskKey.PassportLocationEntry.PASSPORT_LOCATIONS,set).apply();
    }

    @Override
    public ArrayList<String> getPassportLocations()
    {
        Set<String> set_data= preferences.getStringSet(PreferenceTaskKey.PassportLocationEntry.PASSPORT_LOCATIONS,null);
        if(set_data==null)
        {
          return new ArrayList<>();
        }
        return new ArrayList<>(set_data);
    }

    @Override
    public void setFeatureLocation(String name) {
        editor.putString(PreferenceTaskKey.PassportLocationEntry.FEATURE_LOCATION,name).apply();
    }

    @Override
    public String getFeatureLocation() {
        return preferences.getString(PreferenceTaskKey.PassportLocationEntry.FEATURE_LOCATION,"");
    }

    @Override
    public void setCurrentLocation(String name) {
        editor.putString(PreferenceTaskKey.PassportLocationEntry.CURRENT_LOCATION,name).apply();
    }

    @Override
    public String getCurrentLocation() {
        return preferences.getString(PreferenceTaskKey.PassportLocationEntry.CURRENT_LOCATION,"");
    }

    @Override
    public boolean isFeaturesLocActive()
    {
        return preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.IS_PASPORTED,false);
    }

    @Override
    public void setFeaturedLcoationActive(boolean isActive)
    {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.IS_PASPORTED, isActive).apply();
    }

    @Override
    public void setUserActionCount(int actionsCount) {
        editor.putInt(PreferenceTaskKey.PreferenceTaskEntry.USER_ACTION_COUNT,actionsCount).apply();
    }

    @Override
    public int getUserActionCount() {
        return preferences.getInt(PreferenceTaskKey.PreferenceTaskEntry.USER_ACTION_COUNT, 0);
    }

    @Override
    public String getSelectedLanguageIso() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.APP_LANGUAGE_ISO,"-1");
    }

    @Override
    public void setSelectedLanguageIso(String iso) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.APP_LANGUAGE_ISO,iso).apply();
    }

    @Override
    public void setUpdateDialogShowed(boolean isShowed) {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.APP_UPDATE_DIALOG_SHOWED,isShowed);
    }

    @Override
    public boolean isUpdatedDialogShowed() {
        return preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.APP_UPDATE_DIALOG_SHOWED,false);
    }

    @Override
    public void setNewVersionData(String appVersion) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.APP_NEW_VERSION,appVersion).apply();
    }

    @Override
    public String getNewVersion() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.APP_NEW_VERSION,"");
    }

    @Override
    public void setNewVersionUpdatedTime(long milli) {
        editor.putLong(PreferenceTaskKey.PreferenceTaskEntry.NEW_VERSION_UPDATED_TIME,milli).apply();
    }

    @Override
    public long getNewVersionUpdatedTime() {
        return preferences.getLong(PreferenceTaskKey.PreferenceTaskEntry.NEW_VERSION_UPDATED_TIME,0L);
    }

    @Override
    public void setIsMandatory(boolean isMandatory) {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.APP_UPDATE_MANDATORY, isMandatory).apply();
    }

    @Override
    public boolean isMandatory() {
        return preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.APP_UPDATE_MANDATORY,false);
    }

    @Override
    public void setProfileUpdateType(int type) {
        editor.putInt(PreferenceTaskKey.PreferenceTaskEntry.PROFILE_UPDATE_TYPE,type).apply();
    }

    @Override
    public int getProfileUpdateType() {
        return preferences.getInt(PreferenceTaskKey.PreferenceTaskEntry.PROFILE_UPDATE_TYPE, ProfileUpdateType.DEFAULT.value);
    }


    //cloudinary details
    @Override
    public void setCloudName(String cloudName) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.CLOUD_NAME,cloudName).apply();
    }

    @Override
    public String getCloudName() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.CLOUD_NAME,"");
    }

    @Override
    public void setCloudinaryApiKey(String apiKey) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.CLOUDINARY_API_KEY,apiKey).apply();
    }

    @Override
    public String getCloudinaryApiKey() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.CLOUDINARY_API_KEY,"");
    }

    @Override
    public void setClodinaryApiSecret(String apiSecret) {
        editor.putString(PreferenceTaskKey.PreferenceTaskEntry.CLOUDINARY_API_SECRET,apiSecret).apply();
    }

    @Override
    public String getCloudinaryApiSecret() {
        return preferences.getString(PreferenceTaskKey.PreferenceTaskEntry.CLOUDINARY_API_SECRET,"");
    }

    @Override
    public boolean getSwipeCardTutorialView() {
        return  preferences.getBoolean(PreferenceTaskKey.PreferenceTaskEntry.SWIPE_TUTORIAL_VIEW,true);
    }

    @Override
    public void setSwipeCardTutorialView(boolean show) {
        editor.putBoolean(PreferenceTaskKey.PreferenceTaskEntry.SWIPE_TUTORIAL_VIEW,show).apply();
    }
}

