package com.pairzy.com.home.Prospects.MyLikes.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * @since  3/27/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MyLikesResponseHolder
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<MyLikesItemPojo> data = null;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public ArrayList<MyLikesItemPojo> getData()
    {
        return data;
    }

    public void setData(ArrayList<MyLikesItemPojo> data)
    {
        this.data = data;
    }
}
