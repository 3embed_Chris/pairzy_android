package com.pairzy.com.MqttChat.ViewHolders;

import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;

import com.pairzy.com.util.TypeFaceManager;
import com.facebook.drawee.view.SimpleDraweeView;


/**
 * Created by moda on 20/06/17.
 */

public class ViewHolderChatlist extends RecyclerView.ViewHolder {
    public TextView newMessageTime, newMessage, storeName, newMessageDate, newMessageCount;
    public ImageView  lock, tick, chatSelected,ivOnlineDot,ivUnMatched ,ivSuperlikedMe;
    public RelativeLayout rl;
    public SimpleDraweeView storeImage;

    public ViewHolderChatlist(View view, TypeFaceManager typeFaceManager) {
        super(view);

        storeImage =  view.findViewById(R.id.user_profile_pic);
        ivOnlineDot = view.findViewById(R.id.user_status_dot);
        ivUnMatched = view.findViewById(R.id.iv_unmatch_user);
        ivSuperlikedMe = view.findViewById(R.id.iv_super_liked_me);

        tick = (ImageView) view.findViewById(R.id.tick);
        newMessageTime = (TextView) view.findViewById(R.id.newMessageTime);
        newMessage = (TextView) view.findViewById(R.id.newMessage);
        newMessageDate = (TextView) view.findViewById(R.id.newMessageDate);
        storeName = (TextView) view.findViewById(R.id.storeName);

        chatSelected = (ImageView) view.findViewById(R.id.chatSelected);

        rl = (RelativeLayout) view.findViewById(R.id.rl);


        newMessageCount = (TextView) view.findViewById(R.id.newMessageCount);
        lock = (ImageView) view.findViewById(R.id.secretLockIv);


        //Typeface tf = AppController.getInstance().getRobotoCondensedFont();

        newMessageCount.setTypeface(typeFaceManager.getCircularAirBold(), Typeface.BOLD);
        newMessageDate.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.NORMAL);
        newMessageTime.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.NORMAL);
        newMessage.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.NORMAL);
    }
}
