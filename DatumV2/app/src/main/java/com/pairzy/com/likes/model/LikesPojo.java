package com.pairzy.com.likes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LikesPojo {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<LikesDataPojo> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<LikesDataPojo> getData() {
        return data;
    }

    public void setData(ArrayList<LikesDataPojo> data) {
        this.data = data;
    }
}
