package com.pairzy.com.home.Dates.upcomingPage;
import android.content.Intent;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;
import com.pairzy.com.home.Dates.upcomingPage.Model.UpcomingAdapter;

/**
 * @since  3/17/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface UpcomingFrgContract
{
    interface View extends BaseView
    {
        void showLoading();
        void notifyAdapter();
        void doLoadMore();
        void refreshList();
        void showError(int errorId);
        void showError(String error);
        void showMessage(String message);
        void showNetworkError(String error);
        void emptyData();
        void launchUserProfile(DateListPojo upcomingItemPojo);
        void addTothePastDate(PastDateListPojo pastDateListPojo);
        void launchPhyDateScreen(DateListPojo dateListPojo);
        void launchCallDateScreen(DateListPojo dateListPojo);
        void notifyPendingAdapter();
        void launchDirectionScreen(double latitude, double longitude, String placeName);
    }

    interface Presenter extends BasePresenter
    {
        void notifyUpcomingAdapter();
        void doLoadMore(int positionView);
        void setAdapterCallBack(UpcomingAdapter upcomingAdapter);
        boolean checkDateExist();
        void parseDateData(int requestCode, int resultCode, Intent data);
    }
}
