package com.pairzy.com.UserPreference.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * @since  2/22/2018.
 * @author 3Embedd.
 * @version 1.0.
 */
public class Preference_sub_item
{
    @SerializedName("myPreferences")
    @Expose
    private ArrayList<PrefranceDataDetails> myPreferences = null;

    public ArrayList<PrefranceDataDetails> getmyPreferences()
    {
        return myPreferences;
    }

    public void setmyPreferences(ArrayList<PrefranceDataDetails> myPreferences)
    {
        this.myPreferences = myPreferences;
    }
}
