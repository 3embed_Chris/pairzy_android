package com.pairzy.com.chatMessageScreen;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;
import dagger.Binds;
import dagger.Module;

/**
 * <h>ChatMessageModule class</h>
 * @author 3Embed.
 * @since 26/12/18.
 * @version 1.0.
 */
@Module
public abstract class ChatMessageModule {

    @ActivityScoped
    @Binds
    abstract ChatMessageContract.View bindView(ChatMessageActivity activity);

    @ActivityScoped
    @Binds
    abstract ChatMessageContract.Presenter bindPresenter(ChatMessagePresenter presenter);

    @ActivityScoped
    @Binds
    abstract Activity bindActivity(ChatMessageActivity activity);

}
