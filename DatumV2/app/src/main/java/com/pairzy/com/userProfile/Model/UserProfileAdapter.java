package com.pairzy.com.userProfile.Model;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pairzy.com.R;
import com.pairzy.com.data.model.PrefData;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;
import java.util.Locale;
/**
  *<h2>UserProfileAdapter</h2>
 * <P>
 *     Adapter class for the user list data details.
 * </P>
 * @author 3Embed.
 * @since 12-04-2018.
 * @version 1.0.*/
public class UserProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private ArrayList<PrefData> arrayList;
    private TypeFaceManager typeFaceManager;

    public UserProfileAdapter(ArrayList<PrefData> arrayList, TypeFaceManager typeFaceManager)
    {
        this.arrayList = arrayList;
        this.typeFaceManager=typeFaceManager;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_profile_category_row, parent, false);
        return new OptionsItemHolder(itemView,typeFaceManager);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        OptionsItemHolder item_view= (OptionsItemHolder) holder;
        handelView(item_view);
        item_view.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    /*
     *Managing the item details*/
    private void handelView(OptionsItemHolder holder)
    {
        Log.w("UserProfileAdapter","ItemCount: "+getItemCount());
        int position=holder.getAdapterPosition();
        PrefData prefData = arrayList.get(position);
        holder.title.setText(prefData.getLabel());
        try
        {
            if(prefData.getIsDone())
            {
                if(prefData.getSelectedValues().size()-1 > 0)
                {
                    String text_data=prefData.getSelectedValues().get(0);
                    holder.selected_test.setText(text_data);
                    holder.count_data.setText(String.format(Locale.ENGLISH, "+%d", prefData.getSelectedValues().size()-1));
                    holder.count_data.setVisibility(View.VISIBLE);
                }else if(prefData.getSelectedValues().size()>0)
                {
                    holder.selected_test.setText(prefData.getSelectedValues().get(0));
                    holder.count_data.setVisibility(View.GONE);
                }
            }else
            {
                holder.selected_test.setText(R.string.not_set_Text);
                holder.count_data.setVisibility(View.GONE);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
