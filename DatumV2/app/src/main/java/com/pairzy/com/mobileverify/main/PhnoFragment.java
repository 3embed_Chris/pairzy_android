package com.pairzy.com.mobileverify.main;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.mobileverify.MobileVerifyActivity;
import com.pairzy.com.mobileverify.MobileVerifyActivityBuilder;
import com.pairzy.com.mobileverify.MobileVerifyContract;
import com.pairzy.com.util.Countrypicker.Country;
import com.pairzy.com.util.Countrypicker.CountryPicker;
import com.pairzy.com.util.Countrypicker.CountryPickerListener;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Named;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * <h2>PhnoFragment</h2>
 * <p>
 * Select country and verifies mobile number
 * </p>
 * @author 3Embed
 * @version 1.0
 * @since 08/01/2018
 */
@ActivityScoped
public class PhnoFragment extends DaggerFragment implements TextWatcher, CountryPickerListener, PhnoContract.ViewIml
{
    @Inject
    Utility utility;

    @Inject
    CountryPicker countryPicker;

    @Inject
    @Named(MobileVerifyActivityBuilder.ACTIVITY_FRAGMENT_MANAGER)
    FragmentManager fragmentManager;

    @Inject
    Activity activity;

    private MobileVerifyActivity mobileVerifyActivity;

    @Inject
    MobileVerifyContract.Presenter mainPresenter;

    @Inject
    PhnoPresenter mainPresenterImp;

    @Inject
    PreferenceTaskDataSource preferencesHelper;

    @Inject
    TypeFaceManager typeFaceManager;

    @BindView(R.id.ivFlag)
    ImageView ivFlag;

    @BindView(R.id.tvCountry)
    TextView tvCountry;

    @BindView(R.id.etMobileNumber)
    EditText etMobileNumber;

    @BindView(R.id.btnNext)
    RelativeLayout btnNext;

    @BindView(R.id.titleOne)
    TextView titleOne;
    @BindView(R.id.titleTwo)
    TextView titleTwo;
    @BindView(R.id.tv_max_count)
    TextView tvMaxNumCounter;

    private int maxPhnoLength=10;

    private Unbinder unbinder;

    @Inject
    public PhnoFragment() {}


    @Override
    public void onResume()
    {
        mainPresenterImp.takeView(this);
        super.onResume();
        etMobileNumber.requestFocus();
    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.mobile_fragment, viewGroup, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        mobileVerifyActivity = (MobileVerifyActivity) activity;
        initView(view);
        intiData();
        if(mobileVerifyActivity.EDIT_PROFILE_TAG != null)
            setCurrentPhone();
    }

    private void setCurrentPhone()
    {
        String onlyphone = mobileVerifyActivity.phno.replace(mobileVerifyActivity.code,"");
        etMobileNumber.setText(TextUtils.isEmpty(onlyphone)?"":onlyphone);
        if( mobileVerifyActivity.phno != null &&!mobileVerifyActivity.phno.isEmpty() && !mobileVerifyActivity.phno.equalsIgnoreCase("NA") )
            countryPicker.getFlagByCountryCode( mobileVerifyActivity.code);
    }

    @Override
    public void initView(View view)
    {
        btnNext.setEnabled(false);
        titleOne.setTypeface(typeFaceManager.getCircularAirBold());
        titleTwo.setTypeface(typeFaceManager.getCircularAirBold());
        etMobileNumber.setTypeface(typeFaceManager.getCircularAirBook());
        tvCountry.setTypeface(typeFaceManager.getCircularAirBook());
    }

    @Override
    public void intiData()
    {
        etMobileNumber.addTextChangedListener(this);
        etMobileNumber.setOnEditorActionListener((v, actionId, event) -> {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE))
            {
               if(v.getText().length()==maxPhnoLength)
               {
                   continueBtn();
               }else
               {
                  showMessage(getString(R.string.ValidPhNoError));
               }
            }
            return false;
        });
        countryPicker.setListener(this);
        Country country = countryPicker.getUserCountryInfo(getActivity());
        ivFlag.setImageResource(country.getFlag());
        tvCountry.setText(country.getDial_code());
        maxPhnoLength=Integer.parseInt(country.getMax_digits());
        etMobileNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxPhnoLength)});
        updateMaxNumCounter(maxPhnoLength);
    }

    private void updateMaxNumCounter(int maxPhnoLength) {
        try{
            tvMaxNumCounter.setText(String.format(Locale.ENGLISH,"%d/%d",etMobileNumber.getText().length(),maxPhnoLength));
        }catch (Exception e){
            tvMaxNumCounter.setText("");
        }
    }


    @Override
    public void openVerifyPage(DaggerFragment fragment, boolean isBackrequired)
    {
        etMobileNumber.setText(etMobileNumber.getText());
        etMobileNumber.setSelection(etMobileNumber.getText().length());
        utility.closeSpotInputKey(activity,etMobileNumber);
        mainPresenter.moveFragment(fragment,isBackrequired);
    }


    @OnClick(R.id.btnNext)
    public void continueBtn()
    {
        preferencesHelper.setCountryCode(tvCountry.getText().toString());
        preferencesHelper.setMobileNumber(tvCountry.getText().toString() + "" + etMobileNumber.getText().toString());
        mainPresenterImp.validateNumber(tvCountry.getText().toString(),etMobileNumber.getText().toString());
    }

    @OnClick(R.id.select)
    public void pickCountry()
    {
        countryPicker.show(fragmentManager, "Country");
    }

    @OnClick(R.id.ibClose)
    void back()
    {
        utility.closeSpotInputKey(activity,etMobileNumber);
        activity.onBackPressed();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {}
    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {}
    @Override
    public void afterTextChanged(Editable editable) {
        String mobStr = editable.toString();
        if(!mobStr.isEmpty()){
            if(maxPhnoLength == mobStr.length())
            {
                btnNext.setEnabled(true);
            }
            else {
                btnNext.setEnabled(false);
            }
        }
        updateMaxNumCounter(maxPhnoLength);
    }


    @Override
    public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID, int max)
    {
        ivFlag.setImageResource(flagDrawableResID);
        tvCountry.setText(dialCode);
        maxPhnoLength=max;
        etMobileNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(max)});
        if (etMobileNumber.getText().length()!=max)
            etMobileNumber.setText("");
        if(countryPicker.isAdded())
            countryPicker.dismiss();
        updateMaxNumCounter(maxPhnoLength);
    }

    @Override
    public void showMessage(String s)
    {
        mainPresenter.showMessage(s);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy()
    {
        unbinder.unbind();
        mainPresenterImp.dropView();
        super.onDestroy();
    }

}
