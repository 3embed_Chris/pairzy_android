package com.pairzy.com.util;

import com.pairzy.com.BuildConfig;

/**
 * <h2>AppConfig</h2>
 * <P>
 * </P>
 *@since  12/15/2017.
 * @author 3Embed.
 * @version 1.0.
 */
public interface AppConfig
{

    String FREE_PLAN_SUBS_ID = "Free Plan";
    int notification_id = 201;

    long PROFILE_VIDEO_SIZE = 30 * 1024 * 1024;
    String AUTH_KEY="123";
    int CAMERA_CODE=123;
    int GALLERY_CODE=124;
    int LOAD_SCREEN = 2;
    int SPLASH_SCREEN_DURATION = 4;
    int SEARCH_REQUEST=345;
    int PROFILE_REQUEST=142;
    int ON_LIKE=143;
    int ON_DISLIKE =431;
    int ON_SUPER_LIKE=451;
    int ON_CHAT = 541;
    String RESULT_DATA="result";
    String USER_ID="user_id";
    String DEFAULT_MESSAGE = "3embed test";

    String ACTION_START_FOURGROUND = BuildConfig.APPLICATION_ID+".foreground";
    String NOTIFICATION_CHANNEL_ID_FOURGROUND_SERVICE = BuildConfig.APPLICATION_ID+".service_channel_id";
    String NOTIFICATION_CHANNEL_ID_CHAT = BuildConfig.APPLICATION_ID+".chat_channel_id";
    String NOTIFICATION_CHANNEL_ID_DATUM_NOTIFICATION = BuildConfig.APPLICATION_ID+".channel_id";
    String NOTIFICATION_CHANNEL_NAME_CHAT_SERVICE = BuildConfig.APPLICATION_ID+"_CHAT";
    String NOTIFICATION_CHANNEL_NAME_CHAT_NOTIFICATION = BuildConfig.APPLICATION_ID+"_CHAT_NOTIFICATION";
    String NOTIFICATION_CHANNEL_NAME_DATUM_NOTIFICATION = BuildConfig.APPLICATION_ID+"_NOTIFICATION";
    String DEFAULT_PAYMENT_GETWAY = "playStore";
    String DEFAULT_PROFILE_PIC ="https://findamatch.online/datum_2.0-admin/campagin.png";

    int DATE_PICKER_MINUTE_STEP_SIZE =1;
    int CHAT_SCREEN_REQ_CODE = 101;

    String[] APP_LANGUAGES = {};
    String DEFAULT_LANGUAGE="en";

    //search preference constants
    String KM = "km";
    String MI = "mi";
    long UPDATE_CHECK_INTERVAL = 10 * 60 * 1000; //10min
    int COMMENT_SCREEN_REQ_CODE = 11;

    int MOMENT_GRID_REQ_CODE = 211;
    int MOMENT_VERTICAL_REQ_CODE = 212;
    String DEFAULT_OTP = "111111";
    int POST_ACTIVITY_REQ_CODE = 222;


    //for foreground service;
    interface NOTIFICATION_ID {
        int FOREGROUND_SERVICE = 101;
    }

    interface MqttCredendials{
        String USERNAME = "3embed";
        String PASSWORD = "3embed";
    }

    /*Image processor details*/
    String IMAGE_PROCESSOR_KEY="1049832807";
    String IMAGE_PROCESSOR_SECRET_KEY="ybXQww6BEiHAJy2iCo6R";

    /*insatgram details*/
    String INSTA_CLINT_KEY="4f5a78c3929d4f44a3b65c943e04c552";
    String INSTA_SECRET_KEY="bbe8c7e0b1b6483faa0210a613436a86";
    String INSTAGRAM_URL="https://www.instagram.com/";

    /*
    * User name Regex String*/
    String USERNAME_REGEX="[A-Z][a-zA-Z][^#&<>\\\"~;$^%{}?]{1,15}$";

    //foursquare keys
    interface Foursquare{
        String CLIENT_ID ="FISW5FKMS121RFVTSFOJI3XRI33XPUSUWT24RBNGGHBXQMVM";
        String CLIENT_SECRET ="OSTMSRZL1XCDV3R2MJ50WY2R1Z2HBBTX3GCLOC1PRPTC13MN";
    }

    interface DateActivity{
        int LOACTION_REQUEST_CODE = 111;
    }

    interface CloudinaryDetails
    {
        String NAME="pipelineai";
        String KEY="443435715668775";
        String SECRET_KEY="l2iq6In_PajpDN6mAyNvjLDPcvU";

        /*
         * list video quality*/
         String VIDEO_QUALITY ="/q_60";
        /*
         * video format*/
        String VIDEO_FORMATE ="mp4";
        /*
         * Image thumb nail size*/
         String  IMAGE_THUMBNAIL_SIZE="/q_60,w_150,h_150,c_thumb";
        /*
         * Video thumb nail size*/
         String  VIDEO_THUMBNAIL_SIZE="/q_60,w_200,h_200,c_thumb";
    }


    interface InappKey
    {
        String Base64Key="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwR6s7BIOgrJrEODkuqU27lA4wbuxCDTj7Lqu9xIPx4SutYpQSK11eqU4GZvBqcwhw0Jy6imybK+898dHHtcoJGq1U3qKm7Q1GkWSYr/MDbgS/w0p4rHm7sO4G7ZfuD6Lr1yIrz41r0qGQ4Qbn8yzAaVFZuufgR+e3Z/rTHKhr/XMfxdk13fXQLsKFBLlhW/Tu56UDvGAczdLK2+60RRkKWSKkRxaG4lLkk9sX1jrKZNAPV9gVQuqhiq5ntZZFCucF3Hl6lVTAMWAC4d3VEWGuHDLY+irQIx4hDuXKqvwCl583vc+Ww0tY/kKfRCNSsdEw7tT0wyE6Es13PjDjgASXwIDAQAB";
    }
}
