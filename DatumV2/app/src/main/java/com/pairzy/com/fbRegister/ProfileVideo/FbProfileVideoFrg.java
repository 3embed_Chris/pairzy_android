package com.pairzy.com.fbRegister.ProfileVideo;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.fbRegister.FbRegisterContact;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.VideoPlayer;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * A simple {@link Fragment} subclass.
 */
@ActivityScoped
public class FbProfileVideoFrg extends DaggerFragment implements FbProfileVideoContact.View
{
    private Unbinder unbinder;
    @BindString(R.string.gallery_error)
    String file_path_error;
    @Inject
    Utility utility;
    @Inject
    App_permission app_permission;
    @Inject
    Activity activity;
    @Inject
    TypeFaceManager typeFaceManager;
    @BindView(R.id.skip_page)
    TextView skip_page;
    @BindView(R.id.btnNext)
    RelativeLayout btnNext;
    @Inject
    FbProfileVideoContact.Presenter presenter;
    @Inject
    FbRegisterContact.Presenter main_presenter;
    @BindView(R.id.title_first)
    TextView title_first;
    @BindView(R.id.sub_title_text)
    TextView sub_title_text;
    @BindView(R.id.add_photo)
    TextView add_photo;
    @BindView(R.id.add_image)
    ImageView add_image;
    @BindView(R.id.player)
    VideoPlayer player;
    @BindView(R.id.change_pic)
    TextView change_pic;
    @BindView(R.id.video_place_holder)
    FrameLayout video_place_holder;
    @BindView(R.id.pic_container)
    RelativeLayout pic_container;
    private String video_upload_url;
    private String thumbnail_url;

    @Inject
    public FbProfileVideoFrg() {}


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_profile_video, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder= ButterKnife.bind(this,view);
        initUIDetails();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(! app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults))
        {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode==AppConfig.CAMERA_CODE&&resultCode==Activity.RESULT_OK)
        {
            presenter.upDateToGallery();
            pic_container.setVisibility(View.GONE);
            presenter.compressedVideo(presenter.getRecentTempVideo());

        }else if(requestCode==AppConfig.GALLERY_CODE&&resultCode==Activity.RESULT_OK)
        {
            if(data!=null&&data.getData() != null)
            {
                Uri uri = data.getData();
                String file_path=utility.getRealPathFromURI(uri);
                if(!TextUtils.isEmpty(file_path))
                {
                    pic_container.setVisibility(View.GONE);
                    presenter.compressedVideo(file_path);
                }else
                {
                    showError(file_path_error);
                }
            }else
            {
                showError(file_path_error);
            }
        }else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();
        presenter.takeView(this);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        skip_page.requestFocus();
        utility.closeSpotInputKey(activity,change_pic);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        player.pauseVideo();
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
        player.releasePlayer();
        presenter.dropView();
        unbinder.unbind();
    }

    /*
     * intialization of the xml content.*/
    private void initUIDetails()
    {
        btnNext.setEnabled(false);
        skip_page.setTypeface(typeFaceManager.getCircularAirBook());
        title_first.setTypeface(typeFaceManager.getCircularAirBold());
        sub_title_text.setTypeface(typeFaceManager.getCircularAirBold());
        add_photo.setTypeface(typeFaceManager.getCircularAirBook());
        change_pic.setTypeface(typeFaceManager.getCircularAirBook());
    }

    @OnClick(R.id.close_button)
    void onBackClick()
    {
        activity.onBackPressed();
    }
    @OnClick(R.id.skip_page)
    void onSkip()
    {
        updateProfile("","");
    }

    @OnClick(R.id.pic_container)
    void onPicClick()
    {
        stopVideoPlay();
        presenter.openChooser();
    }

    @OnClick(R.id.btnNext)
    void onNextClick()
    {
        if(TextUtils.isEmpty(video_upload_url)||TextUtils.isEmpty(thumbnail_url))
        {
            presenter.uploadVideo(presenter.getActualVIdeo(),true);
        }else
        {
            updateProfile(video_upload_url,thumbnail_url);
        }
    }

    @OnClick(R.id.change_pic)
    void onChangeClick()
    {
        presenter.openChooser();
    }


    @Override
    public void onVideoCompressed(String file_path)
    {
        video_upload_url="";
        thumbnail_url="";
        player.setPath(file_path);
        video_place_holder.setVisibility(View.VISIBLE);
        handelNextButton(true);
    }

    @Override
    public void mediaUploaded(String video_url, String thumb_url)
    {
        video_upload_url=video_url;
        thumbnail_url=thumb_url;
        updateProfile(video_upload_url,thumbnail_url);
    }

    @Override
    public void imageCollectError()
    {
        pic_container.setVisibility(View.VISIBLE);
        video_place_holder.setVisibility(View.GONE);
        handelNextButton(false);
        showError(file_path_error);
    }

    @Override
    public void showChangeButton(boolean show) {
        if(show){
            change_pic.setVisibility(View.VISIBLE);
        }
        else{
            change_pic.setVisibility(View.GONE);
        }
    }


    @Override
    public void stopVideoPlay()
    {
        player.pausePlay();
        player.releasePlayer();
    }

    @Override
    public void openCamera(Uri uri)
    {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 30);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
        startActivityForResult(intent, AppConfig.CAMERA_CODE);
    }

    @Override
    public void openGallery()
    {
        Intent intent = new Intent();
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Video"),AppConfig.GALLERY_CODE);
    }

    @Override
    public void showError(String message)
    {
        video_place_holder.setVisibility(View.GONE);
        main_presenter.showError(message);
    }

    @Override
    public void showMessage(String messaage)
    {
        main_presenter.showMessage(messaage);
    }

    /*
     *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            btnNext.setEnabled(true);
        }
        else {
            btnNext.setEnabled(false);
        }
    }

    /*
    * Updating the profile*/
    private void updateProfile(String video_url,String thumb_nail)
    {
        Bundle data=getArguments();
        if(data==null)
        {
            data=new Bundle();
        }
        data.putString(FbRegisterContact.Presenter.VIDEO_DATA,video_url);
        data.putString(FbRegisterContact.Presenter.VIDEO_THUMB_DATA,thumb_nail);
        main_presenter.updateProfile(data);
    }
}
