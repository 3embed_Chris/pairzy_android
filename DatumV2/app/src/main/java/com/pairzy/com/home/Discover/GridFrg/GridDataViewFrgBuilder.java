package com.pairzy.com.home.Discover.GridFrg;

import android.app.Activity;

import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.data.model.DiscoverTabPositionHolder;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Discover.DiscoveryFragUtil;
import com.pairzy.com.home.Discover.GridFrg.Model.CardDeckViewAdapter;
import com.pairzy.com.home.Discover.Model.UserItemPojo;
import com.pairzy.com.home.HomeModel.HomeAnimation;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import java.util.ArrayList;
import javax.inject.Named;
import dagger.Module;
import dagger.Provides;
/**
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class GridDataViewFrgBuilder
{
    @Provides
    @FragmentScoped
    CardDeckViewAdapter getCardAdapter(Activity activity, TypeFaceManager typeFaceManager, Utility utility, @Named(DiscoveryFragUtil.USER_LIST) ArrayList<UserItemPojo> data, PreferenceTaskDataSource dataSource, DiscoverTabPositionHolder tabPostionHolder, HomeAnimation homeAnimation)
    {
        return new CardDeckViewAdapter(activity,typeFaceManager,utility,data,dataSource,tabPostionHolder,homeAnimation);
    }
}
