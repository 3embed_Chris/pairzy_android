package com.pairzy.com.home.HomeModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LikeResponse {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("nextLikeTime")
    @Expose
    private long nextLikeTime = 0;

    @SerializedName("remainsLikesInString")
    @Expose
    private String remainsLikesCount = "0";

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getNextLikeTime() {
        return nextLikeTime;
    }

    public void setNextLikeTime(Integer nextLikeTime) {
        this.nextLikeTime = nextLikeTime;
    }


    public String getRemainsLikesInString() {
        return remainsLikesCount;
    }

    public void setRemainsLikesInString(String remainsLikesCount) {
        this.remainsLikesCount = remainsLikesCount;
    }

}