package com.pairzy.com.home.Matches;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * <h2>DatesAdapter</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MatchesAdapter extends FragmentPagerAdapter
{
   private ArrayList<Fragment> frg_list;

    public MatchesAdapter(FragmentManager fm, ArrayList<Fragment> frg_list)
    {
        super(fm);
        this.frg_list=frg_list;
    }

    @Override
    public Fragment getItem(int position)
    {
        return frg_list.get(position);
    }

    @Override
    public int getCount()
    {
        return frg_list.size();
    }

}

