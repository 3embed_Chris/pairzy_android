package com.pairzy.com.fbRegister.ProfilePic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.view.View;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.CloudManager.UploadManager;
import com.pairzy.com.util.CloudManager.UploaderCallback;
import com.pairzy.com.util.FileUtil.AppFileManger;
import com.pairzy.com.util.ImageChecker.ImageProcessor;
import com.pairzy.com.util.ImageChecker.ImproperImageAlert;
import com.pairzy.com.util.MediaBottomSelector;
import com.pairzy.com.util.MediaPreview.ImagePreview;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.videocompressor.com.CompressImage;
import com.videocompressor.com.DataModel.CompressedData;
import com.videocompressor.com.RxCompressObservable;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h2>FbProfileVideoFrgPresenter</h2>
 * @since  2/19/2018.
 * @version 1.0.
 * @author 3Embed.
 */
public class FbProfilePicFrgPresenter implements FbProfilePicContact.Presenter,App_permission.Permission_Callback, MediaBottomSelector.Callback
{
    private  final String GALLERY="gallery";
    private  final String CAMERA="camera";
    private File currentPicTemp =null;
    private File selectedPic =null;
    @Inject
    ImproperImageAlert improperImageAlert;
    @Inject
    ImagePreview imagePreview;
    @Inject
    ImageProcessor imageProcessor;
    @Inject
    Context context;
    @Inject
    CompressImage compressImage;
    @Inject
    UploadManager uploadManager;
    @Inject
    LoadingProgress progress;
    @Inject
    FbProfilePicModel model;
    @Inject
    AppFileManger appFileManger;
    @Inject
    Utility utility;
    @Inject
    Activity activity;
    @Inject
    NetworkStateHolder networkStateHolder;

    private FbProfilePicFrg profilePicFrg;
    @Inject
    App_permission app_permission;
    @Inject
    MediaBottomSelector mediaBottomSelector;
    @Inject
    NetworkService service;
    private CompositeDisposable compositeDisposable;

    private FbProfilePicContact.View view;

    @Inject
    FbProfilePicFrgPresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }


    public void takeView(Object view)
    {
        this.profilePicFrg= (FbProfilePicFrg) view;
        this.view= (FbProfilePicContact.View) view;
    }


    @Override
    public void dropView()
    {
        view=null;
        compositeDisposable.clear();
    }


    @Override
    public void setProfilePIc(String image_path,SimpleDraweeView imageView)
    {
        Uri uri;
        if(image_path.contains("http://")||image_path.contains("https://"))
        {
            uri=Uri.parse(image_path);
        }else
        {
            uri=Uri.fromFile(new File(image_path));
        }
        ControllerListener controllerListener = new BaseControllerListener<ImageInfo>() {
            @Override
            public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable anim) {
                selectedPic = new File(image_path);
                if (view != null)
                    view.updateImageSet(image_path);
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                if (view != null)
                    view.imageCollectError();
            }
        };
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(uri)
                .setControllerListener(controllerListener)
                .setAutoPlayAnimations(true)
                .build();
        imageView.setController(controller);
        imageView.setVisibility(View.VISIBLE);
    }


    @Override
    public String getRecentCameraPic()
    {
        return selectedPic.getPath();
    }

    @Override
    public String getTempPic() {
        if(currentPicTemp != null)
            return currentPicTemp.getPath();
        return "";
    }

    @Override
    public void upDateToGallery()
    {
        if(currentPicTemp!=null)
        {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            File f = new File(currentPicTemp.getPath());
            Uri contentUri = Uri.fromFile(f);
            mediaScanIntent.setData(contentUri);
            activity.sendBroadcast(mediaScanIntent);
        }
    }


    @Override
    public void openChooser()
    {
        mediaBottomSelector.showBottomSheet(this);
    }

    @Override
    public void showError(String message)
    {
        if(view!=null)
        {
            view.showError(message);
        }
    }

    @Override
    public void compressImage(String filePath)
    {
        progress.show();
        Observer<CompressedData> observer = new Observer<CompressedData>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {
                compositeDisposable.add(d);
            }
            @Override
            public void onNext(CompressedData value)
            {
                progress.cancel();
                if(view!=null)
                {
                    view.onImageCompressed(value.getPath());
                }
            }
            @Override
            public void onError(Throwable e)
            {
                progress.cancel();
                e.printStackTrace();
                if(view!=null)
                    view.showError("Failed to collect video!");
            }

            @Override
            public void onComplete() {}
        };
        RxCompressObservable observable=compressImage.compressImage(context,filePath);
        observable.subscribeOn(Schedulers.newThread());
        observable.observeOn(AndroidSchedulers.mainThread());
        observable.subscribe(observer);
    }

    /*
     * Uploading the image to cloudinary.*/
    @Override
    public void uploadToCloudinary(String filePath,boolean firstTimeUpload)
    {
        if(networkStateHolder.isConnected()) {
            progress.show();
            uploadManager.uploadFile(filePath, new UploaderCallback() {
                @Override
                public void onSuccess(String main_url, String thumb_nail,int height,int width) {
                    progress.cancel();
                    if (view != null)
                        view.imageUploaded(main_url);
                }

                @Override
                public void onError(String error) {
                    if(error.equals(UploadManager.UN_CAUGHT_ERROR) && firstTimeUpload){
                        getCloudinaryDetails(filePath);
                    }else{
                        progress.cancel();
                        if (view != null)
                            view.showError(error);
                    }
                }
            });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void getCloudinaryDetails(String filePath) {
        service.getCloudinaryDetail()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(Response<ResponseBody> responseBodyResponse) {
                        progress.cancel();
                        if(responseBodyResponse.code() == 200){
                            try {
                                model.parseClodinaryDetail(responseBodyResponse.body().string());
                                uploadToCloudinary(filePath,false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else if(responseBodyResponse.code() == 401){
                            AppController.getInstance().appLogout();
                        }else{
                            if(view != null)
                                view.showError(model.getError(responseBodyResponse));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        progress.cancel();
                    }
                });
    }

    @Override
    public void openImagePreview(String imagePath)
    {
     imagePreview.showDialog(imagePath);
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag)
    {
        if(isAllGranted&&tag.equals(CAMERA))
        {
            if(view!=null)
            {
                try {
                    currentPicTemp = appFileManger.getImageFile();

                    view.openCamera(utility.getUri_Path(currentPicTemp));
                }catch (Exception e){
                    if(view != null)
                        view.showError(e.getMessage());
                }
            }
        }else if(isAllGranted&&tag.equals(GALLERY))
        {
            if(view!=null)
                view.openGallery();
        }
    }


    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag)
    {
        String[] stringArray = deniedPermission.toArray(new String[0]);
        if(tag.equals(GALLERY))
        {
            app_permission.show_Alert_Permission(activity.getString(R.string.photo_access_text),activity.getString(R.string.gallery_acess_subtitle),
                    activity.getString(R.string.gallery_acess_message),stringArray);
        }else if(tag.equals(CAMERA))
        {
            app_permission.show_Alert_Permission(activity.getString(R.string.camera_access_text),activity.getString(R.string.camera_acess_subtitle),
                    activity.getString(R.string.camera_acess_message),stringArray);
        }
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag)
    {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }


    @Override
    public void onPermissionPermanent_Denied(String tag,boolean parmanent)
    {
        if(parmanent)
        {
            if(tag.equals(GALLERY))
            {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.photo_denied_text),activity.getString(R.string.gallery_denied_subtitle),
                        activity.getString(R.string.gallery_denied_message));
            }else if(tag.equals(CAMERA))
            {
                app_permission.showAlertDeniedPermission(activity.getString(R.string.camera_denied_text),activity.getString(R.string.camera_denied_subtitle),
                        activity.getString(R.string.camera_denied_message));
            }
        }
    }


    @Override
    public void onCamera()
    {
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        permissions.add(App_permission.Permission.CAMERA);
        app_permission.getPermission_for_Sup_v4Fragment(CAMERA,permissions,profilePicFrg,this);
    }


    @Override
    public void onGallery()
    {
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.READ_EXTERNAL_STORAGE);
        app_permission.getPermission_for_Sup_v4Fragment(GALLERY,permissions,profilePicFrg,this);
    }
}
