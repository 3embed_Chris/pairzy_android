package com.pairzy.com.util.ImageChecker;

/**
 * <h2>PropertiesExtractor</h2>
 * <P>
 *
 * </P>
 * @since  4/3/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface PropertiesExtractor
{
    String getProperties();
}
