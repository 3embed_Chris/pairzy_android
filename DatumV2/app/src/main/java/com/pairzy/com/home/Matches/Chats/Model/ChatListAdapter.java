package com.pairzy.com.home.Matches.Chats.Model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.MqttChat.Utilities.Utilities;
import com.pairzy.com.R;
import com.pairzy.com.util.CustomView.ViewBinderHelper;
import com.pairzy.com.util.TypeFaceManager;
import com.facebook.drawee.generic.RoundingParams;

import java.util.ArrayList;

import static com.pairzy.com.MqttChat.Utilities.Utilities.convert24to12hourformat;

/**
 *<h2>ChatListAdapter</h2>
 * <P>
 *     Chat list adapter for the chat items.
 * </P>
 * @author 3Embed.
 * @version 1.0.*/
public class ChatListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private ArrayList<ChatListItem> item_list;
    private TypeFaceManager typeFaceManager;
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    private ChatAdapterItemCallback adapterItemCallback;
    private ItemViewCallBack callBack;
    private Context mContext;
    int whiteColor, goldColor;

    public ChatListAdapter(Context mContext,ArrayList<ChatListItem> items, TypeFaceManager typeFaceManager)
    {
        this.mContext = mContext;
        this.item_list = items;
        this.typeFaceManager = typeFaceManager;
        intiCallback();
        whiteColor = mContext.getResources().getColor(R.color.white);
        goldColor = mContext.getResources().getColor(R.color.golden_color);
    }

    public void setItemCallback(ChatAdapterItemCallback callback)
    {
        this.adapterItemCallback=callback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        @SuppressLint("InflateParams")
        View view=LayoutInflater.from( parent.getContext()).inflate(R.layout.chatlist_item_layout,parent,false);
        return new ChatItemHolder(view,typeFaceManager,callBack);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if(holder instanceof ChatItemHolder)
        {
            try
            {
                handelView((ChatItemHolder)holder);
            }catch (Exception e){}
        }

    }

    @Override
    public int getItemViewType(int position)
    {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position)
    {
        return super.getItemId(position);
    }

    @Override
    public int getItemCount()
    {
        return item_list.size();
    }

    /*call back user*/
    private void intiCallback()
    {
        callBack= (view, position) -> {
            switch (view.getId())
            {
                case R.id.container_rl:
                    if(adapterItemCallback!=null)
                        adapterItemCallback.initiateChat(position);
                    break;
                case R.id.report_view:
                    if(adapterItemCallback!=null)
                        adapterItemCallback.reportUser(position);
                    break;
                case R.id.block_view:
                    if(item_list.get(position).isBlockedByMe()) {
                        if (adapterItemCallback != null)
                            adapterItemCallback.unBlockUser(position);
                    }
                    else{
                        if (adapterItemCallback != null)
                            adapterItemCallback.blockUser(position);
                    }
                    break;
                case R.id.unmatched_view:
                    if(item_list.get(position).isMatchedUser()) {
                        if (adapterItemCallback != null)
                            adapterItemCallback.unMatchActiveUser(position);
                    }
                    else{
                        if (adapterItemCallback != null)
                            adapterItemCallback.deleteActiveUserChat(position);
                    }
                    break;
            }
        };
    }

    private void handelView(ChatItemHolder holder)
    {
        int position=holder.getAdapterPosition();
        binderHelper.bind(holder.swipe_layout,""+position);
        ChatListItem chat = item_list.get(position);

        if(chat.isBlockedByMe()){
            holder.block_text.setText(R.string.unblock_text);
        }
        else{
            holder.block_text.setText(R.string.block_text);
        }

        if(chat.isMatchedUser()){
            holder.unmatch_text.setText(R.string.unmatch_text);
        }
        else{
            holder.unmatch_text.setText(R.string.delete_text);
        }

        //need to check where secretId being set.
        if (chat.isSecretChat()) {
            holder.ivSecretLock.setVisibility(View.GONE);
        } else {
            holder.ivSecretLock.setVisibility(View.GONE);
        }

        if(chat.isUserOnline()){
            holder.ivOnlineStatusDot.setImageResource(R.drawable.online_dot);
        }
        else{
            holder.ivOnlineStatusDot.setImageResource(0);
        }
        if (chat.isMatchedUser()) {
            RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
            roundingParams.setBorder(whiteColor, 5.0f);
            roundingParams.setRoundAsCircle(true);

            holder.ivUserProfilePic.getHierarchy().setRoundingParams(roundingParams);
            holder.ivUnmatchUser.setVisibility(View.GONE);
        } else {
            RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
            roundingParams.setBorder(goldColor, 5.0f);
            roundingParams.setRoundAsCircle(true);
            holder.ivUserProfilePic.getHierarchy().setRoundingParams(roundingParams);
            holder.ivUnmatchUser.setVisibility(View.VISIBLE);
        }

        if(chat.isSuperlikedMe()){
            holder.ivSuperlikedMe.setVisibility(View.GONE);
        }
        else{
            holder.ivSuperlikedMe.setVisibility(View.GONE);
        }
        holder.tvStoreName.setText(chat.getReceiverName());

        holder.tvNewMessage.setText(chat.getNewMessage());

        try {
            String formatedDate = Utilities.formatDate(Utilities.tsFromGmt(chat.getNewMessageTime()));

            if ((chat.getNewMessageTime().substring(0, 8)).equals((Utilities.tsInGmt().substring(0, 8)))) {
                holder.tvNewMessageDate.setText(R.string.string_207);
            } else if ((Integer.parseInt((Utilities.tsInGmt().substring(0, 8))) - Integer.parseInt((chat.getNewMessageTime().substring(0, 8)))) == 1) {
                holder.tvNewMessageDate.setText(R.string.string_208);
            } else {
                holder.tvNewMessageDate.setText(formatedDate.substring(9, 24));
            }

            String last = convert24to12hourformat(formatedDate.substring(0, 9));
            holder.tvNewMessageTime.setText(last);

            if (chat.hasNewMessage() && Integer.parseInt(chat.getNewMessageCount()) > 0) {
                holder.tvNewMessageCount.setText(chat.getNewMessageCount());
                holder.rlCount.setVisibility(View.VISIBLE);
            } else {
                holder.tvNewMessageCount.setText("");
                holder.rlCount.setVisibility(View.GONE);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }


        if (chat.getReceiverImage() != null && !chat.getReceiverImage().isEmpty())
        {
                holder.ivUserProfilePic.setImageURI(Uri.parse(chat.getReceiverImage()));
        } else {
            //TODO: later
//            try {
//                holder.storeImage.setImageDrawable(TextDrawable.builder()
//                        .beginConfig()
//                        .textColor(Color.WHITE)
//                        .useFont(Typeface.DEFAULT)
//                        .fontSize(24 * density) /* size in px */
//                        .bold()
//                        .toUpperCase()
//                        .endConfig()
//                        .buildRound((chat.getReceiverName().trim()).charAt(0) + "", Color.parseColor(AppController.getInstance().getColorCode(holder.getAdapterPosition() % 19))));
//            } catch (IndexOutOfBoundsException e) {
//                holder.storeImage.setImageDrawable(TextDrawable.builder()
//                        .beginConfig()
//                        .textColor(Color.WHITE)
//                        .useFont(Typeface.DEFAULT)
//                        .fontSize(24 * density) /* size in px */
//                        .bold()
//                        .toUpperCase()
//                        .endConfig()
//                        .buildRound("C", Color.parseColor(AppController.getInstance().getColorCode(vh.getAdapterPosition() % 19))));
//            }
        }

        if (chat.isShowTick()) {
            holder.ivTick.setVisibility(View.VISIBLE);
            if (chat.getTickStatus() == 0) {
                holder.ivTick.clearColorFilter();
                holder.ivTick.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.clock));
            } else if (chat.getTickStatus() == 1) {
                holder.ivTick.clearColorFilter();
                holder.ivTick.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_single_tick));
            } else if (chat.getTickStatus() == 2) {
                holder.ivTick.clearColorFilter();
                holder.ivTick.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_double_tick));
                holder.ivTick.setColorFilter(ContextCompat.getColor(mContext, R.color.green_light), PorterDuff.Mode.SRC_IN);
            } else if (chat.getTickStatus() == 3) {
                holder.ivTick.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_double_tick));
                holder.ivTick.setColorFilter(ContextCompat.getColor(mContext, R.color.chat_blue_tick), PorterDuff.Mode.SRC_IN);
            }
        } else {
            holder.ivTick.setVisibility(View.GONE);
        }

    }

}
