package com.pairzy.com.photoVidPreview;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

@Module
public interface PhotoVidModule {

    @ActivityScoped
    @Binds
    Activity bindsActivity(PhotoVidActivity activity);

    @ActivityScoped
    @Binds
    PhotoVidContract.View bindsView(PhotoVidActivity activity);

    @ActivityScoped
    @Binds
    PhotoVidContract.Presenter bindsPresenter(PhotoVidPresenter photoVidPresenter);

}
