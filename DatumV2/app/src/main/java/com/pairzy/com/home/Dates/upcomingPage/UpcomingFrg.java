package com.pairzy.com.home.Dates.upcomingPage;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.home.Dates.DatesFragContract;
import com.pairzy.com.home.Dates.DatesFragPresenter;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;
import com.pairzy.com.home.Dates.Pending_page.PendingFrgPresenter;
import com.pairzy.com.home.Dates.upcomingPage.Model.UpcomingAdapter;
import com.pairzy.com.home.HomeContract;
import com.pairzy.com.planCallDate.CallDateActivity;
import com.pairzy.com.planDate.DateActivity;
import com.pairzy.com.util.TypeFaceManager;
import com.google.android.material.snackbar.Snackbar;

import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

import static com.pairzy.com.home.Dates.Pending_page.PendingFrg.RESCHEDULE_REQ_CODE;

/**
 * <h2>PostFeedFrag</h2>
 * <P>
 *
 * </P>
 * A simple {@link Fragment} subclass.
 * @author 3Embed.
 * @version 1.0.
 * @since 17-03-2018.
 */
@ActivityScoped
public class UpcomingFrg extends DaggerFragment implements UpcomingFrgContract.View,SwipeRefreshLayout.OnRefreshListener
{
    @Inject
    Activity activity;
    @Inject
    HomeContract.Presenter main_presenter;
    @Inject
    UpcomingFrgContract.Presenter presenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    DatesFragContract.Presenter datePresenter;
    @Inject
    UpcomingAdapter upcomingAdapter;
    @Named(UpcomingUtil.UPCOMING_LAYOUT_MANAGER)
    @Inject
    LinearLayoutManager linearLayoutManager;

    @BindView(R.id.upcoming_list)
    RecyclerView upcoming_list;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.loading_progress)
    ProgressBar pbLoading;
    @BindView(R.id.loading_text)
    TextView tvLoading;
    @BindView(R.id.error_icon)
    ImageView ivErrorIcon;
    @BindView(R.id.error_msg)
    TextView tvErrorMsg;
    @BindView(R.id.empty_data)
    RelativeLayout rlEmptyData;
    @BindView(R.id.loading_view)
    RelativeLayout rlLoadingView;
    @BindView(R.id.no_more_data)
    TextView tvNoMoreDataTitle;
    @BindView(R.id.empty_details)
    TextView tvEmptyDetail;
    @BindView(R.id.btn_retry)
    Button btnRetry;

    private Unbinder unbinder;
    @Inject
    public UpcomingFrg() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_upcoming_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder=ButterKnife.bind(this,view);
        presenter.takeView(this);
        initUi();
        applyFont();
        //presenter.notifyUpcomingAdapter();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(DatesFragPresenter.upcomingDateListUpdated) {
            upcomingAdapter.notifyDataSetChanged();
            DatesFragPresenter.upcomingDateListUpdated = false;
        }
    }
    private void applyFont() {
        tvNoMoreDataTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvEmptyDetail.setTypeface(typeFaceManager.getCircularAirBook());
        btnRetry.setTypeface(typeFaceManager.getCircularAirBold());
    }

    /*
     * initialization of the required data.*/
    private void initUi()
    {
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh2, R.color.refresh1,R.color.refresh);
        upcoming_list.setHasFixedSize(true);
        //upcoming_list.setNestedScrollingEnabled(false);
        upcoming_list.setLayoutManager(linearLayoutManager);
        upcoming_list.setAdapter(upcomingAdapter);
        presenter.setAdapterCallBack(upcomingAdapter);
        upcoming_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int positionView = linearLayoutManager.findLastVisibleItemPosition();
                presenter.doLoadMore(positionView);
                datePresenter.preFetchImage(false,positionView);
            }
        });
        swipeRefreshLayout.setOnRefreshListener(this);
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
    }

    @Override
    public void showLoading() {
        if(btnRetry != null) {
            btnRetry.setVisibility(View.GONE);
            tvErrorMsg.setVisibility(View.GONE);
            ivErrorIcon.setVisibility(View.GONE);
            tvLoading.setVisibility(View.VISIBLE);
            pbLoading.setVisibility(View.VISIBLE);
            rlLoadingView.setVisibility(View.VISIBLE);
            //upcoming_list.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
        }
    }

    @Override
    public void notifyAdapter() {

        if(rlEmptyData != null) {
            if (presenter != null && !presenter.checkDateExist()) {
                emptyData();
            } else {
                upcomingAdapter.notifyDataSetChanged();
                rlLoadingView.setVisibility(View.GONE);
                rlEmptyData.setVisibility(View.GONE);
                upcoming_list.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void doLoadMore() {
        try
        {
            upcoming_list.post(() -> upcomingAdapter.notifyDataSetChanged());
        }catch (Exception ignored){}
        datePresenter.fetchList(false,false);
    }

    @Override
    public void refreshList() {
        datePresenter.fetchList(false,true);
    }

    @Override
    public void showMessage(String message) {
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showNetworkError(String errorMsg) {
        if(tvErrorMsg != null) {
            btnRetry.setVisibility(View.VISIBLE);
            tvErrorMsg.setVisibility(View.VISIBLE);
            tvErrorMsg.setText(errorMsg);
            ivErrorIcon.setVisibility(View.VISIBLE);
            tvLoading.setVisibility(View.GONE);
            pbLoading.setVisibility(View.GONE);
            upcoming_list.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void emptyData()
    {
        if(upcoming_list != null) {
            //upcoming_list.setVisibility(View.GONE);
            rlLoadingView.setVisibility(View.GONE);
            rlEmptyData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void launchUserProfile(DateListPojo upcomingItemPojo) {
        datePresenter.launchUserProfile(upcomingItemPojo);
    }

    @Override
    public void addTothePastDate(PastDateListPojo pastDateListPojo) {
        datePresenter.addToThePastDate(pastDateListPojo);
    }

    @Override
    public void launchPhyDateScreen(DateListPojo dateListPojo) {
        Intent intent = new Intent(getContext(), DateActivity.class);
        launchScreen(intent,dateListPojo);
    }

    @Override
    public void launchCallDateScreen(DateListPojo dateListPojo) {
        Intent intent = new Intent(getContext(), CallDateActivity.class);
        launchScreen(intent,dateListPojo);
    }

    @Override
    public void notifyPendingAdapter() {
        datePresenter.notifyPendingAdapter();
    }

    @Override
    public void launchDirectionScreen(double latitude, double longitude,String placeName) {
//        Intent intent = new Intent(activity, MapDirectionActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//        startActivity(intent);
        //"geo:<lat>,<long>?q=<lat>,<long>(Label+Name)"
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f?q=%f,%f("+placeName+")", latitude, longitude,latitude, longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    /*
    * helper method
    */
    private void launchScreen(Intent intent,DateListPojo dateListPojo){
        try {
            intent.putExtra("reschedule", PendingFrgPresenter.RESCHEDULE_TAG);
            intent.putExtra("date_data", dateListPojo);
            intent.putExtra("date_type", dateListPojo.getDateType());
            startActivityForResult(intent, RESCHEDULE_REQ_CODE);
            activity.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.parseDateData(requestCode,resultCode,data);
    }

    @Override
    public void showError(int id)
    {
        String message=getString(id);
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showError(String error)
    {
        Snackbar snackbar = Snackbar
                .make(swipeRefreshLayout,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @OnClick(R.id.btn_retry)
    public void onRetry(){
        onRefresh();
    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
        datePresenter.fetchList(false,true);
    }
}
