package com.pairzy.com.MyProfile.editAge;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.MyProfile.MyProfilePagePresenter;
import com.pairzy.com.R;
import com.pairzy.com.util.InputFilterMinMax;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by ankit on 27/4/18.
 */

public class EditDobActivity extends BaseDaggerActivity implements EditDobContract.View{

    @Inject
    EditDobPresenter presenter;

    @BindString(R.string.userNotAdult)
    String userNotAdult;
    @Override
    public String getAdultErrorMessage() {
        return userNotAdult;
    }
    @Inject
    Activity activity;
    @Inject
    Utility utility;
    @Inject
    TypeFaceManager typeFaceManager;
    @BindView(R.id.close_button)
    RelativeLayout close_button;
    @BindView(R.id.skip_page)
    TextView skip_page;
    @BindView(R.id.first_title)
    TextView first_title;
    @BindView(R.id.second_title)
    TextView second_title;
    @BindView(R.id.day1)
    EditText day1;
    @BindView(R.id.day2)
    EditText day2;
    @BindView(R.id.mon1)
    EditText mon1;
    @BindView(R.id.mon2)
    EditText mon2;
    @BindView(R.id.year1)
    EditText year1;
    @BindView(R.id.year2)
    EditText year2;
    @BindView(R.id.year3)
    EditText year3;
    @BindView(R.id.year4)
    EditText year4;
    @BindView(R.id.hint_details)
    TextView hint_details;
    @BindView(R.id.btnNext)
    RelativeLayout next_button;
    @BindView(R.id.parentLayout)
    RelativeLayout parent_view;
    private Unbinder unbinder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_age);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        initUIDetails();
        initData(getIntent());
    }

    private void initData(Intent intent) {
        String dob = intent.getStringExtra(MyProfilePagePresenter.DOB_DATA);
        if(dob != null && !dob.isEmpty() && !dob.equalsIgnoreCase("0")) {
            ArrayList<Integer> dateArray = utility.getDataArrayFromMiliSec(Long.parseLong(dob));
            //Log.d("dob: ",DateFormat.format("dd/MM/yyyy", Long.parseLong(dob)).toString());
            if(dateArray.size()==8){
                day1.setText(""+dateArray.get(0));
                day2.setText(""+dateArray.get(1));
                mon1.setText(""+dateArray.get(2));
                mon2.setText(""+dateArray.get(3));
                year1.setText(""+dateArray.get(4));
                year2.setText(""+dateArray.get(5));
                year3.setText(""+dateArray.get(6));
                year4.setText(""+dateArray.get(7));
                year4.setSelection(year4.getText().toString().length());
            }
        }
    }

    /*
  * intialization of the xml content.*/
    private void initUIDetails()
    {
        next_button.setEnabled(false);
        skip_page.setTypeface(typeFaceManager.getCircularAirBook());
        first_title.setTypeface(typeFaceManager.getCircularAirBold());
        second_title.setTypeface(typeFaceManager.getCircularAirBold());
        day1.setTypeface(typeFaceManager.getCircularAirBook());
        day1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.toString().equals("3"))
                {
                    day2.setFilters(new InputFilter[]{new InputFilterMinMax(0,1)});
                }else
                {
                    day2.setFilters(new InputFilter[]{new InputFilterMinMax(0,9)});
                }
                if(editable.length()>0)
                {
                    day2.requestFocus();
                }
                validateInput();
            }
        });
        day1.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                day1.setText("");
                day1.requestFocus();
            }
            return false;
        });
        day1.setFilters(new InputFilter[]{new InputFilterMinMax(0,3)});

        day2.setTypeface(typeFaceManager.getCircularAirBook());
        day2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.length()>0)
                {
                    mon1.requestFocus();
                }
                validateInput();
            }});
        day2.setFilters(new InputFilter[]{new InputFilterMinMax(0,9)});
        day2.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                day1.setText("");
                day1.requestFocus();
            }
            return false;
        });

        mon1.setTypeface(typeFaceManager.getCircularAirBook());
        mon1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.toString().equals("0"))
                {
                    mon2.setFilters(new InputFilter[]{new InputFilterMinMax(1,9)});
                }else
                {
                    mon2.setFilters(new InputFilter[]{new InputFilterMinMax(0,2)});
                }
                if(editable.length()>0)
                {
                    mon2.requestFocus();
                }
                validateInput();
            }
        });
        mon1.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                day2.setText("");
                day2.requestFocus();
            }
            return false;
        });
        mon1.setFilters(new InputFilter[]{new InputFilterMinMax(0,1)});

        mon2.setTypeface(typeFaceManager.getCircularAirBook());
        mon2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.length()>0)
                {
                    year1.requestFocus();
                }
                validateInput();
            }
        });
        mon2.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                mon1.setText("");
                mon1.requestFocus();
            }
            return false;
        });
        mon2.setFilters(new InputFilter[]{new InputFilterMinMax(0,9)});

        year1.setTypeface(typeFaceManager.getCircularAirBook());
        year1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.toString().equals("1"))
                {
                    year2.setFilters(new InputFilter[]{new InputFilterMinMax(9,9)});
                }else
                {
                    year2.setFilters(new InputFilter[]{new InputFilterMinMax(0,0)});
                }
                if(editable.length()>0)
                {
                    year2.requestFocus();
                }
                validateInput();
            }
        });
        year1.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                mon2.setText("");
                mon2.requestFocus();
            }
            return false;
        });
        year1.setFilters(new InputFilter[]{new InputFilterMinMax(1,2)});
        year2.setTypeface(typeFaceManager.getCircularAirBook());
        year2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.toString().equals("9"))
                {
                    year3.setFilters(new InputFilter[]{new InputFilterMinMax(4,9)});
                }else
                {
                    year3.setFilters(new InputFilter[]{new InputFilterMinMax(1,9)});
                }
                if(editable.length()>0)
                {
                    year3.requestFocus();
                }
                validateInput();
            }
        });
        year2.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                year1.setText("");
                year1.requestFocus();
            }
            return false;
        });

        year3.setTypeface(typeFaceManager.getCircularAirBook());
        year3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.length()>0)
                {
                    year4.requestFocus();
                }
                validateInput();
            }
        });
        year3.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                year2.setText("");
                year2.requestFocus();
            }
            return false;
        });
        year3.setFilters(new InputFilter[]{new InputFilterMinMax(1,9)});

        year4.setTypeface(typeFaceManager.getCircularAirBook());
        year4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
            @Override
            public void afterTextChanged(Editable editable)
            {
                if(editable.length()>0)
                {
                    year4.requestFocus();
                }
                validateInput();
            }
        });
        year4.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_DEL)
            {
                if(year4.getText().toString().length()<1)
                {
                    year3.setText("");
                    year3.requestFocus();
                }else
                {
                    year4.setText("");
                    year4.requestFocus();
                }
            }
            return false;
        });
        year4.setFilters(new InputFilter[]{new InputFilterMinMax(1,9)});
    }

    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(parent_view,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void onError(String error) {
        Snackbar snackbar = Snackbar
                .make(parent_view,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @OnClick(R.id.calender_view)
    public void calenderView(){
        presenter.openDatePicker();
    }

    @Override
    public void finishActivity(double dateInMilli) {
        Intent intent = new Intent();
        intent.putExtra(MyProfilePagePresenter.DOB_DATA,dateInMilli);
        setResult(RESULT_OK,intent);
        onBackPressed();
    }

    /*
     *Handling the focous change.*/
    private void handelFocous()
    {
        if(day1.getText().length()<1)
        {
            day1.requestFocus();
        }else if(day2.getText().length()<1)
        {
            day2.requestFocus();
        }else if(mon1.getText().length()<1)
        {
            mon1.requestFocus();
        }else if(mon2.getText().length()<1)
        {
            mon2.requestFocus();
        }else if(year1.getText().length()<1)
        {
            year1.requestFocus();
        }else if(year2.getText().length()<1)
        {
            year2.requestFocus();
        }else if(year3.getText().length()<1)
        {
            year3.requestFocus();
        }else
        {
            year4.requestFocus();
        }
    }


    @Override
    public void onDateSelected(String day, String month, String year)
    {
        day1.setText(String.format("%s", day.charAt(0)));
        day2.setText(String.format("%s", day.charAt(1)));
        mon1.setText(String.format("%s", month.charAt(0)));
        mon2.setText(String.format("%s", month.charAt(1)));
        year1.setText(String.format("%s", year.charAt(0)));
        year2.setText(String.format("%s", year.charAt(1)));
        year3.setText(String.format("%s", year.charAt(2)));
        year4.setText(String.format("%s", year.charAt(3)));
        handelNextButton(true);
    }


    /*
    * Validating the input*/
    private void validateInput()
    {
        boolean isCompleted;
        if(day1.getText().length()<1)
        {
            isCompleted=false;
            day1.requestFocus();
        }else if(day2.getText().length()<1)
        {
            isCompleted=false;
            day2.requestFocus();
        }else if(mon1.getText().length()<1)
        {
            isCompleted=false;
            mon1.requestFocus();
        }else if(mon2.getText().length()<1)
        {
            isCompleted=false;
            mon2.requestFocus();
        }else if(year1.getText().length()<1)
        {
            isCompleted=false;
            year1.requestFocus();
        }else if(year2.getText().length()<1)
        {
            isCompleted=false;
            year2.requestFocus();
        }else if(year3.getText().length()<1)
        {
            isCompleted=false;
            year3.requestFocus();
        }else if(year4.getText().length()<1)
        {
            isCompleted=false;
            year4.requestFocus();
        }else
        {
            isCompleted=true;
        }
        handelNextButton(isCompleted);
    }

    /*
    *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            next_button.setEnabled(true);
        }
        else {
            next_button.setEnabled(false);
        }
    }



    @OnClick(R.id.close_button)
    void onClose()
    {
        activity.onBackPressed();
    }

    @OnClick(R.id.btnNext)
    void onNext()
    {
        String day=day1.getText().toString()+""+day2.getText().toString();
        String month=mon1.getText().toString()+""+mon2.getText().toString();
        String year=year1.getText().toString()+""+year2.getText().toString()+year3.getText().toString()+""+year4.getText().toString();
        presenter.validateAge(day,month,year);
    }

    @OnClick(R.id.parentLayout)
    void onParentClick(){}

    @OnClick(R.id.skip_page)
    void onSkipCLicked()
    {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        utility.closeSpotInputKey(activity,day1);
        this.finish();
        activity.overridePendingTransition(R.anim.slide_from_left,R.anim.slide_to_right);
        super.onBackPressed();
    }
}
