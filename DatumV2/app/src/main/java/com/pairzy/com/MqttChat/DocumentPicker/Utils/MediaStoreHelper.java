package com.pairzy.com.MqttChat.DocumentPicker.Utils;

/**
 * Created by moda on 22/08/17.
 */

import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;

import com.pairzy.com.MqttChat.DocumentPicker.Cursors.DocScannerTask;
import com.pairzy.com.MqttChat.DocumentPicker.Cursors.LoaderCallbacks.FileResultCallback;
import com.pairzy.com.MqttChat.DocumentPicker.Cursors.LoaderCallbacks.PhotoDirLoaderCallbacks;
import com.pairzy.com.MqttChat.DocumentPicker.FilePickerConst;
import com.pairzy.com.MqttChat.DocumentPicker.Models.Document;
import com.pairzy.com.MqttChat.DocumentPicker.Models.PhotoDirectory;


public class MediaStoreHelper {

    public static void getPhotoDirs(FragmentActivity activity, Bundle args, FileResultCallback<PhotoDirectory> resultCallback) {
        if (activity.getSupportLoaderManager().getLoader(FilePickerConst.MEDIA_TYPE_IMAGE) != null)
            activity.getSupportLoaderManager().restartLoader(FilePickerConst.MEDIA_TYPE_IMAGE, args, new PhotoDirLoaderCallbacks(activity, resultCallback));
        else
            activity.getSupportLoaderManager().initLoader(FilePickerConst.MEDIA_TYPE_IMAGE, args, new PhotoDirLoaderCallbacks(activity, resultCallback));
    }

    public static void getVideoDirs(FragmentActivity activity, Bundle args, FileResultCallback<PhotoDirectory> resultCallback) {
        if (activity.getSupportLoaderManager().getLoader(FilePickerConst.MEDIA_TYPE_VIDEO) != null)
            activity.getSupportLoaderManager().restartLoader(FilePickerConst.MEDIA_TYPE_VIDEO, args, new PhotoDirLoaderCallbacks(activity, resultCallback));
        else
            activity.getSupportLoaderManager().initLoader(FilePickerConst.MEDIA_TYPE_VIDEO, args, new PhotoDirLoaderCallbacks(activity, resultCallback));
    }

    public static void getDocs(FragmentActivity activity, FileResultCallback<Document> fileResultCallback) {
        new DocScannerTask(activity, fileResultCallback).execute();
    }
}