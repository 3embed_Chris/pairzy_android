package com.pairzy.com.chatMessageScreen;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.SpendCoinDialog.CoinDialog;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.progressbar.LoadingProgress;
import com.pairzy.com.util.timerDialog.TimerDialog;

import dagger.Module;
import dagger.Provides;

@Module
public class ChatMessageUtil {

    @ActivityScoped
    @Provides
    LoadingProgress provideLoadingProgress(Activity activity){
        return new LoadingProgress(activity);
    }

    @ActivityScoped
    @Provides
    CoinDialog provideSpendCoinDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility){
        return  new CoinDialog(activity, typeFaceManager,utility);
    }

    @ActivityScoped
    @Provides
    TimerDialog timerDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility, PreferenceTaskDataSource dataSource){
        return  new TimerDialog(activity,typeFaceManager,utility,dataSource);
    }
}
