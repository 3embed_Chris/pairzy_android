package com.pairzy.com.likes.model;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

public class LikesAdapter extends RecyclerView.Adapter {
    private ArrayList<LikesDataPojo> list;
    private TypeFaceManager typeFaceManager;

    public LikesAdapter(ArrayList<LikesDataPojo> list, TypeFaceManager typeFaceManager)
    {
        this.typeFaceManager=typeFaceManager;
        this.list=list;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.likes_item,parent,false);
        return new LikesViewHolder(view,typeFaceManager);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        LikesViewHolder viewHolder= (LikesViewHolder) holder;
        viewHolder.profilePic.setImageURI(list.get(position).getProfilePic());
        viewHolder.profileName.setText(list.get(position).getLikersName());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
