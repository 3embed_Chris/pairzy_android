package com.pairzy.com.MyProfile.editPreference.newListScrollerFrag;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.MyProfile.editPreference.EditAnimatorHandler;
import com.pairzy.com.MyProfile.editPreference.EditPrefPresenter;
import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.data.model.PrefData;
import com.pairzy.com.util.RecyclerItemClickListener;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * <h2>NewListdataFrg</h2>
 * A simple {@link Fragment} subclass.
 * @author 3Embed.
 * @since 17-02-2017.
 * @version 1.0.
 */
@ActivityScoped
public class NewListdataFrg extends DaggerFragment implements NewListdataFrgContract.View
{
    public static final String ITEM_POSITION="pref_item";
    @Inject
    Utility utility;
    @Inject
    Activity activity;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    EditPrefPresenter mainpresenter;
    @Inject
    NewListdataFrgContract.Presenter presenter;
    @Inject
    EditAnimatorHandler animatorHandler;
    private Unbinder unbinder;
    @BindView(R.id.skip_page)
    TextView skip_page;
    @BindView(R.id.first_title)
    TextView first_title;
    @BindView(R.id.second_title)
    TextView second_title;
    @BindView(R.id.not_to_say)
    AppCompatRadioButton not_to_say;
    @BindView(R.id.btnNext)
    RelativeLayout btnNext;
    @BindView(R.id.option_list)
    RecyclerView option_list;
    @BindView(R.id.back_button_img)
    ImageView back_button_img;
    private int seletionType;
    private PrefData currentPrefData;
    private boolean IsPereferedNotSay;
    private ArrayList<ListData> options_list_data;
    private NewOptionListAdapter optionListAdapter;
    private Animation anim1,anim2;
    private View last_selected=null;

    @Inject
    public NewListdataFrg() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle bundle=getArguments();
        assert bundle != null;
        currentPrefData= (PrefData) bundle.getSerializable("pref_data");
        options_list_data =new ArrayList<>();
        optionListAdapter=new NewOptionListAdapter(activity,options_list_data,typeFaceManager);
        this.anim1=animatorHandler.getViewScaleUp();
        this.anim2=animatorHandler.getViewScaleDown();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.new_fragment_listdata_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder= ButterKnife.bind(this,view);
        upDateUI();
        collectData();
        presenter.initSelectionList(currentPrefData.getSelectedValues(),seletionType);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        presenter.takeView(this);
        utility.closeSpotInputKey(activity,first_title);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
    }

    /*
     * Updating the required ui updating like fonts etc.*/
    private void upDateUI()
    {
        btnNext.setEnabled(false);
        skip_page.setTypeface(typeFaceManager.getCircularAirBook());
        first_title.setTypeface(typeFaceManager.getCircularAirBold());
        second_title.setTypeface(typeFaceManager.getCircularAirBold());
        not_to_say.setTypeface(typeFaceManager.getCircularAirBook());
        if(currentPrefData.getType()==10)
        {
            not_to_say.setVisibility(View.GONE);
        }
        option_list.setLayoutManager(new LinearLayoutManager(activity));
        option_list.setHasFixedSize(true);
        option_list.setAdapter(optionListAdapter);
        option_list.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),option_list,new RecyclerItemClickListener.OnItemClickListener()
        {
            @Override
            public void onItemClick(final View view, final int position)
            {
                view.startAnimation(anim1);
                anim1.setAnimationListener(new Animation.AnimationListener()
                {
                    @Override
                    public void onAnimationStart(Animation animation) {}
                    @Override
                    public void onAnimationRepeat(Animation animation) {}
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        view.startAnimation(anim2);
                    }
                });
                anim2.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {}
                    @Override
                    public void onAnimationEnd(Animation animation)
                    {
                        handelSelection(view,position);
                    }
                    @Override
                    public void onAnimationRepeat(Animation animation)
                    {}
                });
            }
            @Override
            public void onItemLongClick(View view, int position) {}
        }));
    }

    /*
     *private void handel selection.*/
    private void handelSelection(View view,int item_position)
    {
        invalidateNotToSayState();
        if(!IsPereferedNotSay) {
            if (seletionType == 1) { //multi selection.
                ListData listData = options_list_data.get(item_position);
                options_list_data.get(item_position).setSelected(!listData.isSelected());
                presenter.updateSelection(options_list_data, seletionType);
                optionListAdapter.notifyDataSetChanged();
                handelNextButton(presenter.isDataExist());
            } else { //single selection
                for (ListData listData : options_list_data) {
                    listData.setSelected(false);
                }
                options_list_data.get(item_position).setSelected(true);
                presenter.updateSelection(options_list_data, seletionType);
                optionListAdapter.notifyDataSetChanged();
                handelNextButton(true);
            }
            view.clearAnimation();
        }
    }

    private void invalidateNotToSayState() {
        if(IsPereferedNotSay) {
            IsPereferedNotSay = false;
            not_to_say.setSelected(false);
            not_to_say.setChecked(false);
        }
    }

    /*
     *collecting the details. */
    private void collectData()
    {
        if(currentPrefData.getType()==1||currentPrefData.getType()==10)
            seletionType=0;
        else
            seletionType=1;

        first_title.setText(utility.formatString(currentPrefData.getTitle()));
        second_title.setText(utility.formatString(currentPrefData.getLabel()));
        options_list_data.clear();
        boolean selected;
        for(String option: currentPrefData.getOptions()){
            selected = false;
            for(String selectedOption:currentPrefData.getSelectedValues()){
                if(selectedOption.equals(option)){
                    selected =true;
                }
            }
            if(selected)
                options_list_data.add(new ListData(option,true));
            else
                options_list_data.add(new ListData(option,false));
        }
        if(currentPrefData.getSelectedValues().size()>0)
            handelNextButton(true);
        optionListAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.btnNext)
    void onNextClicked()
    {
        if(IsPereferedNotSay)
        {
            presenter.updatePreference(currentPrefData.getId(),seletionType,true);
            //mainpresenter.openNextFrag(item_position+1);
        }else
        {
            presenter.updatePreference(currentPrefData.getId(),seletionType,false);
        }
        onBackClicked();
    }

    @OnClick(R.id.close_button)
    void onBackClicked()
    {
        activity.onBackPressed();
    }
    @OnClick(R.id.skip_page)
    void onSkip()
    {
        activity.onBackPressed();
    }
    @OnClick(R.id.parent_view)
    void onParentClicked(){}

    @OnClick(R.id.not_to_say)
    void onRadioButtonChecked(RadioButton radioButton)
    {
        if(radioButton.isSelected())
        {
            radioButton.setSelected(false);
            radioButton.setChecked(false);
        }else
        {
            option_list.setAdapter(optionListAdapter);
            radioButton.setSelected(true);
            radioButton.setChecked(true);
        }
        IsPereferedNotSay=radioButton.isSelected();
        if(IsPereferedNotSay)
        {
            //IsPereferedNotSay=presenter.isDataExist();
            //clear selected value
            for(ListData listData : options_list_data){
                listData.setSelected(false);
            }
            presenter.updateSelection(options_list_data,seletionType);
            optionListAdapter.notifyDataSetChanged();
        }
        handelNextButton(IsPereferedNotSay);
    }

    @Override
    public void showError(String error)
    {
        mainpresenter.showError(error);
    }

    @Override
    public void updatePreferenceData(ArrayList<String> selectedValue) {
        currentPrefData.setSelectedValues(selectedValue);
        mainpresenter.updatePreferenceData(currentPrefData);
    }

    @Override
    public void showMessage(String message)
    {
        mainpresenter.showMessage(message);
    }

    /*
    *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            btnNext.setEnabled(true);
        }
        else {
            btnNext.setEnabled(false);
        }
    }
}
