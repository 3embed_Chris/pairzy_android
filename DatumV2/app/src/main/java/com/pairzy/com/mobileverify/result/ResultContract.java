package com.pairzy.com.mobileverify.result;

import com.pairzy.com.BasePresenter;

/**
 * <h2>ResultContract</h2>
 * <p>
 * Contains @{@link ResultFragment}'s @{@link View} and @{@link Presenter}
 * </P>
 *
 * @author 3Embed.
 * @version 1.0.
 * @since 06/01/2018.
 **/

public interface ResultContract
{
    interface View
    {
        void updateResponseUI(String message);
        /**
         * <p>Displays message</p>
         *
         * @param message : String message
         */
        void showMessage(String message);
        /**
         * <p>Action when mobile number is not available in database</p>
         */
        void mobileNotAvailable();

        void updateMessageText(String message);
        /**
         * <p>Action when mobile number is available in database</p>
         */
        void mobileAvailable();

        void openOtpFrg();

        void onLogin();

        void openSingUp();

        /**
         * <p>Action when OTP send successfully to the mobile number</p>
         */
        void otpSent();

        void finishActivity(String phno);

        void onlyRequestForOtp();

        void goBackToMainPage();
    }

    interface Presenter extends BasePresenter
    {
        /**
         * <p>Checks mobile number is available or not</p>
         *
         * @param mobile_number : Mobile number
         */
        void RequestOtpMobile(String countryCode, String mobile_number);
        /**
         * <p>Sends OTP to given mobile number</p>
         *
         * @param mobile_number : Mobile number
         */
        void sendOtp(String mobile_number);

        void openOtpPage();


        void verifyOtp(String countryCode, String phno, String otp);


        void showMessage(String string);
    }
}
