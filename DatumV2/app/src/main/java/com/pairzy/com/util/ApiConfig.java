package com.pairzy.com.util;
/**
 *@author 3Embed.
 *@version 1.0.  */
public class ApiConfig
{
    public static final String MESSAGE = "message";

    public interface DeviceType{
        String ANDROID = "2";
    }

    private interface BaseApiConfig
    {
        String AUTHORIZATION ="authorization";
        String DEVICE_ID = "deviceId";
        String DEVICE_MAKER = "deviceMake";
        String DEVICE_MODEL = "deviceModel";
        String DEVICE_TYPE = "deviceType";
        String LANGUAGE= "lang";
        String PUSH_TOKEN = "pushToken";
        String DEVICE_OS = "deviceOs";
        String APP_VERSION = "appVersion";

    }


    public interface FBRequestKey extends BaseApiConfig
    {
        String FIRST_NAME = "firstName";
        String DATE_OF_BIRTH = "dateOfBirth";
        String FB_ID = "fbId";
        String GENDER="gender";
        String HEIGHT="height";
        String EMAIL="email";
        String PROFILE_PIC="profilePic";
        String PROFILE_VIDEO="profileVideo";
        String PROFILE_VIDEO_THUM="profileVideoThumbnail";
        String OTHER_IMAGES="otherImages";
        String BIO_DATA="bioData";
        String WORK="work";
        String EDUCATION="eduation";
    }

    public interface RequestOtpKey extends BaseApiConfig
    {
        String PHONE_NUMBER = "phoneNumber";
        String TYPE = "type";
    }

    public interface VerifyEmailKey
    {
        String EMAIL = "email";
    }

    public interface VerifyMobileKey
    {
        String PHONE_NUMBER = "phoneNumber";
    }
    /*
       *verify otp request data.*/
    public interface VerifyOtpKey extends BaseApiConfig
    {
        //only need during number change
        String COUNTRY_CODE ="countryCode";

        String PHONE_NUMBER ="phoneNumber";
        String TYPE ="type";
        String OTP ="otp";
    }

    public interface VerificationType{
        int NEW_REGISTRATION_OR_LOGIN = 1;
        int PASSWORD_RECOVERY  = 2;
        Object PHONE_UPDATE = 3;
    }
    /*
     *Sign up request data.*/
    public interface signUpKey extends BaseApiConfig
    {
        String COUNTRY_CODE="countryCode";
        String PHONE_NUMBER="contactNumber";
        String OTP ="otp";
        String EMAIL="email";
        String FIRST_NAME="firstName";
        String DOB="dob";
        String GENDER="gender";
        String PROFILE_PIC="profilePic";
        String LATITUDE="latitude";
        String LONGITUDE="longitude";
        String PROFILE_VIDEO="profileVideo";
        String VIDEO_THUMBNAIL="profileVideoThumbnail";
    }

    /*
    * Set preference key.*/
    public interface SetPreferenceKey extends BaseApiConfig
    {
        String PREF_ID="pref_id";
        String PREF_VALUES="values";
    }

    /*
   * Set preference key.*/
    public interface updatePreferenceKey extends BaseApiConfig
    {
        String PREF_ID="pref_id";
        String PREF_VALUES="values";
    }

    public interface SearchPrefUpdateReqKey extends BaseApiConfig
    {
        String PREFERENCE = "preferences";
        String LONGITUDE = "longitude";
        String LATITUDE = "latitude";
        String IS_PASSPORT_LOCATION = "isPassportLocation";
        String ADDRESS="address";
    }

    public interface DoLikeService
    {
        String USER_ID="targetUserId";
    }

    public interface UpdateLocationReqKey
    {
        String LONGITUDE="longitude";
        String LATITUDE="latitude";
        String IP="ip";
        String TIMEZONE="timeZone";
        String IS_PASSPORT_LOCATION = "isPassportLocation";
    }

    /*
     *Update Instagram details*/
    public interface UpdateInstaId extends BaseApiConfig
    {
        String ID="instagramId";
        String TOKEN="instaGramToken";
        String USER_NAME="instagramName";
    }

    public interface GetDate{

    }

    public interface GetPastDate{

    }

    public interface PostDateResponse{
        String RESPONSE = "response";
        String DATE_ID = "date_id";
        String PROPOSED_ON = "proposedOn";
        String LATITUDE = "latitude";
        String LONGITUDE = "longitude";
        String DATE_TYPE = "dateType";
        String PLACE_NAME = "placeName";
    }

    public interface PostDateRating{
        String DATE_ID = "dateId";
        String RATE = "rate";
    }

    public interface SuggestionAlert{
        String DESCRIPTION = "description";
    }

    public interface CoinPlan{
        String PLAN_ID = "planId";
        String PAYMENT_GETWAY_TAX_ID = "paymentGatewayTxnId";
        String TRIGGER = "trigger";
        String PAYMENT_TYPE = "paymentType";
        String PAYMENT_TAXN_ID ="paymentTxnId";
        String USER_PURCHASE_TIME = "userPurchaseTime";
    }

    public interface SubsPlan{
        String PLAN_ID = "planId";
        String PAYMENT_GETWAY_TAX_ID = "paymentGatewayTxnId";
        String PAYMENT_GETWAY = "paymentGateway";
        String USER_PURCHASE_TIME = "userPurchaseTime";
    }

    public interface ReportUser{
        String TARGET_USER_ID = "targetUserId";
        String REPORT_REASON = "reason";
        String  REPORT_MESSAGE= "message";
    }

    public interface BlockOrUnblockUser{
        String TARGET_USER_ID = "targetUserId";
        String  BLOCK_MESSAGE= "message";
    }
    public interface UnMatchUser{
        String TARGET_USER_ID = "targetUserId";
    }

    public interface ADD_COIN_TO_WALLET{
        String COIN_AMOUNT = "coinAmount";
    }

    public interface COMMENT
    {
        String POST_ID="postId";
        String COMMENT="comment";
    }

    public interface DELETE_POST
    {
        String POST_ID="postId";
    }

    public interface LIKE_UNLIKE
    {
        String POST_ID="postId";
        String TYPE="type";
    }

    public interface POST
    {

        String TYPE_FLAG="typeFlag";
        String DESC="description";
        String URL="url";
    }


    public interface MOMENTS
    {
        String MOMENTS_LIST="momentsList";
        String POSITION="position";
        String RESULT="result";
    }

    public interface MATCH
    {
       String POST_ID="pairId";
        String ACTION="action";
    }
}
