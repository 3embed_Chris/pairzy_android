package com.pairzy.com.boostLikes;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 7/9/18.
 */

@Module
public abstract class BoostLikeModule {


    @ActivityScoped
    @Binds
    abstract BoostLikeContract.View bindView(BoostLikeActivity boostLikeActivity);

    @ActivityScoped
    @Binds
    abstract Activity bindActivity(BoostLikeActivity boostLikeActivity);

    @ActivityScoped
    @Binds
    abstract BoostLikeContract.Presenter bindPresenter(BoostLikePresenter presenter);

}
