package com.pairzy.com.util.CustomView;

/**
 * <h2>DatumBounceInterpolator</h2>
 * <P>
 *
 * </P>
 * @since  3/21/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class DatumBounceInterpolator implements android.view.animation.Interpolator
{
    private double mAmplitude = 1;
    private double mFrequency = 10;

    DatumBounceInterpolator(double amplitude, double frequency)
    {
        mAmplitude = amplitude;
        mFrequency = frequency;
    }

    public float getInterpolation(float time) {
        return (float) (-1 * Math.pow(Math.E, -time/ mAmplitude) *
                Math.cos(mFrequency * time) + 1);
    }
}