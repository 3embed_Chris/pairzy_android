package com.pairzy.com.MyProfile.editEmail;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.MyProfile.MyProfilePagePresenter;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by ankit on 27/4/18.
 */

public class EditEmailActivity extends BaseDaggerActivity implements EditEmailContract.View,
                            TextWatcher{

    @Inject
    EditEmailPresenter presenter;
    private Unbinder unbinder;
    @BindString(R.string.emailError)
    String email_error_msg;
    @BindString(R.string.emailRegister)
    String emailRegister;
    @Inject
    Activity activity;

    @Inject
    Utility utility;

    @Inject
    TypeFaceManager typeFaceManager;

    @BindView(R.id.skip_page)
    TextView skip_page;

    @BindView(R.id.title_first)
    TextView title_first;

    @BindView(R.id.sub_title_text)
    TextView sub_title_text;

    @BindView(R.id.input_email)
    EditText input_email;

    @BindView(R.id.hint_details)
    TextView hint_details;

    @BindView(R.id.btnNext)
    RelativeLayout next_button;
    @BindView(R.id.parent_layout)
    RelativeLayout parent_view;
    String currentEmail="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_email);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        initUIDetails();
        initData(getIntent());
    }

    private void initData(Intent intent) {
        currentEmail = intent.getStringExtra(MyProfilePagePresenter.EMAIL_DATA);
        input_email.setText(currentEmail==null?"":currentEmail.equalsIgnoreCase("NA")?"":currentEmail);
        input_email.setSelection(input_email.getText().toString().length());
        input_email.requestFocus();
    }

    /*
     * intialization of the xml content.*/
    private void initUIDetails()
    {
        next_button.setEnabled(false);
        skip_page.setTypeface(typeFaceManager.getCircularAirBook());
        title_first.setTypeface(typeFaceManager.getCircularAirBold());
        sub_title_text.setTypeface(typeFaceManager.getCircularAirBold());
        input_email.setTypeface(typeFaceManager.getCircularAirBook());
        hint_details.setTypeface(typeFaceManager.getCircularAirLight());
        input_email.addTextChangedListener(this);
        input_email.setOnEditorActionListener((v, actionId, event) -> {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE))
            {
                if(presenter.validateEmail(input_email.getText().toString()))
                {
                    presenter.checkEmailIdExist(input_email.getText().toString());
                }else
                {
                    showError( email_error_msg);
                }
            }
            return false;
        });
    }

    @OnClick(R.id.close_button)
    void onClose()
    {
        onBackPressed();
    }

    @OnClick(R.id.btnNext)
    void onNext()
    {
        utility.closeSpotInputKey(activity,input_email);
        if(currentEmail.equalsIgnoreCase(input_email.getText().toString())) {
            onBackPressed();
        }
        else
            presenter.checkEmailIdExist(input_email.getText().toString());
    }

    @OnClick(R.id.skip_page)
    void onSkipClick()
    {
        onBackPressed();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {}
    @Override
    public void afterTextChanged(Editable editable) {
        if(presenter.validateEmail(editable.toString()))
        {
            handelNextButton(true);
        }else
        {
            handelNextButton(false);
        }
    }
    @Override
    public void showError(String error) {
        Snackbar snackbar = Snackbar
                .make(parent_view,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void finishActivity(String email)
    {
        Intent intent = new Intent();
        intent.putExtra(MyProfilePagePresenter.EMAIL_DATA,email);
        setResult(RESULT_OK,intent);
        onBackPressed();
    }

    @Override
    public void emailNotAvailable()
    {
        showError(emailRegister);
    }

    /*
     *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            next_button.setEnabled(true);
        }
        else {
            next_button.setEnabled(false);
        }
    }

    @Override
    public void onBackPressed() {
        utility.closeSpotInputKey(activity,input_email);
        this.finish();
        activity.overridePendingTransition(R.anim.slide_from_left,R.anim.slide_to_right);
        super.onBackPressed();
    }
}
