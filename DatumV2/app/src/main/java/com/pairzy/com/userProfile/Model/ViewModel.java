package com.pairzy.com.userProfile.Model;

import android.annotation.SuppressLint;
import android.app.Activity;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.data.model.PrefData;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import java.util.ArrayList;
import javax.inject.Inject;
/**
 *<h2>ProfileViewModel</h2>
 * <P>
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 12-04-2017.
 * */
public class ViewModel
{
    private OnRetryCallback callback;
    @Inject
    Utility utility;
    @Inject
    Activity activity;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    PreferenceTaskDataSource dataSource;

    public  interface OnRetryCallback{
        void onRetry();
    }
    @Inject
    ViewModel() {}

    public View loadingView()
    {
        @SuppressLint("InflateParams")
        View convertView = activity.getLayoutInflater().inflate(R.layout.user_loading_onfo_view, null);
        convertView.findViewById(R.id.loading_progress).setVisibility(View.VISIBLE);
        convertView.findViewById(R.id.connection_error_icon).setVisibility(View.GONE);
        convertView.findViewById(R.id.message_text).setVisibility(View.VISIBLE);
        convertView.findViewById(R.id.error_message).setVisibility(View.GONE);
        return convertView;
    }

    public void setOnRetryCallback(OnRetryCallback callback){
        this.callback = callback;
    }
    /*
     *Adding the error view on loading failed.*/
    public View errorOnLoadingView(boolean isInternetError)
    {
        @SuppressLint("InflateParams")
        View convertView = activity.getLayoutInflater().inflate(R.layout.user_loading_onfo_view, null);
        convertView.findViewById(R.id.loading_progress).setVisibility(View.GONE);
        convertView.findViewById(R.id.connection_error_icon).setVisibility(View.VISIBLE);
        convertView.findViewById(R.id.message_text).setVisibility(View.GONE);
        TextView tvError = convertView.findViewById(R.id.error_message);
        tvError.setVisibility(View.VISIBLE);
        if(isInternetError)
            tvError.setText(R.string.no_internet_error);
        Button btnRetry = convertView.findViewById(R.id.btn_retry);
        btnRetry.setTypeface(typeFaceManager.getCircularAirBold());
        btnRetry.setVisibility(View.VISIBLE);
        btnRetry.setOnClickListener(view ->{
            if(callback != null)
                callback.onRetry();
        });
        return convertView;
    }

    /*
     * Getting item details view*/
    public View addDetailsView(ArrayList<PrefData> prefDataList, String categoryName)
    {
        @SuppressLint("InflateParams")
        View convertView = activity.getLayoutInflater().inflate(R.layout.user_list_item_row, null);
        TextView profileCategoryTv = convertView.findViewById(R.id.profileCategoryTv);
        profileCategoryTv.setText(categoryName);
        profileCategoryTv.setTypeface(typeFaceManager.getCircularAirBold());
        RecyclerView profileCategoryRv = convertView.findViewById(R.id.profileCategory_rv1);
        UserProfileAdapter madapter = new UserProfileAdapter(prefDataList,typeFaceManager);
        profileCategoryRv.setHasFixedSize(true);
        RecyclerView.LayoutManager mlayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        profileCategoryRv.setLayoutManager(mlayoutManager);
        profileCategoryRv.setItemAnimator(new DefaultItemAnimator());
        profileCategoryRv.setAdapter(madapter);
        profileCategoryRv.setNestedScrollingEnabled(false);
        return convertView;
    }
}
