package com.pairzy.com.home.Prospects.LikesMe;
import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.pairzy.com.home.Prospects.OnAdapterItemClicked;

/**
 * <h2>LikesMeContract</h2>
 * <P>
 *
 * </P>
 * @since  3/23/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public interface LikesMeContract
{
    interface View extends BaseView
    {
        void showError(int id);
        void showError(String message);
        void showMessage(int message);
        void onDataUpdate();
        void showProgress();
        void onApiError(String message);
        void emptyData();
        void openUserProfile(String data);
        void adapterListener(OnAdapterItemClicked adapterCallabck);

    }


    interface Presenter extends BasePresenter<View>
    {
        void initAdapterListener();
        void initMatchListener();
        void  getListData(boolean isLoadMore);
        void pee_fetchProfile(int position);
        boolean checkLoadMore(int position);
    }
}
