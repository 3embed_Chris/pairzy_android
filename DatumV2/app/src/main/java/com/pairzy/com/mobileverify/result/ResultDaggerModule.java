package com.pairzy.com.mobileverify.result;

import com.pairzy.com.dagger.FragmentScoped;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
/**
 * <h1>PhnoDaggerModule</h1>
 * This will provide the object of SignUpPresenter
 * @author Shaktisinh Jadeja
 * @since 08/01/2018
 */
@Module
public interface ResultDaggerModule
{
    @Named("da")
    @FragmentScoped
    @Binds
     ResultFragment fragment(ResultFragment resultFragment);

    @FragmentScoped
    @Binds
     ResultContract.Presenter taskPresenter(ResultPresenter presenter);
}