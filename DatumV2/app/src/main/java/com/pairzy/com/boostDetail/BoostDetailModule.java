package com.pairzy.com.boostDetail;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * <h>PassportModule class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

@Module
public abstract class BoostDetailModule {

    @ActivityScoped
    @Binds
    abstract BoostDetailContract.Presenter boostDetailPresenter(BoostDetailPresenter presenter);

    @ActivityScoped
    @Binds
    abstract BoostDetailContract.View boostDetailView(BoostDetailActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity boostDetailActivity(BoostDetailActivity activity);
}
