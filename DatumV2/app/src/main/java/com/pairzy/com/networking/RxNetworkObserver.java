package com.pairzy.com.networking;

import android.util.Log;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observables.ConnectableObservable;

/**
 * <h2>RxNetworkObserver</h2>
 * <P>
 *  Its the network observer class to observe the network
 *  changes in the App.
 * </P>
 * @version 1.0.
 * @author 3Embed.*/

public class RxNetworkObserver
{
    private  ConnectableObservable<NetworkStateHolder> connectableObservable;
    private ObservableEmitter<NetworkStateHolder> emitor;

    public RxNetworkObserver()
    {
        Observable<NetworkStateHolder> observable=Observable.create(e -> emitor=e);
        connectableObservable=observable.publish();
        connectableObservable.share();
        connectableObservable.replay();
        connectableObservable.connect();
    }

    public ConnectableObservable<NetworkStateHolder> getObservable()
    {
        return connectableObservable;
    }

    void publishData(NetworkStateHolder data)
    {
        Log.d("isConected", ""+data.isConnected());
        if(emitor!=null)
        {
            emitor.onNext(data);
        }
    }
}
