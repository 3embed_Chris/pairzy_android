package com.pairzy.com.dublyCamera.CameraInFragments.CustomGallery;

/**
 * <h2>CustomGalleryBucketItemListener</h2>
 * @since 20/1/17.
 */

public interface CustomGalleryBucketItemListener
{
    void onCLick(int position);
}
