package com.pairzy.com.home.Prospects.LikesMe.Model;

import android.net.Uri;
import com.pairzy.com.BaseModel;
import com.pairzy.com.home.Prospects.LikesMe.LikesMePresenter;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.Utility;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
/**
 * <h2>LikesMeDataModel</h2>
 * @since  3/27/2018.
 * @version 1.0.
 * @since 12-03-2017.
 */
public class LikesMeDataModel extends BaseModel
{
    private boolean isServerLMActive;
    @Inject
    Utility utility;
    @Inject
    LikesMeAdapter adapter;

    @Inject
    LikesMeDataModel(){}
    @Inject
    ArrayList<LikesMeItemPojo> userList;

    /*
     * Parsing the data from the server*/
    public void parseData(String response) throws Exception
    {
        try
        {
            LikesMeResponseHolder result_data=utility.getGson().fromJson(response,LikesMeResponseHolder.class);
            ArrayList<LikesMeItemPojo> temp=result_data.getData();
            if(temp!=null&&temp.size()>0)
            {

                int prefetchPosition;
                if(LikesMePresenter.INDEX==0)
                {
                    userList.clear();
                    prefetchPosition=0;
                }else
                {
                    removeLoadMoreItem();
                    prefetchPosition=userList.size()-1;
                }
                isServerLMActive=temp.size()==LikesMePresenter.FEED_SIZE;
                userList.addAll(temp);
                adapter.notifyDataSetChanged();
                prefetchImage(prefetchPosition);
            }else
            {
                throw new Exception("CoinData is empty!");
            }
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    /**
     * Handelign the progess bar
     */
    public boolean handelLoadMore()
    {
        if(LikesMePresenter.INDEX!=0&&userList.size()>0)
        {
            try
            {
                int index=userList.size()-1;
                if(index>=0)
                {
                    LikesMeItemPojo loading_item=userList.get(index);
                    if(!loading_item.isLoading())
                    {
                        LikesMeItemPojo loading=new LikesMeItemPojo();
                        loading.setLoading(true);
                        loading.setLoadingFailed(false);
                        userList.add(loading);
                        adapter.notifyDataSetChanged();
                    }
                }

            }catch (Exception e ) {}
            return true;
        }
        return false;
    }

    /*
     *Handel failed on load more */
    public boolean handelFailedLoadMore()
    {
        if(LikesMePresenter.INDEX!=0&&userList.size()>0)
        {
            try
            {
                int index=userList.size()-1;
                if(index>=0)
                {
                    LikesMeItemPojo loading_item=userList.get(index);
                    if(loading_item.isLoading())
                    {
                        userList.remove(index);
                        adapter.notifyDataSetChanged();
                    }
                }
            }catch (Exception e ) {}
            return true;
        }
        return false;
    }


    /*
     *Checking load more call is required */
    public boolean checkLoadMoreRequired(int position)
    {
        try
        {
            int size=userList.size();
            int thrashHold=size-5;
            if(isServerLMActive&&thrashHold>0&&position>thrashHold)
            {
                LikesMeItemPojo loading_item=userList.get(size-1);
                return !loading_item.isLoading();
            }else
            {
                return false;
            }
        }catch (Exception e){
            return false;
        }
    }

    /*
     * Remove load more item*/
    private void removeLoadMoreItem()
    {
        try
        {
            int index=userList.size()-1;
            if(index>=0)
            {
                LikesMeItemPojo loading_item=userList.get(index);
                if(loading_item.isLoading())
                {
                    userList.remove(index);
                    adapter.notifyDataSetChanged();
                }
            }

        }catch (Exception e ) {}
    }

    /*
    * Changing the user matched status*/
    public void changeTheStatus(String userId)
    {
        try
        {
            for(LikesMeItemPojo itemPojo:userList)
            {
                if(!itemPojo.isLoading())
                {
                    if(itemPojo.getOpponentId().equals(userId))
                    {
                        itemPojo.setMatched(true);
                    }
                }
            }
        }catch (Exception e)
        {}
    }
    /**
     *Method is controller for the image pre fetch.
     * @param position contains the position from where we start prefatch.*/
    public void prefetchImage(int position)
    {
        if(position<0)
        {
            position=0;
        }
        int end_position=position+8;
        if(end_position>userList.size())
        {
            end_position=userList.size();
        }

        try
        {
            List<LikesMeItemPojo> temp_list=userList.subList(position,end_position);
            for(LikesMeItemPojo item:temp_list)
            {
                if(!item.isLoading())
                {
                    prefetch_Image(item.getProfilePic());
                    ArrayList<String> otherImage=item.getOtherImages();
                    if(otherImage!=null&&otherImage.size()>0)
                    {
                        for(String url:otherImage)
                        {
                            prefetch_Image(url);
                        }
                    }
                }
            }
        }catch (Exception e){}

        int start=position-8;
        if(start<0)
        {
            start=0;
        }

        try
        {
            if(start<position)
            {
                List<LikesMeItemPojo> temp_list=userList.subList(start,position);
                for(LikesMeItemPojo item:temp_list)
                {
                    if(!item.isLoading())
                    {
                        prefetch_Image(item.getProfilePic());
                        ArrayList<String> otherImage=item.getOtherImages();
                        if(otherImage!=null&&otherImage.size()>0)
                        {
                            for(String url:otherImage)
                            {
                                prefetch_Image(url);
                            }
                        }
                    }
                }
            }
        }catch (Exception e){}
    }

    /**
     * It prefetch the images for loaded item
     * @see Fresco#getImagePipeline()
     */
    private void prefetch_Image(String pic_url)
    {
        ImagePipeline pipeline = Fresco.getImagePipeline();
        Uri mainUri = Uri.parse(pic_url);
        ImageRequest profilePictureRequest = ImageRequestBuilder
                .newBuilderWithSource(mainUri)
                .setResizeOptions(new ResizeOptions(54, 54))
                .build();
        pipeline.prefetchToDiskCache(profilePictureRequest, null);
    }

    /*
     * User details*/
    public String getUserDetails(int position) throws EmptyData
    {
        try
        {
            LikesMeItemPojo temp=userList.get(position);
            if(temp!=null)
            {
                return utility.getGson().toJson(temp);
            }else
            {
                throw new EmptyData("CoinData is empty!");
            }
        }catch (Exception e){
            throw new EmptyData("CoinData is empty!");
        }
    }
}
