package com.pairzy.com.editProfile.Model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.pairzy.com.R;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfilePicLoadingViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.sdv_profile_pic)
    SimpleDraweeView sdvProfilePic;
    @BindView(R.id.loading_progress_bar)
    ProgressBar loadingProgressBar;
    public ProfilePicLoadingViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}