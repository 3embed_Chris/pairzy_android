package com.pairzy.com.register.Email;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.register.Name.NameFragment;
import com.pairzy.com.register.RegisterContact;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * A simple {@link Fragment} subclass.
 */
@ActivityScoped
public class EmailFragment extends DaggerFragment implements EmailFrgContract.View ,TextWatcher
{
    private Unbinder unbinder;
    @BindString(R.string.emailError)
    String email_error_msg;
    @BindString(R.string.emailRegister)
    String emailRegister;
    @Inject
    Activity activity;

    @Inject
    Utility utility;

    @Inject
    TypeFaceManager typeFaceManager;

    @BindView(R.id.skip_page)
    TextView skip_page;

    @BindView(R.id.title_first)
    TextView title_first;

    @BindView(R.id.sub_title_text)
    TextView sub_title_text;

    @BindView(R.id.input_email)
    EditText input_email;

    @BindView(R.id.hint_details)
    TextView hint_details;

    @BindView(R.id.btnNext)
    RelativeLayout next_button;

    @Inject
    EmailFrgContract.Presenter presenter;

    @Inject
    RegisterContact.Presenter main_presenter;

    @Inject
    public EmailFragment() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_email, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder=ButterKnife.bind(this,view);
        presenter.takeView(this);
        initUIDetails();
    }

    @Override
    public void onStart() {
        super.onStart();
        input_email.requestFocus();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        main_presenter.updateProgress(1);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
    }

    /*
     * intialization of the xml content.*/
    private void initUIDetails()
    {
        next_button.setEnabled(false);
        skip_page.setTypeface(typeFaceManager.getCircularAirBook());
        title_first.setTypeface(typeFaceManager.getCircularAirBold());
        sub_title_text.setTypeface(typeFaceManager.getCircularAirBold());
        input_email.setTypeface(typeFaceManager.getCircularAirBook());
        hint_details.setTypeface(typeFaceManager.getCircularAirLight());
        input_email.addTextChangedListener(this);
        input_email.setOnEditorActionListener((v, actionId, event) -> {
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE))
            {
                if(presenter.validateEmail(input_email.getText().toString()))
                {
                  presenter.checkEmailIdExist(input_email.getText().toString());
                }else
                {
                 main_presenter.showError( email_error_msg);
                }
            }
            return false;
        });
        utility.openSoftInputKeyInDelay(activity,input_email);
    }

    @OnClick(R.id.back_button)
    void onClose()
    {
        activity.onBackPressed();
    }

    @OnClick(R.id.btnNext)
    void onNext()
    {
        presenter.checkEmailIdExist(input_email.getText().toString());
    }

    @OnClick(R.id.skip_page)
    void onSkipClick()
    {
        moveNextFragment("");
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {}
    @Override
    public void afterTextChanged(Editable editable) {
        if(presenter.validateEmail(editable.toString()))
        {
            handelNextButton(true);
        }else
        {
            handelNextButton(false);
        }
    }

    @Override
    public void showError(String message)
    {
        main_presenter.showError(message);
    }

    @Override
    public void moveNextFragment(String email)
    {
        NameFragment nameFragment=new NameFragment();
        Bundle data=new Bundle();
        data.putString(RegisterContact.Presenter.EMAIL_DATA,email);
        nameFragment.setArguments(data);
        main_presenter.launchFragment(nameFragment,true);
    }

    @Override
    public void emailNotAvailable()
    {
        main_presenter.showError(emailRegister);
    }


    /*
     *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            next_button.setEnabled(true);
        }
        else {
            next_button.setEnabled(false);
        }
    }
}
