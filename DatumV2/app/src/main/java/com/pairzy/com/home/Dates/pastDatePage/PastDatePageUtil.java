package com.pairzy.com.home.Dates.pastDatePage;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.home.Dates.DatesFragUtil;
import com.pairzy.com.home.Dates.pastDatePage.Model.PastDateAdapter;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;
import com.pairzy.com.util.RatingDialog.RatingDialog;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * <h2>PastDatePageUtil</h2>
 * <P>
 *     User pending list adapter.
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class PastDatePageUtil
{
    public static final String PAST_DATE_LAYOUT_MANAGER = "past_date_layout_manager";
    @Provides
    @FragmentScoped
    PastDateAdapter getAdapter(Utility utility, @Named(DatesFragUtil.PAST_LIST) ArrayList<PastDateListPojo> list, TypeFaceManager typeFaceManager)
    {
        return new PastDateAdapter(utility,list,typeFaceManager);
    }

    @Named(PastDatePageUtil.PAST_DATE_LAYOUT_MANAGER)
    @Provides
    @FragmentScoped
    LinearLayoutManager provideLinearLayoutManager(Activity activity)
    {
        return new LinearLayoutManager(activity);
    }

    @Provides
    @FragmentScoped
    RatingDialog provideRatingDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        return  new RatingDialog(activity,typeFaceManager);
    }

}
