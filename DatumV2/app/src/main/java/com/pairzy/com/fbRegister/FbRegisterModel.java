package com.pairzy.com.fbRegister;

import android.location.Address;
import android.location.Geocoder;
import android.text.TextUtils;

import com.pairzy.com.AppController;
import com.pairzy.com.BaseModel;
import com.pairzy.com.MqttChat.Database.CouchDbController;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.LoginData;
import com.pairzy.com.data.model.LoginResponse;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.model.coinCoinfig.CoinData;
import com.pairzy.com.data.source.PassportLocationDataSource;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.locationScreen.model.LocationHolder;
import com.pairzy.com.passportLocation.model.PassportLocation;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.DeviceUuidFactory;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.Utility;
import com.facebookmanager.com.FacebookUserDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * @since  2/21/2018.
 */
class FbRegisterModel extends BaseModel
{
    @Inject
    Utility utility;
    @Inject
    DeviceUuidFactory deviceUuidFactory;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    PassportLocationDataSource locationDataSource;
    @Inject
    Geocoder geocoder;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    CoinBalanceHolder coinBalanceHolder;

    @Inject
    FbRegisterModel(){}

    Map<String, Object> prepareFacebookRequest(FacebookUserDetails userDetails)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.FBRequestKey.FIRST_NAME, userDetails.getFirstName());
        Double dob = Double.parseDouble(userDetails.getBirthDate());
        map.put(ApiConfig.FBRequestKey.DATE_OF_BIRTH,dob.longValue());
        map.put(ApiConfig.FBRequestKey.FB_ID, userDetails.getId());
        map.put(ApiConfig.FBRequestKey.PUSH_TOKEN,dataSource.getPushToken());
        map.put(ApiConfig.FBRequestKey.DEVICE_ID,deviceUuidFactory.getDeviceUuid());
        map.put(ApiConfig.FBRequestKey.DEVICE_MAKER,utility.getDeviceMaker());
        map.put(ApiConfig.FBRequestKey.DEVICE_MODEL,utility.getModel());
        map.put(ApiConfig.FBRequestKey.DEVICE_TYPE, ApiConfig.DeviceType.ANDROID);
        if(userDetails.getBio() != null)
            map.put(ApiConfig.FBRequestKey.BIO_DATA,userDetails.getBio());
        if(userDetails.getWork() != null)
            map.put(ApiConfig.FBRequestKey.WORK,userDetails.getWork());
        if(userDetails.getEducation() != null)
            map.put(ApiConfig.FBRequestKey.EDUCATION,userDetails.getEducation());
        int gender_Data=0;
        if(userDetails.getGender().equalsIgnoreCase("male"))
        {
            gender_Data=1;
        }
        else if(userDetails.getGender().equalsIgnoreCase("female"))
        {
            gender_Data=2;
        }
        map.put(ApiConfig.FBRequestKey.GENDER,gender_Data);
        map.put(ApiConfig.FBRequestKey.EMAIL,userDetails.getEmail());
        if(!TextUtils.isEmpty(userDetails.getOtherPic()))
            map.put(ApiConfig.FBRequestKey.OTHER_IMAGES,userDetails.getOtherPic());
        if(!TextUtils.isEmpty(userDetails.getUserPic()))
            map.put(ApiConfig.FBRequestKey.PROFILE_PIC,userDetails.getUserPic());
        else
            map.put(ApiConfig.FBRequestKey.PROFILE_PIC, AppConfig.DEFAULT_PROFILE_PIC);
        map.put(ApiConfig.VerifyOtpKey.DEVICE_OS, ""+android.os.Build.VERSION.RELEASE);
        map.put(ApiConfig.VerifyOtpKey.APP_VERSION, utility.getAppCurrentVersion());
        if(!TextUtils.isEmpty(userDetails.getProfileVideoThumb())){
            map.put(ApiConfig.FBRequestKey.PROFILE_VIDEO , userDetails.getProfileVideo());
            map.put(ApiConfig.FBRequestKey.PROFILE_VIDEO_THUM , userDetails.getProfileVideoThumb());
        }
        return map;
    }

    /*
     * Parsing data.*/
//    public  void parseLoginResponse(String response) throws DataParsingException,EmptyData
//    {
//        try
//        {
//            LoginResponse loginResponse=utility.getGson().fromJson(response,LoginResponse.class);
//            LoginData data= loginResponse.getData();
//            if(data!=null)
//            {
//                try {
//                    if (data.isPassportLocation())
//                    {
//                        LocationResponse locationResponse = data.getLocationResponse();
//                        if (locationResponse != null) {
//                            PassportLocation passportLocation = null;
//                            LocationHolder locationHolder = new LocationHolder();
//                            locationHolder.setLongitude(locationResponse.getLongitude());
//                            locationHolder.setLatitude(locationHolder.getLatitude());
//                            passportLocation = getLocationName(locationHolder);
//                            if (passportLocation != null) {
//                                String jsonPassport = utility.getGson().toJson(passportLocation, PassportLocation.class);
//                                locationDataSource.setFeatureLocation(jsonPassport);
//                                locationDataSource.setFeaturedLcoationActive(true);
//                            }else
//                            {
//                                locationDataSource.setFeaturedLcoationActive(false);
//                            }
//                        }
//                    }else
//                    {
//                        locationDataSource.setFeaturedLcoationActive(false);
//                    }
//
//                    ArrayList<Subscription> subscriptionList = data.getSubscription();
//                    if(subscriptionList != null && !subscriptionList.isEmpty()){
//                        Subscription subscription = subscriptionList.get(0);
//                        String subsJson = utility.getGson().toJson(subscription,Subscription.class);
//                        dataSource.setSubscription(subsJson);
//                    }
//                }catch (Exception ignored){}
//
//                dataSource.setMyDetails(response);
//                dataSource.setToken(data.getToken());
//                dataSource.setName(data.getFirstName());
//                dataSource.setUserId(data.getId());
//                dataSource.setBirthDate(data.getDob());
//                dataSource.setCountryCode(data.getCountryCode());
//                dataSource.setMobileNumber(data.getContactNumber());
//                dataSource.setGender(data.getGender());
//                dataSource.setEmail(data.getEmail());
//                dataSource.setProfilePicture(data.getProfilePic());
//                dataSource.setUserVideoThumbnail(data.getProfileVideoThumbnail());
//                dataSource.setUserVideo(data.getProfileVideo());
//                ArrayList<String> other_images=data.getOtherImages();
//                if(other_images==null)
//                {
//                    other_images=new ArrayList<>();
//                }
//                dataSource.setUserOtherImages(other_images);
//
//                /*
//                 *Storing the search preference*/
//                ArrayList<SearchPreference> search_data=data.getSearchPreferences();
//                String search_details=utility.getGson().toJson(search_data);
//                dataSource.setSearchPreference(search_details);
//                /*
//                 *Storing the My preference*/
//                ArrayList<MyPrefrance> mySearch_data = data.getmyPreferences();
//                String mySearch_details=utility.getGson().toJson(mySearch_data);
//                dataSource.setMyPreference(mySearch_details);
//
//                /*
//                 *coin config data
//                 */
//                CoinData coinData = loginResponse.getCoinConfigData();
//                coinConfigWrapper.setCoinData(coinData);
//
//                /*
//                 *wallet
//                 */
//                Coins coins = loginResponse.getWalletCoins();
//                coinBalanceHolder.setCoinBalance(coins.getCoin().toString());
//
//                initialChatSetup(data);
//                dataSource.setLoggedIn(true);
//            }else
//            {
//                throw new EmptyData("CoinData is empty!");
//            }
//        }catch (Exception e)
//        {
//            throw new DataParsingException(e.getMessage());
//        }
//    }


    /**
     * Fetch the full address via lat and long.
     * @param locationHolder contain lat , long.
     * @return PassportLocation.
     */
    public PassportLocation getLocationName(LocationHolder locationHolder) {
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(locationHolder.getLatitude(),locationHolder.getLongitude(),1);
            if(addresses != null){
                String  subLocation = addresses.get(0).getSubLocality();
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String addressName = addresses.get(0).getFeatureName();

                boolean cityEmpty = TextUtils.isEmpty(city);
                boolean stateEmpty = TextUtils.isEmpty(state);
                boolean countryEmpty = TextUtils.isEmpty(country);
                String subAddress = ((cityEmpty)?"":city+((stateEmpty)?"":","))
                        +((stateEmpty)?"":state+((countryEmpty)?"":","))
                        +((countryEmpty)?"":country+".");
                PassportLocation currentLocation = new PassportLocation();
                currentLocation.setLocationName(addressName);
                currentLocation.setSubLocationName(subAddress);
                currentLocation.setLatitude(locationHolder.getLatitude());
                currentLocation.setLongitude(locationHolder.getLongitude());
                return currentLocation;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /*
     * Code from hola live on success of mob verification.
     */
    private void initialChatSetup(LoginData loginData){

        String profilePic = "", userName = "";

        CouchDbController db = AppController.getInstance().getDbController();

        Map<String, Object> map = new HashMap<>();
        if (!TextUtils.isEmpty(loginData.getProfilePic())) {
            profilePic = loginData.getProfilePic();
            map.put("userImageUrl", profilePic);

        } else {
            map.put("userImageUrl", "");
        }
        if (!TextUtils.isEmpty(loginData.getFirstName())) {
            userName = loginData.getFirstName();
            map.put("userName", userName);
        }

        map.put("userId", loginData.getId());

        /*
         * To save the social status as the text value in the properties
         *
         */
        map.put("userIdentifier", loginData.getId());
        map.put("apiToken", loginData.getToken());

        AppController.getInstance().getSharedPreferences().edit().putString("token", loginData.getToken()).apply();
        /*
         * By phone number verification
         */
        map.put("userLoginType", 1);
        if (!db.checkUserDocExists(AppController.getInstance().getIndexDocId(), loginData.getId())) {
            String userDocId = db.createUserInformationDocument(map);
            db.addToIndexDocument(AppController.getInstance().getIndexDocId(), loginData.getId(), userDocId);
        } else {
            db.updateUserDetails(db.getUserDocId(loginData.getId(), AppController.getInstance().getIndexDocId()), map);
        }
        if (!userName.isEmpty()) {
            db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(), loginData.getId(), 1, true);
        } else {
            db.updateIndexDocumentOnSignIn(AppController.getInstance().getIndexDocId(), loginData.getId(), 1, false);
        }

        /*
         * To update myself as available for call
         */
        AppController.getInstance().setSignedIn(false,true,loginData.getId() , userName, loginData.getId());
    }


    /*
     * Parsing data.*/
    public void parseSignUpResponse(String response) throws DataParsingException,EmptyData
    {
        try
        {
            LoginResponse  loginResponse=utility.getGson().fromJson(response,LoginResponse.class);
            LoginData data= loginResponse.getData();
            if(data!=null)
            {
                dataSource.setUserId(data.getId());
                dataSource.setName(data.getFirstName());
                String gender = data.getGender();
                if(gender != null) {
                        dataSource.setGender(gender);
                }
                dataSource.setProfilePicture(data.getProfilePic());
                dataSource.setToken(data.getToken());

                /*
                 *coin config data
                 */
                CoinData coinData = loginResponse.getCoinConfigData();
                coinConfigWrapper.setCoinData(coinData);

                /*
                 *wallet
                 */
                //Coins coins = loginResponse.getWalletCoins();
                coinBalanceHolder.setCoinBalance("0"/*coins.getCoin().toString()*/);

                dataSource.setLoggedIn(true);
                initialChatSetup(data);
            }else
            {
                throw new EmptyData("CoinData is empty!");
            }
        }catch (Exception e)
        {
            dataSource.setLoggedIn(false);
            throw new DataParsingException(e.getMessage());
        }
    }

}
