package com.pairzy.com.fbRegister.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @since  2/21/2018.
 */
public class ProfileReponsePojo
{
    @SerializedName("message")
    @Expose
    private String message;

    private ProfileData data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProfileData getData() {
        return data;
    }

    public void setData(ProfileData data) {
        this.data = data;
    }

}
