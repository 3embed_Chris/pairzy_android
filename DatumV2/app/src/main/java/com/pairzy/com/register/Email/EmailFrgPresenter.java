package com.pairzy.com.register.Email;

import android.app.Activity;
import android.text.TextUtils;
import com.pairzy.com.R;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.ProgressAleret.DatumProgressDialog;
import com.pairzy.com.util.Utility;
import javax.inject.Inject;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;
/**
 * <h2>EmailFrgPresenter</h2>
 * <P>
 *
 * </P>
 * @since  2/15/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class EmailFrgPresenter implements EmailFrgContract.Presenter
{
    @Inject
    NetworkStateHolder holder;
    @Inject
    Utility utility;
    @Inject
    DatumProgressDialog progressDialog;
    @Inject
    EmailModel emailModel;
    @Inject
    NetworkService service;
    @Inject
    Activity activity;

    private CompositeDisposable compositeDisposable;

    @Inject
    EmailFrgPresenter(){
        compositeDisposable=new CompositeDisposable();
    }

    private EmailFrgContract.View view;

    @Override
    public void takeView(Object view)
    {
        this.view= (EmailFrgContract.View) view;
    }

    @Override
    public void dropView()
    {
        this.view=null;
        compositeDisposable.clear();
    }


    @Override
    public boolean validateEmail(String mail)
    {
        return !TextUtils.isEmpty(mail) && emailModel.isValidEmail(mail);
    }

    @Override
    public void checkEmailIdExist(String email)
    {
        if(!holder.isConnected())
        {
            if(view!=null)
                view.showError(activity.getString(R.string.no_internet_error));
        }else
        {
            progressDialog.show();
            service.checkEmailAvailability(emailModel.getAuthorization(),
                    emailModel.getLanguage(),
                    emailModel.verifyParams(email))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d)
                        {
                            compositeDisposable.add(d);
                        }
                        @Override
                        public void onNext(Response<ResponseBody> result)
                        {
                            progressDialog.cancel();
                            try {
                                if(result.code()==200)
                                {
                                    if(view!=null)
                                        view.emailNotAvailable();
                                }else if(result.code()==412)
                                {
                                    if(view!=null)
                                        view.moveNextFragment(email);
                                }else
                                {
                                    if(view!=null)
                                        view.showError(emailModel.getError(result));
                                }
                            } catch (Exception e) {
                                if(view!=null)
                                    view.showError(e.getMessage());
                            }
                        }
                        @Override
                        public void onError(Throwable errorMsg)
                        {
                            progressDialog.cancel();
                            if(view!=null)
                            view.showError(errorMsg.getMessage());
                        }
                        @Override
                        public void onComplete()
                        {}
                    });
        }
    }
}
