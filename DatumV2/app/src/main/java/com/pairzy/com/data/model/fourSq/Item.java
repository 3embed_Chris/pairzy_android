package com.pairzy.com.data.model.fourSq;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h>Item pojo class</h>
 * @author 3Embed.
 * @since 9/5/18.
 * @version 1.0.
 */

public class Item {
    @SerializedName("venue")
    @Expose
    private Venue venue;

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }
}
