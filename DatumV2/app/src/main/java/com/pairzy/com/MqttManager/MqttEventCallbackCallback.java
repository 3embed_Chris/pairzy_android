package com.pairzy.com.MqttManager;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.json.JSONObject;

/**
 * @since  3/9/2018.
 */

public interface MqttEventCallbackCallback
{
    void onMQttConnected(boolean isReconnection);
    void onConnectionFailed(String error);
    void onMessageReceived(String topic, JSONObject response);
    void onDelivered(IMqttDeliveryToken token);
}
