package com.pairzy.com.chatMessageScreen.Model;

import com.pairzy.com.BaseModel;
import com.pairzy.com.MatchedView.MatchResponseData;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.coinBalance.CoinResponse;
import com.pairzy.com.data.model.coinBalance.Coins;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigResponse;
import com.pairzy.com.data.model.coinCoinfig.CoinConfigWrapper;
import com.pairzy.com.data.model.coinCoinfig.CoinData;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Matches.Chats.Model.ChatListData;
import com.pairzy.com.home.Matches.Chats.Model.MqttUnmatchResponse;
import com.pairzy.com.home.Discover.Model.superLike.SuperLikeResponse;
import com.pairzy.com.home.HomeModel.LikeResponse;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.Exception.DataParsingException;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.Exception.SimilarDataException;
import com.pairzy.com.util.Utility;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class ChatMessageModel  extends BaseModel {

    @Inject
    CoinBalanceHolder coinBalanceHolder;
    @Inject
    CoinConfigWrapper coinConfigWrapper;
    @Inject
    Utility utility;
    @Inject
    PreferenceTaskDataSource dataSource;

    @Inject
    public ChatMessageModel() {
    }

    public String getCoinBalance() {
        return coinBalanceHolder.getCoinBalance();
    }

    public CoinData getCoinConfigData() {
        return coinConfigWrapper.getCoinData();
    }

    public void parseCoinBalance(String response) {
        try{
            CoinResponse coinResponse = utility.getGson().fromJson(response,CoinResponse.class);
            Coins coins = coinResponse.getData().getCoins();
            coinBalanceHolder.setCoinBalance(String.valueOf(coins.getCoin()));
            coinBalanceHolder.setUpdated(true);
        }catch (Exception e){
        }
    }

    public void parseCoinConfig(String response)
    {
        try
        {
            CoinConfigResponse coinConfigResponse =
                    utility.getGson().fromJson(response,CoinConfigResponse.class);
            if(!coinConfigResponse.getData().isEmpty())
                coinConfigWrapper.setCoinData(coinConfigResponse.getData().get(0));
        }catch (Exception e){}
    }

    /*
     *Creating the form data for like service. */
    public Map<String, Object> getUserDetails(String user_id)
    {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.DoLikeService.USER_ID, user_id);
        return map;
    }

    public void parseSuperLike(String response)
    {
        try
        {
            SuperLikeResponse superLikeResponse = utility.getGson().fromJson(response, SuperLikeResponse.class);
            if(superLikeResponse.getCoinWallet() != null) {
                coinBalanceHolder.setCoinBalance(String.valueOf(superLikeResponse.getCoinWallet().getCoin()));
                coinBalanceHolder.setUpdated(true);
            }
        }catch (Exception e){}

    }

    public int getRemainsLikeCount() {
        try {
            String str = dataSource.getRemainsLikesCount();
            if(str.equalsIgnoreCase("unlimited"))
                return 1;
            return Integer.parseInt(str);
        }catch (Exception e){
            return 0;
        }
    }

    public void parseLikeResponse(String response) {
        try{
            LikeResponse likeResponse = utility.getGson().fromJson(response,LikeResponse.class);
            dataSource.setRemainsLinksCount(likeResponse.getRemainsLikesInString());
            dataSource.setNextLikeTime(likeResponse.getNextLikeTime());
        }catch (Exception e){

        }
    }

    public ChatListData parseMatchedMessage(JSONObject obj) throws Exception, SimilarDataException, EmptyData {
        try
        {
            MatchResponseData data=utility.getGson().fromJson(obj.toString(),MatchResponseData.class);
            if(data==null)
            {
                throw  new EmptyData("ChatListData is empty!");
            }
            ChatListData chatListData = new ChatListData();
            if(data.getFirstLikedBy().equals(dataSource.getUserId()))
            {
                chatListData.setFirstName(data.getSecondLikedByName());
                chatListData.setProfilePic(data.getSecondLikedByPhoto());
                chatListData.setRecipientId(data.getSecondLikedBy());
                chatListData.setSuperlikedMe(data.getIsSecondSuperLiked());
                chatListData.setChatId(data.getChatId());
            }else
            {
                chatListData.setFirstName(data.getFirstLikedByName());
                chatListData.setProfilePic(data.getFirstLikedByPhoto());
                chatListData.setRecipientId(data.getFirstLikedBy());
                chatListData.setSuperlikedMe(data.getIsFirstSuperLiked());
                chatListData.setChatId(data.getChatId());
            }
            return chatListData;
        }catch (Exception e)
        {
            throw new DataParsingException(e.getMessage());
        }
    }


    public String parseUserId(String response) {
        try{
            MqttUnmatchResponse mqttUnmatchResponse = utility.getGson().fromJson(response,MqttUnmatchResponse.class);
            return mqttUnmatchResponse.getTargetId();
        }catch (Exception e){

        }
        return "";
    }
}
