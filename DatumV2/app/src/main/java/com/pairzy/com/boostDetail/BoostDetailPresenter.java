package com.pairzy.com.boostDetail;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.addCoin.AddCoinActivity;
import com.pairzy.com.boostDetail.model.BoostDetailModel;
import com.pairzy.com.boostDetail.model.CoinPlan;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.CustomObserver.CoinBalanceObserver;
import com.pairzy.com.util.boostDialog.BoostAlertCallback;
import com.pairzy.com.util.boostDialog.BoostDialog;
import com.pairzy.com.util.boostDialog.Slide;
import com.pairzy.com.util.progressbar.LoadingProgress;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h>PassportPresenter class</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */

public class BoostDetailPresenter implements BoostDetailContract.Presenter ,BoostAlertCallback,ItemActionCallBack{

    @Inject
    BoostDetailContract.View view;
    @Inject
    ArrayList<Slide> slideArrayList;
    //@Inject
    //ArrayList<Offer> offerArrayList;
    @Inject
    BoostDialog boostDialog;
    @Inject
    NetworkService service;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Activity activity;
    @Inject
    BoostDetailModel model;
    @Inject
    LoadingProgress loadingProgress;
    @Inject
    CoinBalanceObserver coinBalanceObserver;

    private CompositeDisposable compositeDisposable;
    @Inject
    public BoostDetailPresenter(){
        compositeDisposable  =new CompositeDisposable();
    }


    @Override
    public void init() {
        if(view != null)
            view.applyFont();
    }

    @Override
    public void launchBoostDialog() {
        if(!model.isSubsPlanEmpty()) {
            model.selectMiddleItem();
            boostDialog.showAlert(this, model.getSubsPlanList(), slideArrayList);
        }
        else{
            if(view != null)
                view.showError("subscription List is Empty!!");
        }
    }

    @Override
    public void getCoinsPlan() {
        if(networkStateHolder.isConnected()) {
            if (view != null)
                view.showLoading();
            service.getCoinPlans(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                try {
                                    model.parseCoinPlanList(value.body().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                if (!model.isCoinPlanEmpty()) {
                                    if (view != null)
                                        view.showData();
                                } else {
                                    if (view != null)
                                        view.showEmptyData();
                                }
                                model.notifyCoinPlanAdapter();
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else {
                                if (view != null)
                                    view.showError(activity.getString(R.string.api_server_error));
                                if (view != null)
                                    view.showNetworkError(activity.getString(R.string.api_server_error));
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null)
                                view.showError(e.getMessage());
                            if (view != null)
                                view.showNetworkError(e.getMessage());
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
            if(view != null)
                view.showNetworkError(activity.getString(R.string.no_internet_error));
        }

    }

    @Override
    public void getSubsPlan() {
        if(view != null)
            view.enableSubsButton(false);

        if(networkStateHolder.isConnected()) {
            service.getSubsPlans(dataSource.getToken(), model.getLanguage())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                try {
                                    model.parseSubsPlanList(value.body().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                if (!model.isSubsPlanEmpty()) {
                                    if (view != null)
                                        view.enableSubsButton(true);
                                }
                                else{
                                    if(view != null){
                                        view.showError(activity.getString(R.string.subs_list_empty));
                                    }
                                }
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                if (!model.isSubsPlanEmpty())
                                    if (view != null)
                                        view.enableSubsButton(false);
                                try {
                                    if (view != null) {
                                        view.showError(value.errorBody().string());
                                    }
                                }catch (Exception ignored){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            if (view != null)
                                view.showError(activity.getString(R.string.failed_to_get_subs_plans));
                            if (view != null)
                                view.showNetworkError(e.getMessage());
                            if (!model.isSubsPlanEmpty())
                                if (view != null)
                                    view.enableSubsButton(false);
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
            if(view != null)
                view.showNetworkError(activity.getString(R.string.no_internet_error));
        }
    }

    @Override
    public void loadCoinPlanIfRequired() {
        if(model.isCoinPlanEmpty())
            getCoinsPlan();
        else{
            if(view != null)
                view.showData();
            model.notifyCoinPlanAdapter();
        }
    }

    @Override
    public void loadSubsPlanIfRequired() {
        if(model.isSubsPlanEmpty()){
            getSubsPlan();
        }
        else{
            if(view != null)
                view.enableSubsButton(true);
        }
    }

    private void callSubscriptionApi(String purchaseId){

        if(networkStateHolder.isConnected()) {
            loadingProgress.show();
            Map<String, Object> body = new HashMap<>();
            body.put(ApiConfig.SubsPlan.PLAN_ID,purchaseId);
            body.put(ApiConfig.SubsPlan.PAYMENT_GETWAY_TAX_ID,"1234");
            body.put(ApiConfig.SubsPlan.PAYMENT_GETWAY,"XYZ");
            body.put(ApiConfig.SubsPlan.USER_PURCHASE_TIME,String.valueOf(System.currentTimeMillis()));
            service.postSubscription(dataSource.getToken(), "en", body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(Response<ResponseBody> value) {
                            loadingProgress.cancel();
                            if(value.code() == 200){
                                if(view != null)
                                    view.showMessage(activity.getString(R.string.plan_purchase_successful));
                            }
                            else if(value.code() == 401){
                                AppController.getInstance().appLogout();
                            }
                            else{
                                try {
                                    if (view != null)
                                        view.showError(value.errorBody().string());
                                }catch (Exception ignored){}
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            loadingProgress.cancel();
                            if(view != null)
                                view.showError(e.getMessage());
                        }
                    });
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }

    }

    /*
     * boost dialog callback
     */
    @Override
    public void onInappSubscribe(int position) {
        String  purchaseId = model.getSubsPurchaseId(position);
        if(!TextUtils.isEmpty(purchaseId)){
            //TODO need to intigrate payment first then need to call
            callSubscriptionApi(purchaseId);
        }
        else{
            if(view != null)
                view.showError(activity.getString(R.string.purchase_id_is_null));
        }
    }

    @Override
    public void onNoThanks() {

    }

    @Override
    public void onClick(int id, int position) {
        switch (id){
            case R.id.coin_plan_rl:
                CoinPlan coinPlan = model.getCoinPlan(position);
                callCoinPurchaseApi(coinPlan);
                break;
        }
    }

    private void launchAddCoinScreen(CoinPlan coinPlan) {
        Intent intent = new Intent(activity, AddCoinActivity.class);
        intent.putExtra("coin_plan",coinPlan);
        if(view != null)
            view.launchAddCoinScreen(intent);
    }

    private void callCoinPurchaseApi(CoinPlan coinPlan) {
        if(coinPlan == null){
            if(view != null)
                view.showError(activity.getString(R.string.failed_to_get_plan));
            return;
        }
        if(view != null)
            loadingProgress.show();
        Map<String, Object> body = new HashMap<>();
        body.put(ApiConfig.CoinPlan.PLAN_ID,coinPlan.get_id());
        body.put(ApiConfig.CoinPlan.PAYMENT_GETWAY_TAX_ID,"1234");
        body.put(ApiConfig.CoinPlan.TRIGGER,"abc");
        body.put(ApiConfig.CoinPlan.PAYMENT_TYPE,"XYZ");
        body.put(ApiConfig.CoinPlan.PAYMENT_TAXN_ID,"0");
        body.put(ApiConfig.CoinPlan.USER_PURCHASE_TIME,"0");
        service.postCoinPlans(dataSource.getToken(),model.getLanguage(),body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Response<ResponseBody>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(Response<ResponseBody> value) {
                        if(view != null)
                            loadingProgress.cancel();
                        if(value.code() == 200){
                            if(view != null)
                                view.showMessage(activity.getString(R.string.coin_plan_successful));
                            try {
                                model.addToCoinBalance(coinPlan.getNoOfCoinUnlock().getCoin());
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            coinBalanceObserver.publishData(true);
                            if(view != null)
                                view.launchCoinWallet();
                        }
                        else if(value.code() == 401){
                            AppController.getInstance().appLogout();
                        }
                        else{
                            try{
                                if(view != null)
                                    view.showError(value.errorBody().string());
                            }catch (Exception e){}
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(view != null)
                            loadingProgress.cancel();
                        if(view != null)
                            view.showError(e.getMessage());
                    }
                });
    }
}
