package com.pairzy.com.MyProfile.editPreference.newEditInputFrag;

import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 * @since  2/26/2018.
 * @version 1.0.
 * @author 3Embed.
 */
@Module
public interface NewUserInputFrgBuilder
{
    @FragmentScoped
    @Binds
    NewUserInputFrg getConChoiceFragment(NewUserInputFrg userInputFrg);
    @FragmentScoped
    @Binds
    NewUserInputContract.Presenter taskPresenter(NewUserInputPresenter presenter);
}
