package com.pairzy.com.home.Prospects.MySuperlikes;
import com.pairzy.com.dagger.FragmentScoped;
import dagger.Binds;
import dagger.Module;
/**
 * @since  3/23/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface MySuperlikesBuilder
{
    @FragmentScoped
    @Binds
    MySuperlikesFrg getProspectItemFragment(MySuperlikesFrg mySuperlikesFrg);

    @FragmentScoped
    @Binds
    MySuperlikesContract.Presenter taskPresenter(MySuperlikesPresenter presenter);
}
