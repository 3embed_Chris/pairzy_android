package com.pairzy.com.util.datum_spinner;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
/**
 * <h2>DatumSpinner</h2>
 * @since  3/8/2018.
 */
public class DatumSpinner extends androidx.appcompat.widget.AppCompatSpinner
{
    public DatumSpinner(Context context)
    {
        super(context);
    }

    public DatumSpinner(Context context, int mode)
    {
        super(context, mode);
        initListener();
    }

    public DatumSpinner(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initListener();
    }

    public DatumSpinner(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        initListener();
    }

    public DatumSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode)
    {
        super(context, attrs, defStyleAttr, mode);
        initListener();
    }

    /*
    * intialization of the xml content.*/
    private void initListener()
    {
        setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                String selected_data=(String)adapterView.getAdapter().getItem(i);
                if (mListener != null) {
                    mListener.onItemSelected(selected_data);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {}
        });
    }

    /*
     *Spinner selection call back */
    public interface OnSpinnerEventsListener
    {
        void onSpinnerOpened(Spinner spinner);
        void onSpinnerClosed(Spinner spinner);
        void onItemSelected(String item);
    }

    private OnSpinnerEventsListener mListener;
    private boolean mOpenInitiated = false;

    @Override
    public boolean performClick()
    {
        mOpenInitiated = true;
        if (mListener != null) {
            mListener.onSpinnerOpened(this);
        }
        return super.performClick();
    }

    public void setSpinnerEventsListener(OnSpinnerEventsListener onSpinnerEventsListener)
    {
        mListener = onSpinnerEventsListener;
    }

    /**
     * Propagate the closed Spinner event to
     * the listener from outside if needed.
     */
    public void performClosedEvent() {
        mOpenInitiated = false;
        if (mListener != null) {
            mListener.onSpinnerClosed(this);
        }
    }

    /**
     * A boolean flag indicating that the Spinner triggered an open event.
     * @return true for opened Spinner
     */
    public boolean hasBeenOpened()
    {
        return mOpenInitiated;
    }

    public void onWindowFocusChanged (boolean hasFocus)
    {
        if (hasBeenOpened() && hasFocus) {
            performClosedEvent();
        }
    }

    public void  setSelectedInitial(int index)
    {
        try {
            this.setSelection(index,false);
        }catch (Exception e){}
    }

}