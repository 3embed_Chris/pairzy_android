package com.pairzy.com.boostLikes.Model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.util.DatumCallbacks.ListItemClick;
import com.pairzy.com.util.TypeFaceManager;
import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Created by ankit on 7/9/18.
 */

public class BoostLikeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public SimpleDraweeView thumbnail;
    public RelativeLayout dislike_view,boots_view,
            like_view,superlike_view;
    public TextView user_name;
    private ListItemClick listItemClick;
    public ImageView user_status;
    public RelativeLayout videoView, chatView;
    public ImageView ivSuperlikeMe;
    private TypeFaceManager typeFaceManager;

    public BoostLikeViewHolder(View view, TypeFaceManager typeFaceManager,ListItemClick callback)
    {
        super(view);
        this.listItemClick=callback;
        this.typeFaceManager = typeFaceManager;

        thumbnail = view.findViewById(R.id.thumbnail);
        dislike_view=view.findViewById(R.id.dislike_view);
        dislike_view.setOnClickListener(this);
        boots_view=view.findViewById(R.id.boots_view);
        boots_view.setOnClickListener(this);
        like_view=view.findViewById(R.id.like_view);
        like_view.setOnClickListener(this);
        superlike_view=view.findViewById(R.id.superlike_view);
        superlike_view.setOnClickListener(this);
        user_name=view.findViewById(R.id.user_name);
        view.findViewById(R.id.card_view).setOnClickListener(this);
        user_status = view.findViewById(R.id.user_status);
        videoView = view.findViewById(R.id.video_view);
        videoView.setOnClickListener(this);
        chatView = view.findViewById(R.id.chat_view);
        chatView.setOnClickListener(this);
        ivSuperlikeMe = view.findViewById(R.id.iv_superliked_me);
    }

    @Override
    public void onClick(View view)
    {
        if(listItemClick!=null)
            listItemClick.onClicked(view.getId(),this.getAdapterPosition());
    }

}
