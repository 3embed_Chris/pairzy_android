package com.pairzy.com.editProfile.Model;

import android.view.View;

public interface PicAdapterClickCallback {
    void onPicRemove(int pos);
    void onPicPreview(String picUrl, View v);
    void onEmptyPicClick(int pos);
    void onSetAsProfilePic(int pos);
}