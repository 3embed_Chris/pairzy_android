package com.pairzy.com.data.model.coinBalance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h>Coins</h>
 * @author 3Embed.
 * @since 4/6/18.
 */

public class Coins {

    @SerializedName("Coin")
    @Expose
    private Integer coin;

    public Integer getCoin() {
        return coin;
    }

    public void setCoin(Integer coin) {
        this.coin = coin;
    }
}
