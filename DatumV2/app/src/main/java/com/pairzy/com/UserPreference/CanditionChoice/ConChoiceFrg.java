package com.pairzy.com.UserPreference.CanditionChoice;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.widget.AppCompatRadioButton;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.UserPreference.AnimatorHandler;
import com.pairzy.com.UserPreference.Model.PreferenceItem;
import com.pairzy.com.UserPreference.MyPreferencePageContract;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * <h2>ConChoiceFrg</h2>
 * A simple {@link Fragment} subclass.
 */
@ActivityScoped
public class ConChoiceFrg extends DaggerFragment implements ConChoiceContract.View
{
    public static final String ITEM_POSITION="pref_item";
    @Inject
    Utility utility;
    @Inject
    Activity activity;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    MyPreferencePageContract.Presenter mainpresenter;
    @Inject
    AnimatorHandler animatorHandler;
    @Inject
    ConChoiceContract.Presenter presenter;
    private Unbinder unbinder;
    @BindView(R.id.skip_page)
    TextView skip_page;
    @BindView(R.id.first_title)
    TextView first_title;
    @BindView(R.id.second_title)
    TextView second_title;
    @BindView(R.id.yes_text)
    TextView yes_text;
    @BindView(R.id.yes_tick)
    ImageView yes_tick;
    @BindView(R.id.no_text)
    TextView no_text;
    @BindView(R.id.no_tick)
    ImageView no_tick;
    @BindView(R.id.sometime_text)
    TextView sometime_text;
    @BindView(R.id.sometime_tick)
    ImageView sometime_tick;
    @BindView(R.id.not_to_say)
    AppCompatRadioButton not_to_say;
    @BindView(R.id.btnNext)
    FloatingActionButton btnNext;
    @BindView(R.id.back_button_img)
    ImageView back_button_img;
    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;
    private int item_position=-1;
    private int list_number;
    private PreferenceItem current_item;
    private TextView last_Selected_Text=null;
    private ImageView last_selected_img=null;
    private boolean IsPereferedNotSay;


    @Inject
    public ConChoiceFrg() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle bundle=getArguments();
        assert bundle != null;
        item_position=bundle.getInt(ITEM_POSITION);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_con_choice_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder=ButterKnife.bind(this,view);
        upDateUI();
        collectData();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        presenter.takeView(this);
        setSelection(yes_text,yes_tick);
    }


    public void setItemData(PreferenceItem current_item)
    {
     this.current_item=current_item;
     list_number=current_item.getList_no();
    }
    /*
     *Setting the initial selection*/
    private void setSelection(TextView textView, ImageView imageView)
    {
        utility.closeSpotInputKey(activity,first_title);
        if(last_Selected_Text==null||last_selected_img==null)
        {
            last_Selected_Text=textView;
            last_selected_img=imageView;
            scaleUp(last_Selected_Text,last_selected_img);
        }else
        {
            switchScale(last_Selected_Text,last_selected_img,textView,imageView);
            last_Selected_Text=textView;
            last_selected_img=imageView;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
    }

    /*
      * Updating the required ui updating like fonts etc.*/
    private void upDateUI()
    {
        if(item_position==0)
        {
            back_button_img.setImageDrawable(utility.getVectorDrawable(R.drawable.ic_cross_svg));
        }
        btnNext.setEnabled(false);
        skip_page.setTypeface(typeFaceManager.getCircularAirBook());
        first_title.setTypeface(typeFaceManager.getCircularAirBold());
        second_title.setTypeface(typeFaceManager.getCircularAirBold());
        yes_text.setTypeface(typeFaceManager.getCircularAirBook());
        no_text.setTypeface(typeFaceManager.getCircularAirBook());
        sometime_text.setTypeface(typeFaceManager.getCircularAirBook());
        not_to_say.setTypeface(typeFaceManager.getCircularAirBook());
    }

    /*
     *collecting the details. */
    private void collectData()
    {
       // current_item=mainpresenter.getPreferenceItem(item_position);
        String label=current_item.getLabel();
        first_title.setText(utility.formatString(current_item.getTitle()));
        second_title.setText(utility.formatString(current_item.getLabel()));
    }

    @OnClick(R.id.close_button)
    void onBackClicked()
    {
        activity.onBackPressed();
    }

    @OnClick(R.id.parent_view)
    void onParentClicked(){}
    @OnClick(R.id.skip_page)
    void onSkip()
    {
        mainpresenter.openNextFrag(list_number,item_position+1);
    }

    @OnClick(R.id.yes_selection)
    void onYesClick()
    {
        setSelection(yes_text,yes_tick);
    }

    @OnClick(R.id.no_selection)
    void onNoClick()
    {
        setSelection(no_text,no_tick);
    }

    @OnClick(R.id.sometime_selection)
    void someThingElse()
    {
        setSelection(sometime_text,sometime_tick);
    }

    @OnClick(R.id.not_to_say)
    void onRadioButtonChecked(RadioButton radioButton)
    {
        if(radioButton.isSelected())
        {
            radioButton.setSelected(false);
            radioButton.setChecked(false);
        }else
        {
            radioButton.setSelected(true);
            radioButton.setChecked(true);
        }
    }

    @Override
    public void showError(String error)
    {
        mainpresenter.showError(error);
    }

    @Override
    public void showMessage(String message)
    {
        mainpresenter.showMessage(message);
    }

    /*
    * Switching the animation*/
    private void switchScale(TextView fist,ImageView firstImg,TextView second,ImageView secImg)
    {
        fist.setTextColor(ContextCompat.getColor(activity,R.color.softLightGray));
        Animation animation=animatorHandler.getItemScaleDown();
        firstImg.setVisibility(View.INVISIBLE);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                scaleUp(second,secImg);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        firstImg.setAnimation(animation);
        animation.start();
    }

    private void scaleUp(TextView textView,ImageView imageView)
    {
        textView.setTextColor(ContextCompat.getColor(activity,R.color.black));
        imageView.setVisibility(View.VISIBLE);
        Animation animation=animatorHandler.getItemScaleUp();
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}
            @Override
            public void onAnimationEnd(Animation animation)
            {
                handelNextButton(true);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        imageView.setAnimation(animation);
        animation.start();
    }

    /*
     *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            btnNext.setBackgroundTintList(getResources().getColorStateList(R.color.datum));
            btnNext.setEnabled(true);
        }
        else {
            btnNext.setBackgroundTintList(getResources().getColorStateList(R.color.dark_gray));
            btnNext.setEnabled(false);
        }
    }
}
