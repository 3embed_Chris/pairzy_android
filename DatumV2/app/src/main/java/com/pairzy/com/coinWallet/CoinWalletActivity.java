package com.pairzy.com.coinWallet;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.coinWallet.Model.CoinHistoryPagerAdapter;
import com.pairzy.com.coinWallet.allCoin.AllCoinFrag;
import com.pairzy.com.coinWallet.coinIn.InCoinFrag;
import com.pairzy.com.coinWallet.coinOut.OutCoinFrag;
import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h>CoinWalletActivity activity</h>
 * @author 3Embed.
 * @since 23/4/18.
 * @version 1.0.
 */
public class CoinWalletActivity extends BaseDaggerActivity implements CoinWalletContract.View{

    @Inject
    CoinWalletPresenter presenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    CoinHistoryPagerAdapter viewPagerAdapter;
    @Inject
    ArrayList<Fragment> fragmentList;
    @Inject
    Activity activity;
    @Inject
    CoinBalanceHolder coinBalanceHolder;

    @BindView(R.id.parent_layout)
    CoordinatorLayout parentLayout;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolbarTitle;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    private Unbinder unbinder;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coin_wallet);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        handler = new Handler();
        unbinder = ButterKnife.bind(this);
        presenter.init();
        initView();
        presenter.observeCoinBalanceChange();
    }

    @Override
    public void onResume() {
        super.onResume();
        //update history as well
        if(presenter.isCoinBalanceUpdateRequired()){
            presenter.getCoinHistory();
        }
    }

    private void initView() {
        fragmentList.add(new AllCoinFrag());
        fragmentList.add(new InCoinFrag());
        fragmentList.add(new OutCoinFrag());
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabCount = viewGroup.getChildCount();
        for(int i =0 ; i< tabCount; i++){
            ViewGroup vgTab = (ViewGroup) viewGroup.getChildAt(i);
            int tabChildsCount = vgTab.getChildCount();
            for(int j = 0; j < tabChildsCount;j++){
                View tabText = vgTab.getChildAt(j);
                if(tabText instanceof TextView){
                    ((TextView) tabText).setTypeface(typeFaceManager.getCircularAirBold());
                    ((TextView) tabText).setAllCaps(false);
                }
            }
        }
    }

    @Override
    public void applyFont() {
        tvToolbarTitle.setTypeface(typeFaceManager.getCircularAirBold());
    }


    @Override
    public void showError(String errorMsg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+errorMsg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(String msg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+msg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(int msgId) {
        String message=getString(msgId);
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }



    @Override
    public void showLoading() {
        for(Fragment fragment: fragmentList){
            if(fragment instanceof AllCoinFrag){
                ((AllCoinFrag)fragment).showLoading();
            }
            else if(fragment instanceof InCoinFrag){
                ((InCoinFrag)fragment).showLoading();
            }
            else if(fragment instanceof OutCoinFrag){
                ((OutCoinFrag)fragment).showLoading();
            }
        }
    }

    @Override
    public void showNetworkError(String error) {
        for(Fragment fragment: fragmentList){
            if(fragment instanceof AllCoinFrag){
                ((AllCoinFrag)fragment).showNetworkError(error);
            }
            else if(fragment instanceof InCoinFrag){
                ((InCoinFrag)fragment).showNetworkError(error);
            }
            else if(fragment instanceof OutCoinFrag){
                ((OutCoinFrag)fragment).showNetworkError(error);
            }
        }
    }

    @Override
    public void notifyAdapter() {
        for(Fragment fragment: fragmentList){
            if(fragment instanceof AllCoinFrag){
                ((AllCoinFrag)fragment).notifyAdapter();
            }
            else if(fragment instanceof InCoinFrag){
                ((InCoinFrag)fragment).notifyAdapter();
            }
            else if(fragment instanceof OutCoinFrag){
                ((OutCoinFrag)fragment).notifyAdapter();
            }
        }
    }


    @OnClick(R.id.back_button)
    public void close(){
        onBackPressed();
    }


    @Override
    public void onBackPressed() {
        this.finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.dispose();
        unbinder.unbind();
    }

}
