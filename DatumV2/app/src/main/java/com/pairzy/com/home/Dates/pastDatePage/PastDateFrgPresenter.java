package com.pairzy.com.home.Dates.pastDatePage;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.home.Dates.Model.PastDateListPojo;
import com.pairzy.com.home.Dates.pastDatePage.Model.PastDateAdapter;
import com.pairzy.com.home.Dates.pastDatePage.Model.PastDateModel;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.RatingDialog.RatingAlertCallback;
import com.pairzy.com.util.RatingDialog.RatingDialog;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * <h2>PastDateFrgPresenter</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class PastDateFrgPresenter implements PastDateFrgContract.Presenter ,ItemActionCallBack , RatingAlertCallback
{
    private CompositeDisposable compositeDisposable;
    private PastDateFrgContract.View view;
    private PastDateListPojo pastDateItem;
    private int position = -1;

    @Named(PastDatePageUtil.PAST_DATE_LAYOUT_MANAGER)
    @Inject
    LinearLayoutManager linearLayoutManager;
    @Inject
    NetworkService service;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    PastDateModel model;
    @Inject
    RatingDialog ratingDialog;
    @Inject
    NetworkStateHolder networkStateHolder;
    @Inject
    Activity activity;

    @Inject
    PastDateFrgPresenter()
    {
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void takeView(Object view)
    {
        this.view= (PastDateFrgContract.View) view;
    }

    @Override
    public void dropView() {
        view=null;
        compositeDisposable.clear();
    }

    @Override
    public void notifyPastdateAdapter() {
        if(model.getListSize() == 0){
            if(view != null)
                view.refreshList();
        }
        else{
            if(view != null)
                view.notifyAdapter();
        }
    }

    @Override
    public boolean checkDateExist() {
        return model.checkDateExist();
    }

    @Override
    public void setAdapterCallBack(PastDateAdapter pastDateAdapter) {
        pastDateAdapter.setCallBack(this);
    }

    @Override
    public void doLoadMore(int positionView) {
        if(model.loadMoreRequired(positionView))
        {
            if(view!=null)
                view.doLoadMore();
        }
    }

    @Override
    public void onClick(int id, int position) {
        pastDateItem = model.getDateItem(position);
        this.position = position;
        switch (id){
            case R.id.rate_the_date_rl:
                if(pastDateItem != null)
                    showRatingDialog(pastDateItem);
                break;
            default:
                PastDateListPojo pastDateListPojo = model.getDateItem(position);
                if(pastDateListPojo != null) {
                    if (view != null)
                        view.launchUserProfile(pastDateListPojo);
                }
        }
    }

    private void showRatingDialog(PastDateListPojo pastDateListPojo) {
        ratingDialog.showAlert(pastDateListPojo.getOpponentProfilePic(),
                pastDateListPojo.getOpponentName(),
                pastDateListPojo.getRequestedFor(),this);
    }

    public void callRatingApi(PastDateListPojo pastDateItem){

        if(networkStateHolder.isConnected()) {
            Map<String, Object> body = new HashMap<>();
            body.put(ApiConfig.PostDateRating.DATE_ID, pastDateItem.getDataId());
            body.put(ApiConfig.PostDateRating.RATE, pastDateItem.getRating());
            service.postDateRating(dataSource.getToken(), "en", body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ResponseBody>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onNext(Response<ResponseBody> value) {
                            if (value.code() == 200) {
                                //successful
                            } else if (value.code() == 401) {
                                AppController.getInstance().appLogout();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            if (view != null)
                                view.showError(e.getMessage());
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else{
            if(view != null)
                view.showError(activity.getString(R.string.no_internet_error));
        }

    }

    @Override
    public void onRate(float rate) {
        if(pastDateItem != null && position != -1){
            pastDateItem.setRating((int)rate);
            if(model.raplaceDateItem(pastDateItem,position)) {
                if(view != null)
                    view.notifyAdapter();
                callRatingApi(pastDateItem);
            }
        }
    }
}
