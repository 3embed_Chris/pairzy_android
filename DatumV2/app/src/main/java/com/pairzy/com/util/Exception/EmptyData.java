package com.pairzy.com.util.Exception;

/**
 * <h2>EmptyData</h2>
 * @since  4/3/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class EmptyData extends Exception
{
    public EmptyData() {
        super();
    }

    public EmptyData(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
