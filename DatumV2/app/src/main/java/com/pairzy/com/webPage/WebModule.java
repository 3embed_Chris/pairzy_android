package com.pairzy.com.webPage;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class WebModule {

    @ActivityScoped
    @Binds
    abstract WebContract.View bindsView(WebActivity activity);

    @ActivityScoped
    @Binds
    abstract Activity bindsActivity(WebActivity activity);

    @ActivityScoped
    @Binds
    abstract WebContract.Presenter bindsPresenter(WebPresenter presenter);
}
