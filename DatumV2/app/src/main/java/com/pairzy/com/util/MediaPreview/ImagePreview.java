package com.pairzy.com.util.MediaPreview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.net.Uri;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import com.pairzy.com.R;
import com.facebook.drawee.view.SimpleDraweeView;
import java.io.File;


public class ImagePreview implements GestureDetector.OnGestureListener
{
    private Activity activity;
    private Dialog dialog;
    private GestureDetector gestureScanner;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;


    public ImagePreview(Activity activity)
    {
        this.activity=activity;
        dialog=new Dialog(activity,R.style.PreviewTheme);
        gestureScanner = new GestureDetector(activity,this);
    }


    public void showDialog(String image_path)
    {

        if(dialog.isShowing())
        {
            dialog.cancel();
        }
        @SuppressLint("InflateParams")
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.image_preview_page, null);
        SimpleDraweeView preview_image=dialogView.findViewById(R.id.preview_image);
        preview_image.setOnTouchListener((v, event) -> gestureScanner.onTouchEvent(event));
        Uri uri;
        if(image_path.contains("http://")||image_path.contains("https://"))
        {
            uri=Uri.parse(image_path);
        }else
        {
            uri=Uri.fromFile(new File(image_path));
        }
        preview_image.setImageURI(uri);
        dialog.setContentView(dialogView);
        dialog.show();
    }


    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }
    @Override
    public void onShowPress(MotionEvent e) {}
    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }
    @Override
    public void onLongPress(MotionEvent e) {}
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    {
        Log.d("ds1","In");
        try {
            if (Math.abs(e1.getX() - e2.getX()) > SWIPE_MAX_OFF_PATH){
                return false;
            }
            // right to left swipe
            if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY)
            {
                Log.d("ds1","down");
            }
            // left to right swipe
            else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                Log.d("ds1","Up");
            }
        } catch (Exception e) {

        }
        return false;
    }
}
