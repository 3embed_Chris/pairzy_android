package com.pairzy.com.home.Dates.upcomingPage;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.home.Dates.DatesFragUtil;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Dates.upcomingPage.Model.UpcomingAdapter;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
/**
 *<h2>PostFeedFragUtil</h2>
 * <P>
 *     User available util class to provide
 *     required data to the util page.
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public class UpcomingUtil
{
    public static final String UPCOMING_LAYOUT_MANAGER = "upcoming_layout_manager";

    @Provides
    @FragmentScoped
    UpcomingAdapter getListAdapter(Utility utility, @Named(DatesFragUtil.UPCOMING_LIST)ArrayList<DateListPojo> list_data, TypeFaceManager typeFaceManager)
    {
        return new UpcomingAdapter(utility,list_data,typeFaceManager);
    }

    @Named(UpcomingUtil.UPCOMING_LAYOUT_MANAGER)
    @Provides
    @FragmentScoped
    LinearLayoutManager provideLinearLayoutManager(Activity activity)
    {
        return  new LinearLayoutManager(activity);
    }

}
