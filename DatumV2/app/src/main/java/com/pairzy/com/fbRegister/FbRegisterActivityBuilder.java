package com.pairzy.com.fbRegister;

import android.app.Activity;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;

import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.fbRegister.Email.FbEmailFragment;
import com.pairzy.com.fbRegister.Email.FbEmailFrgBuilder;
import com.pairzy.com.fbRegister.Gender.FbGenderFragment;
import com.pairzy.com.fbRegister.Gender.FbGenderFrgBuilder;
import com.pairzy.com.fbRegister.Name.FbNameFragBuilder;
import com.pairzy.com.fbRegister.Name.FbNameFragment;
import com.pairzy.com.fbRegister.ProfilePic.FbProfilePicBuilder;
import com.pairzy.com.fbRegister.ProfilePic.FbProfilePicFrg;
import com.pairzy.com.fbRegister.ProfileVideo.FbProfileVideoBuilder;
import com.pairzy.com.fbRegister.ProfileVideo.FbProfileVideoFrg;
import com.pairzy.com.fbRegister.Userdob.FbDobFragment;
import com.pairzy.com.fbRegister.Userdob.FbDobFrgBuilder;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;

/**
 * <h1>FbRegisterActivityBuilder</h1>
 * <p>
 *
 * </p>
 * @author 3Embed
 * @since 13/08/17
 * @version 1.0.
 */
@Module
public abstract class FbRegisterActivityBuilder
{
 static final String ACTIVITY_FRAGMENT_MANAGER = "Register.FragmentManager";

 @ActivityScoped
 @Binds
 abstract Activity provideActivity(FbRegisterPage register_page);

 @ActivityScoped
 @Binds
 abstract FbRegisterContact.View provideView(FbRegisterPage register_page);

 @ActivityScoped
 @Binds
 abstract FbRegisterContact.Presenter taskPresenter(FbRegisterPagePresenter presenter);

 @Provides
 @Named(ACTIVITY_FRAGMENT_MANAGER)
 @ActivityScoped
 static FragmentManager activityFragmentManager(Activity activity)
 {
  return ((AppCompatActivity)activity).getSupportFragmentManager();
 }

 @FragmentScoped
 @ContributesAndroidInjector(modules = {FbEmailFrgBuilder.class})
 abstract FbEmailFragment getFbEmailFragment();

 @FragmentScoped
 @ContributesAndroidInjector(modules ={FbNameFragBuilder.class})
 abstract FbNameFragment getFbNameFragment();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {FbDobFrgBuilder.class})
 abstract FbDobFragment getFbDobFragment();

 @FragmentScoped
 @ContributesAndroidInjector(modules = {FbGenderFrgBuilder.class})
 abstract FbGenderFragment getFbGenderFragment();


 @FragmentScoped
 @ContributesAndroidInjector(modules = {FbProfilePicBuilder.class})
 abstract FbProfilePicFrg getFbProfilePicFragment();


 @FragmentScoped
 @ContributesAndroidInjector(modules = {FbProfileVideoBuilder.class})
 abstract FbProfileVideoFrg getFbProfileVideoFragment();


}
