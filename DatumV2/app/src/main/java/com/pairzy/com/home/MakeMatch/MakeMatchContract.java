package com.pairzy.com.home.MakeMatch;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

public interface MakeMatchContract {
    interface View extends BaseView {

        void showCoinBalance(String Balance);
        void notifyRvAdapter(int position);

    }

    interface Presenter extends BasePresenter {

        void getUserLocation(int OFFSET, int LIMIT);
        void fetchDataFromApi(int offSet, int limit);
//        void loadSavedLocations(int OFFSET, int LIMIT);
//        void isPassportLoc();
    }
}
