package com.pairzy.com.home.Dates.Pending_page.Model;

import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.pairzy.com.R;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.home.Dates.Model.LoadMoreDateViewHolder;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import java.util.ArrayList;
import java.util.Locale;

/**
 * <h2>PastDateAdapter</h2>
 * <P>
 *
 * </P>
 * @since  3/19/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class PendingListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private static final int ONLINE = 1;
    private final int CONFIRM_ITEM = 0, RESCHEDULE_ITEM = 1 , LOADING_ITEM = 2;
    private ArrayList<DateListPojo> list;
    private TypeFaceManager typeFaceManager;
    private ItemActionCallBack callBack;
    private Utility utility;

    public void setCallBack(ItemActionCallBack callBack) {
        this.callBack = callBack;
    }

    public PendingListAdapter(Utility utility,ArrayList<DateListPojo> list, TypeFaceManager typeFaceManager)
    {
        this.utility = utility;
        this.list = list;
        this.typeFaceManager = typeFaceManager;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View view;
        switch (viewType){
            case CONFIRM_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pending_confirm_item,parent,false);
                return new PendingConfirmViewHolder(view, callBack, typeFaceManager);
            case RESCHEDULE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pending_reschedule_item,parent,false);
                return new PendingRescheduleViewHolder(view, callBack, typeFaceManager);
            case LOADING_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dates_loading_item,parent,false);
                return new LoadMoreDateViewHolder(view,callBack, typeFaceManager);
                default:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pending_reschedule_item,parent,false);
                    return new PendingRescheduleViewHolder(view, callBack, typeFaceManager);
        }
    }

    @Override
    public int getItemViewType(int position)
    {
        DateListPojo dateListPojo = list.get(position);
        if(dateListPojo.isLoading())
        {
            return LOADING_ITEM;
        }else if(dateListPojo.getIsInitiatedByMe()==0)
        {
            return CONFIRM_ITEM;
        }else
        {
            return RESCHEDULE_ITEM;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        switch (holder.getItemViewType())
        {
            case LOADING_ITEM:
                try
                {
                    loadingView((LoadMoreDateViewHolder) holder);
                }catch (Exception e){}
                break;
            case CONFIRM_ITEM:
                try {
                    handelConfirmView((PendingConfirmViewHolder)holder);
                }catch (Exception e){}
                break;
            case RESCHEDULE_ITEM:
                try
                {
                    handelResheduleView((PendingRescheduleViewHolder)holder);
                }catch (Exception e){}
                break;

                default:
                    try {
                        handelConfirmView((PendingConfirmViewHolder)holder);
                    }catch (Exception e){}
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    /*
     *Handling the loading view */
    private void loadingView(LoadMoreDateViewHolder holder)
    {
        int position=holder.getAdapterPosition();
        DateListPojo data=  list.get(position);
        if(data.isLoadingFailed())
        {
            holder.setFailed();
        }else
        {
            holder.showLoadingLoadingAgain();
        }
    }




    private void handelConfirmView(PendingConfirmViewHolder holder2)
    {
        int position=holder2.getAdapterPosition();
        DateListPojo dateListPojo=  list.get(position);
        if(dateListPojo==null)
            return;
        holder2.user_profile_pic.setImageURI(Uri.parse(dateListPojo.getOpponentProfilePic()));
        holder2.main_image_view.setImageURI(Uri.parse(dateListPojo.getOpponentProfilePic()));
        holder2.name.setText(utility.formatString(dateListPojo.getOpponentName()));
        if(dateListPojo.isReschedule()){
            holder2.user_message.setText(String.format(Locale.ENGLISH, "Reschedule %s request received from %s",
                    utility.formatString(dateListPojo.getRequestedFor()),
                    utility.formatString(dateListPojo.getOpponentName())));
        }
        else {
            holder2.user_message.setText(String.format(Locale.ENGLISH, "%s request received from %s",
                    utility.formatString(dateListPojo.getRequestedFor()),
                    utility.formatString(dateListPojo.getOpponentName())));
        }

        if(dateListPojo.getOnlineStatus() == ONLINE){
            holder2.user_status_dot.setImageResource(R.drawable.online_dot);
        }
        else{
            holder2.user_status_dot.setImageResource(0);
        }

        if(!TextUtils.isEmpty(dateListPojo.getPlaceName())) {
            holder2.locationTv.setVisibility(View.VISIBLE);
            holder2.locationTv.setText(dateListPojo.getPlaceName());
        }
        else{
            holder2.locationTv.setVisibility(View.GONE);
        }
        if(dateListPojo.getProposedOn() != null) {
            holder2.dateTimeTv.setVisibility(View.VISIBLE);
            holder2.dateTimeTv.setText(utility.getFormatDateTime(dateListPojo.getProposedOn()));
        }
        else{
            holder2.dateTimeTv.setVisibility(View.GONE);
        }
        holder2.tvTimeText.setText(utility.time_converter(dateListPojo.getCreatedTimestamp()>0?dateListPojo.getCreatedTimestamp():0));
    }


    private void handelResheduleView(PendingRescheduleViewHolder holder1)
    {
        int position=holder1.getAdapterPosition();
        DateListPojo dateListPojo=  list.get(position);
        if(dateListPojo==null)
            return;
        holder1.user_profile_pic.setImageURI(Uri.parse(dateListPojo.getOpponentProfilePic()));
        holder1.main_image_view.setImageURI(Uri.parse(dateListPojo.getOpponentProfilePic()));
        holder1.name.setText(utility.formatString(dateListPojo.getOpponentName()));
        holder1.user_message.setText(String.format(Locale.ENGLISH,"%s request send to %s",
                utility.formatString(dateListPojo.getRequestedFor()),
                utility.formatString(dateListPojo.getOpponentName())));

        if(dateListPojo.getOnlineStatus() == ONLINE){
            holder1.user_status_dot.setImageResource(R.drawable.online_dot);
        }
        else{
            holder1.user_status_dot.setImageResource(0);
        }

        if(!TextUtils.isEmpty(dateListPojo.getPlaceName())) {
            holder1.locationTv.setVisibility(View.VISIBLE);
            holder1.locationTv.setText(dateListPojo.getPlaceName());
        }
        else{
            holder1.locationTv.setVisibility(View.GONE);
        }
        if(dateListPojo.getProposedOn() != null) {
            holder1.dateTimeTv.setVisibility(View.VISIBLE);
            holder1.dateTimeTv.setText(utility.getFormatDateTime(dateListPojo.getProposedOn()));
        }
        else{
            holder1.dateTimeTv.setVisibility(View.GONE);
        }
        holder1.tvTimeText.setText(utility.time_converter(dateListPojo.getCreatedTimestamp()>0?dateListPojo.getCreatedTimestamp():0));
    }

}
