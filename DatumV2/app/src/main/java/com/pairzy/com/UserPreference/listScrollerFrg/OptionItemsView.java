package com.pairzy.com.UserPreference.listScrollerFrg;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
/**
 * @since  2/22/2018.
 */
class OptionItemsView extends RecyclerView.ViewHolder
{
    TextView option_text;
    ImageView option_tick;

    OptionItemsView(View itemView, TypeFaceManager typeFaceManager)
    {
        super(itemView);
        option_text=itemView.findViewById(R.id.option_text);
        option_tick=itemView.findViewById(R.id.option_tick);
        option_text.setTypeface(typeFaceManager.getCircularAirBook());

    }
}
