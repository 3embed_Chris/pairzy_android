package com.pairzy.com.util.RatingDialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatRatingBar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RatingBar;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
import com.facebook.drawee.view.SimpleDraweeView;

/**
 * <h2>RatingDialog class</h2>
 * @since  3/5/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class RatingDialog
{
    private RatingAlertCallback callback;
    private Activity activity;
    private Dialog  alert = null;
    private TypeFaceManager typeFaceManager;
    private TextView message;


    public RatingDialog(Activity activity, TypeFaceManager typeFaceManager)
    {
        this.typeFaceManager=typeFaceManager;
        this.activity=activity;
    }

    /*
     * Showing the alert.*/
    public void showAlert(String profilePic, String userName, String dateType, RatingAlertCallback callback1)
    {
        this.callback=callback1;
        if(alert!=null&&alert.isShowing())
        {
            alert.cancel();
        }

        AlertDialog.Builder builder=new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);
        LayoutInflater inflater =activity.getLayoutInflater();
        @SuppressLint("InflateParams")
        View alertLayout = inflater.inflate(R.layout.dialog_rating, null);
        builder.setView(alertLayout);

        SimpleDraweeView userImage = alertLayout.findViewById(R.id.user_profile_pic);
        userImage.setImageURI(profilePic);
        TextView tvUserName = alertLayout.findViewById(R.id.tv_user_name);
        tvUserName.setText(userName);
        TextView tvDateType = alertLayout.findViewById(R.id.tv_date_type);
        tvDateType.setText(dateType);
        AppCompatRatingBar ratingBar = alertLayout.findViewById(R.id.rating_bar);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if(callback != null)
                    callback.onRate(rating>5?5:rating);
                if(alert != null && alert.isShowing()){
                    alert.cancel();
                }

            }
        });
        alert=builder.create();
        Window window = alert.getWindow();
        assert window != null;
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        alert.show();
    }

}
