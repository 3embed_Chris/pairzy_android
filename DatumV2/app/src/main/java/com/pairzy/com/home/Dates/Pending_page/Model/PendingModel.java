package com.pairzy.com.home.Dates.Pending_page.Model;

import com.pairzy.com.data.model.CoinBalanceHolder;
import com.pairzy.com.data.model.DateEvent;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.DatesFragUtil;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.home.Discover.Model.superLike.SuperLikeResponse;
import com.pairzy.com.home.HomeModel.LoadMoreStatus;
import com.pairzy.com.home.HomeUtil;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by ankit on 15/5/18.
 */

public class PendingModel {

    @Named(DatesFragUtil.PENDING_LIST)
    @Inject
    ArrayList<DateListPojo> pendingList;
    @Named(HomeUtil.LOAD_MORE_DATE_STATUS)
    @Inject
    LoadMoreStatus loadMoreStatus;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Utility utility;
    @Inject
    CoinBalanceHolder coinBalanceHolder;

    @Inject
    PendingModel(){
    }

    public void parsePlanDateResponse(String response) {
        try
        {
            SuperLikeResponse superLikeResponse = utility.getGson().fromJson(response, SuperLikeResponse.class);
            if(superLikeResponse.getCoinWallet() != null) {
                coinBalanceHolder.setCoinBalance(String.valueOf(superLikeResponse.getCoinWallet().getCoin()));
                coinBalanceHolder.setUpdated(true);
            }
        }catch (Exception e){
            e.getMessage();
        }
    }

    public int getListSize() {
        return pendingList.size();
    }

    public void clearList() {
        pendingList.clear();
    }

    public boolean loadMoreRequired(int currentPos) {
        if(loadMoreStatus.isNo_more_data()){
            return false;
        }

        try
        {
            int size=pendingList.size();
            int pending=size-currentPos;
            if(pending<5)
            {
                DateListPojo last_item=pendingList.get(size-1);
                if(!last_item.isLoading())
                {
                    DateListPojo loading_item=new DateListPojo();
                    loading_item.setLoading(true);
                    pendingList.add(loading_item);
                    return true;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public DateListPojo getPendingDateItem(int position) {
        if(pendingList.size() > position)
            return pendingList.get(position);
        return null;
    }

    public boolean checkDatesExist() {
        if(pendingList.size() > 0 )
            return true;
        return false;
    }

    public DateListPojo removeDateFromList(int currentPosition) {
        DateListPojo  dateData = null;
        try {
            if (getListSize() > currentPosition) {
                dateData  = pendingList.remove(currentPosition);
            }
        }catch (Exception e){
        }
        return dateData;
    }

    public void replaceDateItem(DateListPojo dateListPojo, int currentPosition) {
        try {
            if(getListSize() > currentPosition){
                pendingList.set(currentPosition,dateListPojo);
            }
        }catch (Exception e){}
    }

    //calender reminder helper methods

    public void saveEventId(DateEvent dateEvent) {
        ArrayList<DateEvent> savedEventList =  getEventId();
        if(!savedEventList.contains(dateEvent))
            savedEventList.add(dateEvent);

        try {
            ArrayList<String> finalEventList = new ArrayList<>();
            for (DateEvent dateEvent1 : savedEventList) {
                String json = utility.getGson().toJson(dateEvent1, DateEvent.class);
                finalEventList.add(json);
            }
            dataSource.setEvents(finalEventList);
        }catch (Exception e){}
    }

    private ArrayList<DateEvent> getEventId(){
        ArrayList<String> jsonEventList = dataSource.getEvents();
        ArrayList<DateEvent> dateEvents = new ArrayList<>();
        try {
            for (String json : jsonEventList) {
                DateEvent dateEvent = utility.getGson().fromJson(json, DateEvent.class);
                if (dateEvent != null)
                    dateEvents.add(dateEvent);
            }
        }catch (Exception e){}
        return dateEvents;
    }

    public String checkIfReminderExist(String userId) {
        ArrayList<DateEvent> savedEventList =  getEventId();
        boolean found = false;
        String eventId = "";
        for(DateEvent dateEvent : savedEventList){
            if(dateEvent.getUserId().equals(userId)){
                found = true;
                eventId = dateEvent.getEventId();
            }
        }
        if(found)
            return eventId;
        return "";
    }

}
