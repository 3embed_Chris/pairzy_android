package com.pairzy.com.dublyCamera.CameraInFragments;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.dublyCamera.SampleCameraGLView;
import com.pairzy.com.dublyCamera.gpuv.camerarecorder.CameraRecordListener;
import com.pairzy.com.dublyCamera.gpuv.camerarecorder.GPUCameraRecorderBuilder;
import com.pairzy.com.dublyCamera.gpuv.camerarecorder.LensFacing;
import com.pairzy.com.util.AppConfig;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TimerTask;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoCaptureFragment extends Fragment {

    private static final int CAMERA_PERMISSIONS_REQ_CODE = 0;
    private static final int MAXIMUM_RECORDING_LENGTH_ALLOWED = 300;
    private final int SELECT_DUB_SOUND_REQUEST = 0;
    private int speed = 3;
    private com.pairzy.com.dublyCamera.gpuv.camerarecorder.GPUCameraRecorder GPUCameraRecorder;
    private LensFacing lensFacing = LensFacing.FRONT;
    //Binding views

    @BindView(R.id.camera)
    FrameLayout camera;
    @BindView(R.id.contentFrame)
    RelativeLayout parent;
    @BindView(R.id.blackCover)
    View coverView;
    //Bindviews for camera controls
    @BindView(R.id.facingButton)
    AppCompatImageView facingButton;
    @BindView(R.id.flashButton)
    AppCompatImageView flashButton;
    @BindView(R.id.captureButton)
    AppCompatImageView captureButton;
    @BindView(R.id.tick)
    AppCompatImageView tick;

    private Handler videoCaptureHandler = new Handler();
    private Handler delayProcessingForFileSaveHandler = new Handler();
    private boolean capturingVideo;
    private TimerTask timerTask;
    private int width;
    private float videoCapturedLength;
    private int durationOfVideoCaptured;
    private long captureStartTime, captureEndTime;
    private String filepath;
    private DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
    private int screenWidth = displayMetrics.widthPixels;
    private int cameraWidth = screenWidth;
    private int videoWidth = screenWidth;
    private int cameraHeight = screenWidth;
    private int videoHeight = screenWidth;
    private boolean toggleCamera = false;
    private boolean fragmentVisible;
    private boolean flashRunning;
    private Bus bus = AppController.getBus();
    private Context context;
    private View view;
    private String folderPath;

    @BindString(R.string.app_name)
    String appName;
    private int widthScreen;
    private boolean noPendingPermissionsRequest = true;
    private SampleCameraGLView sampleGLView;
    private boolean captureVideoRequested;
    private boolean killCameraActivityRequested;

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view == null) {
            view = inflater.inflate(R.layout.camera_main_fragment, container, false);
        } else {
            if (view.getParent() != null)
                ((ViewGroup) view.getParent()).removeView(view);
        }

        context = getActivity();
        ButterKnife.bind(this, view);

        ViewGroup.LayoutParams params = camera.getLayoutParams();
        params.height = screenWidth;
        camera.setLayoutParams(params);
        params = coverView.getLayoutParams();
        params.height = screenWidth;
        coverView.setLayoutParams(params);
        int density = (int) context.getResources().getDisplayMetrics().density;

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) facingButton.getLayoutParams();
        layoutParams.setMargins(13 * density, cameraHeight - 43 * density, 0, 0);

        facingButton.setLayoutParams(layoutParams);

        layoutParams = (RelativeLayout.LayoutParams) flashButton.getLayoutParams();
        layoutParams.setMargins(0, cameraHeight - 43 * density, 10 * density, 0);
        flashButton.setLayoutParams(layoutParams);

        final File imageFolder;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            imageFolder = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.app_name) + "/Media/");

        } else {
            imageFolder = new File(context.getFilesDir() + "/" + getResources().getString(R.string.app_name) + "/Media/");
        }

        if (!imageFolder.exists() && !imageFolder.isDirectory())
            imageFolder.mkdirs();

        folderPath = imageFolder.getAbsolutePath();


        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((CameraInFragmentsActivity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        widthScreen = displayMetrics.widthPixels;

        captureButton.setOnTouchListener(handleTouch);

        facingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                releaseCamera();
                coverView.setAlpha(0);
                coverView.setVisibility(View.VISIBLE);
                coverView.animate().alpha(1)
                        .setStartDelay(0)
                        .setDuration(300)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                releaseCamera();
                                if (lensFacing == LensFacing.FRONT) {

                                    lensFacing = LensFacing.BACK;

                                    changeViewImageResource((ImageView) view, R.drawable.ic_facing_front);
                                } else {
                                    lensFacing = LensFacing.FRONT;

                                    changeViewImageResource((ImageView) view, R.drawable.ic_facing_back);
                                }
                                toggleCamera = true;

                                coverView.animate()
                                        .alpha(0)
                                        .setStartDelay(200)
                                        .setDuration(300)
                                        .setListener(new AnimatorListenerAdapter() {
                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                super.onAnimationEnd(animation);
                                                coverView.setVisibility(View.GONE);
                                            }
                                        })
                                        .start();
                            }
                        })
                        .start();
            }
        });

        flashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (GPUCameraRecorder != null && GPUCameraRecorder.isFlashSupport()) {
                    GPUCameraRecorder.switchFlashMode();

                    GPUCameraRecorder.changeAutoFocus();
                }

                if (flashRunning) {


                    flashRunning = false;
                    changeViewImageResource((AppCompatImageView) view, R.drawable.ic_flash_on);
                } else {

                    flashRunning = true;
                    changeViewImageResource((AppCompatImageView) view, R.drawable.ic_flash_off);
                }


            }
        });

        bus.register(this);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (fragmentVisible) {
            if (noPendingPermissionsRequest) {
                validateCameraPermissions();
            } else {
                noPendingPermissionsRequest = true;
            }
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(isVisibleToUser);
        fragmentVisible = isVisibleToUser;
        if (isVisibleToUser) {
            if (noPendingPermissionsRequest) {
                validateCameraPermissions();
            } else {
                noPendingPermissionsRequest = true;
            }
        } else {
            releaseResources();
        }
    }


    private void releaseResources() {
        try {
            if (timerTask != null) {
                timerTask.cancel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        releaseCamera();
    }

    @Override
    public void onStop() {
        releaseResources();
        super.onStop();
    }


    public void onBackPressed() {
        ((CameraInFragmentsActivity) context).onBackPressed();
    }


    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void handleVideoCaptured(String recordedFilepath) {
     //String filepath = makeMp4FilesStreamable(recordedFilepath);
     sendVideo(recordedFilepath);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void sendVideo(String videoPath) {
        Intent intent = new Intent(context, PreviewFragmentVideoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra("videoPath", videoPath);
        startActivityForResult(intent, AppConfig.POST_ACTIVITY_REQ_CODE);
    }

    private void setMicMuted(boolean state) {
        AudioManager myAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (myAudioManager != null) {
            // get the working mode and keep it
            int workingAudioMode = myAudioManager.getMode();

            myAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);

            // change mic state only if needed
            if (myAudioManager.isMicrophoneMute() != state) {
                myAudioManager.setMicrophoneMute(state);
            }
            // set back the original working mode
            myAudioManager.setMode(workingAudioMode);
        }
    }

    private void checkCameraPermissions() {

        if (ActivityCompat.shouldShowRequestPermissionRationale((CameraInFragmentsActivity) context, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale((CameraInFragmentsActivity) context, Manifest.permission.WRITE_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale((CameraInFragmentsActivity) context, Manifest.permission.RECORD_AUDIO) || ActivityCompat.shouldShowRequestPermissionRationale((CameraInFragmentsActivity) context, Manifest.permission.CAMERA)) {

            AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.UpdateAlertDialog);
            builder.setTitle("Permissions");
            builder.setMessage(R.string.camera_permission);
            builder.setPositiveButton("OK", (dialogInterface, i) -> requestCameraPermission());
            builder.setNegativeButton("DENY", (dialogInterface, i) -> {
                cameraPermissionsDenied();
            });
            builder.show();
        } else {
            requestCameraPermission();
        }


    }

    private void cameraPermissionsDenied() {

        if (parent != null) {
            Snackbar snackbar = Snackbar.make(parent, R.string.camera_permission_denied,
                    Snackbar.LENGTH_SHORT);
            snackbar.show();

            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ((CameraInFragmentsActivity) context).supportFinishAfterTransition();
            }
        }, 500);
    }

    private void requestCameraPermission() {
        ArrayList<String> permissionsRequired = new ArrayList<>();

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            permissionsRequired.add(Manifest.permission.CAMERA);
        }
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            permissionsRequired.add(Manifest.permission.RECORD_AUDIO);
        }
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            permissionsRequired.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            permissionsRequired.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        noPendingPermissionsRequest = false;
        requestPermissions(permissionsRequired.toArray(new String[permissionsRequired.size()]), CAMERA_PERMISSIONS_REQ_CODE);
    }




    private void validateCameraPermissions() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {

            try {
                setUpCamera();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            checkCameraPermissions();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (requestCode == CAMERA_PERMISSIONS_REQ_CODE) {
            int size = grantResults.length;


            boolean allPermissionsGranted = true;
            for (int i = 0; i < size; i++) {

                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {

                    allPermissionsGranted = false;
                    break;
                }

            }
            if (allPermissionsGranted) {

                setUpCamera();
            } else {
                cameraPermissionsDenied();
            }
        }


    }

    private void touchDownAnimation(View view) {
        view.animate()
                .scaleX(0.88f)
                .scaleY(0.88f)
                .setDuration(300)
                .setInterpolator(new OvershootInterpolator())
                .start();
    }

    private void touchUpAnimation(View view) {
        view.animate()
                .scaleX(1f)
                .scaleY(1f)
                .setDuration(300)
                .setInterpolator(new OvershootInterpolator())
                .start();
    }

    private void changeViewImageResource(final ImageView imageView,
                                         @DrawableRes final int resId) {
        imageView.setRotation(0);
        imageView.animate()
                .rotationBy(360)
                .setDuration(400)
                .setInterpolator(new OvershootInterpolator())
                .start();
        imageView.postDelayed(new Runnable() {
            @Override
            public void run() {
                imageView.setImageResource(resId);
            }
        }, 120);
    }


    private void stopVideoCapture(boolean fromNonUIThread) {
        captureEndTime = System.currentTimeMillis();
        if (fromNonUIThread) {
            ((CameraInFragmentsActivity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    GPUCameraRecorder.stop();
                }
            });
        } else {
            GPUCameraRecorder.stop();
        }

        durationOfVideoCaptured = 0;
        if (fromNonUIThread) {
            ((CameraInFragmentsActivity) context).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    captureButton.setColorFilter(null);
                }
            });
        } else {
            captureButton.setColorFilter(null);
        }
    }


    private void startVideoCapture() {
        capturingVideo = true;
        durationOfVideoCaptured = 0;
        videoCapturedLength = 0;
        captureStartTime = System.currentTimeMillis();
        filepath = folderPath + "/" + System.currentTimeMillis() + appName + ".mp4";
        //filepath = getVideoFilePath();
        GPUCameraRecorder.start(filepath);
//        updateProgressBar(true);
        captureButton.setColorFilter(ContextCompat.getColor(context, R.color.doodle_color_red), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void deleteFileFromDisk(String filePath) {
        try {
            File file = new File(filePath);

            if (file.exists()) {

                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    //Code for the video filters
    private void releaseCamera() {
        if (sampleGLView != null) {
            sampleGLView.onPause();
        }
        if (GPUCameraRecorder != null) {
            GPUCameraRecorder.stop();
            GPUCameraRecorder.release();
            GPUCameraRecorder = null;
        }
        if (sampleGLView != null) {
            (camera).removeView(sampleGLView);
            sampleGLView = null;
        }
    }


    private void setUpCameraView() {
        ((CameraInFragmentsActivity) context).runOnUiThread(() -> {
            camera.removeAllViews();
            sampleGLView = null;
            sampleGLView = new SampleCameraGLView(context.getApplicationContext());

            sampleGLView.setTouchListener((event, width, height) -> {
                if (GPUCameraRecorder == null) return;
                GPUCameraRecorder.changeManualFocusPoint(event.getX(), event.getY(), width, height);
            });
            camera.addView(sampleGLView);
        });
    }


    private void setUpCamera() {
        setUpCameraView();
        GPUCameraRecorder = new GPUCameraRecorderBuilder((CameraInFragmentsActivity) context, sampleGLView)
                .cameraRecordListener(new CameraRecordListener() {
                    @Override
                    public void onGetFlashSupport(boolean flashSupport) {
                        ((CameraInFragmentsActivity) context).runOnUiThread(() -> {
                            flashRunning = false;

                            changeViewImageResource(flashButton, R.drawable.ic_flash_on);

                            if (flashSupport) {
                                flashButton.setVisibility(View.VISIBLE);

                            } else {
                                flashButton.setVisibility(View.GONE);
                            }

                        });
                    }

                    @Override
                    public void onRecordComplete() {
                        handleVideoCaptured(filepath);
                    }

                    @Override
                    public void onRecordStart() {

                    }

                    @Override
                    public void onError(Exception exception) {
                        exception.printStackTrace();
                    }

                    @Override
                    public void onCameraThreadFinish() {
                        if (toggleCamera) {
                            ((CameraInFragmentsActivity) context).runOnUiThread(() -> {
                                setUpCamera();
                            });
                        }
                        toggleCamera = false;
                    }
                }).videoSize(videoWidth, videoHeight)
                .cameraSize(cameraWidth, cameraHeight)
                .lensFacing(lensFacing)
                //.mute(true)
                .build();
    }


    private View.OnTouchListener handleTouch = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View view, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    vibrate();
                    if (0 < MAXIMUM_RECORDING_LENGTH_ALLOWED) {
                        captureVideoRequested = true;
                        videoCaptureHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (captureVideoRequested) {
                                    startVideoCapture();
                                }
                            }
                        }, 250);
                    } else {
                        Toast.makeText(context, R.string.recording_max_size, Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
                case MotionEvent.ACTION_UP: {
                    captureVideoRequested = false;

                    if (capturingVideo) {
                        //Stop video capture
                        capturingVideo = false;
                        try {
                            if (timerTask != null) {
                                timerTask.cancel();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        stopVideoCapture(false);
                    }
                    break;
                }
            }
            return true;
        }
    };

    private static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale);
    }


    @SuppressWarnings("ResultOfMethodCallIgnored")
    private String makeMp4FilesStreamable(String inputFilePath) {
        String path = inputFilePath;
        File input = new File(inputFilePath); // Your input file
        File output = new File(folderPath, System.currentTimeMillis() + appName + ".mp4"); // Your output file
        try {
            if (!output.exists()) {// if there is no output file we'll create one
                output.createNewFile();
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        try {
            deleteFileFromDisk(inputFilePath);
            path = output.getAbsolutePath();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return path;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


        bus.unregister(this);
    }

    @Subscribe
    public void getMessage(JSONObject object) {
        try {
            if (object.getString("eventName").equals("killCameraActivity")) {
                //     super.onBackPressed();
                killCameraActivityRequested = true;
                ((CameraInFragmentsActivity) context).supportFinishAfterTransition();
            } else if (object.getString("eventName").equals("selectDubSoundResult")) {

                if (object.getInt("requestCode") == SELECT_DUB_SOUND_REQUEST) {
                    if (object.getInt("resultCode") == Activity.RESULT_OK) {
                    } else if (object.getInt("resultCode") == Activity.RESULT_CANCELED) {
                        //Write your code if there's no result
                        Toast.makeText(context, getString(R.string.dubsound_cancel), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void vibrate() {
        // TODO Auto-generated method stub
        try {
            Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            if (vibrator != null) {

                if (Build.VERSION.SDK_INT >= 26) {
                    vibrator.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    vibrator.vibrate(200);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

