package com.pairzy.com.util.boostDialog;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;
/**
 * <h>CustomScroller class</h>
 * @author 3Embed.
 * @since 10/5/18.
 * @version 1.0.
 */
public class CustomScroller extends Scroller
{
    int mDuration;
    public CustomScroller(Context context, Interpolator interpolator, int duration) {
        super(context, interpolator);
        mDuration = duration;
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        super.startScroll(startX, startY, dx, dy, mDuration);
    }
}
