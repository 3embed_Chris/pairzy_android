package com.pairzy.com.userProfile.Model;

public interface MomentClickCallback {
    void onMomentClick(int position);
}
