package com.pairzy.com.data.source;

import java.util.ArrayList;

/**
 * Created by ankit on 23/5/18.
 */

public interface PassportLocationDataSource
{

    void setPassportLocations(ArrayList<String> passportLocations);

    ArrayList<String> getPassportLocations();

    void setFeatureLocation(String name);

    String getFeatureLocation();

    void setCurrentLocation(String name);

    String getCurrentLocation();

    boolean isFeaturesLocActive();

    void  setFeaturedLcoationActive(boolean isActive);
}
