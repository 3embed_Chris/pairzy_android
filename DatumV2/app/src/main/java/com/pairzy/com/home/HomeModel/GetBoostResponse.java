package com.pairzy.com.home.HomeModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetBoostResponse {

@SerializedName("message")
@Expose
private String message;
@SerializedName("data")
@Expose
private GetBoostData data;

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public GetBoostData getData() {
return data;
}

public void setData(GetBoostData data) {
this.data = data;
}

}