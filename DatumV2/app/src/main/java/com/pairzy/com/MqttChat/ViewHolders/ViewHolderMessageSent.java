package com.pairzy.com.MqttChat.ViewHolders;
/*
 * Created by moda on 02/04/16.
 */

import android.animation.ObjectAnimator;
import android.graphics.Typeface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;

import com.pairzy.com.util.TypeFaceManager;


/**
 * View holder for text message send recycler view item
 */
public class ViewHolderMessageSent extends RecyclerView.ViewHolder {


//    public TextView senderName;

    public TextView message, time, date, previousMessage_head, previousMessage_content;

    public ImageView singleTick, doubleTickGreen, doubleTickBlue, clock, previousMessage_iv, forward;//, blocked;
    public RelativeLayout messageRoot, previousMessage_rl;

    public ViewHolderMessageSent(View view, TypeFaceManager typeFaceManager) {
        super(view);
//        blocked = (ImageView) view.findViewById(R.id.blocked);
        date = (TextView) view.findViewById(R.id.date);
        forward = (ImageView) view.findViewById(R.id.forward_iv);
        // senderName = (TextView) view.findViewById(R.id.lblMsgFrom);
        messageRoot = (RelativeLayout) view.findViewById(R.id.message_root);

        ObjectAnimator animation = ObjectAnimator.ofFloat(forward, "rotationY", 0.0f, 180f);
        animation.setDuration(0);

        animation.start();
          /*
         * For message reply feature
         */
        previousMessage_rl = (RelativeLayout) view.findViewById(R.id.initialMessage_rl);
        previousMessage_head = (TextView) view.findViewById(R.id.senderName_tv);
        previousMessage_iv = (ImageView) view.findViewById(R.id.initialMessage_iv);
        previousMessage_content = (TextView) view.findViewById(R.id.message_tv);
        message = (TextView) view.findViewById(R.id.txtMsg);

        time = (TextView) view.findViewById(R.id.ts);

        singleTick = (ImageView) view.findViewById(R.id.single_tick_green);

        doubleTickGreen = (ImageView) view.findViewById(R.id.double_tick_green);

        doubleTickBlue = (ImageView) view.findViewById(R.id.double_tick_blue);
        clock = (ImageView) view.findViewById(R.id.clock);
        // message.setMovementMethod(LinkMovementMethod.getInstance());
        //Typeface tf = AppController.getInstance().getRobotoCondensedFont();
        time.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.ITALIC);
        date.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.ITALIC);
        message.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.NORMAL);


        if (previousMessage_head != null) {
            previousMessage_head.setTypeface(typeFaceManager.getCircularAirBold(), Typeface.BOLD);
            previousMessage_content.setTypeface(typeFaceManager.getCircularAirBook(), Typeface.NORMAL);
        }
    }
}
