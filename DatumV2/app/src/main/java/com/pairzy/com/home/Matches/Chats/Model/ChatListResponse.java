package com.pairzy.com.home.Matches.Chats.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class ChatListResponse implements Serializable
{
    private String message;

    private ArrayList<ChatListData> data;

    private String code;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public ArrayList<ChatListData> getData ()
    {
        return data;
    }

    public void setData (ArrayList<ChatListData> data)
    {
        this.data = data;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", data = "+data+", code = "+code+"]";
    }
}