package com.pairzy.com.home.Prospects.Online;
import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.home.Prospects.Online.Model.OnlineItemPojo;
import com.pairzy.com.home.Prospects.Online.Model.OnlineUserListAdapter;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import dagger.Module;
import dagger.Provides;
/**
 * @since  3/17/2018.
 * @version 1.0.
 * @author 3Embed.
 */
@Module
public class Online_util
{
    @Provides
    @FragmentScoped
    ArrayList<OnlineItemPojo> getList()
    {
     return new ArrayList<>();
    }

    @Provides
    @FragmentScoped
    OnlineUserListAdapter getUserListAdapter(ArrayList<OnlineItemPojo> list, TypeFaceManager typeFaceManager, Utility utility)
    {
        return new OnlineUserListAdapter(list,typeFaceManager,utility);
    }

    @Provides
    @FragmentScoped
    LinearLayoutManager getLinearLayoutManager(Activity activity)
    {
        return new LinearLayoutManager(activity);
    }
}
