package com.pairzy.com.UserPreference.CanditionChoice;

import com.pairzy.com.BaseModel;
import com.pairzy.com.util.ApiConfig;

import org.json.JSONArray;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
/**
 * @since  2/22/2018.
 */
class CondChoiceModel extends BaseModel
{
    @Inject
    CondChoiceModel(){}
    /**
     * @param pref_id :contains the preference id.
     * @param values contains the pref id.
     * @return parameters
     */
    Map<String, Object> getParams(String pref_id, JSONArray values) {
        Map<String, Object> map = new HashMap<>();
        map.put(ApiConfig.SetPreferenceKey.PREF_ID, pref_id);
        map.put(ApiConfig.SetPreferenceKey.PREF_VALUES, values);
        return map;
    }
}
