package com.pairzy.com.planDate;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.addCoin.AddCoinActivity;
import com.pairzy.com.home.Dates.Model.DateListPojo;
import com.pairzy.com.locationScreen.LocationSearchActivity;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.facebook.drawee.view.SimpleDraweeView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h>DateActivity class</h>
 * <p> This id plan Date activity.</p>
 * @author 3Embed.
 * @since 28/4/18.
 * @version 1.0.
 */

public class DateActivity extends BaseDaggerActivity implements DateContract.View,View.OnClickListener{

    @Inject
    DateContract.Presenter presenter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    Utility utility;
    @Inject
    Activity activity;
    @Inject
    App_permission app_permission;

    @BindView(R.id.tv_page_title)
    TextView tvPageTitle;
    @BindView(R.id.tv_msg)
    TextView tvMsg;
    @BindView(R.id.iv_heart_one)
    ImageView ivHeartOne;
    @BindView(R.id.iv_heart_two)
    ImageView ivHeartTwo;
    @BindView(R.id.iv_heart_three)
    ImageView ivHeartThree;
    @BindView(R.id.iv_heart_four)
    ImageView ivHeartFour;
    @BindView(R.id.user_profile_pic_one)
    SimpleDraweeView profilePicOne;
    @BindView(R.id.user_profile_pic_two)
    SimpleDraweeView profilePicTwo;
    @BindView(R.id.tv_location_title)
    TextView tvLocationTitle;
    @BindView(R.id.tv_location)
    TextView tvLocation;
    @BindView(R.id.tv_time_title)
    TextView tvTimeTitle;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.parent_layout)
    RelativeLayout parentLayout;
    @BindView(R.id.location_ll)
    LinearLayout btnLocation;
    @BindView(R.id.time_ll)
    LinearLayout btnDate;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        presenter.init();
        presenter.initData(getIntent());
        setupClickListener();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setupClickListener() {
        btnLocation.setOnClickListener(this);
        btnDate.setOnClickListener(this);
    }


    private void launchLocationScreen() {
        Intent intent =new Intent(DateActivity.this,LocationSearchActivity.class);
        startActivityForResult(intent, AppConfig.DateActivity.LOACTION_REQUEST_CODE);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }


    @Override
    public void applyFont() {
        tvPageTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvMsg.setTypeface(typeFaceManager.getCircularAirBook());
        tvLocationTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvTimeTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvLocation.setTypeface(typeFaceManager.getCircularAirBook());
        tvTime.setTypeface(typeFaceManager.getCircularAirBook());
    }


    @Override
    public void showError(String errorMsg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+errorMsg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(String msg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+msg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(int msgId) {
        String message=getString(msgId);
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showSelectedDate(String dateStr) {
        tvTime.setText(""+dateStr);
    }

    @Override
    public void showSelectedAddress(SelectedLocationHolder locationHolder) {
        tvLocation.setText(locationHolder.getLocationTitle());
    }

    @Override
    public void initUserData(String userName, String userImage) {
        profilePicTwo.setImageURI(Uri.parse(userImage));
        tvMsg.setText(getResources().getString(R.string.date_msg_text).concat(" if "+userName+" confirms."));
    }

    @Override
    public void initOwnData(String yourName, String profilePic) {
        profilePicOne.setImageURI(Uri.parse(profilePic));
    }

    @Override
    public void initUserData(DateListPojo date_data) {
        initUserData(date_data.getOpponentName(),date_data.getOpponentProfilePic());
        tvLocation.setText(date_data.getPlaceName());
        tvTime.setText(date_data.getProposedOnInMyTimeZone());
    }

    @Override
    public void returnFinalData(DateListPojo date_data) {
        Intent data = new Intent();
        data.putExtra("date_data",date_data);
        setResult(RESULT_OK,data);
        onBackPressed();
    }


    @Override
    public void launchCoinWallet() {
        Intent intent = new Intent(activity,AddCoinActivity.class);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.handleOnActivityResult(requestCode,resultCode,data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        presenter.dispose();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.location_ll:
                launchLocationScreen();
                break;
            case R.id.time_ll:
                presenter.launchDateTimePicker();
                break;
            default:
        }
    }


    @OnClick(R.id.close_button)
    public void close(){
        onBackPressed();
    }

    @OnClick(R.id.tick_mark)
    public void planDate(){
        presenter.checkForValidInput();
    }

    @Override
    public void onBackPressed() {
        this.finish();
        activity.overridePendingTransition(R.anim.slide_from_left,R.anim.slide_to_right);
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if(!app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults))
        {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
