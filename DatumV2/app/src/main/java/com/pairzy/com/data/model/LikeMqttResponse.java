package com.pairzy.com.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 6/9/18.
 */

public class LikeMqttResponse {
    @SerializedName("newLikes")
    @Expose
    private String newLikes;

    public String getNewLikes() {
        return newLikes;
    }

    public void setNewLikes(String newLikes) {
        this.newLikes = newLikes;
    }
}
