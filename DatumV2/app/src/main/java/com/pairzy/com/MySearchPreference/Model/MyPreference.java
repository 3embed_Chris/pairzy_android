package com.pairzy.com.MySearchPreference.Model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 *@since 3/7/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class MyPreference
{
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("PreferenceTitle")
    @Expose
    private String preferenceTitle;
    @SerializedName("Priority")
    @Expose
    private Integer priority;
    @SerializedName("TypeOfPreference")
    @Expose
    private Integer typeOfPreference;
    @SerializedName("Unit")
    @Expose
    private String unit;
    @SerializedName("OptionsValue")
    @Expose
    private ArrayList<Integer> optionsValue = null;
    @SerializedName("selectedValue")
    @Expose
    private ArrayList<Integer> selectedValue = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPreferenceTitle() {
        return preferenceTitle;
    }

    public void setPreferenceTitle(String preferenceTitle) {
        this.preferenceTitle = preferenceTitle;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getTypeOfPreference() {
        return typeOfPreference;
    }

    public void setTypeOfPreference(Integer typeOfPreference) {
        this.typeOfPreference = typeOfPreference;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public ArrayList<Integer> getOptionsValue() {
        return optionsValue;
    }

    public void setOptionsValue(ArrayList<Integer> optionsValue) {
        this.optionsValue = optionsValue;
    }

    public ArrayList<Integer> getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(ArrayList<Integer> selectedValue) {
        this.selectedValue = selectedValue;
    }

}