package com.pairzy.com.coinWallet.coinIn;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;

/**
 *<h>InCoinContract</h>
 * <p></p>
 *@author 3Embed.
 *@since 30/5/18.
 */
public interface InCoinContract {

    interface View extends BaseView{

        void showLoading();
        void showNetworkError(String errorMsg);
        void emptyData();
        void notifyAdapter();
    }

    interface Presenter extends BasePresenter<View>{

        boolean isCoinHistoryEmpty();
    }
}
