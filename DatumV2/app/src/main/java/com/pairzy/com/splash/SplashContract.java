package com.pairzy.com.splash;

/**
 * <h2>SplashContract</h2>
 * <p>
 * Contains @{@link SplashActivity}'s @{@link View} and @{@link Presenter}
 * </P>
 *
 * @author 3Embed.
 * @version 1.0.
 * @since 05/01/2018.
 **/

public interface SplashContract {

    interface View {
        /**
         * <p>Displays message</p>
         *
         * @param message : String message
         */
        void showMessage(String message);

        /**
         * <p>Redirect to Activity to another Activity</p>
         */
        void move();

        void showError(String error);

        void showLoading();

        void showInternetError(String string);

        void invalidateSelectedAppLanguage();
    }

    interface Presenter {
        /**
         * <p>Process to openSignUP to Activity to another Activity</p>
         */
        void redirect();

        void checkAndGetAppData();

        void loadCoinBalance();

        void dispose();
    }
}
