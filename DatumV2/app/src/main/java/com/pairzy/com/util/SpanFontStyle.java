package com.pairzy.com.util;

import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;

/**
 * <h2>SpanFontStyle</h2>
 * <P>
 *    Typeface and color provider for the span style in android.
 * </P>
 * @since  2/1/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class SpanFontStyle extends TypefaceSpan
{
    private Typeface typeface;
    private int color;
    public SpanFontStyle(Typeface typeface,int colorCode)
    {
        super("Datumv2");
        this.typeface=typeface;
        this.color=colorCode;
    }

    @Override
    public void updateDrawState(TextPaint ds)
    {
        super.updateDrawState(ds);
        ds.setTypeface(typeface);
        ds.setColor(color);
    }
}
