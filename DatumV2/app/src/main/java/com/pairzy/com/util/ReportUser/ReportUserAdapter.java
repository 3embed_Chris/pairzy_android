package com.pairzy.com.util.ReportUser;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

/**
 * Created by ankit on 15/6/18.
 */

public class ReportUserAdapter extends RecyclerView.Adapter<ReportUserAdapter.ViewHolder> {

    private ArrayList<String> reportReasonList;
    private TypeFaceManager typeFaceManager;
    private ReportAdapterCallback clickCallback;


    public ReportUserAdapter(ReportAdapterCallback clickCallback,ArrayList<String> reportReasonList, TypeFaceManager typeFaceManager) {
        this.clickCallback = clickCallback;
        this.reportReasonList = reportReasonList;
        this.typeFaceManager = typeFaceManager;
    }

    @NonNull
    @Override
    public ReportUserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.report_user_item,parent,false);
        return new ReportUserAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportUserAdapter.ViewHolder holder, int position) {
        try{
            bindReportView(holder);
        }catch (Exception e){}
    }

    private void bindReportView(ViewHolder holder) {
        int position = holder.getAdapterPosition();
        String reportReason = reportReasonList.get(position);
        holder.tvTitle.setText(reportReason);
    }

    @Override
    public int getItemCount() {
        return reportReasonList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tvTitle;
        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.title_text);
            tvTitle.setTypeface(typeFaceManager.getCircularAirBook());
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(clickCallback != null)
                clickCallback.onAdapterClick(this.getAdapterPosition());
        }
    }
}
