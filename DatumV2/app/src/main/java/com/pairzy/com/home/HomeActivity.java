package com.pairzy.com.home;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.pairzy.com.AppController;
import com.pairzy.com.R;
import com.pairzy.com.addCoin.AddCoinActivity;
import com.pairzy.com.appSettings.AppSettingsActivity;
import com.pairzy.com.chatMessageScreen.ChatMessageActivity;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.home.Dates.DatesFrag;
import com.pairzy.com.home.Discover.DiscoveryFrag;
import com.pairzy.com.home.Discover.DiscoveryFragUtil;
import com.pairzy.com.home.Discover.GridFrg.Model.CardDeckViewAdapter;
import com.pairzy.com.home.Discover.Model.UserItemPojo;
import com.pairzy.com.home.MakeMatch.MakeMatchFrag;
import com.pairzy.com.home.Matches.Chats.ChatsFragment;
import com.pairzy.com.home.Matches.MatchesFrag;
import com.pairzy.com.home.Prospects.ProspectsFrg;
import com.pairzy.com.networking.NetworkService;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.CustomVideoView.NoInterNetView;
import com.pairzy.com.util.LocationProvider.Location_service;
import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.suresh.innapp_purches.InnAppSdk;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * <h2>HomeActivity</h2>
 * <p>
 * Home screen
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 24/01/2018.
 **/
public class HomeActivity extends BaseDaggerActivity implements HomeContract.View
{
    private static final String TAG = HomeActivity.class.getSimpleName();

    @Inject
    Activity activity;
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    DiscoveryFrag discoveryFrg;
    @Inject
    ProspectsFrg prospectsFrg;
    @Inject
    DatesFrag datesFrg;
    @Inject
    MatchesFrag matchesFragment;
    @Inject
    MakeMatchFrag makeMatchFrag;
    @Inject
    @Named(HomeUtil.HOME_FRAGMENT_MANAGER)
    FragmentManager fragmentManager;
    @Inject
    HomeContract.Presenter presenter;
    @Inject
    @Named(DiscoveryFragUtil.USER_LIST)
    ArrayList<UserItemPojo> userList;
    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.parent_layout)
    CoordinatorLayout parent_layout;
    @BindView(R.id.no_internetView)
    NoInterNetView noInterNetView;
    @BindView(R.id.home_frg_container)
    FrameLayout fragContainer;
    @Inject
    NetworkService networkService;
    private TextView tvDateCount, tvChatCount;
    private Unbinder unbinder;
    public static boolean isHomeActivityPaused;
    public static boolean isDiscoverVisible = false;
    private InterstitialAd mInterstitialAd;
    private AlertDialog updateAlertDialog;
    private Bus bus = AppController.getBus();
    Bundle bundle;
    Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        MobileAds.initialize(this, getString(R.string.ADMOB_APP_ID));
        setContentView(R.layout.activity_home);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        unbinder = ButterKnife.bind(this);
        intUI();
        AppController.getInstance().createNewPostListener();
        bundle=getIntent().getBundleExtra("data");
        if(bundle==null)
            launchInitialFrg();
        onNewIntent(getIntent());
        presenter.subscribeToFirebaseTopic();
        presenter.observeCoinBalanceChange();
        presenter.observeAdminCoinAdd();
        presenter.loadCoinBalance();
        loadInterstialAds();
        bus.register(this);

        // Obtain the shared Tracker instance.
        AppController application = (AppController) getApplication();
        mTracker = application.getDefaultTracker();
    }

    @Override
    public void loadInterstialAds(){
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.INTERSTITIAL_AD_UNIT_ID)/*"ca-app-pub-3940256099942544/1033173712"*/);
        mInterstitialAd.setAdListener(new AdListener(){
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.d(TAG, "onAdFailedToLoad: "+i);
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
//                if (mInterstitialAd.isLoaded()) {
//                    mInterstitialAd.show();
//                }
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                Log.d(TAG, "onAdClosed: ");
                loadInterstialAds();
            }
        });
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    @Override
    public void showLoadedProfileAds() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
        else{
            loadInterstialAds();
        }
    }

    @Override
    public void showUpdateDialog(boolean isMandatory) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.UpdateAlertDialog);
        builder.setTitle(R.string.update_app_dialog_title)
                .setMessage(R.string.update_app_dialog_message)
                .setPositiveButton(R.string.update_dialog_positive_button_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent viewIntent =
                                new Intent(Intent.ACTION_VIEW,
                                        Uri.parse(getString(R.string.datum_play_store)));
                        startActivity(viewIntent);
                    }
                })
                .setNegativeButton(R.string.update_dialog_nagtive_button_text, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(isMandatory) {
                            dialog.dismiss();
                            finish();
                        }else{
                            dialog.dismiss();
                        }
                    }
                })
                .setCancelable(!isMandatory);
        updateAlertDialog = builder.create();
        updateAlertDialog.show();
    }

    @Override
    public void dismissUpdateDialog() {
        if(updateAlertDialog != null)
            updateAlertDialog.dismiss();
    }

    /**
     * stops the home as well as moment player.
     */
    @Override
    public void pausePlayer() {
        CardDeckViewAdapter.pausePlayer();
        if(matchesFragment != null)
            matchesFragment.pausePlayer();
    }

    @Override
    public void updateDiscoveryProfilePicture() {
        if(discoveryFrg != null)
            discoveryFrg.updateProfilePicture();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Image~" + "Analytics testing");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        presenter.checkForForceUpdate();
        AppController.getInstance().logIndexDocIDTesting();
        isHomeActivityPaused = false;
        if(isDiscoverVisible && discoveryFrg.getPagePosition() == 0 && !userList.isEmpty() ) {  //card visible.
            CardDeckViewAdapter.playPlayer();
        }else{
            CardDeckViewAdapter.pausePlayer();
        }
        presenter.checkOnlyForProfileBoost();
    }



    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        presenter.parseIntent(intent);
    }


    @Override
    protected void onPause()
    {
        isHomeActivityPaused = true;
        //pause moment player and home video player as well.
        pausePlayer();
        super.onPause();
    }


    @Subscribe
    public void getMessage(JSONObject object){
        try {
            if (object.getString("eventName").equals("callMinimized")) {
                minimizeCallScreen(object);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void minimizeCallScreen(JSONObject obj) {
        try {
            Intent intent = new Intent(activity, ChatMessageActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra("receiverUid", obj.getString("receiverUid"));
            intent.putExtra("receiverName", obj.getString("receiverName"));
            intent.putExtra("documentId", obj.getString("documentId"));
            intent.putExtra("receiverImage", obj.getString("receiverImage"));
            intent.putExtra("colorCode", obj.getString("colorCode"));
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy()
    {
        CardDeckViewAdapter.stopPlayer();
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
        bus.unregister(this);
    }

    /*
     * Init xml content*/
    private void intUI()
    {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
        //BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        isDiscoverVisible = true;
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId())
            {
                case R.id.discover:
                    pausePlayer();
                    isDiscoverVisible = true;
                    presenter.checkOnlyForProfileBoost();
                    AppController.getInstance().updatePresence(1,false);
                    Bundle data=new Bundle();
                    displayLoadedFrg(0,data);
                    return true;
                case R.id.Prospects:
                    pausePlayer();
                    isDiscoverVisible = false;
                    AppController.getInstance().updatePresence(1,false);
//                    if(dataSource.getSubscription()==null)
//                    {
//                        if(presenter!=null)
//                            presenter.openBoostDialog();
//                        return false;
//                    }
                    Bundle prospects_data=new Bundle();
                    displayLoadedFrg(1,prospects_data);
                    return true;
                case R.id.Dates:
                    pausePlayer();
                    isDiscoverVisible = false;
                    AppController.getInstance().updatePresence(1,false);
                    Bundle dates=new Bundle();
                    displayLoadedFrg(2,dates);
                    return true;
                case R.id.Chats:
                    pausePlayer();
                    isDiscoverVisible = false;
                    AppController.getInstance().updatePresence(1,false);
                    Bundle chats_data=new Bundle();
                    displayLoadedFrg(3,chats_data);
                    return true;
                case R.id.settings:
                    pausePlayer();
                    isDiscoverVisible = false;
                    AppController.getInstance().updatePresence(1,false);
                    Bundle settings=new Bundle();
                    displayLoadedFrg(4,settings);
                    return true;
            }
            return false;
        });
        addChatBadgeIcon();
        addDateBadgeIcon();
    }

    private void addDateBadgeIcon() {
        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(2);

        BottomNavigationItemView itemView = (BottomNavigationItemView) v;
        View badge = LayoutInflater.from(this)
                .inflate(R.layout.badge_chat_layout, bottomNavigationMenuView, false);

        tvDateCount = badge.findViewById(R.id.tv_tab_count);
        itemView.addView(badge);
    }

    private void addChatBadgeIcon() {
        BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(3);

        BottomNavigationItemView itemView = (BottomNavigationItemView) v;
        View badge = LayoutInflater.from(this)
                .inflate(R.layout.badge_chat_layout, bottomNavigationMenuView, false);

        tvChatCount = badge.findViewById(R.id.tv_tab_count);
        itemView.addView(badge);
    }

    //from chat frag
    @Override
    public void openProspectScreen() {
        bottomNavigationView.setSelectedItemId(R.id.Prospects);
    }

    @Override
    public void showUnreadChatCount(String unreadChatCount, boolean empty) {
        if(empty){
            tvChatCount.setVisibility(View.GONE);
        }
        else{
            tvChatCount.setText(unreadChatCount);
            tvChatCount.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showPendingDateCount(String pendingDateCount, boolean empty) {
        if(tvDateCount != null) {
            if (empty) {
                tvDateCount.setVisibility(View.GONE);
            } else {
                tvDateCount.setText(pendingDateCount);
                tvDateCount.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void selectTab(int pos) {
        switch (pos){
            case 0: //discovery
                bottomNavigationView.setSelectedItemId(R.id.discover);
                break;
            case 1: //prospect
                bottomNavigationView.setSelectedItemId(R.id.Prospects);
                break;
            case 2: //date
                bottomNavigationView.setSelectedItemId(R.id.Dates);
                break;
            case 3: //chat
                bottomNavigationView.setSelectedItemId(R.id.Chats);
                break;
            case 4://settings
                bottomNavigationView.setSelectedItemId(R.id.settings);
        }
    }

    /*
     * called from Grid and List fragment.
     */
    @Override
    public void stopPlayer() {
//        if(datumVideoPlayer != null)
//            datumVideoPlayer.stopPlayer();
    }

    /*
     * Launching the fragment on the opening of the activity.*/
    private void launchInitialFrg()
    {
        Bundle data=new Bundle();
        displayLoadedFrg(4,data);
    }

    /*
     * Displaying the already loaded fragment.*/
    private void displayLoadedFrg(int number,Bundle data)
    {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        switch (number)
        {
            case 0:

                if(isDiscoverVisible && !isHomeActivityPaused && discoveryFrg.getPagePosition() == 0 && !userList.isEmpty()) {  //card visible.
                    CardDeckViewAdapter.playPlayer();
                } else{
                    CardDeckViewAdapter.pausePlayer();
                }

                if(prospectsFrg.isAdded())
                {
                    ft.hide(prospectsFrg);
                }
                if(datesFrg.isAdded())
                {
                    ft.hide(datesFrg);
                }
                if(matchesFragment.isAdded()) {
                    ft.hide(matchesFragment);
                }

                if(makeMatchFrag.isAdded())
                {
                    ft.hide(makeMatchFrag);
                }
                if (discoveryFrg.isAdded())
                {
                    ft.show(discoveryFrg);
                    ft.commitAllowingStateLoss();
                } else
                {
                    discoveryFrg.setArguments(data);
                    presenter.openFragmentPage(ft,discoveryFrg);
                }
                break;
            case 1:
//                if(datumVideoPlayer != null)
//                    datumVideoPlayer.pausePlayer();
                if (discoveryFrg.isAdded())
                {
                    ft.hide(discoveryFrg);
                }
                if (datesFrg.isAdded()) {
                    ft.hide(datesFrg);
                }
                if(matchesFragment.isAdded()) {
                    ft.hide(matchesFragment);
                }
              if(makeMatchFrag.isAdded())
                {
                    ft.hide(makeMatchFrag);
                }

                if (prospectsFrg.isAdded())
                {
                    ft.show(prospectsFrg);
                    ft.commitAllowingStateLoss();
                } else {
                    prospectsFrg.setArguments(data);
                    presenter.openFragmentPage(ft,prospectsFrg);
                }
                break;
            case 2:
//                if(datumVideoPlayer != null)
//                    datumVideoPlayer.pausePlayer();
                if (discoveryFrg.isAdded())
                {
                    ft.hide(discoveryFrg);
                }
                if (prospectsFrg.isAdded()) {
                    ft.hide(prospectsFrg);
                }
                if (matchesFragment.isAdded()) {
                    ft.hide(matchesFragment);
                }
               if(makeMatchFrag.isAdded())
                {
                    ft.hide(makeMatchFrag);
                }
                if (datesFrg.isAdded())
                {
                    ft.show(datesFrg);
                    ft.commitAllowingStateLoss();
                } else {
                    datesFrg.setArguments(data);
                    presenter.openFragmentPage(ft,datesFrg);
                }
                break;
            case 3:
//                if(datumVideoPlayer != null)
//                    datumVideoPlayer.pausePlayer();
                if (discoveryFrg.isAdded())
                {
                    ft.hide(discoveryFrg);
                }
                if (prospectsFrg.isAdded()) {
                    ft.hide(prospectsFrg);
                }
                if (datesFrg.isAdded()) {
                    ft.hide(datesFrg);
                }
                if(makeMatchFrag.isAdded())
                {
                    ft.hide(makeMatchFrag);
                }
                if (matchesFragment.isAdded())
                {
                    ft.show(matchesFragment);
                    ft.commitAllowingStateLoss();
                } else {
                    matchesFragment.setArguments(data);
                    presenter.openFragmentPage(ft, matchesFragment);
                }
                break;
            case 4:
//                if(datumVideoPlayer != null)
//                    datumVideoPlayer.pausePlayer();
                if (discoveryFrg.isAdded())
                {
                    ft.hide(discoveryFrg);
                }
                if (prospectsFrg.isAdded()) {
                    ft.hide(prospectsFrg);
                }
                if (datesFrg.isAdded()) {
                    ft.hide(datesFrg);
                }
                if(matchesFragment.isAdded())
                {
                    ft.hide(matchesFragment);
                }

             if (makeMatchFrag.isAdded())
                {
                    ft.show(makeMatchFrag);
                    ft.commitAllowingStateLoss();
                } else
                {
                    makeMatchFrag.setArguments(data);
                    presenter.openFragmentPage(ft,makeMatchFrag);
                }
                break;
        }
    }

    @Override
    public void moveFragment(FragmentTransaction fragmentTransaction,DaggerFragment fragment)
    {
        String FRAGMENT_TAG = HomeUtil.SETTINGS_FRAG_TAG;
        if(fragment instanceof DiscoveryFrag){
            FRAGMENT_TAG = HomeUtil.DISCOVER_FRAG_TAG;
        }
        else if(fragment instanceof ProspectsFrg){

            FRAGMENT_TAG = HomeUtil.PROSPECT_FRAG_TAG;
        }
        else if(fragment instanceof DatesFrag){
            FRAGMENT_TAG = HomeUtil.DATE_FRAG_TAG;
        }
        else if(fragment instanceof ChatsFragment){
            FRAGMENT_TAG = HomeUtil.MATCHES_FRAG_TAG;
        } else if(fragment instanceof MakeMatchFrag){
            FRAGMENT_TAG = HomeUtil.MACTCH_MAKE_FRAG_TAG;
        }
        fragmentTransaction.add(R.id.home_frg_container,fragment,FRAGMENT_TAG);
        fragmentTransaction.commitAllowingStateLoss();
    }


    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(fragContainer,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showError(String error)
    {
        Snackbar snackbar = Snackbar
                .make(fragContainer,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }


    @Override
    public void verify_FragmentLoad(Fragment fragment)
    {
        if (fragment instanceof DiscoveryFrag)
        {
            displayLoadedFrg(0,null);
            bottomNavigationView.setSelectedItemId(R.id.discover);
        } else if (fragment instanceof ProspectsFrg)
        {
            displayLoadedFrg(1,null);
            bottomNavigationView.setSelectedItemId(R.id.Prospects);

        } else if (fragment instanceof DatesFrag)
        {
            displayLoadedFrg(2,null);
            bottomNavigationView.setSelectedItemId(R.id.Dates);
        } else if (fragment instanceof ChatsFragment)
        {
            displayLoadedFrg(3,null);
            bottomNavigationView.setSelectedItemId(R.id.Chats);
        }else if (fragment instanceof MakeMatchFrag)
        {
            displayLoadedFrg(4,null);
            bottomNavigationView.setSelectedItemId(R.id.settings);
        } else
        {
            this.finish();
        }
    }

    @Override
    public void updateInterNetStatus(boolean status)
    {
        if(noInterNetView!=null)
        {
            if(status)
            {
                noInterNetView.internetFound();
            }else
            {
                noInterNetView.showNoInternet();
            }
        }
    }

    @Override
    public void openProfile(Intent intent) {
        activity.startActivityForResult(intent,AppConfig.PROFILE_REQUEST);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @Override
    public void launchCampaignScreen(Intent intent1) {
        startActivity(intent1);
    }


    @Override
    public void showCoinBalance(String coinBalance) {
        if(discoveryFrg != null)
            discoveryFrg.showCoinBalance(coinBalance);
        if(prospectsFrg != null)
            prospectsFrg.showCoinBalance(coinBalance);
        if(datesFrg != null)
            datesFrg.showCoinBalance(coinBalance);
        if(matchesFragment != null)
            matchesFragment.showCoinBalance(coinBalance);
        if(makeMatchFrag != null)
            makeMatchFrag.showCoinBalance(coinBalance);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(!InnAppSdk.getVendor().onActivity_result(requestCode,resultCode,data))
        {
            if(!presenter.onHandelActivityResult(requestCode,resultCode,data))
            {

                if(AppConfig.SEARCH_REQUEST==requestCode&&resultCode==Activity.RESULT_OK)
                {
                    displayLoadedFrg(0,data.getExtras());
                    bottomNavigationView.setSelectedItemId(R.id.discover);
                    discoveryFrg.onActivityResult(requestCode,resultCode,data);
                }else if(requestCode== Location_service.REQUEST_CHECK_SETTINGS)
                {
                    discoveryFrg.onActivityResult(requestCode,resultCode,data);
                }
                else
                {
                    super.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

    @Override
    public void launchCoinWallet() {
        Intent intent = new Intent(activity,AddCoinActivity.class);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @Override
    public void showBoostViewCounter(boolean show) {
        if(discoveryFrg != null)
            discoveryFrg.showBoostViewCounter(show);
    }

    @Override
    public void startCoinAnimation() {
        if(discoveryFrg != null)
            discoveryFrg.startCoinAnimation();
    }


    //from match dialog
    public void setNeedToUpdateChat(boolean needToUpdateChat) {
        presenter.setNeedToUpdateChat(needToUpdateChat);
    }
    @Override
    public void launchAppSettingsPage() {
        Intent intent = new Intent(activity, AppSettingsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(intent);
//        activity.overridePendingTransition(R.anim.bottom_slide_in,R.anim.bottom_slide_out);
    }
}