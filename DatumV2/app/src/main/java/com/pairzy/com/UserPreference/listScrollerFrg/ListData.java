package com.pairzy.com.UserPreference.listScrollerFrg;

/**
 * Created by ankit on 4/5/18.
 */

class ListData {
    String option;
    boolean isSelected = false;

    public ListData(String option, boolean isSelected) {
        this.option = option;
        this.isSelected = isSelected;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getOption() {
        return option;
    }

    public boolean isSelected() {
        return isSelected;
    }
}
