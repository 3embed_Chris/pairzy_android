package com.pairzy.com.googleLocationSearch.model.placeDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 23/5/18.
 */

public class PlaceResult {
    @SerializedName("geometry")
    @Expose
    private PlaceGeometry geometry;

    public PlaceGeometry getGeometry() {
        return geometry;
    }

    public void setGeometry(PlaceGeometry geometry) {
        this.geometry = geometry;
    }
}
