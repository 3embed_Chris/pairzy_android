package com.pairzy.com.MySearchPreference;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.pairzy.com.R;
/**
 * @since  3/8/2018.
 */
public class AnimatorHandler
{
    private Context context;

    AnimatorHandler(Context context)
    {
        this.context=context;
    }

    public Animation getFadScaleUp()
    {
        return AnimationUtils.loadAnimation(context, R.anim.fad_in_animation);
    }

    public Animation getScaleUp()
    {
        return AnimationUtils.loadAnimation(context, R.anim.scal_center_up);
    }

    public Animation getScaleDown()
    {
        return AnimationUtils.loadAnimation(context,R.anim.scal_center_down);
    }

}
