package com.pairzy.com.photoVidPreview;

import javax.inject.Inject;

public class PhotoVidPresenter implements PhotoVidContract.Presenter{

    @Inject
    PhotoVidContract.View view;

    @Inject
    public PhotoVidPresenter() {
    }

    @Override
    public void takeView(PhotoVidContract.View view) {
        //already injected
    }

    @Override
    public void dropView() {
        this.view =  null;
    }
}
