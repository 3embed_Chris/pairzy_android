package com.pairzy.com.MqttManager;

/**
 * @since  3/9/2018.
 */

public class MqttConStatus
{
    private boolean isCOnnected=false;
    private String message;
    public boolean isCOnnected() {
        return isCOnnected;
    }

    public void setCOnnected(boolean COnnected) {
        isCOnnected = COnnected;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

