package com.pairzy.com.home.Prospects.SuperLikeMe;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.pairzy.com.dagger.FragmentScoped;
import com.pairzy.com.home.Prospects.SuperLikeMe.Model.SuperLikedMeItemPojo;
import com.pairzy.com.home.Prospects.SuperLikeMe.Model.SuperLikedMeUserAdapter;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;
import dagger.Module;
import dagger.Provides;
/**
 * @since   3/23/2018.
 * @author 3Embed.
 * @version 1.0
 */
@Module
public class SuperLikeMeUtil
{
    @Provides
    @FragmentScoped
    ArrayList<SuperLikedMeItemPojo> getList()
    {
        return new ArrayList<>();
    }
    @Provides
    @FragmentScoped
    SuperLikedMeUserAdapter getUserListAdapter(ArrayList<SuperLikedMeItemPojo> list, TypeFaceManager typeFaceManager, Utility utility)
    {
        return new SuperLikedMeUserAdapter(list,typeFaceManager,utility);
    }

    @Provides
    @FragmentScoped
    LinearLayoutManager getLinearLayoutManager(Activity activity)
    {
        return new LinearLayoutManager(activity);
    }
}
