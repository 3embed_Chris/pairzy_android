package com.pairzy.com.util.Exception;
/**
 * @since  4/27/2018.
 */
public class NoMoreDataException extends Exception
{
    public NoMoreDataException() {
        super();
    }

    public NoMoreDataException(String message) {
        super(message);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
