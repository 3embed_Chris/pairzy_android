package com.pairzy.com.passportLocation;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.text.TextUtils;

import com.pairzy.com.R;
import com.pairzy.com.data.source.PassportLocationDataSource;
import com.pairzy.com.locationScreen.locationMap.CustomLocActivity;
import com.pairzy.com.locationScreen.model.LocationHolder;
import com.pairzy.com.passportLocation.model.PassportAdapter;
import com.pairzy.com.passportLocation.model.PassportLocation;
import com.pairzy.com.passportLocation.model.PassportModel;
import com.pairzy.com.planDate.SelectedLocationHolder;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.CustomObserver.LocationObserver;
import com.pairzy.com.util.FeatureLocAlert.FeatureLocAlertCallback;
import com.pairzy.com.util.FeatureLocAlert.FeatureLocDialog;
import com.pairzy.com.util.LocationProvider.LocationApiCallback;
import com.pairzy.com.util.LocationProvider.LocationApiManager;
import com.pairzy.com.util.LocationProvider.Location_service;
import java.util.ArrayList;
import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;
import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
/**
 * <h>PassportPresenter class</h>
 *
 * @author 3Embed.
 * @since 22/5/18.
 * @version 1.0.
 */
public class PassportPresenter implements PassportContract.Presenter , App_permission.Permission_Callback,Location_service.GetLocationListener,PassportAdapter.OnItemClickListener,FeatureLocAlertCallback
{
    private  final String LOCATION_TAG="location_tag";
    @Inject
    PassportContract.View view;
    @Inject
    App_permission app_permission;
    @Inject
    Activity activity;
    @Inject
    Location_service location_service;
    @Inject
    LocationApiManager locationApiManager;
    @Inject
    PassportModel model;
    @Inject
    PassportLocationDataSource locationDataSource;
    @Inject
    FeatureLocDialog featureLocDialog;
    @Inject
    LocationObserver locationObserver;
    private LocationHolder locationHolder;
    private CompositeDisposable compositeDisposable;
    private PassportLocation currentLocation;

    @Inject
    PassportPresenter()
    {
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void init()
    {
        if(view != null)
        {
            view.initView();
            view.applyFont();
            view.setAdapterListener(this);
        }
    }

    @Override
    public void checkForCurrentLocation()
    {
        currentLocation = model.parseCurrentLocation();
        if(currentLocation == null)
            askLocationPermission();
        else
        {
            showCurrentLocation(currentLocation.getSubLocationName());
        }
    }

    /*
     * getting user location from GPS
     * */
    private void getUserLocationFromService()
    {
        location_service.setListener(this);
        location_service.checkLocationSettings();
    }

    @Override
    public void askLocationPermission()
    {
        if(view != null)
            view.showCurrentLocationLoading(true);
        ArrayList<App_permission.Permission> permissions =new ArrayList<>();
        permissions.add(App_permission.Permission.LOCATION);
        app_permission.getPermission(LOCATION_TAG,permissions,this);
    }

    @Override
    public void onPermissionGranted(boolean isAllGranted, String tag) {
        if (tag.equalsIgnoreCase(LOCATION_TAG) && isAllGranted) {
            getUserLocationFromService();
        }
    }

    @Override
    public void onPermissionDenied(ArrayList<String> deniedPermission, String tag) {
        if(tag.equals(LOCATION_TAG))
        {
            String[] stringArray = deniedPermission.toArray(new String[0]);
            app_permission.show_Alert_Permission(activity.getString(R.string.location_access_text),activity.getString(R.string.location_acess_subtitle),
                    activity.getString(R.string.location_acess_message),stringArray);
        }
    }

    @Override
    public void onPermissionRotation(ArrayList<String> rotationPermission, String tag) {
        String[] permission = rotationPermission.toArray(new String[0]);
        app_permission.ask_permission_rotational(permission);
    }

    @Override
    public void onPermissionPermanent_Denied(String tag, boolean parmanent) {
        if(tag.equals(LOCATION_TAG))
        {
            getUserLocationFromApi();
        }
    }


    /*
     * getting user location from link*/
    private void getUserLocationFromApi()
    {
        locationApiManager.getLocation(new LocationApiCallback()
        {
            @Override
            public void onSuccess(Double lat, Double lng, double accuracy)
            {
                if(view != null)
                    view.showCurrentLocationLoading(false);
                LocationHolder holder = new LocationHolder();
                holder.setLatitude(lat);
                holder.setLongitude(lng);
                locationHolder = holder;
                currentLocation = model.getLocationName(locationHolder);
                if(currentLocation != null)
                    showCurrentLocation(currentLocation.getLocationName());

            }

            @Override
            public void onError(String error)
            {
                if(view != null)
                    view.showCurrentLocationLoading(false);
                if(view!=null)
                    view.showError(error);
            }
        });
    }


    @Override
    public void updateLocation(Location location) {
        location_service.stop_Location_Update();
        LocationHolder holder = new LocationHolder();
        holder.setLatitude(location.getLatitude());
        holder.setLongitude(location.getLongitude());
        locationHolder = holder;
        this.currentLocation = model.getLocationName(locationHolder);
        if(currentLocation != null)
            showCurrentLocation(currentLocation.getSubLocationName());
    }

    /*
     * show current location to ui
     */
    private void showCurrentLocation(String currentLocation)
    {
        if(view!=null)
        {
            view.showCurrentLocationLoading(false);
            view.showCurrentUserLocation(currentLocation);
        }
    }


    @Override
    public void location_Error(String error)
    {
        getUserLocationFromApi();
    }

    @Override
    public void onItemClose(int position)
    {
        boolean success = model.removeItem(position);
        if(!success)
        {
            if(view != null)
                view.showError(activity.getString(R.string.failed_on_remove));
        }
        model.notifyAdapter();
        if(model.getListSize()<1)
        {
            if(view!=null)
                view.onListExist(false);
        }
    }

    @Override
    public void onItemChecked(int position)
    {
        if(model.isSelectionChanged(position))
        {
            model.handleItemSelection(position);
        }
        model.notifyAdapter();
        if(view!=null)
            view.selectCurrent(false);
    }

    @Override
    public void handleOnActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(CustomLocActivity.CHOOSE_LOC_REQ_CODE==requestCode)
        {
            parseLocationFromMap(resultCode,data);

        }else if(requestCode == PassportActivity.LOCATION_REQ_CODE)
        {
            parseLocation(requestCode,resultCode,data);
        } else if(resultCode == RESULT_CANCELED && requestCode == Location_service.REQUEST_CHECK_SETTINGS){
            if(view != null)
                view.finishActivity();
        }
        else if(resultCode == RESULT_OK && requestCode == Location_service.REQUEST_CHECK_SETTINGS)
        {
            getUserLocationFromService();
        }
    }



    private void parseLocation(int requestCode, int resultCode, Intent data)
    {
        if(resultCode  == RESULT_OK && requestCode == PassportActivity.LOCATION_REQ_CODE)
        {
            PassportLocation passportLocation = (PassportLocation) data.getSerializableExtra("passport_location");
            if (passportLocation != null)
            {
                model.addItemToList(passportLocation);
                model.notifyAdapter();
                if(view!=null)
                    view.onListExist(true);
            }
        }
    }

    private void parseLocationFromMap(int resultCode, Intent data)
    {
        if(resultCode  == RESULT_OK)
        {
            SelectedLocationHolder locationHolder=(SelectedLocationHolder) data.getSerializableExtra("location_holder");
            PassportLocation passportLocation = new PassportLocation();
            String placeDetails=locationHolder.getLocationTitle();
            String title="",address="";
            try {
                if (!TextUtils.isEmpty(placeDetails)) {
                    if (placeDetails.contains(",")) {
                        title = placeDetails.substring(0, placeDetails.indexOf(","));
                        address = placeDetails.substring(placeDetails.indexOf(",") + 1, placeDetails.length());
                    } else {
                        title = placeDetails;
                        address = placeDetails;
                    }
                }
                passportLocation.setLocationName(title);
                passportLocation.setSubLocationName(address);
            }catch (Exception e){
                passportLocation.setLocationName(placeDetails);
                passportLocation.setSubLocationName(placeDetails);
            }
            passportLocation.setLatitude(locationHolder.getLatitude());
            passportLocation.setLongitude(locationHolder.getLongitude());
            model.addItemToList(passportLocation);
            model.notifyAdapter();
            if(view!=null)
                view.onListExist(true);
        }
    }


    @Override
    public void loadSavedLocations()
    {
        ArrayList<String> locationArray= locationDataSource.getPassportLocations();
        model.parsePassportLocationList(locationArray);
        model.notifyAdapter();
        boolean isExist=model.getListSize()>0;
        if(view!=null)
            view.onListExist(isExist);

        if(view!=null)
            view.selectCurrent(!locationDataSource.isFeaturesLocActive());
    }

    @Override
    public void saveLocations(boolean isCurrent)
    {
        model.savePassportList();
        if(currentLocation != null)
        {
            model.saveCurrentLocation(currentLocation);
        }

        if(isCurrent)
        {
            locationDataSource.setFeaturedLcoationActive(false);
            model.saveFeatureLocation(new PassportLocation());
            locationObserver.publishData(model.parseCurrentLocation());
        }else
        {
            PassportLocation location=model.getFeatureFromList();
            String addressName = location.getLocationName();
            String subAddressName = location.getSubLocationName();
            if(addressName == null)
                addressName = "";
            if(subAddressName == null)
                subAddressName = "";
            if(!subAddressName.contains(addressName)){
                location.setSubLocationName(addressName+((subAddressName.isEmpty())?"":","+subAddressName));
            }
            locationDataSource.setFeaturedLcoationActive(true);
            model.saveFeatureLocation(location);
            locationObserver.publishData(location);
        }
    }

    @Override
    public void onCurretnSelected()
    {
        model.unSelectAll();
    }


    @Override
    public void setLocationListener()
    {
        location_service.setListener(this);
    }

    public void dropLocationListener()
    {
        location_service.setListener(null);
    }
    @Override
    public void dispose()
    {
        compositeDisposable.clear();
    }
    /*
     * feature location alert callback.
     */
    @Override
    public void onOk() {
    }

    /*
     * feature location alert callback.
     */
    @Override
    public void onCancel() {
        if(view != null) {
            view.finishActivity();
        }
    }
}
