package com.pairzy.com.util.frameviedoview;

public enum ImplType {
    TEXTURE_VIEW,
    VIDEO_VIEW
}
