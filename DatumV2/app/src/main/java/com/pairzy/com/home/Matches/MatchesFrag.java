package com.pairzy.com.home.Matches;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.pairzy.com.R;
import com.pairzy.com.dublyCamera.CameraInFragments.CameraInFragmentsActivity;
import com.pairzy.com.home.HomeContract;
import com.pairzy.com.home.Matches.PostFeed.PostFeedFrag;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.TypeFaceManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

public class MatchesFrag extends DaggerFragment implements MatchesContract.View{

    private static final String TAG = MatchesFrag.class.getSimpleName();

    @Inject
    PostFeedFrag postFeedFrag;

    @BindView(R.id.title_text)
    TextView title_text;
    @BindView(R.id.matches_tab_layout)
    TabLayout matchesTabLayout;
    @BindView(R.id.matchesViewpager)
    ViewPager matchesViewpager;
    @BindView(R.id.parent_layout)
    CoordinatorLayout parent_layout;
    TextView tvCount;
    @BindView(R.id.tv_coin_balance)
    TextView tvCoinBalance;
    @BindView(R.id.ibPostListCreatePost)
    AppCompatButton btnPost;

    @Named(MatchesUtilModule.MATCHES_FRAG_LIST)
    @Inject
    ArrayList<Fragment> fragmentList;

    @Inject
    Activity activity;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    HomeContract.Presenter main_presenter;

    @Inject
    MatchesContract.Presenter presenter;

    private MatchesAdapter matchesAdapter;
    private FragmentManager fragmentManager;
    private Unbinder unbinder;
    private String title[];

    public MatchesFrag() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_matches_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder= ButterKnife.bind(this,view);
        presenter.takeView(this);
        initUI();
    }

    /*
     * initialization of the xml content.*/
    private void initUI()
    {
        matchesTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(btnPost != null) {
                    if (tab.getPosition() == 1) {
                        if(postFeedFrag != null){
                            postFeedFrag.onRefresh();
                        }
                        btnPost.animate().scaleX(1).scaleY(1).setDuration(200).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                btnPost.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        }).start();
                    } else {

                        if(postFeedFrag != null){
                            postFeedFrag.pausePlayer();
                        }

                        btnPost.animate().scaleX(0).scaleY(0).setDuration(200).setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                if(btnPost != null)
                                    btnPost.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        }).start();
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        btnPost.setTypeface(typeFaceManager.getCircularAirBook());
        tvCoinBalance.setTypeface(typeFaceManager.getCircularAirLight());
        title_text.setTypeface(typeFaceManager.getCircularAirBold());

        title = getResources().getStringArray(R.array.matches_tab_titles);
        fragmentManager = getChildFragmentManager();
        matchesAdapter = new MatchesAdapter(fragmentManager,fragmentList);
        matchesAdapter.notifyDataSetChanged();

        matchesViewpager.setOffscreenPageLimit(2);
        matchesViewpager.setAdapter(matchesAdapter);
        matchesTabLayout.setupWithViewPager(matchesViewpager);

        if(main_presenter.isFeedtabNeedToOpen()) {
            matchesViewpager.setCurrentItem(1);
            main_presenter.setFeedTabOpen(false);
        }

        ViewGroup vg = (ViewGroup) matchesTabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(typeFaceManager.getCircularAirBold());
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
        try{
            setupTabCustomView();
        }catch (Exception ignored){}
    }

    @OnClick(R.id.ibPostListCreatePost)
    void createPost() {
        Intent intent=new Intent(getContext(), CameraInFragmentsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(intent,AppConfig.POST_ACTIVITY_REQ_CODE);
    }
    @Override
    public void onResume() {
        super.onResume();
        main_presenter.updateCoinBalance();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: Called");
    }

    private void setupTabCustomView() {
        matchesAdapter.notifyDataSetChanged();
        for (int i = 0; i < matchesAdapter.getCount(); i++) {
            TabLayout.Tab tab = matchesTabLayout.getTabAt(i);
            tab.setCustomView(getTabView(i));
        }
    }

    private View getTabView(int i) {
        View tabView = LayoutInflater.from(matchesTabLayout.getContext()).inflate(R.layout.custom_date_tab_item,null);
        TextView tvTitle = tabView.findViewById(R.id.tv_tab_title);
        tvTitle.setText(title[i]);
        tvTitle.setTypeface(typeFaceManager.getCircularAirBold());
        if(i == 0) {
            tvCount = tabView.findViewById(R.id.tv_tab_count);
            tvCount.setVisibility(View.GONE);
            tvCount.setTypeface(typeFaceManager.getCircularAirBook());
        }
        else {
            TextView textViewCount = tabView.findViewById(R.id.tv_tab_count);
            textViewCount.setTypeface(typeFaceManager.getCircularAirBook());
            textViewCount.setVisibility(View.GONE);
        }
        return tabView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.dropView();
        unbinder.unbind();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }


    @OnClick(R.id.option_view)
    void onOptionClick() {
    }

    @OnClick(R.id.parent_layout)
    void onParentCLicked()
    {}


    @Override
    public void launchUserProfile(Intent intent) {
        startActivityForResult(intent, AppConfig.PROFILE_REQUEST);
    }

    @Override
    public void showCoinBalance(String coinBalance) {
        if(tvCoinBalance != null)
            tvCoinBalance.setText(coinBalance);
    }

    @Override
    public void showError(int id)
    {
        String message=getString(id);
        Snackbar snackbar = Snackbar
                .make(parent_layout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showError(String error)
    {
        Snackbar snackbar = Snackbar
                .make(parent_layout,""+error, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(String message)
    {
        Snackbar snackbar = Snackbar
                .make(parent_layout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK && requestCode == AppConfig.POST_ACTIVITY_REQ_CODE){
            if(postFeedFrag != null)
                postFeedFrag.updatePostfeedList();
        }
    }

    @OnClick(R.id.coin_view)
    void onCoinView()
    {
        main_presenter.launchCoinWallet();
    }


    public void pausePlayer() {
        if(postFeedFrag != null)
            postFeedFrag.pausePlayer();
    }

}
