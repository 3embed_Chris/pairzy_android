package com.pairzy.com.MyProfile.editName;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 27/4/18.
 */

@Module
public abstract class EditNameModule {

    @ActivityScoped
    @Binds
    abstract Activity editNameActivity(EditNameActivity activity);

    @ActivityScoped
    @Binds
    abstract EditNameContract.View editNameView(EditNameActivity activity);

    @ActivityScoped
    @Binds
    abstract EditNameContract.Presenter editNamePresenter(EditNamePresenter presenter);
}
