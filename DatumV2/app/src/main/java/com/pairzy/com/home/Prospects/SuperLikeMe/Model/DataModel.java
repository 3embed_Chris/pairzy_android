package com.pairzy.com.home.Prospects.SuperLikeMe.Model;

import android.net.Uri;
import com.pairzy.com.BaseModel;
import com.pairzy.com.home.Prospects.SuperLikeMe.SuperLikeMePresenter;
import com.pairzy.com.util.Exception.EmptyData;
import com.pairzy.com.util.Utility;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
/**
 * <h2>MyLikesDataModel</h2>
 * <P>
 *     ListModel class for the super like user details.
 * </P>
 *@since 3/27/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class DataModel extends BaseModel
{
    @Inject
    Utility utility;
    @Inject
    SuperLikedMeUserAdapter adapter;

    @Inject
    ArrayList<SuperLikedMeItemPojo> userList;
    private boolean isServerLoadActive;

    @Inject
    DataModel(){}

    /*
     * Parsing the data from the server*/
    public void parseData(String response) throws Exception
    {
        try
        {
            SuperLikeMeResponseHolder result_data=utility.getGson().fromJson(response,SuperLikeMeResponseHolder.class);
            ArrayList<SuperLikedMeItemPojo> temp=result_data.getData();
            if(temp!=null&&temp.size()>0)
            {
                int prefatch_pos=0;
                if(SuperLikeMePresenter.PAGE_COUNT==0)
                {
                    userList.clear();
                }else
                {
                    removeLoadMoreItem();
                    prefatch_pos=userList.size()-1;
                }
                isServerLoadActive=(temp.size()==SuperLikeMePresenter.LIMIT);
                userList.addAll(temp);
                adapter.notifyDataSetChanged();
                prefetchImage(prefatch_pos);
            }else
            {
                throw new Exception("CoinData is empty!");
            }
        }catch (Exception e)
        {
            throw new Exception(e.getMessage());
        }
    }

    /*
     * Remove load more item*/
    private void removeLoadMoreItem()
    {
        try
        {
            int index=userList.size()-1;
            if(index>=0)
            {
                SuperLikedMeItemPojo loading_item=userList.get(index);
                if(loading_item.isLoading())
                {
                    userList.remove(index);
                    adapter.notifyDataSetChanged();
                }
            }

        }catch (Exception e ) {}
    }


    public boolean handelLoadMore()
    {
        if(SuperLikeMePresenter.PAGE_COUNT!=0&&userList.size()>0)
        {
            try
            {
                int index=userList.size()-1;
                if(index>=0)
                {
                    SuperLikedMeItemPojo loading_item=userList.get(index);
                    if(!loading_item.isLoading())
                    {
                        SuperLikedMeItemPojo loading=new SuperLikedMeItemPojo();
                        loading.setLoading(true);
                        loading.setLoadingFailed(false);
                        userList.add(loading);
                        adapter.notifyDataSetChanged();
                    }
                }

            }catch (Exception e ) {}
            return true;
        }

        return false;
    }

    /**
     * Handling the loadmore on the page
     */
    public boolean handelLoadMoreError()
    {
        if(SuperLikeMePresenter.PAGE_COUNT!=0&&userList.size()>0)
        {
            try
            {
                int index=userList.size()-1;
                if(index>=0)
                {
                    SuperLikedMeItemPojo loading_item=userList.get(index);
                    if(loading_item.isLoading())
                    {
                        userList.remove(index);
                        adapter.notifyDataSetChanged();
                    }
                }
            }catch (Exception e ) {}
            return true;
        }
        return false;
    }

    /*
     *Checking load more call is required */
    public boolean checkLoadMoreRequired(int position)
    {
        try
        {
            int size=userList.size();
            int thrashHold=size-5;
            if(isServerLoadActive&&thrashHold>0&&position>thrashHold)
            {
                SuperLikedMeItemPojo loading_item=userList.get(size-1);
                return !loading_item.isLoading();
            }else
            {
                return false;
            }
        }catch (Exception e){
            return false;
        }
    }

    /**
     *Method is controller for the image pre fetch.
     * @param position contains the position from where
     *               we start prefatch.*/
    public void prefetchImage(int position)
    {
        if(position<0)
        {
            position=0;
        }
        int end_position=position+8;
        if(end_position>userList.size())
        {
            end_position=userList.size();
        }

        try
        {
            List<SuperLikedMeItemPojo> temp_list=userList.subList(position,end_position);
            for(SuperLikedMeItemPojo item:temp_list)
            {
                if(!item.isLoading())
                {
                    prefetch_Image(item.getProfilePic());
                    ArrayList<String> otherImage=item.getOtherImages();
                    if(otherImage!=null&&otherImage.size()>0)
                    {
                        for(String url:otherImage)
                        {
                            prefetch_Image(url);
                        }
                    }
                }
            }
        }catch (Exception e){}

        int start=position-8;
        if(start<0)
        {
            start=0;
        }

        try
        {
            if(start<position)
            {
                List<SuperLikedMeItemPojo> temp_list=userList.subList(start,position);
                for(SuperLikedMeItemPojo item:temp_list)
                {
                    if(!item.isLoading())
                    {
                        prefetch_Image(item.getProfilePic());
                        ArrayList<String> otherImage=item.getOtherImages();
                        if(otherImage!=null&&otherImage.size()>0)
                        {
                            for(String url:otherImage)
                            {
                                prefetch_Image(url);
                            }
                        }
                    }
                }
            }
        }catch (Exception e){}
    }

    /**
     * It prefetch the images for loaded item
     * @see Fresco#getImagePipeline()
     */
    private void prefetch_Image(String pic_url)
    {
        ImagePipeline pipeline = Fresco.getImagePipeline();
        Uri mainUri = Uri.parse(pic_url);
        ImageRequest profilePictureRequest = ImageRequestBuilder
                .newBuilderWithSource(mainUri)
                .setResizeOptions(new ResizeOptions(54, 54))
                .build();
        pipeline.prefetchToDiskCache(profilePictureRequest, null);
    }


    /*
     * User details*/
    public String getUserDetails(int position) throws EmptyData
    {
        try
        {
            SuperLikedMeItemPojo temp=userList.get(position);
            if(temp!=null)
            {
                return utility.getGson().toJson(temp);
            }else
            {
                throw new EmptyData("CoinData is empty!");
            }
        }catch (Exception e){
            throw new EmptyData("CoinData is empty!");
        }
    }

    /*
    * Changing the user matched status*/
    public void changeTheStatus(String userId)
    {
        try
        {
            for(SuperLikedMeItemPojo itemPojo:userList)
            {
                if(!itemPojo.isLoading())
                {
                    if(itemPojo.getOpponentId().equals(userId))
                    {
                        itemPojo.setMatched(true);
                    }
                }
            }
        }catch (Exception e)
        {}
    }
}
