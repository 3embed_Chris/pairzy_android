package com.pairzy.com.UserPreference.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
/**
 * @since  4/27/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class PrefranceDataDetails
{
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("data")
    @Expose
    private ArrayList<PreferenceItem> data = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<PreferenceItem> getData() {
        return data;
    }

    public void setData(ArrayList<PreferenceItem> data)
    {
        this.data = data;
    }
}
