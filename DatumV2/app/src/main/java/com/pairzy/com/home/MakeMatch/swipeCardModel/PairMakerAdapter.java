package com.pairzy.com.home.MakeMatch.swipeCardModel;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pairzy.com.R;
import com.pairzy.com.home.MakeMatch.model.MakeMatchDataPOJO;
import com.pairzy.com.util.TypeFaceManager;

import java.util.ArrayList;

public class PairMakerAdapter extends RecyclerView.Adapter {

    private ArrayList<MakeMatchDataPOJO> arrayList;
    private TypeFaceManager typeFaceManager;
    private ItemViewCallBack callback;
    private AdapterItemCallback adapterItemCallback;


    public void setCallback(AdapterItemCallback adapterItemCallback){
        this.adapterItemCallback=adapterItemCallback;
    }

    public PairMakerAdapter( TypeFaceManager typeFaceManager, ArrayList<MakeMatchDataPOJO> arrayList) {
        this.arrayList=arrayList;
        this.typeFaceManager=typeFaceManager;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_make_match_item,parent,false);
        return new PairMakerViewHolder(view,adapterItemCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder1, int position) {

        PairMakerViewHolder holder= (PairMakerViewHolder) holder1;

            //todo init font family
            holder.userTwoName.setTypeface(typeFaceManager.getCircularAirBold());
            holder.userTwoAge.setTypeface(typeFaceManager.getCircularAirLight());
            holder.userOneName.setTypeface(typeFaceManager.getCircularAirBold());
            holder.userOneAge.setTypeface(typeFaceManager.getCircularAirLight());

            //todo binding data
            holder.userTwoName.setText(arrayList.get(position).getUser2Name());
            holder.userTwoAge.setText(arrayList.get(position).getUser2Age()+", "+arrayList.get(position).getUser2HeightInFeet());
            holder.userTwo.setImageURI(arrayList.get(position).getUser2ProfilePic());

            holder.userOneName.setText(arrayList.get(position).getUser1Name());
            holder.userOneAge.setText(arrayList.get(position).getUser1Age()+", "+arrayList.get(position).getUser1HeightInFeet());
            holder.userOne.setImageURI(arrayList.get(position).getUser1ProfilePic());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

}
