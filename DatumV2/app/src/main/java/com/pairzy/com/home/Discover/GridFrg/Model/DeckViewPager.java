package com.pairzy.com.home.Discover.GridFrg.Model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
/**
 * <h2>DeckViewPager</h2>
 * <P>
 *
 * </P>
 * @since  3/22/2018.
 * @version 1.0.
 * @author 3Embed.
 */
public class DeckViewPager extends ViewPager
{
    public DeckViewPager(Context context)
    {
        super(context);
    }

    public DeckViewPager(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event)
    {
        return false;
    }

    /*
     *Loading the next item. */
    public int moveNext()
    {
        PagerAdapter adp=getAdapter();
        if(adp!=null)
        {
            int item_count=adp.getCount();
            int move_position=getCurrentItem()+1;
            //Log.d("dj45", ""+move_position);
            if(move_position<item_count)
            {
                setCurrentItem(move_position);
                return move_position;
            }else
            {
                return -1;
            }
        }else
        {
            return -1;
        }
    }

    /*
     *Loading the next item. */
    public int movePrev()
    {
        int move_position=getCurrentItem()-1;
        if(move_position>=0)
        {
            //Log.d("dj45", ""+move_position);
            setCurrentItem(move_position);
            return move_position;
        }else
        {
            return -1;
        }
    }
}
