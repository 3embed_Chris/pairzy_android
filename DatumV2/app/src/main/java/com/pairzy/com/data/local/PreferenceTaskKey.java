package com.pairzy.com.data.local;

/**
 * <h>PreferenceTaskEntry</h>
 * @author 3Embed.
 * @since 11/14/2017.
 */
public class PreferenceTaskKey
{
    /* Inner class that defines the table contents */
    public interface PreferenceTaskEntry
    {
        String PREFERENCE_NAME = "DatumPrefs";
        String FIRST_NAME = "firstName";
        String COUNTRY_CODE = "countryCode";
        String MOBILE_NUMBER = "mobileNumber";
        String OTP = "otp";
        String EMAIL = "email";
        String DOB = "dob";
        String GENDER = "gender";
        String TOKEN="sessionToken";
        String IS_SIGNUP = "isSignUp";
        String PROFILE_PICTURE="profile_pic";
        String PUSH_TOKEN = "pushToken";
        String LOGGED_IN = "loggein";
        String USER_ID="id";
        String SEARCHPREF="searchPreference";
        String USER_AGE="userAge";
        String USER_PROFILE_VIDEO="profile_video";
        String USER_VIDEO_THUMBNAIL="video_thumbnail";
        String USER_VIDEO_HEIGHT="video_height";
        String USER_VIDEO_WIDTH="video_width";
        String USER_OTHER_IMAGES="other_images";
        String MY_DETAILS="my_details";
        String IS_MILE="is_mile";
        String IS_FACEBOOK="is_facebook";
        String MY_PREFERENCE="my_preference";
        String MY_ABOUTS="my_abouts";
        String MY_EDUCATION = "my_education";
        String MY_WORK_PLACE = "my_work_place";
        String MY_JOB = "my_education";
        String DATE_EVENTS = "date_event";
        String COIN_DIALOG_CHAT = "coin_dialog_chat";
        String COIN_DIALOG_SUPERLIKE = "coin_dialog_superlike";
        String COIN_DIALOG_DATE = "coin_dialog_date";
        String COIN_DIALOG_CALL_DATE = "coin_dialog_call_date";
        String COIN_DIALOG_VIDEO_DATE = "coin_dialog_video_date";
        String COIN_DIALOG_RESCHEDULE_DATE = "coin_dialog_reschedule_date";
        String SUBSCRIPTIONS = "subscriptions";
        String COIN_CONFIG = "coin_config";
        String IS_PASPORTED = "ispassported";
        String CARD_TUTORIAL_VIEW = "card_tutorial_view";
        String BOOST_EXPIRE_TIME = "boost_expire_time";
        String REMAINS_LIKE_COUNT = "remains_like_count";
        String REMAINS_REWIND_COUNT = "remains_rewind_count";
        String NEXT_LIKE_TIME = "_next_like_time";
        String BOOST_PROFILE_VIEW_COUNT = "boost_profile_view_count";
        String FIRST_SPLASH_LAUNCH = "first_splash_launch";
        String APPLICATION_KILLED = "applicationKilled";
        String INDEX_DOC_ID = "indexDoc";
        String CHAT_SYNCED = "chatSynced";
        String TIME_DELTA_REQUIRED = "deltaRequired";
        String TIME_DELTA = "timeDelta";
        String LAST_SEEN_TIME = "lastSeenTime";
        String ENABLE_LAST_SEEN = "enableLastSeen";
        String USER_ACTION_COUNT = "user_action_count";
        String APP_LANGUAGE_ISO = "app_language_iso";
        String APP_NEW_VERSION = "app_new_version";
        String NEW_VERSION_UPDATED_TIME = "new_version_update_time";
        String APP_UPDATE_MANDATORY = "spp_update_mandatory";
        String APP_UPDATE_DIALOG_SHOWED = "app_update_dialog_showed";
        String PROFILE_UPDATE_TYPE = "profile_update_type";
        String CLOUD_NAME = "cloud_name";
        String CLOUDINARY_API_KEY = "cloudinary_api_key";
        String CLOUDINARY_API_SECRET = "cloudinary_api_secret";
        String SWIPE_TUTORIAL_VIEW = "swipe_tutorial_view";
    }

    public interface PassportLocationEntry{
        String PASSPORT_LOCATIONS = "passport_locations";
        String FEATURE_LOCATION = "feature_location";
        String CURRENT_LOCATION = "current_location";
    }
}
