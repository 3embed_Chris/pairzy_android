package com.pairzy.com.MqttManager;

/**
 * Created by ankit on 7/9/18.
 */

public interface   MessageType {

    final String BOOST = "boost";
    final String MATCH = "match/user";
    final String UN_MATCH = "unMatch";
    final String DELETE_USER = "deleteUser";
    final String BANDED_USER = "bannedUser";
    final String PRO_USER = "proUser";
    final String NEW_LIKES = "newLikes";
    final String ADMIN_COIN = "AdminCoin";
    final String BLOCK = "block";
    final String UN_BLOCK = "unBlock";
}
