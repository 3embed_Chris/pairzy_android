package com.pairzy.com.MqttChat.Database;


/*
 * Created by moda on 09/01/17.
 */

interface Validator {

    void validate(Revision newRevision, ValidationContext context);

}
