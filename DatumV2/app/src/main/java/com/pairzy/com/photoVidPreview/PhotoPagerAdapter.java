package com.pairzy.com.photoVidPreview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.androidinsta.com.ImageData;
import com.pairzy.com.R;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;

public class PhotoPagerAdapter extends PagerAdapter {

    private ArrayList<ImageData> photoList;
    private Context context;

    public PhotoPagerAdapter(Context context, ArrayList<ImageData> photoList) {
        this.context = context;
        this.photoList = photoList;
    }


    @Override
    public int getCount() {
        return photoList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.photo_preview_item,container,false);
        SimpleDraweeView sdvImage = itemView.findViewById(R.id.sdv_image_view);
        sdvImage.setImageURI(photoList.get(position).getThumbnail());
        container.addView(itemView);
        return itemView;
        //return super.instantiateItem(container, position);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
        //super.destroyItem(container, position, object);
    }
}
