package com.pairzy.com.login;

import android.app.Activity;
import com.pairzy.com.dagger.ActivityScoped;
import dagger.Binds;
import dagger.Module;
/**
 * <h1>LoginDaggerModule</h1>
 * <p>
 *
 * </p>
 * @author 3Embed
 * @since 13/08/17
 */
@Module
public abstract class LoginDaggerModule
{
    @ActivityScoped
    @Binds
    abstract Activity provideActivity(LoginActivity loginActivity);

    @ActivityScoped
    @Binds
    abstract LoginContract.View provideView(LoginActivity splashActivity);

    @ActivityScoped
    @Binds
    abstract LoginContract.Presenter taskPresenter(LoginPresenter presenter);

}