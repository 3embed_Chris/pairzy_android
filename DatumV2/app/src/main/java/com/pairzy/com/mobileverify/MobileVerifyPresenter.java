package com.pairzy.com.mobileverify;
import android.os.Bundle;
import android.os.CountDownTimer;
import com.pairzy.com.mobileverify.Model.TimerCommunicator;
import com.pairzy.com.networking.NetworkStateHolder;
import com.pairzy.com.networking.RxNetworkObserver;
import com.pairzy.com.util.AppConfig;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import dagger.android.support.DaggerFragment;
import io.reactivex.Completable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
/**
 * <h2>PreferencePresenterImp</h2>
 * <p>
 * Implementation of
 * @{@link com.pairzy.com.mobileverify.MobileVerifyContract.ResendOtpPresenter}
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 06/01/2018.
 **/
public class MobileVerifyPresenter implements MobileVerifyContract.Presenter
{
    private CountDownTimer countDownTimer;
    private TimerCommunicator timerCommunicator;
    private boolean isTickingFinished =false;
    private CompositeDisposable compositeDisposable;
    @Inject
    NetworkStateHolder holder;
    @Inject
    RxNetworkObserver rxNetworkObserver;
    @Inject
    MobileVerifyContract.View view;
    @Inject
    MobileVerifyModel model;
    @Inject
    MobileVerifyPresenter() {
        compositeDisposable=new CompositeDisposable();
    }

    @Override
    public void moveFragment(DaggerFragment fragment, boolean isBaclrequired)
    {
        view.moveFragment(fragment,isBaclrequired);
    }

    @Override
    public void doSingUp (Bundle data)
    {
        Completable.timer(AppConfig.LOAD_SCREEN, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    if(view!=null)
                        view.openSignUP(data);
                });
    }


    @Override
    public void doLogin(Bundle data)
    {
        Completable.timer(AppConfig.LOAD_SCREEN, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    if(view!=null)
                        view.openHomePage(data);});
    }

    @Override
    public void showMessage(String message)
    {
        view.showMessage(message);
    }

    @Override
    public void openPhnoFrgAgain()
    {
        view.openMainPhnoAgainFrg();
    }

    /*
     * being call from result fragment.
     * passing edited phone back.
     */
    @Override
    public void setResultData(String phno) {
        if(view != null)
            view.setResultData(phno);
    }

    @Override
    public void initNetworkObserver()
    {
        view.updateInterNetStatus(holder.isConnected());
        Observer<NetworkStateHolder> observer = new Observer<NetworkStateHolder>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {
                compositeDisposable.add(d);
            }
            @Override
            public void onNext(NetworkStateHolder value)
            {
                view.updateInterNetStatus(value.isConnected());
            }
            @Override
            public void onError(Throwable e)
            {
                e.printStackTrace();
            }
            @Override
            public void onComplete()
            {}
        };
        rxNetworkObserver.getObservable().subscribeOn(Schedulers.newThread());
        rxNetworkObserver.getObservable().observeOn(AndroidSchedulers.mainThread());
        rxNetworkObserver.getObservable().subscribe(observer);
    }



    @Override
    public void setTimerListener(TimerCommunicator timerListener)
    {
        timerCommunicator=timerListener;
        if(isTickingFinished)
            timerCommunicator.onFinished();
    }

    @Override
    public void startTimer()
    {
        if(countDownTimer!=null)
            countDownTimer.cancel();

        isTickingFinished=false;

        countDownTimer = new CountDownTimer(25000,1000)
        {
            @Override
            public void onTick(long l)
            {
                isTickingFinished=false;
                if(l==0)
                {
                    if(timerCommunicator!=null)
                        timerCommunicator.onUpdateTime("00:01");
                }else if(l<10)
                {
                    if (timerCommunicator != null)
                        timerCommunicator.onUpdateTime(String.format(Locale.ENGLISH, "00:0%d", l / 1000));
                } else {
                    if (timerCommunicator != null)
                        timerCommunicator.onUpdateTime(String.format(Locale.ENGLISH, "00:%d", l / 1000));
                }
            }

            @Override
            public void onFinish()
            {
                isTickingFinished=true;
                if (timerCommunicator != null)
                    timerCommunicator.onFinished();
            }
        }.start();
    }

    @Override
    public void takeView(Object view) {}

    @Override
    public void dropView()
    {
        compositeDisposable.clear();
        if(countDownTimer!=null)
        {
            countDownTimer.cancel();
        }
    }
}
