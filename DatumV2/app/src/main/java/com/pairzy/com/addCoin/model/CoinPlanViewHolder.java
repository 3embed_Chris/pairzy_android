package com.pairzy.com.addCoin.model;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.home.Dates.Model.ItemActionCallBack;
import com.pairzy.com.util.TypeFaceManager;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * <h>CoinPlanViewHolder</h>
 * <p> shows the coin plan list.</p>
 * @author 3Embed.
 * @since 1/6/18.
 * @version 1.0.
 */
public class CoinPlanViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    @BindView(R.id.coin_plan_icon)
    ImageView ivCoinPlan;
    @BindView(R.id.coin_plan_title)
    TextView tvCoinPlanTitle;
    @BindView(R.id.coin_plan_button_text)
    TextView tvCoinPlanButtonText;
    @BindView(R.id.coin_plan_button_rl)
    RelativeLayout coinPlanButton;
    private TypeFaceManager typeFaceManager;
    private ItemActionCallBack callBack;
    public CoinPlanViewHolder(View itemView, ItemActionCallBack callBack, TypeFaceManager typeFaceManager) {
        super(itemView);
        this.callBack = callBack;
        this.typeFaceManager = typeFaceManager;
        ButterKnife.bind(this,itemView);
        tvCoinPlanTitle.setTypeface(typeFaceManager.getCircularAirBook());
        tvCoinPlanButtonText.setTypeface(typeFaceManager.getCircularAirBook());
        coinPlanButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(callBack != null)
            callBack.onClick(R.id.coin_plan_rl,this.getAdapterPosition());
    }
}
