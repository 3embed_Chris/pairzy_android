package com.pairzy.com.MqttChat.CameraView;

import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.pairzy.com.R;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import com.pairzy.com.AppController;

/**
 * Created by moda on 08/11/17.
 */

public class LocalCameraView extends AppCompatActivity {


    /*
     *To check for the camera view
     */

    private CameraView mCameraView;
    private Bus bus = AppController.getBus();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        } else {

            getWindow().getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                    );

        }

        setContentView(R.layout.activity_local_cameraview);
        mCameraView = (CameraView) findViewById(R.id.cameraView);


        mCameraView.setFacing(CameraView.FACING_FRONT);
        bus.register(this);
    }

    @Override
    protected void onPause() {


        mCameraView.stop();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mCameraView.start();

    }


    @Subscribe
    public void getMessage(JSONObject object) {

        try {

            if (object.getString("eventName").equals("hideCameraView")) {

                supportFinishAfterTransition();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {

try{
        if (AppController.getInstance().isActiveOnACall()) {
            if (AppController.getInstance().isCallMinimized()) {
                super.onBackPressed();
                supportFinishAfterTransition();
            }
        } else {
            super.onBackPressed();
            supportFinishAfterTransition();
        }
}catch(Exception e){e.printStackTrace();}
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

//        if (AppController.getInstance().isActiveOnACall()) {
//            try {
//                JSONObject obj = new JSONObject();
//
//                obj.put("eventName", "cameraReleased");
//
//
//            } catch (JSONException e) {
//
//            }
//        }

        bus.unregister(this);
    }

}
