package com.pairzy.com.UserPreference;

import android.app.Activity;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import com.pairzy.com.UserPreference.CanditionChoice.ConChoiceBuilder;
import com.pairzy.com.UserPreference.CanditionChoice.ConChoiceFrg;
import com.pairzy.com.UserPreference.EditInputFrg.UserInputFrg;
import com.pairzy.com.UserPreference.EditInputFrg.UserInputFrgBuilder;
import com.pairzy.com.UserPreference.PreviewMessage.PreviewFrg;
import com.pairzy.com.UserPreference.PreviewMessage.PreviewFrgBuilder;
import com.pairzy.com.UserPreference.listScrollerFrg.ListdataFrg;
import com.pairzy.com.UserPreference.listScrollerFrg.ListdataFrgBuilder;
import com.pairzy.com.dagger.ActivityScoped;
import com.pairzy.com.dagger.FragmentScoped;
import javax.inject.Named;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
/**<h2>MyPreferencePageBuilder</h2>
 * <P>
 *
 * </P>
 * @since  2/22/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public abstract class MyPreferencePageBuilder
{
    static final String ACTIVITY_FRAGMENT_MANAGER = "MyPrefrence.FragmentManager";

    @ActivityScoped
    @Binds
    abstract Activity provideActivity(MyPreferencePage myPreferencePage);

    @ActivityScoped
    @Binds
    abstract MyPreferencePageContract.View provideView(MyPreferencePage myPreferencePage);

    @ActivityScoped
    @Binds
    abstract MyPreferencePageContract.Presenter providePresenter(MyPreferencePagePresenter presenter);

    @Provides
    @Named(ACTIVITY_FRAGMENT_MANAGER)
    @ActivityScoped
    static FragmentManager activityFragmentManager(Activity activity)
    {
        return ((AppCompatActivity)activity).getSupportFragmentManager();
    }

    @FragmentScoped
    @ContributesAndroidInjector(modules = {ConChoiceBuilder.class})
    abstract ConChoiceFrg getConChoiceFrg();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {ListdataFrgBuilder.class})
    abstract ListdataFrg getListdataFrg();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {UserInputFrgBuilder.class})
    abstract UserInputFrg getUserInputFrg();

    @FragmentScoped
    @ContributesAndroidInjector(modules = {PreviewFrgBuilder.class})
    abstract PreviewFrg getPreviewFrg();


}
