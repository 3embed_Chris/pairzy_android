package com.pairzy.com.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ankit on 13/7/18.
 */

public class SubscriptionResponse
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private SubscriptionData data;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public SubscriptionData getData()
    {
        return data;
    }

    public void setData(SubscriptionData data) {
        this.data = data;
    }
}
