package com.pairzy.com.util.ReportUser;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

/**
 * <h2>ChatMenuDialog</h2>
 * <P>
 *
 * </P>
 * @since  4/10/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ReportUserDialog implements ReportAdapterCallback
{
    private Activity activity;
    private Dialog dialog;
    private TypeFaceManager typeFaceManager;
    private Utility utility;
    private ReportUserCallBack callBack;
    private ArrayList<String> reportReasonList;

    public ReportUserDialog(Activity activity, TypeFaceManager typeFaceManager, Utility utility)
    {
        this.activity=activity;
        this.typeFaceManager=typeFaceManager;
        this.utility=utility;
    }


    public void showDialog(ArrayList<String> reportReasonList, ReportUserCallBack callBack)
    {

        if(reportReasonList == null)
            return;
        this.reportReasonList = reportReasonList;
        this.callBack = callBack;
        if(dialog != null && dialog.isShowing())
        {
            dialog.cancel();
        }
        @SuppressLint("InflateParams")
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.Datum_AlertDialog);
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.user_report_dialog, null);
        builder.setView(dialogView);
        TextView tvReportUser =dialogView.findViewById(R.id.report_user);
        tvReportUser.setTypeface(typeFaceManager.getCircularAirBold());
        RecyclerView recyclerReportReason =dialogView.findViewById(R.id.recycler_report);
        recyclerReportReason.setHasFixedSize(true);
        recyclerReportReason.setLayoutManager(new LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false));

        ReportUserAdapter adapter = new ReportUserAdapter(this,reportReasonList,typeFaceManager);
        recyclerReportReason.setAdapter(adapter);

        Button onCanceled=dialogView.findViewById(R.id.onCanceled);
        onCanceled.setTypeface(typeFaceManager.getCircularAirBook());
        onCanceled.setOnClickListener(view -> {
            if(dialog!=null)
                dialog.cancel();
        });
        dialog = builder.create();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();
    }

    @Override
    public void onAdapterClick(int position) {
        if(callBack != null)
            callBack.onReportReasonSelect(reportReasonList.get(position));
        if(dialog!=null)
            dialog.cancel();
    }
}
