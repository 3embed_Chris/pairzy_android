package com.pairzy.com.home.Prospects;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.pairzy.com.home.Prospects.LikesMe.LikesMeFrg;
import com.pairzy.com.home.Prospects.MyLikes.MyLikesFrg;
import com.pairzy.com.home.Prospects.MySuperlikes.MySuperlikesFrg;
import com.pairzy.com.home.Prospects.Online.Online_frg;
import com.pairzy.com.home.Prospects.Passed.PassedFrg;
import com.pairzy.com.home.Prospects.RecentVisitors.RecentVisitorsFrg;
import com.pairzy.com.home.Prospects.SuperLikeMe.SuperLikeMeFrg;
/**
 * @since  3/17/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class ProspectsAdapter extends FragmentStatePagerAdapter
{
    private String tile_list[];
    public ProspectsAdapter(FragmentManager fm,String[] tile_list)
    {
        super(fm);
        this.tile_list = tile_list;
    }

    @Override
    public Fragment getItem(int position)
    {
        Fragment fragment;
        switch (position)
        {
            case 0:
                fragment=new Online_frg();
                break;
            case 1:
                fragment=new SuperLikeMeFrg();
                break;
            case 2:
                fragment=new LikesMeFrg();
                break;
            case 3:
                fragment=new RecentVisitorsFrg();
                break;
            case 4:
                fragment=new PassedFrg();
                break;
            case 5:
                fragment=new MyLikesFrg();
                break;
            case 6:
                fragment=new MySuperlikesFrg();
                break;
              default:
                  fragment=new Online_frg();

        }
        return fragment;
    }

    @Override
    public int getCount()
    {
        return tile_list.length;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position)
    {
        return tile_list[position];
    }
}
