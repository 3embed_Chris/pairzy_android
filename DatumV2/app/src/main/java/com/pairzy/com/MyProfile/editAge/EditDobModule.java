package com.pairzy.com.MyProfile.editAge;

import android.app.Activity;

import com.pairzy.com.dagger.ActivityScoped;

import dagger.Binds;
import dagger.Module;

/**
 * Created by ankit on 27/4/18.
 */

@Module
public abstract class EditDobModule {

    @ActivityScoped
    @Binds
    abstract Activity editDobActivity(EditDobActivity activity);

    @ActivityScoped
    @Binds
    abstract EditDobContract.View editDobView(EditDobActivity activity);

    @ActivityScoped
    @Binds
    abstract EditDobContract.Presenter editDobPresenter(EditDobPresenter presenter);
}
