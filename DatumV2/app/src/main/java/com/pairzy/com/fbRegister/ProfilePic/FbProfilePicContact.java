package com.pairzy.com.fbRegister.ProfilePic;
import android.net.Uri;

import com.pairzy.com.BasePresenter;
import com.pairzy.com.BaseView;
import com.facebook.drawee.view.SimpleDraweeView;

/**
 * <h2>FbProfileVideoContact</h2>
 * @since  2/19/2018.
 * @version 1.0.
 * @author 3Embed.
 */
public interface FbProfilePicContact
{
    interface Presenter extends BasePresenter
    {
        String getRecentCameraPic();
        String getTempPic();
        void setProfilePIc(String image_path, SimpleDraweeView imageView);
        void upDateToGallery();
        void openChooser();
        void showError(String message);
        void compressImage(String url);
        void uploadToCloudinary(String filePath,boolean firstTimeUpload);
        void openImagePreview(String imagePath);
        void getCloudinaryDetails(String filePath);
    }


    interface View extends BaseView
    {
        void onImageCompressed(String url_path);
        void openImageCropper(Uri uri);
        void imageUploaded(String image_url);
        void updateImageSet(String file_path);
        void imageCollectError();
        void openCamera(Uri uri);
        void moveFragment();
        void openGallery();
        void showError(String message);
        void showMessage(String messaage);

    }
}
