package com.pairzy.com.userProfile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.androidinsta.com.ImageData;
import com.androidinsta.com.InstagramManger;
import com.pairzy.com.MyProfile.Model.InstaMediaAdapter;
import com.pairzy.com.MyProfile.Model.MomentsData;
import com.pairzy.com.MyProfile.Model.OpenInstagram;
import com.pairzy.com.R;
import com.pairzy.com.addCoin.AddCoinActivity;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.momentGrid.MomentsGridActivity;
import com.pairzy.com.moments.MomentsActivity;
import com.pairzy.com.userProfile.Model.MomentsGridAdapter;
import com.pairzy.com.userProfile.Model.UserMediaAdapter;
import com.pairzy.com.util.AlphaForegroundColorSpan;
import com.pairzy.com.util.ApiConfig;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.CustomView.SquizeButton;
import com.pairzy.com.util.PullZoomView.PullToZoomScrollView;
import com.pairzy.com.util.SegmentProgressBar.SegmentedProgressBar;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.pairzy.com.util.localization.activity.BaseDaggerActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.suresh.innapp_purches.InnAppSdk;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Named;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * <h2>UserProfilePage</h2>
 * <P>
 *      User profile activity to show the user details.
 * </P>
 * @author 3Embed.
 * @version 1.0.
 * @since 27-03-2018.
 */

public class UserProfilePage extends BaseDaggerActivity implements UserProfileContract.View,OpenInstagram
{
    public static final String USER_DATA="user_Data";
    public static final String BUTTON_STATUS="button_status"; //0 show all, 1 hide like
    @Inject
    UserMediaAdapter pagerAdapter;
    @Inject
    TypeFaceManager typeFaceManager;
    @Inject
    UserProfileContract.Presenter presenter;
    @Inject
    Activity activity;
    @Inject
    Utility utility;
    @Inject
    InstagramManger instagramManger;
    @Inject
    PreferenceTaskDataSource dataSource;


    @Named(Profile_util.USER_PROFILE_MOMEMT)
    @Inject
    ArrayList<MomentsData> momentsData;
    @BindView(R.id.scroll_view)
    PullToZoomScrollView scrollView;

    LinearLayout updateProfileLl;
    public ViewPager viewPager;
    TextView itemsCountTv;
    SegmentedProgressBar segmentedProgressBar;
    TextView report_text;
    TextView recommend_text;
    TextView name_text;
    ImageView user_status;
    RelativeLayout school_view;
    RelativeLayout location_view;
    TextView school_name;
    TextView location_text;
    @BindView(R.id.dislike_view)
    SquizeButton dislike_view;
    @BindView(R.id.like_view)
    SquizeButton like_view;
    @BindView(R.id.superLike_view)
    SquizeButton super_like_view;
    @BindView(R.id.boost_view)
    SquizeButton boost_view;
    @BindView(R.id.mainContentContainer)
    RelativeLayout parentLayout;


    public static RelativeLayout actionBarLayout;
    public static TextView tvToolbarTitle;
    public static View toolbarShadow;
    public static ImageView ivBackButton;

    FloatingActionButton fabChat;
    private LinearLayout recomendView;
    private LinearLayout reportView;
    private ImageView school_img;
    private Unbinder unbinder;
    public static int pager_position=0;
    private String profileId;
    //public DatumVideoPlayer video_player;
    private TextView show_insta_pic,connect_insta,connect_instagram,momentsViewAll;
    private RelativeLayout instagram_button_viiew,inst_button_view;
    private RecyclerView insta_media_list,rvMoments;
    private RelativeLayout rlAboutView;
    private TextView tvAbout;
    private int button_status=0;
    private boolean fromChat = false;
    private boolean isOnlinne=false;
    private boolean isChatListNeedToUpdate = false;
    private static AlphaForegroundColorSpan alphaForegroundColorSpan;
    private static SpannableString spannableTitle;
    private int currentTabPosition = 0;

    TextView tvMomentsTitle;

    TextView tvViewAllMoments;

    LinearLayout llMoments;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_activity);
        unbinder = ButterKnife.bind(this);
        //video_player= DatumVideoPlayer.getNewInstance(this,dataSource);
        initUIData();
        profileId = getIntent().getStringExtra("profile_id");
        Bundle data= getIntent().getExtras();
        if(data!=null && data.containsKey(BUTTON_STATUS))
        {
            button_status=data.getInt(BUTTON_STATUS);
        }
        if(data!=null && data.containsKey("fromChat"))
        {
            fromChat = data.getBoolean("fromChat");
        }
        if(TextUtils.isEmpty(profileId))
        {
            presenter.checkIntent(getIntent());
            presenter.getUserDetails();
        }
        else{
            presenter.getUserDetails(profileId);
        }
        showChatIcon(fromChat);
        presenter.initListener();
        presenter.initDataChangeObserver();
    }

    private void showChatIcon(boolean fromChat) {
        if(fromChat){
            fabChat.setVisibility(View.GONE);
        }
        else
            fabChat.setVisibility(View.VISIBLE);
    }

    /*
     *Initialization of the required data */
    private void initUIData()
    {
        actionBarLayout = findViewById(R.id.action_bar_layout);
        tvToolbarTitle = findViewById(R.id.tv_toolbar_title);
        tvToolbarTitle.setTypeface(typeFaceManager.getCircularAirBold());
        toolbarShadow = findViewById(R.id.toolbar_shadow);
        ivBackButton = findViewById(R.id.iv_back_button);
        tvToolbarTitle.setTypeface(typeFaceManager.getCircularAirBold());
        spannableTitle = new SpannableString("");
        alphaForegroundColorSpan = new AlphaForegroundColorSpan(getResources().getColor(R.color.softDeepGray));
        final ColorDrawable cd = new ColorDrawable(ContextCompat.getColor(this, R.color.white));
        actionBarLayout.setBackground(cd);
        cd.setAlpha(0);
        //initial title alpha zeo
//        alphaForegroundColorSpan.setAlpha(0);
        spannableTitle.setSpan(alphaForegroundColorSpan, 0, spannableTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvToolbarTitle.setVisibility(View.GONE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
        DisplayMetrics localDisplayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
        int mScreenWidth = localDisplayMetrics.widthPixels;
        Resources r = getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 400, r.getDisplayMetrics());
        LinearLayout.LayoutParams localObject = new LinearLayout.LayoutParams(mScreenWidth, (int)px);
        scrollView.setHeaderLayoutParams(localObject);
        @SuppressLint("InflateParams")
        View headView = LayoutInflater.from(this).inflate(R.layout.user_profile_header, null, false);
        headView.findViewById(R.id.option_view).setOnClickListener(view -> presenter.showOptionAlert());
        name_text=headView.findViewById(R.id.name_text);
        user_status=headView.findViewById(R.id.user_status);
        fabChat = headView.findViewById(R.id.fab_chat);
        fabChat.setOnClickListener(view ->{
            openChat(view);
        });
        viewPager=headView.findViewById(R.id.viewPager);
        itemsCountTv=headView.findViewById(R.id.itemsCountTv);
        segmentedProgressBar=headView.findViewById(R.id.segmented_progressbar);
        @SuppressLint("InflateParams")
        View contentView = LayoutInflater.from(this).inflate(R.layout.user_profile_content, null, false);


        tvMomentsTitle=contentView.findViewById(R.id.tv_moment_title);
        tvViewAllMoments=contentView.findViewById(R.id.tv_view_all_moments);
        rvMoments=contentView.findViewById(R.id.rvMoments);
        tvMomentsTitle.setTypeface(typeFaceManager.getCircularAirBold());
        tvViewAllMoments.setTypeface(typeFaceManager.getCircularAirBook());
        tvViewAllMoments.setOnClickListener(v -> viewAllMoments());
        llMoments=contentView.findViewById(R.id.ll_moment);
        rlAboutView = contentView.findViewById(R.id.about_view);
        tvAbout = contentView.findViewById(R.id.about_text);
        tvAbout.setTypeface(typeFaceManager.getCircularAirLight());
        recomendView = contentView.findViewById(R.id.recomend_view);
        recomendView.setOnClickListener(this::recommendProfile);
        reportView = contentView.findViewById(R.id.report_view);
        reportView.setOnClickListener(this::reportProfile);
        show_insta_pic=contentView.findViewById(R.id.show_insta_pic);
        show_insta_pic.setTypeface(typeFaceManager.getCircularAirBold());
        instagram_button_viiew=contentView.findViewById(R.id.instagram_button_viiew);
        inst_button_view=contentView.findViewById(R.id.inst_button_view);
        connect_insta=contentView.findViewById(R.id.connect_insta);
        connect_insta.setTypeface(typeFaceManager.getCircularAirLight());
        connect_insta.setOnClickListener(v -> presenter.doInstagramLogin());
        connect_instagram=contentView.findViewById(R.id.connect_instagram);
        connect_instagram.setTypeface(typeFaceManager.getCircularAirBook());
        insta_media_list=contentView.findViewById(R.id.media_list);

        updateProfileLl=contentView.findViewById(R.id.updateProfileLl);
        report_text=contentView.findViewById(R.id.report_text);
        recommend_text=contentView.findViewById(R.id.recommend_text);
        school_view=contentView.findViewById(R.id.school_view);
        school_img= contentView.findViewById(R.id.school_img);
        school_name=contentView.findViewById(R.id.school_name);
        location_view=contentView.findViewById(R.id.location_view);
        location_text=contentView.findViewById(R.id.location_text);
//        chatView =  contentView.findViewById(R.id.chat_view);
//        chatView.setOnClickListener(this::openChat);
        scrollView.setZoomView(headView);
        contentView.setPadding(0,-25,0,0);
        scrollView.setScrollContentView(contentView);
        scrollView.setParallax(false);
        dislike_view.setClickListener(this::doDislike);
        like_view.setClickListener(this::doLike);
        super_like_view.setClickListener(this::doSuperLike);
        boost_view.setClickListener(this::openBoostDialog);
        name_text.setTypeface(typeFaceManager.getCircularAirBold());
        report_text.setTypeface(typeFaceManager.getCircularAirBold());
        recommend_text.setTypeface(typeFaceManager.getCircularAirBold());
        segmentedProgressBar.setContainerColor(ContextCompat.getColor(activity,R.color.black30));
        segmentedProgressBar.setFillColor(ContextCompat.getColor(activity,R.color.white));
        segmentedProgressBar.playSegment(50);
        viewPager.setAdapter(pagerAdapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0) {
                    if (pagerAdapter != null)
                        pagerAdapter.playVideo("onPageSelected()");
                }
                else{
                    if (pagerAdapter != null)
                        pagerAdapter.pauseVideo("onPageSelected()");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        //video_player.stopPlayer();
    }

    /**
     * <p>
     *     Here we set alpha value for action bar title(Product name)
     * </p>
     * @param alpha set value
     */
    public void setTitleAlpha(float alpha)
    {
        if(alpha<1) {
            alpha = 1;
        }

        if (alpha==1)
        {
            //handle video play/pause
            //handle player play pause as well on appscroll
            if(viewPager != null && viewPager.getCurrentItem() == 0) {
                if (pagerAdapter != null) {
                    pagerAdapter.pauseVideo("showToolbarTitle()");
                }
            }

            toolbarShadow.setVisibility(View.VISIBLE);
            ivBackButton.setImageResource(R.drawable.ic_back_svg_icon);
            //iV_option_menu.setImageResource(R.drawable.option_menu_icon);
        }
        else{
            if(viewPager != null && viewPager.getCurrentItem() == 0) {
                if (pagerAdapter != null) {
                    pagerAdapter.playVideo("showToolbarTitle()");
                }
            }
            toolbarShadow.setVisibility(View.GONE);
            ivBackButton.setImageResource(R.drawable.ic_white_svg_icon);
            actionBarLayout.setBackgroundResource(R.drawable.segment_progressbar_profile_bg);
            //iV_option_menu.setImageResource(R.drawable.white_option_menu_icon_with_shadow);
        }

        System.out.println("ProfilePage"+" "+"alpha value="+alpha);
        alphaForegroundColorSpan.setAlpha(alpha);
        spannableTitle.setSpan(alphaForegroundColorSpan, 0, spannableTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvToolbarTitle.setText(spannableTitle);
        tvToolbarTitle.setVisibility(View.VISIBLE);
    }

    private void reportProfile(View view) {
        presenter.reportProfile();
    }

    private void recommendProfile(View view) {
        presenter.shareProfile();
    }

    private void openChat(View view) {
        presenter.openChat();
    }

    private void openBoostDialog(){
        presenter.launchBoostDialog();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        try
        {
            presenter.setOnRetryCallback();
            viewPager.setCurrentItem(pager_position);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }catch (Exception e){}
    }

    @Override
    public void intPageChangeListener(ViewPager.OnPageChangeListener listener)
    {
        viewPager.addOnPageChangeListener(listener);
    }


    @Override
    public void addChildView(ArrayList<View> views)
    {
        if(updateProfileLl.getChildCount()>0)
        {
            updateProfileLl.removeAllViews();
        }
        updateProfileLl.invalidate();
        for(View view:views)
        {
            updateProfileLl.addView(view);
        }
    }

    @OnClick(R.id.back_button)
    void onBack()
    {
        onBackPressed();
    }
    //    @OnClick(R.id.rl_chat_btn)
//    void onChat(View view)
//    {
//        openChat(view);
//    }
    @Override
    public void movePagerPosition(int position)
    {
        pager_position=position;
    }

    @Override
    public void updateSegmentBar(int total, int selected)
    {
        //video_player.stopPlayerFrg();
        segmentedProgressBar.setSegmentCount(total);
        segmentedProgressBar.setCompletedSegments(selected+1);
        itemsCountTv.setText(String.format(Locale.ENGLISH, "%d/%d", selected+1, total));
    }


    @Override
    public void updatePrimeryData(String name,String sch_name,String distance,int online_status)
    {
        spannableTitle = new SpannableString(name);
        tvToolbarTitle.setText(name);
        name_text.setText(name);
        if(!TextUtils.isEmpty(sch_name))
        {
            school_view.setVisibility(View.VISIBLE);
            school_name.setText(sch_name);
        }else
        {
            school_view.setVisibility(View.GONE);
        }

        try
        {
            String name_data[]=name.split(",");
            report_text.setText(String.format(Locale.ENGLISH,"%s %s", getString(R.string.report_michael_text),name_data[0]));
            report_text.setAllCaps(true);
        }catch (Exception e){}
        if(!TextUtils.isEmpty(distance)) {
            location_text.setText(distance);
            location_view.setVisibility(View.VISIBLE);
        }
        else{
            location_view.setVisibility(View.GONE);
        }
        Drawable drawable=online_status==0?null:ContextCompat.getDrawable(activity,R.drawable.online_dot);
        if(drawable == null) {
            user_status.setBackground(drawable);
        }
        else{
            user_status.setBackground(ContextCompat.getDrawable(activity,R.drawable.circle_drop_shadow));
        }
        user_status.setImageDrawable(drawable);
    }


    @Override
    public void addLoadingView(View view)
    {
        if(updateProfileLl.getChildCount()>0)
        {
            updateProfileLl.removeAllViews();
        }
        updateProfileLl.addView(view);
    }

    @Override
    public void showError(String errorMsg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+errorMsg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.colorAccent));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(String msg) {
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+msg, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void showMessage(int msgId) {
        String message=getString(msgId);
        Snackbar snackbar = Snackbar
                .make(parentLayout,""+message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(activity,R.color.dark_gray));
        TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(activity,R.color.white));
        snackbar.show();
    }

    @Override
    public void closePage() {
        this.finish();
    }

    @Override
    public void updateUserAge(String details)
    {
        name_text.setText(details);
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        unbinder.unbind();
        presenter.dropView();
        //if(video_player != null)
        //    video_player.stopPlayer();
    }

    @Override
    public void onBackPressed()
    {
        if(isChatListNeedToUpdate) {
            Intent intent=getIntent();
            Bundle data = new Bundle();
            data.putInt(AppConfig.RESULT_DATA,AppConfig.ON_CHAT);
            data.putString(AppConfig.USER_ID,presenter.collectUserId());
            intent.putExtras(data);
            setResult(RESULT_OK,intent);
        }
        else
            setResult(RESULT_CANCELED);
        super.onBackPressed();
        finish();
    }

    void doDislike()
    {
        presenter.checkForInternetThenDislike();
    }

    void doSuperLike()
    {
        presenter.superLikeThisUser();
    }

    @Override
    public void superLikeUser() {
        Intent intent=getIntent();
        Bundle data=new Bundle();
        data.putInt(AppConfig.RESULT_DATA,AppConfig.ON_SUPER_LIKE);
        data.putString(AppConfig.USER_ID,presenter.collectUserId());
        intent.putExtras(data);
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    public void showAboutText(String aboutText) {
        tvAbout.setText(aboutText);
        rlAboutView.setVisibility(View.VISIBLE);
    }

    @Override
    public void initLikeUser() {
        Intent intent=getIntent();
        Bundle data=new Bundle();
        data.putInt(AppConfig.RESULT_DATA,AppConfig.ON_LIKE);
        data.putString(AppConfig.USER_ID,presenter.collectUserId());
        intent.putExtras(data);
        setResult(RESULT_OK,intent);
        finish();
    }

    @Override
    public void initDislikeUser() {
        Intent intent=getIntent();
        Bundle data=new Bundle();
        data.putInt(AppConfig.RESULT_DATA,AppConfig.ON_DISLIKE);
        data.putString(AppConfig.USER_ID,presenter.collectUserId());
        intent.putExtras(data);
        setResult(RESULT_OK,intent);
        finish();
    }

    void doLike()
    {
        presenter.checkForLikeAvail();
    }

    public void shareDeepLink(String deepLink) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string.recommended_profile));
        intent.putExtra(Intent.EXTRA_TEXT, deepLink);
        startActivity(intent);
    }

    @Override
    public void hideUserDistance() {
        location_view.setVisibility(View.GONE);
    }

    @Override
    public void showDistance(String away_text) {
        location_text.setText(away_text);
        location_view.setVisibility(View.VISIBLE);
    }

    @Override
    public void showUserSchool(String school) {
        school_name.setText(school);
        school_view.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideUserSchool() {
        school_view.setVisibility(View.GONE);
    }

    @Override
    public void showOnlineStatus(boolean userOnline) {
        if(userOnline){
            user_status.setImageResource(R.drawable.online_dot);
            user_status.setBackground(ContextCompat.getDrawable(activity,R.drawable.circle_drop_shadow));
        }
        else{
            user_status.setImageResource(0);
            user_status.setBackground(null);
        }
    }


    @Override
    public void showButtonsState(boolean unliked, boolean superliked, boolean liked,boolean isMatched)
    {
        if(superliked||liked || isMatched)
        {
            super_like_view.setVisibility(View.GONE);
            dislike_view.setVisibility(View.GONE);
            //boost_view.setVisibility(View.GONE);
            like_view.setVisibility(View.GONE);
        }else if(unliked)
        {
            super_like_view.setVisibility(View.VISIBLE);
            dislike_view.setVisibility(View.GONE);
            //boost_view.setVisibility(View.VISIBLE);
            like_view.setVisibility(View.VISIBLE);
        }else
        {
            super_like_view.setVisibility(View.VISIBLE);
            dislike_view.setVisibility(View.VISIBLE);
            //boost_view.setVisibility(View.VISIBLE);
            like_view.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void launchChatScreen(Intent intent) {
        isChatListNeedToUpdate = true;
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(activity).toBundle());
    }

    @Override
    public void showWorkIcon()
    {
        school_img.setImageResource(R.drawable.work);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(!InnAppSdk.getVendor().onActivity_result(requestCode,resultCode,data))
        {
            if (!instagramManger.onActivityResult(requestCode, resultCode, data))
            {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void hideInstadetails()
    {
        instagram_button_viiew.setVisibility(View.GONE);
        inst_button_view.setVisibility(View.GONE);
        insta_media_list.setVisibility(View.GONE);
        show_insta_pic.setVisibility(View.GONE);
    }

    @Override
    public void showInsaPost(List<List<ImageData>> lists)
    {
        show_insta_pic.setText(String.format("Show %s Instagram Photos", presenter.getUserName()));
        show_insta_pic.setVisibility(View.VISIBLE);
        insta_media_list.setAdapter(new InstaMediaAdapter(this,this,lists));
        insta_media_list.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        connect_instagram.setVisibility(View.GONE);
        inst_button_view.setVisibility(View.GONE);
        instagram_button_viiew.setVisibility(View.VISIBLE);
        insta_media_list.setVisibility(View.VISIBLE);
    }


    @Override
    public void handleInstagramDetails()
    {
        /*if(instagramManger.isUserLoggedIn())
        {*/
        presenter.getOtherUserInstPost(presenter.getUserInstaId(),presenter.getUserInstaToken());
//        }else
//        {
//            show_insta_pic.setText(String.format("Show %s Instagram Photos", presenter.getUserName()));
//            connect_instagram.setVisibility(View.VISIBLE);
//            instagram_button_viiew.setVisibility(View.VISIBLE);
//            inst_button_view.setVisibility(View.VISIBLE);
//            show_insta_pic.setVisibility(View.VISIBLE);
//            connect_insta.setVisibility(View.VISIBLE);
//            insta_media_list.setVisibility(View.GONE);
//        }
    }

    @Override
    public void setReportText(String userName) {
        spannableTitle = new SpannableString(userName);
        tvToolbarTitle.setText(userName);
        report_text.setText(String.format(Locale.ENGLISH,"%s %s", getString(R.string.report_michael_text),userName));
        report_text.setAllCaps(true);
    }

    @Override
    public void openInstagram()
    {
        instagramManger.openInInstagram(this,presenter.getUserInstaId(),presenter.getUserInstName());
    }

    @Override
    public void launchCoinWallet() {
        Intent intent = new Intent(activity,AddCoinActivity.class);
        startActivity(intent);
        activity.overridePendingTransition(R.anim.slide_from_right,R.anim.slide_to_left);
    }

    @Override
    public void showUserLocation(String userLocationString, boolean locationNeedToShow) {
        if(locationNeedToShow){
            location_text.setText(userLocationString);
            location_view.setVisibility(View.VISIBLE);
        }else{
            location_view.setVisibility(View.GONE);
        }
    }

    @Override
    public void showMomentData(List<MomentsData> momentList) {
        if(llMoments != null){
            if(!momentList.isEmpty()) {
                llMoments.setVisibility(View.VISIBLE);
                MomentsGridAdapter momentsGridAdapter = new MomentsGridAdapter(activity,momentList);
                rvMoments.setHasFixedSize(true);
                rvMoments.setLayoutManager(new GridLayoutManager(this,3));
                rvMoments.setAdapter(momentsGridAdapter);
                presenter.setMomentClickCallback(momentsGridAdapter);
            }else{
                llMoments.setVisibility(View.GONE);
            }
        }
    }


    void viewAllMoments(){
        presenter.loadMomentScreen();
    }
    @Override
    public void launchMomentScreen(ArrayList<MomentsData> momentsDataList, int position) {
        Intent intent=new Intent(activity, MomentsGridActivity.class);
        intent.putExtra(ApiConfig.MOMENTS.MOMENTS_LIST,momentsDataList);
        intent.putExtra(ApiConfig.MOMENTS.POSITION,position);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(intent, AppConfig.MOMENT_GRID_REQ_CODE);
    }

    @Override
    public void launchMomentVerticalScreen(ArrayList<MomentsData> momentsDataList, int position) {
        Intent intent=new Intent(activity, MomentsActivity.class);
        intent.putExtra(ApiConfig.MOMENTS.MOMENTS_LIST,momentsDataList);
        intent.putExtra(ApiConfig.MOMENTS.POSITION,position);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(intent, AppConfig.MOMENT_VERTICAL_REQ_CODE);
    }

}
