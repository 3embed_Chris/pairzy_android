package com.pairzy.com.data.model.cloudinaryDetail;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CloudinaryDetailResponse {

    @Expose
    @SerializedName("message")
    String message;

    @Expose
    @SerializedName("data")
    CloudinaryData cloudinaryData;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CloudinaryData getCloudinaryData() {
        return cloudinaryData;
    }

    public void setCloudinaryData(CloudinaryData cloudinaryData) {
        this.cloudinaryData = cloudinaryData;
    }
}
