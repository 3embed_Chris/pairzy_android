package com.pairzy.com.likes.model;

import com.pairzy.com.BaseModel;
import com.pairzy.com.data.source.PreferenceTaskDataSource;
import com.pairzy.com.likes.LikesByUtil;
import com.pairzy.com.util.Utility;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

public class LikesModel extends BaseModel {
    @Inject
    PreferenceTaskDataSource dataSource;
    @Inject
    Utility utility;

    @Named(LikesByUtil.LIKES_POST)
    @Inject
    ArrayList<LikesDataPojo> likesDataPojos;

    @Inject
    LikesAdapter adapter;

    @Inject
    public LikesModel() {
    }

    public void parseResponse(String data) {
        try {
            LikesPojo likesPojo = utility.getGson().fromJson(data, LikesPojo.class);
            likesDataPojos.clear();
            likesDataPojos.addAll(likesPojo.getData());
            adapter.notifyDataSetChanged();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
