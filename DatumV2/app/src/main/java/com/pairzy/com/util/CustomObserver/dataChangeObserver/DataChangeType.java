package com.pairzy.com.util.CustomObserver.dataChangeObserver;

public enum DataChangeType {
    BLOCK_USER(1),
    UN_BLOCK_USER(2);
    int value;
    DataChangeType(int value) {
        this.value = value;
    }
}
