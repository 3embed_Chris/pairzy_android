package com.pairzy.com.commentPost;

import com.pairzy.com.commentPost.model.CommentPostAdapter;

public interface CommentPostContract {

    interface View
    {

        void showMessage(String message);

        void CallGetUserComment();

        void notifyDataAdapter(int position);

        void editComment(String comment);
    }

    interface  Presenter
    {

        void validateComment(String comment, String postId);

        void getUserComment();

        void  postUserComment();

        void setAdapterCallBack(CommentPostAdapter postAdapter);
    }
}
