package com.pairzy.com.googleLocationSearch.model;

import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pairzy.com.R;
import com.pairzy.com.util.TypeFaceManager;


import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * <h>LocationAdapter class.</h>
 * @author 3Embed.
 * @since 10/5/18.
 * @version 1.0.
 */
public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.Address_list_item> {
    private TypeFaceManager typeFaceManager;

    public interface OnItemClickListener {
        void onItemClick(AddressListPojo addressPojo, int position);
    }
    private OnItemClickListener listener;
    private ArrayList<AddressListPojo> addressList;

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public ArrayList<AddressListPojo> getAddressList() {
        return addressList;
    }

    @Inject
    public LocationAdapter(@Named("ADDRESS_LIST_POJO")ArrayList<AddressListPojo> addressList, TypeFaceManager typeFaceManager) {
        this.addressList =addressList;
        this.typeFaceManager = typeFaceManager;
    }

    @Override
    public Address_list_item onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.address_item, parent, false);
        return new Address_list_item(v);
    }

    @Override
    public void onBindViewHolder(Address_list_item holder, final int position) {
        AddressListPojo addressPojo = addressList.get(position);
        holder.locationName.setText(addressPojo.getAddress_title());
        holder.addressTextview.setText(addressPojo.getSub_Address());
        holder.tvDistance.setText("");
        holder.tvRating.setText("");
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onItemClick(addressPojo, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }

    class Address_list_item extends RecyclerView.ViewHolder {
        TextView locationName;
        TextView addressTextview;
        RelativeLayout item;
        TextView tvRating;
        TextView tvDistance;
        AppCompatRatingBar ratingBar;
        Address_list_item(View itemView) {
            super(itemView);
            locationName = (TextView) itemView.findViewById(R.id.location_name);
            tvRating = (TextView) itemView.findViewById(R.id.rating_tv);
            tvDistance = (TextView) itemView.findViewById(R.id.distance_tv);
            ratingBar = (AppCompatRatingBar) itemView.findViewById(R.id.rating_bar);
            addressTextview = (TextView) itemView.findViewById(R.id.address_textview);
            item = (RelativeLayout) itemView.findViewById(R.id.item);
            locationName.setTypeface(typeFaceManager.getCircularAirBold());
            addressTextview.setTypeface(typeFaceManager.getCircularAirBook());
            tvRating.setTypeface(typeFaceManager.getCircularAirBook());
            tvDistance.setTypeface(typeFaceManager.getCircularAirBook());
        }
    }
}