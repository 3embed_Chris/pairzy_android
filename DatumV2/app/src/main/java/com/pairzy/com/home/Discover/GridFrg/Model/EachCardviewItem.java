package com.pairzy.com.home.Discover.GridFrg.Model;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.pairzy.com.util.CustomVideoView.widget.media.IjkVideoView;
import com.pairzy.com.util.SegmentProgressBar.SegmentedProgressBar;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

/**
 * <h2>EachCardviewItem</h2>
 * <P>
 *
 * </P>
 */
public class EachCardviewItem
{
    public FrameLayout parent_view,video_container;
    public TextView name_Text,school_name,yesVote,noVote;
    public ImageView super_like_indicator,
            item_swipe_left_indicator,
            item_swipe_right_indicator;
    public FrameLayout background;
    public SimpleDraweeView cardImage;
    public View next_img,prev_img,view_profile;
    public SegmentedProgressBar segmentedProgressBar;
    public ImageView video_play_icon,user_status,muteButton;
    public RelativeLayout content_view;
    public ImageView ivChat, ivProfile, ivSupedlikedMe;
    public UnifiedNativeAdView adView;
    public LinearLayout cardButtonLayout;
    public ProgressBar pbProgress;
    public static IjkVideoView ijkVideoView;
    public TableLayout mHudView;
}