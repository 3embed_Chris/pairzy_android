package com.pairzy.com.MyProfile.editPreference.newListScrollerFrag;

import com.pairzy.com.dagger.FragmentScoped;

import dagger.Binds;
import dagger.Module;

/**
 *@since  2/22/2018.
 * @author 3Embed.
 * @version 1.0.
 */
@Module
public interface NewListdataFrgBuilder
{
    @FragmentScoped
    @Binds
    NewListdataFrg getConChoiceFragment(NewListdataFrg listdataFrg);
    @FragmentScoped
    @Binds
    NewListdataFrgContract.Presenter taskPresenter(NewListdataFrgPresenter presenter);
}
