package com.pairzy.com.MqttManager;

/**
 * @since  3/9/2018.
 */

public interface MqttManager
{
    void connect(MqttEventCallbackCallback connectionCallback);
    void reConnect();
    void setCredential(String userName, String password);
    void setWillMessage(String topic, String payload, int qos, boolean isRetain);
    void disconnect();
    boolean canPublish();
    void publishData(String topic, String payload, boolean isRetain, int qos);
    void publishData(String topic, String payload, boolean isRetain);
    void publishData(String topic, String payload);
    void subscribe(String topic, int qos);
    void unSubscribe(String topic);

}
