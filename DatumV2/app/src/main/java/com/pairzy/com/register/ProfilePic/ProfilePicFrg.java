package com.pairzy.com.register.ProfilePic;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pairzy.com.ImageCropper.Cropper.CropImage;
import com.pairzy.com.ImageCropper.Cropper.CropImageView;
import com.pairzy.com.R;
import com.pairzy.com.register.ProfileVideo.ProfileVideoFrg;
import com.pairzy.com.register.RegisterContact;
import com.pairzy.com.util.AppConfig;
import com.pairzy.com.util.App_permission;
import com.pairzy.com.util.TypeFaceManager;
import com.pairzy.com.util.Utility;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import javax.inject.Inject;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
/**
 * <h2>ProfilePicFrg</h2>
 * A simple {@link Fragment} subclass.
 * @author 3Embed.
 * @version 1.0.
 * @since 12-07-2018.
 */
public class ProfilePicFrg extends DaggerFragment implements ProfilePicContact.View
{

    private Unbinder unbinder;
    private String uploadedImageUrl="";

    @BindString(R.string.gallery_error)
    String file_path_error;

    @Inject
    Utility utility;

    @Inject
    App_permission app_permission;

    @Inject
    Activity activity;
    @Inject
    TypeFaceManager typeFaceManager;

    @BindView(R.id.skip_page)
    TextView skip_page;

    @BindView(R.id.btnNext)
    RelativeLayout btnNext;

    @Inject
    ProfilePicContact.Presenter presenter;

    @Inject
    RegisterContact.Presenter main_presenter;

    @BindView(R.id.title_first)
    TextView title_first;

    @BindView(R.id.sub_title_text)
    TextView sub_title_text;

    @BindView(R.id.add_photo)
    TextView add_photo;

    @BindView(R.id.add_image)
    ImageView add_image;

    @BindView(R.id.user_image)
    SimpleDraweeView user_image;

    @BindView(R.id.change_pic)
    TextView change_pic;

    @Inject
    public ProfilePicFrg() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_profile_pic_frg, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        unbinder= ButterKnife.bind(this,view);
        initUIDetails();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
       if(!app_permission.onRequestPermissionsResult(requestCode, permissions, grantResults))
       {
           super.onRequestPermissionsResult(requestCode, permissions, grantResults);
       }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode==AppConfig.CAMERA_CODE&&resultCode==Activity.RESULT_OK)
        {
            presenter.upDateToGallery();
            presenter.compressImage(presenter.getTempPic());
        }else if(requestCode==AppConfig.GALLERY_CODE&&resultCode==Activity.RESULT_OK)
        {
            if(data!=null&&data.getData() != null)
            {
                Uri uri = data.getData();
                String file_path=utility.getRealPathFromURI(uri);
                if(!TextUtils.isEmpty(file_path))
                {
                    presenter.compressImage(file_path);
                }else
                {
                    showError(file_path_error);
                }
            }else
            {
                showError(file_path_error);
            }
        }else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.takeView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        skip_page.requestFocus();
        utility.closeSpotInputKey(activity,change_pic);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        presenter.dropView();
        unbinder.unbind();
    }

    /*
     * intialization of the xml content.*/
    private void initUIDetails()
    {
        btnNext.setEnabled(false);
        skip_page.setTypeface(typeFaceManager.getCircularAirBook());
        title_first.setTypeface(typeFaceManager.getCircularAirBold());
        sub_title_text.setTypeface(typeFaceManager.getCircularAirBold());
        add_photo.setTypeface(typeFaceManager.getCircularAirBook());
        change_pic.setTypeface(typeFaceManager.getCircularAirBook());
        user_image.getHierarchy().setActualImageScaleType(ScalingUtils.ScaleType.CENTER_CROP);
        user_image.setOnClickListener(v -> presenter.openImagePreview(presenter.getRecentCameraPic()));
    }

    @OnClick(R.id.close_button)
    void onBackClick()
    {
        activity.onBackPressed();
    }

    @OnClick(R.id.pic_container)
    void onPicClick()
    {
        presenter.openChooser();
    }

    @OnClick(R.id.change_pic)
    void onChangeClick()
    {
        presenter.openChooser();
    }

    @OnClick(R.id.btnNext)
    void onNextClick()
    {
        btnNext.setEnabled(false);
        if(TextUtils.isEmpty(uploadedImageUrl))
        {
            presenter.uploadToCloudinary(presenter.getRecentCameraPic(),true);
        }else
        {
            moveFragment();
        }
        btnNext.setEnabled(true);
    }

    @Override
    public void imageCollectError()
    {
        user_image.setVisibility(View.GONE);
        handelNextButton(false);
        showError(file_path_error);
    }


    @Override
    public void onImageCompressed(String file_path)
    {
        presenter.setProfilePIc(file_path,user_image);
    }



    @Override
    public void openImageCropper(Uri uri)
    {
        CropImage.activity(uri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle("Crop")
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setCropMenuCropButtonTitle("Done")
                .setRequestedSize(400, 400)
                .setCropMenuCropButtonIcon(R.drawable.information)
                .start(getContext(),this);
    }



    @Override
    public void imageUploaded(String image_url)
    {
        uploadedImageUrl=image_url;
        moveFragment();
    }


    @Override
    public void updateImageSet(String file_path)
    {
        uploadedImageUrl="";
        handelNextButton(true);
        showChangeButton(true);
    }

    private void showChangeButton(boolean show) {
        if(show){
            change_pic.setVisibility(View.VISIBLE);
        }
        else{
            change_pic.setVisibility(View.GONE);
        }
    }


    @Override
    public void openCamera(Uri uri)
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        if(intent.resolveActivity(activity.getPackageManager()) != null)
            startActivityForResult(intent, AppConfig.CAMERA_CODE);
    }

    @Override
    public void moveFragment()
    {
        ProfileVideoFrg profileVideoFrg=new ProfileVideoFrg();
        Bundle data=getArguments();
        if(data==null)
        {
            data=new Bundle();
        }
        data.putString(RegisterContact.Presenter.PIC_DATA,uploadedImageUrl);
        profileVideoFrg.setArguments(data);
        main_presenter.launchFragment(profileVideoFrg,true);
    }

    @Override
    public void openGallery()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),AppConfig.GALLERY_CODE);
    }

    @Override
    public void showError(String message)
    {
        main_presenter.showError(message);
    }

    @Override
    public void showMessage(String messaage)
    {
        main_presenter.showMessage(messaage);
    }

    /*
     *Handel next button */
    private void handelNextButton(boolean isEnable)
    {
        if(isEnable)
        {
            btnNext.setEnabled(true);
        }
        else {
            btnNext.setEnabled(false);
        }
    }
}
