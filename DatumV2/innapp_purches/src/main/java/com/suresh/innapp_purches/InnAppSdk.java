package com.suresh.innapp_purches;
import android.app.Application;

public class InnAppSdk
{
    private static InnAppVendor innAppVendor=null;
    public static void init(Application application,String appBaseKey)
    {
        if(innAppVendor==null)
        {
            innAppVendor=new InAppManager(application,appBaseKey);
        }
    }

    public static InnAppVendor getVendor()
    {
        if(innAppVendor==null)
        {
            throw new IllegalArgumentException("Init vendor first in application class !");

        }
        return innAppVendor;
    }

    public enum Type
    {
        INNAPP,
        SUB
    }
}
