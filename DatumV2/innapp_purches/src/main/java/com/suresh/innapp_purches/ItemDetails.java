package com.suresh.innapp_purches;

class ItemDetails
{
    String product_id;
    String pay_load;
    InnAppSdk.Type type;
    ItemDetails(String id, String pay_load, InnAppSdk.Type type)
    {
        this.product_id=id;
        this.pay_load=pay_load;
        this.type=type;
    }
}
