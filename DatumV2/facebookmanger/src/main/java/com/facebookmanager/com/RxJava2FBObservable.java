package com.facebookmanager.com;

import io.reactivex.Observable;
import io.reactivex.Observer;

/**
 * <h2></h2>
 *
 * @author Suresh.
 * @since 12/14/2017.
 */
public class RxJava2FBObservable extends Observable<FacebookUserDetails> {
    private static RxJava2FBObservable rxJava2Observable = null;
    private Observer<? super FacebookUserDetails> observer;

    private RxJava2FBObservable() {
    }

    static RxJava2FBObservable getInstance() {
        if (rxJava2Observable == null) {
            rxJava2Observable = new RxJava2FBObservable();
        }
        return rxJava2Observable;
    }

    @Override
    protected void subscribeActual(Observer<? super FacebookUserDetails> observer) {
        this.observer = observer;
    }

    void publishData(FacebookUserDetails data)
    {
        if (observer != null && data != null)
        {
            if (data.isError()) {
                observer.onError(new Throwable(data.getMessage()));
            } else {
                observer.onNext(data);
            }
            observer.onComplete();
        }
    }
}
