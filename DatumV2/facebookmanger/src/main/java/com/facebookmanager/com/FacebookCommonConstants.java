package com.facebookmanager.com;

/**
 * @since 18/11/16.
 * @author Suresh.
 * @version 1.0.
 */
class FacebookCommonConstants {
    static final String None = "NONE";
    static final String Facebook_app_web = "FACEBOOK_APPLICATION_WEB";
    static final String Facebook_app_native = "FACEBOOK_APPLICATION_NATIVE";
    static final String Facebook_app_server = "FACEBOOK_APPLICATION_SERVICE";
    static final String web_view = "WEB_VIEW";
    static final String test_user = "TEST_USER";
    static final String client_token = "CLIENT_TOKEN";

    static final String ERROR = "ERROR";
    static final String MESSAGE = "MESSAGE";
    static final String RESULT = "RESULT";
    static final String PROFILE_PIC = "PROFILE_PIC";
}
