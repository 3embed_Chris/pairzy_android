package com.facebookmanager.com;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
/**
 * <h>FacebookManager</h>
 * <p>
 * Class contain a async task to get the data from facebook .
 * Contains a method to do facebook login .
 * Here doing facebook login and taking data as user_friends .
 * </P>
 *
 * @author Suresh.
 * @since 4/02/2016
 */
public class FacebookManager
{
    private boolean isReady = false;
    private FacebookSessionManager facebook_session_manager;
    private Facebook_callback callback = null;
    private FacebookQueryManager fbQueryManager;
    private CallbackManager callbackManager;
    private static FacebookManager facebookManager;
    private Activity current_Activity;

    public FacebookManager(Context context, String AppID)
    {
        FacebookSdk.sdkInitialize(context, new FacebookSdk.InitializeCallback() {
            @Override
            public void onInitialized() {
                isReady = true;
            }
        });
        FacebookSdk.setApplicationId(AppID);
        callbackManager = CallbackManager.Factory.create();
        facebook_session_manager = new FacebookSessionManager(context);
        fbQueryManager = FacebookFactory.geInstance();
        facebookManager=this;
    }

    static FacebookManager getInstance()
    {
        return facebookManager;
    }


    public void logout(){
        try {
            LoginManager.getInstance().logOut();
        }catch (Exception e){}
    }

    /**
     * <h2>faceBook_Login</h2>
     * <p>
     * Facebook login data from user.
     * </P>
     */
    public void faceBook_Login(Activity activity, final Facebook_callback facebook_callback)
    {
        callback = facebook_callback;
        Refresh_Token();
        LoginManager.getInstance().logInWithReadPermissions(activity, Collections.singletonList("email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                /*
                 * Storing all the data in session manger .*/
                AccessToken accessToken = loginResult.getAccessToken();
                store_Acess_token_details(accessToken);
                /*
                 * Giving the call back for success logged in.*/
                if (callback != null)
                    callback.success(facebook_session_manager.getUserId());
            }

            @Override
            public void onCancel() {
                if (callback != null)
                    callback.cancel("User canceled !");
            }

            @Override
            public void onError(FacebookException error) {
                if (callback != null)
                    callback.error("Got some error!");
            }
        });
    }

    /*
    * Doing login with read permission.*/
    void loginWithReadPermission(ArrayList<String> permission,FacebookRequiredLogin callback)
    {
        if(current_Activity==null)
        {
            callback.onError("Check for the activity reference!");
        }else
        {
            fb_ReadPermission(callback,current_Activity,permission);
        }
    }

    /**
     * <h2>fbLogin_status_for_permission</h2>
     * <p>
     * Fb login status.
     * </P>
     *
     * @param callbackmanager   call back managers.
     * @param facebook_callback user interface manager.
     * @param permission_array  checking for the  permission result.
     */
    public void ask_PublishPermission(CallbackManager callbackmanager, Activity mactivity, String permission_array[], final Facebook_callback facebook_callback) {
        callback = facebook_callback;
        fb_PublishPermission(callbackmanager, mactivity, permission_array);
    }

    /*
     *Doing facebook login facebook login */
    private void fb_ReadPermission(final FacebookRequiredLogin callback, Activity activity, final ArrayList<String> permission_array)
    {
        LoginManager.getInstance().logInWithReadPermissions(activity,permission_array);
        LoginManager.getInstance().registerCallback(callbackManager,new FacebookCallback<LoginResult>()
        {
            @Override
            public void onSuccess(LoginResult loginResult)
            {
               if(checkPermission(permission_array))
               {
                   if (callback != null)
                       callback.onSuccess();
               }else
               {
                   if (callback != null)
                       callback.onError("Please accept all the permission!");
               }
            }

            @Override
            public void onCancel() {
                if (callback != null)
                    callback.onError("User canceled!");
            }

            @Override
            public void onError(FacebookException error)
            {
                if (callback != null)
                    callback.onError(error.getMessage());
            }
        });
    }


    /*
     * Checking the required permission is granted or not.*/
    private boolean checkPermission(ArrayList<String> required)
    {
        Set<String> list = AccessToken.getCurrentAccessToken().getPermissions();
        ArrayList<String> list_Data = new ArrayList<>(list);
        if(list_Data.size()<1)
        {
            return false;
        }else
        {
            ArrayList<String> pending_list=new ArrayList<>();
            for (int count = 0;count<required.size(); count++)
            {
                if(!list_Data.contains(required.get(count)))
                {
                    pending_list.add(required.get(count));
                }
            }
            if (pending_list.size()>0)
            {
               return false;
            }
        }
        return true;
    }

    private void fb_PublishPermission(CallbackManager callbackmanager, Activity mactivity, String permission_array[]) {
        if (verify_Permission(Arrays.asList(permission_array)).size() == 0) {
            if (callback != null)
                callback.success(facebook_session_manager.getUserId());
            return;
        }
        LoginManager.getInstance().logInWithPublishPermissions(mactivity, Arrays.asList(permission_array));
        LoginManager.getInstance().registerCallback(callbackmanager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                /*
                 * Giving the call back for sucess logged in.*/
                if (callback != null)
                    callback.success(facebook_session_manager.getUserId());
            }

            @Override
            public void onCancel() {
                if (callback != null)
                    callback.cancel("User canceled !");
            }

            @Override
            public void onError(FacebookException error) {
                if (callback != null)
                    callback.error("Got some error!");
            }
        });
    }

    /**
     * <h2>verify_Permission</h2>
     * <p>
     * Checking the permission of the data.
     * </P>
     *
     * @param permissions contains the token error.
     */
    private ArrayList<String> verify_Permission(List<String> permissions) {
        ArrayList<String> temp = new ArrayList<>();
        Set<String> granted_list = AccessToken.getCurrentAccessToken().getPermissions();
        for (String check_test : permissions) {
            boolean isFound = false;
            for (String permissioon : granted_list) {
                if (check_test.equals(permissioon)) {
                    isFound = true;
                    break;
                }
            }
            if (!isFound) {
                temp.add(check_test);
            }
        }
        return temp;
    }


    /*
    * Storing the facebook token and other details.*/
    private void store_Acess_token_details(AccessToken accessToken) {
        facebook_session_manager.setFacebookToken(accessToken.getToken());
        facebook_session_manager.setAcessToken(accessToken.getToken());
        facebook_session_manager.setApplicationId(accessToken.getApplicationId());
        facebook_session_manager.setUserId(accessToken.getUserId());
        facebook_session_manager.setPermission(accessToken.getPermissions());
        facebook_session_manager.setDeclinePermission(accessToken.getDeclinedPermissions());

        switch (accessToken.getSource()) {
            case NONE: {
                facebook_session_manager.setAcessTokenSource(FacebookCommonConstants.None);
                break;
            }
            case FACEBOOK_APPLICATION_WEB: {
                facebook_session_manager.setAcessTokenSource(FacebookCommonConstants.Facebook_app_web);
                break;
            }
            case FACEBOOK_APPLICATION_NATIVE: {
                facebook_session_manager.setAcessTokenSource(FacebookCommonConstants.Facebook_app_native);
                break;
            }
            case FACEBOOK_APPLICATION_SERVICE: {
                facebook_session_manager.setAcessTokenSource(FacebookCommonConstants.Facebook_app_server);
                break;
            }
            case WEB_VIEW: {
                facebook_session_manager.setAcessTokenSource(FacebookCommonConstants.web_view);
                break;
            }
            case TEST_USER: {
                facebook_session_manager.setAcessTokenSource(FacebookCommonConstants.test_user);
                break;
            }
            case CLIENT_TOKEN: {
                facebook_session_manager.setAcessTokenSource(FacebookCommonConstants.client_token);
                break;
            }
        }
        @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        facebook_session_manager.setExpTime(formatter.format(accessToken.getExpires()));
        facebook_session_manager.setLastRefTime(formatter.format(accessToken.getLastRefresh()));
    }

    /**
     * <h>Refresh_Token</h>
     * <p>
     * Calback interface of facebook.
     * </P>
     */
    private void Refresh_Token()
    {
        AccessToken accessToken=AccessToken.getCurrentAccessToken();
        if(accessToken!=null)
        {
            try
            {
                removingPermission(accessToken.getUserId());
            }catch (Exception e){}
        }
        LoginManager.getInstance().logOut();
        if (isReady) {
            AccessToken.refreshCurrentAccessTokenAsync();
        }
    }

    /**
     * Removing the given permission
     */
    private void removingPermission(String userId)
    {
        GraphRequest delPermRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), "/"+userId+"/permissions/", null, HttpMethod.DELETE, new GraphRequest.Callback()
        {
            @Override
            public void onCompleted(GraphResponse graphResponse)
            {}
        });
        delPermRequest.executeAsync();
    }

    /**
     * Handling the facebook onActivity result for getting the
     * result.
     */
    public boolean onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (callbackManager == null) {
            throw new IllegalStateException("Error : Initialize the facebook manager first");
        }
        return callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /*
    * Checking the user is already logged in or not.*/
    public boolean isLoggedIn()
    {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @SuppressLint("StaticFieldLeak")
    public RxJava2FBObservable collectUserDetails(Activity activity)
    {
        current_Activity=activity;
        RxJava2FBObservable observable = RxJava2FBObservable.getInstance();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                ArrayList<String> permission_list = new ArrayList<>();
                permission_list.add("email");
//                permission_list.add("user_birthday");
//                permission_list.add("user_photos");
//                permission_list.add("user_location");
                fbQueryManager.getUserDetails(permission_list);
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        return observable;
    }

    /*
    * Facebook revoke these access
    *       permission_list.add("user_religion_politics");
                permission_list.add("user_education_history");
                permission_list.add("user_work_history");*/

    /**
     * <h>Facebook_callback</h>
     * <p>
     * Calback interface of facebook.
     * </P>
     */
    public interface Facebook_callback
    {
        void success(String id);
        void error(String error);
        void cancel(String cancel);
    }

    interface FacebookRequiredLogin
    {
        void onSuccess();
        void onError(String message);
    }
}
