package com.facebookmanager.com;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;
/**
 * <h1>FacebookSessionManager</h1>
 * <P>
 *     Managing the app facebook login.
 * </P>
 * @since  18/11/16.
 * @author Suresh.
 */
class FacebookSessionManager
{
    private static final String PREF_NAME="Facebook_data";
    private static final String ACESS_TOKEN="accessToken";
    private static final String APPLICATION_ID="applicationId";
    private static final String USER_ID="userId";
    private static final String PERMISSION="permissions";
    private static final String DECLINE_PERMISSION="declinedPermissions";
    private static final String ACESS_TOKEN_SOURCE="accessTokenSource";
    private static final String EXP_TIME="expirationTime";
    private static final String LAST_REF_TIME="lastRefreshTime";
    private static final String FACEBOOK_TOKEN="facebook_token";
    private SharedPreferences sharedPreferences;

    FacebookSessionManager(Context context)
    {
        sharedPreferences=context.getSharedPreferences(PREF_NAME,0);
    }

    void clear_session()
    {
        sharedPreferences.edit().clear().apply();
    }

    String getAcessToken()
    {
        return sharedPreferences.getString(ACESS_TOKEN,"");
    }

    void setAcessToken(String acess_token)
    {
        sharedPreferences.edit().putString(ACESS_TOKEN,acess_token).apply();
    }

    String getFacebookToken()
    {
        return sharedPreferences.getString(FACEBOOK_TOKEN,"");
    }

    void setFacebookToken(String token)
    {
        sharedPreferences.edit().putString(FACEBOOK_TOKEN,token).apply();
    }

    String getApplicationId()
    {
        return sharedPreferences.getString(APPLICATION_ID,"");
    }
    void setApplicationId(String applicationId)
    {
        sharedPreferences.edit().putString(APPLICATION_ID,applicationId).apply();
    }
    String getUserId()
    {
        return sharedPreferences.getString(USER_ID,"");
    }
    void setUserId(String user_id)
    {
        sharedPreferences.edit().putString(USER_ID,user_id).apply();
    }
    Set<String> getPermission()
    {
        return sharedPreferences.getStringSet(PERMISSION,null);
    }
    void setPermission(Set<String> permission_list)
    {
        sharedPreferences.edit().putStringSet(PERMISSION,permission_list).apply();
    }
    Set<String> getDeclinePermission()
    {
        return sharedPreferences.getStringSet(DECLINE_PERMISSION,null);
    }
    void setDeclinePermission(Set<String> dec_permission_list)
    {
        sharedPreferences.edit().putStringSet(DECLINE_PERMISSION,dec_permission_list).apply();
    }
    String getAcessTokenSource()
    {
        return sharedPreferences.getString(ACESS_TOKEN_SOURCE,"");
    }
    void setAcessTokenSource(String acess_token_tag)
    {
        sharedPreferences.edit().putString(ACESS_TOKEN_SOURCE,acess_token_tag).apply();
    }
    String getExpTime()
    {
        return sharedPreferences.getString(EXP_TIME,"");
    }
    void setExpTime(String time_data)
    {
        sharedPreferences.edit().putString(EXP_TIME,time_data).apply();
    }
    String getLastRefTime()
    {
        return sharedPreferences.getString(LAST_REF_TIME,"");
    }
    void setLastRefTime(String last_time)
    {
        sharedPreferences.edit().putString(LAST_REF_TIME,last_time).apply();
    }


}
