package com.facebookmanager.com;

import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Set;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
/**
 * <h2>FacebookFactory</h2>
 * Facebook factory to manage facebook request.
 *
 * @author Suresh.
 * @version 1.0.
 * @since 12/14/2017.
 */
class FacebookFactory implements FacebookQueryManager
{
    private static FacebookFactory facebookFactory;
    private final String SOURCE = "source";
    private final String IMAGES = "images";
    private final String PHOTOS = "photos";
    private final String NAME = "name";
    private final String FIRST_NAME = "first_name";
    private final String LAST_NAME = "last_name";
    private final String BIRTHDAY = "birthday";
    private final String EMAIL = "email";
    private final String ID = "id";
    private final String ALBUMS = "albums";
    private final String DATA = "data";
    private final String EDUCATION = "education";
    private final String WORK="work";
    private final String ABOUT="about";
    private final String GENDER="gender";

    /*
     *Collecting user other details*/
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    static FacebookQueryManager geInstance() {
        if (facebookFactory == null) {
            facebookFactory = new FacebookFactory();
        }
        return facebookFactory;
    }

    @Override
    public void getUserDetails(ArrayList<String> permission)
    {
        try {
            isLoggedIn();
            checkPermission(permission);
            GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response)
                        {
                            try {
                                Log.w("fbUserDetail Object: ",object.toString());
                                Log.w("fbUserDetail Response:",response.getJSONObject().toString());
                                if (response.getError() != null) {
                                    throw new FacebookException(FacebookCommonConstants.ERROR + response.getError().getErrorMessage());
                                } else {
                                    FacebookUserDetails facebookUserDetails = new FacebookUserDetails();
                                    if (object.has(NAME))
                                    {
                                        facebookUserDetails.setName(object.getString(NAME));
                                    }
                                    if (object.has(FIRST_NAME)) {
                                        facebookUserDetails.setFirstName(object.getString(FIRST_NAME));
                                    }
                                    if (object.has(LAST_NAME)) {
                                        facebookUserDetails.setLastName(object.getString(LAST_NAME));
                                    }
                                    if (object.has(BIRTHDAY)) {
                                        facebookUserDetails.setBirthDate(object.getString(BIRTHDAY));
                                    }
                                    if (object.has(EMAIL)) {
                                        facebookUserDetails.setEmail(object.getString(EMAIL));
                                    }
                                    if (object.has(ID)) {
                                        facebookUserDetails.setId(object.getString(ID));
                                        facebookUserDetails.setUserPic("http://graph.facebook.com/"+facebookUserDetails.getId()+"/picture?type=large");
                                    }
                                    if (object.has(EDUCATION)) {
                                        facebookUserDetails.setEducation(object.getString(EDUCATION));
                                    }
                                    if(object.has(WORK))
                                    {
                                        facebookUserDetails.setWork(object.getString(WORK));
                                    }
                                    if(object.has(GENDER))
                                    {
                                        facebookUserDetails.setGender(object.getString(GENDER));
                                    }else
                                    {
                                        facebookUserDetails.setGender("");
                                    }
                                    if(object.has(ABOUT))
                                        facebookUserDetails.setBio(object.getString(ABOUT));

                                   //collectOtherImageList(facebookUserDetails);
                                   RxJava2FBObservable.getInstance().publishData(facebookUserDetails);
                                }
                            } catch (Exception e) {
                                FacebookUserDetails facebookUserDetails = new FacebookUserDetails();
                                facebookUserDetails.setError(true);
                                facebookUserDetails.setMessage(e.getMessage());
                                RxJava2FBObservable.getInstance().publishData(facebookUserDetails);
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", NAME + "," + FIRST_NAME + "," + LAST_NAME + "," +BIRTHDAY + "," + EMAIL+ "," + EDUCATION+ "," + WORK+ "," + ABOUT+ "," + GENDER);
            request.setParameters(parameters);
            request.executeAsync();
        }catch (PermissionDeniesException e)
        {
            doingRequiredPermission(e.pendingPermissions());
        }catch (FacebookException e) {
            FacebookUserDetails facebookUserDetails = new FacebookUserDetails();
            facebookUserDetails.setError(true);
            facebookUserDetails.setMessage(e.getMessage());
            RxJava2FBObservable.getInstance().publishData(facebookUserDetails);
        }
    }
    /*
    * Doing facebook login with required permission*/
    private void doingRequiredPermission(final ArrayList<String> pending)
    {
        FacebookManager.getInstance().loginWithReadPermission(pending, new FacebookManager.FacebookRequiredLogin()
        {
            @Override
            public void onSuccess()
            {
                getUserDetails(pending);
            }
            @Override
            public void onError(String message)
            {
                FacebookUserDetails facebookUserDetails = new FacebookUserDetails();
                facebookUserDetails.setError(true);
                facebookUserDetails.setMessage(message);
                RxJava2FBObservable.getInstance().publishData(facebookUserDetails);
            }
        });
    }


    /**
      *Facebook user image details. */
    private void collectOtherImageList(final FacebookUserDetails facebookUserDetails)
    {
        InterRxJava2Observable observable = InterRxJava2Observable.getObservable();
        Observer<JSONObject> observer = new Observer<JSONObject>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(JSONObject value)
            {
                try {
                    facebookUserDetails.setOtherPic(value.getString(FacebookCommonConstants.RESULT));
                    facebookUserDetails.setUserPic(value.getString(FacebookCommonConstants.PROFILE_PIC));
                    RxJava2FBObservable.getInstance().publishData(facebookUserDetails);
                } catch (JSONException e)
                {
                    e.printStackTrace();
                    facebookUserDetails.setError(true);
                    facebookUserDetails.setMessage(e.getMessage());
                    facebookUserDetails.setUserPic("http://graph.facebook.com/"+facebookUserDetails.getId()+"/picture?type=large");
                    RxJava2FBObservable.getInstance().publishData(facebookUserDetails);
                }
            }

            @Override
            public void onError(Throwable e)
            {
                e.printStackTrace();
                facebookUserDetails.setError(true);
                facebookUserDetails.setMessage(e.getMessage());
                facebookUserDetails.setUserPic("http://graph.facebook.com/"+facebookUserDetails.getId()+"/picture?type=large");
                RxJava2FBObservable.getInstance().publishData(facebookUserDetails);
            }

            @Override
            public void onComplete() {
                compositeDisposable.clear();
            }
        };
        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
        /*
         *Getting user details */
        getPictureAlbumList(observable);
    }

    /*
    *getting album list of the user  */
    private void getPictureAlbumList(final InterRxJava2Observable observable)
    {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response)
                    {

                        try {
                            if (response.getError() != null)
                            {
                                throwErrorOnObservable(observable, "" + response.getError().getErrorMessage());
                            } else
                            {
                                if (object.has(ALBUMS))
                                {
                                    JSONObject albums = object.getJSONObject(ALBUMS);
                                    JSONArray data = albums.getJSONArray(DATA);
                                    String album_id = getUserProfilePicAlbumId(data);
                                    if (album_id != null) {
                                        collectingImageFromAlbum(album_id, observable);
                                    } else {
                                        throwErrorOnObservable(observable, "Not able to collect user image");
                                    }
                                } else {
                                    throwErrorOnObservable(observable, "User has no photos");
                                }
                            }
                        } catch (Exception e) {
                            throwErrorOnObservable(observable, "" + e.getMessage());
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", ALBUMS);
        request.setParameters(parameters);
        request.executeAsync();
    }


    /*
    * Crating the album id*/
    private void collectingImageFromAlbum(String albumId, final InterRxJava2Observable observable) {
        GraphRequest request = GraphRequest.newGraphPathRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + albumId,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        try {
                            if (response.getError() != null) {
                                throwErrorOnObservable(observable, "" + response.getError().getErrorMessage());
                            } else {
                                JSONObject object = response.getJSONObject();
                                if (object.has(PHOTOS)) {
                                    JSONObject albums = object.getJSONObject(PHOTOS);
                                    collectBestQualityImage(albums.getJSONArray(DATA), observable);
                                } else {
                                    throwErrorOnObservable(observable, "User has no photo.");
                                }
                            }
                        } catch (Exception e) {
                            throwErrorOnObservable(observable, "" + e.getMessage());
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "photos.limit(5)");
        request.setParameters(parameters);
        request.executeAsync();
    }


    /*
     *collecting the best quality image */
    private void collectBestQualityImage(JSONArray data, final InterRxJava2Observable observable) throws JSONException
    {
        final int maxCount = data.length();
        final ArrayList<String> image_list = new ArrayList<>();
        final ArrayList<String> error_id_list = new ArrayList<>();
        for (int count = 0; count < maxCount; count++)
        {
            JSONObject jsonObject = data.getJSONObject(count);
            String id = jsonObject.getString(ID);
            collectBestQualityImage(id, new PictureCallback() {
                @Override
                public void onSuccess(String url) {
                    image_list.add(url);
                    int isCompleted = image_list.size() + error_id_list.size();
                    if (isCompleted >= maxCount) {
                        sendResult(image_list, observable);
                    }
                }

                @Override
                public void onError(String id, String message) {
                    error_id_list.add(id);
                    int isCompleted = image_list.size() + error_id_list.size();
                    if (isCompleted >= maxCount) {
                        sendResult(image_list, observable);
                    }
                }
            });
        }
    }


    private void sendResult(ArrayList<String> collected_list, InterRxJava2Observable observable) {
        if (collected_list.size() > 0) {
            throwSuccessOnObservable(observable,collected_list.get(0),collected_list);
        } else {
            throwErrorOnObservable(observable, "Not able to get any Image");
        }
    }

    /*
     *Getting the best quality image from Image id. */
    private void collectBestQualityImage(final String imageId, final PictureCallback callback) {

        GraphRequest request = GraphRequest.newGraphPathRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + imageId,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        try {
                            if (response.getError() != null) {
                                callback.onError(imageId, response.getError().getErrorMessage());
                            } else {
                                JSONObject object = response.getJSONObject();
                                if (object.has(IMAGES)) {
                                    JSONArray actualImageList = object.getJSONArray(IMAGES);
                                    JSONObject actualImage = actualImageList.getJSONObject(0);
                                    callback.onSuccess(actualImage.getString(SOURCE));
                                } else {
                                    callback.onError(imageId, "Error on collecting.");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            callback.onError(imageId, e.getMessage());
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", IMAGES);
        request.setParameters(parameters);
        request.executeAsync();
    }

    /**
     * <h2>create_other_image_list</h2>
     * <p>
     * Method is use to make the other image list as a single String link with " ," separater.
     * First receving the Image list and  by the help of loop merging them in a single string.
     * Note : Max size of other link is is 5;
     * first setting the size if list size is greater then five then makinh the max size is five else
     * setting max size as array size.
     * </P>
     *
     * @param picture_list contains the list of of other image list .
     */
    private String create_other_image_list(ArrayList<String> picture_list) {
        StringBuilder other_images = new StringBuilder();
        if (picture_list != null)
        {
            int maxsize = picture_list.size();
            for (int i = 0; i < 5 && i < maxsize; i++) {
                other_images.append(picture_list.get(i));
                if (maxsize > 1 && i < maxsize - 1) {
                    other_images.append(",");
                }
            }
        }
        return other_images.toString();
    }

    /*
     *getting the user related album id to extract image id.*/
    private String getUserProfilePicAlbumId(JSONArray jsonArray) throws JSONException {
        String album_id = null;
        for (int count = 0; count < jsonArray.length(); count++) {
            JSONObject item_data = jsonArray.getJSONObject(count);
            if (item_data.getString(NAME).equals("Profile Pictures")) {
                album_id = item_data.getString(ID);
                break;
            }
        }
        return album_id;
    }

    /*
    * Checking the required permission is granted or not.*/
    private void checkPermission(ArrayList<String> required) throws PermissionDeniesException
    {
        Set<String> list = AccessToken.getCurrentAccessToken().getPermissions();
        ArrayList<String> list_Data = new ArrayList<>(list);
        if(list_Data.size()<1)
        {
            throw new PermissionDeniesException(required);
        }else
        {
            ArrayList<String> pending_list=new ArrayList<>();
            for (int count = 0;count<required.size(); count++)
            {
                if(!list_Data.contains(required.get(count)))
                {
                    pending_list.add(required.get(count));
                }
            }
            if (pending_list.size()>0)
            {
                throw new PermissionDeniesException(pending_list);
            }
        }
    }


    private void isLoggedIn() throws FacebookException {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken == null) {
            throw new FacebookException(FacebookCommonConstants.ERROR + R.string.login_error);
        }
    }

    /*
     *Sending error on every service call for the api access. */
    private void throwErrorOnObservable(InterRxJava2Observable observable, String message) {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(FacebookCommonConstants.ERROR, true);
            jsonObject.put(FacebookCommonConstants.MESSAGE, "" + message);
            observable.publishData(jsonObject);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }

    /*
    *Sending error on every service call for the api access. */
    private void throwSuccessOnObservable(InterRxJava2Observable observable,String profile_pic,ArrayList<String> otherImages) {
        try {
            JSONArray jsArray = new JSONArray(otherImages);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(FacebookCommonConstants.ERROR, false);
            jsonObject.put(FacebookCommonConstants.MESSAGE, "Success");
            jsonObject.put(FacebookCommonConstants.RESULT, jsArray);
            jsonObject.put(FacebookCommonConstants.PROFILE_PIC, profile_pic);
            observable.publishData(jsonObject);
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }

    /*
     *Picture callback details*/
    interface PictureCallback
    {
        void onSuccess(String url);
        void onError(String id, String message);
    }
}
