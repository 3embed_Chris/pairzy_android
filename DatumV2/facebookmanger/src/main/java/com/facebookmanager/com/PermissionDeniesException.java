package com.facebookmanager.com;

import java.util.ArrayList;
/**
 * @since 3/1/2018.
 * @version 1.0.
 * @author Suresh.
 */
public class PermissionDeniesException extends Exception
{
    private ArrayList<String> permission;
    PermissionDeniesException(ArrayList<String> permission)
    {
        this.permission = permission;
    }
    public ArrayList<String> pendingPermissions()
    {
        return permission;
    }
    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
