package com.three_embed.com.circule_image_cropper;

/**
 *@since 05/04/16.
 */
public enum CircleCutterShape {

    CIRCLE,
    HOLE,
    SQUARE;
}
