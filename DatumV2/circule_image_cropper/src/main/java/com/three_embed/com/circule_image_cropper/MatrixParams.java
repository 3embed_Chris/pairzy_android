package com.three_embed.com.circule_image_cropper;

import android.graphics.Matrix;

/**
 * @since  01/04/16.
 */
 class MatrixParams {

    private float x;
    private float y;
    private float scaleWidth;
    private float scaleHeight;

     static MatrixParams fromMatrix(Matrix matrix) {
        float[] matrixValues = new float[9];
        matrix.getValues(matrixValues);
        MatrixParams matrixParams = new MatrixParams();
        matrixParams.x = matrixValues[2];
        matrixParams.y = matrixValues[5];
        matrixParams.scaleWidth = matrixValues[0];
        matrixParams.scaleHeight = matrixValues[4];
        return matrixParams;
    }

    public float getX()
    {
        return x;
    }

    public float getY()
    {
        return y;
    }

     float getScaleWidth()
     {
        return scaleWidth;
    }

     float getScaleHeight()
     {
        return scaleHeight;
    }
}
