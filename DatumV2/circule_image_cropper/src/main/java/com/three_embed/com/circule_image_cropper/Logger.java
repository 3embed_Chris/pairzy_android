package com.three_embed.com.circule_image_cropper;

import android.util.Log;


/**
 * @since 31/03/16.
 */
 class Logger {

    private static boolean sEnabled = false;

    public static void log(String message) {
        if (BuildConfig.DEBUG || sEnabled) {
            Log.d("circle-cropper", message);
        }
    }

    public static void setEnabled(boolean sEnabled) {
        Logger.sEnabled = sEnabled;
    }
}
