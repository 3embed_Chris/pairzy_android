package com.videocompressor.com;
/**
 * @since  3/15/2018.
 * @author Sursh.
 * @version 1.0.
 */
public class Sample {
    private long offset = 0;
    private long size = 0;

    public Sample(long offset, long size) {
        this.offset = offset;
        this.size = size;
    }

    public long getOffset() {
        return offset;
    }

    public long getSize() {
        return size;
    }
}
