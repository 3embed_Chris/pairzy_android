package com.videocompressor.com;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import com.videocompressor.com.DataModel.CompressedData;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
/**
 * <h2>VideoCompressor</h2>
 * <P>
 *
 * </P>
 * @since  4/18/2018.
 * @author 3Embed.
 * @version 1.0.
 */
public class VideoCompressor
{
    private Context context;
    public VideoCompressor(Context context)
    {
        this.context=context;
    }
    public RxCompressObservable compressVideo(String video_file)
    {
        RxCompressObservable observable=RxCompressObservable.getInstance();
        new CompressVideo().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,video_file);
        return observable;
    }

    @SuppressLint("StaticFieldLeak")
    private class CompressVideo extends AsyncTask<String,Void,File>
    {
        private boolean error;
        private String errorText;
        @Override
        protected File doInBackground(String... strings)
        {
            try
            {
                String file_path=strings[0];
                File file=getTargetFile();
                if(MediaController.getInstance().convertVideo(file_path,file.getPath()))
                {
                    return MediaController.cachedFile;
                }else
                {
                    copyFile(new File(file_path),file);
                }
                return file;
            }catch (Exception e)
            {
                errorText=e.getMessage();
                error=true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(File file)
        {
            super.onPostExecute(file);
            CompressedData compressedData=new CompressedData();
            if(error)
            {
                compressedData.setError(true);
                compressedData.setMessage(errorText);
            }
            else
            {
                compressedData.setError(false);
                compressedData.setPath(file.getPath());
            }
            if(RxCompressObservable.getInstance()!=null)
                RxCompressObservable.getInstance().publishData(compressedData);
        }
    }

    /*
     * Coping a file in android.*/
    private void copyFile(File src, File dst) throws IOException
    {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0)
                {
                    out.write(buf, 0, len);
                }
            } finally
            {
                out.close();
            }
        } finally {
            in.close();
        }
    }

    /*
     * Getting the target store file after compression.*/
    private File getTargetFile()
    {
        String targetDir=CompressorFileUtils.getInstance().dirVideoDir(context);
        return new File(targetDir,
                "VIDEO_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date()) + ".mp4");
    }
}
