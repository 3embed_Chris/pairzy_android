package com.videocompressor.com;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import com.coremedia.iso.boxes.Container;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
/**
 * <h2>File_merger</h2>
 * <P>
 *     Validating the video file then merging the video file.
 * </P>
 * @author Suresh.
 * @version 1.0.
 * @since  4/3/2017.
 */
class File_merger
{
    private Merger_callback callback;
    private static File_merger FILE_MERGER;
    public static File_merger getInstance()
    {
        if(FILE_MERGER==null)
        {
            FILE_MERGER=new File_merger();
        }
        return FILE_MERGER;
    }
    /*
    * Merging the file.*/
    public void mergeMediaFiles(Context context, boolean isAudio, String sourceFiles[], Merger_callback mcallback)
    {
        callback=mcallback;
        if(sourceFiles.length<1)
        {
            callback.onError("Provide file to merge file list is empty");
        }else
        {
            /*
            * Validating the all the files before merging.*/
            ArrayList<String> validSources=new ArrayList<>();
            for (String sourceFile : sourceFiles)
            {
                if (isValidFile(sourceFile))
                {
                    validSources.add(sourceFile);
                }
            }
            if(validSources.size()>1)
            {
                /*Merging the video files.*/
                Data_holder data_holder=new Data_holder();
                data_holder.targetDir=CompressorFileUtils.getInstance().dirMergedDir(context);
                data_holder.isAudio=isAudio;
                data_holder.sourceFiles=validSources.toArray(new String[0]);
                new File_merger_task().execute(data_holder);
            }else if(validSources.size()>0)
            {
                callback.onSuccess(validSources.get(0));
            }else
            {
                callback.onError("Given files are too small to merge for ie. less then 1kb.");
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class File_merger_task extends AsyncTask<Data_holder,Void,String>
    {
        @Override
        protected String doInBackground(Data_holder... params)
        {
            Data_holder holder=params[0];
            String targetFile=getTargetFile(holder.targetDir).getPath();
            Boolean isFound=mergeFiles(holder.sourceFiles,targetFile);
            if(isFound)
            {
                deleteTempFile(holder.sourceFiles);
                return targetFile;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String path_file)
        {
            super.onPostExecute(path_file);
            if(path_file!=null)
            {
                if(callback!=null)
                {
                    callback.onSuccess(path_file);
                }

            }else
            {
                if(callback!=null)
                {
                    callback.onError("Error happened on merging!");
                }
            }
        }
    }

    /*
     * Video callback for the app.*/
    public interface Merger_callback
    {
        /*
        * On Success result*/
        void onSuccess(String file_path);
        /*
        * On Error.*/
        void onError(String error);
    }
    /*
    * Merger data holder.*/
    private class Data_holder
    {
        String targetDir;
        boolean isAudio;
        String sourceFiles[];
    }

    /*
    * Merging the file.*/
    private boolean mergeFiles(String sourceFiles[], String targetFile)
    {
        try {
            List<Movie> listMovies = new ArrayList<>();
            for (String filename : sourceFiles) {
                listMovies.add(MovieCreator.build(filename));
            }
            List<Track> videoTracks = new LinkedList<>();
            List<Track> audioTracks = new LinkedList<>();
            for (Movie movie : listMovies)
            {
                for (Track track : movie.getTracks())
                {
                    if (track.getHandler().equals("vide")) {
                        videoTracks.add(track);
                    }
                    if (track.getHandler().equals("soun"))
                    {
                        audioTracks.add(track);
                    }
                }
            }
            Movie outputMovie = new Movie();
            if (!videoTracks.isEmpty()) {
                outputMovie.addTrack(new AppendTrack(videoTracks.toArray(new Track[videoTracks.size()])));
            }
            if (!audioTracks.isEmpty()) {
                outputMovie.addTrack(new AppendTrack(audioTracks.toArray(new Track[audioTracks.size()])));
            }
            Container container = new DefaultMp4Builder().build(outputMovie);
            FileChannel fileChannel = new RandomAccessFile(targetFile, "rw").getChannel();
            container.writeContainer(fileChannel);
            fileChannel.close();
            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }
    /*
     * If the file length is greater then 20 kb
     * then compressing the Image file */
    private boolean isValidFile(String file_path)
    {
        File file = new File(file_path);
        long fileSizeInBytes = file.length();
        long fileSizeInKB = fileSizeInBytes / 1024;
        return fileSizeInKB >1;
    }
    /*
     * deleting the file given for merge.*/
    private void deleteTempFile(String sourceFiles[])
    {
        File temp;
        for (String sourceFile : sourceFiles) {
            temp = new File(sourceFile);
            if (temp.exists()) {
                temp.delete();
            }
        }
    }

    /*
   * Getting the target store file after compression.*/
    private File getTargetFile(String targetDir)
    {
        File file =new File(targetDir,
                "VIDEO_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date()) + ".mp4");
        return file;
    }

}
