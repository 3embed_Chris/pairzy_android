package com.videocompressor.com.DataModel;

/**
 * @since  3/15/2018.
 * @author Sursh.
 * @version 1.0.
 */
public class CompressedData
{
   private boolean isError=false;
   private String path;
   private String  message;

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
