package com.videocompressor.com;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.os.AsyncTask;
import com.videocompressor.com.DataModel.CompressedData;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
/**
 * <h2>CompressImage</h2>
 * <P>
 *  Compressing the image.
 * </P>
 * @see ImageLoadingUtils
 * @author Suresh.
 * @since 22/6/16.
 */
public class CompressImage
{
    private ImageLoadingUtils utils;

    public CompressImage()
    {
        utils =new ImageLoadingUtils();
    }
    /**
     * <h2>compressImage</h2>
     * <P>
     *  Compressed file path to add the file path.
     * </P>
     * @param image_file_path  image file path to compressed the image.
     * @return  RxCompressObservable call back that gives the compressed file path.
     * */
    public RxCompressObservable compressImage(Context context,String image_file_path)
    {
        RxCompressObservable observable=RxCompressObservable.getInstance();
        DataHolder dataHolder=new DataHolder();
        dataHolder.filePath=image_file_path;
        dataHolder.targetDir=CompressorFileUtils.getInstance().dirImageDir(context);
        dataHolder.callback=observable;
        if(isValidToCompressed(image_file_path))
        {
            new ImageCompressionAsyncTask().execute(dataHolder);
        }else
        {
            String filepath =getTargetFile(dataHolder.targetDir).getPath();
            File dest=new File(filepath);
            File source=new File(image_file_path);
            String destination;
            try
            {
                copyFile(source,dest);
                destination=dest.getPath();
            } catch (Exception e)
            {
                destination=image_file_path;
            }
            CompressedData compressedData=new CompressedData();
            compressedData.setError(false);
            compressedData.setPath(destination);
            observable.publishData(compressedData);
        }
        return observable;
    }

    /*
     * Coping a file in android.*/
    private void copyFile(File src, File dst) throws IOException
    {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0)
                {
                    out.write(buf, 0, len);
                }
            } finally
            {
                out.close();
            }
        } finally {
            in.close();
        }
    }

    /**
     * <h2>ImageCompressionAsyncTask</h2>
     * <P>
     *     Image compressing task.
     * </P>*/
    @SuppressLint("StaticFieldLeak")
    private class ImageCompressionAsyncTask extends AsyncTask<DataHolder, Void, String>
    {
        boolean error=false;
        String errorText="";
        RxCompressObservable observable=null;
        @Override
        protected String doInBackground(DataHolder... params)
        {
            observable=params[0].callback;
            return compressImage(params[0].filePath,params[0].targetDir);
        }
        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);
            CompressedData compressedData=new CompressedData();
            if(error)
            {
                compressedData.setError(true);
                compressedData.setMessage(errorText);
            }
            else
            {
                compressedData.setError(false);
                compressedData.setPath(result);
            }
            if(observable!=null)
                observable.publishData(compressedData);
        }

        /*
      * actually compressing image*/
        String compressImage(String filePath,String targetDir)
        {
            Bitmap scaledBitmap = null;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(filePath,options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;
            float maxHeight = 816.0f;
            float maxWidth = 612.0f;
            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;

            if (actualHeight > maxHeight || actualWidth > maxWidth)
            {
                if (imgRatio < maxRatio)
                {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;

                }
            }
            options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight);
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16*1024];

            try
            {
                bmp = BitmapFactory.decodeFile(filePath,options);
            }
            catch(OutOfMemoryError exception)
            {
                error=true;
                errorText=exception.toString();
                exception.printStackTrace();
            }
            try
            {
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
            }
            catch(OutOfMemoryError exception)
            {
                error=true;
                errorText=exception.toString();
                exception.printStackTrace();
            }
            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float)options.outHeight;
            float middleX = actualWidth / 2.0f;
            float middleY = actualHeight / 2.0f;
            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
            assert scaledBitmap != null;
            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth()/2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

            ExifInterface exif;
            try {
                exif = new ExifInterface(filePath);
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
            } catch (IOException e)
            {
                error=true;
                errorText=e.toString();
                e.printStackTrace();
            }
            FileOutputStream out;
            String filepath =getTargetFile(targetDir).getPath();
            try
            {
                out = new FileOutputStream(filepath);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 95, out);

            } catch (FileNotFoundException e)
            {
                error=true;
                errorText=e.toString();
                e.printStackTrace();
            }
            return filepath;
        }
    }
    /*
    * Temp data holder*/
    private class DataHolder
    {
        String filePath;
        String targetDir;
        RxCompressObservable callback;
    }

    /*
     * If the file length is greater then 20 kb
     * then compressing the Image file */
    public static boolean isValidToCompressed(String file_path)
    {
        File file = new File(file_path);
        long fileSizeInBytes = file.length();
        long fileSizeInKB = fileSizeInBytes / 1024;
        return fileSizeInKB >20;
    }

    /*
    * Getting the target store file after compression.*/
    private File getTargetFile(String targetDir)
    {
        File file =new File(targetDir,
                "IMAGE_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date()) + ".jpg");
        return file;
    }
}
