package com.androidinsta.com;

/**
 * @since  12/12/2017.
 * @author Suresh.
 * @version 1.0.
 */
class RequestToken
{
    private String clientId;
    private String clientSecret;
    private String authCallback;
    String getClientId()
    {
        return clientId;
    }
    void setClientId(String clientId)
    {
        this.clientId = clientId;
    }
    String getClientSecret()
    {
        return clientSecret;
    }
    void setClientSecret(String clientSecret)
    {
        this.clientSecret = clientSecret;
    }
    String getAuthCallback()
    {
        return authCallback;
    }
    void setAuthCallback(String authCallback)
    {
        this.authCallback = authCallback;
    }
    String getAuthUrl()
    {
        return "https://api.instagram.com/oauth/authorize/?client_id=" +
                clientId +
                "&" +
                "redirect_uri=" +
                authCallback +
                "&" +
                "response_type=code";
    }

    String getAccessTokenUrl()
    {
        return "https://api.instagram.com/oauth/authorize/?client_id="+
                clientId+
                "&"+
                "redirect_uri="+
                authCallback+
                "&"+
                "response_type=token";
    }

    String getDetailsUrl(String accessToken)
    {
        return "https://api.instagram.com/v1/users/self/?access_token="+accessToken;
    }
}
