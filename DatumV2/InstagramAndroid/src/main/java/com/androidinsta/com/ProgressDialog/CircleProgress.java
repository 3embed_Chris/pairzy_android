package com.androidinsta.com.ProgressDialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import com.androidinsta.com.R;
/**
 * @since 1/11/2018.
 */
public class CircleProgress
{
    private Dialog progress_bar = null;
    private Activity activity;
    public CircleProgress(Activity activity)
    {
        this.activity=activity;
        init(activity);
    }

    /*
     *inti dialog content*/
    private void init(Activity mActivity)
    {
        progress_bar = new Dialog(mActivity, android.R.style.Theme_Translucent);
        progress_bar.setCancelable(false);
        progress_bar.requestWindowFeature(Window.FEATURE_NO_TITLE);
        @SuppressLint("InflateParams")
        View dialogView = LayoutInflater.from(mActivity).inflate(R.layout.progress_bar_view, null);
        progress_bar.setContentView(dialogView);
        progress_bar.setCancelable(true);
        progress_bar.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                //Canceled..
            }
        });
    }


    public void show()
    {
        if (progress_bar != null)
        {
            if (progress_bar.isShowing())
            {
                progress_bar.dismiss();
            }
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progress_bar.show();
                }
            });
        }
    }


    public void cancel()
    {
        try
        {
            if (progress_bar.isShowing()) {
                progress_bar.dismiss();
            }
        }catch (Exception e){}

    }

}
