package com.androidinsta.com;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import androidx.fragment.app.Fragment;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
/**
 * <h2>InstagramManger</h2>
 * Handling the user result data of the access details .
 * @since  12/12/2017.
 * @version 1.0.
 * @author  Suresh.
 */
public class InstagramManger
{
    private InstagramSession instagramSession;


    public InstagramManger(Context context, String instaClientKey, String instaSecrestKey)
    {
        instagramSession =new InstagramSession(context);
        InstagramConfig.INSTAGRAM_CONSUMER_KEY =instaClientKey;
        InstagramConfig.INSTAGRAM_CONSUMER_SECRET =instaSecrestKey;
    }

    /*
     *Redirection url */
    public void setRedirectURl(String url)
    {
        InstagramConfig.INSTAGRAM_CALLBACK_URL =url;
    }

    public void loggedOut()
    {
        InstagramUtil.getInstance().logout();
        instagramSession.clear();
    }


    public boolean isUserLoggedIn()
    {
        return instagramSession.getInstagramLoginStatus();
    }


    public InstaLoginHolder getUserDetails() throws Exception
    {
        if(instagramSession.getInstagramLoginStatus())
        {
            InstaLoginHolder data_temp=new InstaLoginHolder();
            data_temp.setProfile_pic(instagramSession.getUserPic());
            data_temp.setToken(instagramSession.getAccessToken());
            data_temp.setUser_id(instagramSession.getId());
            data_temp.setUser_name(instagramSession.getUserName());
            data_temp.setFull_name(instagramSession.getUserFullName());
            return data_temp;
        }else
        {
           throw new Exception("Login first!");
        }
    }

    /*
     *handling the on activity result on the Instagram request. */
    public boolean onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode==InstagramUtil.getInstance().getREQUEST_CODE())
        {
            RxJava2LoginObservable observable=RxJava2LoginObservable.getInstance();
            InstaLoginHolder loginHolder=new InstaLoginHolder();

            if(resultCode==Activity.RESULT_OK)
            {
                boolean isLoggedIn=data.getBooleanExtra("ISLOGIN",false);
                loginHolder.setError(!isLoggedIn);
                loginHolder.setErrorMessage(data.getStringExtra("Message"));
                loginHolder.setFull_name(instagramSession.getUserFullName());
                loginHolder.setProfile_pic(instagramSession.getUserPic());
                loginHolder.setUser_name(instagramSession.getUserName());
                loginHolder.setUser_id(instagramSession.getId());
                loginHolder.setToken(instagramSession.getAccessToken());
                observable.publishData(loginHolder);
            }else if(resultCode==Activity.RESULT_CANCELED)
            {
                loginHolder.setError(true);
                String message="";
                if(data!=null&&data.hasExtra("Message"))
                {
                    message=data.getStringExtra("Message");
                }else
                {
                    message="Canceled!";
                }
                loginHolder.setErrorMessage(message);
                observable.publishData(loginHolder);
            }
            return true;
        }
        return false;
    }



    @SuppressLint("StaticFieldLeak")
    public RxJava2LoginObservable doLogin(final Activity activity,final Fragment fragment)
    {
        InstagramUtil.getInstance().reset();
        RxJava2LoginObservable observable=RxJava2LoginObservable.getInstance();
        if(activity==null&&fragment==null)
        {
            InstaLoginHolder error=new InstaLoginHolder();
            error.setError(true);
            error.setFull_name(null);
            error.setErrorMessage("Error : Need a Activity context or Fragment context for login.");
        }else
        {
            new AsyncTask<RxJava2LoginObservable,String,RequestToken>()
            {
                @Override
                protected void onPostExecute(RequestToken requestToken)
                {
                    if(fragment!=null)
                    {
                        Intent intent = new Intent(fragment.getContext().getApplicationContext(),OAuthActivity.class);
                        intent.putExtra(InstagramConfig.STRING_EXTRA_AUTHENCATION_URL,requestToken.getAuthUrl());
                        fragment.startActivityForResult(intent,InstagramUtil.getInstance().getREQUEST_CODE());
                    }else
                    {
                        Intent intent = new Intent(activity.getApplicationContext(),OAuthActivity.class);
                        intent.putExtra(InstagramConfig.STRING_EXTRA_AUTHENCATION_URL,requestToken.getAuthUrl());
                        activity.startActivityForResult(intent,InstagramUtil.getInstance().getREQUEST_CODE());
                    }
                }
                @Override
                protected RequestToken doInBackground(RxJava2LoginObservable... params)
                {
                    return InstagramUtil.getInstance().getRequestToken();
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,observable);
        }
        return observable;
    }


    @SuppressLint("StaticFieldLeak")
    public RxJava2Observable getOtherPostDetails(String userId,String userInstaToken)
    {
        RxJava2Observable rxJava2Observable=RxJava2Observable.getInstance();
//        if(!instagramSession.getInstagramLoginStatus())
//        {
//            InstaReponseHolder error=new InstaReponseHolder();
//            error.setImageData(null);
//            error.setError(true);
//            error.setErrorMessage("Error : Login first to get complete the request.");
//            rxJava2Observable.publishData(error);
//        }else
//        {
            new AsyncTask<Object,String,InstaReponseHolder>()
            {
                private RxJava2Observable Observable;
                @Override
                protected InstaReponseHolder doInBackground(Object... params)
                {
                    String user_id=(String)params[0];
                    String userInstaToken = (String) params[1];
                    Observable= (RxJava2Observable) params[2];
                    try
                    {
                        //TODO need to check
//                        if(!InstagramUtil.getInstance().getInstagram().isAccessTokenCreated())
//                        {
//                            AccessToken accessToken=InstagramUtil.getInstance().getInstagram().recreateAccessToke(instagramSession);
//                            InstagramUtil.getInstance().getInstagram().setOAuthAccessToken(accessToken);
//                        }
                        String result=InstagramUtil.getInstance().getInstagram().getOtherRecentPost(user_id,userInstaToken);
                        return createUserResponse(result);
                    } catch (InstagramException e)
                    {
                        InstaReponseHolder error=new InstaReponseHolder();
                        error.setImageData(null);
                        error.setError(true);
                        error.setErrorMessage(e.getMessage());
                        e.printStackTrace();
                        return error;
                    }
                }

                @Override
                protected void onPostExecute(InstaReponseHolder result)
                {
                    if(Observable!=null)
                    {
                        Observable.publishData(result);
                    }
                }

            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,userId,userInstaToken,rxJava2Observable);
        //}
        return rxJava2Observable;
    }


    @SuppressLint("StaticFieldLeak")
    public RxJava2Observable getUserRecentPost()
    {
        RxJava2Observable rxJava2Observable=RxJava2Observable.getInstance();
        if(!instagramSession.getInstagramLoginStatus())
        {
            InstaReponseHolder error=new InstaReponseHolder();
            error.setImageData(null);
            error.setError(true);
            error.setErrorMessage("Error : Login first to get complete the request.");
            rxJava2Observable.publishData(error);
        }else
        {
            new AsyncTask<Object,String,InstaReponseHolder>()
            {
                private RxJava2Observable Observable;
                @Override
                protected InstaReponseHolder doInBackground(Object... params)
                {
                    Observable= (RxJava2Observable) params[0];
                    try
                    {
                        if(!InstagramUtil.getInstance().getInstagram().isAccessTokenCreated())
                        {
                            AccessToken accessToken=InstagramUtil.getInstance().getInstagram().recreateAccessToke(instagramSession);
                            InstagramUtil.getInstance().getInstagram().setOAuthAccessToken(accessToken);

                        }
                        String result=InstagramUtil.getInstance().getInstagram().getUserRecentPost();
                        return createUserResponse(result);
                    } catch (InstagramException e)
                    {
                        InstaReponseHolder error=new InstaReponseHolder();
                        error.setImageData(null);
                        error.setError(true);
                        error.setErrorMessage(e.getMessage());
                        e.printStackTrace();
                        return error;
                    }
                }
                @Override
                protected void onPostExecute(InstaReponseHolder result)
                {
                    if(Observable!=null)
                    {
                        Observable.publishData(result);
                    }
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,rxJava2Observable);
        }
        return rxJava2Observable;
    }

    /*
     *parsing the result data */
    private InstaReponseHolder createUserResponse(String response) throws InstagramException
    {
        InstaReponseHolder result_data=new InstaReponseHolder();
        ArrayList<ImageData> dataList=new ArrayList<>();
        result_data.setImageData(dataList);
        try
        {
            JSONObject jsonObject=new JSONObject(response);
            JSONArray data=jsonObject.getJSONArray("data");
            for(int count=0;count<data.length();count++)
            {
             JSONObject item_object=data.getJSONObject(count);
             String types=item_object.getString("type");

             if(types.equals("image"))
             {
                 JSONObject imagesJson=item_object.getJSONObject("images");
                 ImageData imageTemp=new ImageData();
                 if(imagesJson.has("low_resolution"))
                 {
                     JSONObject loweResObject=imagesJson.getJSONObject("low_resolution");
                     String url=loweResObject.getString("url");
                     imageTemp.setLow_resolution(url);
                 }
                 if(imagesJson.has("thumbnail"))
                 {
                     JSONObject thumbnailObject=imagesJson.getJSONObject("thumbnail");
                     String url=thumbnailObject.getString("url");
                     imageTemp.setThumbnail(url);
                 }
                 if(imagesJson.has("standard_resolution"))
                 {
                     JSONObject standeredResObject=imagesJson.getJSONObject("standard_resolution");
                     String url=standeredResObject.getString("url");
                     imageTemp.setStandard_resolution(url);
                 }
                 dataList.add(imageTemp);
             }else if(types.equals("video"))
             {
                 //later we will do
             }
            }
        } catch (Exception e)
        {
            throw new InstagramException("Error :"+e.getMessage());
        }
        return result_data;
    }



    public void openInInstagram(Activity activity,String user_id,String user_name)
    {
        Uri uri = Uri.parse("http://instagram.com/_u/"+user_id);
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
        likeIng.setPackage("com.instagram.android");
        try {
            activity.startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://instagram.com/"+user_name)));
        }
    }


    public String getLoggedInUserID()
    {
        if(instagramSession.getInstagramLoginStatus())
        {
            AccessToken accessToken=InstagramUtil.getInstance().getInstagram().recreateAccessToke(instagramSession);
            return accessToken.getUSER_ID();
        }else
        {
            return null;
        }
    }

    public String getLoggedInUserName()
    {
        if(instagramSession.getInstagramLoginStatus())
        {
            AccessToken accessToken=InstagramUtil.getInstance().getInstagram().recreateAccessToke(instagramSession);
            return accessToken.getUSER_NAME();
        }else
        {
            return null;
        }
    }
}

