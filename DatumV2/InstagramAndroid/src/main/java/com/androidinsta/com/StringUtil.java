package com.androidinsta.com;

/**
 * <h2>StringUtil</h2>
 * <P>
 *
 * </P>
 * @since  12/12/2017.
 * @author Suresh.
 * @version 1.0.
 */
public class StringUtil
{
    public StringUtil() {
    }
    static boolean isNullOrWhitespace(String string) {
        return string == null || string.isEmpty() || string.trim().isEmpty();
    }
}
