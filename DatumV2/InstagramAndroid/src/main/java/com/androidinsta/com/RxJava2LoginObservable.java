package com.androidinsta.com;

import io.reactivex.Observable;
import io.reactivex.Observer;
/**
 * <h2>RxJava2LoginObservable</h2>
 *
 * @since  12/13/2017.
 * @author Suresh.
 * @version 1.0.
 */
public class RxJava2LoginObservable extends Observable<InstaLoginHolder>
{
    private RxJava2LoginObservable(){}
    private static RxJava2LoginObservable rxJava2Observable=null;
    static RxJava2LoginObservable getInstance()
    {
        if(rxJava2Observable==null)
        {
            rxJava2Observable=new RxJava2LoginObservable();
        }
        return rxJava2Observable;
    }
    private Observer<?super InstaLoginHolder> observer;
    @Override
    protected void subscribeActual(Observer<? super InstaLoginHolder> observer)
    {
        this.observer=observer;
    }

    void publishData(InstaLoginHolder data)
    {
        if(observer!=null&&data!=null)
        {
            if(data.isError())
            {
                observer.onError(new Throwable(data.getErrorMessage()));
            }else
            {
                observer.onNext(data);
            }
            observer.onComplete();
        }
    }
}

