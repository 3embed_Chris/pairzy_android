package com.androidinsta.com;
import android.content.Context;
import android.content.SharedPreferences;
/**
 * <h2>InstagramSession</h2>
 * <P>
 *
 * </P>
 * @since  12/12/2017.
 * @author Suresh.
 * @version 1.0.
 */
public class InstagramSession
{
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public InstagramSession(Context context)
    {
        String SK_TITLE = "Instagram_sk";
        /*
          Mod is private.*/
        int PREFERENCE_MODE = 0;
        sharedPreferences=context.getSharedPreferences(SK_TITLE, PREFERENCE_MODE);
        editor=sharedPreferences.edit();
        editor.commit();
    }

    void clear()
    {
        editor.clear();
        editor.commit();
    }


    boolean getInstagramLoginStatus()
    {
        return sharedPreferences.getBoolean(InstagramConfig.PREFERENCE_INSTAGRAM_IS_LOGGED_IN,false);
    }

    void setInstagramLoginStatus(boolean login_status)
    {
        editor.putBoolean(InstagramConfig.PREFERENCE_INSTAGRAM_IS_LOGGED_IN,login_status);
        editor.commit();
    }

    String getAccessToken()
    {
        return sharedPreferences.getString(InstagramConfig.PREFERENCE_INSTAGRAM_ACESSTOKEN,"");
    }

    void setAccessToken(String token)
    {
        editor.putString(InstagramConfig.PREFERENCE_INSTAGRAM_ACESSTOKEN,token);
        editor.commit();
    }

    String getId()
    {
        return sharedPreferences.getString(InstagramConfig.PREFERENCE_INSTAGRAM_ID,"");
    }

    void setId(String id)
    {
        editor.putString(InstagramConfig.PREFERENCE_INSTAGRAM_ID,id);
        editor.commit();
    }

    String getUserName()
    {
        return sharedPreferences.getString(InstagramConfig.PREFERENCE_INSTAGRAM_USERNMAE,"");
    }

    void setUserName(String id)
    {
        editor.putString(InstagramConfig.PREFERENCE_INSTAGRAM_USERNMAE,id);
        editor.commit();
    }

    String getUserFullName()
    {
        return sharedPreferences.getString(InstagramConfig.PREFERENCE_INSTAGRAM_FULLNAME,"");
    }

    void setUserFullName(String name)
    {
        editor.putString(InstagramConfig.PREFERENCE_INSTAGRAM_FULLNAME,name);
        editor.commit();
    }

    String getUserWeb()
    {
        return sharedPreferences.getString(InstagramConfig.PREFERENCE_INSTAGRAM_WEB,"");
    }

    void setUserWeb(String name)
    {
        editor.putString(InstagramConfig.PREFERENCE_INSTAGRAM_WEB,name);
        editor.commit();
    }

    String getUserBio()
    {
        return sharedPreferences.getString(InstagramConfig.PREFERENCE_INSTAGRAM_BIO,"");
    }

    void setUserBio(String bio)
    {
        editor.putString(InstagramConfig.PREFERENCE_INSTAGRAM_BIO,bio);
        editor.commit();
    }

    String getUserPic()
    {
        return sharedPreferences.getString(InstagramConfig.PREFERENCE_INSTAGRAM_PIC,"");
    }

    void setUserPic(String pic)
    {
        editor.putString(InstagramConfig.PREFERENCE_INSTAGRAM_PIC,pic);
        editor.commit();
    }


    public String getAuthenticatedAppName()
    {
        return sharedPreferences.getString("APPNAME","");
    }

    public void setAuthenticatedAppName(String app_name)
    {
        editor.putString("APPNAME", app_name);
        editor.commit();
    }

}