package com.androidinsta.com;
import java.util.ArrayList;
/**
 * @since  3/15/2018.
 * @author Suresh.
 * @version 1.0.
 */
public class InstaReponseHolder
{
    private boolean isError;
    private String errorMessage;
    private ArrayList<ImageData> imageData;
    public ArrayList<ImageData> getImageData()
    {
        return imageData;
    }
    public boolean getError()
    {
        return isError;
    }

    public String getErrorMessage()
    {
        return errorMessage;
    }
    void setImageData(ArrayList<ImageData> imageData) {
        this.imageData = imageData;
    }
     void setError(boolean isError)
    {
        this.isError=isError;
    }

    void setErrorMessage(String message)
    {
        this.errorMessage=message;
    }

}

