package com.androidinsta.com;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * <h2>ImageData</h2>
 * @since  12/13/2017.
 * @author Suresh.
 * @version 1.0.
 */
public class ImageData implements Parcelable
{
    private String id;
    private String thumbnail;
    private String standard_resolution;
    private String low_resolution;
    private int pagePosition;

    public ImageData(){
    }

    public ImageData(Parcel in) {
        id = in.readString();
        thumbnail = in.readString();
        standard_resolution = in.readString();
        low_resolution = in.readString();
        pagePosition = in.readInt();
    }

    public static final Creator<ImageData> CREATOR = new Creator<ImageData>() {
        @Override
        public ImageData createFromParcel(Parcel in) {
            return new ImageData(in);
        }

        @Override
        public ImageData[] newArray(int size) {
            return new ImageData[size];
        }
    };

    public String getId()
    {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getStandard_resolution() {
        return standard_resolution;
    }

    public void setStandard_resolution(String standard_resolution) {
        this.standard_resolution = standard_resolution;
    }

    public String getLow_resolution() {
        return low_resolution;
    }

    public void setLow_resolution(String low_resolution) {
        this.low_resolution = low_resolution;
    }

    public int getPagePosition() {
        return pagePosition;
    }

    public void setPagePosition(int pagePosition) {
        this.pagePosition = pagePosition;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(thumbnail);
        parcel.writeString(standard_resolution);
        parcel.writeString(low_resolution);
        parcel.writeInt(pagePosition);
    }
}
