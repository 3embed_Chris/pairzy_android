package com.androidinsta.com;

/**
 * <h2>InstagramException</h2>
 * <P>
 *     InstagramException;
 * </P>
 * @since  12/12/2017.
 * @version 1.0.
 * @author Suresh.
 */
public class InstagramException extends Exception
{
    private String error_message;
    InstagramException(String message)
    {
        this.error_message=message;
    }

    @Override
    public String getMessage()
    {
        return error_message;
    }

    @Override
    public synchronized Throwable getCause()
    {
        return super.getCause();
    }
}
