package com.androidinsta.com;

import android.util.Log;
import org.json.JSONObject;
import org.json.JSONTokener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
/**
 * <h2>InstagramFactory </h2>
 * <P>
 *
 * </P>
 * @since /12/2017.
 * @author Suresh.
 * @since 1.0.
 */
class InstagramFactory implements Instagram
{
    private Configuration configuration;
    private AccessToken accessToken;
    InstagramFactory(Configuration configuration)
    {
        this.configuration = configuration;
    }
    @Override
    public RequestToken getOAuthRequestToken(String callbackUrl)
    {
        checkNotBuilt();
        if (StringUtil.isNullOrWhitespace(callbackUrl))
        {
            throw new IllegalArgumentException("Empty call back url" + "" + callbackUrl);
        }
        RequestToken temp = new RequestToken();
        temp.setClientId(configuration.getOAuthConsumerKey());
        temp.setClientSecret(configuration.getOAuthConsumerSecret());
        temp.setAuthCallback(callbackUrl);
        return temp;
    }

    @Override
    public void setOAuthAccessToken(AccessToken authAccessToken)
    {
        this.accessToken = authAccessToken;
    }


    private void checkNotBuilt()
    {
        if (configuration == null) {
            throw new IllegalStateException("Cannot use this builder any longer, build() has already been called");
        }
    }

    @Override
    public AccessToken getUserDetails(RequestToken requestToken,String token)
    {
        try {
            URL url = new URL(requestToken.getDetailsUrl(token));
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(false);
            int code_status=urlConnection.getResponseCode();
            if(code_status==200)
            {
                String response = streamToString(urlConnection.getInputStream());
                JSONObject jsonObj =new JSONObject(response);
                JSONObject data=jsonObj.getJSONObject("data");
                String id =data.getString("id");
                String username=data.getString("username");
                String full_name = data.getString("full_name");
                String profile_pic =data.getString("profile_picture");
                String  bio=data.getString("bio");
                String  web_site= data.getString("website");
                Log.d("123q", ":"+username);
                AccessToken accessToken = new AccessToken();
                accessToken.setACCESS_TOKEN(token);
                accessToken.setUSER_ID(id);
                accessToken.setUSER_NAME(username);
                accessToken.setFULL_NAME(full_name);
                accessToken.setUSER_PIC(profile_pic);
                accessToken.setWEB_SITE(web_site);
                accessToken.setUSER_BIO(bio);
                return accessToken;
            }else
            {
                Log.d("Error",""+code_status);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getUserRecentPost()
    {
        if(accessToken==null)
        {
            throw new IllegalArgumentException("In Valid access token");
        }
        try {
            URL url = new URL(accessToken.getUserRecentPostUrl());
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(false);
            int code_status=urlConnection.getResponseCode();
            if(code_status==200)
            {
                return streamToString(urlConnection.getInputStream());
            }else
            {
                throw  new Exception("Error on server "+code_status);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean isAccessTokenCreated()
    {
        return accessToken!=null;
    }

    @Override
    public AccessToken recreateAccessToke(InstagramSession instagramSession)
    {
        AccessToken accessToken = new AccessToken();
        accessToken.setACCESS_TOKEN(instagramSession.getAccessToken());
        accessToken.setUSER_ID(instagramSession.getId());
        accessToken.setUSER_NAME(instagramSession.getUserName());
        accessToken.setFULL_NAME(instagramSession.getUserFullName());
        accessToken.setUSER_PIC(instagramSession.getUserPic());
        accessToken.setWEB_SITE(instagramSession.getUserWeb());
        accessToken.setUSER_BIO(instagramSession.getUserBio());
        return accessToken;
    }

    @Override
    public String getOtherRecentPost(String userId,String userInstaToken) throws InstagramException
    {
        if(userInstaToken==null)
        {
            throw new InstagramException("In Valid access token");
        }
        try {
            Log.d("InstagramFactory", "getOtherRecentPost: "+AccessToken.getOtherRecentPostUrl(userId,userInstaToken));
            URL url = new URL(AccessToken.getOtherRecentPostUrl(userId,userInstaToken));
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(false);
            int code_status=urlConnection.getResponseCode();
            if(code_status==200)
            {
                return streamToString(urlConnection.getInputStream());
            }else
            {
                throw  new Exception("Error on server "+code_status);
            }
        } catch (Exception ex)
        {
            throw new InstagramException("Error :"+ex.getMessage());
        }
    }

    /*
     *Converting stream response to the string */
    private String streamToString(InputStream is) throws IOException
    {
        String str = "";
        if (is != null) {
            StringBuilder sb = new StringBuilder();
            String line;
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                reader.close();
            } finally {
                is.close();
            }
            str = sb.toString();
        }
        return str;
    }
}
